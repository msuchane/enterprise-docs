include::making-open-source-more-inclusive.adoc[]

In Identity Management, planned terminology replacements include:

* *_block list_* replaces _blacklist_
* *_allow list_* replaces _whitelist_
* *_secondary_* replaces _slave_
* The word _master_ is going to be replaced with more precise language, depending on the context:
** *_IdM server_* replaces _IdM master_
** *_CA renewal server_* replaces _CA renewal master_
** *_CRL publisher server_* replaces _CRL master_
** *_multi-supplier_* replaces _multi-master_
