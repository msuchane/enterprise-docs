:_mod-docs-content-type: PROCEDURE
[id="adding-a-custom-public-key-for-uefi-secure-boot_{context}"]
= Adding a custom public key for UEFI Secure Boot

[role="_abstract"]
You can add an existing custom public key for UEFI Secure Boot.

.Prerequisites

* You have disabled UEFI Secure Boot on the system.
* You are logged in to the system and have completed the tasks in the *Initial Setup* window.

.Procedure

. Generate a public key and store it on a local drive. For example, __my_signing_key_pub.der__.

. Enroll the Red Hat custom public key in the system's Machine Owner Key (MOK) list:
+
[subs=+quotes]
----
# mokutil --import __my_signing_key_pub.der__
----

. Enter a password when prompted.

. Reboot the system and press any key to continue the startup.
  The Shim UEFI key management utility starts during the system startup.

. Select *Enroll MOK*.

. Select *Continue*.

. Select *Yes* and enter the password.
+
The key is imported into the system's firmware.

. Select *Reboot*.

. Enable Secure Boot on the system.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_monitoring_and_updating_the_kernel/signing-a-kernel-and-modules-for-secure-boot_managing-monitoring-and-updating-the-kernel#generating-a-public-and-private-key-pair_signing-a-kernel-and-modules-for-secure-boot[Generating public and private keys]
