:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// assembly_kickstart-and-advanced-boot-options.adoc

[id="removed-boot-options_{context}"]
= Removed boot options

[role="_abstract"]
The following boot options have been removed from {productname}:

inst.zram::
The `zram.service` cannot be run anymore. See `zram-generator` for more information.

inst.singlelang::
The single language mode is not supported anymore.

inst.loglevel::
The log level is always set to debug.
