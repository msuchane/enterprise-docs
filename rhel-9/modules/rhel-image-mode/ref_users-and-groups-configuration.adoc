:_newdoc-version: 2.15.0
:_template-generated: 2024-4-16

:_mod-docs-content-type: REFERENCE

[id="users-and-groups-configuration_{context}"]
= Users and groups configuration

[role="_abstract"]
Image mode for RHEL is a generic operating system update and configuration mechanism. You cannot use it to configure users or groups. 
The only exception is the `bootc install` command that has the `--root-ssh-authorized-keys` option.

Users and groups configuration for generic base images::

Usually, the distribution base images do not have any configuration. 
Do not encrypt passwords and SSH keys with publicly-available private keys in generic images because of security risks.

Injecting SSH keys through `systemd` credentials::

You can use `systemd` to inject a root password or SSH `authorized_keys` file in some environments. 
For example, use System Management BIOS (SMBIOS) to inject SSH keys system firmware. You can configure this in local virtualization environments, such as `qemu`.

Injecting users and SSH keys by using `cloud-init`::

Many Infrastructure as a service (IaaS) and virtualization systems use metadata servers that are commonly processed by software such as `cloud-init` or `ignition`. See link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html[AWS instance metadata].
The base image you are using might include `cloud-init` or Ignition, or you can install it in your own derived images.
In this model, the SSH configuration is managed outside of the bootable image. 

Adding users and credentials by using container or unit custom logic::

Systems such as `cloud-init` are not privileged. You can inject any logic you want to manage credentials in the way you want to launch a container image, for example, by using a `systemd` unit. To manage the credentials, you can use a custom network-hosted source, for example, link:https://www.freeipa.org/page/Main_Page[FreeIPA].

Adding users and credentials statically in the container build::

In package-oriented systems, you can use the derived build to inject users and credentials by using the following command:
+
[literal,subs="+quotes"]
----
`RUN useradd someuser` 
----
+
You can find issues in the default `shadow-utils` implementation of `useradd`:
Users and groups IDs are allocated dynamically, and this can cause drift.

User and group home directories and `/var` directory::

For systems configured with persistent `/home → /var/home`, any changes to `/var` made in the container image after initial installation will not be applied on subsequent updates. 
+
For example, if you inject `/var/home/someuser/.ssh/authorized_keys` into a container build, existing systems do not get the updated `authorized_keys` file.

Using DynamicUser=yes for `systemd` units::

Use the `systemd` `DynamicUser=yes` option where possible for system users.
+
This is significantly better than the pattern of allocating users or groups at package install time, because it avoids potential UID or GID drift.

Using `systemd`-sysusers::

Use `systemd`-sysusers, for example, in your derived build. For more information, see the `systemd` link:https://www.freedesktop.org/software/systemd/man/latest/systemd-sysusers.html[-sysusers] documentation.
+
[literal,subs="+quotes"]
----
COPY mycustom-user.conf /usr/lib/sysusers.d
----
+
The `sysusers` tool makes changes to the traditional `/etc/passwd` file as necessary during boot time. If `/etc` is persistent, this can avoid `UID` or `GID` drift. It means that the `UID` or `GID` allocation depends on how a specific machine was upgraded over time.

Using `systemd` JSON user records::

See link:https://systemd.io/USER_RECORD/[JSON user records] `systemd` documentation. Unlike `sysusers`, the canonical state for these users lives in `/usr`. If a subsequent image drops a user record, then it also vanishes from the system.

Using `nss-altfiles`::

With `nss-altfiles`, you can remove the `systemd` JSON user records. It splits system users into `/usr/lib/passwd` and `/usr/lib/group`, aligning with the way the OSTree project handles the 3 way merge for `/etc` as it relates to `/etc/passwd`. Currently, if the `/etc/passwd` file is modified in any way on the local system, then subsequent changes to `/etc/passwd` in the container image are not applied.
+
Base images built by `rpm-ostree` have `nns-altfiles` enabled by default.
+
Also, base images have a system users pre-allocated and managed by the NSS file to avoid UID or GID drift.
+
In a derived container build, you can also append users to `/usr/lib/passwd`, for example. Use `sysusers.d` or `DynamicUser=yes`.

Machine-local state for users::

The filesystem layout depends on the base image.
+
By default, the user data is stored in both `/etc`, `/etc/passwd`, `/etc/shadow` and `groups`, and `/home`, depending on the base image. However, the generic base images have to both be machine-local persistent state. In this model `/home` is a symlink to `/var/home/_user_`.

Injecting users and SSH keys at system provisioning time::

For base images where `/etc` and `/var` are configured to persist by default, you can inject users by using installers such as Anaconda or Kickstart.
+
Typically, generic installers are designed for one time bootstrap. Then, the configuration becomes a mutable machine-local state that you can change in Day 2 operations, by using some other mechanism.
+
You can use the Anaconda installer to set the initial password. However, changing this initial password requires a different in-system tool, such as `passwd`.
+
These flows work equivalently in a `bootc-compatible` system, to support users directly installing generic base images, without requiring changes to the different in-system tool.

Transient home directories::

Many operating system deployments minimize persistent, mutable, and executable state. This can damage user home directories.
+
The `/home` directory can be set as `tmpfs`, to ensure that user data is cleared across reboots. This approach works especially well when combined with a transient `/etc` directory.
+
To set up the user's home directory to, for example, inject SSH `authorized_keys` or other files, use the `systemd` `tmpfiles.d` snippets:
+
[literal,subs="+quotes"]
----
f~ /home/_user_/.ssh/_authorized_keys_ 600 _user_ _user_ - <base64 encoded data>
----
+
SSH is embedded in the image as: `/usr/lib/tmpfiles.d/<username-keys.conf`.
Another example is a service embedded in the image that can fetch keys from the network and write them. This is the pattern used by `cloud-init`.

UID and GID drift::

The `/etc/passwd` and similar files are a mapping between names and numeric identifiers. When the mapping is dynamic and mixed with "stateless" container image builds, it can cause issues.
Each container image build might result in the UID changing due to RPM installation ordering or other reasons.
This can be a problem if that user maintains a persistent state. To handle such cases, convert it to use `sysusers.d` or use `DynamicUser=yes`.



