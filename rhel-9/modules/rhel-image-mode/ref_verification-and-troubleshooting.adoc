:_newdoc-version: 2.15.0
:_template-generated: 2024-4-18

:_mod-docs-content-type: REFERENCE

[id="verification-and-troubleshooting_{context}"]
= Verification and troubleshooting

[role="_abstract"]
If you have any issues configuring the requirements for your AWS image, see the following documentation::

* link:https://aws.amazon.com/iam/[AWS IAM account manager]
* link:https://docs.aws.amazon.com/cli/latest/userguide/cli-services-s3-commands.html[Using high-level (s3) commands with the AWS CLI].
* link:https://aws.amazon.com/s3/[S3 buckets].
* link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-regions[Regions and Zones].
* link:https://access.redhat.com/documentation/en-us/red_hat_insights/1-latest/html-single/deploying_and_managing_rhel_systems_in_hybrid_clouds/index#proc_launching-customized-rhel-image-to-aws.adoc_host-management-services[Launching a customized RHEL image on AWS].


For more details on users, groups, SSH keys, and secrets, see::

* xref:managing-users-groups-ssh-key-and-secrets-in-image-mode-for-rhel_using-image-mode-for-rhel-to-build-deploy-and-manage-operating-systems[Managing users, groups, SSH keys, and secrets in image mode for RHEL]
