:_newdoc-version: 2.16.0
:_template-generated: 2024-04-16
:_mod-docs-content-type: PROCEDURE

[id="building-and-launching-configured-images_{context}"]
= Building, configuring, and launching disk images with bootc-image-builder

[role="_abstract"]
You can inject configuration into a custom image by using a Containerfile. 

.Procedure

. Create a disk image. The following example shows how to add a user to the disk image.
+
[literal,subs="+quotes"]
----
[[blueprint.customizations.user]]
name = "user"
password = "pass"
key = "ssh-rsa AAA ... user@email.com"
groups = ["wheel"]
----
* `name` - User name. Mandatory
* `password` - Nonencrypted password. Not mandatory
* `key` - Public SSH key contents. Not mandatory
* `groups` -	An array of groups to add the user into. Not mandatory

. Run `bootc-image-builder` and pass the following arguments:
+
[literal,subs="+quotes"]
----
$ *sudo podman run \*
    *--rm \*
    *-it \*
    *--privileged \*
    *--pull=newer \*
    *--security-opt label=type:unconfined_t \*
    *-v $(pwd)/config.toml:/config.toml \*
    *-v $(pwd)/output:/output \*
    *registry.redhat.io/rhel9/bootc-image-builder:latest \*
    *--type qcow2 \*
    *--config config.toml \*
    *quay.io/_<namespace>_/_<image>_:__<tag>__*
----

. Launch a VM, for example, by using `virt-install`:
+
[literal,subs="+quotes"]
----
$ *sudo virt-install \*
  *--name bootc \*
  *--memory 4096 \*
  *--vcpus 2 \*
  *--disk qcow2/disk.qcow2 \*
  *--import \*
  *--os-variant rhel9*
----

.Verification

* Access the system with SSH:
+
[literal,subs="+quotes"]
----
# *ssh -i /_<path_to_private_ssh-key>_ _<user1>_@_<ip-address>_*
----

.Next steps
* After you deploy your container image, you can make updates to the image and push the changes to a registry. See xref:managing-rhel-bootable-images_using-image-mode-for-rhel-to-build-deploy-and-manage-operating-systems[Managing RHEL bootable images].



