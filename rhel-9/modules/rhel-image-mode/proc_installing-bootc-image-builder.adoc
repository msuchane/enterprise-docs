:_newdoc-version: 2.16.0
:_template-generated: 2024-04-15
:_mod-docs-content-type: PROCEDURE

[id="installing-bootc-image-builder_{context}"]
= Installing bootc-image-builder

[role="_abstract"]
The `bootc-image-builder` is intended to be used as a container and it is not available as an RPM package in RHEL. To access it, follow the procedure.

.Prerequisites

* The `container-tools` meta-package is installed. The meta-package contains all container tools, such as Podman, Buildah, and Skopeo. 
* You are authenticated to `registry.redhat.io`. For details, see link:https://access.redhat.com/RegistryAuthentication[Red Hat Container Registry Authentication]. 


.Procedure

. Login to authenticate to `registry.redhat.io`:
+
[literal,subs="+quotes"]
----
$ *sudo podman login registry.redhat.io*
----

. Install the `bootc-image-builder` tool:
+
[literal,subs="+quotes"]
----
$ *sudo podman pull registry.redhat.io/rhel9/bootc-image-builder* 
----

.Verification

* List all images pulled to your local system:
+
[literal,subs="+quotes"]
----
$ *sudo podman images*
REPOSITORY                                    TAG         IMAGE ID      CREATED       SIZE
registry.redhat.io/rhel9/bootc-image-builder  latest      b361f3e845ea  24 hours ago  676 MB
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/RegistryAuthentication[Red Hat Container Registry Authentication]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/building_running_and_managing_containers/index#proc_pulling-images-from-registries_working-with-container-registries[Pulling images from registries]



