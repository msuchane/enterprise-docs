:_newdoc-version: 2.16.0
:_template-generated: 2024-04-15
:_mod-docs-content-type: PROCEDURE

[id="creating-raw-disk-images-by-using-bootc-image-builder_{context}"]
= Creating Raw disk images by using bootc-image-builder

[role="_abstract"]
You can convert a bootable container image to a Raw image with an MBR or GPT partition table by using `bootc-image-builder`. The RHEL base image does not include a default user, so optionally, you can inject a user configuration with the `--config` option to run the `bootc-image-builder` container. Alternatively, you can configure the base image with `cloud-init` to inject users and SSH keys on first boot. See xref:users-and-groups-configuration_managing-users-groups-ssh-key-and-secrets-in-image-mode-for-rhel[Injecting users and SSH keys by using cloud-init].

.Prerequisites

* You have Podman installed on your host machine.
* You have root access to run the `bootc-image-builder` tool, and run the containers in `--privileged` mode, to build the images.
* You have pulled your target container image in the container storage. 

.Procedure

. Optional: Create a `config.toml` to configure user access, for example:
+
[literal,subs="+quotes"]
----
[[blueprint.customizations.user]]
name = "user"
password = "pass"
key = "ssh-rsa AAA ... user@email.com"
groups = ["wheel"]
----

. Run `bootc-image-builder`. If you want to use user access configuration, pass the `config.toml` as an argument:
+
[literal,subs="+quotes"]
----
$ *sudo podman run \*
    *--rm \*
    *-it \*
    *--privileged \*
    *--pull=newer \*
    *--security-opt label=type:unconfined_t \*
    *-v /var/lib/containers/storage:/var/lib/containers/storage \*
    *-v ./config.toml:/config.toml \*
    *-v ./output:/output \*
    *registry.redhat.io/rhel9/bootc-image-builder:latest \*
    *--local \*
    *--type raw \*
    *--config config.toml \*
  *quay.io/_<namespace>_/_<image>_:__<tag>__*
----
+
You can find the `.raw` image in the output folder. 
 
[role="_additional-resources"]
.Next steps

* You can deploy your image. See xref:deploying-a-container-image-using-kvm-qemu-with-a-qcow2-disk-image_deploying-the-rhel-bootable-images[Deploying a container image by using KVM with a QCOW2 disk image]. 
* You can make updates to the image and push the changes to a registry. See xref:managing-rhel-bootable-images_using-image-mode-for-rhel-to-build-deploy-and-manage-operating-systems[Managing RHEL bootable images].



