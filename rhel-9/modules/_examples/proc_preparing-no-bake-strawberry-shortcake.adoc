:_mod-docs-content-type: PROCEDURE
[id="proc_preparing-no-bake-strawberry-shortcake_{context}"]
= Preparing no-bake strawberry shortcake

[role="_abstract"]
Foodista is a new online destination for those interested in all things culinary-related. The site is divided into four sections – recipes, foods, tools and techniques – and is based around the idea that community knowledge and sharing can result in a better resource than one built by a restricted and closed group. As such, the folks behind Foodista have “developed a system to let everyone edit content to make it better rather than have multiple versions of the same recipe.” At its core, this system is based around a site-wide CC BY license.

.Prerequisites

* 1 box of vanilla instant pudding
* 1/2 cup strawberry juice
* 1 1/2 cups non-fat milk
* 1 teaspoon vanilla extract
* 24 Savoiardi. Lady Fingers
* 220 g (about 1 cup) whipped cream, chilled
* 1 pound of fresh strawberries, hulled and sliced and patted dry

.Procedure

. In a bowl Whisk pudding, milk and vanilla and set aside
. With a hand mixer or in the bowl of a stand mixer, whip the cream until it just holds, stiff peaks.
. [Optional] Add sugar.
. Dip ladyfingers briefly in strawberry juice and arrange them in the bottom of a dish.
. Spread half the pudding mixture over the ladyfingers.
. Place the strawberries in a single layer over the pudding.
. Repeat with remaining ladyfingers, pudding mixture and strawberries.
. Top with whipped cream and strawberries.
. Chill for at least 4 hours before serving.

.Verification

. Check the cake using your eyes.
. Smell to it.
. Grab a spoon, open your mouth, and try a first slice.

[role="_additional-resources"]
.Additional resources
* link:https://www.foodista.com/[Foodista]
* link:https://creativecommons.org/licenses/by/3.0/[CC BY license]
