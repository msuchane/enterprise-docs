:_mod-docs-content-type: PROCEDURE
[id=stop-decommission-server_{context}]

= Stopping and decommissioning the RHEL 8 server

[role="_abstract"]
. Make sure that all data, including the latest changes, have been correctly migrated from `rhel8.example.com` to `rhel9.example.com`. For example:
.. Add a new user on `rhel8.example.com`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel8 ~]# *ipa user-add _random_user_*
First name: random
Last name: user
....

.. Check that the user has been replicated to `rhel9.example.com`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel9 ~]# *ipa user-find _random_user_*
--------------
1 user matched
--------------
  User login: random_user
  First name: random
  Last name: user
....

+
. Stop all IdM services on `rhel8.example.com` to force domain discovery to the new `rhel9.example.com` server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel8 ~]# *ipactl stop*
Stopping CA Service
Stopping pki-ca:                                           [  OK  ]
Stopping HTTP Service
Stopping httpd:                                            [  OK  ]
Stopping MEMCACHE Service
Stopping ipa_memcached:                                    [  OK  ]
Stopping DNS Service
Stopping named:                                            [  OK  ]
Stopping KPASSWD Service
Stopping Kerberos 5 Admin Server:                          [  OK  ]
Stopping KDC Service
Stopping Kerberos 5 KDC:                                   [  OK  ]
Stopping Directory Service
Shutting down dirsrv:
    EXAMPLE-COM...                                         [  OK  ]
    PKI-IPA...                                             [  OK  ]
....

+
After this, the `ipa` utility will contact the new server through a remote procedure call (RPC).

+
. Remove the RHEL 8 server from the topology by executing the removal commands on the RHEL 9 server. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/installing_identity_management/uninstalling-an-ipa-server_installing-identity-management[Uninstalling an IdM server].
