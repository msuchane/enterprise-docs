:_mod-docs-content-type: CONCEPT
[id="system-requirements-in-ipa_{context}"]
= Custom configuration requirements for IdM

Install an {IPA} (IdM) server on a clean system without any custom configuration for services such as DNS, Kerberos, Apache, or Directory{nbsp}Server.

The IdM server installation overwrites system files to set up the IdM domain. IdM backs up the original system files to `/var/lib/ipa/sysrestore/`. When an IdM server is uninstalled at the end of the lifecycle, these files are restored.

== IPv6 requirements in IdM

The IdM system must have the IPv6 protocol enabled in the kernel. If IPv6 is disabled, then the CLDAP plug-in used by the IdM services fails to initialize.

[NOTE]
====
IPv6 does not have to be enabled on the network.
====


== Support for encryption types in IdM

Red Hat Enterprise Linux (RHEL) uses Version 5 of the Kerberos protocol, which supports encryption types such as Advanced Encryption Standard (AES), Camellia, and Data Encryption Standard (DES).

.List of supported encryption types

While the Kerberos libraries on IdM servers and clients might support more encryption types, the IdM Kerberos Distribution Center (KDC) only supports the following encryption types:

* `aes256-cts:normal`
* `aes256-cts:special` (default)
* `aes128-cts:normal`
* `aes128-cts:special` (default)
* `aes128-sha2:normal`
* `aes128-sha2:special`
* `aes256-sha2:normal`
* `aes256-sha2:special`
* `camellia128-cts-cmac:normal`
* `camellia128-cts-cmac:special`
* `camellia256-cts-cmac:normal`
* `camellia256-cts-cmac:special`

.RC4 encryption types are disabled by default

The following RC4 encryption types have been disabled by default in RHEL 9, as they are considered less secure than the newer AES-128 and AES-256 encryption types:

* `arcfour-hmac:normal`
* `arcfour-hmac:special`

For more information about manually enabling RC4 support for compatibility with legacy Active Directory environments, see
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/installing_trust_between_idm_and_ad/ensuring-support-for-common-encryption-types-in-ad-and-rhel_installing-trust-between-idm-and-ad[Ensuring support for common encryption types in AD and RHEL].
endif::[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_identity_management/installing-trust-between-idm-and-ad_installing-identity-management#ensuring-support-for-common-encryption-types-in-ad-and-rhel_installing-trust-between-idm-and-ad[Ensuring support for common encryption types in AD and RHEL].
endif::[]

.Support for DES and 3DES encryption has been removed

Due to security reasons, support for the DES algorithm was deprecated in RHEL 7. Single-DES (DES) and triple-DES (3DES) encryption types were removed from RHEL 8 and are not used in RHEL 9.


== Support for system-wide cryptographic policies in IdM

IdM uses the `DEFAULT` system-wide cryptographic policy. This policy offers secure settings for current threat models. It allows the TLS 1.2 and 1.3 protocols, as well as the IKEv2 and SSH2 protocols. The RSA keys and Diffie-Hellman parameters are accepted if they are at least 2048 bits long. This policy does not allow DES, 3DES, RC4, DSA, TLS v1.0, and other weaker algorithms.

[NOTE]
====
You cannot install an IdM server while using the `FUTURE` system-wide cryptographic policy. When installing an IdM server, ensure you are using the `DEFAULT` system-wide cryptographic policy.
====

.Additional Resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[System-wide cryptographic policies]

== FIPS compliance

You can install a new IdM server or replica on a system with the Federal Information Processing Standard (FIPS) mode enabled. The only exception is a system on which the `FIPS:OSPP` cryptographic subpolicy is enabled. 

To install IdM with FIPS, first enable FIPS mode on the host, then install IdM. The IdM installation script detects if FIPS is enabled and configures IdM to only use encryption types that are compliant with FIPS 140-3:

* `aes128-sha2:normal`
* `aes128-sha2:special`
* `aes256-sha2:normal`
* `aes256-sha2:special`

For an IdM environment to be FIPS-compliant, *all* IdM replicas must have FIPS mode enabled.

Red Hat recommends that you enable FIPS in IdM clients as well, especially if you might promote those clients to IdM replicas. Ultimately, it is up to administrators to determine how they meet FIPS requirements; Red Hat does not enforce FIPS criteria.

.Migration to FIPS-compliant IdM

You cannot migrate an existing IdM installation from a non-FIPS environment to a FIPS-compliant installation. This is not a technical problem but a legal and regulatory restriction.

To operate a FIPS-compliant system, all cryptographic key material must be created in FIPS mode. Furthermore, the cryptographic key material must never leave the FIPS environment unless it is securely wrapped and never unwrapped in non-FIPS environments.

If your scenario requires a migration of a non-FIPS IdM realm to a FIPS-compliant one, you must:

. create a new IdM realm in FIPS mode
. perform data migration from the non-FIPS realm to the new FIPS-mode realm with a filter that blocks all key material

The migration filter must block:

* KDC master key, keytabs, and all related Kerberos key material
* user passwords
* all certificates including CA, service, and user certificates
* OTP tokens
* SSH keys and fingerprints
* DNSSEC KSK and ZSK
* all vault entries
* AD trust-related key material

Effectively, the new FIPS installation is a different installation. Even with rigorous filtering, such a migration may not pass a FIPS 140 certification. Your FIPS auditor may flag this migration.

.Support for cross-forest trust with FIPS mode enabled

To establish a cross-forest trust with an Active Directory (AD) domain while FIPS mode is enabled, you must authenticate with an AD administrative account. You cannot establish a trust using a shared secret while FIPS mode is enabled.

[IMPORTANT]
====
RADIUS authentication is not FIPS compliant. Do not install IdM on a server with FIPS mode enabled if you require RADIUS authentication.
////
RADIUS authentication is not FIPS compliant as the RADIUS protocol uses the MD5 hash function to encrypt passwords between client and server and, in FIPS mode, OpenSSL disables the use of the MD5 digest algorithm. However, if the RADIUS server is running on the same host as the IdM server, you can work around the problem and enable MD5 by performing the steps described in link:https://access.redhat.com/solutions/4650511[How to configure FreeRADIUS authentication in FIPS mode].
////
====

.Additional Resources
* To enable FIPS mode in the RHEL operating system, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening#switching-the-system-to-fips-mode_using-the-system-wide-cryptographic-policies[Switching the system to FIPS mode] in the _Security Hardening_ guide.
* For more details on FIPS 140-2, see the link:https://csrc.nist.gov/publications/detail/fips/140/2/final[Security Requirements for Cryptographic Modules] on the National Institute of Standards and Technology (NIST) web site.

////
== IPv6 requirements in IdM

IPv6 must be enabled on the network to enable installing and running an IdM server. Most of IdM servers listen on TCP6 or UDP6 on all network interfaces. This
allows to implement double stack (TCP/TCP6 or UDP/UDP6) with the same code base and is recommended for contemporary networking applications.

IPv6 is enabled by default on {RHEL}{nbsp}7 systems. If you have disabled it, re-enable it following the steps in link:++https://access.redhat.com/solutions/8709++[How do I disable or enable the IPv6 protocol in Red Hat Enterprise Linux?].
// in Red{nbsp}Hat Knowledgebase.
//NOTE: IPv6 is enabled by default on {RHEL}{nbsp}7 systems.

//.Additional resources

//* If you disabled IPv6 before, re-enable it as described in link:++https://access.redhat.com/solutions/8709++[How do I disable or enable the IPv6 protocol in Red Hat Enterprise Linux?] in Red{nbsp}Hat Knowledgebase.
////
