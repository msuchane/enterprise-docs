:_content-type: REFERENCE

[id="authentication-policies_{context}"]
= Authentication policies

[role="_abstract"]

Use authentication policies to configure the available online and local authentication methods. 

Authentication with online connection:: Uses all online authentication methods that the service provides on the server side. For IdM, AD, or Kerberos services, the default authentication method is Kerberos.
Authentication without online connection:: Uses authentication methods that are available for a user. You can tune the authentication method with the [option]`local_auth_policy` option.

Use the [option]`local_auth_policy` option in the `/etc/sssd/sssd.conf` file to configure the available online and offline authentication methods. By default, the authentication is performed only with the methods that the server side of the service supports. You can tune the policy with the following values:

* The `match` value enables the matching of offline and online states. For example, the IdM server supports online passkey authentication and `match` enables offline and online authentications for the passkey method.
* The `only` value offers only offline methods and ignores the online methods.
* The `enable` and `disable` values explicitly define the methods for offline authentication. For example, `enable:passkey` enables only passkey for offline authentication.

The following configuration example allows local users to authenticate locally using smart card authentication:

----
[domain/shadowutils]
id_provider = proxy
proxy_lib_name = files
auth_provider = none
local_auth_policy = only
----

The [option]`local_auth_policy` option applies to the passkey and smart card authentication methods.
