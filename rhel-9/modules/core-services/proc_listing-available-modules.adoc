:_mod-docs-content-type: PROCEDURE

[id="proc_listing-available-modules_{context}"]
= Listing available modules and their contents

[role="_abstract"]
By searching for modules and displaying information about them with *{PackageManagerName}*, you can identify which modules are available in the repositories and select the appropriate stream before you install a module.


.Procedure

. List the module information in one of the following ways:

** List all available modules:
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} module list*
Name        Stream   Profiles                               Summary
...
nodejs      18       common [d], development, minimal, s2i  Javascript runtime
postgresql  15       client, server                         PostgreSQL server and client module
...
Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
....
+
Use the `{PackageManagerCommand} module list __<module_name>__` command to list the same information but only for a specific module.

** Search for which module provides a certain package: 
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} module provides __<package_name>__*
....
+
For example, to display which module and profiles provide the `npm` package, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module provides npm*
npm-1:8.19.2-1.18.10.0.3.module+el9.1.0+16866+0fab0697.x86_64
Module   : nodejs:18:9010020221009220316:rhel9:x86_64
Profiles : common development s2i
Repo     : rhel-9-for-x86_64-appstream-rpms
Summary  : Javascript runtime
...
....

. Use one of these methods to list module details:

** List all details about a module, including a description, list of all profiles, and a list of all packages the module provides:
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} module info __<module_name>__*
....
+
For example, to display details about the `nodejs` package, enter:
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} module info nodejs*
Name             : nodejs
Stream           : 18
Version          : 9010020221009220316
Context          : rhel9
Architecture     : x86_64
Profiles         : common [d], development, minimal, s2i
Default profiles : common
Repo             : rhel-9-for-x86_64-appstream-rpms
Summary          : Javascript runtime
Description      : Node.js is a platform built on Chrome's JavaScript runtime...
Requires         : platform:[el9]
Artifacts        : nodejs-1:18.10.0-3.module+el9.1.0+16866+0fab0697.src
                 : nodejs-1:18.10.0-3.module+el9.1.0+16866+0fab0697.x86_64
                 : npm-1:8.19.2-1.18.10.0.3.module+el9.1.0+16866+0fab0697.x86_64
...
....

** List which packages each module profile installs:
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} module info --profile __<module_name>__*
....
+
For example, to display this information for the `nodejs` module, enter:
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} module info --profile _nodejs_*
Name        : nodejs:18:9010020221009220316:rhel9:x86_64
common      : nodejs
            : npm
development : nodejs
            : nodejs-devel
            : npm
minimal     : nodejs
s2i         : nodejs
            : nodejs-nodemon
            : npm
...
....


[role="_additional-resources"]
.Additional resources

* xref:con_modules_assembly_distribution-of-content-in-rhel-9[Modules]
* xref:con_module-streams_assembly_distribution-of-content-in-rhel-9[Module streams]
* xref:con_module-profiles_assembly_distribution-of-content-in-rhel-9[Module profiles]

