:_mod-docs-content-type: PROCEDURE

[id="proc_enabling-and-disabling-yum-plug-ins_{context}"]
= Enabling and disabling {PackageManagerName} plug-ins

[role="_abstract"]
In the *{PackageManagerName}* tool, plug-ins are loaded by default. However, you can influence which plug-ins *{PackageManagerName}* loads.

[WARNING]
====
Disable all plug-ins only for diagnosing a potential problem. *{PackageManagerName}* requires certain plug-ins, such as `product-id` and `subscription-manager`, and disabling them causes {ProductName} to not be able to install or update software from the Content Delivery Network (CDN).
====


.Procedure

* Use one of the following methods to influence how *{PackageManagerName}* uses plug-ins:

** To enable or disable loading of *{PackageManagerName}* plug-ins globally, add the `plugins` parameter to the `[main]` section of the `/etc/dnf/dnf.conf` file.

*** Set `plugins=1` (default) to enable loading of all *{PackageManagerName}* plug-ins.
*** Set `plugins=0` to disable loading of all *{PackageManagerName}* plug-ins.

** To disable a particular plug-in, add `enabled=False` to the `[main]` section in the `/etc/dnf/plugins/_<plug-in_name>_.conf` file.

** To disable all *{PackageManagerName}* plug-ins for a particular command, append the `--noplugins` option to the command. For example, to disable *{PackageManagerName}* plug-ins for a single update command, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} --noplugins update*
....

** To disable certain *{PackageManagerName}* plug-ins for a single command, append the `--disableplugin=_plugin-name_` option to the command. For example, to disable a certain *{PackageManagerName}* plug-in for a single update command, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} update --disableplugin=__<plugin_name>__*
....

** To enable certain *{PackageManagerName}* plug-ins for a single command, append the `--enableplugin=_plugin-name_` option to the command. For example, to enable a certain *{PackageManagerName}* plug-in for a single update command, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} update --enableplugin=__<plugin_name>__*
....

