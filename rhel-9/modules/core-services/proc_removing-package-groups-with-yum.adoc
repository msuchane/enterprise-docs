
:_mod-docs-content-type: PROCEDURE

[id="proc_removing-package-groups-with-yum_{context}"]
= Removing package groups

[role="_abstract"]
Package groups bundle multiple packages. You can use package groups to remove all packages assigned to a group in a single step. 

.Procedure

* Remove package groups by the group name or group ID:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} group remove _<group_name>_ _<group_id>_*
....
