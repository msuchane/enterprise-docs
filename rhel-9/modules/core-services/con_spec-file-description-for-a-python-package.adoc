:_mod-docs-content-type: CONCEPT
[id="con_spec-file-description-for-a-python-package_{context}"]
= SPEC file description for a Python package

[role="_abstract"]
A SPEC file contains instructions that the `rpmbuild` utility uses to build an RPM. The instructions are included in a series of sections. A SPEC file has two main parts in which the sections are defined:

* Preamble (contains a series of metadata items that are used in the Body)
* Body (contains the main part of the instructions)

//TODO: add a conditional reference to Packaging; the assembly is now reused in this title too.
//For further information about SPEC files, see Packaging and distributing software.

An RPM SPEC file for Python projects has some specifics compared to non-Python RPM SPEC files. 

IMPORTANT: A name of any RPM package of a Python library must always include the `python3-`, `python3.11-`, or `python3.12-` prefix.

Other specifics are shown in the following SPEC file example for the `pass:[python3*-pello]` package. For description of such specifics, see the notes below the example.
