:_mod-docs-content-type: CONCEPT

[id="con_repositories_{context}"]
= Repositories

[role="_abstract"]
{ProductName} ({ProductShortName}) distributes content through different repositories, for example:

BaseOS::
Content in the BaseOS repository consists of the core set of the underlying operating system functionality that provides the foundation for all installations. This content is available in the RPM format and is subject to support terms similar to those in earlier releases of {ProductShortName}.

AppStream::
Content in the AppStream repository includes additional user-space applications, runtime languages, and databases in support of the varied workloads and use cases.

[IMPORTANT]
====
Both the BaseOS and AppStream content sets are required by {ProductShortName} and are available in all {ProductShortName} subscriptions.
====

CodeReady Linux Builder::
The CodeReady Linux Builder repository is available with all {ProductShortName} subscriptions. It provides additional packages for use by developers. {RH} does not support packages included in the CodeReady Linux Builder repository.



[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/package_manifest/index[Package manifest]

