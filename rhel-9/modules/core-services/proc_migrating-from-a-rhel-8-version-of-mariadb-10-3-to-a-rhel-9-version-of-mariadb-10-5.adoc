:_newdoc-version: 2.17.0
:_template-generated: 2024-04-11
:_mod-docs-content-type: PROCEDURE

[id="migrating-from-a-rhel-8-version-of-mariadb-10-3-to-a-rhel-9-version-of-mariadb-10-5_{context}"]
= Migrating from a RHEL 8 version of MariaDB 10.3 to a RHEL 9 version of MariaDB 10.5

[role="_abstract"]
This procedure describes migrating from the *MariaDB 10.3* to the *MariaDB 10.5* using the `mariadb-upgrade` utility.

The `mariadb-upgrade` utility is provided by the `mariadb-server-utils` subpackage, which is installed as a dependency of the `mariadb-server` package.

.Prerequisites

* Before performing the upgrade, back up all your data stored in the *MariaDB* databases.

.Procedure

. Ensure that the `mariadb-server` package is installed on the RHEL 9 system:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install mariadb-server*
----

. Ensure that the `mariadb` service is not running on either of the source and target systems at the time of copying data:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl stop mariadb.service*
----

. Copy the data from the source location to the `/var/lib/mysql/` directory on the RHEL 9 target system.

. Set the appropriate permissions and SELinux context for copied files on the target system:
+
[literal,subs="+quotes,attributes"]
----
# *restorecon -vr /var/lib/mysql*
----

. Ensure that `mysql:mysql` is owner of all data in the `/var/lib/mysql` directory:
+
[literal,subs="+quotes,attributes"]
----
# *chown -R mysql:mysql /var/lib/mysql*
----

. Adjust the configuration so that option files located in `/etc/my.cnf.d/` include only options valid for *MariaDB 10.5*. For details, see upstream documentation for link:https://mariadb.com/kb/en/upgrading-from-mariadb-103-to-mariadb-104/#options-that-have-changed-default-values[MariaDB 10.4] and link:https://mariadb.com/kb/en/upgrading-from-mariadb-104-to-mariadb-105/#options-that-have-been-removed-or-renamed[MariaDB 10.5].

. Start the *MariaDB* server on the target system.
+
* When upgrading a database running standalone:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl start mariadb.service*
----
+
* When upgrading a *Galera* cluster node:
+
[literal,subs="+quotes,attributes"]
----
# *galera_new_cluster*
----
+
The `mariadb` service will be started automatically.

. Execute the *mariadb-upgrade* utility to check and repair internal tables.
+
* When upgrading a database running standalone:
+
[literal,subs="+quotes,attributes"]
----
$ *mariadb-upgrade*
----
+
* When upgrading a *Galera* cluster node:
+
[literal,subs="+quotes,attributes"]
----
$ *mariadb-upgrade --skip-write-binlog*
----


[IMPORTANT]
====
There are certain risks and known problems related to an in-place upgrade. For example, some queries might not work or they will be run in a different order than before the upgrade. For more information about these risks and problems, and for general information about an in-place upgrade, see link:https://mariadb.com/kb/en/release-notes-mariadb-105-series/[MariaDB 10.5 Release Notes].
====
