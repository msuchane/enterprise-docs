:_mod-docs-content-type: PROCEDURE

[id="proc_installing-packages_{context}"]
= Installing packages

[role="_abstract"]
If a software is not part of the default installation, you can manually install it. *{PackageManagerName}* automatically resolves and installs dependencies.


.Prerequisites

* Optional: xref:assembly_searching-for-rhel-9-content_managing-software-with-the-dnf-tool[You know the name of the package you want to install].
* If the package you want to install is provided by a module stream, the respective module stream is enabled.


.Procedure

* Use one of the following methods to install packages:

** To install packages from the repositories, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} install __<package_name_1>__ __<package_name_2>__ ...*
....
+
If you install packages on a system that supports multiple architectures, such as `i686` and `x86_64`, you can specify the architecture of the package by appending it to the package name:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} install __<package_name>__.__<architecture>__*
....

** To install a package if you only know the path to the file the package provides but not the package name, you can use this path to install the corresponding package:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} install __<path_to_file>__*
....

** To install a local RPM file, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} install __<path_to_RPM_file>__*
....
+
If the package has dependencies, specify the paths to these RPM files as well. Otherwise, *{PackageManagerName}* downloads the dependencies from the repositories or fails if they are not available in the repositories.


[role="_additional-resources"]
.Additional resources

* xref:proc_installing-modular-content_{context}[Installing modular content]

