:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_migrating-to-a-rhel-9-version-of-postgresql.adoc

[id="fast-upgrade-using-the-pg_upgrade-tool_{context}"]
= Fast upgrade using the pg_upgrade utility
[role="_abstract"]

As a system administrator, you can upgrade to the most recent version of *PostgreSQL* by using the fast upgrade method. To perform a fast upgrade, copy binary data files to the [filename]`/var/lib/pgsql/data/` directory and use the `pg_upgrade` utility.

You can use this method for migrating data: 

* From the RHEL 8 version of *PostgreSQL 12* to a RHEL version of *PostgreSQL 13*
* From a RHEL 8 or 9 version of *PostgreSQL 13* to a RHEL version of *PostgreSQL 15*
* From a RHEL 8 or 9 version of *PostgreSQL 15* to a RHEL version of *PostgreSQL 16*

The following procedure describes migration from the RHEL 8 version of *PostgreSQL 12* to the RHEL 9 version of *PostgreSQL 13* using the fast upgrade method.
For migration from `postgresql` streams other than `12`, use one of the following approaches:

* Update your *PostgreSQL* server to version 12 on RHEL 8 and then use the `pg_upgrade` utility to perform the fast upgrade to RHEL 9 version of *PostgreSQL 13*.

* Use the dump and restore upgrade directly between any RHEL 8 version of *PostgreSQL* and an equal or later *PostgreSQL* version in RHEL 9.

.Prerequisites

* Before performing the upgrade, back up all your data stored in the *PostgreSQL* databases. By default, all data is stored in the [filename]`/var/lib/pgsql/data/` directory on both the RHEL 8 and RHEL 9 systems.

.Procedure

. On the RHEL 9 system, install the [package]`postgresql-server` and [package]`postgresql-upgrade` packages:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install postgresql-server postgresql-upgrade*
----
+
Optionally, if you used any *PostgreSQL* server modules on RHEL 8, install them also on the RHEL 9 system in two versions, compiled both against *PostgreSQL 12* (installed as the [package]`postgresql-upgrade` package) and the target version of *PostgreSQL 13* (installed as the [package]`postgresql-server` package). If you need to compile a third-party *PostgreSQL* server module, build it both against the [package]`postgresql-devel` and [package]`postgresql-upgrade-devel` packages.

. Check the following items:
+
* Basic configuration: On the RHEL 9 system, check whether your server uses the default [filename]`/var/lib/pgsql/data` directory and the database is correctly initialized and enabled. In addition, the data files must be stored in the same path as mentioned in the [filename]`/usr/lib/systemd/system/postgresql.service` file.
* *PostgreSQL* servers: Your system can run multiple *PostgreSQL* servers. Ensure that the data directories for all these servers are handled independently.
* *PostgreSQL* server modules: Ensure that the *PostgreSQL* server modules that you used on RHEL 8 are installed on your RHEL 9 system as well. Note that plugins are installed in the [filename]`/usr/lib64/pgsql/` directory.

. Ensure that the `postgresql` service is not running on either of the source and target systems at the time of copying data.
+
[literal,subs="+quotes,attributes"]
----
# *systemctl stop postgresql.service*
----

. Copy the database files from the source location to the [filename]`/var/lib/pgsql/data/` directory on the RHEL 9 system.

. Perform the upgrade process by running the following command as the *PostgreSQL* user:
+
[literal,subs="+quotes,attributes"]
----
# *postgresql-setup --upgrade*
----
+
This launches the `pg_upgrade` process in the background.
+
In case of failure, [command]`postgresql-setup` provides an informative error message.

. Copy the prior configuration from [filename]`/var/lib/pgsql/data-old` to the new cluster.
+
Note that the fast upgrade does not reuse the prior configuration in the newer data stack and the configuration is generated from scratch. If you want to combine the old and new configurations manually, use the *.conf files in the data directories.

. Start the new *PostgreSQL* server:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl start postgresql.service*
----


. Analyze the new database cluster.
+
* For *PostgreSQL 13*:
+
[literal,subs="+quotes,attributes"]
----
*su postgres -c '~/analyze_new_cluster.sh'*
----
+
* For *PostgreSQL 15* or later:
+
[literal,subs="+quotes,attributes"]
----
*su postgres -c 'vacuumdb --all --analyze-in-stages'*
----
+
NOTE: You may need to use `ALTER COLLATION name REFRESH VERSION`, see the link:https://www.postgresql.org/docs/current/sql-altercollation.html[upstream documentation] for details.
// ^ this is a pre-GA hotfix, will be tested and written properly, possibly with examples

. If you want the new *PostgreSQL* server to be automatically started on boot, run:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl enable postgresql.service*
----
