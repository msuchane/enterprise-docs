:_mod-docs-content-type: PROCEDURE

[id="reverting-multiple-dnf-transactions-using-dnf-history-rollback_{context}"]
= Reverting multiple DNF transactions 

You can revert all *{PackageManagerName}* transactions performed between a specified transaction and the last transaction by using the [command]`{PackageManagerCommand} history rollback` command. Note that the transaction specified by the transaction ID remains unchanged. 

.Procedure

. Identify the transaction ID of the state you want to revert to:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} history*
ID | Command line     | Date and time     | Action(s)   | Altered
------------------------------------------------------------------
14 | install wget     | 2022-11-03 10:49  | Install     |    1   
13 | install unzip    | 2022-11-03 10:49  | Install     |    1  
12 | install vim-X11  | 2022-11-03 10:20  | Install     |  171 EE 
....

. Revert specified transactions:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} history rollback _<transaction_id>_*
....
+
For example, to revert to the state before the `wget` and `unzip` packages were installed, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} history rollback 12*
....
+
Alternatively, to revert all transactions in the transaction history, use the transaction ID *1*:
+
[literal,subs="+quotes,verbatim,normal"]
....
# **{PackageManagerCommand} history rollback 1**
....