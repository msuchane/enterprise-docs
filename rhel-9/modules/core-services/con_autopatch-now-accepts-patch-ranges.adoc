
:_mod-docs-content-type: CONCEPT


[id="con_autopatch-now-accepts-patch-ranges_{context}"]
= `%autopatch` now accepts patch ranges

The `%autopatch` macro now accepts the `-m` and `-M` parameters to limit the minimum and maximum patch number to apply, respectively:

* The `-m` parameter specifies the patch number (inclusive) to start at when applying patches.

* The `-M` parameter specifies the patch number (inclusive) to stop at when applying patches.

This feature can be useful when an action needs to be performed in between certain patch sets.
