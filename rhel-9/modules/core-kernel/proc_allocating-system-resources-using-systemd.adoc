:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_using-systemd-to-manage-resources-used-by-applications

[id="proc_allocating-system-resources-using-systemd_{context}"]
= Allocating system resources using systemd

.Procedure

To change the required value of the unit file option of your service, you can adjust the value in the unit file, or use the `systemctl` command:

. Check the assigned values for the service of your choice.
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl show --property <pass:quotes[_unit file option_]> <pass:quotes[_service name_]>*
....

. Set the required value of the CPU time allocation policy option:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl set-property <pass:quotes[_service name_]> <pass:quotes[_unit file option_]>=<pass:quotes[_value_]>*
....


.Verification steps

* Check the newly assigned values for the service of your choice.
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl show --property <pass:quotes[_unit file option_]> <pass:quotes[_service name_]>*
....


[role="_additional-resources"]
.Additional resources
* The `systemd.resource-control(5)` and `systemd.exec(5)` man pages
