:_newdoc-version: 2.15.0
:_template-generated: 2024-4-3
:_mod-docs-content-type: PROCEDURE

[id="onboarding-devices-with-an-fdo-database_{context}"]
= Onboarding devices with an FDO database

[role="_abstract"]
Use an SQL database to onboard your Edge device. The following example uses the `diesel` tool, but you can also use the SQLite or the PostgreSQL databases.

NOTE: You can use different database storages in some servers with filesystem storage in other servers, for example, using the filesystem storage onboarding for the Manufacturing server and Postgres for Rendezvous and Owner servers.

.Prerequisites

* You used FDO to generate the key and certificates to configure the manufacturing server. See link to [Generating key and certificates]

* You installed and configured the manufacturing server. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/composing_installing_and_managing_rhel_for_edge_images/index#proc_installing-the-manufacturing-server-package_assembly_automatically-provisioning-and-onboarding-rhel-for-edge-devices[Installing and running the manufacturing server]

* You installed and configured the rendezvous server. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/composing_installing_and_managing_rhel_for_edge_images/index#proc_installing-configuring-and-running-the-rendezvous-server_assembly_automatically-provisioning-and-onboarding-rhel-for-edge-devices[Installing, configuring, and running the Rendezvous server]

* You installed and configured the owner server. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/composing_installing_and_managing_rhel_for_edge_images/index#proc_installing-configuring-and-running-the-rendezvous-server_assembly_automatically-provisioning-and-onboarding-rhel-for-edge-devices[Installing, configuring, and running the Owner server]

* You have your server configuration files in `/etc/fdo`
* You built an OSTree commit for RHEL for Edge and used that to generate an `edge-simplified-installer` artifact
* Your device is assembled
* You installed the `diesel` tool or a SQL database to your host.
* You have configured your database system and have permissions to create tables.

.Procedure

. Install the following packages:
+
[subs="quotes,attributes"]
----
$ dnf  install -y sqlite sqlite-devel libpq libpq-devel
----

. Access the `/usr/share/doc/fdo/migrations/*` directory. It contains `.sql` files that you need to create the database for each of the server and type combinations, after you have installed the manufacturing,  rendezvous, and owner server's RPMs.

. Initialize the database content. You can use either an SQL database, such as SQLite or PostgreSQL, or the `diesel` tool to run the SQL for you.

* If you are using an SQL database, configure the database server with, for example, user creation, access management, for example. After you configure the database server, you can run the database.

* If you do not want to run the `.sql` files in your database system, you can use the `diesel` tool to run the sql for you. If you are using the `diesel` tool, after you have configured the database, use the `diesel migration run` command to create the database:

. After you configure the DB system, you can use the .sql files installed at `/usr/share/doc/fdo/migrations/*` to create the database for each server type.
+
You must use the `.sql` file that matches the server type and database type that you are initializing. For example, when initializing the Owner Onboarding Server in a PostgreSQL database,  you must use the `/usr/share/doc/fdo/migrations/migrations_owner_onboarding_server_postgres/up.sql` folder. The `up.sql` file in the migration folder creates the database, and the `down.sql` file  destroys it.

. After you create the database, change the configuration file of the specific server to make it use the database. 
+
Each server has a storage configuration section.

* Manufacturer server: `ownership_voucher_store_driver` 
* Owner server: `ownership_voucher_store_driver`
* Rendezvous server: `storage_driver` 

.. For the `manufacturing-server.yml` file, open it in your editor and change the store database:
+
[subs="quotes,attributes"]
----
$ sudo _editor_ manufacturing-server.yml
----

.. Change the `ownership_voucher_store_driver` configuration under the Directory section:
+
[subs="quotes,attributes"]
----
$ /home/rhel/fido-device-onboard-rs/aio-dir/stores/owner_vouchers
----

.. Specify the following details:

* The database type that you are using: SQLite or PostgreSQL
* The server type: for example, when using PostgreSQL, set the following configuration:
+
[subs="quotes,attributes"]
----
ownership_voucher_store_driver:
  Postgres:
    Manufacturer
----

.. Repeat the steps to configure the Owner server and Rendezvous server.

. Run the FDO onboarding service. See Automatically provisioning and onboarding RHEL for Edge devices with FDO for more details. Start the FDO onboarding process device initialization by running the manufacturing server:
+
[subs="quotes,attributes"]
----
$ sudo LOG-LEVEL=debug SQLITE_MANUFACTURER-DATABASE_URL=./manufacturer-db.sqlite ./usr/libexec/fdo/fdo-manufacturing-server
----
+
The onboarding process happens in two phases: 

* The device initialization phase that usually happens in the manufacturing site.
* The device onboarding process that happens at the final destination of the device. 
+
As a result, the Ownership Vouchers stored in the Manufacturing Server's Database must be exported and transferred to the final Owner Database.

. To export the Ownership Vouchers, from the manufacturing-vouchers database file, copy the Owner Voucher to the owner to continue the FDO onboarding protocol.

.. Create a folder `export`:
+
[subs="quotes,attributes"]
----
$ mkdir export
----

.. Export the Owner Voucher present in the Manufacturing Database by providing all the variables required for the command. 
+
[subs="quotes,attributes"]
----
$ fdo-owner-tool export-manufacturer-vouchers DB_TYPE DB_URL PATH [GUID]
----
+
DB_TYPE ::  The type of the Manufacturing DB holding the Owner Vouchers: sqlite, postgres
DB_URL:: The database connection URL, or path to the database file
PATH:: The path to the directory where the Owner Vouchers will be exported
GUID:: GUID of the owner voucher to be exported. If you do not provide a GUID, all the Owner Vouchers will be exported.

. The OVs must be delivered to the final Owner's database. For that, use the `fdo-owner-tool` to import the Owner vouchers. Change to owner database. Import the Ownership Vouchers by running the following command:
+
[subs="quotes,attributes"]
----
$ fdo-owner-tool import-ownership-vouchers DB_TYPE DB_URL SOURCE_PATH
----
+

DB_TYPE:: Type of the Owner DB to import the OVs. Possible values: sqlite, postgres
DB_URL:: DB connection URL or path to DB file
SOURCE_PATH:: Path to the OV to be imported, or path to a directory where all the OVs to be imported are located

The command reads each OV that is specified in the <SOURCE_PATH> once, one by one, and tries to import them into the database. If the command finds errors, it returns an output with the GUIDs of the OVs that are faulty and specified with information about what caused the error. The non-faulty OVs are imported into the database.
The device receives the configuration from the onboarding server. Then, the device receives an SSH key and starts to install the operating system on the device. Finally, the operating system automatically reboots in the device, and encrypts the device with a strong key stored at TPM.
