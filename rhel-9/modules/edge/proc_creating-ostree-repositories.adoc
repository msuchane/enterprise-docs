:_mod-docs-content-type: PROCEDURE

[id="creating-ostree-repositories_{context}"]
= Creating OSTree repositories

You can create OSTree repos with RHEL image builder by using either `RHEL for Edge Commit (.tar)` or `RHEL for Edge Container (.tar)` image types. These image types contain an OSTree repo that contains a single OSTree commit.

* You can extract the `RHEL for Edge Commit (.tar)` on a web server and it is ready to be served. 
* You must import the `RHEL for Edge Container (.tar)` to a local container image storage or push the image to a container registry. After you start the container, it serves the commit over an integrated `nginx` web server. 

Use the `RHEL for Edge Container (.tar)` on a RHEL server with Podman to create an OSTree repo:

.Prerequisite

* You created a  `RHEL for Edge Container (.tar)` image.

.Procedure

. Download the container image from image builder:
+
[subs="quotes,attributes"]
----
$ *composer-cli compose image _&lt;UUID&gt;*
----

. Import the container into Podman:
+
[subs="quotes,attributes"]
----
$ *skopeo copy oci-archive:_&lt;UUID&gt;_-container.tar containers-storage:localhost/ostree*
----

. Start the container and make it available by using the port `8080`:
+
[subs="quotes,attributes"]
----
$ *podman run -rm -p 8080:8080 ostree*
----

.Verification

* Check that the container is running:
+
[subs="quotes,attributes"]
----
$ *podman ps -a*
----
