:_mod-docs-content-type: REFERENCE

[id="the-rtla-osnoise-tracepoints_{context}"]
= The rtla-osnoise tracepoints

The `rtla-osnoise` includes a set of `tracepoints` to identify the source of the operating system noise (`osnoise`). 



.Trace points for `rtla-osnoise` 
osnoise:sample_threshold:: Displays a noise when the noise is more than the configured threshold (`tolerance_ns`).
osnoise:nmi_noise:: Displays noise and the noise duration from non maskable interrupts (NMIs). 
osnoise:irq_noise:: Displays noise and the noise duration from interrupt requests (IRQs). 
osnoise:softirq_noise:: Displays noise and the noise duration from soft interrupt requests (SoftIRQs), 
osnoise:thread_noise:: Displays noise and the noise duration from a thread. 



