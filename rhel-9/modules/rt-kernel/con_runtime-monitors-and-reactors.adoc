:_newdoc-version: 2.15.0
:_template-generated: 2023-11-1

:_mod-docs-content-type: CONCEPT

[id="runtime-monitors-and-reactors_{context}"]
= Runtime monitors and reactors

[role="_abstract"]

The runtime verification (RV) monitors are encapsulated inside the RV monitor abstraction and coordinate between the defined specifications and the kernel trace to capture runtime events in trace files. The RV monitor includes:

* Reference Model is a reference model of the system.
* Monitor Instance(s) is a set of instance for a monitor, such as a per-CPU monitor or a per-task monitor. 
* Helper functions that connect the monitor to the system.  

In addition to verifying and monitoring a system at runtime, you can enable a response to an unexpected system event. The forms of reaction can vary from capturing an event in the trace file to initiating an extreme reaction, such as a shut-down to avoid a system failure on safety critical systems.  

Reactors are reaction methods available for RV monitors to define reactions to system events as required. By default, monitors provide a trace output of the actions. 










