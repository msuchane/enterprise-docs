:_mod-docs-content-type: PROCEDURE

[id="proc_setting-keylime-server-using-system-roles_{context}"]
= Setting up a Keylime server by using system roles


[role="_abstract"]
You can set up the verifier and registrar, which are the Keylime server components, by using the `keylime_server` RHEL system role. The `keylime_server` role installs and configures both the verifier and registrar components together on each node.

Perform this procedure on the Ansible control node.

[NOTE]
====
For more information about Keylime, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/security_hardening/assembly_ensuring-system-integrity-with-keylime_security-hardening#con_how-keylime-works_assembly_ensuring-system-integrity-with-keylime[8.1. How Keylime works]
====

.Prerequisites

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes]
* You are logged in to the control node as a user who can run playbooks on the managed nodes.
* The account you use to connect to the managed nodes has `sudo` permissions on them.
* The managed nodes or groups of managed nodes on which you want to run this playbook are listed in the Ansible inventory file.

.Procedure

. Create a playbook that defines the required role:
.. Create a new YAML file and open it in a text editor, for example:
+
[subs="quotes"]
----
# *vi keylime-playbook.yml*
----
.. Insert the following content:
+
[subs="quotes,attributes"]
----
---
- name: Manage keylime servers
  hosts: all
  vars:
    keylime_server_verifier_ip: "{{ ansible_host }}"
    keylime_server_registrar_ip: "{{ ansible_host }}"
    keylime_server_verifier_tls_dir: _&lt;ver_tls_directory_&gt;
    keylime_server_verifier_server_cert: _&lt;ver_server_certfile_&gt;
    keylime_server_verifier_server_key: _&lt;ver_server_key_&gt;
    keylime_server_verifier_server_key_passphrase: _&lt;ver_server_key_passphrase_&gt;
    keylime_server_verifier_trusted_client_ca: _&lt;ver_trusted_client_ca_list_&gt;
    keylime_server_verifier_client_cert: _&lt;ver_client_certfile_&gt;
    keylime_server_verifier_client_key: _&lt;ver_client_key_&gt;
    keylime_server_verifier_client_key_passphrase: _&lt;ver_client_key_passphrase_&gt;
    keylime_server_verifier_trusted_server_ca: _&lt;ver_trusted_server_ca_list_&gt; 
    keylime_server_registrar_tls_dir: _&lt;reg_tls_directory_&gt;
    keylime_server_registrar_server_cert: _&lt;reg_server_certfile_&gt;
    keylime_server_registrar_server_key: _&lt;reg_server_key_&gt;
    keylime_server_registrar_server_key_passphrase: _&lt;reg_server_key_passphrase_&gt;
    keylime_server_registrar_trusted_client_ca: _&lt;reg_trusted_client_ca_list_&gt;
  roles:
    - rhel-system-roles.keylime_server

----
+
You can find out more about the variables in 
ifdef::security-hardening[]
xref:ref_variables-for-keylime-server-system-role_assembly_ensuring-system-integrity-with-keylime[Variables for the keylime_server RHEL system role].
endif::[]
ifndef::security-hardening[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security-hardening/ref_variables-for-keylime-server-system-role[Variables for the keylime_server RHEL system role].
endif::[]


. Run the playbook:
+
[subs="quotes,attributes"]
----
$ *ansible-playbook  _&lt;keylime-playbook.yml&gt;_*
----

.Verification

. Check that the `keylime_verifier` service is active and running on the managed host:
+
[subs="quotes"]
----
# *systemctl status keylime_verifier*
● keylime_verifier.service - The Keylime verifier
     Loaded: loaded (/usr/lib/systemd/system/keylime_verifier.service; disabled; vendor preset: disabled)
     Active: active (running) since Wed 2022-11-09 10:10:08 EST; 1min 45s ago
----

. Check that the `keylime_registrar` service is active and running:
+
[subs="quotes"]
----
# *systemctl status keylime_registrar*
● keylime_registrar.service - The Keylime registrar service
     Loaded: loaded (/usr/lib/systemd/system/keylime_registrar.service; disabled; vendor preset: disabled)
     Active: active (running) since Wed 2022-11-09 10:10:17 EST; 1min 42s ago
...
----

.Next steps

ifdef::security-hardening[]
xref:configuring-keylime-tenant_assembly_ensuring-system-integrity-with-keylime[]
endif::[]
ifndef::security-hardening[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/assembly_ensuring-system-integrity-with-keylime_security-hardening#configuring-keylime-tenant_assembly_ensuring-system-integrity-with-keylime[Install the Keylime tenant]
endif::[]