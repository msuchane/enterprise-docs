:_mod-docs-content-type: PROCEDURE

[id="proc_re-enabling-sha-1_{context}"]
= Re-enabling SHA-1

The use of the SHA-1 algorithm for creating and verifying signatures is restricted in the `DEFAULT` cryptographic policy. If your scenario requires the use of SHA-1 for verifying existing or third-party cryptographic signatures, you can enable it by applying the `SHA1` subpolicy, which RHEL 9 provides by default. Note that it weakens the security of the system.

.Prerequisites

* The system uses the `DEFAULT` system-wide cryptographic policy.

.Procedure

. Apply the `SHA1` subpolicy to the `DEFAULT` cryptographic policy:
+
[subs="quotes"]
----
# *update-crypto-policies --set DEFAULT:SHA1*
Setting system policy to DEFAULT:SHA1
Note: System-wide crypto policies are applied on application start-up.
It is recommended to restart the system for the change of policies
to fully take place.
----


. Restart the system:
+
[subs="quotes"]
----
# *reboot*
----

.Verification

* Display the current cryptographic policy:
+
[subs="quotes"]
----
# *update-crypto-policies --show*
DEFAULT:SHA1
----

[IMPORTANT]
====
Switching to the `LEGACY` cryptographic policy by using the `update-crypto-policies --set LEGACY` command also enables SHA-1 for signatures. However, the `LEGACY` cryptographic policy makes your system much more vulnerable by also enabling other weak cryptographic algorithms. Use this workaround only for scenarios that require the enablement of other legacy cryptographic algorithms than SHA-1 signatures.
====


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/6816771[SSH from RHEL 9 to RHEL 6 systems does not work] KCS article
* link:https://access.redhat.com/solutions/6868611[Packages signed with SHA-1 cannot be installed or upgraded] KCS article
