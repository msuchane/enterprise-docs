:_mod-docs-content-type: PROCEDURE

[id="configuring-keylime-verifier_{context}"]
= Configuring Keylime verifier

The verifier is the most important component in Keylime. It performs initial and periodic checks of system integrity and supports bootstrapping a cryptographic key securely with the agent. The verifier uses mutual TLS encryption for its control interface.

[IMPORTANT]
====
To maintain the chain of trust, keep the system that runs the verifier secure and under your control.
====

You can install the verifier on a separate system or on the same system as the Keylime registrar, depending on your requirements. Running the verifier and registrar on separate systems provides better performance.

[NOTE]
====
To keep the configuration files organized within the drop-in directories, use file names with a two-digit number prefix, for example `/etc/keylime/verifier.conf.d/00-verifier-ip.conf`. The configuration processing reads the files inside the drop-in directory in lexicographic order and sets each option to the last value it reads.
====

.Prerequisites

* You have `root` permissions and network connection to the system or systems on which you want to install Keylime components.
* You have valid keys and certificates from your certificate authority.
* Optional: You have access to the databases where Keylime saves data from the verifier. You can use any of the following database management systems:
+
** SQLite (default)
** PostgreSQL
** MySQL
** MariaDB

.Procedure

. Install the Keylime verifier:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install keylime-verifier*
----

. Define the IP address and port of verifier by creating a new `.conf` file in the  `/etc/keylime/verifier.conf.d/` directory, for example, `/etc/keylime/verifier.conf.d/00-verifier-ip.conf`, with the following content:
+
[subs="quotes"]
----
*[verifier]*
*ip = __&lt;verifier_IP_address&gt;__*
----
+
* Replace `__&lt;verifier_IP_address&gt;__` with the verifier's IP address. Alternatively, use `ip = *` or `ip = 0.0.0.0` to bind the verifier to all available IP addresses.
* Optionally, you can also change the verifier's port from the default value `8881` by using the `port` option.

. Optional: Configure the verifier's database for the list of agents. The default configuration uses an SQLite database in the verifier’s `/var/lib/keylime/cv_data.sqlite/` directory. You can define a different database by creating a new `.conf` file in the `/etc/keylime/verifier.conf.d/` directory, for example, `/etc/keylime/verifier.conf.d/00-db-url.conf`, with the following content:
+
[subs="quotes"]
----
[verifier]
database_url = __&lt;protocol&gt;://&lt;name&gt;:&lt;password&gt;@&lt;ip_address_or_hostname&gt;/&lt;properties&gt;__
----
+
Replace `__&lt;protocol&gt;://&lt;name&gt;:&lt;password&gt;@&lt;ip_address_or_hostname&gt;/&lt;properties&gt;__` with the URL of the database, for example, `postgresql://verifier:UQ?nRNY9g7GZzN7@198.51.100.1/verifierdb`.
+
Ensure that the credentials you use provide the permissions for Keylime to create the database structure.

. Add certificates and keys to the verifier. You can either let Keylime generate them, or use existing keys and certificates:

* With the default `tls_dir = generate` option, Keylime generates new certificates for the verifier, registrar, and tenant in the `/var/lib/keylime/cv_ca/` directory.

* To load existing keys and certificates in the configuration, define their location in the verifier configuration.
+
[NOTE]
====
Certificates must be accessible by the `keylime` user, under which the Keylime services are running.
====
+
Create a new `.conf` file in the `/etc/keylime/verifier.conf.d/` directory, for example, `/etc/keylime/verifier.conf.d/00-keys-and-certs.conf`, with the following content:
+
[subs="quotes"]
----
[verifier]
tls_dir = /var/lib/keylime/cv_ca
server_key = _&lt;/path/to/server_key&gt;_
server_key_password = _&lt;passphrase1&gt;_
server_cert = _&lt;/path/to/server_cert&gt;_
trusted_client_ca = ['_&lt;/path/to/ca/cert1&gt;_', '_&lt;/path/to/ca/cert2&gt;_']
client_key = _&lt;/path/to/client_key&gt;_
client_key_password = _&lt;passphrase2&gt;_
client_cert = _&lt;/path/to/client_cert&gt;_
trusted_server_ca = ['_&lt;/path/to/ca/cert3&gt;_', '_&lt;/path/to/ca/cert4&gt;_']
----
+
[NOTE]
====
Use absolute paths to define key and certificate locations. Alternatively, relative paths are resolved from the directory defined in the `tls_dir` option.
====

. Open the port in firewall:
+
[subs="quotes"]
----
# *firewall-cmd --add-port 8881/tcp*
# *firewall-cmd --runtime-to-permanent*
----
+
If you use a different port, replace `8881` with the port number defined in the `.conf` file.

. Start the verifier service:
+
[subs="quotes"]
----
# *systemctl enable --now keylime_verifier*
----
+
[NOTE]
====
In the default configuration, start the `keylime_verifier` before starting the `keylime_registrar` service because the verifier creates the CA and certificates for the other Keylime components. This order is not necessary when you use custom certificates.
====

.Verification

* Check that the `keylime_verifier` service is active and running:
+
[subs="quotes"]
----
# *systemctl status keylime_verifier*
● keylime_verifier.service - The Keylime verifier
     Loaded: loaded (/usr/lib/systemd/system/keylime_verifier.service; disabled; vendor preset: disabled)
     Active: active (running) since Wed 2022-11-09 10:10:08 EST; 1min 45s ago
----

.Next steps

* {blank}
ifdef::security-hardening[]
xref:configuring-keylime-registrar_assembly_ensuring-system-integrity-with-keylime[].
endif::[]
ifndef::security-hardening[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/assembly_ensuring-system-integrity-with-keylime_security-hardening#configuring-keylime-registrar_assembly_ensuring-system-integrity-with-keylime[Install the Keylime registrar].
endif::[]
