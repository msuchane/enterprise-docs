:_mod-docs-content-type: PROCEDURE

[id="logging-into-to-gnome-with-enterprise-credentials_{context}"]
= Logging in to GNOME with enterprise credentials

If your network has an Active Directory or IPA domain available, and you have a domain account, you can log in to GNOME using your enterprise credentials. 

.Procedure

* At the GNOME login prompt, type your domain username followed by an `@` sign and then your domain name.
+
----
username@domain.com
----

