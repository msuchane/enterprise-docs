:_mod-docs-content-type: PROCEDURE

[id="launching-an-application-remotely-using-waypipe_{context}"]
= Launching an application remotely using waypipe

You can accesses a graphical application on Wayland on a RHEL server from a remote client using SSH and `waypipe`.

[NOTE]
====
This procedure does not work for legacy X11 applications. For X11 applications,
see xref:remotely-accessing-an-individual-application-X11_getting-started-with-the-gnome-desktop-environment[Remotely accessing an individual application on X11].
====

.Prerequisites

* A Wayland display server is running on your system. On RHEL 9, GNOME as a Wayland compositor is the default.
* The `waypipe` package is installed on both the client and the remote system.
* The application is capable of running natively on Wayland.


.Procedure

. Launch the application remotely through `waypipe` and SSH.
+
[subs="+quotes,macros"]
....
[local-user]$ *waypipe -c lz4=9 ssh _remote-server_ _application-binary_*

The authenticity of host '__remote-server__ (__192.168.122.120__)' can't be established.
ECDSA key fingerprint is SHA256:__uYwFlgtP/2YABMHKv5BtN7nHK9SHRL4hdYxAPJVK/kY__.
Are you sure you want to continue connecting (yes/no/[fingerprint])?
....

. Confirm that a server key is valid by checking its fingerprint.

. Continue connecting by typing `yes`.
+
[subs="+quotes"]
----
Warning: Permanently added '__remote-server__' (ECDSA) to the list of known hosts.
----

. When prompted, type the server password.
+
----
remote-user's password:
[remote-user]$
----

