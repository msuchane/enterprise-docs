:_mod-docs-content-type: CONCEPT

[id="con_single-application-mode_{context}"]
= Single-application mode

[role="_abstract"]
Single-application mode is a modified GNOME session that reconfigures the Mutter window manager into an interactive kiosk. This session locks down certain behavior to make the standard desktop more restrictive. The user can interact only with a single application selected by the administrator.

You can set up single-application mode for several use cases, such as:

* In the communication, entertainment, or education fields
* As a self-serve machine
* As an event manager
* As a registration point

The GNOME Kiosk utility provides the single-application mode configuration and sessions.

The following single-application sessions are available:

Search Appliance Session::
This session always starts the *Mozilla Firefox* web browser at the `www.google.com` website.

Kiosk Script Session::
This session starts an arbitrary application that you specify in a shell script.

