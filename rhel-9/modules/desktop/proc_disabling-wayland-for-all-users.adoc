:_mod-docs-content-type: PROCEDURE

[id="proc_disabling-wayland-for-all-users_{context}"]
= Disabling Wayland for all users

You can disable the Wayland session for all users on the system, so that they always log in with the X11 session.

.Procedure

. Open the `/etc/gdm/custom.conf` file as the `root` user.

. Locate the following line in the `[daemon]` section of the file:
+
----
#WaylandEnable=false
----

. Uncomment the line by remove the `#` character. As a result, the line says:
+
----
WaylandEnable=false
----

. Reboot the system.

////
[role="_additional-resources"]
.Additional resources
* A bulleted list of links to other closely-related material. These links can include `link:` and `xref:` macros.
* For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
* Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
////
