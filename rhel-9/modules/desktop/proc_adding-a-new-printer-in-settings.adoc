:_mod-docs-content-type: PROCEDURE

:experimental:

[id="adding-a-printer-in-settings_{context}"]
= Adding a new printer in Settings

You can add a new printer using the [application]*Settings* application.

.Prerequisites

* Click the btn:[Unlock] button, which appears near the upper-right corner of the *Printers* screen, and authenticate as one of the following users:

** Superuser
** Any user with the administrative access provided by [command]`sudo` (users listed within [filename]`/etc/sudoers`)
** Any user belonging to the `printadmin` group in [filename]`/etc/group`

.Procedure

. Open the *Printers* dialog.
+
image::gnome-control-center-add-printer-9.png[]

. Click on *Unlock* and authenticate.
+
image::add-printer-gcc-unlock-authenticate-9.png[]

. Select one of the available printers (including also network printers), or enter printer IP address or the host name of a printer server.
+
image::gnome-control-center-select-printer-9.png[]
+
image::gnome-control-center-add-network-printer-9.png[]

. Confirm your selection by clicking btn:[Add] in the top right corner.
+
image::gnome-control-center-add-network-printer-confirm-9.png[]
