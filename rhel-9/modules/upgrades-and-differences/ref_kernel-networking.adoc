:_module-type: REFERENCE

[id="ref_kernel-networking_{context}"]
= Kernel

.WireGuard VPN is available as a Technology Preview

WireGuard, which Red Hat provides as an unsupported Technology Preview, is a high-performance VPN solution that runs in the Linux kernel. It uses modern cryptography and is easier to configure than other VPN solutions. Additionally, the small code-basis of WireGuard reduces the surface for attacks and, therefore, improves the security.

For further details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_networking/assembly_setting-up-a-wireguard-vpn_configuring-and-managing-networking[Setting up a WireGuard VPN].
//BZ#1613522


.The PRP and HSR protocols are available as a Technology Preview

Starting with RHEL 9.3, the `hsr` kernel module is available as an unsupported Technology Preview. The module provides the following protocols:

* Parallel Redundancy Protocol (PRP)
* High-availability Seamless Redundancy (HSR)

The IEC 62439-3 standard defines these protocols, and you can use this feature to configure zero-loss redundancy in Ethernet networks.
//BZ#2177256


.Segment Routing over IPv6 (SRv6) is available as a Technology Preview

The RHEL 9.3 kernel provides Segment Routing over IPv6 (SRv6) as an unsupported Technology Preview. You can use this functionality to optimize traffic flows in edge computing or to improve network programmability in data centers. However, the most significant use case is the end-to-end (E2E) network slicing in 5G deployment scenarios. In that area, the SRv6 protocol provides you with the programmable custom network slices and resource reservations to address network requirements for specific applications or services. At the same time, the solution can be deployed on a single-purpose appliance, and it satisfies the need for a smaller computational footprint.
//BZ#2186375


.NetworkManager and the Nmstate API support MACsec hardware offload

You can use both NetworkManager and the Nmstate API to enable MACsec hardware offload if the hardware supports this feature. As a result, you can offload MACsec operations, such as encryption, from the CPU to the network interface card.

Note that this feature is an unsupported Technology Preview.
//RHEL-24337