
:_mod-docs-content-type: REFERENCE

[id="ref_changes-to-qemu_{context}"]
= Notable changes to QEMU

.QEMU no longer includes the SGA option ROM

In RHEL 9, the Serial Graphics Adapter (SGA) option ROM has been replaced by an equivalent functionality in SeaBIOS. However, if your virtual machine (VM) configuration uses the following XML fragament, this change will not affect your VM functionality.

----
  <bios useserial='yes'/>
----

// (BZs#1995807 & 2002993)

.TPM passthrough has been removed

It is no longer possible to assign a physical Trusted Platform Module (TPM) device using the passthrough back end to a VM on RHEL 9. Note that this was an unsupported feature in RHEL 8. Instead, use the vTPM functionality, which uses the emulator back end, and is fully supported.

.Other unsupported devices

QEMU no longer supports the following virtual devices:

* The Cirrus graphics device. The default graphics devices are now set to `stdvga` on BIOS-based machines and `bochs-display` on UEFI-based machines.
* The `ac97` audio device. In RHEL 9, `libvirt` uses the `ich9` device instead.

.Intel vGPU removed

The packages required for the Intel vGPU feature were removed in RHEL 9.3.

Previously, as a Technology Preview, it was possible to divide a physical Intel GPU device into multiple virtual devices referred to as `mediated devices`. These mediated devices could then be assigned to multiple virtual machines (VMs) as virtual GPUs.

Since RHEL 9.3, you cannot use this feature.
