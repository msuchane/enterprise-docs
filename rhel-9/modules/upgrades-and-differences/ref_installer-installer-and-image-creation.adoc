
:_module-type: REFERENCE

[id="ref_installer-installer-and-image-creation_{context}"]
= Installer

.Anaconda activates network automatically for interactive installations
Anaconda now activates the network automatically when performing interactive installation, without requiring users to manually activate it in the network spoke. This update does not change the installation experience for Kickstart installations and installations using the `ip=` boot option.

.New options to `Lock root account` and `Allow root SSH login with password`

RHEL 9 adds following new options to the root password configuration screen:

* `Lock root account`: To lock the root access to the machine.

* `Allow root SSH login with password`: To enable password-based SSH root logins.

During Kickstart installations, you can enable root access via SSH with password by using the `--allow-ssh` option of the `rootpw` Kickstart command. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/performing_an_advanced_rhel_9_installation/kickstart-commands-and-options-reference_installing-rhel-as-an-experienced-user#rootpw-required_kickstart-commands-for-system-configuration[rootpw (required)].

.Licensing, system, and user setting configuration screens have been disabled post standard installation

Previously, RHEL users configured Licensing, System (Subscription manager), and User Settings prior to `gnome-initial-setup` and `login` screens. Starting with RHEL 9, the initial setup screens have been disabled by default to improve user experience.
If you must run the initial setup for user creation or license display, install the following packages based on the requirements.

. To install initial setup packages:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install initial-setup initial-setup-gui*
----

. To enable initial setup after the next reboot of the system.
+
[subs="quotes,attributes"]
----
# *systemctl enable initial-setup*
----

. Reboot the system to view initial setup.

For Kickstart installations, add `initial-setup-gui` to the packages section and enable the `initial-setup` service.

[options="nowrap",role=white-space-pre]
----
firstboot --enable
%packages
@^graphical-server-environment
initial-setup-gui
%end
----

.The `rhsm` command for machine provisioning through Kickstart for Satellite is now available

The `rhsm` command replaces the `%post` scripts for machine provisioning on RHEL 9. The `rhsm` command helps with all provisioning tasks such as registering the system, attaching RHEL subscriptions, and installing from a Satellite instance. For more information, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_an_advanced_rhel_{ProductNumber}_installation/assembly_registering-and-installing-rhel-from-satellite-via-kickstart_installing-rhel-as-an-experienced-user[Registering and installing RHEL from Satellite using Kickstart] section in the Performing an advanced RHEL installation guide.

.New Kickstart command - `timesource`

The new `timesource` Kickstart command is optional and it helps to set NTP, NTS servers, and NTP pools that provide time data. It also helps to control enabling or disabling the NTP services on the system. The `--ntpservers` option from the timezone command has been deprecated and has been replaced with this new command.

.Support for Anaconda boot arguments without inst. prefix is no longer available

Anaconda boot arguments without the `inst.` prefix have been deprecated since RHEL 7. Support for these boot arguments has been removed in RHEL 9. To continue using these options, use the `inst.` prefix

For example, to force the installation program to run in the `text` mode instead of the `graphical` mode, use the following option:
[options="nowrap",role=white-space-pre]
----
inst.text
----

.Removed Kickstart commands and options

The following Kickstart commands and options have been removed from RHEL 9. Using them in Kickstart files causes an error.

* `device`
* `deviceprobe`
* `dmraid`
* `install` - use the subcommands or methods directly as commands
* `multipath`
* `bootloader` `--upgrade`
* `ignoredisk` `--interactive`
* `partition` `--active`
* `harddrive` `--biospart`
* `autostep`

Where only specific options and values are listed, the base command and its other options are still available and not removed.

.Removed boot options

The following boot options have been removed from Red Hat Enterprise Linux:

* `inst.zram`
+
RHEL 9 does not support the `zram` service. See the `zram-generator(8)` man page for more information.

* `inst.singlelang`
+
The single language mode is not supported on RHEL 9.

* `inst.loglevel`
+
The log level is always set to debug.
