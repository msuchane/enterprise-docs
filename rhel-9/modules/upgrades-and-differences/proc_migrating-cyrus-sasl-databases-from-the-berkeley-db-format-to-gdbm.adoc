:_mod-docs-content-type: PROCEDURE

[id="proc_migrating-cyrus-sasl-databases-from-the-berkeley-db-format-to-gdbm_{context}"]
= Migrating Cyrus SASL databases from the Berkeley DB format to GDBM

The RHEL 9 `cyrus-sasl` package is built without the `libdb` dependency, and the `sasldb` plugin uses the GDBM database format instead of Berkeley DB.

.Prerequisites

* The `cyrus-sasl-lib` package is installed on your system.

.Procedure

* To migrate your existing Simple Authentication and Security Layer (SASL) databases stored in the old Berkeley DB format, use the `cyrusbdb2current` tool with the following syntax:
+
[subs="quotes"]
----
# *cyrusbdb2current* _&lt;sasldb_path&gt;_ _&lt;new_path&gt;_
----


[role="_additional-resources"]
.Additional resources
* `cyrusbdb2current(1)` man page
