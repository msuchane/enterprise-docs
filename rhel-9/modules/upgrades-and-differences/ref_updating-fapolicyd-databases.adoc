:_mod-docs-content-type: REFERENCE

[id="ref_updating-fapolicyd-databases_{context}"]
= Updating fapolicyd databases

The `fapolicyd` software framework controls the execution of applications based on a user-defined policy.

In rare cases, a problem with the `fapolicyd` trust database format can occur. To rebuild the database:

. Stop the service:
+
[subs="quotes"]
----
# *systemctl stop fapolicyd*
----

. Delete the database:
+
[subs="quotes"]
----
# *fapolicyd-cli --delete-db*
----

. Start the service:
+
[subs="quotes"]
----
# *systemctl start fapolicyd*
----

If you added custom trust files to the trust database, update them either individually by using the `fapolicyd-cli -f update _&lt;FILE&gt;_` command or altogether by using `fapolicyd-cli -f update`. To apply the changes, use either the `fapolicyd-cli --update` command or restart the `fapolicyd` service.

Additionally, custom binaries might require a rebuild for the new RHEL version. Perform any such updates before you update the fapolicyd database.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/security_hardening/assembly_blocking-and-allowing-applications-using-fapolicyd_security-hardening[Blocking and allowing applications using fapolicyd]
