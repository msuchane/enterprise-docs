:_mod-docs-content-type: REFERENCE
:experimental:
// included in:
// assembly_applying-security-policies.adoc

[id="system-wide-cryptographic-policies_{context}"]
= System-wide cryptographic policies

[role="_abstract"]
The system-wide cryptographic policies is a system component that configures the core cryptographic subsystems, covering the TLS, IPSec, SSH, DNSSec, and Kerberos protocols.

The in-place upgrade process preserves the cryptographic policy you used in RHEL 8. For example, if you used the `DEFAULT` cryptographic policy in RHEL 8, your system upgraded to RHEL 9 also uses `DEFAULT`. Note that specific settings in predefined policies differ, and RHEL 9 cryptographic policies contain more strict and more secure default values. For example, the RHEL 9 `DEFAULT` cryptographic policy restricts SHA-1 usage for signatures and the `LEGACY` policy no longer allows DH and RSA ciphers with less than 2048 bits. See the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[Using system-wide cryptographic policies] section in the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/index[Security hardening] document for more information. Custom cryptographic policies are preserved across the in-place upgrade.

To view or change the current system-wide cryptographic policy, use the update-crypto-policies tool:

[subs="quotes,attributes"]
----
$ *update-crypto-policies --show*
DEFAULT
----

For example, the following command switches the system-wide crypto policy level to `FUTURE`, which should withstand any near-term future attacks:
[subs="quotes"]
----
# *update-crypto-policies --set FUTURE*
Setting system policy to FUTURE
----

If your scenario requires the use of SHA-1 for verifying existing or third-party cryptographic signatures, you can enable it by entering the following command:
[subs="quotes"]
----
# *update-crypto-policies --set DEFAULT:SHA1*
----
Alternatively, you can switch the system-wide crypto policies to the `LEGACY` policy. However, `LEGACY` also enables many other algorithms that are not secure.

[WARNING]
====
Enabling the `SHA` subpolicy makes your system more vulnerable than the default RHEL 9 settings. Switching to the `LEGACY` policy is even less secure, and you should use it with caution.
====

You can also customize system-wide cryptographic policies. For details, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening#customizing-system-wide-cryptographic-policies-with-subpolicies_using-the-system-wide-cryptographic-policies[Customizing system-wide cryptographic policies with subpolicies] and link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening#creating-and-setting-a-custom-system-wide-cryptographic-policy_using-the-system-wide-cryptographic-policies[Creating and setting a custom system-wide cryptographic policy] sections. If you use a custom cryptographic policy, consider reviewing and updating the policy to mitigate threats brought by advances in cryptography and computer hardware.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[Using system-wide cryptographic policies]
* `update-crypto-policies(8)` man page
