:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// enterprise/assemblies/assembly_upgrading-from-rhel-8-to-rhel-9.adoc


// This module can be included from assemblies using the following include statement:
// include::<path>/proc_verifying-the-post-upgrade-state-of-the-rhel-9-system.adoc[leveloffset=+1]

[id="verifying-the-post-upgrade-state-of-the-rhel-9-system_{context}"]
= Verifying the post-upgrade state of the RHEL 9 system

[role="_abstract"]
This procedure lists verification steps recommended to perform after an in-place upgrade to RHEL 9.

.Prerequisites

* The system has been upgraded following the steps described in
ifdef::upgrading-to-rhel-9[]
xref:performing-the-upgrade_upgrading-from-rhel-8-to-rhel-9[Performing the upgrade]
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#performing-the-upgrade_upgrading-from-rhel-8-to-rhel-9[Performing the upgrade]
endif::[]
and you have been able to log in to RHEL 9.

.Procedure

After the upgrade completes, determine whether the system is in the required state, at least:

* Verify that the current OS version is RHEL 9. For example:
+
[subs="quotes,attributes"]
----
# *cat /etc/redhat-release*
Red Hat Enterprise Linux release 9.0 (Plow)
----
+

* Check the OS kernel version. For example:
+
[subs="quotes,attributes"]
----
# *uname -r*
5.14.0-70.10.1.el9_0.x86_64
----
+
Note that `.el9` is important and the version should not be earlier than 5.14.0.
// TODO: update kernel version when changes

* If you are using the Red Hat Subscription Manager:
+
** Verify that the correct product is installed. For example:
+
[subs="quotes,attributes"]
----
# *subscription-manager list --installed*
+-----------------------------------------+
    	  Installed Product Status
+-----------------------------------------+
Product Name: Red Hat Enterprise Linux for x86_64
Product ID:   479
Version:      9.0
Arch:         x86_64
Status:       Subscribed
----
+

** Verify that the release version is set to the expected target OS version immediately after the upgrade. For example:
+
[subs="quotes,attributes"]
----
# *subscription-manager release*
Release: 9.0
----

* Verify that network services are operational, for example, try to connect to a server using SSH.

* Check the post-upgrade status of your applications. In some cases, you may need to perform migration and configuration changes manually. For example, to migrate your databases, follow instructions in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_using_database_servers/index[Configuring and using database servers].
