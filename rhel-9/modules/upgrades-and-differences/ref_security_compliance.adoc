:_module-type: REFERENCE

[id="ref_considerations-security-compliance_{context}"]
= Security compliance

.CIS and DISA STIG profiles provided as DRAFT

The profiles based on benchmarks from the Center for Internet Security (CIS) and Defence Industry Security Association Security Technical Implementation Guides (DISA STIG) are provided as DRAFT because the issuing authorities have not yet published an official benchmark for RHEL 9. In addition, the OSSP profile is in DRAFT because it is being implemented.

For a complete list of profiles available in {ProductShortName} 9, see
ifdef::security-hardening[]
xref:scap-security-guide-profiles-supported-in-rhel-9_scanning-the-system-for-configuration-compliance-and-vulnerabilities[].
endif::[]
ifndef::security-hardening[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/scanning-the-system-for-configuration-compliance-and-vulnerabilities_security-hardening#scap-security-guide-profiles-supported-in-rhel-9_scanning-the-system-for-configuration-compliance-and-vulnerabilities[SCAP Security Guide profiles supported in RHEL 9].
endif::[]

.OpenSCAP no longer supports SHA-1 and MD5

Due to removal of SHA-1 and MD5 hash functions in Red Hat Enterprise Linux 9, support for OVAL `filehash_test` has been removed from OpenSCAP. Also, support for SHA-1 and MD5 hash functions has been removed from OVAL `filehash58_test` implementation in OpenSCAP. As a result, OpenSCAP evaluates rules in SCAP content that use the OVAL `filehash_test` as `notchecked`. In addition, OpenSCAP returns `notchecked` also when evaluating OVAL `filehash58_test` with the `hash_type` element within `filehash58_object` set to `SHA-1` or `MD5`.

To update your OVAL content, rewrite the affected SCAP content so that it uses `filehash58_test` instead of `filehash_test` and use one of `SHA-224`, `SHA-256`, `SHA-384`, `SHA-512` in the `hash_type` element within `filehash58_object`.

.OpenSCAP uses the data stream file instead of the XCCDF file

The SCAP source data stream file (`ssg-rhel9-ds.xml`) contains all the data that in previous versions of RHEL were contained in the XCCDF file (`ssg-rhel9-xccdf.xml`). The SCAP source data stream is a container file that includes all the components (XCCDF, OVAL, CPE) needed to perform a compliance scan. Using the SCAP source data stream instead of XCCDF has been recommended since RHEL 7. In previous versions of RHEL, the data in the XCCDF file and SCAP source data stream was duplicated. In RHEL 9, this duplication is removed to reduce the RPM package size. If your scenario requires using separate files instead of the data stream, you can split the data stream file by using this command: `# oscap ds sds-split /usr/share/xml/scap/ssg/content/ssg-rhel9-ds.xml output_directory`.
