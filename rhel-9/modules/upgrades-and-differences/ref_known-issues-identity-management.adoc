:_mod-docs-content-type: REFERENCE

[id="known-issues-identity-management_{context}"]
= Known issues


.Users without SIDs cannot log in to IdM after an upgrade

After upgrading your Identity Management (IdM) replica to RHEL 9.2, the IdM Kerberos Distribution Centre (KDC) might fail to issue ticket-granting tickets (TGTs) to users who do not have Security Identifiers (SIDs) assigned to their accounts. Consequently, the users cannot log in to their accounts.

To work around the problem, generate SIDs by running the following command as an IdM administrator on another IdM replica in the topology:
----
# ipa config-mod --enable-sid --add-sids
----
Afterward, if users still cannot log in, examine the Directory Server error log. You might have to adjust ID ranges to include user POSIX identities.


.Adding a RHEL 9 replica in FIPS mode to an IdM deployment in FIPS mode that was initialized with RHEL 8.6 or earlier fails

The default RHEL 9 FIPS cryptographic policy aiming to comply with FIPS 140-3 does not allow the use of the AES HMAC-SHA1 encryption types' key derivation function as defined by RFC3961, section 5.1.

This constraint does not allow you to add a RHEL 9 IdM replica in FIPS mode to a RHEL 8 IdM environment in FIPS mode in which the first server was installed on a RHEL 8.6 or earlier systems. This is because there are no common encryption types between RHEL 9 and the previous RHEL versions, which commonly use the AES HMAC-SHA1 encryption types but do not use the AES HMAC-SHA2 encryption types.

To work around the problem, enable the use of AES HMAC-SHA1 on the RHEL 9 replica:
[subs="quotes"]
----
# update-crypto-policies --set FIPS:AD-SUPPORT
----

By setting the cryptographic policy to `FIPS:AD-SUPPORT`, you are adding the following encryption types to the list of already allowed encryption types that comply with FIPS 140-3:

* aes256-cts:normal
* aes256-cts:special
* aes128-cts:normal
* aes128-cts:special

As a result, adding the RHEL 9 replica to the IdM deployment proceeds correctly.

[NOTE]
====
There is ongoing work to provide a procedure to generate missing AES HMAC-SHA2-encrypted Kerberos keys on RHEL 7 and RHEL 8 servers. This will achieve FIPS 140-3 compliance on the RHEL 9 replica. However, this process cannot be fully automated, because the design of Kerberos key cryptography makes it impossible to convert existing keys to different encryption types. The only way is to ask users to renew their passwords.
====

[NOTE]
====
You can view the encryption type of your IdM master key by entering the following command on the first IdM server in the RHEL 8 deployment:
[subs="quotes"]
----
# kadmin.local getprinc K/M | grep -E '^Key:'
----
If the string in the output contains the `sha1` term, you must enable the use of AES HMAC-SHA1 on the RHEL 9 replica.
====

[WARNING]
====
Microsoft's Active Directory implementation does not yet support any of the RFC8009 Kerberos encryption types that use SHA-2 HMAC. If you have an IdM-AD trust configured, FIPS:AD-SUPPORT crypto subpolicy use is therefore required even if the encryption type of your IdM master key is `aes256-cts-hmac-sha384-192`.
====
