:_module-type: PROCEDURE

[id="proc_running-a-program_{context}"]
= Running a program 

The Clang compiler creates an executable binary file as a result of compiling.
Complete the following steps to execute this file and run your program.

.Prerequisites
* Your program is compiled. +
For more information on how to compile your program, see xref:proc_compiling-a-source-file_{context}[Compiling a source file].

.Procedure
[role="_abstract"]

To run your program, run in the directory containing the executable file:

[subs="quotes,attributes"]
----
$ ./&lt;__binary_file__&gt;
----

* Replace `&lt;__binary_file__&gt;` with the name of your executable file.

//.Verification

//[role="_additional-resources"]
//.Additional resources


