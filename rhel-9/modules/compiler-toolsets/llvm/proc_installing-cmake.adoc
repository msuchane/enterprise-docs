:_module-type: PROCEDURE

[id="proc_installing-cmake_{context}"]
= Installing the CMake build manager

[role="_abstract"]
The CMake build manager is a tool that manages the build process of your source  code independently from your compiler.
CMake can generate a native build environment to compile source code, create libraries, generate wrappers, and build executable files. +
Complete the following steps to install the CMake build manager.

.Prerequisites
* {comp} Toolset is installed. +
For more information, see xref:proc_installing-comp-toolset_{context}[Installing {comp} Toolset].

.Procedure
To install CMake, run the following command:

////
ifdef::publish-rhel-7[:rhel-7: ]
ifdef::publish-rhel-8[:rhel-8: ]
ifdef::publish-rhel-9[:rhel-9: ]
ifdef::publish-rhel-7+publish-rhel-8[:rhel-7: On {RHEL}{nbsp}7:] 
ifdef::publish-rhel-7+publish-rhel-8[:rhel-8: On {RHEL}{nbsp}8:]
ifdef::publish-rhel-8+publish-rhel-9[:rhel-8: On {RHEL}{nbsp}8:] 
ifdef::publish-rhel-8+publish-rhel-9[:rhel-9: On {RHEL}{nbsp}9:] 
ifdef::publish-rhel-7+publish-rhel-9[:rhel-7: On {RHEL}{nbsp}7:]
ifdef::publish-rhel-7+publish-rhel-9[:rhel-9: On {RHEL}{nbsp}9:] 
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:rhel-7: On {RHEL}{nbsp}7:] 
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:rhel-8: On {RHEL}{nbsp}8:]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:rhel-9: On {RHEL}{nbsp}9:]
////

ifdef::publish-rhel-7[]
//{rhel-7}
* On {RHEL}{nbsp}7:
[subs="quotes,attributes"]
----
# yum install llvm-toolset-15.0-cmake 
----
endif::publish-rhel-7[]

ifdef::publish-rhel-8[]
//{rhel-8}
* On {RHEL}{nbsp}8:
[subs="quotes,attributes"]
----
# yum install cmake 
----
endif::publish-rhel-8[]

ifdef::publish-rhel-9[]
//{rhel-9}
* On {RHEL}{nbsp}9:
[subs="quotes,attributes"]
----
# dnf install cmake 
----
endif::publish-rhel-9[]


.Additional resources

* For more information on the CMake build manager, see the official CMake documentation overview link:https://cmake.org/overview/[About CMake]. 

* For an introduction to using the CMake build manager, see: 
** The CMake Reference Documentation link:https://cmake.org/cmake/help/v3.21/[Introduction].
** The official CMake documentation link:https://cmake.org/cmake/help/v3.21/guide/tutorial/index.html#guide:CMake%20Tutorial[CMake Tutorial].

////
[NOTE]
====
The [package]*cmake* package contains installed documentation.

ifdef::publish-rhel-7[] 
* On {RHEL}{nbsp}7, find the documentation in  [filename]`opt/rh/llvm-toolset-{comp-ver-short}/root/usr/share/doc/llvm-toolset-{comp-ver-short}-cmake-3.6.2/html/index.html`.
endif::publish-rhel-7[]

ifdef::publish-rhel-8[]
* On {RHEL}{nbsp}8, find the documentation in [filename]`/usr/share/doc/llvm/html/index.html`.
endif::publish-rhel-8[]
====
////
//.Verification

//[role="_additional-resources"]
//.Additional resources


