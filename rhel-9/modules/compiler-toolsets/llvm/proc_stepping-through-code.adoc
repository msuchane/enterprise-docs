:_module-type: PROCEDURE

[id="proc_stepping-through-code_{context}"]
= Stepping through code

[role="_abstract"]
You can use LLDB to step through the code of your program to execute only one line of code after the line pointer.

.Prerequisites
* You have started an interactive debugging session. +
For more information, see xref:proc_starting-a-debugging-session-with-lldb_{context}[Starting a debugging session with LLDB].

.Procedure
* To step through one line of code:
. Set your line pointer to the line you want to execute.
. Run the following command:
+
[subs="quotes,attributes"]
----
(lldb) step
----

* To step through a specific number of lines of code:
. Set your line pointer to the line you want to execute.
. Run the following command:
+
[subs="quotes,attributes"]
----
(lldb) step -c &lt;__number__&gt;
----
+
** Replace `&lt;__number__&gt;` with the number of lines you want to execute.

