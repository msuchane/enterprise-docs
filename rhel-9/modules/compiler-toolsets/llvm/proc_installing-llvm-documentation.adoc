:_module-type: PROCEDURE

[id="proc_installing-llvm-documentation_{context}"]
= Installing {comp} Toolset documentation

You can install documentation for {comp} Toolset on your local system.

.Prerequisites
* {comp} Toolset is installed. +
For more information, see xref:proc_installing-comp-toolset_{context}[Installing {comp} Toolset].

.Procedure
To install the [package]`llvm-doc` package, run the following command:

ifdef::publish-rhel-7[]
* On {RHEL}{nbsp}7:
+
[subs="quotes,attributes"]
----
# yum install {comp-collection}-llvm-doc
----
+
You can find the documentation under the following path: [filename]`/opt/rh/llvm-toolset-{comp-ver-short}/root/usr/share/doc/llvm-toolset-{comp-ver-short}-llvm-{comp-ver}/html/index.html`. 
endif::publish-rhel-7[]

ifdef::publish-rhel-8[]
* On {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
# yum install llvm-doc
----
+
You can find the documentation under the following path: [filename]`/usr/share/doc/llvm/html/index.html`.
endif::publish-rhel-8[]

ifdef::publish-rhel-9[]
* On {RHEL}{nbsp}9:
+
[subs="quotes,attributes"]
----
# dnf install llvm-doc
----
+
You can find the documentation under the following path: [filename]`/usr/share/doc/llvm/html/index.html`.
endif::publish-rhel-9[]



