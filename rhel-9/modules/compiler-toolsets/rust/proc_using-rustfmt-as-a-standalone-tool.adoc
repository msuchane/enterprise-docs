:_module-type: PROCEDURE

[id="proc_using-rustfmt-as-a-standalone-tool_{context}"]
= Using rustfmt as a standalone tool

[role="_abstract"]
Use `rustfmt` as a standalone tool to format a {comp} source file and all its dependencies.
As an alternative, use `rustfmt` with the Cargo build tool.     
For more information, see xref:proc_using-rustfmt-with-cargo_{context}[Using rustfmt with Cargo].

.Prerequisites
* An existing {comp} project. +
For information on how to create a {comp} project, see xref:proc_creating-a-rust-project_assembly_the-cargo-build-tool[Creating a {comp} project].

.Procedure

To format a {comp} source file using `rustfmt` as a standalone tool, run the following command:

ifdef::publish-rhel-7[]
* On {RHEL}{nbsp}7:
+
[subs="quotes,attributes"]
----
$ scl enable {comp-pkg} 'rustfmt &lt;__source-file__&gt;'
----
+
** Replace `&lt;__source_file__&gt;` with the name of your source file. +
Alternatively, you can replace `&lt;__source_file__&gt;` with standard input. [command]`rustfmt` then provides its output in standard output.

endif::publish-rhel-7[]
ifdef::publish-rhel-8[]
* On {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
$ rustfmt &lt;__source-file__&gt;
----
** Replace `&lt;__source_file__&gt;` with the name of your source file. +
Alternatively, you can replace `&lt;__source_file__&gt;` with standard input. [command]`rustfmt` then provides its output in standard output.
endif::publish-rhel-8[]
ifdef::publish-rhel-9[]
* On {RHEL}{nbsp}9:
+
[subs="quotes,attributes"]
----
$ rustfmt &lt;__source-file__&gt;
----
** Replace `&lt;__source_file__&gt;` with the name of your source file. +
Alternatively, you can replace `&lt;__source_file__&gt;` with standard input. [command]`rustfmt` then provides its output in standard output.
endif::publish-rhel-9[]

//For further details see the help message of [command]`rustfmt`:

[NOTE]
====
By default, [command]`rustfmt` modifies the affected files without displaying details or creating backups.
To display details and create backups, run `rustfmt` with the [option]`--write-mode _value_`.
====

//.Verification

//[role="_additional-resources"]
//.Additional resources

