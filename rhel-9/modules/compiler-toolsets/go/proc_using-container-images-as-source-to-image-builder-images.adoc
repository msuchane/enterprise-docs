:_module-type: PROCEDURE

[id="proc_using-container-images-as-source-to-image-builder-images_{context}"]
= Using container images as Source-to-Image builder images on {RHEL} 7

[role="_abstract"]
You can use the {comp} Toolset container image as a Source-to-Image (S2I) builder image on {RHEL} 7.

//.Prerequisites

.Procedure
. Set the  [envvar]`IMPORT_URL` variable to a URL specifying the location of your code.
. To build your S2I builder image, run the `s2i build` command.

[NOTE]
====
If the main package location is not identical with the location specified by the `IMPORT_URL` variable, set the `INSTALL_URL` variable to a URL that specifies the package location providing the application's main executable file when built.
====

//.Verification

[role="_additional-resources"]
.Additional resources
* The link:https://access.redhat.com/documentation/en-us/openshift_container_platform/{ocp-ver}/html-single/images/index#images-create-guide-openshift_create-images[OpenShift Container Platform Image Creation Guide].

* For more information on using Source-to-Image (S2I), see link:https://access.redhat.com/documentation/en-us/red_hat_software_collections/3{dashbeta}/html/using_red_hat_software_collections_container_images/building_application_images[Using Red Hat Software Collections Container Images].

