:_mod-docs-content-type: ASSEMBLY
//include::modules/core-services/attributes-core-services.adoc[]

:parent-context-of-assembly_setting-apache-http-server: {context}

[id="setting-apache-http-server_{context}"]
= Setting up the Apache HTTP web server
:context: setting-apache-http-server

:setting-apache-server:


include::rhel-8/modules/core-services/con_apache-intro.adoc[leveloffset=+1]

include::modules/core-services/con_apache-changes-rhel-9.adoc[leveloffset=+1]

//not relevant for RHEL 9
//include::rhel-8/modules/core-services/proc_apache-updating.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/con_the-apache-configuration-files.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_managing-the-httpd-service.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_setting-up-a-single-instance-apache-http-server.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_configuring-apache-name-based-virtual-hosts.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-kerberos-authentication-for-the-apache-http-web-server.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-tls-encryption-on-an-apache-http-server.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_configuring-tls-client-certificate-authentication.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_securing-web-applications-on-a-web-server-using-modsecurity.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_installing-the-apache-http-server-manual.adoc[leveloffset=+1]

//include::rhel-8/modules/core-services/proc_apache-working-with-modules.adoc[leveloffset=+1]
//A new assembly added instead (see below)
include::rhel-8/assemblies/assembly_working-with-apache-modules.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_exporting-a-private-key-and-certificates-from-an-nss-database-to-use-them-in-an-apache-web-server-configuration.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* [citetitle]`httpd(8)` man page
* [citetitle]`httpd.service(8)` man page
* [citetitle]`httpd.conf(5)` man page
* [citetitle]`apachectl(8)` man page
* Kerberos authentication on an Apache HTTP server: link:https://access.redhat.com/articles/5854761[Using GSS-Proxy for Apache httpd operation]. Using Kerberos is an alternative way to enforce client authorization on an Apache HTTP Server.
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/configuring-applications-to-use-cryptographic-hardware-through-pkcs-11_security-hardening[Configuring applications to use cryptographic hardware through PKCS #11].

:!setting-apache-server:

:context: {parent-context-of-assembly_setting-apache-http-server}
