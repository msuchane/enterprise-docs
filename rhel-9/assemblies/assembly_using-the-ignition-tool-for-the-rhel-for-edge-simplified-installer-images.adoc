ifdef::context[:parent-context-of-assembly_using-the-ignition-tool-for-the-rhel-for-edge-simplified-installer-images: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_using-the-ignition-tool-for-the-rhel-for-edge-simplified-installer-images"]
endif::[]
ifdef::context[]
[id="assembly_using-the-ignition-tool-for-the-rhel-for-edge-simplified-installer-images_{context}"]
endif::[]
= Using the Ignition tool for the RHEL for Edge Simplified Installer images

:context: assembly_using-the-ignition-tool-for-the-rhel-for-edge-simplified-installer-images

RHEL for Edge uses the Ignition tool to inject the user configuration into the images at an early stage of the boot process. The user configuration that the Ignition tool injects includes:

* The user configuration. 
* Writing files, such as regular files, and `systemd` units.

On the first boot, Ignition reads its configuration either from a remote URL or a file embedded in the simplified installer ISO. Then, Ignition applies that configuration into the image.


include::modules/edge/proc_creating-an-ignition-configuration-file.adoc[leveloffset=+1]

include::modules/edge/proc_creating-a-blueprint-in-the-cli-with-support-to-ignition.adoc[leveloffset=+1]

include::modules/edge/proc_creating-a-blueprint-with-support-to-ignition-using-the-gui.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_using-the-ignition-tool-for-the-rhel-for-edge-simplified-installer-images[:context: {parent-context-of-assembly_using-the-ignition-tool-for-the-rhel-for-edge-simplified-installer-images}]
ifndef::parent-context-of-assembly_using-the-ignition-tool-for-the-rhel-for-edge-simplified-installer-images[:!context:]

