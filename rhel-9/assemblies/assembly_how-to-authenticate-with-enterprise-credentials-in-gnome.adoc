ifdef::context[:parent-context-of-how-to-authenticate-with-enterprise-credentials-in-gnome: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="how-to-authenticate-with-enterprise-credentials-in-gnome"]
endif::[]
ifdef::context[]
[id="how-to-authenticate-with-enterprise-credentials-in-gnome_{context}"]
endif::[]
= Enabling authentication with enterprise credentials in GNOME

:context: how-to-authenticate-with-enterprise-credentials-in-gnome

If your workplace uses a system called Active Directory or IPA, and you have an account there, you can use that account to log into the GNOME desktop environment.

Logging in using enterprise credentials provides centralized account management, streamlines access to work-related resources, and gives the convenience of Single Sign-On (SSO). 

include::modules/desktop/proc_configuring-enterprise-credentials-in-gnome.adoc[leveloffset=+1]

include::modules/desktop/proc_adding-enterprise-user-in-gnome.adoc[leveloffset=+1]

include::modules/desktop/proc_logging-into-to-gnome-with-enterprise-credentials.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* For troubleshooting, see the `realm` man page


ifdef::parent-context-of-how-to-authenticate-with-enterprise-credentials-in-gnome[:context: {parent-context-of-how-to-authenticate-with-enterprise-credentials-in-gnome}]
ifndef::parent-context-of-how-to-authenticate-with-enterprise-credentials-in-gnome[:!context:]

