:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_dotnet: {context}]


ifndef::context[]
[id="assembly_dotnet"]
endif::[]
ifdef::context[]
[id="assembly_dotnet_{context}"]
endif::[]
= .NET


:context: assembly_dotnet


[role="_abstract"]
This chapter lists the most notable changes to .NET between RHEL 8 and RHEL 9.


include::modules/upgrades-and-differences/ref_changes-to-dotnet.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_dotnet[:context: {parent-context-of-assembly_dotnet}]
ifndef::parent-context-of-assembly_dotnet[:!context:]
