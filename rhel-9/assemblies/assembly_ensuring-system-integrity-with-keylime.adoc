ifdef::context[:parent-context-of-assembly_ensuring-system-integrity-with-keylime: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_ensuring-system-integrity-with-keylime"]
endif::[]
ifdef::context[]
[id="assembly_ensuring-system-integrity-with-keylime_{context}"]
endif::[]
= Ensuring system integrity with Keylime

:context: assembly_ensuring-system-integrity-with-keylime

With Keylime, you can continuously monitor the integrity of remote systems and verify the state of systems at boot. You can also send encrypted files to the monitored systems, and specify automated actions triggered whenever a monitored system fails the integrity test.

include::modules/security/con_how-keylime-works.adoc[leveloffset=+1]

include::modules/security/proc_configuring-keylime-verifier.adoc[leveloffset=+1]

include::modules/security/proc_configuring-keylime-verifier-as-a-container.adoc[leveloffset=+1]

include::modules/security/proc_configuring-keylime-registrar.adoc[leveloffset=+1]

include::modules/security/proc_configuring-keylime-registrar-as-a-container.adoc[leveloffset=+1]

include::modules/security/proc_setting-keylime-server-using-system-roles.adoc[leveloffset=+1]

include::modules/security/ref_variables-for-keylime-server-system-role.adoc[leveloffset=+1]

include::modules/security/proc_configuring-keylime-tenant.adoc[leveloffset=+1]

include::modules/security/proc_configuring-keylime-agent.adoc[leveloffset=+1]

include::modules/security/proc_deploying-keylime-for-runtime-monitoring.adoc[leveloffset=+1]

include::modules/security/proc_deploying-keylime-for-measured-boot-attestation.adoc[leveloffset=+1]

include::modules/security/ref_keylime-environment-variables.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_ensuring-system-integrity-with-keylime[:context: {parent-context-of-assembly_ensuring-system-integrity-with-keylime}]
ifndef::parent-context-of-assembly_ensuring-system-integrity-with-keylime[:!context:]
