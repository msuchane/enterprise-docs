:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-assembly_the-web-console: {context}]



ifndef::context[]
[id="assembly_the-web-console"]
endif::[]
ifdef::context[]
[id="assembly_the-web-console_{context}"]
endif::[]
= The web console

:context: assembly_the-web-console


[role="_abstract"]
The following chapter contains the most notable changes to the web console between RHEL 8 and RHEL 9.


include::modules/upgrades-and-differences/ref_changes-to-the-web-console.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_the-web-console[:context: {parent-context-of-assembly_the-web-console}]
ifndef::parent-context-of-assembly_the-web-console[:!context:]

