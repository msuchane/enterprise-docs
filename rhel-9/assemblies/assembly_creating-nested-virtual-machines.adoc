:_newdoc-version: 2.17.0
:_template-generated: 2024-04-06

ifdef::context[:parent-context-of-creating-nested-virtual-machines: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="creating-nested-virtual-machines"]
endif::[]
ifdef::context[]
[id="creating-nested-virtual-machines_{context}"]
endif::[]
= Creating nested virtual machines

:context: creating-nested-virtual-machines

You can use nested virtual machines (VMs) if you require a different host operating system than what your local host is running. This eliminates the need for additional physical hardware. 

include::modules/virtualization/snip_tp-warning-box-for-nested-virtualization.adoc[leveloffset=+1]

include::rhel-8/modules/virtualization/con_what-is-nested-virtualization.adoc[leveloffset=+1]

include::modules/virtualization/ref_support-limitations-for-nested-virtualization.adoc[leveloffset=+1]

include::rhel-8/modules/virtualization/proc_creating-a-nested-virtual-machine-on-intel.adoc[leveloffset=+1]

include::rhel-8/modules/virtualization/proc_creating-a-nested-virtual-machine-on-amd.adoc[leveloffset=+1]

include::rhel-8/modules/virtualization/proc_creating-a-nested-virtual-machine-on-ibm-z.adoc[leveloffset=+1]

ifdef::parent-context-of-creating-nested-virtual-machines[:context: {parent-context-of-creating-nested-virtual-machines}]
ifndef::parent-context-of-creating-nested-virtual-machines[:!context:]

