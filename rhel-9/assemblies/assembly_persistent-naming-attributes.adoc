ifdef::context[:parent-context-of-persistent-naming-attributes: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="persistent-naming-attributes"]
endif::[]
ifdef::context[]
[id="persistent-naming-attributes_{context}"]
endif::[]
= Persistent naming attributes

:context: persistent-naming-attributes

The way you identify and manage storage devices ensures the stability and predictability of the system.
{PRODUCT} uses two primary naming schemes for this purpose: traditional device names and persistent naming attributes.


.Traditional device names

Traditional device names are determined by the Linux kernel based on the physical location of the device in the system. For example, the first SATA drive is usually labeled as `/dev/sda`, the second as `/dev/sdb`, and so on. While these names are straightforward, they are subject to change when devices are added or removed or when the hardware configuration is modified. This can pose challenges for scripting and configuration files. Furthermore, traditional names lack descriptive information about the purpose or characteristics of the device.

.Persistent naming attributes

Persistent naming attributes are based on unique characteristics of the storage devices, making them more stable and predictable across system reboots.  Implementing PNAs involves a more detailed initial configuration compared to traditional naming. One of the key benefits of PNAs is their resilience to changes in hardware configurations, making them ideal for maintaining consistent naming conventions. When using PNAs, you can reference storage devices within scripts, configuration files, and management tools without concerns about unexpected name changes. Additionally, PNAs often include valuable metadata, such as device type or manufacturer information, enhancing their descriptiveness for effective device identification and management.



include::modules/filesystems-and-storage/con_persistent-attributes-for-identifying-file-systems-and-block-devices.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_udev-device-naming-rules.adoc[leveloffset=+1]


ifdef::parent-context-of-persistent-naming-attributes[:context: {parent-context-of-persistent-naming-attributes}]
ifndef::parent-context-of-persistent-naming-attributes[:!context:]

