:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-troubleshooting: {context}]


ifndef::context[]
[id="troubleshooting"]
endif::[]
ifdef::context[]
[id="troubleshooting_{context}"]
endif::[]
= Troubleshooting

:context: troubleshooting

[role="_abstract"]
You can refer to the following tips to troubleshoot upgrading from RHEL 8 to RHEL 9.

include::modules/upgrades-and-differences/ref_troubleshooting-resources-for-upgrading-from-rhel-8-to-rhel9.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_troubleshooting-tips-for-upgrading-from-rhel-8-to-rhel9.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_known-issues-for-upgrading-from-rhel-8-to-rhel9.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_known-issues-for-upgrading-from-rhel-8.8-to-rhel9.adoc[leveloffset=+1]

include::rhel-8/modules/upgrades-and-differences/ref_obtaining-support-for-upgrading-from-rhel-7-to-rhel8.adoc[leveloffset=+1]


ifdef::parent-context-of-troubleshooting[:context: {parent-context-of-troubleshooting}]
ifndef::parent-context-of-troubleshooting[:!context:]
