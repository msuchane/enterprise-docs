:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_hardware-enablement: {context}]


ifndef::context[]
[id="assembly_hardware-enablement"]
endif::[]
ifdef::context[]
[id="assembly_hardware-enablement_{context}"]
endif::[]
= Hardware enablement


:context: assembly_hardware-enablement



[role="_abstract"]
The following chapters contain the most notable changes to hardware enablement between RHEL 8 and RHEL 9.

//Include modules here.

include::modules/upgrades-and-differences/ref_unmaintained-hardware-hardware-support.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_removed-hardware-support.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_hardware-enablement[:context: {parent-context-of-assembly_hardware-enablement}]
ifndef::parent-context-of-assembly_hardware-enablement[:!context:]

