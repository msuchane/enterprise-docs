:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_edge: {context}]



ifndef::context[]
[id="assembly_edge"]
endif::[]
ifdef::context[]
[id="assembly_edge_{context}"]
endif::[]
= Edge

:context: assembly_edge



[role="_abstract"]
The following chapter contains the most notable changes to RHEL Edge between RHEL 8 and RHEL 9.


include::modules/upgrades-and-differences/ref_changes-to-edge.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_edge[:context: {parent-context-of-assembly_edge}]
ifndef::parent-context-of-assembly_edge[:!context:]

