:_mod-docs-content-type: ASSEMBLY
// This Assembly is included in titles/installing/installing-identity-management-and-access-control/,
// which has a context of "installing-identity-management"

:parent-context-of-preparing-the-system-for-ipa-client-installation: {context}

// Commented out blank parent-context
//:parent-context: {context}

[id="preparing-the-system-for-ipa-client-installation_{context}"]
= Preparing the system for IdM client installation

:context: preparing-the-system-for-ipa-client-installation

Ensure your system meets the following conditions before you install an {IPA} (IdM) client.

include::rhel-8/modules/identity-management/ref_supported-versions-of-rhel-for-installing-idm-clients.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/con_dns-requirements-for-idm-clients.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/con_port-requirements-for-ipa-clients.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/con_ipv6-requirements-for-idm-clients.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-packages-required-for-an-idm-client.adoc[leveloffset=+1]

:context: {parent-context-of-preparing-the-system-for-ipa-client-installation}
