ifdef::context[:parent-context-of-assembly_the-lldb-debugger: {context}]

ifndef::context[]
[id="assembly_the-lldb-debugger"]
endif::[]
ifdef::context[]
[id="assembly_the-lldb-debugger_{context}"]
endif::[]
= The LLDB debugger

:context: assembly_the-lldb-debugger

[role="_abstract"]
The LLDB debugger is a command-line tool for debugging C and {cpp} programs.
Use LLDB to inspect memory within the code being debugged, control the execution state of the code, and detect the execution of particular sections of code.

////
ifdef::publish-rhel-7[:distributed-with: {comp} Toolset is distributed with lldb {lldb-ver-rhel-7}.]
ifdef::publish-rhel-8[:distributed-with: {comp} Toolset is distributed with lldb {lldb-ver-rhel-8}.]
ifdef::publish-rhel-9[:distributed-with: {comp} Toolset is distributed with lldb {lldb-ver-rhel-9}.]
ifdef::publish-rhel-7+publish-rhel-8[:distributed-with: {comp} Toolset is distributed with lldb {lldb-ver-rhel-7} on {rhel} 7 and lldb {lldb-ver-rhel-8} on {rhel} 8.]
ifdef::publish-rhel-8+publish-rhel-9[:distributed-with: {comp} Toolset is distributed with lldb {lldb-ver-rhel-8} on {rhel} 8 and lldb {lldb-ver-rhel-9} on {rhel} 9.]
ifdef::publish-rhel-7+publish-rhel-9[:distributed-with: {comp} Toolset is distributed with lldb {lldb-ver-rhel-7} on {rhel} 7 and lldb {lldb-ver-rhel-9} on {rhel} 9.]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:distributed-with: {comp} Toolset is distributed with lldb {lldb-ver-rhel-7} on {rhel} 7, lldb {lldb-ver-rhel-8} on {rhel} 8 and lldb {lldb-ver-rhel-9} on {rhel} 9.]

{distributed-with}
////

{comp} Toolset is distributed with LLDB {comp-ver-rhel-7}.

== Prerequisites

* {comp} Toolset is installed. +
For more information, see xref:proc_installing-comp-toolset_assembly_llvm[Installing {comp} Toolset].
* Your compiler is configured to create debug information. +
For instructions on configuring the Clang compiler, see link:http://clang.llvm.org/docs/UsersManual.html#controlling-debug-information[Controlling Debug Information] in the Clang Compiler User’s Manual. +
For instructions on configuring the GCC compiler, see link:https://access.redhat.com/documentation/en-us/red_hat_developer_toolset/9/html/user_guide/chap-gdb#sect-GDB-Prepare[Preparing a Program for Debugging] in the {DTS} User Guide.

include::modules/compiler-toolsets/llvm/proc_starting-a-debugging-session-with-lldb.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/proc_executing-your-program.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/proc_using-breakpoints.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/proc_stepping-through-code.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/proc_listing-source-code.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/proc_displaying-current-program-data.adoc[leveloffset=+1]
//include::modules/compiler-toolsets/llvm/[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* For more information on the LLDB debugger, see the official LLDB documentation link:https://lldb.llvm.org/tutorial.html[LLDB Tutorial].

* For a list of GDB commands and their LLDB equivalents, see the link:https://lldb.llvm.org/lldb-gdb.html[GDB to LLDB Command Map]. 

ifdef::parent-context-of-assembly_the-lldb-debugger[:context: {parent-context-of-assembly_the-lldb-debugger}]
ifndef::parent-context-of-assembly_the-lldb-debugger[:!context:]

