:_mod-docs-content-type: ASSEMBLY
// Include shared, global attributes

ifdef::context[:parent-context-of-using-the-php-scripting-language: {context}]

ifndef::context[]
[id="assembly_using-the-php-scripting-language"]
endif::[]
ifdef::context[]
[id="assembly_using-the-php-scripting-language_{context}"]
endif::[]

= Using the PHP scripting language

:context: assembly_using-the-php-scripting-language

Hypertext Preprocessor (PHP) is a general-purpose scripting language mainly used for server-side scripting. You can use PHP to run the PHP code by using a web server.

include::modules/core-services/proc_installing-the-php-scripting-language.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_using-the-php-scripting-language-with-a-web-server.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_running-a-php-script-using-the-command-line-interface.adoc[leveloffset=+1]



== Additional resources

* `httpd(8)` — The manual page for the `httpd` service containing the complete list of its command-line options.
* `httpd.conf(5)` — The manual page for `httpd` configuration, describing the structure and location of the `httpd` configuration files.
* `nginx(8)` — The manual page for the `nginx` web server containing the complete list of its command-line options and list of signals.
* `php-fpm(8)` — The manual page for PHP FPM describing the complete list of its command-line options and configuration files.


ifdef::parent-context-of-using-the-php-scripting-language[:context: {parent-context-of-using-the-php-scripting-language}]
ifndef::parent-context-of-using-the-php-scripting-language[:!context:]
