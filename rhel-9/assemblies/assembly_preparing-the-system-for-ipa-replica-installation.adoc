ifdef::context[:parent-context-of-preparing-the-system-for-ipa-replica-installation: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="preparing-the-system-for-ipa-replica-installation"]
endif::[]
ifdef::context[]
[id="preparing-the-system-for-ipa-replica-installation_{context}"]
endif::[]
= Preparing the system for an IdM replica installation


:context: preparing-the-system-for-ipa-replica-installation



[role="_abstract"]
The following links list the requirements to install an {IPA} (IdM) replica. Before the installation, make sure your system meets these requirements.


. Ensure link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/installing_identity_management/preparing-the-system-for-ipa-server-installation_installing-identity-management[the target system meets the general requirements for IdM server installation].

. Ensure xref:replica-version-requirements_{context}[the target system meets the additional, version requirements for IdM replica installation].

. [Optional] If you are adding a RHEL 9 Identity Management (IdM) replica on which FIPS mode is enabled to a RHEL 8 IdM deployment in FIPS mode, xref:ensuring-fips-compliance-for-a-rhel-9-replica-joining-a-rhel-8-idm-environment_{context}[ensure that the replica has the correct encryption types enabled].

. Authorize the target system for enrollment into the IdM domain. For more information, see one of the following sections that best fits your needs:

** xref:authorizing-the-installation-of-a-replica-on-an-ipa-client_{context}[Authorizing the installation of a replica on an IdM client]
** xref:authorizing-the-installation-of-a-replica-on-a-system-that-is-not-enrolled-into-idm_{context}[Authorizing the installation of a replica on a system that is not enrolled into IdM]

.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/planning_identity_management/index[Planning the replica topology]

include::rhel-8/modules/identity-management/con_replica-version-requirements.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/ref_display-software-version.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-fips-compliance-for-a-rhel-9-replica-joining-a-rhel-8-idm-environment.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_authorizing-the-installation-of-a-replica-on-an-ipa-client.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_authorizing-the-installation-of-a-replica-on-a-system-that-is-not-enrolled-into-idm.adoc[leveloffset=+1]




ifdef::parent-context-of-preparing-the-system-for-ipa-replica-installation[:context: {parent-context-of-preparing-the-system-for-ipa-replica-installation}]
ifndef::parent-context-of-preparing-the-system-for-ipa-replica-installation[:!context:]
