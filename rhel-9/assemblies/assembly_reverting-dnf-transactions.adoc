
ifdef::context[:parent-context-of-reverting-dnf-transactions: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="reverting-dnf-transactions"]
endif::[]
ifdef::context[]
[id="reverting-dnf-transactions_{context}"]
endif::[]
= Reverting DNF transactions

:context: reverting-dnf-transactions

Reverting a *{PackageManagerName}* transaction can be useful if you want to undo operations performed during the transaction. For example, if you installed several packages by using the [command]`{PackageManagerCommand} install` command, you can uninstall these packages at once by reverting an installation transaction.

You can revert *{PackageManagerName}* transactions the following ways:

* Revert a single *{PackageManagerName}* transaction by using the [command]`{PackageManagerCommand} history undo` command.
* Revert all *{PackageManagerName}* transactions performed between the specified transaction and the last transaction by using the [command]`{PackageManagerCommand} history rollback` command.

[IMPORTANT]
====
Downgrading RHEL system packages to an older version by using the [command]`{PackageManagerCommand} history undo` and [command]`{PackageManagerCommand} history rollback` command is not supported. This concerns especially the `selinux`, `selinux-policy-*`, `kernel`, and `glibc` packages, and dependencies of `glibc` such as `gcc`. Therefore, downgrading a system to a minor version (for example, from RHEL 9.1 to RHEL 9.0) is not recommended because it might leave the system in an incorrect state.
====
 

include::modules/core-services/proc_reverting-a-single-dnf-transaction-using-dnf-history-undo.adoc[leveloffset=+1]

include::modules/core-services/proc_reverting-multiple-dnf-transactions-using-dnf-history-rollback.adoc[leveloffset=+1]



ifdef::parent-context-of-reverting-dnf-transactions[:context: {parent-context-of-reverting-dnf-transactions}]
ifndef::parent-context-of-reverting-dnf-transactions[:!context:]

