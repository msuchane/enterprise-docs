:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>


:parent-context-of-post-installation-tasks: {context}


[id="post-installation-tasks_{context}"]
= Completing post-installation tasks


:context: post-installation-tasks


After installing {ProductName} on AMD64, Intel 64, and 64-bit ARM systems, you must register and secure your system.

Depending on your requirements, you can use several methods to register your system. Most of these methods are completed as part of post-installation tasks. However, a registered system is authorized to access protected content repositories for subscribed products through the Red Hat Content Delivery Network (CDN) *before* the installation process starts. See xref:register-and-install-from-cdn-using-gui_register-and-install-from-cdn[Registering and installing RHEL from the CDN] for more information.

include::rhel-8/modules/installer/con_the-value-of-registering-your-rhel-system-to-red-hat.adoc[leveloffset=+1]

include::rhel-8/modules/installer/proc_subscription-manager-post-install-ui.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_registering-rhel-8-4-using-the-installer-gui.adoc[leveloffset=+1]

include::rhel-8/modules/installer/con_registration-assistant.adoc[leveloffset=+1]

include::modules/installer/proc_registering-your-system-using-the-command-line.adoc[leveloffset=+1]

include::rhel-8/modules/installer/proc_configuring-system-purpose-using-the-subscription-manager-command-line-tool.adoc[leveloffset=+1]

include::rhel-8/modules/installer/proc_securing_your_system.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_deploying-systems-that-are-compliant-with-a-security-profile-immediately-after-an-installation.adoc[leveloffset=+1]

include::rhel-8/modules/installer/ref_next-steps.adoc[leveloffset=+1]

:context: {parent-context-of-post-installation-tasks}
