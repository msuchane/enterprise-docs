
ifdef::context[:parent-context-of-assembly_improved-patch-declaration: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_improved-patch-declaration"]
endif::[]
ifdef::context[]
[id="assembly_improved-patch-declaration_{context}"]
endif::[]
= Improved patch declaration

:context: assembly_improved-patch-declaration


include::modules/core-services/con_optional-automatic-patch-and-source-numbering.adoc[leveloffset=+1]

include::modules/core-services/con_patchlist-and-sourcelist-sections.adoc[leveloffset=+1]

include::modules/core-services/con_autopatch-now-accepts-patch-ranges.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_improved-patch-declaration[:context: {parent-context-of-assembly_improved-patch-declaration}]
ifndef::parent-context-of-assembly_improved-patch-declaration[:!context:]
