// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:rdma-title:

include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::rhel-8/modules/infiniband-rdma/con_understanding-infiniband-and-rdma.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-soft-iwarp.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-roce.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-the-core-rdma-subsystem.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-an-infiniband-subnet-manager.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-ipoib.adoc[leveloffset=+1]

include::assemblies/assembly_testing-infiniband-networks.adoc[leveloffset=+1]
