// Show the table of contents
:toc:
// The name of the title
:ProjectName: Tuning performance in Identity{nbsp}Management
// The subtitle of the title
:Subtitle: Optimizing the IdM services, such as Directory Server, KDC, and SSSD, for better performance
// The abstract of the title
:Abstract: {RH} tunes {IPA} (IdM) to perform well in most deployments. However, in specific scenarios, it can be beneficial to tune IdM components, such as replication agreements, the Directory Server, the Kerberos Key Distribution Center (KDC), or the System Security Services Daemon (SSSD).
// The name of the title for the purposes of {context}
:ProjectNameID: tuning-performance-in-idm

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
