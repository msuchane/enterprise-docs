// Show the table of contents
:toc:
// The name of the title
:ProjectName: Using Ansible to install and manage Identity Management
// The subtitle of the title
:Subtitle: Using Ansible to maintain an IdM environment
// The abstract of the title
//:Abstract: The abstract is directly in docinfo.xml, because within an attribute, markups are not supported.
// The name of the title for the purposes of {context}
:ProjectNameID: using-ansible-to-install-and-manage-identity-management

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
