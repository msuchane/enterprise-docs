// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}
:managing-certificates-in-idm:

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not


include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]


include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]


include::rhel-8/assemblies/assembly_using-certs-idm-intro.adoc[leveloffset=+1]


include::rhel-8/assemblies/assembly_managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca.adoc[leveloffset=+1]


include::rhel-8/assemblies/assembly_managing-idm-certificates-using-ansible.adoc[leveloffset=+1]


include::rhel-8/assemblies/assembly_managing-externally-signed-certificates-for-idm-users-hosts-and-services.adoc[leveloffset=+1]


include::rhel-8/assemblies/assembly_converting-cert-formats-idm.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_creating-and-managing-certificate-profiles-in-identity-management.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_managing-the-validity-of-certificates-in-idm.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-idm-for-smart-card-auth.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-certificates-issued-by-adcs-for-smart-card-authentication-in-idm.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-cert-mapping-idm.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_certs-web-ui-auth-idm.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_ipa-ca-renewal.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_managing-externally-signed-ca-certificates.adoc[leveloffset=+1]


include::rhel-8/assemblies/assembly_renewing-expired-system-certificates-when-idm-is-offline.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_replacing-the-web-server-and-ldap-server-certificates-if-they-have-not-yet-expired-on-an-idm-replica.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_replacing-the-web-server-and-ldap-server-certificates-if-they-have-expired-in-the-whole-idm-deployment.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_generating-crl-on-the-idm-ca-server.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_decommissioning-a-server-that-performs-the-ca-renewal-server-and-crl-publisher-roles.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_certmonger-for-service-certs.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_deploying-and-managing-the-acme-service-in-idm.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_requesting-certificates-using-rhel-system-roles.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_restricting-an-application-to-trust-only-a-subset-of-certificates.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_invalidating-a-specific-group-of-related-certificates-quickly.adoc[leveloffset=+1]


include::rhel-8/assemblies/assembly_verifying-certificates-using-idm-healthcheck.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_verifying-system-certificates-using-idm-healthcheck.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_understanding-what-certificates-are-used-by-idm.adoc[leveloffset=+1]
