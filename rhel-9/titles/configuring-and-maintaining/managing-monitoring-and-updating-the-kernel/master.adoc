// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:kernel-title:

// include::common-content/beta.adoc[leveloffset=+1]

include::common-content/making-open-source-more-inclusive.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

// Managing modules and kernel configuration - at the boot time or run-time

include::rhel-8/assemblies/assembly_the-linux-kernel.adoc[leveloffset=+1]

include::modules/core-kernel/con_what-is-kernel-64k.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_managing-kernel-modules.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-kernel-command-line-parameters.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-kernel-parameters-at-runtime.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-kernel-parameters-permanently-by-using-the-kernel-settings-rhel-system-role.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_applying-patches-with-kernel-live-patching.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_keeping-kernel-panic-parameters-disabled-in-virtualized-environments.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_adjusting-kernel-parameters-for-database-servers.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_getting-started-with-kernel-logging.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_reinstalling-grub.adoc[leveloffset=+1]

// Kernel dumping topics

include::rhel-8/assemblies/assembly_installing-kdump.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-kdump-on-the-command-line.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_enabling-kdump.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_supported-kdump-configurations-and-targets.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_firmware-assisted-dump-mechanisms.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_analyzing-a-core-dump.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_using-early-kdump-to-capture-boot-time-crashes.adoc[leveloffset=+1]

// Enhancing security

include::rhel-8/assemblies/assembly_signing-a-kernel-and-modules-for-secure-boot.adoc[leveloffset=+1]

include::assemblies/assembly_updating-the-secure-boot-revocation-list.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_enhancing-security-with-the-kernel-integrity-subsystem.adoc[leveloffset=+1]

// Best practices for performance tuning

include::assemblies/assembly_using-systemd-to-manage-resources-used-by-applications.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_setting-limits-for-applications.adoc[leveloffset=+1]

include::assemblies/assembly_using-cgroupfs-to-manually-manage-cgroups.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_analyzing-system-performance-with-bpf-compiler-collection.adoc[leveloffset=+1]



