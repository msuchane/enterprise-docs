// Show the table of contents
:toc:
// The name of the title
:ProjectName: Configuring and managing logical volumes

// The subtitle of the title
:Subtitle: Configuring and managing the LVM on RHEL

// The abstract of the title
//:Abstract: The abstract is added to the docinfo.xml file because within an attribute, multiple paragraphs and markups are not supported.

// The name of the title for the purposes of {context}
:ProjectNameID: configuring-and-managing-logical-volumes

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
