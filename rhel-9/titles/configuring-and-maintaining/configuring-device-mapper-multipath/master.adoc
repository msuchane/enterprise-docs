// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_overview-of-device-mapper-multipathing.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_multipath-devices.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-dm-multipath.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_enabling-multipathing-on-nvme-devices.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_modifying-dm-multipath-configuration-file.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_preventing-devices-from-multipathing.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_managing-mpath.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_removing-storage-devices.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_troubleshooting-multipath.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-maximum-time-for-storage-error-recovery-with-eh_deadline.adoc[leveloffset=+1]

// define a unique token to check against, so as to let content verify if it's in this book or not
:configuring-device-mapper-multipath:

// RHEL 9 assembly- include::assemblies/assembly_mixing-unrelated-items.adoc[leveloffset=+1]

// RHEL 9 module- include::modules/_examples/con_the-metamorphosis.adoc[leveloffset=+1]

// RHEL 8 module- include::rhel-8/modules/security/con_system-wide-crypto-policies.adoc[leveloffset=+1]

// RHEL 8 assembly-include::rhel-8/assemblies/assembly_protecting-systems-against-intrusive-usb-devices.adoc[leveloffset=+1]
