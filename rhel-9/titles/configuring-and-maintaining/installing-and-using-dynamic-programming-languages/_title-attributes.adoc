// Show the table of contents
:toc:
// The name of the title
:ProjectName: Installing and using dynamic programming languages

// The subtitle of the title
:Subtitle: Installing and using Python and PHP in {RHEL9}

// The abstract of the title
:Abstract: Install and use Python 3, package Python 3 RPMs, and learn how to handle interpreter directives in Python scripts. Install the PHP scripting language, use PHP with the Apache HTTP Server or the ngninx web server, and run a PHP script from a command-line interface.

// The name of the title for the purposes of {context}
:ProjectNameID: installing-and-using-dynamic-programming-languages

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
