// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:upgrading-to-rhel-9:


include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::rhel-8/modules/upgrades-and-differences/ref_key-migration-terminology.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/con_supported-upgrade-paths.adoc[leveloffset=+1]

include::assemblies/assembly_planning-an-upgrade-to-rhel-9.adoc[leveloffset=+1]

//include::modules/upgrades-and-differences/con_planning-an-upgrade.adoc[leveloffset=+1]

include::assemblies/assembly_preparing-for-the-upgrade.adoc[leveloffset=+1]

include::assemblies/assembly_reviewing-the-pre-upgrade-report.adoc[leveloffset=+1]

include::assemblies/assembly_performing-the-upgrade.adoc[leveloffset=+1]

//include::modules/upgrades-and-differences/proc_performing-the-upgrade-from-rhel-8-to-rhel-9.adoc[leveloffset=+1]

include::assemblies/assembly_verifying-the-post-upgrade-state.adoc[leveloffset=+1]

//include::modules/upgrades-and-differences/proc_verifying-the-post-upgrade-state-of-the-rhel-9-system.adoc[leveloffset=+1]

include::assemblies/assembly_performing-post-upgrade-tasks-on-the-rhel-9-system.adoc[leveloffset=+1]

//include::modules/upgrades-and-differences/proc_performing-post-upgrade-tasks-rhel-8-to-rhel-9.adoc[leveloffset=+1]

include::assemblies/assembly_applying-security-policies.adoc[leveloffset=+1]

include::assemblies/assembly_troubleshooting-for-upgrading-from-rhel-8-to-rhel9.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_related-information.adoc[leveloffset=+1]


//Appendix
include::modules/upgrades-and-differences/ref_rhel-8-repositories.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_rhel-9-repositories.adoc[leveloffset=+1]
