include::common-content/_attributes.adoc[]

include::_title-attributes.adoc[]

[id="{ProjectNameID}"]
= {ProjectName}

:context: {ProjectNameID}

:edge-guide:

// include::common-content/beta.adoc[leveloffset=+1]

include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_introducing-rhel-for-edge-images.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_setting-up-image-builder.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_managing-repositories.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_composing-rhel-for-edge-images-using-image-builder-in-rhel-web-console.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_composing-a-rhel-for-edge-image-using-image-builder-command-line.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_building-and-provisioning-simplified-installer-images.adoc[leveloffset=+1]

include::assemblies/assembly_building-and-provisioning-a-minimal-raw-image.adoc[leveloffset=+1]

include::assemblies/assembly_using-the-ignition-tool-for-the-rhel-for-edge-simplified-installer-images.adoc[leveloffset=+1]

include::assemblies/assembly_creating-vmdk-images-for-rhel-for-edge.adoc[leveloffset=+1]

include::assemblies/assembly_creating-rhel-for-edge-ami-images.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_automatically-provisioning-and-onboarding-rhel-for-edge-devices.adoc[leveloffset=+1]

include::assemblies/assembly_using-fdo-to-onboard-rhel-for-edge-devices-with-a-database-backend.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-rpm-ostree-images.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_deploying-a-non-network-rhel-for-edge-image.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_managing-rhel-for-edge-images.adoc[leveloffset=+1]

include::assemblies/assembly_creating-and-managing-ostree-image-updates.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_edge-terminology-and-commands.adoc[leveloffset=+1]
