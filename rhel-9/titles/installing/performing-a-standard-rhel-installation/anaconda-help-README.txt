The built-in help for anaconda (the installer) is generated from a fraction of the standard installation guide. The following lists the bits needed to achieve this:

An additional title is present in this directory. The title is not meant to be build with ccutil, but bare asciidoctor only, so it ignores the rest of files. The file that "implements" this title is anaconda-master.adoc. To build a local preview of this title: asciidoctor anaconda-master.adoc

This help title is built in DocBook 5.0 format and post-processed by a python script. This script is located in the scripts directory in the top of the repository, scripts/prepare_anaconda_help_content.py To run the build, navigate to the scripts directory and run the script as archived/prepare_anaconda_help_content.py The output is saved as file anaconda-help.xml, which can be loaded by yelp, the gnome help viewer used by anaconda.

The results of the build are copied to the scripts directory:
- anaconda-help.xml - built and processed docbook
- anaconda-help-anchors.json - copied from the title directory

When users press the help button in anaconda, the DocBook file is opened at a location that corresponds to the currently visible screen of the installer. This is configured with the file anaconda-help-anchors.json which maps "spoke ids" to anchors (ids) in the document.

When you want to update the help contents:

- Ensure the asciidoctor "title" builds correctly. Test this with the normal html output. If it does not work, fix the build.
- Check if the json file lists up to date anchor ids. All of them must point to a real destination in the title.
- Check that the scripts works as intended. If not, check which of the assumptions changed, and fix it.
- Once all of that works, commit, push, and tag this as anaconda-user-help-8.x.y (also do not forget to git push --tags). You should probably have x as the minor release number, and y as an incremental counter in case something does not work and you have to tag again.
- Notify the packager for the anaconda-user-help component (as of now mkolman), and send them the last tag name.

