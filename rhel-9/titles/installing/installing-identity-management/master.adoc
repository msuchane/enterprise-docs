// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:installing-identity-management:

// include::common-content/beta.adoc[leveloffset=+1]

include::rhel-8/common-content/making-open-source-more-inclusive-idm.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

// Examples from template:
// RHEL 9 assembly
//include::assemblies/assembly_mixing-unrelated-items.adoc[leveloffset=+1]
// RHEL 9 module
//include::modules/_examples/con_the-metamorphosis.adoc[leveloffset=+1]

[id=installing-idm]

// Docs for installing servers
include::assemblies/assembly_preparing-the-system-for-ipa-server-installation.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-ipa-server-with-integrated-dns.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-ipa-server-with-external-ca.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-ipa-server-without-a-ca.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-ipa-server-without-integrated-dns.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-ipa-server-without-dns-with-external-ca.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_installing-an-idm-server-or-replica-with-custom-database-settings-from-an-ldif-file.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_troubleshooting-idm-server-installation.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_uninstalling-an-ipa-server.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_renaming-an-idm-server.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_update-downgrade-ipa.adoc[leveloffset=+1]

// Docs for installing a client
include::assemblies/assembly_preparing-the-system-for-ipa-client-installation.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-idm-client.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-ipa-client-with-kickstart.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_troubleshooting-idm-client-installation.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_re-enrolling-an-ipa-client.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_uninstalling-an-ipa-client.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_renaming-ipa-client-systems.adoc[leveloffset=+1]

// Docs for installing a replica
include::assemblies/assembly_preparing-the-system-for-ipa-replica-installation.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-ipa-replica-on-a-client.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_troubleshooting-idm-replica-installation.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_uninstalling-an-idm-replica.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_managing-replication-topology.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-and-running-the-ipa-healthcheck-tool.adoc[leveloffset=+1]

include::assemblies/assembly_installing-an-Identity-Management-server-using-an-Ansible-playbook.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-Identity-Management-replica-using-an-Ansible-playbook.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-an-Identity-Management-client-using-an-Ansible-playbook.adoc[leveloffset=+1]

//docs for installing individual IdM services post-postinstallation
include::rhel-8/modules/identity-management/proc_installing-dns-on-an-existing-idm-server.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_uninstalling-the-integrated-idm-dns-service-from-an-idm-server.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_adding-the-idm-ca-service-to-an-idm-server-in-a-deployment-without-a-ca.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_adding-the-idm-ca-service-to-an-idm-server-in-a-deployment-with-a-ca.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/proc_uninstalling-the-idm-ca-service-from-an-idm-server.adoc[leveloffset=+1]