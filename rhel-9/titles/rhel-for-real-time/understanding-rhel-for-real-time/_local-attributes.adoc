// Show the table of contents
:toc:
// The name of the title
:ProjectName: Understanding RHEL for Real Time
// The subtitle of the title
:Subtitle: An introduction to RHEL for Real Time kernel
// The abstract of the title
:Abstract: Understand the fundamental concepts and associated references on tuning the RHEL for Real Time kernel to maintain low latency and a consistent response time on latency sensitive applications.
// The name of the title for the purposes of {context}
:ProjectNameID: understanding-RHEL-for-Real-Time-core-concepts

// The following are not required
:ProjectVersion: 0.1

:RT: Red Hat Enterprise Linux for Real Time
