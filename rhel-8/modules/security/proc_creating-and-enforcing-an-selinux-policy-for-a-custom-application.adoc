:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_writing-a-custom-selinux-policy.adoc

[id="creating-and-enforcing-an-selinux-policy-for-a-custom-application_{context}"]
= Creating and enforcing an SELinux policy for a custom application

You can confine applications by SELinux to increase the security of host systems and users' data. Because each application has specific requirements, modify this example procedure for creating an SELinux policy that confines a simple daemon according to your use case.

.Prerequisites

* The `selinux-policy-devel` package and its dependencies are installed on your system.

.Procedure

. For this example procedure, prepare a simple daemon that opens the `/var/log/messages` file for writing:
.. Create a new file, and open it in a text editor of your choice:
+
[subs="quotes"]
----
$ *vi mydaemon.c*
----
.. Insert the following code:
+
[source,c]
----
#include <unistd.h>
#include <stdio.h>

FILE *f;

int main(void)
{
while(1) {
f = fopen("/var/log/messages","w");
        sleep(5);
        fclose(f);
    }
}
----
.. Compile the file:
+
[subs="quotes"]
----
$ *gcc -o mydaemon mydaemon.c*
----
.. Create a `systemd` unit file for your daemon:
+
[subs="quotes"]
----
$ *vi mydaemon.service*
[Unit]
Description=Simple testing daemon

[Service]
Type=simple
ExecStart=/usr/local/bin/mydaemon

[Install]
WantedBy=multi-user.target
----
.. Install and start the daemon:
+
[subs="quotes",options="nowrap",role=white-space-pre]
----
# *cp mydaemon /usr/local/bin/*
# *cp mydaemon.service /usr/lib/systemd/system*
# *systemctl start mydaemon*
# *systemctl status mydaemon*
● mydaemon.service - Simple testing daemon
   Loaded: loaded (/usr/lib/systemd/system/mydaemon.service; disabled; vendor preset: disabled)
   Active: active (running) since Sat 2020-05-23 16:56:01 CEST; 19s ago
 Main PID: 4117 (mydaemon)
    Tasks: 1
   Memory: 148.0K
   CGroup: /system.slice/mydaemon.service
           └─4117 /usr/local/bin/mydaemon

May 23 16:56:01 localhost.localdomain systemd[1]: Started Simple testing daemon.
----

.. Check that the new daemon is not confined by SELinux:
+
[subs="quotes",options="nowrap",role=white-space-pre]
----
$ *ps -efZ | grep mydaemon*
system_u:system_r:unconfined_service_t:s0 root 4117    1  0 16:56 ?        00:00:00 /usr/local/bin/mydaemon
----
. Generate a custom policy for the daemon:
+
[subs="quotes"]
----
$ *sepolicy generate --init /usr/local/bin/mydaemon*
Created the following files:
/home/example.user/mysepol/mydaemon.te # Type Enforcement file
/home/example.user/mysepol/mydaemon.if # Interface file
/home/example.user/mysepol/mydaemon.fc # File Contexts file
/home/example.user/mysepol/mydaemon_selinux.spec # Spec file
/home/example.user/mysepol/mydaemon.sh # Setup Script
----
. Rebuild the system policy with the new policy module using the setup script created by the previous command:
+
[subs="quotes"]
----
# *./mydaemon.sh*
Building and Loading Policy
+ make -f /usr/share/selinux/devel/Makefile mydaemon.pp
Compiling targeted mydaemon module
Creating targeted mydaemon.pp policy package
rm tmp/mydaemon.mod.fc tmp/mydaemon.mod
+ /usr/sbin/semodule -i mydaemon.pp
...
----
+
Note that the setup script relabels the corresponding part of the file system using the `restorecon` command:
+
----
restorecon -v /usr/local/bin/mydaemon /usr/lib/systemd/system
----
. Restart the daemon, and check that it now runs confined by SELinux:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *systemctl restart mydaemon*
$ *ps -efZ | grep mydaemon*
system_u:system_r:mydaemon_t:s0 root        8150       1  0 17:18 ?        00:00:00 /usr/local/bin/mydaemon
----
. Because the daemon is now confined by SELinux, SELinux also prevents it from accessing `/var/log/messages`. Display the corresponding denial message:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *ausearch -m AVC -ts recent*
...
type=AVC msg=audit(1590247112.719:5935): avc:  denied  { open } for  pid=8150 comm="mydaemon" path="/var/log/messages" dev="dm-0" ino=2430831 scontext=system_u:system_r:mydaemon_t:s0 tcontext=unconfined_u:object_r:var_log_t:s0 tclass=file permissive=1
...
----
. You can get additional information also using the `sealert` tool:
+
[subs="quotes,macros",options="nowrap",role=white-space-pre]
----
$ *sealert -l "pass:[*]"*
SELinux is preventing mydaemon from open access on the file /var/log/messages.

pass:[*****  Plugin catchall (100. confidence) suggests   **************************]

If you believe that mydaemon should be allowed open access on the messages file by default.
Then you should report this as a bug.
You can generate a local policy module to allow this access.
Do allow this access for now by executing:
# ausearch -c 'mydaemon' --raw | audit2allow -M my-mydaemon
# semodule -X 300 -i my-mydaemon.pp

Additional Information:
Source Context                system_u:system_r:mydaemon_t:s0
Target Context                unconfined_u:object_r:var_log_t:s0
Target Objects                /var/log/messages [ file ]
Source                        mydaemon
…
----
. Use the `audit2allow` tool to suggest changes:
+
[subs="quotes,attributes"]
----
$ *ausearch -m AVC -ts recent | audit2allow -R*

require {
	type mydaemon_t;
}

#============= mydaemon_t ==============
logging_write_generic_logs(mydaemon_t)
----
. Because rules suggested by `audit2allow` can be incorrect for certain cases, use only a part of its output to find the corresponding policy interface. Inspect the `logging_write_generic_logs(mydaemon_t)` macro with the `macro-expander` tool, to see all allow rules the macro provides:
+
[subs="quotes"]
----
$ *macro-expander "logging_write_generic_logs(mydaemon_t)"*
allow mydaemon_t var_t:dir { getattr search open };
allow mydaemon_t var_log_t:dir { getattr search open read lock ioctl };
allow mydaemon_t var_log_t:dir { getattr search open };
allow mydaemon_t var_log_t:file { open { getattr write append lock ioctl } };
allow mydaemon_t var_log_t:dir { getattr search open };
allow mydaemon_t var_log_t:lnk_file { getattr read };
----
. In this case, you can use the suggested interface, because it only provides read and write access to log files and their parent directories. Add the corresponding rule to your type enforcement file:
+
[subs="quotes,attributes"]
----
$ *echo "logging_write_generic_logs(mydaemon_t)" >> mydaemon.te*
----
+
Alternatively, you can add this rule instead of using the interface:
+
[subs="quotes,attributes"]
----
$ *echo "allow mydaemon_t var_log_t:file { open write getattr };" >> mydaemon.te*
----
. Reinstall the policy:
+
[subs="quotes,attributes"]
----
# *./mydaemon.sh*
Building and Loading Policy
+ make -f /usr/share/selinux/devel/Makefile mydaemon.pp
Compiling targeted mydaemon module
Creating targeted mydaemon.pp policy package
rm tmp/mydaemon.mod.fc tmp/mydaemon.mod
+ /usr/sbin/semodule -i mydaemon.pp
...
----

.Verification

. Check that your application runs confined by SELinux, for example:
+
[subs="quotes,attributes"]
----
$ *ps -efZ | grep mydaemon*
system_u:system_r:mydaemon_t:s0 root        8150       1  0 17:18 ?        00:00:00 /usr/local/bin/mydaemon
----
. Verify that your custom application does not cause any SELinux denials:
+
[subs="quotes,attributes"]
----
# *ausearch -m AVC -ts recent*
&lt;no matches&gt;
----

[role="_additional-resources"]
.Additional resources
* `sepolgen(8)`, `ausearch(8)`, `audit2allow(1)`, `audit2why(1)`, `sealert(8)`, and `restorecon(8)` man pages
* link:https://access.redhat.com/articles/6999267[Quick start to write a custom SELinux policy] Knowledgebase article
