:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="proc_configuring-openssh-clients-using-the-ssh-system-role_{context}"]
= Configuring OpenSSH clients by using the `ssh` RHEL system role
You can use the `ssh` system role to configure multiple SSH clients by running an Ansible playbook.

[NOTE]
====
You can use the `ssh` system role with other system roles that change SSH and SSHD configuration, for example the Identity Management {RHELSystemRoles}s. To prevent the configuration from being overwritten, make sure that the `ssh` role uses a drop-in directory (default in {ProductShortName} 8 and later).
====


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: SSH client configuration
  hosts: managed-node-01.example.com
  tasks:
    - name: "Configure ssh clients"
      ansible.builtin.include_role:
        name: rhel-system-roles.ssh
      vars:
        ssh_user: root
        ssh:
          Compression: true
          GSSAPIAuthentication: no
          ControlMaster: auto
          ControlPath: ~/.ssh/.cm%C
          Host:
            - Condition: example
              Hostname: server.example.com
              User: user1
        ssh_ForwardX11: no
....
+
This playbook configures the `root` user's SSH client preferences on the managed nodes with the following configurations:
+
* Compression is enabled.
* ControlMaster multiplexing is set to `auto`.
* The `example` alias for connecting to the `server.example.com` host is `user1`.
* The `example` host alias is created, which represents a connection to the `server.example.com` host the with the `user1` user name.
* X11 forwarding is disabled.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification
* Verify that the managed node has the correct configuration by displaying the SSH configuration file:
+
[subs="+quotes"]
....
# *cat ~/root/.ssh/config*
# Ansible managed
Compression yes
ControlMaster auto
ControlPath ~/.ssh/.cm%C
ForwardX11 no
GSSAPIAuthentication no
Host example
  Hostname example.com
  User user1
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.ssh/README.md` file
* `/usr/share/doc/rhel-system-roles/ssh/` directory

