:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_protecting-systems-against-intrusive-usb-devices.adoc

[id="logging-usbguard-authorization-events-to-the-linux-audit-log_{context}"]
= Logging USBguard authorization events to the Linux Audit log

[role="_abstract"]
Use the following steps to integrate logging of USBguard authorization events to the standard Linux Audit log. By default, the `usbguard` daemon logs events to the `/var/log/usbguard/usbguard-audit.log` file.

.Prerequisites

* The `usbguard` service is installed and running.
* The `auditd` service is running.

.Procedure

. Edit the `usbguard-daemon.conf` file with a text editor of your choice:
[subs="quotes,attributes"]
+
----
# *vi /etc/usbguard/usbguard-daemon.conf*
----
. Change the `AuditBackend` option from `FileAudit` to `LinuxAudit`:
+
----
AuditBackend=LinuxAudit
----
. Restart the `usbguard` daemon to apply the configuration change:
+
[subs="quotes,attributes"]
----
# *systemctl restart usbguard*
----

.Verification

. Query the `audit` daemon log for a USB authorization event, for example:
+
[subs="quotes,attributes"]
----
# *ausearch -ts recent -m USER_DEVICE*
----

[role="_additional-resources"]
.Additional resources
* `usbguard-daemon.conf(5)` man page.
