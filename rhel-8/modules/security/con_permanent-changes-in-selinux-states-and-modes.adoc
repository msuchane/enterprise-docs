:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// assembly_changing-selinux-states-and-modes.adoc

[id="changing-selinux-modes_{context}"]
= Permanent changes in SELinux states and modes

[role="_abstract"]
As discussed in
ifdef::using-selinux[]
xref:selinux-states-and-modes_getting-started-with-selinux[SELinux states and modes],
endif::[]
ifndef::using-selinux[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/getting-started-with-selinux_using-selinux#selinux-states-and-modes_getting-started-with-selinux[SELinux states and modes],
endif::[]
SELinux can be enabled or disabled. When enabled, SELinux has two modes: enforcing and permissive.

Use the [command]`getenforce` or [command]`sestatus` commands to check in which mode SELinux is running. The [command]`getenforce` command returns `Enforcing`, `Permissive`, or `Disabled`.

The [command]`sestatus` command returns the SELinux status and the SELinux policy being used:
[subs="quotes,attributes"]
----
$ *sestatus*
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      31
----

[WARNING]
====
When systems run SELinux in permissive mode, users and processes might label various file-system objects incorrectly. File-system objects created while SELinux is disabled are not labeled at all. This behavior causes problems when changing to enforcing mode because SELinux relies on correct labels of file-system objects.

To prevent incorrectly labeled and unlabeled files from causing problems, SELinux automatically relabels file systems when changing from the disabled state to permissive or enforcing mode. Use the [command]`fixfiles -F onboot` command as root to create the [filename]`/.autorelabel` file containing the [option]`-F` option to ensure that files are relabeled upon next reboot.

Before rebooting the system for relabeling, make sure the system will boot in permissive mode, for example by using the `enforcing=0` kernel option. This prevents the system from failing to boot in case the system contains unlabeled files required by `systemd` before launching the `selinux-autorelabel` service. For more information, see link:https://bugzilla.redhat.com/show_bug.cgi?id=2021835[RHBZ#2021835].
====
