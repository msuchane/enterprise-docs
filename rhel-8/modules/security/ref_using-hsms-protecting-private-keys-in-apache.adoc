:_mod-docs-content-type: REFERENCE
:experimental:
// included in configuring-applications-to-use-cryptographic-hardware-through-pkcs-11

[id="ref_using-hsms-protecting-private-keys-in-apache_{context}"]
= Using HSMs protecting private keys in Apache

[role="_abstract"]
The `Apache` HTTP server can work with private keys stored on hardware security modules (HSMs), which helps to prevent the keys' disclosure and man-in-the-middle attacks. Note that this usually requires high-performance HSMs for busy servers.

For secure communication in the form of the HTTPS protocol, the `Apache` HTTP server (`httpd`) uses the OpenSSL library. OpenSSL does not support PKCS #11 natively. To use HSMs, you have to install the [package]`openssl-pkcs11` package, which provides access to PKCS #11 modules through the engine interface. You can use a PKCS #11 URI instead of a regular file name to specify a server key and a certificate in the `/etc/httpd/conf.d/ssl.conf` configuration file, for example:
----
SSLCertificateFile    "pkcs11:id=%01;token=softhsm;type=cert"
SSLCertificateKeyFile "pkcs11:id=%01;token=softhsm;type=private?pin-value=111111"
----
Install the [package]`httpd-manual` package to obtain complete documentation for the `Apache` HTTP Server, including TLS configuration. The directives available in the `/etc/httpd/conf.d/ssl.conf` configuration file are described in detail in the `/usr/share/httpd/manual/mod/mod_ssl.html` file.
