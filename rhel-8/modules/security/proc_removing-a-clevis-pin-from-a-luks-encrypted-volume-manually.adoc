:_mod-docs-content-type: PROCEDURE
:experimental:

[id="removing-a-clevis-pin-from-a-luks-encrypted-volume-manually_{context}"]
= Removing a Clevis pin from a LUKS-encrypted volume manually

[role="_abstract"]
Use the following procedure for manual removing the metadata created by the `clevis luks bind` command and also for wiping a key slot that contains passphrase added by Clevis.

[IMPORTANT]
====
The recommended way to remove a Clevis pin from a LUKS-encrypted volume is through the `clevis luks unbind` command. The removal procedure using `clevis luks unbind` consists of only one step and works for both LUKS1 and LUKS2 volumes. The following example command removes the metadata created by the binding step and wipe the key slot `_1_` on the `_/dev/sda2_` device:

[subs="quotes,attributes"]
----
# *clevis luks unbind -d /dev/sda2 -s 1*
----
====

.Prerequisites

* A LUKS-encrypted volume with a Clevis binding.

.Procedure

. Check which LUKS version the volume, for example `_/dev/sda2_`, is encrypted by and identify a slot and a token that is bound to Clevis:
+
[subs="quotes,attributes"]
----
# *cryptsetup luksDump _/dev/sda2_*
LUKS header information
Version:        2
...
Keyslots:
  0: luks2
...
1: luks2
      Key:        512 bits
      Priority:   normal
      Cipher:     aes-xts-plain64
...
      Tokens:
        0: clevis
              Keyslot:  1
...
----
+
In the previous example, the Clevis token is identified by `_0_` and the associated key slot is `_1_`.
. In case of LUKS2 encryption, remove the token:
+
[subs="quotes,attributes"]
----
# *cryptsetup token remove --token-id _0_ _/dev/sda2_*
----
. If your device is encrypted by LUKS1, which is indicated by the `Version:        1` string in the output of the `cryptsetup luksDump` command, perform this additional step with the `luksmeta wipe` command:
+
[subs="quotes,attributes"]
----
# *luksmeta wipe -d _/dev/sda2_ -s _1_*
----
. Wipe the key slot containing the Clevis passphrase:
+
[subs="quotes,attributes"]
----
# *cryptsetup luksKillSlot _/dev/sda2_ _1_*
----

[role="_additional-resources"]
.Additional resources
* `clevis-luks-unbind(1)`, `cryptsetup(8)`, and `luksmeta(8)` man pages
