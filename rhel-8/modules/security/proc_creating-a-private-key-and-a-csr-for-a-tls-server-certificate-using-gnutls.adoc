:_mod-docs-content-type: PROCEDURE

[id="creating-a-private-key-and-a-csr-for-a-tls-server-certificate-using-gnutls_{context}"]
= Creating a private key and a CSR for a TLS server certificate using GnuTLS

To obtain the certificate, you must create a private key and a certificate signing request (CSR) for your server first.

.Procedure

. Generate a private key on your server system, for example:
+
[subs="quotes"]
----
$ *certtool --generate-privkey --sec-param High --outfile _&lt;example-server.key&gt;_*
----

. Optional: Use a text editor of your choice to prepare a configuration file that simplifies creating your CSR, for example:
+
[subs="quotes"]
----
$ *vim _&lt;example_server.cnf&gt;_*
signing_key
encryption_key
key_agreement

tls_www_server

country = "US"
organization = "Example Organization"
cn = "server.example.com"

dns_name = "example.com"
dns_name = "server.example.com"
ip_address = "192.168.0.1"
ip_address = "::1"
ip_address = "127.0.0.1"
----

. Create a CSR using the private key you created previously:
+
[subs="quotes"]
----
$ *certtool --generate-request --template _&lt;example-server.cfg&gt;_ --load-privkey _&lt;example-server.key&gt;_ --outfile _&lt;example-server.crq&gt;_*
----
+
If you omit the `--template` option, the `certool` utility prompts you for additional information, for example:
+
[subs="quotes"]
----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Generating a PKCS #10 certificate request...
Country name (2 chars): _&lt;US&gt;_
State or province name: _&lt;Washington&gt;_
Locality name: _&lt;Seattle&gt;_
Organization name: _&lt;Example Organization&gt;_
Organizational unit name:
Common name: _&lt;server.example.com&gt;_
----

.Next steps

* Submit the CSR to a CA of your choice for signing. Alternatively, for an internal use scenario within a trusted network, use your private CA for signing. See
ifdef::securing-networks[]
xref:using-a-private-ca-to-issue-certificates-for-csrs-with-gnutls_creating-and-managing-tls-keys-and-certificates[]
endif::[]
ifndef::securing-networks[]
the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using-a-private-ca-to-issue-certificates-for-csrs-with-gnutls_creating-and-managing-tls-keys-and-certificates[Using a private CA to issue certificates for CSRs with GnuTLS] section
endif::[]
for more information. 

.Verification

. After you obtain the requested certificate from the CA, check that the human-readable parts of the certificate match your requirements, for example:
+
[subs="quotes"]
----
$ *certtool --certificate-info --infile _&lt;example-server.crt&gt;_*
Certificate:
…
        Issuer: CN = Example CA
        Validity
            Not Before: Feb  2 20:27:29 2023 GMT
            Not After : Feb  2 20:27:29 2024 GMT
        Subject: C = US, O = Example Organization, CN = server.example.com
        Subject Public Key Info:
            Public Key Algorithm: id-ecPublicKey
                Public-Key: (256 bit)
…
        X509v3 extensions:
            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment, Key Agreement
            X509v3 Extended Key Usage:
                TLS Web Server Authentication
            X509v3 Subject Alternative Name:
                DNS:example.com, DNS:server.example.com, IP Address:192.168.0.1, IP
…

----

[role="_additional-resources"]
.Additional resources
* `certtool(1)` man page

