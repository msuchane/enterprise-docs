:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_configuring-a-vpn-with-ipsec.adoc

[id="deploying-a-fips-compliant-ipsec-vpn_{context}"]
= Deploying a FIPS-compliant IPsec VPN

[role="_abstract"]
Use this procedure to deploy a FIPS-compliant IPsec VPN solution based on Libreswan. The following steps also enable you to identify which cryptographic algorithms are available and which are disabled for Libreswan in FIPS mode.

.Prerequisites

* The `AppStream` repository is enabled.

.Procedure

. Install the [package]`libreswan` packages:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install libreswan*
----

. If you are re-installing Libreswan, remove its old NSS database:
+
[subs="quotes,attributes"]
----
# *systemctl stop ipsec*
ifeval::[{ProductNumber} == 8]
# *rm /etc/ipsec.d/*db*
endif::[]

ifeval::[{ProductNumber} == 9]
# *rm /var/lib/ipsec/nss/*db*
endif::[]
----

. Start the `ipsec` service, and enable the service to be started automatically on boot:
+
[subs="quotes,attributes"]
----
# *systemctl enable ipsec --now*
----
. Configure the firewall to allow 500 and 4500/UDP ports for the IKE, ESP, and AH protocols by adding the `ipsec` service:
+
[subs="quotes,attributes"]
----
# *firewall-cmd --add-service="ipsec"*
# *firewall-cmd --runtime-to-permanent*
----
. Switch the system to FIPS mode:
+
[subs="quotes,attributes"]
----
# *fips-mode-setup --enable*
----
. Restart your system to allow the kernel to switch to FIPS mode:
+
[subs="quotes,attributes"]
----
# *reboot*
----

.Verification

. To confirm Libreswan is running in FIPS mode:
+
[subs="quotes,attributes"]
----
# *ipsec whack --fipsstatus*
000 FIPS mode enabled
----
. Alternatively, check entries for the `ipsec` unit in the `systemd` journal:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *journalctl -u ipsec*
...
ifeval::[{ProductNumber} == 8]
Jan 22 11:26:50 localhost.localdomain pluto[3076]: FIPS Product: YES
Jan 22 11:26:50 localhost.localdomain pluto[3076]: FIPS Kernel: YES
endif::[]
Jan 22 11:26:50 localhost.localdomain pluto[3076]: FIPS Mode: YES
----
. To see the available algorithms in FIPS mode:
+
[subs="quotes,attributes"]
----
ifeval::[{ProductNumber} == 8]
# *ipsec pluto --selftest 2>&amp;1 | head -11*
FIPS Product: YES
FIPS Kernel: YES
FIPS Mode: YES
NSS DB directory: sql:/etc/ipsec.d
Initializing NSS
Opening NSS database "sql:/etc/ipsec.d" read-only
NSS initialized
NSS crypto library initialized
FIPS HMAC integrity support [enabled]
FIPS mode enabled for pluto daemon
NSS library is running in FIPS mode
FIPS HMAC integrity verification self-test passed
endif::[]
ifeval::[{ProductNumber} == 9]
# *ipsec pluto --selftest 2>&amp;1 | head -6*
Initializing NSS using read-write database "sql:/var/lib/ipsec/nss"
FIPS Mode: YES
NSS crypto library initialized
FIPS mode enabled for pluto daemon
NSS library is running in FIPS mode
FIPS HMAC integrity support [disabled]
endif::[]
----
. To query disabled algorithms in FIPS mode:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *ipsec pluto --selftest 2>&amp;1 | grep disabled*
ifeval::[{ProductNumber} == 8]
Encryption algorithm CAMELLIA_CTR disabled; not FIPS compliant
Encryption algorithm CAMELLIA_CBC disabled; not FIPS compliant
Encryption algorithm SERPENT_CBC disabled; not FIPS compliant
Encryption algorithm TWOFISH_CBC disabled; not FIPS compliant
Encryption algorithm TWOFISH_SSH disabled; not FIPS compliant
Encryption algorithm NULL disabled; not FIPS compliant
Encryption algorithm CHACHA20_POLY1305 disabled; not FIPS compliant
Hash algorithm MD5 disabled; not FIPS compliant
PRF algorithm HMAC_MD5 disabled; not FIPS compliant
PRF algorithm AES_XCBC disabled; not FIPS compliant
Integrity algorithm HMAC_MD5_96 disabled; not FIPS compliant
Integrity algorithm HMAC_SHA2_256_TRUNCBUG disabled; not FIPS compliant
Integrity algorithm AES_XCBC_96 disabled; not FIPS compliant
DH algorithm MODP1024 disabled; not FIPS compliant
DH algorithm MODP1536 disabled; not FIPS compliant
DH algorithm DH31 disabled; not FIPS compliant
endif::[]
ifeval::[{ProductNumber} == 9]
Encryption algorithm CAMELLIA_CTR disabled; not FIPS compliant
Encryption algorithm CAMELLIA_CBC disabled; not FIPS compliant
Encryption algorithm NULL disabled; not FIPS compliant
Encryption algorithm CHACHA20_POLY1305 disabled; not FIPS compliant
Hash algorithm MD5 disabled; not FIPS compliant
PRF algorithm HMAC_MD5 disabled; not FIPS compliant
PRF algorithm AES_XCBC disabled; not FIPS compliant
Integrity algorithm HMAC_MD5_96 disabled; not FIPS compliant
Integrity algorithm HMAC_SHA2_256_TRUNCBUG disabled; not FIPS compliant
Integrity algorithm AES_XCBC_96 disabled; not FIPS compliant
DH algorithm MODP1536 disabled; not FIPS compliant
DH algorithm DH31 disabled; not FIPS compliant
endif::[]
----
. To list all allowed algorithms and ciphers in FIPS mode:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *ipsec pluto --selftest 2>&amp;1 | grep ESP | grep FIPS | sed "s/^.*FIPS//"*
ifeval::[{ProductNumber} == 8]
{256,192,*128}  aes_ccm, aes_ccm_c
{256,192,*128}  aes_ccm_b
{256,192,*128}  aes_ccm_a
[*192]  3des
{256,192,*128}  aes_gcm, aes_gcm_c
{256,192,*128}  aes_gcm_b
{256,192,*128}  aes_gcm_a
{256,192,*128}  aesctr
{256,192,*128}  aes
{256,192,*128}  aes_gmac
sha, sha1, sha1_96, hmac_sha1
sha512, sha2_512, sha2_512_256, hmac_sha2_512
sha384, sha2_384, sha2_384_192, hmac_sha2_384
sha2, sha256, sha2_256, sha2_256_128, hmac_sha2_256
aes_cmac
null
null, dh0
dh14
dh15
dh16
dh17
dh18
ecp_256, ecp256
ecp_384, ecp384
ecp_521, ecp521
endif::[]
ifeval::[{ProductNumber} == 9]
aes_ccm, aes_ccm_c
aes_ccm_b
aes_ccm_a
NSS(CBC)  3des
NSS(GCM)  aes_gcm, aes_gcm_c
NSS(GCM)  aes_gcm_b
NSS(GCM)  aes_gcm_a
NSS(CTR)  aesctr
NSS(CBC)  aes
aes_gmac
NSS       sha, sha1, sha1_96, hmac_sha1
NSS       sha512, sha2_512, sha2_512_256, hmac_sha2_512
NSS       sha384, sha2_384, sha2_384_192, hmac_sha2_384
NSS       sha2, sha256, sha2_256, sha2_256_128, hmac_sha2_256
aes_cmac
null
NSS(MODP) null, dh0
NSS(MODP) dh14
NSS(MODP) dh15
NSS(MODP) dh16
NSS(MODP) dh17
NSS(MODP) dh18
NSS(ECP)  ecp_256, ecp256
NSS(ECP)  ecp_384, ecp384
NSS(ECP)  ecp_521, ecp521
endif::[]
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[Using system-wide cryptographic policies].
