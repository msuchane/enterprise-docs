:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// assembly_using-secure-communications-between-two-systems-with-openssh.adoc


[id="connecting-to-remote-machines-with-ssh-keys-using-ssh-agent_{context}"]
= Connecting to remote machines with SSH keys using ssh-agent

[role="_abstract"]
To avoid entering a passphrase each time you initiate an SSH connection, you can use the [systemitem]`ssh-agent` utility to cache the private SSH key. The private key and the passphrase remain secure.

.Prerequisites

* You have a remote host with SSH daemon running and reachable through the network.
* You know the IP address or hostname and credentials to log in to the remote host.
* You have generated an SSH key pair with a passphrase and transferred the public key to the remote machine.
////
ifdef::basic-system-settings[]
For more information, see xref:generating-ssh-key-pairs_using-secure-communications-between-two-systems-with-openssh[Generating SSH key pairs].
endif::[]
////
ifdef::basic-system-settings[]
For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/assembly_using-secure-communications-between-two-systems-with-openssh_securing-networks#generating-ssh-key-pairs_assembly_using-secure-communications-between-two-systems-with-openssh[Generating SSH key pairs].
endif::[]


.Procedure

. Optional: Verify you can use the key to authenticate to the remote host:
.. Connect to the remote host using SSH:
+
[subs="quotes,attributes"]
----
$ *ssh _example.user1_@_198.51.100.1_ _hostname_*
----

.. Enter the passphrase you set while creating the key to grant access to the private key.
+
[subs="quotes,attributes"]
----
$ *ssh _example.user1_@_198.51.100.1_ _hostname_*
 host.example.com
----

. Start the `ssh-agent`.
+
[subs="quotes,attributes"]
----
$ *eval $(ssh-agent)*
Agent pid 20062
----

. Add the key to `ssh-agent`.
+
[subs="quotes,attributes"]
----
$ *ssh-add ~/.ssh/id_rsa*
Enter passphrase for ~/.ssh/id_rsa:
Identity added: ~/.ssh/id_rsa (_example.user0_@_198.51.100.12_)
----


.Verification

* Optional: Log in to the host machine using SSH.
+
[subs="quotes,attributes"]
----
$ *ssh _example.user1_@_198.51.100.1_*

Last login: Mon Sep 14 12:56:37 2020
----
+
Note that you did not have to enter the passphrase.
