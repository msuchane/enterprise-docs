:_mod-docs-content-type: CONCEPT

[id="confined-non-administrator-roles-in-selinux_{context}"]
= Confined non-administrator roles in SELinux

In SELinux, confined non-administrator roles grant specific sets of privileges and permissions for performing specific tasks to the Linux users assigned to them. By assigning separate confined non-administrator roles, you can assign specific privileges to individual users. This is useful in scenarios with multiple users who each have a different level of authorizations.

You can also customize the permissions of SELinux roles by changing the related SELinux booleans on your system. To see the SELinux booleans and their current state, use the `semanage boolean -l` command as root. You can get more detailed descriptions if you install the `selinux-policy-devel` package.

[subs="quotes,attributes"]
----
# *semanage boolean -l*
SELinux boolean                State  Default Description
...
xguest_connect_network         (on   ,   on)  Allow xguest users to configure Network Manager and connect to apache ports
xguest_exec_content            (on   ,   on)  Allow xguest to exec content
...
----

Linux users in the `user_t`, `guest_t`, and `xguest_t` domains can only run set user ID (`setuid`) applications if SELinux policy permits it (for example, `passwd`). These users cannot run the `setuid` applications `su` and `sudo`, and therefore cannot use these applications to become root.

By default, Linux users in the `staff_t`, `user_t`, `guest_t`, and `xguest_t` domains can execute applications in their home directories and `/tmp`. Applications inherit the permissions of the user that executed them.

To prevent `guest_t`, and `xguest_t` users from executing applications in directories in which they have write access, set the `guest_exec_content` and `xguest_exec_content` booleans to `off`.

SELinux has the following confined non-administrator roles, each with specific privileges and limitations:

`guest_r`::
Has very limited permissions. Users assigned to this role cannot access the network, but can execute files in the `/tmp` and `/home` directories.
+
Related boolean:
+
----
SELinux boolean                State  Default Description
guest_exec_content             (on   ,   on)  Allow guest to exec content
----

`xguest_r`::
Has limited permissions. Users assigned to this role can log into X Window, access web pages by using network browsers, and access media. They can also execute files in the `/tmp` and `/home` directories.
+
Related booleans:
+
----
SELinux boolean                State  Default Description
xguest_connect_network         (on   ,   on)  Allow xguest users to configure Network Manager and connect to apache ports
xguest_exec_content            (on   ,   on)  Allow xguest to exec content
xguest_mount_media             (on   ,   on)  Allow xguest users to mount removable media
xguest_use_bluetooth           (on   ,   on)  Allow xguest to use blue tooth devices
----

`user_r`::
Has non-privileged access with full user permissions. Users assigned to this role can perform most actions that do not require administrative privileges.
+
Related booleans:
+
----
SELinux boolean                State  Default Description
unprivuser_use_svirt           (off  ,  off)  Allow unprivileged user to create and transition to svirt domains.
----

`staff_r`::
Has permissions similar to `user_r` and additional privileges. In particular, users assigned to this role are allowed to run `sudo` to execute administrative commands that are normally reserved for the `root` user. This changes roles and the effective user ID (EUID) but does not change the SELinux user.
+
Related booleans:
+
----
SELinux boolean                State  Default Description
staff_exec_content             (on   ,   on)  Allow staff to exec content
staff_use_svirt                (on   ,   on)  allow staff user to create and transition to svirt domains.
----

[role="_additional-resources"]
.Additional resources

* For more information about `sudo`, see
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/managing-sudo-access_configuring-basic-system-settings[Managing sudo access].

* To map a Linux user to `staff_u` and configure `sudo`, see
ifdef::using-selinux[]
xref:confining-an-administrator-using-sudo-and-the-sysadm_r-role_managing-confined-and-unconfined-users[Confining an administrator using sudo and the sysadm_r role].
endif::[]
ifndef::using-selinux[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/managing-confined-and-unconfined-users_using-selinux#confining-an-administrator-using-sudo-and-the-sysadm_r-role_managing-confined-and-unconfined-users[Confining an administrator using sudo and the sysadm_r role].
endif::[]

* For additional information about each role and the associated types, see the relevant man pages installed with the `selinux-policy-doc` package:

** `guest_selinux(8)`
** `xguest_selinux(8)`
** `user_selinux(8)`
** `staff_selinux(8)`
