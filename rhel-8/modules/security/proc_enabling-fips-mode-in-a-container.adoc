:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
// assembly_using-the-system-wide-cryptographic-policies

[id="enabling-fips-mode-in-a-container_{context}"]
= Enabling FIPS mode in a container

ifeval::[{ProductNumber} == 8]
To enable the full set of cryptographic module self-checks mandated by the Federal Information Processing Standard Publication 140-2 (FIPS mode), the host system kernel must be running in FIPS mode. Depending on the version of your host system, enabling FIPS mode on containers either is fully automatic or requires only one command.
endif::[]
ifeval::[{ProductNumber} == 9]
To enable the full set of cryptographic module self-checks mandated by the Federal Information Processing Standard Publication 140-2 (FIPS mode), the host system kernel must be running in FIPS mode. The `podman` utility automatically enables FIPS mode on supported containers.
endif::[]


[NOTE]
====
The `fips-mode-setup` command does not work correctly in containers, and it cannot be used to enable or check FIPS mode in this scenario.
====

ifeval::[{ProductNumber} == 9]
[NOTE]
====
The cryptographic modules of {ProductShortName} {ProductNumber} are not yet certified for the FIPS 140-3 requirements.
====
endif::[]

.Prerequisites

* The host system must be in FIPS mode.

.Procedure

ifeval::[{ProductNumber} == 8]
* *On hosts running RHEL 8.1 and 8.2*: Set the FIPS cryptographic policy level in the container using the following command, and ignore the advice to use the `fips-mode-setup` command:
+
[subs="quotes"]
----
$ *update-crypto-policies --set FIPS*
----


* *On hosts running RHEL 8.4 and later*: On systems with FIPS mode enabled, the `podman` utility automatically enables FIPS mode on supported containers.
endif::[]
ifeval::[{ProductNumber} == 9]
* On systems with FIPS mode enabled, the `podman` utility automatically enables FIPS mode on supported containers.
endif::[]


[role="_additional-resources"]
.Additional resources
ifdef::security-hardening[]
* xref:switching-the-system-to-fips-mode_using-the-system-wide-cryptographic-policies[Switching the system to FIPS mode].
endif::[]
ifndef::security-hardening[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening#switching-the-system-to-fips-mode_using-the-system-wide-cryptographic-policies[Switching the system to FIPS mode].
endif::[]
ifdef::security-hardening[]
ifeval::[{ProductNumber} == 8]
* xref:assembly_installing-a-rhel-8-system-with-fips-mode-enabled_security-hardening[Installing a RHEL 8 system with FIPS mode enabled]
endif::[]
ifeval::[{ProductNumber} == 9]
* xref:assembly_installing-the-system-in-fips-mode_security-hardening[Installing the system in FIPS mode]
endif::[]
endif::[]
ifndef::security-hardening[]
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/assembly_installing-a-rhel-8-system-with-fips-mode-enabled_security-hardening[Installing a RHEL 8 system with FIPS mode enabled]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/index/assembly_installing-the-system-in-fips-mode_security-hardening[Installing the system in FIPS mode]
endif::[]
endif::[]
