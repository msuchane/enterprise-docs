:_mod-docs-content-type: PROCEDURE
[id="installing-rsyslog-documentation_{context}"]
= Installing Rsyslog documentation

[role="_abstract"]
The Rsyslog application has extensive online documentation that is available at link:https://www.rsyslog.com/doc/[https://www.rsyslog.com/doc/], but you can also install the `rsyslog-doc` documentation package locally.

.Prerequisites
* You have activated the `AppStream` repository on your system.
* You are authorized to install new packages using `sudo`.

.Procedure
* Install the `rsyslog-doc` package:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install rsyslog-doc*
----

.Verification
* Open the `/usr/share/doc/rsyslog/html/index.html` file in a browser of your choice, for example:
+
[subs="quotes,attributes"]
----
$ *firefox /usr/share/doc/rsyslog/html/index.html &amp;*
----
