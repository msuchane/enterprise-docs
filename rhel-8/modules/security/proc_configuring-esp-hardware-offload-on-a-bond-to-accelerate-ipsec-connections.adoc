:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-esp-hardware-offload-on-a-bond-to-accelerate-an-ipsec-connection_{context}"]
= Configuring ESP hardware offload on a bond to accelerate an IPsec connection

[role="_abstract"]
Offloading Encapsulating Security Payload (ESP) to the hardware accelerates IPsec connections. If you use a network bond for fail-over reasons, the requirements and the procedure to configure ESP hardware offload are different from those using a regular Ethernet device. For example, in this scenario, you enable the offload support on the bond, and the kernel applies the settings to the ports of the bond.

.Prerequisites

* All network cards in the bond support ESP hardware offload.

* The network driver supports ESP hardware offload on a bond device. In {ProductShortName}, only the `ixgbe` driver supports this feature.

* The bond is configured and works.

* The bond uses the `active-backup` mode. The bonding driver does not support any other modes for this feature.

* The IPsec connection is configured and works.


.Procedure

. Enable ESP hardware offload support on the network bond:
+
[subs="quotes"]
----
# *nmcli connection modify __bond0__ ethtool.feature-esp-hw-offload on*
----
+
This command enables ESP hardware offload support on the [systemitem]`bond0` connection.

. Reactivate the [systemitem]`bond0` connection:
+
[subs="quotes"]
----
# *nmcli connection up __bond0__*
----


. Edit the Libreswan configuration file in the `/etc/ipsec.d/` directory of the connection that should use ESP hardware offload, and append the `nic-offload=yes` statement to the connection entry:
+
[subs="quotes"]
----
conn __example__
    ...
    *nic-offload=yes*
----

. Restart the `ipsec` service:
+
[subs="quotes"]
----
# *systemctl restart ipsec*
----



.Verification

. Display the active port of the bond:
+
[subs="quotes"]
----
# *grep "Currently Active Slave" /proc/net/bonding/__bond0__*
Currently Active Slave: __enp1s0__
----

. Display the `tx_ipsec` and `rx_ipsec` counters of the active port:
+
[subs="quotes"]
----
# *ethtool -S __enp1s0__ | egrep "_ipsec"*
     tx_ipsec: 10
     rx_ipsec: 10
----

. Send traffic through the IPsec tunnel. For example, ping a remote IP address:
+
[subs="quotes"]
----
# *ping -c 5 __remote_ip_address__*
----

. Display the `tx_ipsec` and `rx_ipsec` counters of the active port again:
+
[subs="quotes"]
----
# *ethtool -S enp1s0 | egrep "_ipsec"*
     tx_ipsec: 15
     rx_ipsec: 15
----
+
If the counter values have increased, ESP hardware offload works.



[role="_additional-resources"]
.Additional resources
ifdef::networking-title[]
* xref:configuring-network-bonding_configuring-and-managing-networking[Configuring network bonding]
endif::[]
ifndef::networking-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/configuring-network-bonding_configuring-and-managing-networking[Configuring network bonding]
endif::[]

ifdef::securing-networks[]
* xref:configuring-a-vpn-with-ipsec_securing-networks[Configuring a VPN with IPsec]
endif::[]

ifndef::securing-networks[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/configuring-a-vpn-with-ipsec_securing-networks[Configuring a VPN with IPsec] section in the Securing networks document
endif::[]
