:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_configuring-manual-enrollment-of-root-volumes-using-clevis.adoc[leveloffset=+1]

[id="configuring-manual-enrollment-of-volumes-using-tpm2_{context}"]
= Configuring manual enrollment of LUKS-encrypted volumes by using a TPM 2.0 policy

[role="_abstract"]
Use the following steps to configure unlocking of LUKS-encrypted volumes by using a Trusted Platform Module 2.0 (TPM 2.0) policy.

.Prerequisites

* An accessible TPM 2.0-compatible device.
* A system with the 64-bit Intel or 64-bit AMD architecture.


.Procedure

. To automatically unlock an existing LUKS-encrypted volume, install the [package]`clevis-luks` subpackage:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install clevis-luks*
----
. Identify the LUKS-encrypted volume for PBD. In the following example, the block device is referred as _/dev/sda2_:
+
[subs="quotes",options="nowrap",role="white-space-pre"]
----
# *lsblk*
NAME                                          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda                                             8:0    0    12G  0 disk
├─sda1                                          8:1    0     1G  0 part  /boot
└─sda2                                          8:2    0    11G  0 part
  └─luks-40e20552-2ade-4954-9d56-565aa7994fb6 253:0    0    11G  0 crypt
    ├─rhel-root                               253:0    0   9.8G  0 lvm   /
    └─rhel-swap                               253:1    0   1.2G  0 lvm   [SWAP]
----
. Bind the volume to a TPM 2.0 device using the [command]`clevis luks bind` command, for example:
+
[subs="quotes",options="nowrap",role="white-space-pre"]
----
# *clevis luks bind -d _/dev/sda2_ tpm2 '{"hash":"sha256","key":"rsa"}'*
...
Do you wish to initialize /dev/sda2? [yn] y
Enter existing LUKS password:
----
+
This command performs four steps:

a. Creates a new key with the same entropy as the LUKS master key.

b. Encrypts the new key with Clevis.

c. Stores the Clevis JWE object in the LUKS2 header token or uses LUKSMeta if the non-default LUKS1 header is used.

d. Enables the new key for use with LUKS.
+
[NOTE]
====
The binding procedure assumes that there is at least one free LUKS password slot. The [command]`clevis luks bind` command takes one of the slots.
====
+
Alternatively, if you want to seal data to specific Platform Configuration Registers (PCR) states, add the `pcr_bank` and `pcr_ids` values to the [command]`clevis luks bind` command, for example:
+
[subs="quotes",options="nowrap",role="white-space-pre"]
----
# *clevis luks bind -d _/dev/sda2_ tpm2 '{"hash":"sha256","key":"rsa","pcr_bank":"sha256","pcr_ids":"0,1"}'*
----
+
[WARNING]
====
Because the data can only be unsealed if PCR hashes values match the policy used when sealing and the hashes can be rewritten, add a strong passphrase that enable you to unlock the encrypted volume manually when a value in a PCR changes.

If the system cannot automatically unlock your encrypted volume after an upgrade of the [package]`shim-x64` package, follow the steps in the link:https://access.redhat.com/solutions/6175492[Clevis TPM2 no longer decrypts LUKS devices after a restart] KCS article.
====
. The volume can now be unlocked with your existing password as well as with the Clevis policy.
+
. To enable the early boot system to process the disk binding, use the `dracut` tool on an already installed system:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install clevis-dracut*
# *dracut -fv --regenerate-all*
----


.Verification

. To verify that the Clevis JWE object is successfully placed in a LUKS header, use the `clevis luks list` command:
+
[subs="quotes,attributes"]
----
# *clevis luks list -d _/dev/sda2_*
1: tpm2 '{"hash":"sha256","key":"rsa"}'
----


[role="_additional-resources"]
.Additional resources
* `clevis-luks-bind(1)`, `clevis-encrypt-tpm2(1)`, and `dracut.cmdline(7)` man pages
