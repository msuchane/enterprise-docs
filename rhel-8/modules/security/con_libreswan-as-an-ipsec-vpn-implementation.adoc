:_mod-docs-content-type: CONCEPT
:experimental:
// included in configuring-a-vpn-with-ipsec

[id="libreswan-as-an-ipsec-vpn-implementation_{context}"]
= Libreswan as an IPsec VPN implementation

[role="_abstract"]
In {ProductShortName}, a Virtual Private Network (VPN) can be configured using the IPsec protocol, which is supported by the Libreswan application. Libreswan is a continuation of the Openswan application, and many examples from the Openswan documentation are interchangeable with Libreswan.

The IPsec protocol for a VPN is configured using the Internet Key Exchange (IKE) protocol. The terms IPsec and IKE are used interchangeably. An IPsec VPN is also called an IKE VPN, IKEv2 VPN, XAUTH VPN, Cisco VPN or IKE/IPsec VPN. A variant of an IPsec VPN that also uses the Layer 2 Tunneling Protocol (L2TP) is usually called an L2TP/IPsec VPN, which requires the `xl2tpd` package provided by the `optional` repository.

Libreswan is an open-source, user-space IKE implementation. IKE v1 and v2 are implemented as a user-level daemon. The IKE protocol is also encrypted. The IPsec protocol is implemented by the Linux kernel, and Libreswan configures the kernel to add and remove VPN tunnel configurations.

The IKE protocol uses UDP port 500 and 4500. The IPsec protocol consists of two protocols:

* Encapsulated Security Payload (ESP), which has protocol number 50.
* Authenticated Header (AH), which has protocol number 51.

The AH protocol is not recommended for use. Users of AH are recommended to migrate to ESP with null encryption.

The IPsec protocol provides two modes of operation:

* Tunnel Mode (the default)
* Transport Mode.

You can configure the kernel with IPsec without IKE. This is called _manual keying_. You can also configure manual keying using the [command]`ip xfrm` commands, however, this is strongly discouraged for security reasons. Libreswan communicates with the Linux kernel using the Netlink interface. The kernel performs packet encryption and decryption.

Libreswan uses the Network Security Services (NSS) cryptographic library. NSS is certified for use with the _Federal Information Processing Standard_ (_FIPS_) Publication 140-2.

[IMPORTANT]
====
IKE/IPsec VPNs, implemented by Libreswan and the Linux kernel, is the only VPN technology recommended for use in {ProductShortName}. Do not use any other VPN technology without understanding the risks of doing so.
====

In {ProductShortName}, Libreswan follows *system-wide cryptographic policies* by default. This ensures that Libreswan uses secure settings for current threat models including IKEv2 as a default protocol. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[Using system-wide crypto policies] for more information.

Libreswan does not use the terms "source" and "destination" or "server" and "client" because IKE/IPsec are peer to peer protocols. Instead, it uses the terms "left" and "right" to refer to end points (the hosts). This also allows you to use the same configuration on both end points in most cases. However, administrators usually choose to always use "left" for the local host and "right" for the remote host.

The [option]`leftid` and [option]`rightid` options serve as identification of the respective hosts in the authentication process. See the `ipsec.conf(5)` man page for more information.
