:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_deploying-systems-that-are-compliant-with-a-security-profile-immediately-after-an-installation.adoc

[id="deploying-baseline-compliant-rhel-systems-using-kickstart_{context}"]
= Deploying baseline-compliant {ProductShortName} systems using Kickstart

[role="_abstract"]
Use this procedure to deploy {ProductShortName} systems that are aligned with a specific baseline. This example uses Protection Profile for General Purpose Operating System (OSPP).

.Prerequisites

* The `scap-security-guide` package is installed on your {ProductShortName}{nbsp}{ProductNumber} system.

.Procedure
ifeval::[{ProductNumber} == 8]
. Open the `/usr/share/scap-security-guide/kickstart/ssg-rhel8-ospp-ks.cfg` Kickstart file in an editor of your choice.
endif::[]

ifeval::[{ProductNumber} == 9]
. Open the `/usr/share/scap-security-guide/kickstart/ssg-rhel9-ospp-ks.cfg` Kickstart file in an editor of your choice.
endif::[]

. Update the partitioning scheme to fit your configuration requirements. For OSPP compliance, the separate partitions for `/boot`, `/home`, `/var`, `/tmp`, `/var/log`, `/var/tmp`, and `/var/log/audit` must be preserved, and you can only change the size of the partitions.

. Start a Kickstart installation as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_an_advanced_rhel_{ProductNumber}_installation/starting-kickstart-installations_installing-rhel-as-an-experienced-user[Performing an automated installation using Kickstart].

[IMPORTANT]
Passwords in Kickstart files are not checked for OSPP requirements.

.Verification

. To check the current status of the system after installation is complete, reboot the system and start a new scan:
+
[subs="quotes,attributes"]
----
ifeval::[{ProductNumber} == 8]
# *oscap xccdf eval --profile ospp --report eval_postinstall_report.html /usr/share/xml/scap/ssg/content/ssg-rhel8-ds.xml*
endif::[]

ifeval::[{ProductNumber} == 9]
# *oscap xccdf eval --profile ospp --report eval_postinstall_report.html /usr/share/xml/scap/ssg/content/ssg-rhel9-ds.xml*
endif::[]
----

[role="_additional-resources"]
.Additional resources
* link:https://www.open-scap.org/tools/oscap-anaconda-addon/[OSCAP Anaconda Add-on]
