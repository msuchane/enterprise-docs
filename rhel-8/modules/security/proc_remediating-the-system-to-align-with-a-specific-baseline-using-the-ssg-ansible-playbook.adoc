:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_scanning-the-system-for-configuration-compliance-and-vulnerabilities.adoc

[id="remediating-the-system-to-align-with-a-specific-baseline-using-the-ssg-ansible-playbook_{context}"]
= Remediating the system to align with a specific baseline using an SSG Ansible playbook

You can remediate your system to align with a specific baseline by using an Ansible playbook file from the SCAP Security Guide project. This example uses the Health Insurance Portability and Accountability Act (HIPAA) profile, but you can remediate to align with any other profile provided by the SCAP Security Guide. For the details on listing the available profiles, see the
ifdef::security-hardening[]
xref:viewing-profiles-for-configuration-compliance_configuration-compliance-scanning[Viewing profiles for configuration compliance]
endif::[]
ifndef::security-hardening[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/scanning-the-system-for-configuration-compliance-and-vulnerabilities_security-hardening#viewing-profiles-for-configuration-compliance_configuration-compliance-scanning[Viewing profiles for configuration compliance]
endif::[]
section.

[WARNING]
If not used carefully, running the system evaluation with the `Remediate` option enabled might render the system non-functional. Red Hat does not provide any automated method to revert changes made by security-hardening remediations. Remediations are supported on {ProductShortName} systems in the default configuration. If your system has been altered after the installation, running remediation might not make it compliant with the required security profile.

.Prerequisites

* The `scap-security-guide` package is installed.
* The `ansible-core` package is installed. See the link:https://docs.ansible.com/ansible/latest/installation_guide/[Ansible Installation Guide] for more information.

[NOTE]
====
In {ProductShortName} 8.6 and later, Ansible Engine is replaced by the `ansible-core` package, which contains only built-in modules. Note that many Ansible remediations use modules from the community and Portable Operating System Interface (POSIX) collections, which are not included in the built-in modules. In this case, you can use Bash remediations as a substitute to Ansible remediations. The Red Hat Connector in RHEL {ProductNumber} includes the necessary Ansible modules to enable the remediation playbooks to function with Ansible Core.
====

.Procedure

. Remediate your system to align with HIPAA using Ansible:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *ansible-playbook -i localhost, -c local /usr/share/scap-security-guide/ansible/rhel{ProductNumber}-playbook-hipaa.yml*
----
. Restart the system.

.Verification

. Evaluate compliance of the system with the HIPAA profile, and save scan results in the `hipaa_report.html` file:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *oscap xccdf eval --profile hipaa --report hipaa_report.html /usr/share/xml/scap/ssg/content/ssg-rhel{ProductNumber}-ds.xml*
----

[role="_additional-resources"]
.Additional resources
* `scap-security-guide(8)` and `oscap(8)` man pages
* link:https://docs.ansible.com/[Ansible Documentation]
