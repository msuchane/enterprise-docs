:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// assembly_getting-started-with-selinux.adoc

[id="selinux-architecture_{context}"]
= SELinux architecture and packages

[role="_abstract"]
SELinux is a Linux Security Module (LSM) that is built into the Linux kernel. The SELinux subsystem in the kernel is driven by a security policy which is controlled by the administrator and loaded at boot. All security-relevant, kernel-level access operations on the system are intercepted by SELinux and examined in the context of the loaded security policy. If the loaded policy allows the operation, it continues. Otherwise, the operation is blocked and the process receives an error.

SELinux decisions, such as allowing or disallowing access, are cached. This cache is known as the Access Vector Cache (AVC). When using these cached decisions, SELinux policy rules need to be checked less, which increases performance. Remember that SELinux policy rules have no effect if DAC rules deny access first. Raw audit messages are logged to the `/var/log/audit/audit.log` and they start with the `type=AVC` string.

In {ProductShortName}{nbsp}{ProductNumber}, system services are controlled by the `systemd` daemon; `systemd` starts and stops all services, and users and processes communicate with `systemd` using the `systemctl` utility. The `systemd` daemon can consult the SELinux policy and check the label of the calling process and the label of the unit file that the caller tries to manage, and then ask SELinux whether or not the caller is allowed the access. This approach strengthens access control to critical system capabilities, which include starting and stopping system services.

The `systemd` daemon also works as an SELinux Access Manager. It retrieves the label of the process running `systemctl` or the process that sent a `D-Bus` message to `systemd`. The daemon then looks up the label of the unit file that the process wanted to configure. Finally, `systemd` can retrieve information from the kernel if the SELinux policy allows the specific access between the process label and the unit file label. This means a compromised application that needs to interact with `systemd` for a specific service can now be confined by SELinux. Policy writers can also use these fine-grained controls to confine administrators.

If a process is sending a `D-Bus` message to another process and if the SELinux policy does not allow the `D-Bus` communication of these two processes, then the system prints a `USER_AVC` denial message, and the D-Bus communication times out. Note that the D-Bus communication between two processes works bidirectionally.

[IMPORTANT]
====
To avoid incorrect SELinux labeling and subsequent problems, ensure that you start services using a `systemctl start` command.
====

{ProductShortName}{nbsp}{ProductNumber} provides the following packages for working with SELinux:

* policies: [package]`selinux-policy-targeted`, [package]`selinux-policy-mls`
* tools: [package]`policycoreutils`, [package]`policycoreutils-gui`, [package]`libselinux-utils`, [package]`policycoreutils-python-utils`, [package]`setools-console`, [package]`checkpolicy`
