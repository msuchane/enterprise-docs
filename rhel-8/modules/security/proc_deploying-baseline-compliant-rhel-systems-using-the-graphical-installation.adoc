:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_deploying-systems-that-are-compliant-with-a-security-profile-immediately-after-an-installation.adoc

[id="deploying-baseline-compliant-rhel-systems-using-the-graphical-installation_{context}"]
= Deploying baseline-compliant {ProductShortName} systems using the graphical installation

[role="_abstract"]
Use this procedure to deploy a {ProductShortName} system that is aligned with a specific baseline. This example uses Protection Profile for General Purpose Operating System (OSPP).

[WARNING]
====
Certain security profiles provided as part of the *SCAP Security Guide* are not compatible with the extended package set included in the *Server with GUI* base environment. For additional details, see
ifdef::security_hardening[]
xref:ref_profiles-not-compatible-with-server-with-gui_deploying-systems-that-are-compliant-with-a-security-profile-immediately-after-an-installation[Profiles not compatible with a GUI server].
endif::[]
ifndef::security_hardening[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/scanning-the-system-for-configuration-compliance-and-vulnerabilities_security-hardening#ref_profiles-not-compatible-with-server-with-gui_deploying-systems-that-are-compliant-with-a-security-profile-immediately-after-an-installation[Profiles not compatible with a GUI server].
endif::[]
====

.Prerequisites

* You have booted into the `graphical` installation program. Note that the *OSCAP Anaconda Add-on* does not support interactive text-only installation.

* You have accessed the `Installation Summary` window.

.Procedure

. From the `Installation Summary` window, click `Software Selection`. The `Software Selection` window opens.
. From the `Base Environment` pane, select the `Server` environment. You can select only one base environment.

. Click `Done` to apply the setting and return to the `Installation Summary` window.
. Because OSPP has strict partitioning requirements that must be met, create separate partitions for `/boot`, `/home`, `/var`, `/tmp`, `/var/log`, `/var/tmp`, and `/var/log/audit`.
. Click `Security Policy`. The `Security Policy` window opens.
. To enable security policies on the system, toggle the `Apply security policy` switch to `ON`.
. Select `Protection Profile for General Purpose Operating Systems` from the profile pane.
. Click `Select Profile` to confirm the selection.
. Confirm the changes in the `Changes that were done or need to be done` pane that is displayed at the bottom of the window. Complete any remaining manual changes.
. Complete the graphical installation process.
+
[NOTE]
The graphical installation program automatically creates a corresponding Kickstart file after a successful installation. You can use the `/root/anaconda-ks.cfg` file to automatically install OSPP-compliant systems.

.Verification

* To check the current status of the system after installation is complete, reboot the system and start a new scan:
+
[subs="quotes,attributes"]
----
ifeval::[{ProductNumber} == 8]
# *oscap xccdf eval --profile ospp --report eval_postinstall_report.html /usr/share/xml/scap/ssg/content/ssg-rhel8-ds.xml*
endif::[]

ifeval::[{ProductNumber} == 9]
# *oscap xccdf eval --profile ospp --report eval_postinstall_report.html /usr/share/xml/scap/ssg/content/ssg-rhel9-ds.xml*
endif::[]
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_a_standard_rhel_{ProductNumber}_installation/graphical-installation_graphical-installation#manual-partitioning_graphical-installation[Configuring manual partitioning]
