:_mod-docs-content-type: CONCEPT

[id="export-options-for-securing-an-nfs-server_{context}"]
= Export options for securing an NFS server

The NFS server determines a list structure of directories and hosts about which file systems to export to which hosts in the `/etc/exports` file.

[WARNING]
====
Extra spaces in the syntax of the exports file can lead to major changes in the configuration.

In the following example, the `/tmp/nfs/` directory is shared with the `bob.example.com` host and has read and write permissions.
----
/tmp/nfs/     bob.example.com(rw)
----

The following example is the same as the previous one but shares the same directory to the `bob.example.com` host with read-only permissions and shares it to the _world_ with read and write permissions due to a single space character after the hostname.
----
/tmp/nfs/     bob.example.com (rw)
----

You can check the shared directories on your system by entering the `showmount -e <hostname>` command.
====

Use the following export options on the `/etc/exports` file:

[WARNING]
====
Export an entire file system because exporting a subdirectory of a file system is not secure. An attacker can possibly access the unexported part of a partially-exported file system.
====

ro:: Use the `ro` option to export the NFS volume as read-only.
rw:: Use the `rw` option to allow read and write requests on the NFS volume. Use this option cautiously because allowing write access increases the risk of attacks.
+
[NOTE]
====
If your scenario requires to mount the directories with the `rw` option, make sure they are not writable for all users to reduce possible risks.
====

root_squash:: Use the `root_squash` option to map requests from `uid`/`gid` 0 to the anonymous `uid`/`gid`. This does not apply to any other `uids` or `gids` that might be equally sensitive, such as the `bin` user or the `staff` group.
no_root_squash:: Use the `no_root_squash` option to turn off root squashing. By default, NFS shares change the `root` user to the `nobody` user, which is an unprivileged user account. This changes the owner of all the `root` created files to `nobody`, which prevents the uploading of programs with the `setuid` bit set. When using the `no_root_squash` option, remote root users can change any file on the shared file system and leave applications infected by trojans for other users.
secure:: Use the `secure` option to restrict exports to reserved ports. By default, the server allows client communication only through reserved ports. However, it is easy for anyone to become a `root` user on a client on many networks, so it is rarely safe for the server to assume that communication through a reserved port is privileged. Therefore the restriction to reserved ports is of limited value; it is better to rely on Kerberos, firewalls, and restriction of exports to particular clients.

Additionally, consider the following best practices when exporting an NFS server:

* Exporting home directories is a risk because some applications store passwords in plain text or in a weakly encrypted format. You can reduce the risk by reviewing and improving the application code.
* Some users do not set passwords on SSH keys which again leads to risks with home directories. You can reduce these risks by enforcing the use of passwords or using Kerberos.
* Restrict the NFS exports only to required clients. Use the `showmount -e` command on the NFS server to review what the server is exporting. Do not export anything that is not specifically required.
* Do not allow unnecessary users to log in to a server to reduce the risk of attacks. You can periodically check who and what can access the server.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_and_managing_identity_management/index#using-automount-in-idm_configuring-and-managing-idm[Secure NFS with Kerberos] when using Red Hat Identity Management
* `exports(5)` and `nfs(5)` man pages
