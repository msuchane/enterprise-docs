:_mod-docs-content-type: REFERENCE
:experimental:
// Module included in the following assemblies:
// assembly_using-shared-system-certificates.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_managing-trusted-system-certificates.adoc[leveloffset=+1]

[id="managing-trusted-system-certificates_{context}"]
= Managing trusted system certificates

[role="_abstract"]
The `trust` command provides a convenient way for managing certificates in the shared system-wide truststore.

* To list, extract, add, remove, or change trust anchors, use the [command]`trust` command. To see the built-in help for this command, enter it without any arguments or with the [option]`--help` directive:
+
[subs="quotes",options="nowrap",role="white-space-pre"]
----
$ *trust*
usage: trust command &lt;args&gt;...

Common trust commands are:
  list             List trust or certificates
  extract          Extract certificates and trust
  extract-compat   Extract trust compatibility bundles
  anchor           Add, remove, change trust anchors
  dump             Dump trust objects in internal format

See 'trust &lt;command&gt; --help' for more information
----

* To list all system trust anchors and certificates, use the [command]`trust list` command:
+
[subs="quotes",options="nowrap",role="white-space-pre"]
----
$ *trust list*
pkcs11:id=%d2%87%b4%e3%df%37%27%93%55%f6%56%ea%81%e5%36%cc%8c%1e%3f%bd;type=cert
    type: certificate
    label: ACCVRAIZ1
    trust: anchor
    category: authority

pkcs11:id=%a6%b3%e1%2b%2b%49%b6%d7%73%a1%aa%94%f5%01%e7%73%65%4c%ac%50;type=cert
    type: certificate
    label: ACEDICOM Root
    trust: anchor
    category: authority
...
----

* To store a trust anchor into the system-wide truststore, use the [command]`trust anchor` sub-command and specify a path to a certificate. Replace _&lt;path.to/certificate.crt&gt;_ by a path to your certificate and its file name:
+
[subs="quotes,attributes"]
----
# *trust anchor _&lt;path.to/certificate.crt&gt;_*
----

* To remove a certificate, use either a path to a certificate or an ID of a certificate:
+
[subs="quotes",options="nowrap",role="white-space-pre"]
----
# *trust anchor --remove _&lt;path.to/certificate.crt&gt;_*
# *trust anchor --remove "pkcs11:id=_&lt;%AA%BB%CC%DD%EE&gt;_;type=cert"*
----

[role="_additional-resources"]
.Additional resources
* All sub-commands of the [command]`trust` commands offer a detailed built-in help, for example:
+
[subs="quotes",options="nowrap",role="white-space-pre"]
----
$ *trust list --help*
usage: trust list --filter=&lt;what&gt;

  --filter=&lt;what&gt;     filter of what to export
                        ca-anchors        certificate anchors
...
  --purpose=&lt;usage&gt;   limit to certificates usable for the purpose
                        server-auth       for authenticating servers
...
----

[role="_additional-resources"]
.Additional resources
* `update-ca-trust(8)` and `trust(1)` man pages
