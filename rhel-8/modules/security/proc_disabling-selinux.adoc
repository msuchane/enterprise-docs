:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// assembly_changing-selinux-states-and-modes.adoc

[id='Enabling_and_Disabling_SELinux-Disabling_SELinux_{context}']
= Disabling SELinux

[role="_abstract"]
Use the following procedure to permanently disable SELinux.

[IMPORTANT]
====
When SELinux is disabled, SELinux policy is not loaded at all; it is not enforced and AVC messages are not logged. Therefore, all
ifdef::using-selinux[]
xref:benefits-of-selinux_getting-started-with-selinux[benefits of running SELinux]
endif::[]
ifndef::using-selinux[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/getting-started-with-selinux_using-selinux#benefits-of-selinux_getting-started-with-selinux[benefits of running SELinux]
endif::[]
are lost.

Red{nbsp}Hat strongly recommends to use permissive mode instead of permanently disabling SELinux. See
ifdef::using-selinux[]
xref:changing-to-permissive-mode_changing-selinux-states-and-modes[Changing to permissive mode]
endif::[]
ifndef::using-selinux[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/changing-selinux-states-and-modes_using-selinux#changing-to-permissive-mode_changing-selinux-states-and-modes[Changing to permissive mode]
endif::[]
for more information about permissive mode.
====

[WARNING]
====
Disabling SELinux using the `SELINUX=disabled` option in the `/etc/selinux/config` results in a process in which the kernel boots with SELinux enabled and switches to disabled mode later in the boot process. Because memory leaks and race conditions causing kernel panics can occur, prefer disabling SELinux by adding the `selinux=0` parameter to the kernel command line as described in
ifdef::using-selinux[]
xref:changing-selinux-modes-at-boot-time_changing-selinux-states-and-modes[Changing SELinux modes at boot time]
endif::[]
ifndef::using-selinux[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/changing-selinux-states-and-modes_using-selinux#changing-selinux-modes-at-boot-time_changing-selinux-states-and-modes[Changing SELinux modes at boot time]
endif::[]
if your scenario really requires to completely disable SELinux.
====


.Procedure

. Open the `/etc/selinux/config` file in a text editor of your choice, for example:
+
[subs="quotes,attributes"]
----
# *vi /etc/selinux/config*
----
+
. Configure the [option]`SELINUX=disabled` option:
+
[subs="quotes,attributes"]
----
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#       enforcing - SELinux security policy is enforced.
#       permissive - SELinux prints warnings instead of enforcing.
#       disabled - No SELinux policy is loaded.
SELINUX=*disabled*
# SELINUXTYPE= can take one of these two values:
#       targeted - Targeted processes are protected,
#       mls - Multi Level Security protection.
SELINUXTYPE=targeted
----

. Save the change, and restart your system:
+
[subs="quotes,attributes"]
----
# *reboot*
----

.Verification

. After reboot, confirm that the [command]`getenforce` command returns `Disabled`:
+
[subs="quotes,attributes"]
----
$ *getenforce*
Disabled
----
