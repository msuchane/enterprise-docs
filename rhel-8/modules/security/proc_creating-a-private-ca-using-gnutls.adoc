:_mod-docs-content-type: PROCEDURE

[id="creating-a-private-ca-using-gnutls_{context}"]
= Creating a private CA using GnuTLS

Private certificate authorities (CA) are useful when your scenario requires verifying entities within your internal network. For example, use a private CA when you create a VPN gateway with authentication based on certificates signed by a CA under your control or when you do not want to pay a commercial CA. To sign certificates in such use cases, the private CA uses a self-signed certificate.

.Prerequisites

* You have `root` privileges or permissions to enter administrative commands with `sudo`. Commands that require such privileges are marked with `#`.

* You have already installed GnuTLS on your system. If you did not, you can use this command:
+
[subs="quotes,attributes"]
----
$ *{PackageManagerCommand} install gnutls-utils*
----

.Procedure

. Generate a private key for your CA. For example, the following command creates a 256-bit ECDSA (Elliptic Curve Digital Signature Algorithm) key:
+
[subs="quotes"]
----
$ *certtool --generate-privkey --sec-param High --key-type=ecdsa --outfile _&lt;ca.key&gt;_*
----
+
The time for the key-generation process depends on the hardware and entropy of the host, the selected algorithm, and the length of the key.

. Create a template file for a certificate.
.. Create a file with a text editor of your choice, for example:
+
[subs="quotes"]
----
$ *vi _&lt;ca.cfg&gt;_*
----
+
.. Edit the file to include the necessary certification details:
+
[subs="quotes"]
----
organization = "Example Inc."
state = "Example"
country = EX
cn = "Example CA"
serial = 007
expiration_days = 365
ca
cert_signing_key
crl_signing_key
----
. Create a certificate signed using the private key generated in step 1:
+
The generated _&lt;ca.crt&gt;_ file is a self-signed CA certificate that you can use to sign other certificates for one year.
_&lt;ca.crt&gt;_ file is the public key (certificate). The loaded file _&lt;ca.key&gt;_ is the private key. You should keep this file in safe location.
+
[subs="quotes"]
----
$ *certtool --generate-self-signed --load-privkey _&lt;ca.key&gt;_ --template _&lt;ca.cfg&gt;_ --outfile _&lt;ca.crt_&gt;*
----
+
. Set secure permissions on the private key of your CA, for example:
+
[subs="quotes,macros"]
----
# *chown _&lt;root&gt;_pass:[:]_&lt;root&gt;_ _&lt;ca.key&gt;_*
# *chmod 600 _&lt;ca.key&gt;_*
----

.Next steps

* To use a self-signed CA certificate as a trust anchor on client systems, copy the CA certificate to the client and add it to the clients' system-wide truststore as `root`:
+
[subs="quotes"]
----
# *trust anchor _&lt;ca.crt&gt;_*
----
+
See
ifdef::securing-networks[]
xref:using-shared-system-certificates_securing-networks[]
endif::[]
ifndef::securing-networks[]
the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/using-shared-system-certificates_securing-networks[Using shared system certificates] chapter
endif::[]
for more information.


.Verification

. Display the basic information about your self-signed CA:
+
[subs="quotes"]
----
$ *certtool --certificate-info --infile _&lt;ca.crt&gt;_*
Certificate:
…
    	X509v3 extensions:
        	…
        	X509v3 Basic Constraints: critical
            	CA:TRUE
        	X509v3 Key Usage: critical
            	Certificate Sign, CRL Sign
----
+
. Create a certificate signing request (CSR), and use your CA to sign the request. The CA must successfully create a certificate based on the CSR, for example:
.. Generate a private key for your CA:
+
[subs="quotes"]
----
$ *certtool --generate-privkey --outfile _&lt;example-server.key&gt;_*
----
+
.. Open a new configuration file in a text editor of your choice, for example:
+
[subs="quotes"]
----
$ *vi _&lt;example-server.cfg&gt;_*
----
+
.. Edit the file to include the necessary certification details:
+
[subs="quotes"]
----
signing_key
encryption_key
key_agreement

tls_www_server

country = "US"
organization = "Example Organization"
cn = "server.example.com"

dns_name = "example.com"
dns_name = "server.example.com"
ip_address = "192.168.0.1"
ip_address = "::1"
ip_address = "127.0.0.1"
----
+
.. Generate a request with the previously created private key:
+
[subs="quotes"]
----
$ *certtool --generate-request --load-privkey _&lt;example-server.key&gt;_ --template _&lt;example-server.cfg&gt;_ --outfile _&lt;example-server.crq&gt;_*
----
+
.. Generate the certificate and sign it with the private key of the CA:
+
[subs="quotes"]
----
$ *certtool --generate-certificate --load-request _&lt;example-server.crq&gt;_  --load-ca-certificate _&lt;ca.crt&gt;_ --load-ca-privkey _&lt;ca.key&gt;_ --outfile _&lt;example-server.crt&gt;_*
----
+

[role="_additional-resources"]
.Additional resources
* `certtool(1)` and `trust(1)` man pages
