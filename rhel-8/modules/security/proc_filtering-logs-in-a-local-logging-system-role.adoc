:_mod-docs-content-type: PROCEDURE
[id="proc_filtering-logs-in-a-local-logging-system-role_{context}"]
= Filtering logs in a local `logging` RHEL system role

[role="_abstract"]
You can deploy a logging solution which filters the logs based on the `rsyslog` property-based filter.

.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]

[NOTE]
====
You do not have to have the `rsyslog` package installed, because the {RHELSystemRoles} installs `rsyslog` when deployed.
====

.Procedure

. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Deploying files input and configured files output
  hosts: managed-node-01.example.com
  roles:
    - rhel-system-roles.logging
  vars:
    logging_inputs:
      - name: files_input
        type: basics
    logging_outputs:
      - name: files_output0
        type: files
        property: msg
        property_op: contains
        property_value: error
        path: /var/log/errors.log
      - name: files_output1
        type: files
        property: msg
        property_op: "!contains"
        property_value: error
        path: /var/log/others.log
    logging_flows:
      - name: flow0
        inputs: [files_input]
        outputs: [files_output0, files_output1]
....
+
Using this configuration, all messages that contain the `error` string are logged in `/var/log/errors.log`, and all other messages are logged in `/var/log/others.log`.
+
You can replace the `error` property value with the string by which you want to filter.
+
You can modify the variables according to your preferences.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification

. Test the syntax of the `/etc/rsyslog.conf` file:
+
[literal,subs="+quotes"]
....
# *rsyslogd -N 1*
rsyslogd: version 8.1911.0-6.el8, config validation run...
rsyslogd: End of config validation run. Bye.
....


. Verify that the system sends messages that contain the `error` string to the log:

.. Send a test message:
+
[literal,subs="+quotes"]
....
# *logger error*
....

.. View the `/var/log/errors.log` log, for example:
+
[literal,subs="+quotes"]
....
# *cat /var/log/errors.log*
Aug  5 13:48:31 _hostname_ root[6778]: error
....
+
Where `_hostname_` is the host name of the client system. Note that the log contains the user name of the user that entered the logger command, in this case `root`.

[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.logging/README.md` file
* `/usr/share/doc/rhel-system-roles/logging/` directory
