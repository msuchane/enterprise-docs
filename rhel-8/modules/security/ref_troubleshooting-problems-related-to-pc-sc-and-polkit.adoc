:_mod-docs-content-type: REFERENCE

// included in assembly_controlling-access-to-smart-cards-using-polkit.adoc

[id="ref_troubleshooting-problems-related-to-pc-sc-and-polkit_{context}"]
= Troubleshooting problems related to PC/SC and polkit

Polkit policies that are automatically enforced after you install the `pcsc-lite` package and start the `pcscd` daemon may ask for authentication in the user's session even if the user does not directly interact with a smart card. In GNOME, you can see the following error message:
----
Authentication is required to access the PC/SC daemon
----
////
In KDE, a dialog window with the following title is displayed after you open a web browser:
----
Authentication Required - PolicyKit1 KDE Agent
----
////
Note that the system can install the `pcsc-lite` package as a dependency when you install other packages related to smart cards such as `opensc`.

If your scenario does not require any interaction with smart cards and you want to prevent displaying authorization requests for the PC/SC daemon, you can remove the `pcsc-lite` package. Keeping the minimum of necessary packages is a good security practice anyway.

If you use smart cards, start troubleshooting by checking the rules in the system-provided policy file at `/usr/share/polkit-1/actions/org.debian.pcsc-lite.policy`. You can add your custom rule files to the policy in the `/etc/polkit-1/rules.d/` directory, for example, `03-allow-pcscd.rules`. Note that the rule files use the JavaScript syntax, the policy file is in the XML format.

To understand what authorization requests the system displays, check the Journal log, for example:
[subs="quotes"]
----
$ *journalctl -b | grep pcsc*
...
Process 3087 (user: 1001) is NOT authorized for action: access_pcsc
...
----
The previous log entry means that the user is not authorized to perform an action by the policy. You can solve this denial by adding a corresponding rule to `/etc/polkit-1/rules.d/`.

You can search also for log entries related to the `polkitd` unit, for example:
[subs="quotes"]
----
$ *journalctl -u polkit*
...
polkitd[NNN]: Error compiling script /etc/polkit-1/rules.d/00-debug-pcscd.rules
...
polkitd[NNN]: Operator of unix-session:c2 FAILED to authenticate to gain authorization for action org.debian.pcsc-lite.access_pcsc for unix-process:4800:14441 [/usr/libexec/gsd-smartcard] (owned by unix-user:group)
...
----
In the previous output, the first entry means that the rule file contains some syntax error. The second entry means that the user failed to gain the access to `pcscd`.

You can also list all applications that use the PC/SC protocol by a short script. Create an executable file, for example, `pcsc-apps.sh`, and insert the following code:
[subs="attributes"]
----
#!/bin/bash

cd /proc

for p in [0-9]*
do
	if grep libpcsclite.so.1.0.0 $p/maps &amp;&gt; /dev/null
	then
		echo -n "process: "
		cat $p/cmdline
		echo " ($p)"
	fi
done
----
Run the script as `root`:
[subs="quotes"]
----
# *./pcsc-apps.sh*
process: /usr/libexec/gsd-smartcard (3048)
enable-sync --auto-ssl-client-auth --enable-crashpad (4828)
...
----

[role="_additional-resources"]
.Additional resources
* `journalctl`, `polkit(8)`, `polkitd(8)`, and `pcscd(8)` man pages.
