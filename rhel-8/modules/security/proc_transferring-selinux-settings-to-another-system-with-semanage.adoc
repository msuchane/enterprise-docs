:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_deploying-the-same-selinux-configuration-on-multiple-systems.adoc

[id="transferring-selinux-settings-to-another-system-with-semanage_{context}"]
= Transferring SELinux settings to another system with semanage

[role="_abstract"]
Use the following steps for transferring your custom and verified SELinux settings between {ProductShortName}{nbsp}{ProductNumber}-based systems.

.Prerequisites

* The `policycoreutils-python-utils` package is installed on your system.

.Procedure

. Export your verified SELinux settings:
+
[subs="quotes,attributes"]
----
# *semanage export -f ./_my-selinux-settings.mod_*
----
. Copy the file with the settings to the new system:
+
[subs="quotes,attributes"]
----
# *scp ./_my-selinux-settings.mod_ _new-system-hostname_:*
----
. Log in on the new system:
+
[subs="quotes,attributes"]
----
$ *ssh root@_new-system-hostname_*
----
. Import the settings on the new system:
+
[subs="quotes,attributes"]
----
_new-system-hostname_# *semanage import -f ./_my-selinux-settings.mod_*
----

[role="_additional-resources"]
.Additional resources
* `semanage-export(8)` and `semanage-import(8)` man pages
