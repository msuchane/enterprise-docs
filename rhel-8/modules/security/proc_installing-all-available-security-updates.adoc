:_mod-docs-content-type: PROCEDURE


[id="installing-all-available-security-updates_{context}"]
= Installing all available security updates

To keep the security of your system up to date, you can install all currently available security updates using the `{PackageManagerCommand}` utility.

.Prerequisite

* A Red Hat subscription attached to the host.

.Procedure

. Install security updates using `{PackageManagerCommand}` utility:
+
[literal,subs="+quotes,attributes"]
----
# [command]`*{PackageManagerCommand} update --security*`
----
+
[NOTE]
====
The `--security` parameter is important. Without it, `{PackageManagerCommand} update` installs all updates, including bug fixes and enhancements.
====

. Confirm and start the installation by pressing kbd:[y]:
+
[literal,subs="+quotes,attributes"]
----
...
Transaction Summary
===========================================
Upgrade  ... Packages

Total download size: ... M
Is this ok [y/d/N]: *y*
----

. Optional: list processes that require a manual restart of the system after installing the updated packages:
+
[literal,subs="+quotes,attributes"]
----
# [command]`*{PackageManagerCommand} needs-restarting*`
1107 : /usr/sbin/rsyslogd -n
1199 : -bash
----
+
[NOTE]
====
This command lists only processes that require a restart, and not services. That is, you cannot restart  processes listed using the `systemctl` utility. For example, the `bash` process in the output is terminated when the user that owns this process logs out.
====
