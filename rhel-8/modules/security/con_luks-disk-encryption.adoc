:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
// assembly_encrypting-block-devices-using-luks.adoc


[id="luks-disk-encryption_{context}"]
= LUKS disk encryption

Linux Unified Key Setup-on-disk-format (LUKS) provides a set of tools that simplifies managing the encrypted devices. With LUKS, you can encrypt block devices and enable multiple user keys to decrypt a master key. For bulk encryption of the partition, use this master key.

{RHEL} uses LUKS to perform block device encryption. By default, the option to encrypt the block device is unchecked during the installation. If you select the option to encrypt your disk, the system prompts you for a passphrase every time you boot the computer. This passphrase unlocks the bulk encryption key that decrypts your partition. If you want to modify the default partition table, you can select the partitions that you want to encrypt. This is set in the partition table settings.

.Ciphers

The default cipher used for LUKS is `aes-xts-plain64`. The default key size for LUKS is 512 bits. The default key size for LUKS with *Anaconda* XTS mode is 512 bits. The following are the available ciphers:

* Advanced Encryption Standard (AES)

* Twofish
//Twofish - a 128-bit block cipher. Even Serpant is a 128-bit block cipher, and AES too. For the same, commenting it out.

* Serpent

.Operations performed by LUKS

* LUKS encrypts entire block devices and is therefore well-suited for protecting contents of mobile devices such as removable storage media or laptop disk drives.

* The underlying contents of the encrypted block device are arbitrary, which makes it useful for encrypting swap devices. This can also be useful with certain databases that use specially formatted block devices for data storage.

* LUKS uses the existing device mapper kernel subsystem.

* LUKS provides passphrase strengthening, which protects against dictionary attacks.

* LUKS devices contain multiple key slots, which means you can add backup keys or passphrases.

[IMPORTANT]
====
LUKS is not recommended for the following scenarios:

* Disk-encryption solutions such as LUKS protect the data only when your system is off. After the system is on and LUKS has decrypted the disk, the files on that disk are available to anyone who have access to them.

* Scenarios that require multiple users to have distinct access keys to the same device. The LUKS1 format provides eight key slots and LUKS2 provides up to 32 key slots.

* Applications that require file-level encryption.
====

[role="_additional-resources"]
.Additional resources
* link:https://gitlab.com/cryptsetup/cryptsetup/blob/master/README.md[LUKS Project Home Page]
* link:https://gitlab.com/cryptsetup/LUKS2-docs/blob/master/luks2_doc_wip.pdf[LUKS On-Disk Format Specification]
* link:https://doi.org/10.6028/NIST.FIPS.197-upd1[FIPS 197: Advanced Encryption Standard (AES)]
