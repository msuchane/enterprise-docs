:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
// assembly_using-the-system-wide-cryptographic-policies

[id="disabling-sha-1-by-customizing-a-system-wide-cryptographic-policy_{context}"]
= Disabling SHA-1 by customizing a system-wide cryptographic policy

[role="_abstract"]
Because the SHA-1 hash function has an inherently weak design, and advancing cryptanalysis has made it vulnerable to attacks, {ProductShortName} 8 does not use SHA-1 by default. Nevertheless, some third-party applications, for example, public signatures, still use SHA-1. To disable the use of SHA-1 in signature algorithms on your system, you can use the `NO-SHA1` policy module.

[IMPORTANT]
====
The `NO-SHA1` policy module disables the SHA-1 hash function only in signatures and not elsewhere. In particular, the `NO-SHA1` module still allows the use of SHA-1 with hash-based message authentication codes (HMAC). This is because HMAC security properties do not rely on the collision resistance of the corresponding hash function, and therefore the recent attacks on SHA-1 have a significantly lower impact on the use of SHA-1 for HMAC.
====

If your scenario requires disabling a specific key exchange (KEX) algorithm combination, for example, `diffie-hellman-group-exchange-sha1`, but you still want to use both the relevant KEX and the algorithm in other combinations, see link:https://access.redhat.com/solutions/4278651[Steps to disable the diffie-hellman-group1-sha1 algorithm in SSH] for instructions on opting out of system-wide crypto-policies for SSH and configuring SSH directly.

[NOTE]
====
The module for disabling SHA-1 is available from {ProductShortName} 8.3.
Customization of system-wide cryptographic policies is available from {ProductShortName} 8.2.
====

.Procedure

. Apply your policy adjustments to the `DEFAULT` system-wide cryptographic policy level:
+
[subs="quotes,+macros"]
----
# *update-crypto-policies --set DEFAULTpass:quotes[:]NO-SHA1*
----
. To make your cryptographic settings effective for already running services and applications, restart the system:
+
[subs="quotes,attributes"]
----
# *reboot*
----

[role="_additional-resources"]
.Additional resources
* `Custom Policies` section in the `update-crypto-policies(8)` man page.
* `Crypto Policy Definition Format` section in the `crypto-policies(7)` man page.
* link:https://www.redhat.com/en/blog/how-customize-crypto-policies-rhel-82[How to customize crypto policies in RHEL] Red Hat blog article.
