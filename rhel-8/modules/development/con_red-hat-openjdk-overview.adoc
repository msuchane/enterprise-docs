:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// master.adoc

// This module can be included from assemblies using the following include statement:
// include::modules/con_red-hat-openjdk-overview.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_red-hat-openjdk-overview.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//

[id="red-hat-openjdk-overview_{context}"]
= Red Hat OpenJDK overview

The Red Hat build of OpenJDK is a free and open source implementation of the Java Platform, Standard Edition (Java SE). It is based on the upstream OpenJDK 8u and 11u projects and includes the Shenandoah Garbage Collector in both OpenJDK versions 8 and 11.

* *Multi-platform* - OpenJDK is now supported on Windows and RHEL. This helps you standardize on a single Java platform across desktop, datacenter, and hybrid cloud.

* *Frequent releases* - Red Hat delivers quarterly JRE and JDK updates per year for the OpenJDK 8 and 11 distributions. These are available as [filename]`rpm`, [filename]`msi`, [filename]`zip` files and as containers.

* *Long-term support* - Red Hat supports the recently released OpenJDK 11, as well as, OpenJDK 7 and 8. For more information about the support lifecycle, see link:https://access.redhat.com/articles/1299013[OpenJDK Life Cycle and Support Policy].

* *Java Web Start* - Red Hat OpenJDK supports Java Web Start for Windows and RHEL.
