:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_updating-java-on-rhel-8.adoc

// This module can be included from assemblies using the following include statement:
// include::modules/development\proc_updating-openjdk-on-rhel-8-using-an-archive.adoc[leveloffset=+1]

[id="updating-openjdk-on-rhel-8-using-an-archive_{context}"]
= Updating Java on RHEL 8 using an archive

You can update Java using a `ZIP` bundle. This is useful if the Java administrator does not have root privileges.

[discrete]
== Prerequisites

* Know the generic path pointing to your Java Development Kit (JDK) or Java Runtime Environment (JRE) installation. For example, [filename]`~/jdks/java-11`

[discrete]
== Procedure

. Remove the existing symbolic link of the generic path to your JDK or JRE.
+
For example:
+
`$ *unlink ~/jdks/java-11*`

. Install the latest version of the JDK or JRE in your installation location.
+
* For instructions on installing a JRE, see xref:installing-jvm-on-rhel-using-an-archive_installing-java-on-rhel-8[Installing a JRE on RHEL 8].

* For instructions on installing a JDK, see xref:installing-the-jdk-on-rhel-8-using-an-archive-bundle_installing-java-on-rhel-8[Installing a JDK on RHEL 8].

// [discrete]
// == Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
