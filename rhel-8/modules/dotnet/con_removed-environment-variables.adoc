:_mod-docs-content-type: CONCEPT
[id="removed-variables_{context}"]
= Removed environment variables
The following environment variables have been removed from the {dotnet} {dotnet-ver} container image.

**DOTNET_ASPNET_STORE**
Publish assuming the runtime image contains the ASP.NET Core runtime store.

See link:{url-dotnet-getting-started-rhel}/using_net_core_2_1_on_openshift_container_platform#environmental-variables-for-dotnet_using-dotnet-on-openshift-container-platform[Environment Variables] for the list of environment variables available with {dotnet} {dotnet-ver}.
