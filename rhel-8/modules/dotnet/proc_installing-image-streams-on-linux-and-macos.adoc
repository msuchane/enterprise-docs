:_mod-docs-content-type: PROCEDURE


[id="installing-image-streams-on-linux-and-macos_{context}"]
= Installing image streams on Linux and macOS

You can use link:https://raw.githubusercontent.com/redhat-developer/s2i-dotnetcore/master/install-imagestreams.sh[this script] to install, upgrade, or remove the image streams on Linux and macOS.

.Procedure

. Download the script.
.. On Linux use:
+
----
$ wget https://raw.githubusercontent.com/redhat-developer/s2i-dotnetcore/master/install-imagestreams.sh
----
.. On Mac use:
+
----
$ curl https://raw.githubusercontent.com/redhat-developer/s2i-dotnetcore/master/install-imagestreams.sh -o install-imagestreams.sh
----

. Make the script executable:
+
----
$ chmod +x install-imagestreams.sh
----

. Log in to the {os} cluster:
[subs="+quotes,attributes"]
+
----
$ oc login
----
. Install image streams and add a pull secret for authentication against the `registry.redhat.io`:
+
ifeval::[{dotnet-ver} == 5.0]
[subs="+quotes,attributes"]
----
./install-imagestreams.sh --os rhel [--user _subscription_username_ --password _subscription_password_]
----
endif::[]
ifeval::[{dotnet-ver} <= 3.1]
ifeval::[{rhel-ver} < 9]
[subs="+quotes,attributes"]
----
./install-imagestreams.sh --os rhel{rhel-ver} [--user _subscription_username_ --password _subscription_password_]
----
endif::[]
endif::[]
ifeval::[{dotnet-ver} <= 3.1]
ifeval::[{rhel-ver} == 9]
[subs="+quotes,attributes"]
----
./install-imagestreams.sh --os rhel [--user _subscription_username_ --password _subscription_password_]
----
endif::[]
endif::[]
Replace _subscription_username_ with the name of the user, and replace _subscription_password_ with the user's password. The credentials may be omitted if you do not plan to use the RHEL7-based images.
+
If the pull secret is already present, the `--user` and `--password` arguments are ignored.

.Additional information

* `./install-imagestreams.sh --help`
