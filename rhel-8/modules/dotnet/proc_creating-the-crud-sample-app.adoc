:_mod-docs-content-type: PROCEDURE


[id="creating-the-crud-sample-app_{context}"]
= Creating the CRUD sample {app}

`s2i-dotnetcore-persistent-ex` is a simple Create, Read, Update, Delete (CRUD) {dotnet} web application that stores data in a PostgreSQL database.

.Procedure

To create the sample application using `oc`:

. Add the database:
+
[subs="+quotes,attributes"]
----
$ oc new-app postgresql-ephemeral
----

. Add the {dotnet} application:
+
ifeval::[{dotnet-ver} >= 5.0]
[subs="+quotes,attributes"]
----
$ oc new-app dotnet:{dotnet-ver}-ubi8~https://github.com/redhat-developer/s2i-dotnetcore-persistent-ex#{dotnet-branch} --context-dir app
----
endif::[]
ifeval::[{dotnet-ver} <= 3.1]
[subs="+quotes,attributes"]
----
$ oc new-app dotnet:{dotnet-ver}~https://github.com/redhat-developer/s2i-dotnetcore-persistent-ex#{dotnet-branch} --context-dir app
----
endif::[]

. Add environment variables from the `postgresql` secret and database service name environment variable:
+
[subs="+quotes,attributes"]
----
$ oc set env dc/s2i-dotnetcore-persistent-ex --from=secret/postgresql -e database-service=postgresql
----

. Make the application accessible externally:
+
[subs="+quotes,attributes"]
----
$ oc expose service s2i-dotnetcore-persistent-ex
----

. Obtain the sharable URL:
+
----
$ oc get route s2i-dotnetcore-persistent-ex
----

[role="_additional-resources"]
.Additional resources
* link:https://github.com/redhat-developer/s2i-dotnetcore-persistent-ex[`s2i-dotnetcore-ex` application repository on GitHub]
