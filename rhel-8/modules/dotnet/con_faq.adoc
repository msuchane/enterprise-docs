:_mod-docs-content-type: CONCEPT


[id="faq_{context}"]
= Frequently asked questions

Here are four of the most common support questions for Integrated Support.

. When do I access Integrated Support?
+
You can engage link:{url-rh-access}/support/[Red Hat Support] directly.
If the Red Hat Support Engineer assigned to your case needs assistance from Microsoft, the Red Hat Support Engineer will collaborate with Microsoft directly without any action required from you.
Likewise on the Microsoft side, they have a process for directly collaborating with Red Hat Support Engineers.

. What happens after I file a support case?
+
Once the Red Hat support case has been created, a Red Hat Support Engineer will be assigned to the case and begin collaborating on the issue with you and your Microsoft Support Engineer.
You should expect a response to the issue based on link:{url-rh-access}/support/offerings/production/sla[Red Hat's Production Support Service Level Agreement].

. What if I need further assistance?
+
Contact link:{url-rh-access}/support/[Red Hat Support] for assistance in creating your case or with any questions related to this process.
You can view any of your open cases here.

. How do I engage Microsoft for support for an Azure platform issue?
+
If you have support from Microsoft, you can open a case using whatever process you typically would follow.
If you do not have support with Microsoft, you can always get support from link:https://azure.microsoft.com/en-us/support/options/[Microsoft Support].
