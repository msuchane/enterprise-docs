:_mod-docs-content-type: CONCEPT


[id="customer-privacy_{context}"]
= Customer privacy

Various Microsoft products have a feature that reports usage statistics, analytics, and various other metrics to Microsoft over the network.
Microsoft calls this Telemetry.
Red Hat is disabling telemetry because we do not recommend sending customer data to anyone without explicit permission.

ifeval::[{dotnet-ver} < 5.0]
ifeval::[{rhel-ver} >= 8]
As part of installing .NET on {rhel} {rhel-ver},
endif::[]
ifeval::[{rhel-ver} == 7]
As part of the `scl enable` command on {rhel} 7,
endif::[]
we automatically set an environment variable that tells the CLI to disable its telemetry reporting. Any customer that
ifeval::[{rhel-ver} >= 8]
runs {dotnet} {dotnet-ver}
endif::[]
ifeval::[{rhel-ver} == 7]
uses `scl enable` to run {dotnet}
endif::[]
will not report telemetry information to Microsoft. This helps keep customer information confidential.
ifeval::[{rhel-ver} == 7]
Customers are free to override the environment variable after using `scl enable` to report telemetry, if they wish.
endif::[]

Customers can enable telemetry by setting the environment variable `DOTNET_CLI_TELEMETRY_OPTOUT` to `0`. See link:https://docs.microsoft.com/en-us/dotnet/core/tools/telemetry[.NET SDK Tools Telemetry collection] for more information.
endif::[]

////
ifeval::[{dotnet-ver} < 5.0]
ifeval::[{rhel-ver} >= 8]
As part of installing .NET on {rhel} {rhel-ver}, we automatically set an environment variable that tells the CLI to disable its telemetry reporting. Any customer that runs {dotnet} {dotnet-ver} will not report telemetry information to Microsoft. This helps keep customer information confidential.
endif::[]
ifeval::[{rhel-ver} == 7]
As part of the `scl enable` command on {RHEL7}, we automatically set an environment variable that tells the CLI to disable its telemetry reporting. Any customer that uses `scl enable` to run {dotnet} will not report telemetry information to Microsoft. This helps keep customer information confidential. Customers are free to override the environment variable after using `scl enable` to report telemetry, if they wish.
endif::[]
endif::[]
////
