:_mod-docs-content-type: CONCEPT
[id="introducing-dotnet_{context}"]
= Introducing {dotnet} {dotnet-ver}

{dotnet} is a general-purpose development platform featuring automatic memory management and modern programming languages. Using .NET, you can build high-quality applications efficiently. {dotnet} is available on {rhel} (RHEL) and {ocp} through certified containers.

{dotnet} offers the following features:

* The ability to follow a microservices-based approach, where some components are built with .NET and others with Java, but all can run on a common, supported platform on RHEL and {ocp}.

* The capacity to more easily develop new {dotnet} workloads on Microsoft Windows. You can deploy and run your applications on either RHEL or Windows Server.

* A heterogeneous data center, where the underlying infrastructure is capable of running .NET applications without having to rely solely on Windows Server.

ifeval::[{dotnet-ver} == 7]
{dotnet} {dotnet-ver} is supported on {supportedOS}, and supported {ocp} versions. For more information, see the link:https://access.redhat.com/support/policy/updates/openshift[Red Hat OpenShift Container Platform Life Cycle Policy].
endif::[]

ifeval::[{dotnet-ver} < 7]
//maybe this can be deleted off of master as we should probably not reference specific openshift versions
{dotnet} {dotnet-ver} is supported on {supportedOS}, and {ocp} versions 3.11 and later.
endif::[]
//.Documentation for the currently supported versions of .NET and .NET Core
//[options="header"]
//|===
//|.NET Version |Getting started guides |Release notes
//ifeval::[{dotnet-ver} == 6.0]
//|.NET 6
//|link:{dotnet-url}/6.0/html/getting_started_with_.net_on_rhel_9/[.NET 6 on RHEL 9]

//link:{dotnet-url}/6.0/html/getting_started_with_.net_on_rhel_8/[.NET 6 on RHEL 8]

//Red Hat Enterprise Linux 7?

//link:some[.NET 6 on {ocp}]

//|link:{dotnet-url}/6.0/html/release_notes_for_.net_5.0_containers/[.NET 6 containers]

//link:{dotnet-url}/6.0/html/release_notes_for_.net_6.0_rpm_packages[.NET 6 RPMs]
//endif::[]

//|.NET 5
//|link:{dotnet-url}/5.0/html/getting_started_with_.net_on_rhel_8/[.NET 5 on RHEL 8]

//link:{dotnet-url}/5.0/html/getting_started_with_.net_on_rhel_7/[.NET 5 on RHEL 7]

//link:some[.NET 5 on {ocp}]

//|link:{dotnet-url}/5.0/html/release_notes_for_.net_5.0_containers/[.NET 5 containers]

//link:{dotnet-url}/5.0/html/release_notes_for_.net_5.0_rpm_packages[.NET 5 RPMs]

//|.NET Core 3.1
//|link:{dotnet-url}/3.1/html/getting_started_with_.net_on_rhel_9/[.NET Core 3.1 on RHEL 9]

//link:{dotnet-url}/3.1/html/getting_started_with_.net_on_rhel_8/[.NET Core 3.1 on RHEL 8]

//link:{dotnet-url}/3.1/html/getting_started_with_.net_on_rhel_7/[.NET Core 3.1 on RHEL 7]

//link:some[.NET Core 3.1 on {ocp}]

//|link:{dotnet-url}/3.1/html/release_notes_for_.net_core_3.1_containers/[.NET Core 3.1 containers]

//link:{dotnet-url}/3.1/html/release_notes_for_.net_core_3.1_rpm_packages[.NET Core 3.1 RPMs]
//|===
