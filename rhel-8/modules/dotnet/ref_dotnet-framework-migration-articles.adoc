:_mod-docs-content-type: REFERENCE


[id="dotnet-framework-migration-articles_{context}"]
= Porting from .NET Framework

Refer to the following Microsoft articles when migrating from .NET Framework:

* For general guidelines, see link:https://docs.microsoft.com/en-us/dotnet/core/porting/[Porting to .NET Core from .NET Framework].
* For porting libraries, see link:https://docs.microsoft.com/en-us/dotnet/core/porting/libraries[Porting to .NET Core - Libraries].
* For migrating to ASP.NET Core, see link:https://docs.microsoft.com/en-us/aspnet/core/migration/?view=aspnetcore-2.2[Migrating to ASP.NET Core].

Several technologies and APIs present in the .NET Framework are not available in .NET Core and .NET.
If your application or library requires these APIs, consider finding alternatives or continue using the .NET Framework.
.NET Core and .NET do not support the following technologies and APIs:

* Desktop applications, for example, Windows Forms and Windows Presentation Foundation (WPF)
* Windows Communication Foundation (WCF) servers (WCF clients are supported)
* .NET remoting

Additionally, several .NET APIs can only be used in Microsoft Windows environments.
The following list shows examples of these Windows-specific APIs:

* `Microsoft.Win32.Registry`
* `System.AppDomains`
* `System.Security.Principal.Windows`

//make for all dotnet versions after apiport is supported on 6
ifeval::[{dotnet-ver} < 6]
Consider using the link:https://docs.microsoft.com/en-us/dotnet/standard/analyzers/portability-analyzer[.NET Portability Analyzer] to identify API gaps and potential replacements.

For example, enter the following command to find out how much of the API used by your .NET Framework application is supported by the newer version of .NET Core:

[subs="+quotes,attributes"]
----
$ dotnet /path/to/ApiPort.dll analyze -f . -r html --target '.NET Framework,Version=_<dotnet-framework-version>_' --target '.NET Core,Version=_<dotnet-version>_'
----
Replace _<dotnet-framework-version>_ with the .NET Framework version you are currently using. For example, _4.6_. Replace _<dotnet-version>_ with the version of .NET Core you plan to migrate to. For example, _3.1_.
endif::[]
[IMPORTANT]
====
Several APIs that are not supported in the default version of {dotnet} may be available from the link:https://blogs.msdn.microsoft.com/dotnet/2017/11/16/announcing-the-windows-compatibility-pack-for-net-core/#using-the-windows-compatibility-pack[Microsoft.Windows.Compatibility] NuGet package.
Be careful when using this NuGet package.
Some of the APIs provided (such as `Microsoft.Win32.Registry`) only work on Windows, making your application incompatible with {rhel}.
====
