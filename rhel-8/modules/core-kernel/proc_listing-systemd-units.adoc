:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="listing-systemd_units_{context}"]
= Listing systemd units

[role="_abstract"]
Use the `systemd` system and service manager to list its units.


.Procedure

* List all active units on the system with the `systemctl` utility. The terminal returns an output similar to the following example:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl*
UNIT                                                LOAD   ACTIVE SUB       DESCRIPTION
...
init.scope                                          loaded active running   System and Service Manager
session-2.scope                                     loaded active running   Session 2 of user jdoe
abrt-ccpp.service                                   loaded active exited    Install ABRT coredump hook
abrt-oops.service                                   loaded active running   ABRT kernel log watcher
abrt-vmcore.service                                 loaded active exited    Harvest vmcores for ABRT
abrt-xorg.service                                   loaded active running   ABRT Xorg log watcher
...
-.slice                                             loaded active active    Root Slice
machine.slice                                       loaded active active    Virtual Machine and Container Slice system-getty.slice                                                                       loaded active active    system-getty.slice
system-lvm2\x2dpvscan.slice                         loaded active active    system-lvm2\x2dpvscan.slice
system-sshd\x2dkeygen.slice                         loaded active active    system-sshd\x2dkeygen.slice
system-systemd\x2dhibernate\x2dresume.slice         loaded active active    system-systemd\x2dhibernate\x2dresume>
system-user\x2druntime\x2ddir.slice                 loaded active active    system-user\x2druntime\x2ddir.slice
system.slice                                        loaded active active    System Slice
user-1000.slice                                     loaded active active    User Slice of UID 1000
user-42.slice                                       loaded active active    User Slice of UID 42
user.slice                                          loaded active active    User and Session Slice
...
....
+
`UNIT`::
A name of a unit that also reflects the unit position in a control group hierarchy. The units relevant for resource control are a _slice_, a _scope_, and a _service_.
`LOAD`::
Indicates whether the unit configuration file was properly loaded. If the unit file failed to load, the field contains the state _error_ instead of _loaded_. Other unit load states are: _stub_, _merged_, and _masked_.
`ACTIVE`::
The high-level unit activation state, which is a generalization of `SUB`.
`SUB`::
The low-level unit activation state. The range of possible values depends on the unit type.
`DESCRIPTION`::
The description of the unit content and functionality.

* List all active and inactive units:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl --all*
....

* Limit the amount of information in the output:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl --type service,masked*
....
+
The `--type` option requires a comma-separated list of unit types such as a _service_ and a _slice_, or unit load states such as _loaded_ and _masked_.

[role="_additional-resources"]
.Additional resources
* [citetitle]_link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/managing-systemd_configuring-basic-system-settings#managing-system-services-with-systemctl_managing-systemd[Managing system services with systemctl]_ in {ProductShortName}
* The `systemd.resource-control(5)`, `systemd.exec(5)` manual pages
