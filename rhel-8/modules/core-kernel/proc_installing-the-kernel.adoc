:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// assembly_the-linux-kernel.adoc

[id="installing-the-kernel_{context}"]
= Installing specific kernel versions

[role="_abstract"]
Install new kernels using the [application]*{PackageManagerCommand}* package manager.

.Procedure

* To install a specific kernel version, enter the following command:
+
[subs="quotes,attributes,macros"]
....
# *{PackageManagerCommand} install kernel-_{version}_*
....

[role="_additional-resources"]
.Additional resources
* [citetitle]_link:++https://access.redhat.com/labs/rhcb/++[Red Hat Code Browser]_
* [citetitle]_link:++https://access.redhat.com/articles/3078++[Red Hat Enterprise Linux Release Dates ]_
