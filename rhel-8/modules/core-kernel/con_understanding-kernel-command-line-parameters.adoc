:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// rhel-8/assemblies/assembly_configuring-kernel-command-line-parameters.adoc

[id="understanding-kernel-command-line-parameters_{context}"]
= What are kernel command-line parameters

[role="_abstract"]
With kernel command-line parameters, you can overwrite default values and set specific hardware settings. At boot time, you can configure the following features:

* The {RHEL} kernel
* The initial RAM disk
* The user space features

ifeval::[{ProductNumber} == 8]

By default, the kernel command-line parameters for systems using the GRUB boot loader are defined in the `kernelopts` variable of the `/boot/grub2/grubenv` file for each kernel boot entry.

[NOTE]
====
For IBM Z, the kernel command-line parameters are stored in the boot entry configuration file because the `zipl` boot loader does not support environment variables. Thus, the `kernelopts` environment variable cannot be used.
====

endif::[]


ifeval::[{ProductNumber} >= 9]

By default, the kernel command-line parameters for systems using the GRUB boot loader are defined in the boot entry configuration file for each kernel boot entry.

endif::[]

You can manipulate boot loader configuration files by using the `grubby` utility. With `grubby`, you can perform these actions:

* Change the default boot entry.
* Add or remove arguments from a GRUB menu entry.

[role="_additional-resources"]
.Additional resources
* `kernel-command-line(7)`, `bootparam(7)` and `dracut.cmdline(7)` manual pages
* link:https://access.redhat.com/articles/3938081[How to install and boot custom kernels in Red Hat Enterprise Linux 8]
* The `grubby(8)` manual page
