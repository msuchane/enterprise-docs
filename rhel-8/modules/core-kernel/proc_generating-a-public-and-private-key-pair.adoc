:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// rhel-8/assemblies/assembly_signing-a-kernel-and-modules-for-secure-boot.adoc

[id="generating-a-public-and-private-key-pair_{context}"]
= Generating a public and private key pair

[role="_abstract"]
To use a custom kernel or custom kernel modules on a Secure Boot-enabled system, you must generate a public and private X.509 key pair. You can use the generated private key to sign the kernel or the kernel modules. You can also validate the signed kernel or kernel modules by adding the corresponding public key to the Machine Owner Key (MOK) for Secure Boot.

[WARNING]
====
Apply strong security measures and access policies to guard the contents of your private key. In the wrong hands,
the key could be used to compromise any system which is authenticated by the corresponding public key.
====

.Procedure

* Create an X.509 public and private key pair:

** If you only want to sign custom kernel _modules_:
+
[subs="+quotes,attributes"]
----
# *efikeygen --dbdir /etc/pki/pesign \*
            *--self-sign \*
            *--module \*
            *--common-name 'CN=_Organization signing key_' \*
            *--nickname '_Custom Secure Boot key_'*
----

** If you want to sign custom _kernel_:
+
[subs="+quotes,attributes"]
----
# *efikeygen --dbdir /etc/pki/pesign \*
            *--self-sign \*
            *--kernel \*
            *--common-name 'CN=_Organization signing key_' \*
            *--nickname '_Custom Secure Boot key_'*
----

** When the RHEL system is running FIPS mode:
+
[subs="+quotes,attributes"]
----
# *efikeygen --dbdir /etc/pki/pesign \*
            *--self-sign \*
            *--kernel \*
            *--common-name 'CN=_Organization signing key_' \*
            *--nickname '_Custom Secure Boot key_'*
            *--token 'NSS FIPS 140-2 Certificate DB'*
----
+
[NOTE]
====
In FIPS mode, you must use the `--token` option so that `efikeygen` finds the default "NSS Certificate DB" token in the PKI database.
====
+
The public and private keys are now stored in the `/etc/pki/pesign/` directory.

[IMPORTANT]
====

It is a good security practice to sign the kernel and the kernel modules within the validity period of its signing key. However, the `sign-file` utility does not warn you and the key will be usable in {PRODUCT} regardless of the validity dates.

====


[role="_additional-resources"]
.Additional resources
* `openssl(1)` manual page
* [citetitle]_link:++https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/security_guide/index#sec-Using_OpenSSL++[RHEL Security Guide]_
* xref:enrolling-public-key-on-target-system-by-adding-the-public-key-to-the-mok-list_{context}[Enrolling public key on target system by adding the public key to the MOK list]
