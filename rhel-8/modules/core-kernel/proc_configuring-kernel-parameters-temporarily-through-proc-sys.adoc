:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="configuring-kernel-parameters-temporarily-through-proc-sys_{context}"]
= Configuring kernel parameters temporarily through /proc/sys/

[role="_abstract"]
Set kernel parameters temporarily through the files in the `/proc/sys/` virtual file system directory.

.Prerequisites

* Root permissions

.Procedure

. Identify a kernel parameter you want to configure.
+
[literal,subs="+quotes,verbatim,normal"]
....
[command]`# *ls -l /proc/sys/<pass:quotes[_TUNABLE_CLASS_]>/*`
....
+
The writable files returned by the command can be used to configure the kernel. The files with read-only permissions provide feedback on the current settings.

. Assign a target value to the kernel parameter.
+
[literal,subs="+quotes,verbatim,normal"]
....
[command]`# *echo <pass:quotes[_TARGET_VALUE_]> > /proc/sys/<pass:quotes[_TUNABLE_CLASS_]>/<pass:quotes[_PARAMETER_]>*`
....
+
The command makes configuration changes that will disappear once the system is restarted.

. Optionally, verify the value of the newly set kernel parameter.
+
[literal,subs="+quotes,verbatim,normal"]
....
[command]`# *cat /proc/sys/<pass:quotes[_TUNABLE_CLASS_]>/<pass:quotes[_PARAMETER_]>*`
....

[role="_additional-resources"]
.Additional resources
* xref:configuring-kernel-parameters-permanently-with-sysctl_configuring-kernel-parameters-at-runtime[Configuring kernel parameters permanently with sysctl]
* xref:using-configuration-files-in-etc-sysctl-d-to-adjust-kernel-parameters_configuring-kernel-parameters-at-runtime[Using configuration files in /etc/sysctl.d/ to adjust kernel parameters]
