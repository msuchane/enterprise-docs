:_mod-docs-content-type: PROCEDURE
:experimental:

[id="installing-kdump-anaconda{context}"]
= Installing kdump using Anaconda

[role="_abstract"]
The *Anaconda* installer provides a graphical interface screen for `kdump` configuration during an interactive installation. The installer screen is titled as *KDUMP* and is available from the main *Installation Summary* screen. You can enable `kdump` and reserve the required amount of memory.

.Procedure

. Under the *KDUMP* field, enable `kdump` if not already enabled.
+
ifeval::[{ProductNumber} == 8]
image::installation-summary-kdump-8.png[Enable kdump during RHEL installation]
endif::[]
ifeval::[{ProductNumber} == 9]
image::installation-summary-kdump-9.png[Enable kdump during RHEL installation]
endif::[]

. Under *Kdump Memory Reservation*, select *Manual*` if you must customize the memory reserve.
. Under *KDUMP* field, in *Memory To Be Reserved (MB)*, set the required memory reserve for `kdump`.
+
ifeval::[{ProductNumber} == 8]
image::kdump-memory-reservation-8.png[Kdump Memory Reservation]
endif::[]
ifeval::[{ProductNumber} == 9]
image::kdump-memory-reservation-9.png[Memory setting]
endif::[]


// delete image::anaconda-summary-hub-states.png[Enable kdump during RHEL installation]
