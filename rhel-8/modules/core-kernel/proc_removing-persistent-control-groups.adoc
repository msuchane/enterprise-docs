:_mod-docs-content-type: PROCEDURE
:experimental:
[id="removing-persistent-control-groups_{context}"]
= Removing persistent control groups

[role="_abstract"]
You can use the `systemd` system and service manager to remove persistent control groups (`cgroups`) if you no longer need to limit, prioritize, or control access to hardware resources for groups of processes.

Persistent `cgroups` are released when a service or a scope unit is stopped or disabled and its configuration file is deleted.


.Procedure

. Stop the service unit:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl stop <pass:quotes[_name_]>.service*
....

. Disable the service unit:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl disable <pass:quotes[_name_]>.service*
....

. Remove the relevant unit configuration file:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *rm /usr/lib/systemd/system/<pass:quotes[_name_]>.service*
....

. Reload all unit configuration files so that changes take effect:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl daemon-reload*
....


[role="_additional-resources"]
.Additional resources
* xref:understanding-control-groups_setting-limits-for-applications[What are control groups]
* xref:what-kernel-resource-controllers-are_setting-limits-for-applications[What are kernel resource controllers]
* `systemd.resource-control(5)`, `cgroups(7)`, and `systemd.kill(5)` manual pages

ifdef::kernel-title[]
* xref:role-of-systemd-in-control-groups-version-1_using-control-groups-version-1-with-systemd[Role of systemd in control groups]
endif::[]

ifndef::kernel-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_monitoring_and_updating_the_kernel/using-control-groups-version-1-with-systemd_managing-monitoring-and-updating-the-kernel#role-of-systemd-in-control-groups-version-1_using-control-groups-version-1-with-systemd[Role of systemd in control groups]
endif::[]

* [citetitle]_link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/managing-systemd_configuring-basic-system-settings#managing-system-services-with-systemctl_managing-systemd[Managing system services with systemctl]_ in {ProductShortName}

