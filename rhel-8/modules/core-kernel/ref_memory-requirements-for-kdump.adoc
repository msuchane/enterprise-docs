:_mod-docs-content-type: REFERENCE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="memory-requirements-for-kdump_{context}"]
= Memory requirements for kdump

[role="_abstract"]
For `kdump` to capture a kernel crash dump and save it for further analysis, a part of the system memory should be permanently reserved for the capture kernel. When reserved, this part of the system memory is not available to the main kernel.

The memory requirements vary based on certain system parameters. One of the major factors is the system's hardware architecture. To find out the exact machine architecture (such as Intel 64 and AMD64, also known as x86_64) and print it to standard output, use the following command:

[subs="quotes,attributes,macros"]
----
[command]`$ *uname -m*`
----
With the stated list of minimum memory requirements, you can set the appropriate memory size to automatically reserve a memory for `kdump` on the latest available versions. The memory size depends on the system's architecture and total available physical memory.


.Minimum amount of reserved memory required for kdump

ifeval::[{ProductNumber} == 8]

[options="header"]
|===
|Architecture|Available Memory|Minimum Reserved Memory
.3+|AMD64 and Intel{nbsp}64 (`x86_64`)
|1{nbsp}GB to 4{nbsp}GB
|192{nbsp}MB of RAM
|4{nbsp}GB to 64{nbsp}GB
|256{nbsp}MB of RAM
|64{nbsp}GB and more|512 MB of RAM
|64-bit ARM architecture (`arm64`)|2{nbsp}GB and more|480{nbsp}MB of RAM
.5+|IBM Power Systems (`ppc64le`)|2{nbsp}GB to 4{nbsp}GB|384{nbsp}MB of RAM
|4{nbsp}GB to 16{nbsp}GB|512{nbsp}MB of RAM
|16{nbsp}GB to 64{nbsp}GB|1{nbsp}GB of RAM
|64{nbsp}GB to 128{nbsp}GB|2{nbsp}GB of RAM
|128{nbsp}GB and more|4{nbsp}GB of RAM
.3+|IBM{nbsp}Z (`s390x`)|1{nbsp}GB to 4{nbsp}GB|192{nbsp}MB of RAM
|4{nbsp}GB to 64{nbsp}GB|256{nbsp}MB of RAM
|64{nbsp}GB and more|512{nbsp}MB of RAM
|===

endif::[]




ifeval::[{ProductNumber} == 9]

[options="header"]
|===
|Architecture|Available Memory|Minimum Reserved Memory
.3+|AMD64 and Intel{nbsp}64 (`x86_64`)
|1{nbsp}GB to 4{nbsp}GB
|192{nbsp}MB of RAM
|4{nbsp}GB to 64{nbsp}GB
|256{nbsp}MB of RAM
|64{nbsp}GB and more|512 MB of RAM
.3+|64-bit ARM (4k pages)
|1 GB to 4 GB|256 MB of RAM
|4 GB to 64 GB|320 MB of RAM
|64 GB and more|576 MB of RAM
.3+|64-bit ARM (64k pages)
|1 GB to 4 GB|356 MB of RAM
|4 GB to 64 GB|420 MB of RAM
|64 GB and more|676 MB of RAM
.5+|IBM Power Systems (`ppc64le`)|2{nbsp}GB to 4{nbsp}GB|384{nbsp}MB of RAM
|4{nbsp}GB to 16{nbsp}GB|512{nbsp}MB of RAM
|16{nbsp}GB to 64{nbsp}GB|1{nbsp}GB of RAM
|64{nbsp}GB to 128{nbsp}GB|2{nbsp}GB of RAM
|128{nbsp}GB and more|4{nbsp}GB of RAM
.3+|IBM{nbsp}Z (`s390x`)|1{nbsp}GB to 4{nbsp}GB|192{nbsp}MB of RAM
|4{nbsp}GB to 64{nbsp}GB|256{nbsp}MB of RAM
|64{nbsp}GB and more|512{nbsp}MB of RAM
|===

endif::[]



On many systems, `kdump` is able to estimate the amount of required memory and reserve it automatically. This behavior is enabled by default, but only works on systems that have more than a certain amount of total available memory, which varies based on the system architecture.

[IMPORTANT]
====
The automatic configuration of reserved memory based on the total amount of memory in the system is a best effort estimation. The actual required memory may vary due to other factors such as I/O devices. Using not enough of memory might cause that a debug kernel is not able to boot as a capture kernel in case of a kernel panic. To avoid this problem, sufficiently increase the crash kernel memory.
====


[role="_additional-resources"]
.Additional resources

ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/solutions/5907911[How has the crashkernel parameter changed between RHEL8 minor releases?]
* link:https://access.redhat.com/articles/rhel-limits++[Technology capabilities and limits tables]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_monitoring_and_updating_the_kernel/supported-kdump-configurations-and-targets_managing-monitoring-and-updating-the-kernel#minimum-threshold-for-automatic-memory-reservation_supported-kdump-configurations-and-targets[Minimum threshold for automatic memory reservation]
endif::[]

ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/articles/rhel-limits++[Technology capabilities and limits tables]
endif::[]
