:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="firmware-assisted-dump-on-ibm-powerpc-hardware_{context}"]

= Firmware assisted dump on IBM PowerPC hardware

[role="_abstract"]
The `fadump` utility captures the `vmcore` file from a fully-reset system with PCI and I/O devices. This mechanism uses firmware to preserve memory regions during a crash and then reuses the `kdump` userspace scripts to save the `vmcore` file. The memory regions consist of all system memory contents, except the boot memory, system registers, and hardware Page Table Entries (PTEs).

The `fadump` mechanism offers improved reliability over the traditional dump type, by rebooting the partition and using a new kernel to dump the data from the previous kernel crash. The `fadump` requires an IBM POWER6 processor-based or later version hardware platform.

For further details about the `fadump` mechanism, including PowerPC specific methods of resetting hardware, see the `/usr/share/doc/kexec-tools/fadump-howto.txt` file.

[NOTE]
====
The area of memory that is not preserved, known as boot memory, is the amount of RAM required to successfully boot the kernel after a crash event. By default, the boot memory size is 256MB or 5% of total system RAM, whichever is larger.
====

Unlike `kexec-initiated` event, the `fadump` mechanism uses the production kernel to recover a crash dump. When booting after a crash, PowerPC hardware makes the device node `/proc/device-tree/rtas/ibm.kernel-dump` available to the `proc` filesystem (`procfs`). The `fadump-aware kdump` scripts, check for the stored `vmcore`, and then complete the system reboot cleanly.
  