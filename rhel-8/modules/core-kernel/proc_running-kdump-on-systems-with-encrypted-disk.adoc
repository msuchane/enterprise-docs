:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="running-kdump-on-systems-with-encrypted-disk_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Running kdump on systems with encrypted disk

[role="_abstract"]
When you run a LUKS encrypted partition, systems require certain amount of available memory.
If the system has less than the required amount of available memory, the `cryptsetup` utility fails to mount the partition.
As a  result, capturing the  `vmcore` file to an encrypted target location fails in the second kernel (capture kernel).

The `kdumpctl estimate` command helps you estimate the amount of memory you need for `kdump`.
`kdumpctl estimate` prints the recommended `crashkernel` value, which is the most suitable memory size required for `kdump`.

The recommended `crashkernel` value is calculated based on the current kernel size, kernel module, initramfs, and the LUKS encrypted target memory requirement.

In case you are using the custom `crashkernel=` option, `kdumpctl estimate` prints the `LUKS required size` value.
The value is the memory size required for LUKS encrypted target.

.Procedure
. Print the estimate `crashkernel=` value:
+
----
# *kdumpctl estimate*

Encrypted kdump target requires extra memory, assuming using the keyslot with minimum memory requirement
   Reserved crashkernel:    256M
   Recommended crashkernel: 652M

   Kernel image size:   47M
   Kernel modules size: 8M
   Initramfs size:      20M
   Runtime reservation: 64M
   LUKS required size:  512M
   Large modules: <none>
   WARNING: Current crashkernel size is lower than recommended size 652M.
----

. Configure the amount of required memory by increasing the `crashkernel=` value.

. Reboot the system.

[NOTE]
====
If the `kdump` service still fails to save the dump file to the encrypted target, increase the `crashkernel=` value as required.
====


// Run the `kdump` mechanism using one of the following procedures:

// * To run the `kdump` define one of the following:
//  ** Configure a remote `kdump` target.
//  ** Define the dump to an unencrypted partition.
//  ** Specify an increased `crashkernel=` value to the required level.

// * Add an extra key slot by using a key derivation function (KDF):
// . `*cryptsetup luksAddKey --pbkdf pbkdf2 /dev/vda2*`
// . `*cryptsetup config --key-slot 1 --priority prefer /dev/vda2*`
// . `*cryptsetup luksDump /dev/vda2*`

// Using the default KDF of the encrypted partition may consume a lot of memory. You must manually provide the password in the second kernel (capture), even if you encounter an Out of Memory (OOM) error message.

// [WARNING]
// ====
// Adding an extra key slot can have a // negative effect on security, as
// multiple keys can decrypt an // encrypted volume. This may cause a potential risk to the volume.
// ====
