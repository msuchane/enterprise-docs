:_mod-docs-content-type: PROCEDURE
:experimental:
[id="working-with-encrypted-keys_{context}"]
= Working with encrypted keys

[role="_abstract"]
You can improve system security on systems where a Trusted Platform Module (TPM) is not available by managing encrypted keys.


.Prerequisites
ifeval::[{ProductNumber} == 8]
* For the 64-bit ARM architecture and IBM Z, the `encrypted-keys` kernel module is loaded:
+
----
# modprobe encrypted-keys
----
+
For more information about how to load kernel modules, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_monitoring_and_updating_the_kernel/index#loading-kernel-modules-at-system-runtime_managing-kernel-modules[Loading kernel modules at system runtime].
endif::[]

.Procedure

. Generate a user key by using a random sequence of numbers.
+
[literal,subs="+quotes", options="nowrap",role=white-space-pre]
....
# *keyctl add user kmk-user "$(dd if=/dev/urandom bs=1 count=32 2>/dev/null)" @u*
427069434
....
+
The command generates a user key called `kmk-user` which acts as a _primary key_ and is used to seal the actual encrypted keys.

. Generate an encrypted key using the primary key from the previous step:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *keyctl add encrypted encr-key "new user:kmk-user 32" @u*
1012412758
....

. Optionally, list all keys in the specified user keyring:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *keyctl list @u*
2 keys in keyring:
427069434: --alswrv  1000  1000 user: kmk-user
1012412758: --alswrv  1000  1000 encrypted: encr-key
....

[IMPORTANT]
====
Encrypted keys that are not sealed by a trusted primary key are only as secure as the user primary key (random-number key) that was used to encrypt them.
Therefore, load the primary user key as securely as possible and preferably early during the boot process.
====


[role="_additional-resources"]
.Additional resources
* The `keyctl(1)` manual page
* link:https://www.kernel.org/doc/html/v4.18/security/keys/core.html#[Kernel Key Retention Service]
