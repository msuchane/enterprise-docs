// Module included in the following assemblies:
//
// assembly_making-persistent-changes-to-a-grub-2-menu.adoc


[id="proc_listing-the-default-kernel_{context}"]
= Listing the default kernel

[role="_abstract"]
By listing the default kernel, you can find the file name and the index number of the default kernel to make permanent changes to the GRUB boot loader.

.Procedure

* To find out the file name of the default kernel, enter:

[literal,subs="+quotes,verbatim"]
....
# *grubby --default-kernel*
/boot/vmlinuz-4.18.0-372.9.1.el8.x86_64
....

* To find out the index number of the default kernel, enter:

[literal,subs="+quotes,verbatim"]
....
# *grubby --default-index*
0
....
