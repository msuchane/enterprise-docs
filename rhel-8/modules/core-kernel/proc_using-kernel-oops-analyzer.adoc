:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// rhel-8/assemblies/assembly_analyzing-a-core-dump.adoc

[id="using-kernel-oops-analyzer_{context}"]
= Using Kernel Oops Analyzer

[role="_abstract"]
The Kernel Oops Analyzer tool analyzes the crash dump by comparing the oops messages with known issues in the knowledge base.

.Prerequisites

* Secure an oops message to feed the Kernel Oops Analyzer.

.Procedure

. Access the Kernel Oops Analyzer tool.

. To diagnose a kernel crash issue, upload a kernel oops log generated in `vmcore`.
+
** Alternatively you can also diagnose a kernel crash issue by providing a text message or a `vmcore-dmesg.txt` as an input.
+
image::2KernelOopsAnalyzer.png[Kernel oops analyzer]

. Click `DETECT` to compare the oops message based on information from the [command]`makedumpfile` against known solutions.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/labs/kerneloopsanalyzer/[The Kernel Oops Analyzer] article
* link:https://access.redhat.com/articles/206873[A Guide to Unexpected System Restarts]
