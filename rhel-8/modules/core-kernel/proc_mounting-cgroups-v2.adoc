:_mod-docs-content-type: PROCEDURE

[id="mounting-cgroups-v2_{context}"]

[role="_abstract"]
= Mounting cgroups-v2

During the boot process, {ProductShortName} {ProductNumber} mounts the `cgroup-v1` virtual filesystem by default. To utilize `cgroup-v2` functionality in limiting resources for your applications, manually configure the system.


.Prerequisites

* You have root permissions.


.Procedure

. Configure the system to mount `cgroups-v2` by default during system boot by the `systemd` system and service manager:
+
[literal,subs="quotes,attributes", options="nowrap",role=white-space-pre]
....
# *grubby --update-kernel=/boot/vmlinuz-$(uname -r) --args="systemd.unified_cgroup_hierarchy=1"*
....
+
This adds the necessary kernel command-line parameter to the current boot entry.
+
To add the `systemd.unified_cgroup_hierarchy=1` parameter to all kernel boot entries:
+
[literal,subs="quotes,attributes", options="nowrap",role=white-space-pre]
....
# *grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=1"*
....


. Reboot the system for the changes to take effect.


.Verification steps

. Optionally, verify that the `cgroups-v2` filesystem was mounted:
+
[literal,subs="quotes,attributes", options="nowrap",role=white-space-pre]
....
# *mount -l | grep cgroup*
cgroup2 on /sys/fs/cgroup type cgroup2 (rw,nosuid,nodev,noexec,relatime,seclabel,nsdelegate)
....
+
The `cgroups-v2` filesystem was successfully mounted on the `/sys/fs/cgroup/` directory.


. Optionally, inspect the contents of the `/sys/fs/cgroup/` directory:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *ll /sys/fs/cgroup/*
-r--r--r--.  1 root root 0 Apr 29 12:03 cgroup.controllers
-rw-r--r--.  1 root root 0 Apr 29 12:03 cgroup.max.depth
-rw-r--r--.  1 root root 0 Apr 29 12:03 cgroup.max.descendants
-rw-r--r--.  1 root root 0 Apr 29 12:03 cgroup.procs
-r--r--r--.  1 root root 0 Apr 29 12:03 cgroup.stat
-rw-r--r--.  1 root root 0 Apr 29 12:18 cgroup.subtree_control
-rw-r--r--.  1 root root 0 Apr 29 12:03 cgroup.threads
-rw-r--r--.  1 root root 0 Apr 29 12:03 cpu.pressure
-r--r--r--.  1 root root 0 Apr 29 12:03 cpuset.cpus.effective
-r--r--r--.  1 root root 0 Apr 29 12:03 cpuset.mems.effective
-r--r--r--.  1 root root 0 Apr 29 12:03 cpu.stat
drwxr-xr-x.  2 root root 0 Apr 29 12:03 init.scope
-rw-r--r--.  1 root root 0 Apr 29 12:03 io.pressure
-r--r--r--.  1 root root 0 Apr 29 12:03 io.stat
-rw-r--r--.  1 root root 0 Apr 29 12:03 memory.pressure
-r--r--r--.  1 root root 0 Apr 29 12:03 memory.stat
drwxr-xr-x. 69 root root 0 Apr 29 12:03 system.slice
drwxr-xr-x.  3 root root 0 Apr 29 12:18 user.slice
....
+
The `/sys/fs/cgroup/` directory, also called the _root control group_, by default, contains interface files (starting with `cgroup`) and controller-specific files such as `cpuset.cpus.effective`. In addition, there are some directories related to `systemd`, such as, `/sys/fs/cgroup/init.scope`, `/sys/fs/cgroup/system.slice`, and `/sys/fs/cgroup/user.slice`.


[role="_additional-resources"]
.Additional resources
* xref:understanding-control-groups_setting-limits-for-applications[Understanding control groups]
* xref:what-kernel-resource-controllers-are_setting-limits-for-applications[What kernel resource controllers are]
* `cgroups(7)`, `sysfs(5)` manual pages
