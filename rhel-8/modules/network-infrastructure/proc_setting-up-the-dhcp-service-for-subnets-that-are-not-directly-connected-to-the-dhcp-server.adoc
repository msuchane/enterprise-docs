:_mod-docs-content-type: PROCEDURE

[id="setting-up-the-dhcp-service-for-subnets-that-are-not-directly-connected-to-the-dhcp-server_{context}"]
= Setting up the DHCP service for subnets that are not directly connected to the DHCP server

[role="_abstract"]
Use the following procedure if the DHCP server is not directly connected to the subnet for which the server should answer DHCP requests. This is the case if a DHCP relay agent forwards requests to the DHCP server, because none of the DHCP server's interfaces is directly connected to the subnet the server should serve.

Depending on whether you want to provide DHCP for IPv4, IPv6, or both protocols, see the procedure for:

* xref:setting-up-the-dhcp-service-for-subnets-that-are-not-directly-connected-to-the-dhcp-server_{context}_ipv4[IPv4 networks]
* xref:setting-up-the-dhcp-service-for-subnets-that-are-not-directly-connected-to-the-dhcp-server_{context}_ipv6[IPv6 networks]

.Prerequisites

* You are logged in as the `root` user.
* The `dhcp-server` package is installed.


.Procedure

[discrete,id="setting-up-the-dhcp-service-for-subnets-that-are-not-directly-connected-to-the-dhcp-server_{context}_ipv4"]
* For IPv4 networks:

. Edit the `/etc/dhcp/dhcpd.conf` file:

.. Optionally, add global parameters that `dhcpd` uses as default if no other directives contain these settings:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
option domain-name "example.com";
default-lease-time 86400;
....
+
This example sets the default domain name for the connection to `example.com`, and the default lease time to `86400` seconds (1 day).

.. Add the `authoritative` statement on a new line:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
authoritative;
....
+
[IMPORTANT]
====
Without the `authoritative` statement, the `dhcpd` service does not answer `DHCPREQUEST` messages with `DHCPNAK` if a client asks for an address that is outside of the pool.
====

.. Add a `shared-network` declaration, such as the following, for IPv4 subnets that are not directly connected to an interface of the server:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
shared-network _example_ {
  option domain-name-servers 192.0.2.1;
  ...

  subnet 192.0.2.0 netmask 255.255.255.0 {
    range 192.0.2.20 192.0.2.100;
    option routers 192.0.2.1;
  }

  subnet 198.51.100.0 netmask 255.255.255.0 {
    range 198.51.100.20 198.51.100.100;
    option routers 198.51.100.1;
  }
  ...
}
....
+
This example adds a shared network declaration, that contains a `subnet` declaration for both the 192.0.2.0/24 and 198.51.100.0/24 networks. With this configuration, the DHCP server assigns the following settings to a client that sends a DHCP request from one of these subnets:
+
** The IP of the DNS server for clients from both subnets is: `192.0.2.1`.
** A free IPv4 address from the range defined in the `range` parameter, depending on from which subnet the client sent the request.
** The default gateway is either `192.0.2.1` or `198.51.100.1` depending on from which subnet the client sent the request.

.. Add a `subnet` declaration for the subnet the server is directly connected to and that is used to reach the remote subnets specified in `shared-network` above:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
subnet 203.0.113.0 netmask 255.255.255.0 {
}
....
+
[NOTE]
====
If the server does not provide DHCP service to this subnet, the `subnet` declaration must be empty as shown in the example. Without a declaration for the directly connected subnet, `dhcpd` does not start.
====

. Optionally, configure that `dhcpd` starts automatically when the system boots:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# **systemctl enable dhcpd**
....

. Start the `dhcpd` service:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# **systemctl start dhcpd**
....


[discrete,id="setting-up-the-dhcp-service-for-subnets-that-are-not-directly-connected-to-the-dhcp-server_{context}_ipv6"]
* For IPv6 networks:

. Edit the `/etc/dhcp/dhcpd6.conf` file:

.. Optionally, add global parameters that `dhcpd` uses as default if no other directives contain these settings:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
option dhcp6.domain-search "example.com";
default-lease-time 86400;
....
+
This example sets the default domain name for the connection to `example.com`, and the default lease time to `86400` seconds (1 day).

.. Add the `authoritative` statement on a new line:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
authoritative;
....
+
[IMPORTANT]
====
Without the `authoritative` statement, the `dhcpd` service does not answer `DHCPREQUEST` messages with `DHCPNAK` if a client asks for an address that is outside of the pool.
====

.. Add a `shared-network` declaration, such as the following, for IPv6 subnets that are not directly connected to an interface of the server:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
shared-network _example_ {
  option domain-name-servers 2001:db8:0:1::1:1
  ...

  subnet6 2001:db8:0:1::1:0/120 {
    range6 2001:db8:0:1::1:20 2001:db8:0:1::1:100
  }

  subnet6 2001:db8:0:1::2:0/120 {
    range6 2001:db8:0:1::2:20 2001:db8:0:1::2:100
  }
  ...
}
....
+
This example adds a shared network declaration that contains a `subnet6` declaration for both the 2001:db8:0:1::1:0/120 and 2001:db8:0:1::2:0/120 networks. With this configuration, the DHCP server assigns the following settings to a client that sends a DHCP request from one of these subnets:
+
** The IP of the DNS server for clients from both subnets is `2001:db8:0:1::1:1`.
** A free IPv6 address from the range defined in the `range6` parameter, depending on from which subnet the client sent the request.
+

Note that IPv6 requires uses router advertisement messages to identify the default gateway.

.. Add a `subnet6` declaration for the subnet the server is directly connected to and that is used to reach the remote subnets specified in `shared-network` above:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
subnet6 2001:db8:0:1::50:0/120 {
}
....
+
[NOTE]
====
If the server does not provide DHCP service to this subnet, the `subnet6` declaration must be empty as shown in the example. Without a declaration for the directly connected subnet, `dhcpd` does not start.
====

. Optionally, configure that `dhcpd6` starts automatically when the system boots:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# **systemctl enable dhcpd6**
....

. Start the `dhcpd6` service:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# **systemctl start dhcpd6**
....

[role="_additional-resources"]
.Additional resources
* `dhcp-options(5)` man page
* `dhcpd.conf(5)` man page
* `/usr/share/doc/dhcp-server/dhcpd.conf.example` file
* `/usr/share/doc/dhcp-server/dhcpd6.conf.example` file
ifdef::net-infrastructure-services-title[]
* xref:setting-up-a-dhcp-relay-agent_{context}[Setting up a DHCP relay agent]
endif::[]
ifndef::net-infrastructure-services-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_networking_infrastructure_services/providing-dhcp-services_networking-infrastructure-services#setting-up-a-dhcp-relay-agent_providing-dhcp-services[Setting up a DHCP relay agent]
endif::[]

