:_mod-docs-content-type: PROCEDURE
[id="temporarily-setting-the-current-qdisc-of-a-network-interface-using-the-tc-utility_{context}"]

= Temporarily setting the current qdisc of a network interface using the tc utility

[role="_abstract"]
You can update the current `qdisc` without changing the default one.


.Procedure

. Optional: View the current `qdisc`:
+
[literal,subs="+quotes"]
....
# **tc -s qdisc show dev _enp0s1_**
....

. Update the current `qdisc`:
+
[literal,subs="+quotes"]
....
# **tc qdisc replace dev _enp0s1_ root _htb_**
....


.Verification

* View the updated current `qdisc`:
+
[literal,subs="+quotes"]
----
# **tc -s qdisc show dev _enp0s1_**
qdisc htb 8001: root refcnt 2 r2q 10 default 0 direct_packets_stat 0 direct_qlen 1000
Sent 0 bytes 0 pkt (dropped 0, overlimits 0 requeues 0)
backlog 0b 0p requeues 0
----

