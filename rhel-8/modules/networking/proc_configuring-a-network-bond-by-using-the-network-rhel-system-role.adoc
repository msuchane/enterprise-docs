:_mod-docs-content-type: PROCEDURE
:experimental:

[id="proc_configuring-a-network-bond-by-using-network-rhel-system-role_{context}"]
= Configuring a network bond by using the `network` {RHELSystemRoles}

[role="_abstract"]
You can remotely configure a network bond by using the `network` {RHELSystemRoles}.


.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]
// The following are prerequisites that are specific for this task:
* Two or more physical or virtual network devices are installed on the server.


.Procedure

. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Configure the network
  hosts: managed-node-01.example.com
  tasks:
    - name: Configure a network bond that uses two Ethernet ports
      ansible.builtin.include_role:
        name: rhel-system-roles.network
      vars:
        network_connections:
          # Define the bond profile
          - name: bond0
            type: bond
            interface_name: bond0
            ip:
              address:
                - "192.0.2.1/24"
                - "2001:db8:1::1/64"
              gateway4: 192.0.2.254
              gateway6: 2001:db8:1::fffe
              dns:
                - 192.0.2.200
                - 2001:db8:1::ffbb
              dns_search:
                - example.com
            bond:
              mode: active-backup
            state: up

          # Add an Ethernet profile to the bond
          - name: bond0-port1
            interface_name: enp7s0
            type: ethernet
            controller: bond0
            state: up

          # Add a second Ethernet profile to the bond
          - name: bond0-port2
            interface_name: enp8s0
            type: ethernet
            controller: bond0
            state: up
....
+
These settings define a network bond with the following settings:
+
* A static IPv4 address - `192.0.2.1` with a `/24` subnet mask
* A static IPv6 address - `2001:db8:1::1` with a `/64` subnet mask
* An IPv4 default gateway - `192.0.2.254`
* An IPv6 default gateway - `2001:db8:1::fffe`
* An IPv4 DNS server - `192.0.2.200`
* An IPv6 DNS server - `2001:db8:1::ffbb`
* A DNS search domain - `example.com`
* Ports of the bond - `enp7s0` and `enp8s0`
* Bond mode - `active-backup`
+
[NOTE]
====
Set the IP configuration on the bond and not on the ports of the Linux bond.
====


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file
* `/usr/share/doc/rhel-system-roles/network/` directory
