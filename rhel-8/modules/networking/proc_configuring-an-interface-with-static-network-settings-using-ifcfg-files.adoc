:_mod-docs-content-type: PROCEDURE

[id="configuring-an-interface-with-static-network-settings-using-ifcfg-files_{context}"]
= Configuring an interface with static network settings using ifcfg files

[role="_abstract"]
If you do not use the NetworkManager utilities and applications, you can manually configure a network interface by creating `ifcfg` files.

.Procedure

* To configure an interface with static network settings using `ifcfg` files, for an interface with the name `enp1s0`, create a file with the name `ifcfg-enp1s0` in the `/etc/sysconfig/network-scripts/` directory that contains:

** For `IPv4` configuration:
+
[source]
....
DEVICE=enp1s0
BOOTPROTO=none
ONBOOT=yes
PREFIX=24
IPADDR=192.0.2.1
GATEWAY=192.0.2.254
....

** For `IPv6` configuration:
+
[source]
....
DEVICE=enp1s0
BOOTPROTO=none
ONBOOT=yes
IPV6INIT=yes
IPV6ADDR=2001:db8:1::2/64

....

[role="_additional-resources"]
.Additional resources
* `nm-settings-ifcfg-rh(5)` man page

