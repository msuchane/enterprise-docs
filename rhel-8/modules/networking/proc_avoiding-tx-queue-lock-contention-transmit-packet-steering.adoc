:_mod-docs-content-type: PROCEDURE

[id="avoiding-tx-queue-lock-contention-transmit-packet-steering_{context}"]
= Avoiding TX queue lock contention: Transmit packet steering
In hosts with a network interface controller (NIC) that supports multiple queues, transmit packet steering (XPS) distributes the processing of outgoing network packets among several queues. This enables multiple CPUs to process the outgoing network traffic and to avoid transmit queue lock contention and, consequently, packet drops.

Certain drivers, such as `ixgbe`, `i40e`, and `mlx5` automatically configure XPS. To identify if the driver supports this capability, consult the documentation of your NIC driver.
Consult your NIC driver's documentation to identify if the driver supports this capability. If the driver does not support XPS auto-tuning, you can manually assign CPU cores to the transmit queues.

[NOTE]
====
{ProductName} does not provide an option to permanently assign transmit queues to CPU cores. Use the commands in a script and run it when the system boots.
====


.Prerequisites
* The NIC supports multiple queues.
* The `numactl` package is installed.


.Procedure

. Display the count of available queues:
+
[literal,subs="+quotes"]
....
# *ethtool -l _enp1s0_*
Channel parameters for _enp1s0_:
Pre-set maximums:
RX:		_0_
TX:		_0_
Other:		_0_
Combined:	_4_
Current hardware settings:
RX:		_0_
TX:		_0_
Other:		_0_
Combined:	_1_
....
+
The `Pre-set maximums` section shows the total number of queues and `Current hardware settings` the number of queues that are currently assigned to the receive, transmit, other, or combined queues.

. Optional: If you require queues on specific channels, assign them accordingly. For example, to assign the 4 queues to the `Combined` channel, enter:
+
[literal,subs="+quotes"]
....
# *ethtool -L _enp1s0_ combined 4*
....

. Display to which Non-Uniform Memory Access (NUMA) node the NIC is assigned:
+
[literal,subs="+quotes"]
....
# *cat /sys/class/net/_enp1s0_/device/numa_node*
0
....
+
If the file is not found or the command returns `-1`, the host is not a NUMA system.

. If the host is a NUMA system, display which CPUs are assigned to which NUMA node:
+
[literal,subs="+quotes"]
....
# *lscpu | grep NUMA*
NUMA node(s):       2
NUMA node0 CPU(s):  0-3
NUMA node1 CPU(s):  4-7
....

. In the example above, the NIC has 4 queues and the NIC is assigned to NUMA node 0. This node uses the CPU cores 0-3. Consequently, map each transmit queue to one of the CPU cores from 0-3:
+
[literal,subs="+quotes"]
....
# *echo _1_ > /sys/class/net/_enp1s0_/queues/tx-_0_/xps_cpus*
# *echo _2_ > /sys/class/net/_enp1s0_/queues/tx-_1_/xps_cpus*
# *echo _4_ > /sys/class/net/_enp1s0_/queues/tx-_2_/xps_cpus*
# *echo _8_ > /sys/class/net/_enp1s0_/queues/tx-_3_/xps_cpus*
....
+
If the number of CPU cores and transmit (TX) queues is the same, use a 1 to 1 mapping to avoid any kind of contention on the TX queue. Otherwise, if you map multiple CPUs on the same TX queue, transmit operations on different CPUs will cause TX queue lock contention and negatively impacts the transmit throughput.
+
Note that you must pass the bitmap, containing the CPU's core numbers, to the queues. Use the following command to calculate the bitmap:
+
[literal,subs="+quotes"]
....
# *printf %x $((1 << __<core_number>__ ))*
....


.Verification

. Identify the process IDs (PIDs) of services that send traffic:
+
[literal,subs="+quotes"]
....
# *pidof _<process_name>_*
_12345_ _98765_
....

. Pin the PIDs to cores that use XPS:
+
[literal,subs="+quotes"]
....
# *numactl -C _0-3_ _12345_ _98765_*
....

. Monitor the `requeues` counter while the process send traffic:
+
[literal,subs="+quotes"]
....
# *tc -s qdisc*
_qdisc fq_codel 0: dev enp10s0u1 root refcnt 2 limit 10240p flows 1024 quantum 1514 target 5ms interval 100ms memory_limit 32Mb ecn drop_batch 64_
 Sent _125728849_ bytes _1067587_ pkt (dropped _0_, overlimits _0_ requeues _30_)
 backlog _0b 0p_ requeues _30_
 ...
....
+
If the `requeues` counter no longer increases at a significant rate, TX queue lock contention no longer happens.


[role="_additional-resources"]
.Additional resources
* `/usr/share/doc/kernel-doc-_<version>/Documentation/networking/scaling.rst`

