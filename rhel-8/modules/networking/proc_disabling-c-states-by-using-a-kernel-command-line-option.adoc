:_mod-docs-content-type: PROCEDURE

[id="disabling-c-states-by-using-a-kernel-command-line-option_{context}"]
= Disabling C-states by using a kernel command line option
The `processor.max_cstate` and `intel_idle.max_cstat` kernel command line parameters configure the maximum consumption states (C-state) CPU cores can use. For example, setting the parameters to `1` ensures that the CPU will never request a C-state below C1.

Use this method to test whether the latency of applications on a host are being affected by C-states. To not hard code a specific state, consider using a more dynamic solution. See xref:disabling-c-states-by-using-a-custom-tuned-profile_improving-the-network-latency[Disabling C-states by using a custom TuneD profile].


.Prerequisites

* The `tuned` service is not running or configured to not update C-state settings.


.Procedure

. Display the idle driver the system uses:
+
[literal,subs="+quotes"]
....
# *cat /sys/devices/system/cpu/cpuidle/current_driver*
__intel_idle__
....

. If the host uses the `intel_idle` driver, set the `intel_idle.max_cstate` kernel parameter to define the highest C-state that CPU cores should be able to use:
+
[literal,subs="+quotes"]
....
# *grubby --update-kernel=ALL --args="intel_idle.max_cstate=_0_"*
....
+
Setting `intel_idle.max_cstate=0` disables the `intel_idle` driver. Consequently, the kernel uses the `acpi_idle` driver that uses the C-state values set in the EFI firmware. For this reason, also set `processor.max_cstate` to override these C-state settings.

. On every host, independent from the CPU vendor, set the highest C-state that CPU cores should be able to use:
+
[literal,subs="+quotes"]
....
# *grubby --update-kernel=ALL --args="processor.max_cstate=_0_"*
....
+
[IMPORTANT]
====
If you set `processor.max_cstate=0` in addition to `intel_idle.max_cstate=0`, the `acpi_idle` driver overrides the value of `processor.max_cstate` and sets it to `1`. As a result, with `processor.max_cstate=0 intel_idle.max_cstate=0`, the highest C-state the kernel will use is C1, not C0.
====

. Restart the host for the changes to take effect:
+
[literal,subs="+quotes"]
....
# *reboot*
....


.Verification

. Display the maximum C-state:
+
[literal,subs="+quotes"]
....
# *cat /sys/module/processor/parameters/max_cstate*
1
....

. If the host uses the `intel_idle` driver, display the maximum C-state:
+
[literal,subs="+quotes"]
....
# *cat /sys/module/intel_idle/parameters/max_cstate*
0
....


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/202743[What are CPU "C-states" and how to disable them if needed?]
* `/usr/share/doc/kernel-doc-__<version>__/Documentation/admin-guide/pm/cpuidle.rst` provided by the `kernel-doc` package

