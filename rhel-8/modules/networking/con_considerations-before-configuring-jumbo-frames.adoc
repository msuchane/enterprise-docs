:_mod-docs-content-type: CONCEPT

[id="considerations-before-configuring-jumbo-frames_{context}"]
= Considerations before configuring jumbo frames

Depending on your hardware, applications, and services in your network, jumbo frames can have different impacts. Decide carefully whether enabling jumbo frames provides a benefit in your scenario.


.Prerequisites

All network devices on the transmission path must support jumbo frames and use the same Maximum Transmission Unit (MTU) size. In the opposite case, you can face the following problems:

* Dropped packets.
* Higher latency due to fragmented packets.
* Increased risk of packet loss caused by fragmentation. For example, if a router fragments a single 9000-bytes frame into six 1500-bytes frames, and any of those 1500-byte frames are lost, the whole frame is lost because it cannot be reassembled.

In the following diagram, all hosts in the three subnets must use the same MTU if a host from network A sends a packet to a host in network C:

image::network-diagram-MTU.png[]


.Benefits of jumbo frames

* Higher throughput: Each frame contains more user data while the protocol overhead is fixed.
* Lower CPU utilization: Jumbo frames cause fewer interrupts and, therefore, save CPU cycles.


.Drawbacks of jumbo frames

* Higher latency: Larger frames delay packets that follow.
* Increased memory buffer usage: Larger frames can fill buffer queue memory more quickly.

