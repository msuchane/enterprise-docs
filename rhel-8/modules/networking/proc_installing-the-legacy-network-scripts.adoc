:_mod-docs-content-type: PROCEDURE

[id="proc_installing-the-legacy-network-scripts{context}"]
= Installing the legacy network scripts

[role="_abstract"]
If you require the deprecated network scripts that processes the network configuration without using NetworkManager, you can install them. In this case, the `/usr/sbin/ifup` and `/usr/sbin/ifdown` scripts link to the deprecated shell scripts that manage the network configuration.



.Procedure

* Install the `network-scripts` package:
+
[literal,subs="+quotes,attributes"]
....
# **{PackageManagerCommand} install network-scripts**
....

