:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-a-static-ethernet-connection-with-802-1x-network-authentication-by-using-nmstatectl_{context}"]
= Configuring a static Ethernet connection with 802.1X network authentication by using `nmstatectl`

[role="_abstract"]
Use the `nmstatectl` utility to configure an Ethernet connection with 802.1X network authentication through the Nmstate API. The Nmstate API ensures that, after setting the configuration, the result matches the configuration file. If anything fails, `nmstatectl` automatically rolls back the changes to avoid leaving the system in an incorrect state.

[NOTE]
====
The `nmstate` library only supports the `TLS` Extensible Authentication Protocol (EAP) method.
====



.Prerequisites

* The network supports 802.1X network authentication.
* The managed node uses NetworkManager.
* The following files required for TLS authentication exist on the client:
** The client key stored is in the `/etc/pki/tls/private/client.key` file, and the file is owned and only readable by the `root` user.
** The client certificate is stored in the `/etc/pki/tls/certs/client.crt` file.
** The Certificate Authority (CA) certificate is stored in the `/etc/pki/tls/certs/ca.crt` file.


.Procedure

. Create a YAML file, for example `~/create-ethernet-profile.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
interfaces:
- name: __enp1s0__
  type: __ethernet__
  state: __up__
  ipv4:
    enabled: __true__
    address:
    - ip: __192.0.2.1__
      prefix-length: __24__
    dhcp: __false__
  ipv6:
    enabled: __true__
    address:
    - ip: __2001:db8:1::1__
      prefix-length: __64__
    autoconf: __false__
    dhcp: __false__
  802.1x:
    ca-cert: __/etc/pki/tls/certs/ca.crt__
    client-cert: __/etc/pki/tls/certs/client.crt__
    eap-methods:
      - tls
    identity: __client.example.org__
    private-key: __/etc/pki/tls/private/client.key__
    private-key-password: __password__
routes:
  config:
  - destination: __0.0.0.0/0__
    next-hop-address: __192.0.2.254__
    next-hop-interface: __enp1s0__
  - destination: __::/0__
    next-hop-address: __2001:db8:1::fffe__
    next-hop-interface: __enp1s0__
dns-resolver:
  config:
    search:
    - __example.com__
    server:
    - __192.0.2.200__
    - __2001:db8:1::ffbb__
....
+
These settings define an Ethernet connection profile for the `enp1s0` device with the following settings:
+
* A static IPv4 address - `192.0.2.1` with a `/24` subnet mask
* A static IPv6 address - `2001:db8:1::1` with a `/64` subnet mask
* An IPv4 default gateway - `192.0.2.254`
* An IPv6 default gateway - `2001:db8:1::fffe`
* An IPv4 DNS server - `192.0.2.200`
* An IPv6 DNS server - `2001:db8:1::ffbb`
* A DNS search domain - `example.com`
* 802.1X network authentication using the `TLS` EAP protocol


. Apply the settings to the system:
+
[literal,subs="+quotes"]
....
# **nmstatectl apply ~/create-ethernet-profile.yml**
....



.Verification

* Access resources on the network that require network authentication. 

