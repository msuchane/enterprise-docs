:_newdoc-version: 2.16.0
:_template-generated: 2024-03-01
:_mod-docs-content-type: PROCEDURE

[id="configuring-a-host-to-host-ipsec-vpn-with-pki-authentication-and-tunnel-mode-by-using-nmstatectl_{context}"]
= Configuring a host-to-host IPsec VPN  with PKI authentication and  tunnel mode by using nmstatectl

[role="_abstract"]
IPsec (Internet Protocol Security) is a security protocol suite to authenticate and encrypt IP communications within networks and devices. The `Libreswan` software  provides an IPsec implementation for VPNs. 

In tunnel mode, the source and destination IP address of communication is encrypted in the IPsec tunnel. External network sniffers can only get left IP and right IP. In general, for tunnel mode, it supports `host-to-host`, `host-to-subnet`, and `subnet-to-subnet`. In this mode, a new IP packet encapsulates an existing packet along with its payload and header. Encapsulation in this mode protects IP data, source, and destination headers over an unsecure network. This mode is useful to transfer data in `subnet-to-subnet`, remote access connections, and untrusted networks, such as open public Wi-Fi networks. By default, IPsec establishes a secure channel between two sites in tunnel mode. With the following configuration, you can establish a VPN connection as a `host-to-host` architecture.     

By using Nmstate, a declarative API for network management, you can configure an IPsec VPN connection. After setting the configuration, the Nmstate API ensures that the result matches with the configuration file. If anything fails, `nmstate` automatically rolls back the changes to avoid incorrect state of the system.

In `host-to-host` configuration, you need to set `leftmodecfgclient: no` so that it can’t receive network configuration from the server, hence the value `no`. In the case of defining systems for `IPsec` in nmstate, the `left`-named system is the local host while the `right`-named system is the remote host. The following procedure needs to run on both hosts.

.Prerequisites

* By using a password, you have generated a PKCS #12 file that stores certificates and cryptographic keys.

.Procedure

. Install the required packages:
+
[subs="+quotes"]
----
# **dnf install nmstate libreswan NetworkManager-libreswan**
----

. Restart the NetworkManager service:
+
[subs="+quotes"]
----
# **systemctl restart NetworkManager**
----

. As `Libreswan` was already installed, remove its old database files and re-create them:
+
[subs="+quotes"]
----
# **systemctl stop ipsec**
# **rm /etc/ipsec.d/*db**
# **ipsec initnss**
----

. Import the PKCS#12 file:
+
[subs="+quotes"]
----
# **ipsec import node-example.p12**
----
+
When importing the PKCS#12 file, enter the password that was used to generate the file.

. Enable and start the `ipsec` service:
+
[subs="+quotes"]
----
# **systemctl enable --now ipsec**
----

. Create a YAML file, for example `~/create-p2p-vpn-authentication.yml`, with the following content:
+
[source,yaml]
----
---
interfaces:
- name: 'example_ipsec_conn1'             <1>
  type: ipsec
  libreswan:
    left: '192.0.2.250'                   <2>
    leftid:  'local-host.example.com'     <3>
    leftcert: 'local-host.example.com'    <4>
    leftmodecfgclient: 'no'               <5>
    right: '192.0.2.150'                  <6>
    rightid: 'remote-host.example.com'    <7>
    rightsubnet: '192.0.2.150/32'         <8>
    ikev2: 'insist'                       <9>

----
The YAML file defines the following settings:				
+
<1> An IPsec connection name
<2> A static IPv4 address of public network interface for a local host
<3> A distinguished Name (DN) of a local host
<4> A certificate name installed on a local host
<5> The value for not to retrieve client configuration from a remote host 
<6> A static IPv4 address of public network interface for a remote host
<7> A distinguished Name (DN) of a remote host
<8> The subnet range of a remote host - `192.0.2.150` with `32` IPv4 addresses
<9> The value to accept and receive only the Internet Key Exchange (IKEv2) protocol

. Apply the settings to the system:
+
[subs="+quotes"]
----
# **nmstatectl apply ~/create-p2p-vpn-authentication.yml**
----

.Verification

. Display the created P2P policy:
+
[subs="+quotes"]
----
# **ip xfrm policy**
----

. Verify IPsec status:
+
[subs="+quotes"]
----
# **ip xfrm status**
----

[role="_additional-resources"]
.Additional resources
* `ipsec.conf(5)` man page

