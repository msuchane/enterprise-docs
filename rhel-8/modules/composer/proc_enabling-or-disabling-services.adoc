:_mod-docs-content-type: PROCEDURE

[id="enabling-or-disabling-services_{context}"]
= Enabling or disabling services

[role="_abstract"]
You can control which services to enable during the boot time. Some image types already have services enabled or disabled to ensure that the image works correctly and you cannot override this setup. The `[customizations.services]` settings in the blueprint do not replace these services, but add the services to the list of services already present in the image templates.

.Procedure

* Customize which services to enable during the boot time:
+
[subs="quotes,attributes"]
----
[customizations.services]
enabled = _["SERVICES"]_
disabled = _["SERVICES"]_
----
+
For example:
+
[subs="quotes,attributes"]
----
[customizations.services]
enabled = ["sshd", "cockpit.socket", "httpd"]
disabled = ["postfix", "telnetd"]
----

