:_mod-docs-content-type: CONCEPT

[id="con_single-application-mode_{context}"]
= Single-application mode

[role="_abstract"]
Single-application mode is a modified GNOME session that reconfigures the Mutter window manager into an interactive kiosk. This session locks down certain behavior to make the standard desktop more restrictive. The user can interact only with a single application selected by the administrator.

You can set up single-application mode for several use cases, such as:

* In the communication, entertainment, or education fields
* As a self-serve machine
* As an event manager
* As a registration point

The `gnome-session-kiosk-session` package provides the single-application mode configuration and sessions in {PRODUCT}.

// [role="_additional-resources"]
// .Additional resources
// * A bulleted list of links to other closely-related material. These links can include `link:` and `xref:` macros.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

