:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_handling-printing.adoc

[id="printing-a-test-page-new_{context}"]
= Printing a test page in the Setting application

[role="_abstract"]
You can print a test page to make sure that the printer functions properly.

You might want to print a test page if one of the below prerequisites is met.

.Prerequisites

* A printer has been set up.
* A printer configuration has been changed.

.Procedure

. Click the settings (⚙️) button on the right to display a settings menu for the selected printer:
+
image::gnome-control-center-printer-settings.png[]

. Click menu:Printing Options[Test Page].
