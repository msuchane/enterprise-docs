:_mod-docs-content-type: PROCEDURE
[id="authorizing-users-of-braille-display-device_{context}"]
= Authorizing users of a Braille display device

To use a Braille display device, you must set the users who are authorized to use the Braille display device.

.Procedure

. In the [filename]`/etc/brltty.conf` file, ensure that `keyfile` is set to `/etc/brlapi.key`:
+
----
api-parameters Auth=keyfile:/etc/brlapi.key
----
+
This is the default value. Your organization might have overridden it.

. Authorize the selected users by adding them to the `brlapi` group:
+
[subs="+quotes"]
----
# usermod --append -G brlapi __user-name__
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/managing-users-and-groups_configuring-basic-system-settings#editing-user-groups-using-the-command-line_managing-users-and-groups[Editing user groups using the command line]
