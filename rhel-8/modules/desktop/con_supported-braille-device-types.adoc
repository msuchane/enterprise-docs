:_mod-docs-content-type: CONCEPT
[id="supported-braille-device-types_{context}"]
= Supported types of Braille display device

The following types of Braille display devices are supported on {PRODUCT}.

.Braille display device types and the corresponding syntax
[options="header"]
|=======
| Braille device type | Syntax of the type | Note
| Serial device | `serial:path` | Relative paths are at [filename]`/dev`.
| USB device    | `[serial-number]` | The brackets (`[]`) here indicate optionality.
| Bluetooth device | `bluetooth:address` |
|=======
