// Module included in the following assemblies:
//
// assemblies/assembly_configuring-the-screen-rotation.adoc 

:experimental:

[id="proc_configuring-the-screen-rotation-single-user_{context}"]
= Configuring the screen rotation for a single user

[role="_abstract"]
This procedure sets screen rotation for the current user.

.Procedure

. Go to the *system menu*, which is accessible from the top-right screen corner, and click the *Settings* icon.
+
image:gnome-system-menu.png[System menu]

ifeval::[{ProductNumber} == 8]
. In the menu:Settings[Devices] section, choose *Displays*. 
endif::[]
ifeval::[{ProductNumber} == 9]
. In the menu:Settings[Devices] section, choose *Displays*.  
endif::[]

. Configure the rotation using the *Orientation* field. 

. Confirm your choice by clicking btn:[Apply].

. If you are satisfied with the new setup preview, click on btn:[Keep changes].

The setting persists to your next login.

[role="_additional-resources"]
.Additional resources
* For information about rotating the screen for all users on a system, see xref:proc_configuring-the-screen-rotation-all-users_configuring-the-screen-rotation[Configuring the screen rotation for all users].
