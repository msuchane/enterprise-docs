:_mod-docs-content-type: PROCEDURE

[id="customizing-the-branding-of-the-login-screen_{context}"]
= Customizing the branding of the login screen

[role="_abstract"]
You can change the logo that appears on the GNOME login screen (GDM) using a `dconf` profile.

.Prerequisites

* Prepare an image file in any of the supported formats: ANI, BPM, GIF, GTIFF, ICNS, ICO, JPEG, JPEG 2000, PCX, PNM, PBM, PGM, PPM, RAS, SVG, TGA, TIFF, WBMP, XBM, or XPM.
* The image must be around 48 pixels in height. If it is significantly larger, it will exceed the logo area.
* Store the image file in a location that the `gdm` user can access. For example, select a universally readable system directory such as `/opt/` or `/usr/local/`.

.Procedure

. Create the `/etc/dconf/db/gdm.d/01-override-logo` configuration file with the following content:
+
----
[org/gnome/login-screen]
logo='/path/to/logo.png'
----
+
Replace `__/path/to/logo.png__` with the full path to the image file that you want to use as the login screen logo.

. Update the system databases:
+
----
# dconf update
----

.Verification

. Log out or otherwise switch to the login screen.

. Check if it displays the logo that you selected.

. If the logo does not update, restart GDM:
+
----
# systemctl restart gdm
----
+
include::common-content/snip_restarting-gdm-warning.adoc[leveloffset=+1]

////
[role="_additional-resources"]
.Additional resources

* This section is optional.
* Provide a bulleted list of links to other closely-related material. These links can include `link:` and `xref:` macros.
* Use an unnumbered bullet (*) if the list includes only one step.
////
