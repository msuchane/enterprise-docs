:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_handling-printing.adoc

:experimental:

[id="setting-print-options_gcc-desktop_{context}"]
= Setting print options using the *Settings* application

[role="_abstract"]
You can set print options using the [application]*Settings* application.

.Procedure

. Click the settings (⚙️) button on the right to display a settings menu for the selected printer:
+
image::gnome-control-center-printer-settings.png[]

. Click *Printing Options*.
