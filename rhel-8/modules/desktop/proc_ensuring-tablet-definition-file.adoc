:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="ensuring-tablet-definition-file_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Preparing a tablet definition file
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

The `libwacom` tablet client library needs a definition file for the tablet that you want to add.

To ensure that the tablet definition file exists, follow this procedure. 

.Prerequisites

* List all local devices recognized by `libwacom`:
+
....
$ libwacom-list-local-devices
....
+
Make sure that your device is recognized in the output.
+
If your device is not listed, the device is missing from the `libwacom` database. However, the device might still be visible as an event device in the kernel under [filename]`/proc/bus/input/devices`, and if you use the *X.Org* display server, in the *X11* session on the `xinput` list.

// If your device is not listed, but it is available as an event device in the kernel under [filename]`/proc/bus/input/devices` and, in case of the *X.Org* display server, in the `X session` on the `xinput` list, the device is missing from libwacom's database.

.Procedure

. Install the package that provides tablet definition files:
+
[subs="+attributes"]
----
# {PackageManagerCommand} install libwacom-data
----
+
The package installs tablet definitions in the [filename]`/usr/share/libwacom/` directory.

. Check whether the definition file is available in the [filename]`/usr/share/libwacom/` directory.
+
To use the screen mapping correctly, support for your tablet must be included in the `libwacom` database and in the `udev` rules file.
+
[IMPORTANT]
====
A common indicator that a device is not supported by `libwacom` is that it works normally in a GNOME session, but the device is not correctly mapped to the screen. 
====

. If the definition file for your device is not available in [filename]`/usr/share/libwacom/`, you have these options:

** The required definition file may already be available in the link:https://github.com/linuxwacom/libwacom[linuxwacom/libwacom] upstream repository. You can try to find the definition file there. If you find your tablet model in the list, copy the file to the local machine. 

** You can create a new tablet definition file. Use the [filename]`data/wacom.example` file below, and edit particular lines based on the characteristics of your device.
+
.Example model file description for a tablet
====

....
[Device]

# The product is the product name announced by the kernel
Product=Intuos 4 WL 6x9

# Vendor name of this tablet
Vendor=Wacom

# DeviceMatch includes the bus (usb, serial), the vendor ID and the actual
# product ID 
DeviceMatch=usb:056a:00bc

# Class of the tablet. Valid classes include Intuos3, Intuos4, Graphire, Bamboo, Cintiq
Class=Intuos4

# Exact model of the tablet, not including the size.
Model=Intuos 4 Wireless

# Width in inches, as advertised by the manufacturer
Width=9

# Height in inches, as advertised by the manufacturer
Height=6

# Optional features that this tablet supports
# Some features are dependent on the actual tool used, e.g. not all styli
# have an eraser and some styli have additional custom axes (e.g. the
# airbrush pen). These features describe those available on the tablet.
#
# Features not set in a file default to false/0

[Features]
# This tablet supports styli (and erasers, if present on the actual stylus)
Stylus=true

# This tablet supports touch.
Touch=false

# This tablet has a touch ring (Intuos4 and Cintiq 24HD)
Ring=true
# This tablet has a second touch ring (Cintiq 24HD)
Ring2=false

# This tablet has a vertical/horizontal scroll strip
VStrip=false
HStrip=false

# Number of buttons on the tablet
Buttons=9

# This tablet is built-in (most serial tablets, Cintiqs) 
BuiltIn=false
....

====

// .Additional resources

// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * Currently, modules cannot include xrefs, so you cannot include links to other content in your collection. If you need to link to another assembly, add the xref to the assembly that includes this module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

