:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="troubleshooting-a-busy-disk-in-gnome_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Troubleshooting a busy disk in GNOME
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
If you receive a notification about your disk being busy, determine the programs that are accessing the disk.
Then, you can end the programs that are running. You can also use the *System Monitor* application to kill the programs forcefully.

.Prerequisites
* The `iotop` utility is installed:
+
[subs="+attributes"]
----
# {PackageManagerCommand} install iotop
----

.Procedure
. Examine the list of open files.
** Run the `lsof` command to get the list of open files.
** If `lsof` is not available, run the `ps ax` command.
** You can use *System Monitor* to display the running processes in a GUI.

. When you have determined the programs, terminate them using any of the following methods:
** On the command line, execute the `kill` command.
** In *System Monitor*, right-click the line with the program process name, and click *End* or *Kill* from the context menu.

[role="_additional-resources"]
.Additional resources
* The `kill` man page.
