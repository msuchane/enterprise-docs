:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="the-gvfs-system_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= The GVFS system

[role="_abstract"]
GVFS provides complete virtual file system infrastructure and handles storage in the GNOME desktop.
It uses addresses for full identification based on the Uniform Resource Identifier (URI) standard, syntactically similar to URL addresses in web browsers.
These addresses in the form of `_schema_://_user_@_server/path_` are the key information determining the kind of service.

GVFS helps to mount the resources. These mounts are shared between multiple applications. Resources are tracked globally within the running desktop session, which means that even if you quit an application that triggered the mount, the mount continues to be available for any other application. Multiple applications can access the mount at the same time unless it is limited by a back end. Some protocols by design permit only a single channel.

GVFS mounts removable media in the `/run/media/` directory.
