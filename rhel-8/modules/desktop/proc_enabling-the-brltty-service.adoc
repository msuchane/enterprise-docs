:_mod-docs-content-type: PROCEDURE
[id="enabling-the-brltty-service_{context}"]
= Enabling the brltty service

To enable the Braille display, enable the `brltty` service to start automatically on boot. By default, `brltty` is disabled.

.Prerequisites

* Ensure that the `brltty` package is installed:
+
[subs="+attributes"]
----
# {PackageManagerCommand} install brltty
----

* Optionally, you can install speech synthesis support for `brltty`:
+
[subs="+attributes"]
----
# {PackageManagerCommand} install brltty-espeak-ng
----

.Procedure

* Enable the `brltty` service to start on boot:
+
----
# systemctl enable --now brltty
----

.Verification steps

. Reboot the system.
. Check that the `brltty` service is running:
+
----
# systemctl status brltty
● brltty.service - Braille display driver for Linux/Unix
   Loaded: loaded (/usr/lib/systemd/system/brltty.service; enabled; vendor pres>
   Active: active (running) since Tue 2019-09-10 14:13:02 CEST; 39s ago
  Process: 905 ExecStart=/usr/bin/brltty (code=exited, status=0/SUCCESS)
 Main PID: 914 (brltty)
    Tasks: 3 (limit: 11360)
   Memory: 4.6M
   CGroup: /system.slice/brltty.service
           └─914 /usr/bin/brltty
----

