:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="unmounting-a-storage-volume-in-gnome_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Unmounting a storage volume in GNOME
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
You can unmount a storage volume, a network share, or another resource in the *Files* application.

.Procedure
. Open the *Files* application.

. In the side bar, click the *Unmount* (⏏) icon next to the chosen mount.

. Wait until the mount disappears from the side bar or a notification about the safe removal appears.
