:_mod-docs-content-type: PROCEDURE

[id="enabling-the-screen-reader_{context}"]
= Enabling the screen reader

You can enable the *Orca* screen reader in your desktop environment. The screen reader then reads the text displayed on the screen to improve accessibility.

.Procedure

* Enable the screen reader using either of the following ways:

** Press the kbd:[Super+Alt+S] keyboard shortcut.

** If the top panel shows the *Universal Access* menu, select *Screen Reader* in the menu.


.Verification

. Open an application with text content.

. Check that the screen reader reads the text in the application.
