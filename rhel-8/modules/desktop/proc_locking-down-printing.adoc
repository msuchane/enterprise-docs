:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_locking-down-printing.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.

[id="locking-down-printing_{context}"]
= Locking down printing

You can disable the print dialog from being shown to users. This can be useful if you are giving temporary access to a user or you do not want the user to print to network printers.

[IMPORTANT]
====
The feature only works in applications which support it. Not all GNOME and third party applications have this feature enabled. The changes do not have effect on applications which do not support this feature.
====

To prevent applications from printing, lock down the `org.gnome.desktop.lockdown.disable-printing` key:

.Procedure

. Create the user profile in [filename]`/etc/dconf/profile/user` unless it already exists:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
user-db:user
system-db:local
....

. Create a local database for machine-wide settings in the [filename]`etc/dconf/db/local.d/00-lockdown` file:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
[org/gnome/desktop/lockdown]
      
# Prevent applications from printing
disable-printing=true
....

. Override the user's setting and prevent the user from changing it in the [filename]`/etc/dconf/db/local.d/locks/lockdown` file:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# List the keys used to configure lockdown
/org/gnome/desktop/lockdown/disable-printing
....

. Update the system databases:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# dconf update
....

Having followed these steps, applications supporting this lockdown key, such as [application]*Evolution*, [application]*Evince*, or [application]*Gedit*, will disable printing. 




