:_mod-docs-content-type: PROCEDURE
[id="launching-an-application-remotely-using-x11-forwarding_{context}"]
= Launching an application remotely using X11 forwarding

Access a graphical application on a {ProductShortName} server from a remote client using SSH.

.Prerequisites

* X11 forwarding over SSH is enabled on the server. For details, see xref:enabling-x11-forwarding-on-the-server_{context}[].

* Ensure that an X11 display server is running on your system:

** On {ProductShortName}, X11 is available by default in the graphical interface.
** On Microsoft Windows, install an X11 server such as Xming.
** On macOS, install the XQuartz X11 server.

* You have configured and restarted an OpenSSH server. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/assembly_using-secure-communications-between-two-systems-with-openssh_securing-networks#configuring-and-starting-an-openssh-server_assembly_using-secure-communications-between-two-systems-with-openssh[Configuring and starting an OpenSSH server].


.Procedure

. Log in to the server using SSH:
+
[subs=+quotes]
....
[local-user]$ *ssh -X -Y _remote-server_*
The authenticity of host 'remote-server (192.168.122.120)' can't be established.
ECDSA key fingerprint is SHA256:__uYwFlgtP/2YABMHKv5BtN7nHK9SHRL4hdYxAPJVK/kY__.
Are you sure you want to continue connecting (yes/no/[fingerprint])?
....

. Confirm that a server key is valid by checking its fingerprint.
+
[NOTE]
====
If you plan to log in to the server on a regular basis, add the user's public key to the server using the `ssh-copy-id` command.
====

. Continue connecting by typing *yes*.
+
[subs=+quotes]
....
Warning: Permanently added '__remote-server__' (ECDSA) to the list of known hosts.
....

. When prompted, type the server password.
+
[literal,subs=+quotes]
....
local-user's password:
[local-user ~]$
....

. Launch the application from the command line:
+
[subs=+quotes]
....
[remote-user]$ *_application-binary_*
....

[TIP]
====
To skip the intermediate terminal session, use the following command:

[subs=+quotes]
----
$ *ssh user@server -X -Y -C _binary_application_*
----
====
