:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/con_what-is-gnome-3.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.

[id="what-is-gnome-3_{context}"]
= What GNOME 3 is

{RHEL} 8 is shipped with the default desktop environment GNOME 3.

GNOME 3 represents a presentation layer that provides a graphical user interface as well as the focused working environment, which enables you to access all your work from one place.

include::common-content/snip_3d-acceleration-with-gnome.adoc[]


// This section included definitions of GNOME Standard and Classic, which are now explained in other modules. However, the definition of GTK remained only here, and it might be useful to create a new concept module just for it.
//
// [discrete]
// == GTK+
// 
// GTK+ is a multi-platform toolkit for creating graphical user interfaces, which provides a highly-usable feature-rich API. GTK+ enables GNOME 3 to change the look of an application or provide smooth appearance of graphics. In addition, GTK+ contains a number of features such as object-oriented programming support (GObject), wide support of international character sets and text layouts (Pango), or a set of accessibility interfaces (ATK). 
