:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_managing-storage-volumes-in-gnome.adoc

[id="password-management-of-gvfs-mounts_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Password management of GVFS mounts
// In the title of concept modules, include nouns or noun phrases that are used in the body text. This helps readers and search engines find the information quickly.
// Do not start the title of concept modules with a verb. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
Learn more about the GVFS mount authentication.

A typical GVFS mount authenticates on its activation unless the resource allows anonymous authentication or does not require any authentication at all.

In a standard GTK+ dialog, you can choose whether to store the password.

When you select the persistent storage, the password is stored in the user keyring. *GNOME Keyring* is a central place for secrets storage.
The password is encrypted and automatically unlocked on desktop session start using the password provided on login. For protecting it by a different password, you can set the password at the first use.

The *Passwords and Keys* application helps to manage the stored password and *GNOME Keyring*. It allows removing individual records or changing passwords.
