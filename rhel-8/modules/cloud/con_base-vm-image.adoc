:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
// assembly_understanding-base-images-azure.adoc
//

[id="about-vm-images_{context}"]
= Using a custom base image

To manually configure a virtual machine (VM), first create a base (starter) VM image. Then, you can modify configuration settings and add the packages the VM requires to operate on the cloud. You can make additional configuration changes for your specific application after you upload the image.

To prepare a cloud image of RHEL, follow the instructions in the sections below. To prepare a Hyper-V cloud image of RHEL, see the link:https://docs.microsoft.com/en-us/azure/virtual-machines/linux/redhat-create-upload-vhd#prepare-a-red-hat-based-virtual-machine-from-hyper-v-manager[Prepare a Red Hat-based virtual machine from Hyper-V Manager].


// KVM Guest image no longer recommended.

////
The recommended base VM image to use for all public cloud platforms is the *{ProductName}{nbsp}{ProductNumber} KVM Guest Image*, which you download from the link:https://access.redhat.com/products/red-hat-enterprise-linux/[Red Hat Customer Portal]. The KVM Guest Image is preconfigured with the following cloud configuration settings.

* _The root account is disabled._ You temporarily enable root account access to make configuration changes and install packages that the cloud may require. This guide provides instructions for enabling root account access.
// * _A user account named `cloud-user` is pre-configured on the image._ The `cloud-user` account has sudo access.
* _The image has `cloud-init` installed and enabled._ `cloud-init` is a service that handles provisioning of the VM (or instance) at initial boot.

You can choose to use a custom {ProductName} ISO image; however, when using a custom ISO image, you may need to make additional configuration changes.
////

////
.Additional resources

link:https://access.redhat.com/products/red-hat-enterprise-linux/[Red Hat Enterprise Linux]
////
