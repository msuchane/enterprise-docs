:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_uploading-the-rhel-image-to-aws


[id="creating-the-vmimport-role_{context}"]
= Creating the vmimport role

To be able to import a RHEL virtual machine (VM) to Amazon Web Services (AWS) by using the VM Import service, you need to create the `vmimport` role.

For more information, see link:https://docs.aws.amazon.com/vm-import/latest/userguide/vmimport-image-import.html[Importing a VM as an image using VM Import/Export] in the Amazon documentation.

.Procedure

. Create a file named `trust-policy.json` and include the following policy. Save the file on your system and note its location.
+
[subs="+attributes"]
----
{
   "Version": "2012-10-17",
   "Statement": [
      {
         "Effect": "Allow",
         "Principal": { "Service": "vmie.amazonaws.com" },
         "Action": "sts:AssumeRole",
         "Condition": {
            "StringEquals":{
               "sts:Externalid": "vmimport"
            }
         }
      }
   ]
}
----

. Use the [command]`create role` command to create the `vmimport` role. Specify the full path to the location of the `trust-policy.json` file. Prefix `file://` to the path. For example:
+
[subs="+quotes,attributes"]
----
$ *aws iam create-role --role-name vmimport --assume-role-policy-document file:///home/sample/ImportService/trust-policy.json*
----

. Create a file named `role-policy.json` and include the following policy. Replace `s3-bucket-name` with the name of your S3 bucket.
+
[subs="+attributes"]
----
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Action":[
            "s3:GetBucketLocation",
            "s3:GetObject",
            "s3:ListBucket"
         ],
         "Resource":[
            "arn:aws:s3:::s3-bucket-name",
            "arn:aws:s3:::s3-bucket-name/*"
         ]
      },
      {
         "Effect":"Allow",
         "Action":[
            "ec2:ModifySnapshotAttribute",
            "ec2:CopySnapshot",
            "ec2:RegisterImage",
            "ec2:Describe*"
         ],
         "Resource":"*"
      }
   ]
}
----

. Use the [command]`put-role-policy` command to attach the policy to the role you created. Specify the full path of the `role-policy.json` file. For example:
+
[subs="+quotes,attributes"]
----
$ *aws iam put-role-policy --role-name vmimport --policy-name vmimport --policy-document file:///home/sample/ImportService/role-policy.json*
----

[role="_additional-resources"]
.Additional resources
 * link:https://docs.aws.amazon.com/vm-import/latest/userguide/vmimport-image-import.html[VM Import Service Role]
 * link:https://docs.aws.amazon.com/vm-import/latest/userguide/vmie_prereqs.html#vmimport-role[Required Service Role]
