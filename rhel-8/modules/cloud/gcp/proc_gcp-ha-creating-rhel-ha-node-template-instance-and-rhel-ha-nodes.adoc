:_mod-docs-content-type: PROCEDURE
[id="gcp-ha-creating-rhel-ha-node-template-instance-and-rhel-ha-nodes_{context}"]
= Creating an HA node template instance and HA nodes

Once you have configured an image from the snapshot, you can create a node template. Use this template to create all HA nodes. Complete the following steps to create the template and HA nodes.

.Procedure

. Create an instance template.
+
[subs="+quotes,attributes"]
----
$ *gcloud compute instance-templates create _InstanceTemplateName_ --can-ip-forward --machine-type n1-standard-2  --image _ConfiguredImageFromSnapshot_ --service-account _ServiceAccountEmailAddress_*
----
+
Example:
+
[subs="+quotes,attributes"]
----
[admin@localhost ~] $ *gcloud compute instance-templates create rhel-{ProductNumber}1-instance-template --can-ip-forward --machine-type n1-standard-2 --image rhel-{ProductNumber}1-gcp-image --service-account account@project-name-on-gcp.iam.gserviceaccount.com*
Created [https://www.googleapis.com/compute/v1/projects/project-name-on-gcp/global/instanceTemplates/rhel-{ProductNumber}1-instance-template].
NAME  MACHINE_TYPE   PREEMPTIBLE  CREATION_TIMESTAMP
rhel-{ProductNumber}1-instance-template   n1-standard-2          2018-07-25T11:09:30.506-07:00
----

. Create multiple nodes in one zone.
+
[subs="+quotes,attributes"]
----
# *gcloud compute instances create _NodeName01_ _NodeName02_ --source-instance-template _InstanceTemplateName_ --zone _RegionZone_ --network=_NetworkName_ --subnet=_SubnetName_*
----
+
Example:
+
[subs="+quotes,attributes"]
----
[admin@localhost ~] $ *gcloud compute instances create rhel81-node-01 rhel81-node-02 rhel81-node-03 --source-instance-template rhel-{ProductNumber}1-instance-template --zone us-west1-b --network=projectVPC --subnet=range0*
Created [https://www.googleapis.com/compute/v1/projects/project-name-on-gcp/zones/us-west1-b/instances/rhel81-node-01].
Created [https://www.googleapis.com/compute/v1/projects/project-name-on-gcp/zones/us-west1-b/instances/rhel81-node-02].
Created [https://www.googleapis.com/compute/v1/projects/project-name-on-gcp/zones/us-west1-b/instances/rhel81-node-03].
NAME            ZONE        MACHINE_TYPE   PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP    STATUS
rhel81-node-01  us-west1-b  n1-standard-2               10.10.10.4   192.230.25.81   RUNNING
rhel81-node-02  us-west1-b  n1-standard-2               10.10.10.5   192.230.81.253  RUNNING
rhel81-node-03  us-east1-b  n1-standard-2               10.10.10.6   192.230.102.15  RUNNING
----
