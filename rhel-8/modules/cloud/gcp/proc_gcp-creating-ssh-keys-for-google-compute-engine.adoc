:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_uploading-the-rhel-image-to-gcp.adoc
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id='doing-procedure-a']
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="creating-ssh-keys-for-google-compute-engine_{context}"]
= Creating SSH keys for Google Compute Engine

Perform the following procedure to generate and register SSH keys with GCE so that you can SSH directly into an instance by using its public IP address.

.Procedure

. Use the [command]`ssh-keygen` command to generate an SSH key pair for use with GCE.
+
[subs="+quotes"]
----
# *ssh-keygen -t rsa -f ~/.ssh/google_compute_engine*
----

. From the link:https://console.cloud.google.com/home/dashboard[GCP Console Dashboard page], click the *Navigation* menu to the left of the Google *Cloud Console banner* and select *Compute Engine* and then select *Metadata*.

. Click *SSH Keys* and then click *Edit*.

. Enter the output generated from the `~/.ssh/google_compute_engine.pub` file and click *Save*.
+
You can now connect to your instance by using standard SSH.
+
[subs="+quotes"]
----
# *ssh -i ~/.ssh/google_compute_engine _<username>_@_<instance_external_ip>_*
----

[NOTE]
====
You can run the [command]`gcloud compute config-ssh` command to populate your config file with aliases for your instances. The aliases allow simple SSH connections by instance name. For information about the [command]`gcloud compute config-ssh` command, see link:https://cloud.google.com/sdk/gcloud/reference/compute/config-ssh[gcloud compute config-ssh].
====


[role="_additional-resources"]
.Additional resources
 * link:https://cloud.google.com/sdk/gcloud/reference/compute/config-ssh[gcloud compute config-ssh]
 * link:https://cloud.google.com/compute/docs/instances/connecting-to-instance[Connecting to instances]
