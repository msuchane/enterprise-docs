:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_uploading-the-rhel-image-to-gcp.adoc
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id='doing-procedure-a']
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="connecting-to-your-instance_{context}"]
= Connecting to your instance

Perform the following procedure to connect to your GCE instance using its public IP address.

.Procedure

. Run the following command to ensure that your instance is running. The command lists information about your GCE instance, including whether the instance is running, and, if so, the public IP address of the running instance.
+
[options="nowrap"]
----
# gcloud compute instances list
----

. Connect to your instance by using standard SSH. The example uses the [command]`google_compute_engine` key created earlier.
+
[options="nowrap"]
----
# ssh -i ~/.ssh/google_compute_engine <user_name>@<instance_external_ip>
----
+
[NOTE]
====
GCP offers a number of ways to SSH into your instance. See link:https://cloud.google.com/compute/docs/instances/connecting-to-instance[Connecting to instances] for more information. You can also connect to your instance using the root account and password you set previously.
====


[role="_additional-resources"]
.Additional resources
 * link:https://cloud.google.com/sdk/gcloud/reference/compute/instances/list[gcloud compute instances list]
 * link:https://cloud.google.com/compute/docs/instances/connecting-to-instance[Connecting to instances]
