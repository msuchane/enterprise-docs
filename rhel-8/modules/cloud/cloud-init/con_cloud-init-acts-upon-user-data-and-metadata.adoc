:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
// enterprise/assemblies/assembly_introduction-to-cloud-init.adoc
// <List assemblies here, each on a new line>

[id="cloud-init-acts-upon-user-data-and-metadata_{context}"]
= cloud-init acts upon user data, metadata, and vendor data

The datasources that `cloud-init` consumes are user data, metadata, and vendor data.

* User data includes directives you specify in the `cloud.cfg` file and in the `cloud.cfg.d` directory, for example, user data can include files to run, packages to install, and shell scripts. Refer to the `cloud-init` Documentation section link:https://cloudinit.readthedocs.io/en/latest/topics/format.html#user-data-formats[User-Data Formats] for information about the types of user data that `cloud-init` allows.
* Metadata includes data associated with a specific datasource, for example, metadata can include a server name and instance ID. If you are using a specific cloud platform, the platform determines where your instances find user data and metadata. Your platform may require that you add metadata and user data to an HTTP service; in this case, when `cloud-init` runs it consumes metadata and user data from the HTTP service.
* Vendor data is optionally provided by the organization (for example, a cloud provider) and includes information that can customize the image to better fit the environment where the image runs. `cloud-init` acts upon optional vendor data and user data after it reads any metadata and initializes the system. By default, vendor data runs on the first boot. You can disable vendor data execution.
+
Refer to the `cloud-init` Documentation section link:https://cloudinit.readthedocs.io/en/latest/explanation/instancedata.html[Instance Metadata] for a description of metadata; link:https://cloudinit.readthedocs.io/en/latest/topics/datasources.html#datasources[Datasources] for a list of datasources; and link:https://cloudinit.readthedocs.io/en/latest/explanation/vendordata.html[Vendor Data] for more information about vendor data.
