:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// enterprise/titles/configuring-and-maintaining/configuring-and-managing-cloud-init-for-rhel-8/assemblies/assembly_configuring-cloud-init.adoc
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="running-first-boot-commands-with-cloud-init_{context}"]
= Running first boot commands with cloud-init

You can use the `runcmd` and `bootcmd` sections to execute commands during startup and initialization.

The `bootcmd` section executes early in the initialization process and by default runs on every boot. The `runcmd` section executes near the end of the process and is only executed during the first boot and initialization.

.Procedure

include::reuse/include_directives.adoc[tag=where-to-add-directives-user-data]

+
. Add the sections for `bootcmd` and `runcmd`; include commands you want `cloud-init` to execute.
+
[options="nowrap"]
....
#cloud-config
users:
  - default
  - name: user2
    gecos: User N. Ame
    groups: users
chpasswd:
  list: |
    root:password
    fedora:myfedpassword
    user2:mypassword2
  expire: False
bootcmd:
 - echo New MOTD >> /etc/motd
runcmd:
 - echo New MOTD2 >> /etc/motd
....
