:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// enterprise/titles/configuring-and-maintaining/configuring-and-managing-cloud-init-for-rhel-8/assemblies/assembly_configuring-cloud-init.adoc

[id="expiring-a-cloud-user-password-with-cloud-init_{context}"]
= Expiring a cloud user password with cloud-init

To force `cloud-user` to change the `cloud-user` password at the first login, you can set their password as expired.

.Procedure

include::reuse/include_directives.adoc[tag=where-to-add-directives-user-data]

+
. Change the line `chpasswd: {expire: False}` to `chpasswd: {expire: True}`:
+
----
#cloud-config
password: mypassword
chpasswd: {expire: True}
ssh_pwauth: True
ssh_authorized_keys:
  - ssh-rsa AAA...SDvz user1@yourdomain.com
  - ssh-rsa AAB...QTuo user2@yourdomain.com
----
+
This works to expire the password because `password` and `chpasswd` operate on the default user unless you indicate otherwise.
+
[NOTE]
====
This is a global setting. When you set `chpasswd` to `True`, all users you create need to change their passwords when they log in.
====
