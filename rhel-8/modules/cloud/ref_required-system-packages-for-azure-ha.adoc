:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
// assembly_deploying-a-virtual-machine-on-microsoft-azure
//
// <List assemblies here, each on a new line>

[id="required-system-packages-for-azure-ha_{context}"]
= Required system packages for High Availability

The procedure assumes you are creating a VM image for Azure HA that uses {ProductName}. To successfully complete the procedure, the following packages must be installed.

.System packages
[options="header"]
|====
|Package|Repository|Description
|libvirt|rhel-{ProductNumber}-for-x86_64-appstream-rpms|Open source API, daemon, and management tool for managing platform virtualization
|virt-install|rhel-{ProductNumber}-for-x86_64-appstream-rpms|A command-line utility for building VMs
|libguestfs|rhel-{ProductNumber}-for-x86_64-appstream-rpms|A library for accessing and modifying VM file systems
ifeval::[{ProductNumber} == 8]
|libguestfs-tools|rhel-{ProductNumber}-for-x86_64-appstream-rpms|System administration tools for VMs; includes the `guestfish` utility
endif::[]
ifeval::[{ProductNumber} == 9]
|guestfs-tools|rhel-{ProductNumber}-for-x86_64-appstream-rpms|System administration tools for VMs; includes the `virt-customize` utility
endif::[]
|====
