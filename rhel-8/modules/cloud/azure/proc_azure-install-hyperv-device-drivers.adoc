:_mod-docs-content-type: PROCEDURE
[id="installing-hyperv-drivers_{context}"]
= Installing Hyper-V device drivers
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

Microsoft provides network and storage device drivers as part of their Linux Integration Services (LIS) for Hyper-V package. You may need to install Hyper-V device drivers on the VM image prior to provisioning it as an Azure virtual machine (VM). Use the [command]`lsinitrd | grep hv` command to verify that the drivers are installed.

.Procedure

. Enter the following [command]`grep` command to determine if the required Hyper-V device drivers are installed.
+
[subs="+quotes"]
----
# *lsinitrd | grep hv*
----
+
In the example below, all required drivers are installed.
+
[subs="+quotes,attributes"]
----
# *lsinitrd | grep hv*
drwxr-xr-x   2 root     root            0 Aug 12 14:21 usr/lib/modules/3.10.0-932.el{ProductNumber}.x86_64/kernel/drivers/hv
-rw-r--r--   1 root     root        31272 Aug 11 08:45 usr/lib/modules/3.10.0-932.el{ProductNumber}.x86_64/kernel/drivers/hv/hv_vmbus.ko.xz
-rw-r--r--   1 root     root        25132 Aug 11 08:46 usr/lib/modules/3.10.0-932.el{ProductNumber}.x86_64/kernel/drivers/net/hyperv/hv_netvsc.ko.xz
-rw-r--r--   1 root     root         9796 Aug 11 08:45 usr/lib/modules/3.10.0-932.el{ProductNumber}.x86_64/kernel/drivers/scsi/hv_storvsc.ko.xz
----
+
If all the drivers are not installed, complete the remaining steps.
+
[NOTE]
====
An `hv_vmbus` driver may exist in the environment. Even if this driver is present, complete the following steps.
====

. Create a file named `hv.conf` in `/etc/dracut.conf.d`.

. Add the following driver parameters to the `hv.conf` file.
+
----
add_drivers+=" hv_vmbus "
add_drivers+=" hv_netvsc "
add_drivers+=" hv_storvsc "
add_drivers+=" nvme "
----
+
[NOTE]
====
Note the spaces before and after the quotes, for example, `add_drivers+=" hv_vmbus "`. This ensures that unique drivers are loaded in the event that other Hyper-V drivers already exist in the environment.
====

. Regenerate the `initramfs` image.
+
[subs="+quotes,attributes"]
----
# *dracut -f -v --regenerate-all*
----

.Verification

. Reboot the machine.

. Run the [command]`lsinitrd | grep hv` command to verify that the drivers are installed.
