
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="formatting-raid-in-the-web-console_{context}"]
= Formatting RAID in the web console

You can format and mount software RAID devices in the {ProductShortName}{nbsp}{ProductNumber} web console.

.Prerequisites

* Physical disks are connected and visible by {ProductShortName}{nbsp}{ProductNumber}.
* RAID is created.
* Consider the file system to be used for the RAID.
* Consider creating a partitioning table.

.Procedure

. Open the {ProductShortName}{nbsp}{ProductNumber} web console.

. Click *Storage*.

. In the *Storage* table, click the menu button, btn:[⋮], next to the RAID device you want to format.

. From the drop-down menu, select btn:[Format].

. In the *Format* dialog box, enter a name.

. In the *Mount Point* field, add the mount path.

. From the *Type* drop-down list, select the type of file system.

. Select the *Overwrite existing data with zeros* checkbox if you want the {ProductShortName} web console to rewrite the whole disk with zeros. This option is slower because the program has to go through the whole disk, but it is more secure.
Use this option if the disk includes any data and you need to overwrite it.
+
If you do not select the *Overwrite existing data with zeros* checkbox, the {ProductShortName} web console rewrites only the disk header. This increases the speed of formatting.

. If you want to encrypt the volume, select the type of encryption from the *Encryption* drop-down menu.
+
If you do not want to encrypt the volume, select *No encryption*.

. In the *At boot* drop-down menu, select when you want to mount the volume.

. In the *Mount options* section:
.. Select the *Mount read only* checkbox if you want the to mount the volume as a read-only logical volume.
.. Select the *Custom mount options* checkbox and add the mount options if you want to change the default mount option. For more information, see
ifdef::cockpit-title[]
xref:customizing-nfs-mount-options-in-the-web-console_managing-nfs-mounts-in-the-web-console[Customizing NFS mount options in the web console].
endif::[]
ifndef::cockpit-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/managing-nfs-mounts-in-the-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#customizing-nfs-mount-options-in-the-web-console_managing-nfs-mounts-in-the-web-console[Customizing NFS mount options in the web console].
endif::[]

. Format the RAID partition:
** If you want to format and mount the partition, click the btn:[Format and mount] button.
** If you want to only format the partition, click the btn:[Format only] button.
+
Formatting can take several minutes depending on the volume size and which formatting options are selected.


.Verification
* After the formatting has completed successfully, you can see the details of the formatted logical volume in the *Storage* table on the *Storage* page.
