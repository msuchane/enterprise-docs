////
Base the file name and the ID on the module title. For example:
* file name: ref-my-reference-a.adoc
* ID: [id="ref-my-reference-a_{context}"]
* Title: = My reference A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix ref- or ref_ to the file name.
Add the following attribute before the module ID:
////
:_content-type: REFERENCE

[id="ref_satellite-host-management-and-monitoring_{context}"]
= Satellite host management and monitoring
////
In the title of a reference module, include nouns that are used in the body text. For example, "Keyboard shortcuts for ___" or "Command options for ___." This helps readers and search engines find the information quickly.
////

[role="_abstract"]
Red Hat Satellite is a system management solution that enables you to deploy, configure, and maintain your systems across physical, virtual, and cloud environments. Satellite provides provisioning, remote management and monitoring of multiple Red Hat Enterprise Linux deployments with a single, centralized tool.

By default, Red Hat web console integration is disabled in Red Hat Satellite. If you want to access Red Hat web console features for your hosts from within Red Hat Satellite, you must first enable Red Hat web console integration on a Red Hat Satellite Server.

.Satellite documentation for managing many host at scale in the web console

* For more details on integrating web console and Satellite, see link:https://access.redhat.com/documentation/en-us/red_hat_satellite/6.7/html/managing_hosts/host_management_and_monitoring_using_red_hat_web_console#integrating_satellite_with_red_hat_web_console[Integrating Satellite with Red Hat web console].

* For more information about managing and monitoring hosts using the web console, see link:https://access.redhat.com/documentation/en-us/red_hat_satellite/6.7/html/managing_hosts/host_management_and_monitoring_using_red_hat_web_console#integrating_satellite_with_red_hat_web_console[Managing and Monitoring Hosts Using Red Hat web console].



