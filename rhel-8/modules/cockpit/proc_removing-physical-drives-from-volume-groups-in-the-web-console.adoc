// Module included in the following assemblies:
//
// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="removing-physical-drives-from-volume-groups-in-the-web-console_{context}"]
= Removing physical drives from volume groups in the web console

[role="_abstract"]
If a logical volume includes multiple physical drives, you can remove one of the physical drives online.

The system moves automatically all data from the drive to be removed to other drives during the removal process. Notice that it can take some time.

The web console also verifies, if there is enough space for removing the physical drive.

.Prerequisites

* A volume group with more than one physical drive connected.

.Procedure

. Log in to the {ProductShortName}{nbsp}{ProductNumber} web console.

. Click *Storage*.

. In the *Storage* table, click the volume group to which you want to add physical drives.

. On the *LVM2 volume group* page, scroll to the *Physical volumes* section.

. Click the menu button, btn:[⋮], next to the physical volume you want to remove.

. From the drop-down menu, select btn:[Remove].
+
The {ProductShortName}{nbsp}{ProductNumber} web console verifies whether the logical volume has enough free space to removing the disk.
If there is no free space to transfer the data, you cannot remove the disk and you must first add another disk to increase the capacity of the volume group.
For details, see
ifdef::cockpit-title[]
xref:adding-physical-drives-to-volume-groups-in-the-web-console_changing-physical-drives-in-volume-groups-using-the-web-console[Adding physical drives to logical volumes in the web console].
endif::[]
ifndef::cockpit-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/using-the-web-console-for-changing-physical-drives-in-volume-groups_system-management-using-the-rhel-8-web-console#adding-physical-drives-to-volume-groups-in-the-web-console_changing-physical-drives-in-volume-groups-using-the-web-console[Adding physical drives to logical volumes in the web console].
endif::[]
