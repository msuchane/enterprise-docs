// Module included in the following assemblies:
//
// include::assemblies/assembly_managing-file-systems-using-the-web-console.adoc[leveloffset=+1]

ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="displaying-partitions-in-the-web-console_{context}"]
= Displaying partitions formatted with file systems in the web console

[role="_abstract"]
The *Storage* section in the web console displays all available file systems in the *Filesystems* table.

Besides the list of partitions formatted with file systems, you can also use the page for creating new storage.

.Prerequisites

* The `cockpit-storaged` package is installed on your system.

* The web console must be installed and accessible. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].

.Procedure

. Log in to the {ProductShortName}{nbsp}{ProductNumber} web console. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].

. Click the *Storage* tab.
+
In the *Storage* table, you can see all available partitions formatted with file systems, their ID, types, locations, sizes, and how much space is available on each partition.
+
image:cockpit-filesystems-partitions.png[Image displaying the Storage table available in the cockpit Storage tab.]
+
You can also use the drop-down menu in the top-right corner to create new local or networked storage.
+
image:cockpit-adding-volume-groups.png[Image displaying the drop-down menu available in the Storage table., widht=100%]
