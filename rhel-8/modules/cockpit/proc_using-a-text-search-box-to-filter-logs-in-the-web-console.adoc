:_mod-docs-content-type: PROCEDURE

[id="using-a-text-search-box-to-filter-logs-in-the-web-console_{context}"]
= Using a text search box to filter logs in the web console

[role="_abstract"]
Using the text search box allows you to filter logs according to different parameters. The search combines usage of the filtering drop-down menus, quantifiers, log fields and free-form string search.

.Prerequisites

* The web console interface must be installed and accessible.
+
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].

.Procedure

. Log in to the {ProductShortName} web console.
+
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].
+
. Click *Logs*.
. Use the drop-down menus to specify the three main quantifiers - time range, priority, and identifier(s) - you want to filter.
+
The *Priority* quantifier always has to have a value. If you do not specify it, it automatically filters the *Error and above* priority. Notice that the options you set reflect in the text search box.
+
. Specify the log field you want to filter.
+
It is possible to add several log fields.
+
. You can use a free-form string to search for anything else. The search box also accepts regular expressions.
