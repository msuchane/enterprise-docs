// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/con_what-are-zones-and-why-to-use-them.adoc[leveloffset=+1]

:_mod-docs-content-type: CONCEPT

[id="zones-in-the-web-console_{context}"]
= Zones in the web console

[role="_abstract"]
The Red Hat Enterprise Linux web console implements major features of the firewalld service and enables you to:

* Add predefined firewall zones to a particular interface or range of IP addresses
* Configure zones with selecting services into the list of enabled services
* Disable a service by removing this service from the list of enabled service
* Remove a zone from an interface
