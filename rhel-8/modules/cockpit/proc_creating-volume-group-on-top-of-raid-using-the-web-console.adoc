// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="creating-volume-group-on-top-of-raid-using-the-web-console_{context}"]
= Creating a volume group on top of RAID using the web console

[role="_abstract"]
Build a volume group from software RAID.

.Prerequisites

* RAID device that is not formatted and not mounted.

.Procedure

. Open the {ProductShortName}{nbsp}{ProductNumber} web console.

. Click *Storage*.

. In the *Storage* table, click the menu button.

. From the drop-down menu, select *Create LVM2 volume group*.
+
image:cockpit-adding-volume-groups-create-lvm2.png[Image displaying the available options in the Storage table drop-down menu. Selecting Create LVM2 volume group.]

. In the *Create LVM2 volume group* dialog box, enter a name for the new volume group.

. From the *Disks* list, select a RAID device.
+
If you do not see the RAID in the list, unmount the RAID from the system. The RAID device must not be in use by the {ProductShortName}{nbsp}{ProductNumber} system.

. Click btn:[Create].
