// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="adding-interfaces-to-the-bond-using-the-web-console_{context}"]
= Adding interfaces to the bond using the web console

[role="_abstract"]
Network bonds can include multiple interfaces and you can add or remove any of them at any time.

Learn how to add a network interface to an existing bond.

.Prerequisites

* Having a bond with multiple interfaces configured as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/configuring-network-bonds-using-the-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#proc_configuring-a-network-bond-by-using-the-rhel-web-console_configuring-network-bonds-using-the-web-console[Configuring a network bond using the web console]


.Procedure

. Log in to the web console.
+
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].

. Open *Networking*.

. In the *Interfaces* table, click on the bond you want to configure.

. In the bond settings screen, scroll down to the table of members (interfaces).

. Click the btn:[Add member] drop-down icon.

. Select the interface From the drop-down menu and click it.


.Verification steps

* Check that the selected interface appeared in the *Interface members* table in the bond settings screen.
