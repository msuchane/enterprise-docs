// Module included in the following assemblies:
// assembly_configuring-system-settings-in-the-web-console.adoc

// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="rebooting-the-system-using-the-web-console_{context}"]
[[using-the-web-console-to-restart-the-system_performing-basic-system-administration-tasks-in-the-web-console]]
= Rebooting the system using the web console

You can use the web console to restart a {ProductShortName} system that the web console is attached to.

.Prerequisites

* The web console is installed and accessible. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].


.Procedure

. Log into the RHEL web console. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].

. In the *Overview* page, click the btn:[Reboot] button.
+
image:cockpit-system-restart-pf4.png[]
. If any users are logged in to the system, you can write a message about the restart in the *Reboot* dialog box.

. Optional: In the *Delay* drop-down list, select a time interval for the reboot delay.
+
image:cockpit-restart-delay-pf4.png[]
. Click *Reboot*.
