:_mod-docs-content-type: PROCEDURE

[id="proc_enabling-password-less-sudo-for-smart-card-users_{context}"]
= Enabling passwordless sudo authentication for smart card users


[role="_abstract"]
You can use the web console to configure passwordless authentication to `sudo` and other services for smart card users.

As an alternative, if you use Red Hat Identity Management, you can declare the initial web console certificate authentication as trusted for authenticating to `sudo`, SSH, or other services. For that purpose, the web console automatically creates an S4U2Proxy Kerberos ticket in the user session.

.Prerequisites

* Identity Management installed.

* Active Directory connected in the cross-forest trust with Identity Management.

* Smart card set up to log in to the web console. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/configuring-smart-card-authentication-with-the-web-console_system-management-using-the-rhel-{ProductNumber}-web-console[Configuring smart card authentication with the web console for centrally managed users] for more information.


.Procedure

. Set up constraint delegation rules to list which hosts the ticket can access.
+
[[ex-setting-up-constraint-delegation-rules]]
.Setting up constraint delegation rules
====
The web console session runs host `host.example.com` and should be trusted to access its own host with `sudo`. Additionally, we are adding second trusted host - `remote.example.com`.

* Create the following delegation:
** Run the following commands to add a list of target machines a particular rule can access:
+
[subs="quotes"]
----
# *ipa servicedelegationtarget-add cockpit-target*
# *ipa servicedelegationtarget-add-member cockpit-target \
   --principals=host/host.example.com@EXAMPLE.COM \
   --principals=host/remote.example.com@EXAMPLE.COM*
----

** To allow the web console sessions (HTTP/principal) to access that host list, use the following commands:
+
[subs="quotes"]
----
# *ipa servicedelegationrule-add cockpit-delegation*
# *ipa servicedelegationrule-add-member cockpit-delegation \
  --principals=HTTP/host.example.com@EXAMPLE.COM*
# *ipa servicedelegationrule-add-target cockpit-delegation \
  --servicedelegationtargets=cockpit-target*
----
====

. Enable GSS authentication in the corresponding services:

.. For sudo, enable the `pam_sss_gss` module in the `/etc/sssd/sssd.conf` file:

... As root, add an entry for your domain to the `/etc/sssd/sssd.conf` configuration file.
+
[subs="quotes,attributes"]
----
[domain/example.com]
pam_gssapi_services = sudo, sudo-i
----

... Enable the module in the `/etc/pam.d/sudo` file on the first line.
+
[subs="quotes,attributes"]
----
auth sufficient pam_sss_gss.so
----

.. For SSH, update the `GSSAPIAuthentication` option in the `/etc/ssh/sshd_config` file to `yes`.

WARNING: The delegated S4U ticket is not forwarded to remote SSH hosts when connecting to them from the web console. Authenticating to sudo on a remote host with your ticket will not work.


.Verification

. Log in to the web console using a smart card.

. Click the `Limited access` button.

. Authenticate using your smart card.

Alternatively:

* Try to connect to a different host with SSH.

