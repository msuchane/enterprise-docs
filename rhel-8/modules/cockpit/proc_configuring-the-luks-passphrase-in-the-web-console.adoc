// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="configuring-the-luks-passphrase-in-the-web-console_{context}"]
= Configuring the LUKS passphrase in the web console

[role="_abstract"]
If you want to add encryption to an existing logical volume on your system, you can only do so through formatting the volume.

.Prerequisites


* The web console must be installed and accessible. For details, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].

* The `cockpit-storaged` package is installed on your system.

* Available existing logical volume without encryption.

.Procedure

. Log in to the {ProductShortName}{nbsp}{ProductNumber} web console.
+
For details, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].

. Click *Storage*.

. In the *Storage* table, click the menu button, btn:[⋮], next to the storage device you want to encrypt.

. From the drop-down menu, select btn:[Format].

. In the *Encryption field*, select the encryption specification, *LUKS1* or *LUKS2*.

. Set and confirm your new passphrase.

. [Optional] Modify further encryption options.

. Finalize formatting settings.

. Click *Format*.
