// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="removing-interfaces-from-the-bridge-using-the-web-console_{context}"]
= Removing interfaces from the bridge using the web console

[role="_abstract"]
Network bridges can include multiple interfaces. You can remove them from the bridge. Each removed interface will be automatically changed to the standalone interface.

Learn how to remove a network interface from a software bridge created in the {ProductShortName}{nbsp}{ProductNumber} system.


.Prerequisites

* Having a bridge with multiple interfaces in your system.

.Procedure

. Log in to the {ProductShortName} web console. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].

. Open *Networking*.

. Click the bridge you want to configure.

. In the bridge settings screen, scroll down to the table of ports (interfaces).

. Select an interface and click the btn:[-] button.

.Verification steps

* Go to *Networking* to check that you can see the interface as a standalone interface in the *Interface members* table.
