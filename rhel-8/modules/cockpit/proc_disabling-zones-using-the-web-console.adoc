
// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="disabling-zones-using-the-web-console_{context}"]
= Disabling zones using the web console

[role="_abstract"]
You can disable a firewall zone in your firewall configuration using the web console.

.Prerequisites

*  The {ProductShortName}{nbsp}{ProductNumber} web console has been installed. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].

.Procedure

. Log in to the {ProductShortName} web console with administrator privileges. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].

. Click *Networking*.

. Click on the btn:[Edit rules and zones] button.
+
image:cockpit_edit-rules-and-zones.png[]
+
If you do not see the btn:[Edit rules and zones] button, log in to the web console with the administrator privileges.

. Click on the *Options* icon at the zone you want to remove.
+
image:cockpit_delete-zone.png[]

. Click *Delete*.

The zone is now disabled and the interface does not include opened services and ports which were configured in the zone.
