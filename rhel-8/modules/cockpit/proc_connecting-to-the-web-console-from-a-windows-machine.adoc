:_mod-docs-content-type: PROCEDURE

[id="connecting-to-the-web-console-from-a-remote-machine_{context}"]
= Connecting to the web console from a remote machine

You can connect to your web console interface from any client operating system and also from mobile phones or tablets.

.Prerequisites

* A device with a supported internet browser, such as:

** Mozilla Firefox 52 and later

** Google Chrome 57 and later

** Microsoft Edge 16 and later

ifeval::[{ProductNumber} == 8]
* RHEL 8 server you want to access with an installed and accessible web console. For more information about the installation of the web console see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_systems_using_the_rhel_8_web_console/getting-started-with-the-rhel-8-web-console_system-management-using-the-rhel-8-web-console#installing-the-web-console_getting-started-with-the-rhel-8-web-console[Installing the web console].
endif::[]

ifeval::[{ProductNumber} == 9]
* RHEL 9 server you want to access with an installed and accessible web console.
endif::[]

.Procedure

. Open your web browser.
. Type the remote server's address in one of the following formats:
.. With the server's host name:
+
[subs="quotes,macros"]
----
https://_&lt;server.hostname.example.com&gt;_pass:[:]_&lt;port-number&gt;_
----
+
For example:
+
----
https://example.com:9090
----

.. With the server's IP address:
+
[subs="quotes,macros"]
----
https://_&lt;server.IP_address&gt;_pass:[:]_&lt;port-number&gt;_
----
+
For example:
+
----
https://192.0.2.2:9090
----
. After the login interface opens, log in with your {ProductShortName} system credentials.
