// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="formatting-vdo-volumes-in-the-web-console_{context}"]
= Formatting VDO volumes in the web console

[role="_abstract"]
VDO volumes act as physical drives. To use them, you must format them with a file system.

WARNING: Formatting erases all data on the volume.

.Prerequisites

* A VDO volume is created.
For details, see
ifdef::cockpit-title[]
xref:creating-virtual-data-optimizer-in-the-web-console_managing-virtual-data-optimizer-volumes-using-the-web-console[Creating VDO volumes in the web console].
endif::[]
ifndef::cockpit-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/using-the-web-console-for-managing-virtual-data-optimizer-volumes_system-management-using-the-rhel-{ProductNumber}-web-console#creating-virtual-data-optimizer-in-the-web-console_managing-virtual-data-optimizer-volumes-using-the-web-console[Creating VDO volumes in the web console].
endif::[]

.Procedure

. Log in to the {ProductShortName}{nbsp}{ProductNumber} web console. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].

. Click *Storage*.

. Click the LVM2 volume group containing the VDO volume you want to format.

. Click the menu button, btn:[⋮], at the end of the line with the VDO volume you want to format.

. Click *Format*.
+
image:cockpit-vdo-03.png[Format option in the VDO logical volume menu]

. In the *Name* field, enter the logical volume name.

. In the *Mount Point* field, add the mount path.

. By default, the web console rewrites only the disk header after you finish this dialog. The advantage of this option is the speed of formatting. If you check the *Overwrite existing data with zeros* option, the web console rewrites the whole disk with zeros. This option is slower because the program has to go through the whole disk. Use this option if the disk includes any sensitive data and you want to rewrite them.

. In the *Type* drop-down menu, select a file system:
+
--
* The default option, the *XFS* file system, supports large logical volumes, switching physical drives online without outage, and growing.
+
XFS does not support shrinking volumes. Therefore, you cannot reduce the size of a volume formatted with XFS.

* The *ext4* file system supports logical volumes, switching physical drives online without outage, growing, and shrinking.

--
+
You can also select a version with the LUKS (Linux Unified Key Setup) encryption, which allows you to encrypt the volume with a passphrase.


. In the *At boot* drop-down menu, select when you want to mount the volume.


. Click *Format and mount* or *Format only*.
+
Formatting can take several minutes depending on the used formatting options and the volume size.
+
image:cockpit-vdo-04.png[Format option in the VDO logical volume menu]

.Verification

* After a successful finish, you can see the details of the formatted VDO volume on the *Storage* tab and in the LVM2 volume group tab.
