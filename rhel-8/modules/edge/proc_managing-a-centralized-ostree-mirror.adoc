:_mod-docs-content-type: PROCEDURE

[id="managing-a-centralized-ostree-mirror_{context}"]
= Managing a centralized OSTree mirror

For production environments, having a central OSTree mirror that serves all the commits has several advantages, including:

* Deduplicating and minimizing disk storage
* Optimizing the updates to clients by using static delta updates
* Pointing to a single OSTree mirror for their deployment life.

To manage a centralized OSTree mirror, you must pull each commit from RHEL image builder into the centralized repository where it will be available to your users. 

NOTE: You can also automate managing an OSTree mirror by using the `osbuild.infra` Ansible collection. See link:https://www.ansible.com/blog/ansible-validated-content-introduction-to-infra.osbuild-collection[osbuild.infra Ansible].

To create a centralized repository you can run the following commands directly on a web server:

.Procedure

. Create an empty blueprint, customizing it to use "rhel-93" as the distro:
+
[subs="quotes,attributes"]
----
name = "minimal-rhel93"
description = "minimal blueprint for ostree commit"
version = "1.0.0"
modules = []
groups = []
distro = "rhel-93"
----

. Push the blueprint to the server: 
+
[subs="quotes,attributes"]
----
# *composer-cli blueprints push minimal-rhel93.toml*
----

. Build a RHEL for Edge Commit (`.tar`) image from the blueprint you created: 
+
[subs="quotes,attributes"]
----
# *composer-cli compose start-ostree minimal-rhel93 edge-commit*
----

. Retrieve the `.tar ﬁle` and decompress it to the disk: 
+
[subs="quotes,attributes"]
----
# *composer-cli compose image _&lt;rhel-93-uuid&gt;*
$ *tar -xf &lt;rhel-93-uuid&gt;.tar -C /usr/share/nginx/html/*
----
+
The `/usr/share/nginx/html/repo` location on disk will become the single OSTree repo for all refs and commits.

. Create another empty blueprint, customizing it to use "rhel-89" as the distro: 
+
[subs="quotes,attributes"]
----
name = "minimal-rhel89"
description = "minimal blueprint for ostree commit"
version = "1.0.0"
modules = []
groups = []
distro = "rhel-89"
----

. Push the blueprint and create another RHEL for Edge Commit (`.tar`) image:
[subs="quotes,attributes"]
----
# *composer-cli blueprints push minimal-rhel89.toml*
# *composer-cli compose start-ostree minimal-rhel89 edge-commit*
----

. Retrieve the `.tar ﬁle` and decompress it to the disk:  
+
[subs="quotes,attributes"]
----
# *composer-cli compose image &lt;rhel-89-uuid&gt;*
$ *tar -xf &lt;rhel-89-uuid&gt;.tar*
----

. Pull the commit to the local repo. By using `ostree pull-local`, you can copy the commit data from one local repo to another local repo.
+
[subs="quotes,attributes"]
----
# *ostree --repo=/usr/share/nginx/html/repo pull-local repo*
----

. Optional: Inspect the status of the OSTree repo. The following is an output example:
+
[subs="quotes,attributes"]
----
$ *ostree --repo=/usr/share/nginx/html/repo refs* 

rhel/8/x86_64/edge
rhel/9/x86_64/edge

$ ostree --repo=/usr/share/nginx/html/repo show rhel/8/x86_64/edge
commit f7d4d95465fbd875f6358141f39d0c573df6a321627bafde68c73850667e5443
ContentChecksum:  41bf2f8b442a770e9bf03e096a46a286f5836e0a0702b7c3516ef4e0acec2dea
Date:  2023-09-15 16:17:04 +0000
Version: 8.9
(no subject)

$ ostree --repo=/usr/share/nginx/html/repo show rhel/9/x86_64/edge
commit 89290dbfd6f749700c77cbc434c121432defb0c1c367532368eee170d9e53ea9
ContentChecksum:  70235bfb9cae82c53f856183750e809becf0b9b076122b19c40fec92fc6d74c1
Date:  2023-09-15 15:30:24 +0000
Version: 9.3
(no subject)
----

. Update the RHEL 9.3 blueprint to include a new package and build a new commit, for example: 
+
[subs="quotes,attributes"]
----
name = "minimal-rhel93"
description = "minimal blueprint for ostree commit"
version = "1.1.0"
modules = []
groups = []
distro = "rhel-93"

[[packages]]
name = "strace"
version = "*"
----

. Push the updated blueprint and create a new RHEL for Edge Commit (`.tar`) image, pointing the compose to the existing OSTree repo:
+
[subs="quotes,attributes"]
----
# *composer-cli blueprints push minimal-rhel93.toml*
# *composer-cli compose start-ostree minimal-rhel93 edge-commit --url http://localhost/repo --ref rhel/9/x86_64/edge*
----

. Retrieve the `.tar` ﬁle and decompress it to the disk: 
+
[subs="quotes,attributes"]
----
# *rm -rf repo*
# *composer-cli compose image &lt;rhel-93-uuid&gt;*
# *tar -xf &lt;rhel-93-uuid&gt;.tar*
----

. Pull the commit to repo:
+
[subs="quotes,attributes"]
----
# *ostree --repo=/usr/share/nginx/html/repo pull-local repo*
----

. Optional: Inspect the OSTree repo status again: 
+
[subs="quotes,attributes"]
----
$ *ostree --repo=/usr/share/nginx/html/repo refs*
rhel/8/x86_64/edge
rhel/9/x86_64/edge

$ ostree --repo=/usr/share/nginx/html/repo show rhel/8/x86_64/edge
commit f7d4d95465fbd875f6358141f39d0c573df6a321627bafde68c73850667e5443
ContentChecksum:  41bf2f8b442a770e9bf03e096a46a286f5836e0a0702b7c3516ef4e0acec2dea
Date:  2023-09-15 16:17:04 +0000
Version: 8.9
(no subject)

$ ostree --repo=/usr/share/nginx/html/repo show rhel/9/x86_64/edge
commit a35c3b1a9e731622f32396bb1aa84c73b16bd9b9b423e09d72efaca11b0411c9
Parent:  89290dbfd6f749700c77cbc434c121432defb0c1c367532368eee170d9e53ea9
ContentChecksum:  2335930df6551bf7808e49f8b35c45e3aa2a11a6c84d988623fd3f36df42a1f1
Date:  2023-09-15 18:21:31 +0000
Version: 9.3
(no subject)

$ ostree --repo=/usr/share/nginx/html/repo log rhel/9/x86_64/edge
commit a35c3b1a9e731622f32396bb1aa84c73b16bd9b9b423e09d72efaca11b0411c9
Parent:  89290dbfd6f749700c77cbc434c121432defb0c1c367532368eee170d9e53ea9
ContentChecksum:  2335930df6551bf7808e49f8b35c45e3aa2a11a6c84d988623fd3f36df42a1f1
Date:  2023-09-15 18:21:31 +0000
Version: 9.3
(no subject)

commit 89290dbfd6f749700c77cbc434c121432defb0c1c367532368eee170d9e53ea9
ContentChecksum:  70235bfb9cae82c53f856183750e809becf0b9b076122b19c40fec92fc6d74c1
Date:  2023-09-15 15:30:24 +0000
Version: 9.3
(no subject)

$ rpm-ostree db diff --repo=/usr/share/nginx/html/repo 89290dbfd6f749700c77cbc434c121432defb0c1c367532368eee170d9e53ea9 a35c3b1a9e731622f32396bb1aa84c73b16bd9b9b423e09d72efaca11b0411c9
ostree diff commit from: 89290dbfd6f749700c77cbc434c121432defb0c1c367532368eee170d9e53ea9
ostree diff commit to:   a35c3b1a9e731622f32396bb1aa84c73b16bd9b9b423e09d72efaca11b0411c9
Added:
  elfutils-default-yama-scope-0.188-3.el9.noarch
  elfutils-libs-0.188-3.el9.x86_64
  strace-5.18-2.el9.x86_64
----

