:_mod-docs-content-type: PROCEDURE

[id="proc_installing-the-manufacturing-server-package_{context}"]
= Installing and running the manufacturing server

[role="_abstract"]
The `fdo-manufacturing-server` RPM package enables you to run the Manufacturing Server component of the FDO protocol. It also stores other components, such as the owner vouchers, the manufacturer keys, and information about the manufacturing sessions. During the device installation, the Manufacturing server generates the device credentials for the specific device, including its GUID, rendezvous information and other metadata. Later on in the process, the device uses this rendezvous information to contact the Rendezvous server.

IMPORTANT:  Red Hat provides the `fdo-manufacturing-server` tool as a Technology Preview feature and should run on secure networks because Technology Preview features are not supported with Red Hat production service level agreements (SLAs) and might not be functionally complete. These features provide early access to upcoming product features, enabling customers to test functionality and provide feedback during the development process. See link:https://access.redhat.com/support/offerings/techpreview[Technology Preview Features Support Scope] on the Red Hat Customer Portal for information about the support scope for Technology Preview features.

To install the `manufacturing server` RPM package, complete the following steps:

.Procedure

. Install the `fdo-admin-cli` package:
+
[subs="quotes,attributes"]
----
# {PackageManagerCommand} install -y fdo-admin-cli
----

. Check if the `fdo-manufacturing-server` RPM package is installed:
+
[subs="quotes,attributes"]
----
$ rpm -qa | grep fdo-manufacturing-server --refresh
----

. Check if the files were correctly installed: 
+
[subs="quotes,attributes"]
----
$ ls /usr/share/doc/fdo
----
+
You can see the following output:
+
[subs="quotes,attributes"]
----	
Output: 
manufacturing-server.yml 
owner-onboarding-server.yml
rendezvous-info.yml
rendezvous-server.yml
serviceinfo-api-server.yml
----

. Optional: Check the content of each file, for example:
+
[subs="quotes,attributes"]
----
$ cat /usr/share/doc/fdo/manufacturing-server.yml
----
  
. Configure the Manufacturing server. You must provide the following information:

* The Manufacturing server URL
* The IP address or DNS name for the Rendezvous server
* The path to the keys and certificates you generated. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/composing_installing_and_managing_rhel_for_edge_images/assembly_automatically-provisioning-and-onboarding-rhel-for-edge-devices_composing-installing-managing-rhel-for-edge-images#proc_generating-key-and-certificates_assembly_automatically-provisioning-and-onboarding-rhel-for-edge-devices[Generating key and certificates].
+
You can find an example of a Manufacturing server configuration file in the `/usr/share/doc/fdo/manufacturing-server.yml` directory. The following is a `manufacturing server.yml` example that is created and saved in the `/etc/fdo` directory. It contains paths to the directories, certificates, keys that you created, the Rendezvous server IP address and the default port. 
+
[subs="quotes,attributes"]
----
session_store_driver:
  Directory:
    path: /etc/fdo/stores/manufacturing_sessions/
ownership_voucher_store_driver:
  Directory:
    path: /etc/fdo/stores/owner_vouchers
public_key_store_driver:
  Directory:
    path: /etc/fdo/stores/manufacturer_keys
bind: "0.0.0.0:8080"
protocols:
  plain_di: false
  diun:
    mfg_string_type: SerialNumber
    key_type: SECP384R1
    allowed_key_storage_types:
      - Tpm
      - FileSystem
    key_path: /etc/fdo/keys/diun_key.der
    cert_path: /etc/fdo/keys/diun_cert.pem
rendezvous_info:
  - deviceport: 8082
    ip_address: 192.168.122.99
    ownerport: 8082
    protocol: http
manufacturing:
  manufacturer_cert_path: /etc/fdo/keys/manufacturer_cert.pem
  device_cert_ca_private_key: /etc/fdo/keys/device_ca_key.der
  device_cert_ca_chain: /etc/fdo/keys/device_ca_cert.pem
  owner_cert_path: /etc/fdo/keys/owner_cert.pem
  manufacturer_private_key: /etc/fdo/keys/manufacturer_key.der
---- 

. Start the Manufacturing server.
.. Check if the systemd unit file are in the server:
+
[subs="quotes,attributes"]
----
# systemctl list-unit-files | grep fdo | grep manufacturing
fdo-manufacturing-server.service disabled disabled
----

.. Enable and start the manufacturing server.
+
[subs="quotes,attributes"]
----
# systemctl enable --now fdo-manufacturing-server.service
----

.. Open the default ports in your firewall: 
+
[subs="quotes,attributes"]
----
# firewall-cmd --add-port=8080/tcp --permanent
# systemctl restart firewalld
----

.. Ensure that the service is listening on the port 8080:
+
[subs="quotes,attributes"]
----
# ss -ltn
----

. Install RHEL for Edge onto your system using the simplified installer. See xref:building-and-provisioning-simplified-installer-images_composing-installing-managing-rhel-for-edge-images[Building simplified installer images to provision a RHEL for Edge image].

[role="_additional-resources"]
.Additional resources
* The link:https://github.com/fedora-iot/fido-device-onboard-rs/blob/main/examples/config/manufacturing-server.yml[manufacturing-server.yml example]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/composing_installing_and_managing_rhel_for_edge_images/index#ref_fdo-terminology_edge-terminology-and-commands[FDO automatic onboarding terminology]
