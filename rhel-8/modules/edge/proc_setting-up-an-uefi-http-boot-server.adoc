:_mod-docs-content-type: PROCEDURE

[id="setting-up-an-uefi-http-boot-server_{context}"]
= Setting up an UEFI HTTP Boot server

[role="_abstract"]
To set up an *UEFI HTTP Boot* server, so that you can start to provision a RHEL for Edge Virtual Machine over network by connecting to this UEFI HTTP Boot server, follow the steps:

.Prerequisites

* You have created the ISO simplified installer image.
* An http server that serves the ISO content.

.Procedure

//. Set up a network configuration to support *UEFI HTTP* boot. See link:https://www.redhat.com/sysadmin/uefi-http-boot-libvirt[Setting up UEFI HTTP boot with libvirt].

. Mount the ISO image to the directory of your choice:
+
[subs="quotes,attributes"]
----
# mkdir /mnt/rhel{ProductNumber}-install/
# mount -o loop,ro -t iso9660 /path_directory/installer.iso /mnt/rhel{ProductNumber}-install/
----
+
Replace `/path_directory/installer.iso` with the path to the RHEL for Edge bootable ISO image.

. Copy the files from the mounted image to the HTTP server root. This command creates the `/var/www/html/rhel{ProductNumber}-install/` directory with the contents of the image.
+
[subs="quotes,attributes"]
----
# mkdir /var/www/html/httpboot/
# cp -R /mnt/rhel{ProductNumber}-install/* /var/www/html/httpboot/
# chmod -R +r /var/www/html/httpboot/*
----
+
NOTE: Some copying methods can skip the `.treeinfo` file which is required for a valid installation source. Running the `cp` command for whole directories as shown in this procedure will copy `.treeinfo` correctly.

. Update the `/var/www/html/EFI/BOOT/grub.cfg` file, by replacing:

.. `coreos.inst.install_dev=/dev/sda` with `coreos.inst.install_dev=/dev/vda`
.. `linux /images/pxeboot/vmlinuz` with `linuxefi /images/pxeboot/vmlinuz`
.. `initrd /images/pxeboot/initrd.img` with `initrdefi /images/pxeboot/initrd.img`
.. `coreos.inst.image_file=/run/media/iso/disk.img.xz` with `coreos.inst.image_url=http://{IP-ADDRESS}/disk.img.xz`
+
The _IP-ADDRESS_ is the ip address of this machine, which will serve as a http boot server.


. Start the httpd service:
+
[subs="quotes,attributes"]
----
# systemctl start httpd.service
----
+
As a result, after you set up an *UEFI HTTP Boot* server, you can install your RHEL for Edge devices by using *UEFI HTTP* boot.


