:_mod-docs-content-type: PROCEDURE
[id="creating-a-rhel-for-edge-image-with-a-parent-ref-using-image-builder-command-line-interface_{context}"]
= Creating a RHEL for Edge image update with a ref commit by using RHEL image builder CLI

[role="_abstract"]
If you performed a change in an existing blueprint, for example, you added a new package, and you want to update an existing RHEL for Edge image with this new package, you can use the `--parent` argument to generate an updated `RHEL for Edge Commit (.tar)` image. The `--parent` argument can be either a `ref` that exists in the repository specified by the `URL` argument, or you can use the `Commit ID`, which you can find in the extracted `.tar` image file. Both `ref` and `Commit ID` arguments retrieve a parent for the new commit that you are building. RHEL image builder can read information from the parent commit that will affect parts of the new commit that you are building. As a result, RHEL image builder reads the parent commit's user database and preserves UIDs and GIDs for the package-created system users and groups.

.Prerequisites

* You have updated an existing blueprint for RHEL for Edge image.
* You have an existing RHEL for Edge image (OSTree commit). See 
ifdef::composing-installing-and-managing-rhel-for-edge-images[]
xref:composing-installing-and-managing-rhel-for-edge-images_extracting-the-tar-file-commit_installing-rpm-ostree-images[Extracting RHEL for Edge image commit].
endif::[]
ifndef::composing-installing-and-managing-rhel-for-edge-images[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/composing_installing_and_managing_rhel_for_edge_images/installing-rpm-ostree-images_composing-installing-managing-rhel-for-edge-images#extracting-the-tar-file-commit_installing-rpm-ostree-images[Extracting RHEL for Edge image commit].
endif::[]
* The `ref` being built is available at the `OSTree` repository specified by the URL.

.Procedure

. Create the RHEL for Edge commit image:
+
[subs="quotes,attributes"]
----
# composer-cli compose start-ostree --ref _rhel/{ProductNumber}/x86_64/edge_ --parent _parent-OSTree-REF_ --url _URL_ _blueprint-name_ _image-type_
----
+
For example:
+
** To create a new RHEL for Edge commit based on a `parent` and with a new `ref`, run the following command:
+
[subs="quotes,attributes"]
----
# composer-cli compose start-ostree --ref _rhel/{ProductNumber}/x86_64/edge_ --parent _rhel/{ProductNumber}/x86_64/edge_ --url _http://10.0.2.2:8080/repo_ _rhel_update_ _edge-commit_
----

** To create a new RHEL for Edge commit based on the same `ref`, run the following command:
+
[subs="quotes,attributes"]
----
# composer-cli compose start-ostree --ref rhel/{ProductNumber}/x86_64/edge --url _http://10.0.2.2:8080/repo_ _rhel_update_ _edge-commit_
----
+
Where:

* The _--ref_ argument specifies the same path value that you used to build an OSTree repository.
* The _--parent_ argument specifies the parent commit. It can be ref to be resolved and pulled, for example `rhel/{ProductNumber}/x86_64/edge`, or the `Commit ID` that you can find in the extracted `.tar` file. 
* _blueprint-name_ is the RHEL for Edge blueprint name.
* The `--url` argument specifies the URL to the OSTree repository of the commit to embed in the image, for example, \http://10.0.2.2:8080/repo.
* _image-type_ is `edge-commit` for *network-based deployment*.
+
[NOTE] 
====
* The `--parent` argument can only be used for the `RHEL for Edge Commit (.tar)` image type. Using the `--url` and `--parent` arguments together results in errors with the `RHEL for Edge Container (.tar)` image type.
* If you omit the `parent ref` argument, the system falls back to the `ref` specified by the `--ref` argument.
====
+
A confirmation that the composer process has been added to the queue appears. It also shows a Universally Unique Identifier (UUID) number for the image created.
Use the UUID number to track your build. Also keep the UUID number handy for further tasks.

. Check the image compose status.
+
[subs="quotes,attributes"]
----
# composer-cli compose status
----
+
The output displays the status in the following format:
+
[subs="quotes,attributes"]
----
_&lt;UUID&gt;_ RUNNING date _blueprint-name_ _blueprint-version_ _image-type_
----
+
NOTE: The image creation process takes a few minutes to complete.
+
(Optional) To interrupt the image creation process, run:
+
[subs="quotes,attributes"]
----
# composer-cli compose cancel _&lt;UUID&gt;_
----
+
(Optional) To delete an existing image, run:
+
[subs="quotes,attributes"]
----
# composer-cli compose delete _&lt;UUID&gt;_
----

After the image creation is complete, to upgrade an existing OSTree deployment, you need:

* Set up a repository. See 
ifdef::composing_installing_and_managing_rhel_for_edge_images[]
xref:installing-rpm-ostree-images_composing-installing-managing-rhel-for-edge-images[Deploying a RHEL for Edge image]
endif::[]
ifndef::composing_installing_and_managing_rhel_for_edge_images[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/composing_installing_and_managing_rhel_for_edge_images/installing-rpm-ostree-images_composing-installing-managing-rhel-for-edge-images[Deploying a RHEL for Edge image]
endif::[]
.
* Add this repository as a remote, that is, the http or https endpoint that hosts the OSTree content.
*  Pull the new OSTree commit onto their existing running instance. See
ifdef::composing_installing_and_managing_rhel_for_edge_images[]
xref:xref:editing-a-rhel-for-edge-image-blueprint-using-command-line-interface_managing-rhel-for-edge-images[Deploying RHEL for Edge image updates manually]
endif::[]
ifndef::composing_installing_and_managing_rhel_for_edge_images[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/composing_installing_and_managing_rhel_for_edge_images/installing-rpm-ostree-images_composing-installing-managing-rhel-for-edge-images[Deploying RHEL for Edge image updates manually]
endif::[]
.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/composing_installing_and_managing_rhel_for_edge_images/composing-a-rhel-for-edge-image-using-image-builder-command-line_composing-installing-managing-rhel-for-edge-images[Creating a system image with RHEL image builder in the command-line interface].
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/composing_installing_and_managing_rhel_for_edge_images/composing-a-rhel-for-edge-image-using-image-builder-command-line_composing-installing-managing-rhel-for-edge-images#downloading-a-rhel-for-edge-image-using-the-command-line_composing-a-rhel-for-edge-image-using-image-builder-command-line[Downloading a RHEL for Edge image by using the RHEL image builder command-line interface].
