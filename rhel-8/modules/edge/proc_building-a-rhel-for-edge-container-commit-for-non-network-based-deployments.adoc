:_mod-docs-content-type: PROCEDURE

[id="building-a-rhel-for-edge-container-commit-for-non-network-based-deployments_{context}"]
= Creating a RHEL for Edge Container image for non-network-based deployments

[role="_abstract"]
You can build a running container by loading the downloaded RHEL for Edge Container OSTree commit into Podman. For that, follow the steps:

.Prerequisites

* You created and downloaded a RHEL for Edge Container OSTree commit.
* You have installed `Podman` on your system. See link:https://access.redhat.com/solutions/3650231[How do I install Podman in RHEL].

.Procedure

. Navigate to the directory where you have downloaded the RHEL for Edge Container OSTree commit.

. Load the RHEL for Edge Container OSTree commit into `Podman`.
+
[subs="quotes,attributes"]
----
$ sudo podman load -i _UUID_-container.tar
----
+
The command output provides the image ID, for example: `@8e0d51f061ff1a51d157804362bc875b649b27f2ae1e66566a15e7e6530cec63`

. Tag the new RHEL for Edge Container image, using the image ID generated by the previous step.
+
[subs="quotes,attributes"]
----
$ sudo podman tag _image-ID_ _localhost/edge-container_
----
+
The `podman tag` command assigns an additional name to the local image. 

. Run the container named `edge-container`.
+
[subs="quotes,attributes"]
----
$ sudo podman run -d --name=_edge-container_ -p 8080:8080 _localhost/edge-container_
----
+
The `podman run -d --name=edge-container` command assigns a name to your container-based on the `localhost/edge-container` image.

. List containers:
+
[subs="quotes,attributes"]
----
$ sudo podman ps -a 
CONTAINER ID  IMAGE                               	COMMAND	CREATED    	STATUS                	PORTS   NAMES
2988198c4c4b  …./localhost/edge-container   /bin/bash  3 seconds ago  Up 2 seconds ago      	edge-container
----

As a result, `Podman` runs a container that serves an OSTree repository with the RHEL for Edge Container commit.
