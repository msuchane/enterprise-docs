:_mod-docs-content-type: PROCEDURE

[id="proc_creating-a-rhel-for-edge-container-image-image-using-image-builder-in-rhel-web-console_{context}"]
= Creating a RHEL for Edge Container image by using RHEL image builder in RHEL web console

[role="_abstract"]
You can create RHEL for Edge images by selecting *“RHEL for Edge Container (.tar)”*. The *RHEL for Edge Container (.tar)* image type creates an OSTree commit and embeds it into an OCI container with a web server. When the container is started, the web server serves the commit as an OSTree repository.

Follow the steps in this procedure to create a RHEL for Edge Container image using image builder in RHEL web console. 

.Prerequisites

* On a RHEL system, you have accessed the RHEL image builder dashboard.
* You have created a blueprint. 

.Procedure

. On the RHEL image builder dashboard click btn:[Create Image].

. On the *Image output* page, perform the following steps:

. From the *Select a blueprint* dropdown menu, select the blueprint you want to use.
.. From the *Image output type* dropdown list, select *“RHEL for Edge Container (.tar)”* for network-based deployment.  
.. Click btn:[Next].

.. On the *OSTree* page, enter:
... *Repository URL*: specify the URL to the OSTree repository of the commit to embed in the image. For example, \http://10.0.2.2:8080/repo/. By default, the repository folder for a RHEL for Edge Container image is "/repo". 
+
To find the correct URL to use, access the running container and check the `nginx.conf` file. To find which URL to use, access the running container and check the `nginx.conf` file. Inside the `nginx.conf` file, find the `root` directory entry to search for the `/repo/` folder information. Note that, if you do not specify a repository URL when creating a RHEL for Edge Container image `(.tar)` by using RHEL image builder, the default `/repo/` entry is created in the `nginx.conf` file.

... *Parent commit*: specify a previous commit, or leave it empty if you do not have a commit at this time.
... In the *Ref* textbox, specify a reference path for where your commit is going to be created. By default, the web console specifies `rhel/{ProductNumberLink}/$ARCH/edge`. The "$ARCH" value is determined by the host machine. Click btn:[Next].

.. On the *Review* page, check the customizations. Click btn:[Save blueprint]. 

. Click btn:[Create].
+
RHEL image builder starts to create a RHEL for Edge Container image for the blueprint that you created.
+
NOTE: The image creation process takes up to 20 minutes to complete. 

.Verification

. To check the RHEL for Edge Container image creation progress:
+
.. Click the btn:[Images] tab. 

After the image creation process is complete, you can download the resulting *“RHEL for Edge Container (.tar)”* image.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/composing_installing_and_managing_rhel_for_edge_images/composing-rhel-for-edge-images-using-image-builder-in-rhel-web-console_composing-installing-managing-rhel-for-edge-images#downloading-a-rhel-for-edge-image_composing-rhel-for-edge-images-using-image-builder-in-rhel-web-console[Downloading a RHEL for Edge image]
