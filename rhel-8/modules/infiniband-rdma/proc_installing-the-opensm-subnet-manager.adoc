:_mod-docs-content-type: PROCEDURE

:experimental:

[id="installing-the-opensm-subnet-manager_{context}"]
= Installing the OpenSM subnet manager

[role="_abstract"]
`OpenSM` is a subnet manager and administrator that follows the InfiniBand specifications to initialize InfiniBand hardware where at least one instance of `OpenSM` service always runs.

.Procedure

. Install the `opensm` package:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# **`{PackageManagerCommand}` install opensm**
....

. Configure OpenSM in case the default installation does not match your environment.
+
With only one InfiniBand port, the host acts as the master subnet manager that does not require any custom changes. The default configuration works without any modification.

. Enable and start the `opensm` service:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# **systemctl enable --now opensm**
....

[role="_additional-resources"]
.Additional resources
* `opensm(8)` man page
