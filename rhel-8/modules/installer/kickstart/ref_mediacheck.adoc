:_mod-docs-content-type: REFERENCE
[id="mediacheck_{context}"]
= mediacheck

The [command]`mediacheck` Kickstart command is optional. This command forces the installation program to perform a media check before starting the installation. This command requires that installations be attended, so it is disabled by default.


.Syntax

[subs="quotes,macros"]
----
[command]`mediacheck`
----


.Notes

* This Kickstart command is equivalent to the [option]`rd.live.check` boot option.

* This command has no options.

