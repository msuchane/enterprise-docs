:_mod-docs-content-type: REFERENCE
[id="zfcp_{context}"]
= zfcp

The [command]`zfcp` Kickstart command is optional. It defines a Fibre channel device.

This option only applies on 64-bit IBM{nbsp}Z.
ifeval::[{ProductNumber} == 8]
All of the options described below must be specified.
endif::[]

.Syntax

[subs="quotes,macros"]
----
[command]``zfcp --devnum=__devnum__ [--wwpn=__wwpn__ --fcplun=__lun__]``
----


.Options

* [option]`--devnum=` - The device number (zFCP adapter device bus ID).

* [option]`--wwpn=` - The device's World Wide Port Name (WWPN). Takes the form of a 16-digit number, preceded by `0x`.

* [option]`--fcplun=` - The device's Logical Unit Number (LUN). Takes the form of a 16-digit number, preceded by `0x`.

[NOTE]
====
It is sufficient to specify an FCP device bus ID if automatic LUN scanning is available and when installing {ProductNumber} or later releases. Otherwise all three parameters are required.
Automatic LUN scanning is available for FCP devices operating in NPIV mode if it is not disabled through the `zfcp.allow_lun_scan` module parameter (enabled by default). It provides access to all SCSI devices found in the storage area network attached to the FCP device with the specified bus ID.
====

.Example

[subs="quotes,macros"]
----
[command]`zfcp --devnum=0.0.4000 --wwpn=0x5005076300C213e9 --fcplun=0x5022000000000000`
[command]`zfcp --devnum=0.0.4000`
----
