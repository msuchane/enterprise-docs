:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="using-a-prepared-fcp-attached-scsi-disk_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using a Prepared FCP attached SCSI Disk
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
Perform the following steps to boot from a prepared FCP-attached SCSI disk:

.Procedure

. Configure the SCSI boot loader of z/VM to access the prepared SCSI disk in the FCP Storage Area Network. Select the prepared *zipl* boot menu entry referring to the Red Hat Enterprise Linux installation program. Use a command of the following form:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[command]`cp set loaddev portname _WWPN_ lun LUN bootprog _boot_entry_number_`
....
+
Replace _WWPN_ with the World Wide Port Name of the storage system and _LUN_ with the Logical Unit Number of the disk. The 16-digit hexadecimal numbers must be split into two pairs of eight digits each. For example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[command]`cp set loaddev portname 50050763 050b073d lun 40204011 00000000 bootprog 0`
....

. Optionally, confirm your settings with the command:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[command]`query loaddev`
....

. Boot the FCP device connected with the storage system containing the disk with the following command:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[command]`cp ipl _FCP_device_`
....
+
For example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[command]`cp ipl fc00`
....
