:_mod-docs-content-type: CONCEPT
[id="installation-source-boot-options_{context}"]
= Installation source boot options

[role="_abstract"]
This section describes various installation source boot options.

inst.repo=::
The `inst.repo=` boot option specifies the installation source, that is, the location providing the package repositories and a valid `.treeinfo` file that describes them. For example: `inst.repo=cdrom`.
The target of the `inst.repo=` option must be one of the following installation media:

* an installable tree, which is a directory structure containing the installation program images, packages, and repository data as well as a valid `.treeinfo` file
* a DVD (a physical disk present in the system DVD drive)
* an ISO image of the full Red Hat Enterprise Linux installation DVD, placed on a disk or a network location accessible to the system.
+
Use the `inst.repo=` boot option to configure different installation methods using different formats. The following table contains details of the `inst.repo=` boot option syntax:
+
.Types and format for the inst.repo= boot option and installation source
[options="header",cols="1,3,2"]
|===
| Source type  | Boot option format | Source format
| CD/DVD drive  | `inst.repo=cdrom:__<device>__` | Installation DVD as a physical disk. footnote:[If __device__ is left out, installation program automatically searches for a drive containing the installation DVD.]
| Mountable device (HDD and USB stick) | `inst.repo=hd:__<device>__:/__<path>__` | Image file of the installation DVD.
| NFS Server  | `inst.repo=nfs:&#91;__options__:&#93;__<server>__:/__<path>__` | Image file of the installation DVD, or an installation tree, which is a complete copy of the directories and files on the installation DVD. footnote:[The NFS Server option uses NFS protocol version 3 by default. To use a different version, add `nfsvers=__X__` to __options__, replacing _X_ with the version number that you want to use.]
| HTTP Server | `inst.repo=http://__<host>__/__<path>__` .3+| Installation tree that is a complete copy of the directories and files on the installation DVD.
| HTTPS Server | `inst.repo=https://__<host>__/__<path>__`
| FTP Server | `inst.repo=ftp://__<username>__:__<password>__@__<host>__/__<path>__`
| HMC  | `inst.repo=hmc` |
|===

Set disk device names with the following formats:

* Kernel device name, for example `/dev/sda1` or `sdb2`
* File system label, for example `LABEL=Flash` or `LABEL=RHEL8`
* File system UUID, for example `UUID=8176c7bf-04ff-403a-a832-9557f94e61db`

Non-alphanumeric characters must be represented as `\xNN`, where _NN_ is the hexadecimal representation of the character. For example, `\x20` is a white space `(" ")`.

inst.addrepo=::
Use the `inst.addrepo=` boot option to add an additional repository that you can use as another installation source along with the main repository (`inst.repo=`). You can use the `inst.addrepo=` boot option multiple times during one boot. The following table contains details of the `inst.addrepo=` boot option syntax.
+
[NOTE]
====
The `REPO_NAME` is the name of the repository and is required in the installation process. These repositories are only used during the installation process; they are not installed on the installed system.
====

For more information about unified ISO, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/considerations_in_adopting_rhel_8/installer-and-image-creation_considerations-in-adopting-rhel-8#unified_iso[Unified ISO.]

.Installation sources and boot option format
[options="header"]
|===
| Installation source  | Boot option format | Additional information
| Installable tree at a URL  | `inst.addrepo=REPO_NAME,[http,https,ftp]://_<host>_/_<path>_` | Looks for the installable tree at a given URL.
| Installable tree at an NFS path   | `inst.addrepo=REPO_NAME,nfs://_<server>_:/_<path>_` | Looks for the installable tree at a given NFS path. A colon is required after the host. The installation program passes everything after `nfs://` directly to the mount command instead of parsing URLs according to RFC 2224.
| Installable tree in the installation environment | `inst.addrepo=REPO_NAME,file://_<path>_` | Looks for the installable tree at the given location in the installation environment. To use this option, the repository must be mounted before the installation program attempts to load the available software groups. The benefit of this option is that you can have multiple repositories on one bootable ISO, and you can install both
 the main repository and additional repositories from the ISO. The path to the additional repositories is `/run/install/source/REPO_ISO_PATH`. Additionally, you can mount the repository directory in the `%pre` section in the Kickstart file. The path must be absolute and start with `/`, for example `inst.addrepo=REPO_NAME,file:///_<path>_`
| Disk | `inst.addrepo=REPO_NAME,hd:__<device>__:__<path>__` | Mounts the given _<device>_ partition and installs from the ISO that is specified by the _<path>_. If the _<path>_ is not specified, the installation program looks for a valid installation ISO on the _<device>_. This installation method requires an ISO with a valid installable tree.
|===


inst.stage2=::

The `inst.stage2=` boot option specifies the location of the installation program’s runtime image. This option expects the path to a directory that contains a valid `.treeinfo` file and reads the runtime image location from the `.treeinfo` file. If the `.treeinfo` file is not available, the installation program attempts to load the image from `images/install.img`.
+
When you do not specify the `inst.stage2` option, the installation program attempts to use the location specified with the `inst.repo` option.
+
Use this option when you want to manually specify the installation source in the installation program at a later time. For example, when you want to select the Content Delivery Network (CDN) as an installation source. The installation DVD and Boot ISO already contain a suitable `inst.stage2` option to boot the installation program from the respective ISO.
+
If you want to specify an installation source, use the `inst.repo=` option instead.
+
[NOTE]
By default, the `inst.stage2=` boot option is used on the installation media and is set to a specific label; for example, `inst.stage2=hd:LABEL=RHEL-x-0-0-BaseOS-x86_64`. If you modify the default label of the file system that contains the runtime image, or if you use a customized procedure to boot the installation system, verify that the `inst.stage2=` boot option is set to the correct value.

inst.noverifyssl::
Use the `inst.noverifyssl` boot option to prevent the installer from verifying SSL certificates for all HTTPS connections with the exception of additional Kickstart repositories, where `--noverifyssl` can be set per repository.
+
For example, if your remote installation source is using self-signed SSL certificates, the `inst.noverifyssl` boot option enables the installer to complete the installation without verifying the SSL certificates.
+
.Example when specifying the source using `inst.stage2=`
----
inst.stage2=https://hostname/path_to_install_image/ inst.noverifyssl
----
+
.Example when specifying the source using `inst.repo=`
----
inst.repo=https://hostname/path_to_install_repository/ inst.noverifyssl
----

inst.stage2.all::
Use the `inst.stage2.all` boot option to specify several HTTP, HTTPS, or FTP sources. You can use the `inst.stage2=` boot option multiple times with the `inst.stage2.all` option to fetch the image from the sources sequentially until one succeeds.
For example:
+
[subs="macros,attributes"]
----
inst.stage2.all
inst.stage2=http://hostname1/path_to_install_tree/
inst.stage2=http://hostname2/path_to_install_tree/
inst.stage2=http://hostname3/path_to_install_tree/
----


inst.dd=::
The `inst.dd=` boot option is used to perform a driver update during the installation. For more information about how to update drivers during installation, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_an_advanced_rhel_{ProductNumber}_installation/index/[_Performing an advanced RHEL {ProductNumber} installation_] document.

inst.repo=hmc::
This option eliminates the requirement of an external network setup and expands the installation options. When booting from a Binary DVD, the installation program prompts you to enter additional kernel parameters. To set the DVD as an installation source, append the `inst.repo=hmc` option to the kernel parameters. The installation program then enables support element (SE) and hardware management console (HMC) file access, fetches the images for stage2 from the DVD, and provides access to the packages on the DVD for software selection.

inst.proxy=::
The `inst.proxy=` boot option is used when performing an installation from a HTTP, HTTPS, and FTP protocol. For example:
+
[subs="macros,attributes"]
----
[PROTOCOL://][USERNAME[:PASSWORD]@]HOST[:PORT]
----

inst.nosave=::
Use the `inst.nosave=` boot option to control the installation logs and related files that are not saved to the installed system, for example `input_ks`, `output_ks`, `all_ks`, `logs` and `all`. You can combine multiple values separated by a comma. For example,
+
....
inst.nosave=Input_ks,logs
....

+
[NOTE]
====
The `inst.nosave` boot option is used for excluding files from the installed system that cannot be removed by a Kickstart %post script, such as logs and input/output Kickstart results.
====

`input_ks`:: Disables the ability to save the input Kickstart results.
`output_ks`:: Disables the ability to save the output Kickstart results generated by the installation program.
`all_ks`:: Disables the ability to save the input and output Kickstart results.
`logs`:: Disables the ability to save all installation logs.
`all`:: Disables the ability to save all Kickstart results, and all logs.


inst.multilib::
Use the `inst.multilib` boot option to set DNF's `multilib_policy` to *all*, instead of *best*.

inst.memcheck::
The `inst.memcheck` boot option performs a check to verify that the system has enough RAM to complete the installation. If there is not enough RAM, the installation process is stopped. The system check is approximate and memory usage during installation depends on the package selection, user interface, for example graphical or text, and other parameters.

inst.nomemcheck::
The `inst.nomemcheck` boot option does not perform a check to verify if the system has enough RAM to complete the installation. Any attempt to perform the installation with less than the recommended minimum amount of memory is unsupported, and might result in the installation process failing.
