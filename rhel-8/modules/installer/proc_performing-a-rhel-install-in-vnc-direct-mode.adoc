:_mod-docs-content-type: PROCEDURE

[id="performing-a-rhel-install-in-vnc-direct-mode_{context}"]
= Performing a remote RHEL installation in VNC Direct mode

[role="_abstract"]
Use this procedure to perform a remote RHEL installation in VNC Direct mode. Direct mode expects the VNC viewer to initiate a connection to the target system that is being installed with RHEL. In this procedure, the system with the VNC viewer is called the *remote* system. You are prompted by the RHEL installation program to initiate the connection from the VNC viewer on the remote system to the target system.

[NOTE]
====
This procedure uses *TigerVNC* as the VNC viewer. Specific instructions for other viewers might differ, but the general principles apply.
====

.Prerequisites

* You have installed a VNC viewer on a remote system as a root user.
* You have set up a network boot server and booted the installation on the target system.

.Procedure

. From the RHEL boot menu on the target system, press the `Tab` key on your keyboard to edit the boot options.
. Append the `inst.vnc` option to the end of the command line.
.. If you want to restrict VNC access to the system that is being installed, add the `inst.vncpassword=PASSWORD` boot option to the end of the command line. Replace *PASSWORD* with the password you want to use for the installation. The VNC password must be between 6 and 8 characters long.
+
[IMPORTANT]
====
Use a temporary password for the `inst.vncpassword=` option. It should not be an existing or root password.
====
+
. Press *Enter* to start the installation. The target system initializes the installation program and starts the necessary services. When the system is ready, a message is displayed providing the IP address and port number of the system.

. Open the VNC viewer on the remote system.

. Enter the IP address and the port number into the *VNC server* field.

. Click *Connect*.

. Enter the VNC password and click *OK*. A new window opens with the VNC connection established, displaying the RHEL installation menu. From this window, you can install RHEL on the target system using the graphical user interface.
