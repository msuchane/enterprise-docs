:_mod-docs-content-type: PROCEDURE
[id="fcp-luns-that-are-not-part-of-the-root-file-system_{context}"]
= FCP LUNs that are not part of the root file system

[role="_abstract"]
FCP LUNs that are not part of the root file system, such as data disks, are persistently configured in the file `/etc/zfcp.conf`. It contains one FCP LUN per line. Each line contains the device bus ID of the FCP adapter, the target WWPN as 16 digit hexadecimal number prefixed with `0x`, and the FCP LUN prefixed with `0x` and padded with zeroes to the right to have 16 hexadecimal digits, separated by a space or tab.

ifeval::[{ProductNumber} == 8]
The WWPN and FCP LUN values are only necessary if the `zFCP` device is not configured in NPIV mode, when `auto LUN` scanning is disabled by the `zfcp.allow_lun_scan=0` kernel module parameter or when installing RHEL-8.6 or older releases. Otherwise they can be omitted and only the device bus ID is mandatory.
endif::[]

ifeval::[{ProductNumber} == 9]
The WWPN and FCP LUN values are only necessary if the `zFCP` device is not configured in NPIV mode, when `auto LUN` scanning is disabled by the `zfcp.allow_lun_scan=0` kernel module parameter or when installing RHEL-9.0 or older releases. Otherwise they can be omitted and only the device bus ID is mandatory.
endif::[]

Entries in `/etc/zfcp.conf` are activated and configured by udev when an FCP adapter is added to the system. At boot time, all FCP adapters visible to the system are added and trigger [application]*udev*.

Example content of `/etc/zfcp.conf`:

[literal,subs="+quotes,attributes,verbatim"]
....
0.0.fc00 0x5105074308c212e9 0x401040a000000000
0.0.fc00 0x5105074308c212e9 0x401040a100000000
0.0.fc00 0x5105074308c212e9 0x401040a300000000
0.0.fcd0 0x5105074308c2aee9 0x401040a000000000
0.0.fcd0 0x5105074308c2aee9 0x401040a100000000
0.0.fcd0 0x5105074308c2aee9 0x401040a300000000
0.0.4000
0.0.5000
....

Modifications of `/etc/zfcp.conf` only become effective after a reboot of the system or after the dynamic addition of a new FCP channel by changing the system's I/O configuration (for example, a channel is attached under z/VM). Alternatively, you can trigger the activation of a new entry in `/etc/zfcp.conf` for an FCP adapter which was previously not active, by executing the following commands:

ifeval::[{ProductNumber} == 8]
. Use the [command]`cio_ignore` utility to remove the FCP adapter from the list of ignored devices and make it visible to Linux:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] cio_ignore -r device_number
....
+
Replace _device_number_ with the device number of the FCP adapter. For example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] cio_ignore -r fcfc
....

. To trigger the uevent that activates the change, issue:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] echo add > /sys/bus/ccw/devices/device-bus-ID/uevent
....
+
For example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] echo add > /sys/bus/ccw/devices/0.0.fcfc/uevent
....
endif::[]


ifeval::[{ProductNumber} == 9]
. Use the `zfcp_cio_free` utility to remove the FCP adapters from the list of ignored devices and make them visible to Linux:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# zfcp_cio_free
....

. To apply the additions from `/etc/zfcp.conf` to the running system, issue:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# zfcpconf.sh
....
endif::[]
