:_mod-docs-content-type: PROCEDURE
:experimental:
[id="creating-a-bootable-usb-windows_{context}"]
= Creating a bootable USB device on Windows

[role="_abstract"]
You can create a bootable USB device on a Windows system with various tools.
Red Hat recommends using Fedora Media Writer, available for download at https://github.com/FedoraQt/MediaWriter/releases.
Note that Fedora Media Writer is a community product and is not supported by Red Hat.
You can report any issues with the tool at https://github.com/FedoraQt/MediaWriter/issues.

[IMPORTANT]
====
Following this procedure overwrites any data previously stored on the USB drive without any warning.
Back up any data or use an empty flash drive.
A bootable USB drive cannot be used for storing data.
====

.Prerequisites

ifdef::installation-title[]
* You have downloaded an installation ISO image as described in xref:downloading-a-specific-beta-iso-image_downloading-beta-installation-images[Downloading the installation ISO image].
endif::[]
ifndef::installation-title[]
* You have downloaded an installation ISO image as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#downloading-beta-installation-images_assembly_preparing-for-your-installation[Downloading the installation ISO image].
endif::[]
* You have a USB flash drive with enough capacity for the ISO image. The required size varies, but the recommended USB size is 8 GB.

.Procedure

. Download and install Fedora Media Writer from https://github.com/FedoraQt/MediaWriter/releases.
. Connect the USB flash drive to the system.
. Open Fedora Media Writer.
. From the main window, click btn:[Custom Image] and select the previously downloaded {productname} ISO image.
. From the *Write Custom Image* window, select the drive that you want to use.
. Click btn:[Write to disk]. The boot media creation process starts. Do not unplug the drive until the operation completes. The operation may take several minutes, depending on the size of the ISO image, and the write speed of the USB drive.
. When the operation completes, unmount the USB drive. The USB drive is now ready to be used as a boot device.
