:_mod-docs-content-type: REFERENCE
[id="parameters-for-kickstart-installations-on-ibm-z_{context}"]
= Parameters for kickstart installations on 64-bit IBM Z

[role="_abstract"]
The following parameters can be defined in a parameter file but do not work in a CMS configuration file.

`inst.ks=_URL_`::
+
References a Kickstart file, which usually resides on the network for Linux installations on 64-bit IBM{nbsp}Z. Replace _URL_ with the full path including the file name of the Kickstart file. This parameter activates automatic installation with Kickstart.

`inst.cmdline`::
+
This requires installation with a Kickstart file that answers all questions, because the installation program does not support interactive user input in cmdline mode. Ensure that your Kickstart file contains all required parameters before you use the `inst.cmdline` option. If a required command is missing, the installation will fail.
