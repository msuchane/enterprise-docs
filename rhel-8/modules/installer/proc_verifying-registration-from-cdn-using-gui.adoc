:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_verifing-registration-from-cdn-using-gui.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="verifying-registration-from-cdn-using-gui_{context}"]
= Verifying your system registration from the CDN

[role="_abstract"]
Use this procedure to verify that your system is registered to the CDN using the GUI.

[WARNING]
====
You can only verify your registration from the CDN if you have *not* clicked the *Begin Installation* button from the *Installation Summary* window. Once the *Begin Installation* button is clicked, you cannot return to the Installation Summary window to verify your registration.
====

.Prerequisite

* You have completed the registration process as documented in the xref:register-and-install-from-cdn-using-gui_register-and-install-from-cdn[Register and install from CDN using GUI] and _Registered_ is displayed under *Connect to Red Hat* on the *Installation Summary* window.

.Procedure

. From the *Installation Summary* window, select *Connect to Red Hat*.

. The window opens and displays a registration summary:
+
Method:: The registered account name or activation keys are displayed.
System Purpose:: If set, the role, SLA, and usage details are displayed.
Insights:: If enabled, the Insights details are displayed.
Number of subscriptions:: The number of subscriptions attached are displayed. Note: In the simple content access mode, no subscription being listed is a valid behavior.
+
. Verify that the registration summary matches the details that were entered.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/articles/simple-content-access[Simple Content Access]
