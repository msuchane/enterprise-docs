:_mod-docs-content-type: PROCEDURE
[id="creating-an-installation-source_{context}"]
= Creating an installation source on an NFS server

[role="_abstract"]
Use this installation method to install multiple systems from a single source, without having to connect to physical media.

.Prerequisites

* You have an administrator-level access to a server with {RHEL} {ProductNumber}, and this server is on the same network as the system to be installed.
ifdef::installation-title[]
* You have downloaded a DVD ISO image. For more information, see xref:downloading-a-specific-beta-iso-image_downloading-beta-installation-images[Downloading the installation ISO image].
* You have created a bootable CD, DVD, or USB device from the image file. For more information, see xref:making-an-installation-cd-or-dvd_assembly_creating-a-bootable-installation-medium[Creating a bootable DVD or CD].
* You have verified that your firewall allows the system you are installing to access the remote installation source. For more information, see xref:ports-for-network-based-installation_prepare-installation-source[Ports for network-based installation].
endif::[]
ifndef::installation-title[]
* You have downloaded a Binary DVD image. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#downloading-a-specific-beta-iso-image_downloading-beta-installation-images[Downloading the installation ISO image].
* You have created a bootable CD, DVD, or USB device from the image file. For more information, see 
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#assembly_creating-a-bootable-installation-medium_installing-RHEL[Creating installation media] 
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#assembly_creating-a-bootable-installation-medium_assembly_preparing-for-your-installation[Creating installation media].
endif::[]

* You have verified that your firewall allows the system you are installing to access the remote installation source. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#ports-for-network-based-installation_prepare-installation-source[Ports for network-based installation].
endif::[]

.Procedure

. Install the [package]`nfs-utils` package:
+
[subs="quotes, macros, attributes"]
----
# {PackageManagerCommand} install nfs-utils
----

. Copy the DVD ISO image to a directory on the NFS server.

. Open the [filename]`/etc/exports` file using a text editor and add a line with the following syntax:
+
[subs="quotes, macros, attributes"]
----
/__exported_directory__/ __clients__
----

* Replace _/exported_directory/_ with the full path to the directory with the ISO image.
* Replace __clients__ with one of the following:
+
--
** The host name or IP address of the target system
** The subnetwork that all target systems can use to access the ISO image
** To allow any system with network access to the NFS server to use the ISO image, the asterisk sign (`*`)
--
+
See the `exports(5)` man page for detailed information about the format of this field.
+
For example, a basic configuration that makes the `/rhel{ProductNumber}-install/` directory available as read-only to all clients is:
+
[subs="quotes, macros, attributes"]
----
/rhel{ProductNumber}-install *
----
. Save the [filename]`/etc/exports` file and exit the text editor.
. Start the nfs service:
+
[subs="quotes, macros, attributes"]
----
# systemctl start nfs-server.service
----
+
If the service was running before you changed the [filename]`/etc/exports` file, reload the NFS server configuration:
+
[subs="quotes, macros, attributes"]
----
# systemctl reload nfs-server.service
----
+
The ISO image is now accessible over NFS and ready to be used as an installation source.

[NOTE]
====
When configuring the installation source, use `nfs:` as the protocol, the server host name or IP address, the colon sign `(:)`, and the directory holding the ISO image. For example, if the server host name is `myserver.example.com` and you have saved the ISO image in `/rhel{ProductNumber}-install/`, specify `nfs:myserver.example.com:/rhel{ProductNumber}-install/` as the installation source.
====
