:_mod-docs-content-type: PROCEDURE

:experimental:

[id="unregistering-using-command-line_{context}"]
= Unregistering using command line

Use the [command]`unregister` command to unregister a RHEL system from Red Hat Subscription Management Server.

.Procedure

. Run the unregister command as a root user, without any additional parameters.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] subscription-manager unregister
....
+
. When prompted, provide a root password.

The system is unregistered from the Subscription Management Server, and the status 'The system is currently not registered' is displayed with the btn:[Register] button enabled.

[NOTE]
====
To continue uninterrupted services, re-register the system with either of the management services. If you do not register the system with a management service, you may fail to receive the system updates.
For more information about registering a system, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_a_standard_rhel_{ProductNumber}_installation/post-installation-tasks_installing-rhel#subman-rhel8-setup_post-installation-tasks[Registering your system using the command line].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/performing_a_standard_rhel_9_installation/assembly_installing-on-amd64-intel-64-and-64-bit-arm_installing-rhel#proc_registering-your-system-using-the-command-line_post-installation-tasks[Registering your system using the command line].
endif::[]
====

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_subscription_management/1/html-single/rhsm/index/[_Using and Configuring Red Hat Subscription Manager_]
