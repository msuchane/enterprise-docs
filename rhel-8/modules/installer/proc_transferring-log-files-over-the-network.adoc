:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_transferring-log-files-over-the-network.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.

:experimental:

[id="transferring-log-files-over-the-network_{context}"]
= Transferring installation log files over the network

[role="_abstract"]
Use this procedure to transfer installation log files over the network.

.Prerequisites

* You are logged into a root account and you have access to the installation program's temporary file system.

.Procedure

. Press kbd:[Ctrl] + kbd:[Alt] + kbd:[F2] to access a shell prompt on the system you are installing.

. Switch to the `/tmp` directory where the log files are located:
+
[subs="quotes, macros, attributes"]
----
# cd /tmp
----
+
. Copy the log files onto another system on the network using the `scp` command:
+
[subs="quotes, macros, attributes"]
----
# scp *log user@address:path
----
+
.. Replace *user* with a valid user name on the target system, *address* with the target system's address or host name, and *path* with the path to the directory where you want to save the log files. For example, if you want to log in as `john` on a system with an IP address of 192.168.0.122 and place the log files into the `/home/john/logs/` directory on that system, the command is as follows:
+
[subs="quotes, macros, attributes"]
----
# scp *log john@192.168.0.122:/home/john/logs/
----
+
When connecting to the target system for the first time, the SSH client asks you to confirm that the fingerprint of the remote system is correct and that you want to continue:
+
[subs="quotes, macros, attributes"]
----
The authenticity of host '192.168.0.122 (192.168.0.122)' can't be established.
ECDSA key fingerprint is a4:60:76:eb:b2:d0:aa:23:af:3d:59:5c:de:bb:c4:42.
Are you sure you want to continue connecting (yes/no)?
----
+
.. Type *yes* and press kbd:[Enter] to continue. Provide a valid password when prompted. The files are transferred to the specified directory on the target system.
