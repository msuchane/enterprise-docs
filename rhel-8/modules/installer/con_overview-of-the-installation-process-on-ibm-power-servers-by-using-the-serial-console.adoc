////
This module is included in assembly_rhel_installations-on-ibm-power-servers.adoc
////

[id="overview-of-the-installation-process-on-ibm-power-servers-by-using-the-serial-console_{context}"]
= Overview of the installation process on IBM Power Servers by using the serial console

You can install {ProductShortName} on IBM Power Systems servers by using the serial console. 

The installation workflow involves the following general steps:

. xref:preparing-installation-media-for-installing-rhel-on-ibm-power-servers_{context}[Download the RHEL installation ISO].
. Prepare a bootable physical installation medium based on your installation method.
. Prepare your machine for the RHEL installation. 
. Boot the installer kernel. 
. Start a VNC session. 
+
For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/performing_a_standard_rhel_9_installation/assembly_performing-a-remote-installation-using-vnc_installing-rhel[Performing a remote RHEL installation by using VNC]. 
. Install Red Hat Enterprise Linux.
. Optional: Install IBM Tools Repository to use additional software. For more information, see link:https://www.ibm.com/docs/en/linux-on-systems?topic=servers-linux-power-tools-repository[IBM Linux on Power tools repository]. 

For detailed instructions, see link:https://www.ibm.com/docs/en/linux-on-systems?topic=ilps-installing-linux-power-systems-servers-by-using-serial-console[Installing Linux on Power Systems servers by using the serial console] in the IBM documentation.