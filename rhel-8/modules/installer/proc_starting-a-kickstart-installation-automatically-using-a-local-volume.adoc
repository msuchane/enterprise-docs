:_mod-docs-content-type: PROCEDURE
[id="starting-a-kickstart-installation-automatically-using-a-local-volume_{context}"]
= Starting a Kickstart installation automatically using a local volume

[role="_abstract"]
You can start a Kickstart installation by putting a Kickstart file with a specific name on a specifically labelled storage volume.


.Prerequisites

ifdef::installation-advanced-title[]
* You have a volume prepared with xref:making-a-kickstart-file-available-on-a-local-volume-for-automatic-loading_making-kickstart-files-available-to-the-installation-program[label `OEMDRV` and the Kickstart file present in its root as `ks.cfg`].
endif::[]
ifndef::installation-advanced-title[]
* You have a volume prepared with label `OEMDRV` and the Kickstart file present in its root as `ks.cfg`.
endif::[]

* A drive containing this volume is available on the system as the installation program boots.


.Procedure

. Boot the system using a local media (a CD, DVD, or a USB flash drive).

. At the boot prompt, specify the required boot options.

..  If a required repository is in a network location, you may need to configure the network using the [option]`ip=` option. The installer tries to configure all network devices using the DHCP protocol by default without this option.

..  In order to access a software source from which necessary packages will be installed, you may need to add the [option]`inst.repo=` option.
If you do not specify this option, you must specify the installation source in the Kickstart file.
+
ifdef::installation-advanced-title[]
For more information about installation sources, see xref:kickstart-commands-for-installation-program-configuration-and-flow-control_kickstart-commands-and-options-reference[Kickstart commands for installation program configuration and flow control].
endif::[]

ifndef::installation-advanced-title[]
For more information about installation sources, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html-single/performing_an_advanced_rhel_{ProductNumber}_installation/index#kickstart-commands-for-installation-program-configuration-and-flow-control_kickstart-commands-and-options-reference[Kickstart commands for installation program configuration and flow control].
endif::[]

.  Start the installation by confirming your added boot options.
+
The installation begins now, and the Kickstart file is automatically detected and used to start an automated Kickstart installation.

ifdef::installation-advanced-title[]
[NOTE]
====
If you have installed a {ProductName} Beta release, on systems having UEFI Secure Boot enabled, then add the Beta public key to the system's Machine Owner Key (MOK) list.
ifeval::[{ProductNumber} == 8]
For more information about UEFI Secure Boot and {ProductName} Beta releases, see xref:booting-a-beta-system-with-uefi-secure-boot_installing-rhel-as-an-experienced-user[Booting a beta system with UEFI Secure Boot].
endif::[]
====
endif::[]
ifndef::installation-advanced-title[]
[NOTE]
====
If you have installed a {ProductName} Beta release, on systems having UEFI Secure Boot enabled, then add the Beta public key to the system's Machine Owner Key (MOK) list.
ifeval::[{ProductNumber} == 8]
For more information about UEFI Secure Boot and {ProductName} Beta releases, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_a_standard_rhel_{ProductNumber}_installation/post-installation-tasks_installing-rhel[Completing post-installation tasks] section of the _Performing a standard RHEL {ProductNumber} installation_ document.
endif::[]
====
endif::[]
