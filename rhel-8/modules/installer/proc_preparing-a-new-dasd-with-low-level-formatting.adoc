:_mod-docs-content-type: PROCEDURE
[id="preparing-a-new-dasd-with-low-level-formatting_{context}"]
= Preparing a new DASD with low-level formatting

[role="_abstract"]
Once the disk is online, change back to the `/root` directory and low-level format the device. This is only required once for a DASD during its entire lifetime:

[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] cd /root
pass:quotes[`#`] dasdfmt -b 4096 -d cdl -p /dev/disk/by-path/ccw-0.0.4b2e
Drive Geometry: 10017 Cylinders * 15 Heads =  150255 Tracks

I am going to format the device /dev/disk/by-path/ccw-0.0.4b2e in the following way:
Device number of device : 0x4b2e
Labelling device        : yes
Disk label              : VOL1
Disk identifier         : 0X4B2E
Extent start (trk no)   : 0
Extent end (trk no)     : 150254
Compatible Disk Layout  : yes
Blocksize               : 4096

--->> ATTENTION! <<---
All data of that device will be lost.
Type "yes" to continue, no will leave the disk untouched: yes
cyl    97 of  3338 |#----------------------------------------------|   2%
....

When the progress bar reaches the end and the format is complete, [application]*dasdfmt* prints the following output:

[literal,subs="+quotes,attributes,verbatim"]
....
Rereading the partition table...
Exiting...
....

Now, use [application]*fdasd* to partition the DASD. You can create up to three partitions on a DASD. In our example here, we create one partition spanning the whole disk:

[literal,subs="+quotes,verbatim,macros,attributes"]
....
# fdasd -a /dev/disk/by-path/ccw-0.0.4b2e
reading volume label ..: VOL1
reading vtoc ..........: ok

auto-creating one partition for the whole disk...
writing volume label...
writing VTOC...
rereading partition table...
....

After a (low-level formatted) DASD is online, it can be used like any other disk under Linux. For example, you can create file systems, LVM physical volumes, or swap space on its partitions, for example `/dev/disk/by-path/ccw-0.0.4b2e-part1`. Never use the full DASD device (`dev/dasdb`) for anything but the commands [command]`dasdfmt` and [command]`fdasd`. If you want to use the entire DASD, create one partition spanning the entire drive as in the `fdasd` example above.

To add additional disks later without breaking existing disk entries in, for example, `/etc/fstab`, use the persistent device symbolic links under `/dev/disk/by-path/`.
