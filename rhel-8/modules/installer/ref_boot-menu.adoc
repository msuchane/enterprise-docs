:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_boot-menu.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.

:experimental:

[id="boot-menu_{context}"]
= Boot menu

[role="_abstract"]
The {RHEL} boot menu is displayed using *GRand Unified Bootloader version 2* (GRUB2) when your system has completed loading the boot media.

ifeval::[{RH} == Red{nbsp}Hat]
.{RHEL} boot menu
ifeval::[{ProductNumber} == 8]
image::Boot_menu_8.0.png["Boot menu window."]
endif::[]

ifeval::[{ProductNumber} == 9]
image::Boot_menu_9.0.png["Boot menu window."]
endif::[]
endif::[]

The boot menu provides several options in addition to launching the installation program. If you do not make a selection within 60 seconds, the default boot option (highlighted in white) is run. To select a different option, use the arrow keys on your keyboard to make your selection and press the kbd:[Enter] key.

You can customize boot options for a particular menu entry:

* *On BIOS-based systems:* Press the kbd:[Tab] key and add custom boot options to the command line. You can also access the `boot:` prompt by pressing the kbd:[Esc] key but no required boot options are preset. In this scenario, you must always specify the Linux option before using any other boot options.
* *On UEFI-based systems:* Press the kbd:[e] key and add custom boot options to the command line. When ready press kbd:[Ctrl]+kbd:[X] to boot the modified option.



.Boot menu options
ifdef::installation-title[]
[options="header"]
|====
|Boot menu option|Description
|*Install {RHEL} {ProductNumber}* |Use this option to install {RHEL} using the graphical installation program. For more information, see xref:performing-a-quick-install-with-gui_installing-RHEL[Performing a quick install with GUI]
|*Test this media & install {RHEL} {ProductNumber}* | Use this option to check the integrity of the installation media. For more information, see xref:verifying-boot-media_troubleshooting-at-the-start-of-the-installation[Verifying a boot media]
|*Troubleshooting >* |Use this option to resolve various installation issues. Press kbd:[Enter] to display its contents.
|====
endif::[]

ifndef::installation-title[]
[options="header"]
|====
|Boot menu option|Description
|*Install {RHEL} {ProductNumber}* |Use this option to install {RHEL} using the graphical installation program. For more information, link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#performing-a-quick-install-with-gui_assembly_installing-on-amd64-intel-64-and-64-bit-arm[Installing RHEL using an ISO image from the Customer Portal]
|*Test this media & install {RHEL} {ProductNumber}* | Use this option to check the integrity of the installation media. For more information, see xref:verifying-boot-media_troubleshooting-at-the-start-of-the-installation[Verifying a boot media]
|*Troubleshooting >* |Use this option to resolve various installation issues. Press kbd:[Enter] to display its contents.
|====
endif::[]

.Troubleshooting options
[options="header"]
|====
|Troubleshooting option|Description
|*Troubleshooting > Install {RHEL} {ProductNumber} in basic graphics mode* |Use this option to install {RHEL} in graphical mode even if the installation program is unable to load the correct driver for your video card. If your screen is distorted when using the Install {RHEL} {ProductNumber} option, restart your system and use this option. For more information, see xref:cannot-boot-into-the-graphical-installation_troubleshooting-at-the-start-of-the-installation[Cannot boot into graphical installation]
|*Troubleshooting > Rescue a {RHEL} system* | Use this option to repair any issues that prevent you from booting. For more information, see xref:using-rescue-mode_troubleshooting-after-installation[Using a rescue mode]
|*Troubleshooting > Run a memory test* |Use this option to run a memory test on your system. Press kbd:[Enter] to display its contents. For more information, see xref:memtest86_troubleshooting-at-the-start-of-the-installation[memtest86]
|*Troubleshooting > Boot from local drive* |Use this option to boot the system from the first installed disk. If you booted this disk accidentally, use this option to boot from the disk immediately without starting the installation program.
|====
