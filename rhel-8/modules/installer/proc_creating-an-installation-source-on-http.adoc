:_mod-docs-content-type: PROCEDURE
[id="creating-an-installation-source-on-http_{context}"]
= Creating an installation source using HTTP or HTTPS

[role="_abstract"]
You can create an installation source for a network-based installation using an installation tree, which is a directory containing extracted contents of the DVD ISO image and a valid [filename]`.treeinfo` file. The installation source is accessed over HTTP or HTTPS.

.Prerequisites

* You have an administrator-level access to a server with {RHEL} {ProductNumber}, and this server is on the same network as the system to be installed.
ifdef::installation-title[]
* You have downloaded a DVD ISO image. For more information, see xref:downloading-a-specific-beta-iso-image_downloading-beta-installation-images[Downloading the installation ISO image].
* You have created a bootable CD, DVD, or USB device from the image file. For more information, see xref:making-an-installation-cd-or-dvd_assembly_creating-a-bootable-installation-medium[Creating a bootable DVD or CD].
* You have verified that your firewall allows the system you are installing to access the remote installation source. For more information, see xref:ports-for-network-based-installation_prepare-installation-source[Ports for network-based installation].
endif::[]
ifndef::installation-title[]
* You have downloaded a Binary DVD image. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#downloading-a-specific-beta-iso-image_downloading-beta-installation-images[Downloading the installation ISO image].
* You have created a bootable CD, DVD, or USB device from the image file. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#assembly_creating-a-bootable-installation-medium_assembly_preparing-for-your-installation[Creating installation media].
* You have verified that your firewall allows the system you are installing to access the remote installation source. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#ports-for-network-based-installation_prepare-installation-source[Ports for network-based installation].
endif::[]
* The `httpd` package is installed.
* The `mod_ssl` package is installed, if you use the `https` installation source.

[WARNING]
====
ifeval::[{ProductNumber} == 8]
If your Apache web server configuration enables SSL security, prefer to enable the TLSv1.3 protocol.
By default, TLSv1.2 is enabled and you may use the TLSv1 (LEGACY) protocol.
endif::[]

ifeval::[{ProductNumber} == 9]
If your Apache web server configuration enables SSL security, prefer to enable the TLSv1.3 protocol.
By default, TLSv1.2 (LEGACY) is enabled.
endif::[]
====

[IMPORTANT]
====
If you use an HTTPS server with a self-signed certificate,
you must boot the installation program with the [option]`noverifyssl` option.
====

.Procedure

. Copy the DVD ISO image to the HTTP(S) server.
. Create a suitable directory for mounting the DVD ISO image, for example:
+
[subs="quotes, macros, attributes"]
----
# mkdir /mnt/rhel{ProductNumber}-install/
----
. Mount the DVD ISO image to the directory:
+
[subs="quotes, macros, attributes"]
----
# mount -o loop,ro -t iso9660 _/image_directory/image.iso_ /mnt/rhel{ProductNumber}-install/
----
+
Replace _/image_directory/image.iso_ with the path to the DVD ISO image.

. Copy the files from the mounted image to the HTTP(S) server root.
+
[subs="quotes, macros, attributes"]
----
# cp -r /mnt/rhel{ProductNumber}-install/ /var/www/html/
----
This command creates the [literal]`/var/www/html/rhel{ProductNumber}-install/` directory with the content of the image. Note that some other copying methods might skip the `.treeinfo` file which is required for a valid installation source. Entering the `cp` command for entire directories as shown in this procedure copies `.treeinfo` correctly.

. Start the `httpd` service:
+
[subs="quotes, macros, attributes"]
----
# systemctl start httpd.service
----
+
The installation tree is now accessible and ready to be used as the installation source.
+
[NOTE]
====
When configuring the installation source, use `http://` or `https://` as the protocol, the server host name or IP address, and the directory that contains the files from the ISO image, relative to the HTTP server root. For example, if you use HTTP, the server host name is `myserver.example.com`, and you have copied the files from the image to `/var/www/html/rhel{ProductNumber}-install/`, specify `\http://myserver.example.com/rhel{ProductNumber}-install/` as the installation source.
====

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/index/[Deploying different types of servers]
