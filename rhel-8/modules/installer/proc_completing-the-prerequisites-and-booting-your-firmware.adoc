:_mod-docs-content-type: PROCEDURE
[id="completing-the-prerequisites-and-booting-your-firmware_{context}"]
= Completing the prerequisites and booting your firmware

[role="_abstract"]
Before you power on the system, ensure that you have the following items:

* Ethernet cable
* VGA monitor with resolution set to 1024x768-60Hz
* USB Keyboard
* Power cords and outlet for your system

These instructions require you to have a network server set up with {productname} {ProductNumber}. To do so, download {productname} {ProductNumber} at link:https://access.redhat.com/downloads/content/279/ver=/rhel---8/8.2/ppc64le/product-software[Software Download page on the Customer Portal] by following the steps below:

. Log in to your Red{nbsp}Hat account (if you have not already done so).

. Select Downloads for *Red{nbsp}Hat Enterprise{nbsp}Linux for Power, little endian* in the *Product Variant* list.

. Select the correct version in the *Version* list, next to the *Product Variant* list.

. Click *_Download Now_* beside the *{productname} {ProductNumber}.x DVD_ ISO* file in the *Product Software* tab.

Once {productname} {ProductNumber} is downloaded, complete the steps below to boot your firmware:

* Optional: If your system belongs in a rack, install your system into that rack. For instructions, see IBM Power Systems information at link:https://www.ibm.com/support/knowledgecenter/POWER9/p9hdx/POWER9welcome.htm[https://www.ibm.com/support/knowledgecenter/POWER9/p9hdx/POWER9welcome.htm].

* Connect an Ethernet cable to the embedded Ethernet port next to the serial port on the back of your system. Connect the other end to your network.

* Connect your VGA monitor to the VGA port on the back of the system.

* Connect your USB keyboard to an available USB port.

* Connect the power cords to the system and plug them into the outlets.

At this point, your firmware is booting. Wait for the green LED on the power button to start flashing, indicating that it is ready to use. If your system does not have a green LED indicator light, then wait from 1 to 2 minutes to finish the firmware booting successfully.
