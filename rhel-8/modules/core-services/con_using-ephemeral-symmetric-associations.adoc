:_mod-docs-content-type: CONCEPT
[id="using-ephemeral-symmetric-associations_{context}"]

= Using ephemeral symmetric associations

[role="_abstract"]
In {RHEL} 7, `ntpd` supported ephemeral symmetric associations, which can be mobilized by packets from peers which are not specified in the [filename]`ntp.conf` configuration file. In {RHEL} 8, `chronyd` needs all peers to be specified in [filename]`chrony.conf`. Ephemeral symmetric associations are not supported.

Note that using the client/server mode enabled by the [command]`server` or [command]`pool` directive is more secure compared to the symmetric mode enabled by the [command]`peer` directive.
