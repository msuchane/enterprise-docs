:_mod-docs-content-type: PROCEDURE
ifdef::context[:parent-context-of-restoring-data-using-the-mariabackup-tool: {context}]

[id="restoring-data-using-the-mariabackup-tool_{context}"]
= Restoring data using the Mariabackup utility

:context: restoring-data-using-the-mariabackup-tool

[role="_abstract"]
When the backup is complete, you can restore the data from the backup by using the [command]`mariabackup` command with one of the following options:

* [option]`--copy-back` allows you to keep the original backup files.
* [option]`--move-back` moves the backup files to the data directory and removes the original backup files.

To restore data using the [application]*Mariabackup* utility, use the following procedure.

.Prerequisites

* Verify that the `mariadb` service is not running:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl stop mariadb.service*
----
* Verify that the data directory is empty.
* Users of [application]*Mariabackup* must have the `RELOAD`, `LOCK TABLES`, and `REPLICATION CLIENT` privileges.


.Procedure

. Run the [command]`mariabackup` command:
+
* To restore data and keep the original backup files, use the [option]`--copy-back` option:
+
[literal,subs="+quotes,attributes"]
----
$ *mariabackup --copy-back --target-dir=/var/mariadb/backup/*
----
+
* To restore data and remove the original backup files, use the [option]`--move-back` option:
+
[literal,subs="+quotes,attributes"]
----
$ *mariabackup --move-back --target-dir=/var/mariadb/backup/*
----

. Fix the file permissions.
+
When restoring a database, [application]*Mariabackup* preserves the file and directory privileges of the backup. However, [application]*Mariabackup* writes the files to disk as the user and group restoring the database. After restoring a backup, you may need to adjust the owner of the data directory to match the user and group for the *MariaDB* server, typically `mysql` for both.
+
For example, to recursively change ownership of the files to the `mysql` user and group:
+
[literal,subs="+quotes,attributes"]
----
# *chown -R mysql:mysql /var/lib/mysql/*
----

. Start the `mariadb` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl start mariadb.service*
----

[role="_additional-resources"]
.Additional resources
* link:https://mariadb.com/kb/en/library/full-backup-and-restore-with-mariabackup/[Full Backup and Restore with Mariabackup]

:context: {parent-context-of-restoring-data-using-the-mariabackup-tool}


ifdef::parent-context-of-restoring-data-using-the-mariabackup-tool[:context: {parent-context-of-restoring-data-using-the-mariabackup-tool}]
ifndef::parent-context-of-restoring-data-using-the-mariabackup-tool[:!context:]
