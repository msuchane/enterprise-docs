:_mod-docs-content-type: PROCEDURE
[id="checking-if-chrony-is-synchronized_{context}"]

= Checking if chrony is synchronized

[role="_abstract"]
The following procedure describes how to check if [application]*chrony* is synchronized with the use of the [command]`tracking`, [command]`sources`, and [command]`sourcestats` commands.

.Procedure

. To check [application]*chrony* tracking, issue the following command:
+
[literal,subs="+quotes,verbatim,normal,macros"]
....
$ **pass:attributes[{blank}] chronyc tracking**
Reference ID    : CB00710F (ntp-server.example.net)
Stratum         : 3
Ref time (UTC)  : Fri Jan 27 09:49:17 2017
System time     :  0.000006523 seconds slow of NTP time
Last offset     : -0.000006747 seconds
RMS offset      : 0.000035822 seconds
Frequency       : 3.225 ppm slow
Residual freq   : 0.000 ppm
Skew            : 0.129 ppm
Root delay      : 0.013639022 seconds
Root dispersion : 0.001100737 seconds
Update interval : 64.2 seconds
Leap status     : Normal

....

. The sources command displays information about the current time sources that `chronyd` is accessing. To check [application]*chrony* sources, issue the following command:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ **chronyc sources**
	210 Number of sources = 3
MS Name/IP address         Stratum Poll Reach LastRx Last sample
===============================================================================
#* GPS0                          0   4   377    11   -479ns[ -621ns] +/-  134ns
^? a.b.c                         2   6   377    23   -923us[ -924us] +/-   43ms
^+ d.e.f                         1   6   377    21  -2629us[-2619us] +/-   86ms
....
+
You can specify the optional `-v` argument to print more verbose information. In this case, extra caption lines are shown as a reminder of the meanings of the columns.

. The [command]`sourcestats` command displays information about the drift rate and offset estimation process for each of the sources currently being examined by `chronyd`. To check [application]*chrony* source statistics, issue the following command:
+
[literal,subs="+quotes,verbatim,normal,macros"]
....
$ **pass:attributes[{blank}] chronyc sourcestats**
210 Number of sources = 1
Name/IP Address            NP  NR  Span  Frequency  Freq Skew  Offset  Std Dev
===============================================================================
abc.def.ghi                11   5   46m     -0.001      0.045      1us    25us
....
+
The optional argument [option]`-v` can be specified, meaning verbose. In this case, extra caption lines are shown as a reminder of the meanings of the columns.

[role="_additional-resources"]
.Additional resources
* `chronyc(1)` man page
