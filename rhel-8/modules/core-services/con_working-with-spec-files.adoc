
:_mod-docs-content-type: CONCEPT

[id="con_working-with-spec-files_{context}"]
= Working with SPEC files

[role="_abstract"]
To package new software, you need to create a new SPEC file.

You can create the SPEC file either of the following ways:

* Write the new SPEC file manually from scratch.
* Use the `rpmdev-newspec` utility.
+
This utility creates an unpopulated SPEC file, and you fill in the necessary directives and fields.

[NOTE]
====

Some programmer-focused text editors pre-populate a new `.spec` file with their own SPEC template. The `rpmdev-newspec` utility provides an editor-agnostic method.

====

The following sections use the three example implementations of the `Hello World!` program:

[cols="15%,85%"]
|====
| Software Name | Explanation of example
| bello | A program written in a raw interpreted programming language. It demonstrates when the source code does not need to be built, but only needs to be installed. If a pre-compiled binary needs to be packaged, you can also use this method because the binary would also just be a file.
| pello | A program written in a byte-compiled interpreted programming language. It demonstrates byte-compiling the source code and installing the bytecode - the resulting pre-optimized files.
| cello | A program written in a natively compiled programming language. It demonstrates a common process of compiling the source code into machine code and installing the resulting executables.
|====

For more information, see xref:what-is-source-code_creating-software-for-rpm-packaging[What is source code].

The implementations of `Hello World!` are the following:

* https://github.com/redhat-developer/rpm-packaging-guide/raw/master/example-code/bello-0.1.tar.gz[bello-0.1.tar.gz]

// * https://github.com/redhat-developer/rpm-packaging-guide/raw/master/example-code/pello-0.1.1.tar.gz[pello-0.1.1.tar.gz]
// * http://nest.test.redhat.com/mnt/qa/scratch/ksrot/tmp/pello-0.1.2.tar.gz[pello-0.1.2.tar.gz]
* https://github.com/redhat-developer/rpm-packaging-guide/raw/master/example-code/pello-0.1.2.tar.gz[pello-0.1.2.tar.gz]

* https://github.com/redhat-developer/rpm-packaging-guide/raw/master/example-code/cello-1.0.tar.gz[cello-1.0.tar.gz] ( https://raw.githubusercontent.com/redhat-developer/rpm-packaging-guide/master/example-code/cello-output-first-patch.patch[cello-output-first-patch.patch] )

As a prerequisite, these implementations need to be placed into the [filename]`~/rpmbuild/SOURCES` directory.
