:_mod-docs-content-type: CONCEPT
[id="con_finding-the-systemd-service-description_{context}"]
= Finding the systemd service description

[role="_abstract"]
You can find descriptive information about the script on the line starting with *#description*. Use this description together with the service name in the [option]`Description` option in the [Unit] section of the unit file. The header might contain similar data on the *#Short-Description* and *#Description* lines.
