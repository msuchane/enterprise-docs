:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_managing-system-services-with-systemctl.adoc


[id="listing-system-services_{context}"]
= Listing system services

[role="_abstract"]
You can list all currently loaded service units and display the status of all available service units.


.Procedure

Use the `systemctl` command to perform any of the following tasks:

* List all currently loaded service units:
+
[literal,subs="+quotes,attributes"]
----
$ *systemctl list-units --type service*
UNIT                     LOAD   ACTIVE SUB     DESCRIPTION
abrt-ccpp.service        loaded active exited  Install ABRT coredump hook
abrt-oops.service        loaded active running ABRT kernel log watcher
abrtd.service            loaded active running ABRT Automated Bug Reporting Tool
...
systemd-vconsole-setup.service loaded active exited  Setup Virtual Console
tog-pegasus.service            loaded active running OpenPegasus CIM Server

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, or a generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.

46 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'
----
+
By default, the [command]`systemctl list-units` command displays only active units. For each service unit file, the command provides an overview of the following parameters:

`UNIT`:: The full name of the service unit
`LOAD`:: The load state of the configuration file
`ACTIVE` or `SUB`:: The current high-level and low-level unit file activation state
`DESCRIPTION`:: A short description of the unit's purpose and functionality

* List *all loaded units regardless of their state*, by using the following command with the [option]`--all` or [option]`-a` command line option:
+
[literal,subs="+quotes,attributes"]
----
$ *systemctl list-units --type service --all*
----

* List the status (*enabled* or *disabled*) of all available service units:
+
[literal,subs="+quotes,attributes"]
----
$ *systemctl list-unit-files --type service*
UNIT FILE                               STATE
abrt-ccpp.service                       enabled
abrt-oops.service                       enabled
abrtd.service                           enabled
...
wpa_supplicant.service                  disabled
ypbind.service                          disabled

208 unit files listed.
----
+
For each service unit, this command displays:

`UNIT FILE`:: The full name of the service unit
`STATE`:: The information whether the service unit is enabled or disabled to start automatically during boot

[role="_additional-resources"]
.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/managing-systemd_configuring-basic-system-settings#displaying-system-service-status_managing-system-services-with-systemctl[Displaying system service status]
