:_mod-docs-content-type: PROCEDURE
[id="adding-an-user-to-the-wheel-group-with-the-users-settings-tool_{context}"]
= Adding a user to the wheel group with the Users settings tool

.Prerequisites

* Open the [application]*Users* settings tool as described in xref:opening-the-users-settings-tool_managing-users-in-a-graphical-environment[].

.Procedure

* To add a user to the administrative group `wheel`, change the `Account Type` from `Standard` to `Administrator`.



