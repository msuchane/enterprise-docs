:_mod-docs-content-type: PROCEDURE
[id="installing-mariadb_{context}"]
= Installing MariaDB

In RHEL 8, the *MariaDB* server is available in the following versions, each provided by a separate stream:

* *MariaDB 10.3*
* *MariaDB 10.5* - available since RHEL 8.4
* *MariaDB 10.11* - available since RHEL 8.10

[NOTE]
====
By design, it is impossible to install more than one version (stream) of the same module in parallel. Therefore, you must choose only one of the available streams from the `mariadb` module. You can use different versions of the *MariaDB* database server in containers, see xref:running-multiple-mariadb-versions-in-containers_{context}[Running multiple MariaDB versions in containers].

The *MariaDB* and *MySQL* database servers cannot be installed in parallel in RHEL 8 due to conflicting RPM packages. You can use the *MariaDB* and *MySQL* database servers in parallel in containers, see xref:running-multiple-mysql-mariadb-versions-in-containers_assembly_using-mysql[Running multiple MySQL and MariaDB versions in containers].
====

To install *MariaDB*, use the following procedure.

.Procedure

. Install *MariaDB* server packages by selecting a stream (version) from the `mariadb` module and specifying the `server` profile. For example:
+
[literal,subs="+quotes,attributes,verbatim,normal,normal"]
----
# *{PackageManagerCommand} module install mariadb:10.3/server*
----

. Start the `mariadb` service:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
# *systemctl start mariadb.service*
----

. Enable the `mariadb` service to start at boot:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
# *systemctl enable mariadb.service*
----

. _Recommended for MariaDB 10.3:_ To improve security when installing *MariaDB*, run the following command:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
$ *mysql_secure_installation*
----
+
The command launches a fully interactive script, which prompts for each step in the process. The script enables you to improve security in the following ways:
+
* Setting a password for root accounts
* Removing anonymous users
* Disallowing remote root logins (outside the local host)
+

NOTE: The  *mysql_secure_installation* script is no longer valuable in *MariaDB 10.5* or later. The security enhancements are part of the default behavior since *MariaDB 10.5*.
// BZ#2159717


[IMPORTANT]
====
If you want to upgrade from an earlier `mariadb` stream within RHEL 8, follow both procedures described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_managing_and_removing_user-space_components/managing-versions-of-appstream-content_using-appstream#switching-to-a-later-stream_managing-versions-of-appstream-content[Switching to a later stream] and in xref:upgrading-from-mariadb-10-3-to-mariadb-10-5_{context}[Upgrading from MariaDB 10.3 to MariaDB 10.5] or in xref:upgrading-from-mariadb-10-5-to-mariadb-10-11_{context}[Upgrading from MariaDB 10.5 to MariaDB 10.11].
====

