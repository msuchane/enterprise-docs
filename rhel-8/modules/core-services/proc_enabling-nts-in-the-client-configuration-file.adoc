:_module-type: PROCEDURE

[id="proc_enabling-nts-in-the-client-configuration-file_{context}"]
= Enabling Network Time Security (NTS) in the client configuration file

[role="_abstract"]
By default, Network Time Security (NTS) is not enabled. You can enable NTS in the `/etc/chrony.conf`. For that, perform the following steps:


.Prerequisites

* Server with the NTS support


.Procedure

In the client configuration file:

. Specify the server with the `nts` option in addition to the recommended `iburst` option.
+
[subs="quotes",options="nowrap"]
----
For example:
server _time.example.com_ iburst nts
server nts.netnod.se iburst nts
server ptbtime1.ptb.de iburst nts
----
+
. To avoid repeating the Network Time Security-Key Establishment (NTS-KE) session during system boot, add the following line to `chrony.conf`, if it is not present:
+
----
ntsdumpdir /var/lib/chrony
----
+
ifeval::[{ProductNumber} == 8]
. Add the following line to `/etc/sysconfig/network` to disable synchronization with Network Time Protocol (NTP) servers provided by `DHCP`:
+
----
PEERNTP=no
----
endif::[]

ifeval::[{ProductNumber} == 9]
. To disable synchronization with Network Time Protocol (NTP) servers provided by `DHCP`, comment out or remove the following line in `chrony.conf`, if it is present:
+
----
sourcedir /run/chrony-dhcp
----
endif::[]
+
. Save your changes.

. Restart the `chronyd` service:
+
----
systemctl restart chronyd
----


.Verification

* Verify if the `NTS` keys were successfully established:
+
[literal,subs="+quotes,attributes"]
----
# *chronyc -N authdata*

Name/IP address  Mode KeyID Type KLen Last Atmp  NAK Cook CLen
================================================================
_time.example.com_  NTS     1   15  256  33m    0    0    8  100
nts.sth1.ntp.se   NTS     1   15  256  33m    0    0    8  100
nts.sth2.ntp.se   NTS     1   15  256  33m    0    0    8  100
----
+
The `KeyID`, `Type`, and `KLen` should have non-zero values. If the value is zero, check the system log for error messages from `chronyd`.

* Verify the client is making NTP measurements:
+
[literal,subs="+quotes,attributes"]
----
# *chronyc -N sources*

MS Name/IP address Stratum Poll Reach LastRx Last sample
=========================================================
_time.example.com_   3        6   377    45   +355us[ +375us] +/-   11ms
nts.sth1.ntp.se    1        6   377    44   +237us[ +237us] +/-   23ms
nts.sth2.ntp.se    1        6   377    44   -170us[ -170us] +/-   22ms
----
+
The `Reach` column should have a non-zero value; ideally 377. If the value rarely gets 377 or never gets to 377, it indicates that NTP requests or responses are getting lost in the network.

[role="_additional-resources"]
.Additional resources
* `chrony.conf(5)` man page
