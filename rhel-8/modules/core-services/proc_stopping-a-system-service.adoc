:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_managing-system-services-with-systemctl.adoc


[id="stopping-a-system-service_{context}"]
= Stopping a system service

[role="_abstract"]
If you want to stop a system service in the current session, use the [command]`stop` command.

.Prerequisites

* Root access

.Procedure

* Stop a system service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl stop _<name>_.service*
----
+
Replace `_<name>_` with the name of the service unit you want to stop (for example, [command]`bluetooth`).



[role="_additional-resources"]
.Additional resources
* `systemctl(1)` man page
* xref:disabling-a-system-service_managing-system-services-with-systemctl[Disabling a system service to start at boot]
* xref:displaying-system-service-status_managing-system-services-with-systemctl[Displaying system service status]
