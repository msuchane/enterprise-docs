:_mod-docs-content-type: PROCEDURE

[id="accessing-the-cups-documentation_{context}"]
= Accessing the CUPS documentation
CUPS provides browser-based access to the service's documentation that is installed on the CUPS server. This documentation includes:

* Administration documentation, such as for command-line printer administration and accounting
* Man pages
* Programming documentation, such as the administration API
* References
* Specifications


.Prerequisites
* xref:installing-and-configuring-cups_configuring-printing[CUPS is installed and running].
* The IP address of the client you want to use has permissions to access the web interface.


.Procedure

. Use a browser, and access `http://_<hostname_or_ip_address>_:631/help/`:
+
image::cups-help.png[]

. Expand the entries in `Online Help Documents`, and select the documentation you want to read.

