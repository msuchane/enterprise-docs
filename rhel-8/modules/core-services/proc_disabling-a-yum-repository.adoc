:_mod-docs-content-type: PROCEDURE

:_module-type: PROCEDURE

[id="disabling-a-yum-repository_{context}"]

= Disabling a YUM repository

[role="_abstract"]
Disable a specific YUM repository to prevent particular packages from installation or update.

.Procedure

* To disable a {PackageManagerCommand} repository, use:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand}-config-manager --disable _repositoryID_*
----
Replace _repositoryID_ with the unique repository ID.
+
To list available repository IDs, see
ifndef::configuring-basic-system-settings[]
xref:listing-packages-with-yum_searching-for-software-packages[Listing packages with yum].
endif::[]

