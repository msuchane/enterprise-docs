:_mod-docs-content-type: PROCEDURE

:_module-type: PROCEDURE

[id="enabling-a-yum-repo_{context}"]

= Enabling a YUM repository

[role="_abstract"]
Once you added a `yum` repository to your system, enable it to ensure installation and updates.

.Procedure

* To enable a repository, use:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand}-config-manager --enable _repositoryID_*
----
Replace _repositoryID_ with the unique repository ID.
+
To list available repository IDs, see
ifndef::configuring-basic-system-settings[]
xref:listing-packages-with-yum_searching-for-software-packages[Listing packages with yum].
endif::[]

