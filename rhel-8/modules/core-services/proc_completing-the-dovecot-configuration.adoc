:_mod-docs-content-type: PROCEDURE

[id="completing-the-dovecot-configuration_{context}"]
= Completing the Dovecot configuration

Once you have installed and configured Dovecot, open the required ports in the `firewalld` service, and enable and start the service. Afterwards, you can test the server.


.Prerequisites

* The following has been configured in Dovecot:
** TLS encryption
** An authentication backend

* Clients trust the Certificate Authority (CA) certificate.


.Procedure

. If you want to provide only an IMAP or POP3 service to users, uncomment the `protocols` parameter in the `/etc/dovecot/dovecot.conf` file, and set it to the required protocols. For example, if you do not require POP3, set:
+
[literal,subs="+quotes",options="nowrap",role=white-space-pre]
....
**protocols = imap lmtp**
....
+
By default, the `imap`, `pop3`, and `lmtp` protocols are enabled.

. Open the ports in the local firewall. For example, to open the ports for the IMAPS, IMAP, POP3S, and POP3 protocols, enter:
+
[literal,subs="+quotes",options="nowrap",role=white-space-pre]
....
# **firewall-cmd --permanent --add-service=imaps --add-service=imap --add-service=pop3s --add-service=pop3**
# **firewall-cmd --reload**
....

. Enable and start the `dovecot` service:
+
[literal,subs="+quotes",options="nowrap",role=white-space-pre]
....
# **systemctl enable --now dovecot**
....


.Verification

. Use a mail client, such as Mozilla Thunderbird, to connect to Dovecot and read emails. The settings for the mail client depend on the protocol you want to use:
+
[cols="1,1,2,2",options="header"]
.Connection settings to the Dovecot server
|===
| Protocol | Port | Connection security | Authentication method
| IMAP | 143 | STARTTLS | PLAINfootnoteref:[fn-dovecot-con-settings-normal-pw_{context},The client transmits data encrypted through the TLS connection. Consequently, credentials are not disclosed.]
| IMAPS | 993 | SSL/TLS | PLAINfootnoteref:[fn-dovecot-con-settings-normal-pw_{context}]
| POP3 | 110 | STARTTLS | PLAINfootnoteref:[fn-dovecot-con-settings-normal-pw_{context}]
| POP3S | 995 | SSL/TLS | PLAINfootnoteref:[fn-dovecot-con-settings-normal-pw_{context}]
|===
+
Note that this table does not list settings for unencrypted connections because, by default, Dovecot does not accept plain text authentication on connections without TLS.

. Display configuration settings with non-default values:
+
[literal,subs="+quotes",options="nowrap",role=white-space-pre]
....
# **doveconf -n**
....


[role="_additional-resources"]
.Additional resources
* `firewall-cmd(1)` man page

