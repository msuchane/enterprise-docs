:_mod-docs-content-type: CONCEPT
[id="con_unit-file-structure_{context}"]
= Unit file structure

[role="_abstract"]
Unit files typically consist of three following sections:

The `[Unit]` section :: Contains generic options that are not dependent on the type of the unit. These options provide unit description, specify the unit's behavior, and set dependencies to other units. For a list of most frequently used [Unit] options, see xref:tabl-systemd-Unit_Sec_Options[Important [Unit\] section options].

The `[Unit type]` section :: Contains type-specific directives, these are grouped under a section named after the unit type. For example, service unit files contain the `[Service]` section.

The `[Install]` section :: Contains information about unit installation used by `systemctl enable` and `disable` commands. For a list of options for the `[Install]` section, see xref:tabl-systemd-Install_Sec_Options[Important [Install\] section options].


[role="_additional-resources"]
.Additional resources
* xref:ref_important-unit-section-options_assembly_working-with-systemd-unit-files[Important [Unit\] section options]
* xref:ref_important-service-section-options_assembly_working-with-systemd-unit-files[Important [Service\] section options]
* xref:ref_important-install-section-options_assembly_working-with-systemd-unit-files[Important [Install\] section options]
