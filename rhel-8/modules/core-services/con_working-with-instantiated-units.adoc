:_mod-docs-content-type: CONCEPT
[id="con_working-with-instantiated-units_{context}"]
= Working with instantiated units

[role="_abstract"]
You can manage multiple instances of a service by using a single template configuration. You can define a generic template for a unit and generate multiple instances of that unit with specific parameters at runtime. The template is indicated by the at sign (@). Instantiated units can be started from another unit file (using [option]`Requires` or [option]`Wants` options), or with the [command]`systemctl start` command. Instantiated service units are named the following way:

[subs="+quotes"]
----
_<template_name>_@_<instance_name>_.service
----

The _<template_name>_ stands for the name of the template configuration file. Replace _<instance_name>_ with the name for the unit instance. Several instances can point to the same template file with configuration options common for all instances of the unit. Template unit name has the form of:

[subs="+quotes"]
----
_<unit_name>_@.service
----

For example, the following [option]`Wants` setting in a unit file:

[subs="+quotes"]
----
Wants=getty@ttyA.service getty@ttyB.service
----

first makes systemd search for given service units. If no such units are found, the part between "@" and the type suffix is ignored and [application]*systemd* searches for the `getty@.service` file, reads the configuration from it, and starts the services.


For example, the `getty@.service` template contains the following directives:

[subs="+quotes"]
----
[Unit]
Description=Getty on %I
...
[Service]
ExecStart=-/sbin/agetty --noclear %I $TERM
...
----

When the `getty@ttyA.service` and `getty@ttyB.service` are instantiated from the above template, [option]`Description`pass:attributes[{blank}]= is resolved as *Getty on ttyA* and *Getty on ttyB*.

