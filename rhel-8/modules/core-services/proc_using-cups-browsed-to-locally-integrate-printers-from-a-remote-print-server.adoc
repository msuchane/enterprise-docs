:_mod-docs-content-type: PROCEDURE

[id="using-cups-browsed-to-locally-integrate-printers-from-a-remote-print-server_{context}"]
= Using cups-browsed to locally integrate printers from a remote print server
The `cups-browsed` service uses DNS service discovery (DNS-SD) and CUPS browsing to make all or a filtered subset of shared remote printers automatically available in a local CUPS service.

For example, administrators can use this feature on workstations to make only printers from a trusted print server available in a print dialog of applications. It is also possible to configure `cups-browsed` to filter the browsed printers by certain criteria to reduce the number of listed printers if a print server shares a large number of printers.

[NOTE]
====
If the print dialog in an application uses other mechanisms than, for example DNS-SD, to list remote printers, `cups-browsed` has no influence. The `cups-browsed` service also does not prevent users from manually accessing non-listed printers.
====


.Prerequisites
* xref:installing-and-configuring-cups_configuring-printing[The CUPS service is configured on the local host].
* A remote CUPS print server exists, and the following conditions apply to this server:
** The server listens on an interface that is accessible from the client.
** The `Allow from` parameter in the server's `<Location />` directive in the `/etc/cups/cups.conf` file allows access from the client's IP address.
** The server shares printers.
** Firewall rules allow access from the client to the CUPS port on the server.


.Procedure

. Edit the `/etc/cups/cups-browsed.conf` file, and make the following changes:

.. Add `BrowsePoll` parameters for each remote CUPS server you want to poll:
+
[literal,subs="+quotes"]
....
*BrowsePoll __remote_cups_server.example.com__*
*BrowsePoll __192.0.2.100:1631__*
....
+
Append `:__<port>__` to the hostname or IP address if the remote CUPS server listens on a port different from 631.

.. Optional: Configure a filter to limit which printers are shown in the local CUPS service. For example, to filter for queues whose name contain `sales_`, add:
+
[literal,subs="+quotes"]
....
*BrowseFilter name sales_*
....
+
You can filter by different field names, negate the filter, and match the exact values. For further details, see the parameter description and examples in the `cups-browsed.conf(5)` man page.

.. Optional: Change the polling interval and timeout to limit the number of browsing cycles:
+
[literal,subs="+quotes"]
....
*BrowseInterval _1200_*
*BrowseTimeout _6000_*
....
+
Increase both `BrowseInterval` and `BrowseTimeout` in the same ratio to avoid situations in which printers disappear from the browsing list. This mean, multiply the value of `BrowseInterval` by 5 or a higher integer, and use this result value for `BrowseTimeout`.
+
By default, `cups-browsed` polls remote servers every 60 seconds and the timeout is 300 seconds. However, on print servers with many queues, these default values can cost many resources.

. Enable and start the `cups-browsed` service:
+
[literal,subs="+quotes"]
....
# *systemctl enable --now cups-browsed*
....


.Verification

* List the available printers:
+
[literal,subs="+quotes"]
....
# *lpstat -v*
device for _Demo-printer_: implicitclass://_Demo-printer_/
...
....
+
If the output for a printer contains `implicitclass`, `cups-browsed` manages the printer in CUPS.


[role="_additional-resources"]
.Additional resources

* `cups-browsed.conf(5)` man page

