:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8/asseblies/assembly_editing-user-groups-using-the-command-line.adoc

[id="removing-a-user-from-a-supplementary-group_{context}"]
= Removing a user from a supplementary group

[role="_abstract"]
You can remove an existing user from a supplementary group to limit their permissions or access to files and devices.

.Prerequisites
* `root` access

.Procedure
* Remove a user from a supplementary group:
+
[literal,subs="+quotes,attributes"]
----
# *gpasswd -d _user-name_ _group-name_*
----
+
Replace _user-name_ with the name of the user, and replace _group-name_ with the name of the supplementary group.
+
.Removing user from a supplementary group
====
If the user sarah has a primary group `sarah2`, and belongs to the secondary groups `wheel` and `developers`, and you want to remove that user from the group `developers`, use:
[literal,subs="+quotes,attributes"]
----
# *gpasswd -d sarah developers*
----
====

.Verification steps
* Verify that you removed the user sarah from the secondary group developers:
+
[literal,subs="+quotes,attributes"]
----
$ *groups sarah*
----
+
The output displays:
+
----
sarah : sarah2 wheel
----
