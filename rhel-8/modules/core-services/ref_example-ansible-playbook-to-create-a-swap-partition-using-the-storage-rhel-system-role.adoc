:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="example-ansible-playbook-to-create-a-swap-partition-using-the-storage-rhel-system-role_{context}"]
= Creating a swap volume by using the `storage` {RHELSystemRoles}
This section provides an example Ansible playbook. This playbook applies the `storage` role to create a swap volume, if it does not exist, or to modify the swap volume, if it already exist, on a block device by using the default parameters.


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Create a disk device with swap
  hosts: managed-node-01.example.com
  roles:
    - rhel-system-roles.storage
  vars:
    storage_volumes:
      - name: swap_fs
        type: disk
        disks:
          - /dev/sdb
        size: 15 GiB
        fs_type: swap
....
+
The volume name (`_swap_fs_` in the example) is currently arbitrary. The `storage` role identifies the volume by the disk device listed under the `disks:` attribute.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file
* `/usr/share/doc/rhel-system-roles/storage/` directory

