:_mod-docs-content-type: PROCEDURE
[id="proc_changing-the-current-target_{context}"]
= Changing the current target

[role="_abstract"]
On a running system, you can change the target unit in the current boot without reboot. If you switch to a different target, `systemd` starts all services and their dependencies that this target requires, and stops all services that the new target does not enable. Isolating a different target affects only the current boot.

.Procedure

. Optional: Determine the current target:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl get-default*
graphical.target
----
+

. Optional: Display the list of targets you can select: 
+
[literal,subs="+quotes,attributes"]
----
# *systemctl list-units --type target*
----
+
[NOTE]
====
You can only isolate targets that have the `AllowIsolate=yes` option set in the unit files.
====
+

. Change to a different target unit in the current boot:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl isolate _<name>_.target*
----
+
Replace _<name>_ with the name of the target unit you want to use in the current boot.

+
[literal,subs="+quotes,attributes"]
----
Example: 
# *systemctl isolate multi-user.target*
----
+
This command starts the target unit named `*multi-user*` and all dependent units, and immediately stops all other unit.



[role="_additional-resources"]
.Additional resources
//* xref:con_target-unit-files.adoc[Target unit files]
* `systemctl(1)` man page
