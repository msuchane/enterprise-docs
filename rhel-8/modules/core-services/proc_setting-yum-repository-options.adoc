:_mod-docs-content-type: PROCEDURE

:_module-type: PROCEDURE

[id="setting-yum-repository-options_{context}"]

= Setting YUM repository options

[subs="+quotes,attributes"]
The [filename]`/etc/yum.conf` configuration file contains the `[_repository_]` sections, where _repository_ is a unique repository ID. The `[_repository_]` sections allows you to define individual [application]*{PackageManagerCommand}* repositories.

NOTE: Do not give custom repositories names used by the Red Hat repositories to avoid conflicts.

[subs="+quotes,attributes"]
For a complete list of available `[_repository_]` options, see the `[_repository_] OPTIONS` section of the *{PackageManagerCommand}.conf*(5) manual page.
