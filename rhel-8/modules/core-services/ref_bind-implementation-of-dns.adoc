:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_bind-implementation-of-dns.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.

[id="bind-implementation-of-dns_{context}"]
= BIND - Implementation of DNS

RHEL 8 includes BIND (Berkeley Internet Name Domain) in version 9.11. This version of the DNS server introduces multiple new features and feature changes compared to version 9.10.

New features:

* A new method of provisioning secondary servers called *Catalog Zones* has been added.
* Domain Name System Cookies are now sent by the `named` service and the `dig` utility.
* The *Response Rate Limiting* feature can now help with mitigation of DNS amplification attacks.
* Performance of response-policy zone (RPZ) has been improved.
* A new zone file format called `map` has been added. Zone data stored in this format can be mapped directly into memory, which enables zones to load significantly faster.
* A new tool called `delv` (domain entity lookup and validation) has been added, with dig-like semantics for looking up DNS data and performing internal DNS Security Extensions (DNSSEC) validation.
* A new [command]`mdig` command is now available. This command is a version of the [command]`dig` command that sends multiple pipelined queries and then waits for responses, instead of sending one query and waiting for the response before sending the next query.
* A new `prefetch` option, which improves the recursive resolver performance, has been added.
* A new `in-view` zone option, which allows zone data to be shared between views, has been added. When this option is used, multiple views can serve the same zones authoritatively without storing multiple copies in memory.
* A new `max-zone-ttl` option, which enforces maximum TTLs for zones, has been added. When a zone containing a higher TTL is loaded, the load fails. Dynamic DNS (DDNS) updates with higher TTLs are accepted but the TTL is truncated.
* New quotas have been added to limit queries that are sent by recursive resolvers to authoritative servers experiencing denial-of-service attacks.
* The `nslookup` utility now looks up both IPv6 and IPv4 addresses by default.
* The `named` service now checks whether other name server processes are running before starting up.
* When loading a signed zone, `named` now checks whether a Resource Record Signature's (RSIG) inception time is in the future, and if so, it regenerates the RRSIG immediately.
* Zone transfers now use smaller message sizes to improve message compression, which reduces network usage.

Feature changes:

* The version `3 XML` schema for the statistics channel, including new statistics and a flattened XML tree for faster parsing, is provided by the HTTP interface. The legacy version `2 XML` schema is no longer supported.
* The `named` service now listens on both IPv6 and IPv4 interfaces by default.
* The `named` service no longer supports GeoIP databases. Access control lists (ACLs) defined by presumed location of query sender are unavailable.
* Since RHEL 8.2, the `named` service supports GeoIP2, which is provided in the `libmaxminddb` data format.
