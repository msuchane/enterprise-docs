:_mod-docs-content-type: PROCEDURE

[id="proc_delivering-email-from-postfix-to-dovecot-on-the-same-host_{context}"]
= Delivering email from Postfix to Dovecot running on the same host

[role="_abstract"]
You can configure Postfix to deliver incoming mail to Dovecot on the same host using LMTP over a UNIX socket. This socket enables direct communication between Postfix and Dovecot on the local machine. 

.Prerequisites

* You have the root access.

* You have configured a Postfix server.

* You have configured a Dovecot server, see 
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/configuring-and-maintaining-a-dovecot-imap-and-pop3-server_deploying-different-types-of-servers#doc-wrapper[Configuring and maintaining a Dovecot IMAP and POP3 server].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/deploying_mail_servers/configuring-and-maintaining-a-dovecot-imap-and-pop3-server_deploying-mail-servers#doc-wrapper[Configuring and maintaining a Dovecot IMAP and POP3 server].
endif::[]

* You have configured the LMTP socket on your Dovecot server, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/configuring-and-maintaining-a-dovecot-imap-and-pop3-server_deploying-different-types-of-servers#configuring-an-lmtp-socket-and-lmtps-listener_configuring-and-maintaining-a-dovecot-imap-and-pop3-server[Configuring an LMTP socket and LMTPS listener].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/deploying_mail_servers/configuring-and-maintaining-a-dovecot-imap-and-pop3-server_deploying-mail-servers#configuring-an-lmtp-socket-and-lmtps-listener_configuring-and-maintaining-a-dovecot-imap-and-pop3-server[Configuring an LMTP socket and LMTPS listener].
endif::[]


.Procedure

. Configure Postfix to use the LMTP protocol and the UNIX domain socket for delivering mail to Dovecot in the `/etc/postfix/main.cf` file:
+
* If you want to use virtual mailboxes, add the following content:
+
[literal,subs="+quotes,attributes"]
----
virtual_transport = lmtp:unix:/var/run/dovecot/lmtp
----
+
* If you want to use non-virtual mailboxes, add the following content:
+
[literal,subs="+quotes,attributes"]
----
mailbox_transport = lmtp:unix:/var/run/dovecot/lmtp
----
+

. Reload `postfix` to apply the changes:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl reload postfix*
---- 


.Verification

* Send an test email to verify that the LMTP socket works correctly. Check the mail logs in `/var/log/maillog` for any errors.


