:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_setting-acls-on-a-share-that-uses-posix-acls.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_setting-standard-linux-acls-on-a-samba-share-that-uses-posix-acls.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_setting-standard-linux-acls-on-a-samba-share-that-uses-posix-acls_{context}"]
= Setting standard Linux ACLs on a Samba share that uses POSIX ACLs

The standard ACLs on Linux support setting permissions for one owner, one group, and for all other undefined users. You can use the `chown`, `chgrp`, and `chmod` utility to update the ACLs. If you require precise control, then you use the more complex POSIX ACLs, see

ifdef::differentserver-title,network-file-services[]
xref:proc_setting-extended-acls-on-a-samba-share-that-uses-posix-acls_assembly_setting-up-a-samba-file-share-that-uses-posix-acls[Setting extended ACLs on a Samba share that uses POSIX ACLs].
endif::[]
ifndef::differentserver-title,network-file-services[]
ifeval::[{ProductNumberLink} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/assembly_using-samba-as-a-server_deploying-different-types-of-servers#proc_setting-extended-acls-on-a-samba-share-that-uses-posix-acls_assembly_setting-up-a-samba-file-share-that-uses-posix-acls[Setting extended ACLs on a Samba share that uses POSIX ACLs].
endif::[]
ifeval::[{ProductNumberLink} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_using_network_file_services/assembly_using-samba-as-a-server_configuring-and-using-network-file-services#proc_setting-extended-acls-on-a-samba-share-that-uses-posix-acls_assembly_setting-up-a-samba-file-share-that-uses-posix-acls[Setting extended ACLs on a Samba share that uses POSIX ACLs].
endif::[]
endif::[]

The following procedure sets the owner of the [filename]`/srv/samba/example/` directory to the `root` user, grants read and write permissions to the `Domain Users` group, and denies access to all other users.


.Prerequisites

* The Samba share on which you want to set the ACLs exists.


.Procedure

[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *chown root:"Domain Users" /srv/samba/example/*
# *chmod 2770 /srv/samba/example/*
----

[NOTE]
====
Enabling the set-group-ID (SGID) bit on a directory automatically sets the default group for all new files and subdirectories to that of the directory group, instead of the usual behavior of setting it to the primary group of the user who created the new directory entry.
====


[role="_additional-resources"]
.Additional resources
* `chown(1)` and `chmod(1)` man pages
