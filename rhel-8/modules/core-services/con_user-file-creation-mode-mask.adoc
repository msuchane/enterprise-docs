:_mod-docs-content-type: CONCEPT
[id="user-file-creation-mode-mask_{context}"]

= User file-creation mode mask

[role="_abstract"]
The user file-creation mode mask (_umask_) is variable that controls how file permissions are set for newly created files and directories. The _umask_ automatically removes permissions from the base permission value to increase the overall security of a Linux system.
The _umask_ can be expressed in _symbolic_ or _octal_ values.

|===
|*Permission*|*Symbolic value*|*Octal value*
|Read, write, and execute|rwx|0
|Read and write|rw-|1
|Read and execute|r-x|2
|Read|r--|3
|Write and execute|-wx|4
|Write|-w-|5
|Execute|--x|6
|No permissions|---|7
|===

ifeval::[{ProductNumber} == 8]
The default _umask_ for a standard user is `0002`. The default _umask_ for a `root` user is `0022`.
endif::[]

ifeval::[{ProductNumber} == 9]
The default _umask_ for both a standard user and for a `root` user is `0022`.
endif::[]

The first digit of the _umask_ represents special permissions (sticky bit, ). The last three digits of the _umask_ represent the permissions that are removed from the user owner (*u*), group owner (*g*), and others (*o*) respectively.

.Applying the umask when creating a file
====
The following example illustrates how the _umask_ with an octal value of `0137` is applied to the file with the base permission of `777`, to create the file with the default permission of `640`.

image::Users_Groups-Umask_Example.png[]
====

////
The default _umask_ for a standard user is `0002` (`rwxrwxr-x`), which grants the owner and the group permissions to read, write, and execute. Others can only read and execute.

The default _umask_ for a `root` user is `0022` (`rwxr-xr-x`), which grants the owner the permission to read, write, and execute, while the group and others can only read and execute.
////
