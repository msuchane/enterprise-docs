
:_mod-docs-content-type: CONCEPT


[id="con_boolean-dependencies-syntax_{context}"]
= Boolean dependencies syntax

Boolean expressions are always enclosed with parenthesis.

They are build out of normal dependencies:

* Name only or name
* Comparison
* Version description
