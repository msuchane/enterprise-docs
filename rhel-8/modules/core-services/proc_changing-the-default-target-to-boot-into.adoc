:_mod-docs-content-type: PROCEDURE
[id="proc_changing-the-default-target-to-boot-into_{context}"]
= Changing the default target to boot into

[role="_abstract"]
When a system starts, `systemd` activates the `default.target` symbolic link, which points to the true target unit. You can find the currently selected default target unit in the `/etc/systemd/system/default.target` file. Each target represents a certain level of functionality and is used for grouping other units. Additionally, target units serve as synchronization points during boot. You can change the default target your system boots into. When you set a default target unit, the current target remains unchanged until the next reboot.

.Prerequisites

* Root access


.Procedure

. Determine the current default target unit `systemd` uses to start the system:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl get-default*
graphical.target
----
+

. List the currently loaded targets:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl list-units --type target*
----
+

. Configure the system to use a different target unit by default:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl set-default _<name>_.target*
----
+
Replace `_<name>_` with the name of the target unit you want to use by default.
+
[literal,subs="+quotes,attributes"]
----
Example: 
# *systemctl set-default multi-user.target*
Removed /etc/systemd/system/default.target
Created symlink /etc/systemd/system/default.target -> /usr/lib/systemd/system/multi-user.target  
----
+

. Verify the default target unit:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl get-default*
multi-user.target
----
+
. Apply the changes by rebooting:
+
[literal,subs="+quotes,attributes"]
----
# *reboot*
----

[role="_additional-resources"]
.Additional resources
* `systemctl(1)` man page
* `systemd.special(7)` man page
* `bootup(7)` man page
//* xref:target-unit-files[Target unit files]
