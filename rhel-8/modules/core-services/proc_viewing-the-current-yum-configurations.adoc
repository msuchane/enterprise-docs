:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

:_module-type: PROCEDURE

[id="viewing-the-current-yum-configurations_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Viewing the current YUM configurations

[role="_abstract"]
Use the following procedure to view the current `yum` configurations.

.Procedure

* To display the current values of global `{PackageManagerCommand}` options specified in the `[main]` section of the [filename]`/etc/yum.conf` file, use:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} config-manager --dump*
----
