:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_proxy-caching-servers.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
[id="proxy-caching-servers_{context}"]
= Proxy caching servers

[id="varnish_cache_{context}"]
== `Varnish Cache` new in RHEL

`Varnish Cache`, a high-performance HTTP reverse proxy, is provided for the first time in RHEL. It was previously available only as a Software Collection. `Varnish Cache` stores files or fragments of files in memory that are used to reduce the response time and network bandwidth consumption on future equivalent requests. RHEL 8.0 is distributed with `Varnish Cache 6.0`.
//https://bugzilla.redhat.com/show_bug.cgi?id=1633338


[id="squid_{context}"]
== Notable changes in `Squid`

RHEL 8.0 is distributed with `Squid 4.4`, a high-performance proxy caching server for web clients, supporting FTP, Gopher, and HTTP data objects. This release provides numerous new features, enhancements, and bug fixes over the version 3.5 available in RHEL 7. 

Notable changes include:

* Configurable helper queue size
* Changes to helper concurrency channels
* Changes to the helper binary
* Secure Internet Content Adaptation Protocol (ICAP)
* Improved support for Symmetric Multi Processing (SMP)
* Improved process management
* Removed support for SSL
* Removed Edge Side Includes (ESI) custom parser
* Multiple configuration changes
//https://bugzilla.redhat.com/show_bug.cgi?id=1656871
