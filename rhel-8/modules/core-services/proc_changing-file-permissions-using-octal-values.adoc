:_mod-docs-content-type: PROCEDURE

[id="changing-file-permissions-using-octal-values_{context}"]

= Changing file permissions using octal values

[role="_abstract"]
You can use the `chmod` utility with octal values (numbers) to change file permissions for a file or directory.

.Procedure
* To change the file permissions for an existing file or directory, use:
+
[literal,subs="+quotes,attributes"]
----
$ *chmod _octal_value_ _file-name_*
----
+
Replace _file-name_ with the name of the file or directory. Replace _octal_value_ with an octal value. See xref:base-permissions_assembly_managing-file-permissions[Base file permissions] for more details.
////
+
[NOTE]
====
If you want to change the permissions for a directory and all its sub-directories recursively, add the `-R` option:

[literal,subs="+quotes,attributes"]
----
$ *chmod -R _octal_value_ _file-name_*
----
====
////
