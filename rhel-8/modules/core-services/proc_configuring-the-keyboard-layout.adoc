:_mod-docs-content-type: PROCEDURE
[id="proc_configuring-the-keyboard-layout_{context}"]
= Configuring the keyboard layout

[role="_abstract"]
The keyboard layout settings control the layout used on the text console and graphical user interfaces.

.Procedure

* To list available keymaps:
+
[literal,subs="+quotes,attributes"]
----
$ *localectl list-keymaps*
ANSI-dvorak
al
al-plisi
amiga-de
amiga-us
...
----

* To display the current status of keymaps settings:
+
[literal,subs="+quotes,attributes"]
----
$ *localectl status*
...
VC Keymap: us
...
----

* To set or change the default system keymap. For example:
+
[literal,subs="+quotes,attributes"]
----
# *localectl set-keymap _us_*
----


[role="_additional-resources"]
.Additional resources
* `man localectl(1)`, `man locale(7)`, and `man locale.conf(5)`
