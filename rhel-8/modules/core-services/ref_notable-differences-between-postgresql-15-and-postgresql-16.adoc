:_mod-docs-content-type: REFERENCE

[id="notable-differences-between-postgresql-15-and-postgresql-16_{context}"]
= Notable differences between PostgreSQL 15 and PostgreSQL 16

[role="_abstract"]
*PostgreSQL 16* introduced the following notable changes. 

.The `postmasters` binary is no longer available

*PostgreSQL* is no longer distributed with the `postmaster` binary. Users who start the `postgresql` server by using the provided `systemd` unit file (the `systemctl start postgres` command) are not affected by this change. If you previously started the `postgresql` server directly through the `postmaster` binary, you must now use the `postgres` binary instead.


.Documentation is no longer packaged

*PostgreSQL* no longer provides documentation in PDF format within the package. Use the link:https://www.postgresql.org/files/documentation/pdf/16/postgresql-16-US.pdf[online documentation] instead.

