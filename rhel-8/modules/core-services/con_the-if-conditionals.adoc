:_mod-docs-content-type: CONCEPT


[id="con_the-if-conditionals_{context}"]
= The %if conditionals

[role="_abstract"]
The following examples shows the usage of `%if` RPM conditionals.

.Using the %if conditional to handle compatibility between {RHEL} 8 and other operating systems
====
....
%if 0%{?rhel} == 8
sed -i '/AS_FUNCTION_DESCRIBE/ s/^/#/' configure.in
sed -i '/AS_FUNCTION_DESCRIBE/ s/^/#/' acinclude.m4
%endif
....
====

This conditional handles compatibility between RHEL 8 and other operating systems in terms of support of the AS_FUNCTION_DESCRIBE macro. If the package is built for RHEL, the `%rhel` macro is defined, and it is expanded to RHEL version. If its value is 8, meaning the package is build for RHEL 8, then the references to AS_FUNCTION_DESCRIBE, which is not supported by RHEL 8, are deleted from autoconfig scripts.

ifdef::community[]
%if 0%{?el8}
%global ruby_sitearch %(ruby -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')
%endif

This conditional handles compatibility between Fedora version 17 and newer and RHEL 8 in terms of support of the `%ruby_sitearch` macro. Fedora version 17 and newer defines `%ruby_sitearch` by default, but RHEL6 does not support this macro. The conditional checks whether the operating system is RHEL6. If it is, `%ruby_sitearch` is defined explicitly. Note that `0%{?el6}` has the same meaning as `0%{?rhel} == 6` from the previous example, and it tests whether a package is built on RHEL6.
endif::community[]

ifdef::community[]
  %if 0%{?fedora} >= 19
  %global with_rubypick 1
  %endif

This conditional handles support for the rubypick tool. If the operating system is Fedora version 19 or newer, rubypick is supported.
endif::community[]

.Using the %if conditional to handle definition of macros
====
[literal,subs="+quotes,verbatim,normal,normal"]
....
%define ruby_archive %{name}-%{ruby_version}
%if 0%{?milestone:1}%{?revision:1} != 0
%define ruby_archive %{ruby_archive}-%{?milestone}%{?!milestone:%{?revision:r%{revision}}}
%endif
....
====

This conditional handles definition of macros. If the `%milestone` or the `%revision` macros are set, the `%ruby_archive` macro, which defines the name of the upstream tarball, is redefined.
