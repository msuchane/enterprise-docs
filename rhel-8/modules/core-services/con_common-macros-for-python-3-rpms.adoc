:_mod-docs-content-type: CONCEPT
[id="con_common-macros-for-python-3-rpms_{context}"]
= Common macros for Python 3 RPMs

[role="_abstract"]
In a `spec` file, always use the macros that are described in the following Macros for Python 3 RPMs table rather than hardcoding their values.

In macro names, always use `python3` or `python2` instead of unversioned `python`. Configure the particular Python 3 version in the `BuildRequires` section of the SPEC file to `python36-rpm-macros`, `python38-rpm-macros`, `python39-rpm-macros`, `python3.11-rpm-macros`, or `python3.12-rpm-macros`.

[table]
.Macros for Python 3 RPMs

|===
|Macro |Normal Definition |Description

|%{__python3}
|/usr/bin/python3
|Python 3 interpreter

|%{python3_version}
|3.6
|The full version of the Python 3 interpreter.

|%{python3_sitelib}
|/usr/lib/python3.6/site-packages
|Where pure-Python modules are installed.

|%{python3_sitearch}
|/usr/lib64/python3.6/site-packages
|Where modules containing architecture-specific extensions are installed.

|%py3_build
|
|Runs the [command]`setup.py build` command with arguments suitable for a system package.

|%py3_install
|
|Runs the [command]`setup.py install` command with arguments suitable for a system package.
|===

// All Python specific RPM macros can be found on this page link:https://fedoraproject.org/wiki/Packaging:Python#Macros[Python packaging macros].
