:_mod-docs-content-type: REFERENCE
[id="ref_postfix_{context}"]
= Postfix

[role="_abstract"]
By default in RHEL 8, `Postfix` uses MD5 fingerprints with the TLS for backward compatibility. But in FIPS mode, the MD5 hashing function is not available, which may cause TLS to incorrectly function in the default `Postfix` configuration. As a workaround, the hashing function needs to be changed to `SHA-256` in the postfix configuration file.

For more details, see the related link: https://access.redhat.com/articles/5824391
