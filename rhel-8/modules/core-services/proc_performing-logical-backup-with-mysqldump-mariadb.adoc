:_mod-docs-content-type: PROCEDURE
ifdef::context[:parent-context-of-performing-logical-backup-with-mysqldump: {context}]

[id="performing-logical-backup-with-mysqldump_{context}"]
= Performing logical backup with mysqldump

:context: performing-logical-backup-with-mysqldump

The [application]*mysqldump* client is a backup utility, which can be used to dump a database or a collection of databases for the purpose of a backup or transfer to another database server.
The output of [application]*mysqldump* typically consists of SQL statements to re-create the server table structure, populate it with data, or both. [application]*mysqldump* can also generate files in other formats, including XML and delimited text formats, such as CSV.

To perform the [application]*mysqldump* backup, you can use one of the following options:

* Back up one or more selected databases
* Back up all databases
* Back up a subset of tables from one database


.Procedure

* To dump a single database, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **mysqldump [_options_] --databases _db_name_ &gt; _backup-file.sql_**
....

* To dump multiple databases at once, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **mysqldump [_options_] --databases _db_name1_ [_db_name2_ ...] &gt; _backup-file.sql_**
....

* To dump all databases, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **mysqldump [_options_] --all-databases &gt; _backup-file.sql_**
....

* To load one or more dumped full databases back into a server, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **mysql &lt; _backup-file.sql_**
....

* To load a database to a remote *MariaDB* server, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **mysql --host=_remote_host_ < _backup-file.sql_**
....

* To dump a subset of tables from one database, add a list of the chosen tables at the end of the [command]`mysqldump` command:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **mysqldump [_options_] _db_name_ [_tbl_name_ ...​] &gt; _backup-file.sql_**
....

* To load a subset of tables dumped from one database, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **mysql _db_name_ < _backup-file.sql_**
....
+
NOTE: The _db_name_ database must exist at this point.

* To see a list of the options that [application]*mysqldump* supports, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
$ **mysqldump --help**
....


[role="_additional-resources"]
.Additional resources
* For more information about logical backup with [application]*mysqldump*, see the link:https://mariadb.com/kb/en/library/mysqldump/[MariaDB Documentation].

:context: {parent-context-of-performing-logical-backup-with-mysqldump}


ifdef::parent-context-of-performing-logical-backup-with-mysqldump[:context: {parent-context-of-performing-logical-backup-with-mysqldump}]
ifndef::parent-context-of-performing-logical-backup-with-mysqldump[:!context:]
