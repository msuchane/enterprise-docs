
:_mod-docs-content-type: PROCEDURE

[id="proc_using-gem2rpm-to-covert-rubygems-packages-to-rpm-spec-files_{context}"]
= Using gem2rpm to covert RubyGems packages to RPM spec files

[role="_abstract"]
The following procedure describes how to use the `gem2rpm` utility to covert RubyGems packages to RPM `spec` files.

.Procedure

// Based on a review - include only one option to make the docs more concise.

// . Download a gem in its latest version:
// +
// [literal,subs="+quotes,verbatim,normal,normal"]
// ....
// $ gem fetch <gem_name>
// ....
//
// . Run gem2rpm:
// +
// [literal,subs="+quotes,verbatim,normal,normal"]
// ....
// $ gem2rpm <gem_name>-<latest_version>.gem
// ....
// +
// [NOTE]
// ====
// Alternatively, you can use the [option]`--fetch` flag to fetch the latest gem before generating the SPEC file:
// [literal,subs="+quotes,verbatim,normal,normal"]
// ....
// $ gem2rpm --fetch <gem_name>
// ....
// This has the same effect as running `gem fetch <gem_name>` plus `gem2rpm <gem_name>.-<latest_version>.gem`.
// ====

* Download a gem in its latest version, and generate the RPM `spec` file for this gem:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
$ *gem2rpm --fetch <gem_name> > <gem_name>.spec*
....

The described procedure creates an RPM `spec` file based on the information provided in the gem's metadata. However, the gem misses some important information that is usually provided in RPMs, such as the license and the changelog. The generated `spec` file thus needs to be edited.
