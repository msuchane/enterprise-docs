:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_using-samba-as-a-server.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/con_the-samba-security-services.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.
[id="con_the-samba-security-services_{context}"]
= The Samba security services

The `security` parameter in the `[global]` section in the [filename]`/etc/samba/smb.conf` file manages how Samba authenticates users that are connecting to the service. Depending on the mode you install Samba in, the parameter must be set to different values:

On an AD domain member, set `security = ads`::
In this mode, Samba uses Kerberos to authenticate AD users.
+
ifeval::[{ProductNumber} == 8]
ifdef::differentserver-title[]
For details about setting up Samba as a domain member, see xref:assembly_setting-up-samba-as-an-ad-domain-member-server_assembly_using-samba-as-a-server[Setting up Samba as an AD domain member server].
endif::[]
ifndef::differentserver-title[]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/deploying_different_types_of_servers/index#assembly_setting-up-samba-as-an-ad-domain-member-server_assembly_using-samba-as-a-server[Setting up Samba as an AD domain member server].
endif::[]
endif::[]

ifeval::[{ProductNumber} == 9]
ifdef::network-file-services[]
For details about setting up Samba as a domain member, see xref:assembly_setting-up-samba-as-an-ad-domain-member-server_assembly_using-samba-as-a-server[Setting up Samba as an AD domain member server].
endif::[]
ifndef::network-file-services[]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring-and-using-network-file-services/assembly_setting-up-samba-as-an-ad-domain-member-server_assembly_using-samba-as-a-server[Setting up Samba as an AD domain member server].
endif::[]
endif::[]


On a standalone server, set `security = user`::
In this mode, Samba uses a local database to authenticate connecting users.
+
ifeval::[{ProductNumber} == 8]
ifdef::differentserver-title[]
For details about setting up Samba as a standalone server, see xref:assembly_setting-up-samba-as-a-standalone-server_assembly_using-samba-as-a-server[Setting up Samba as a standalone server].
endif::[]
ifndef::differentserver-title[]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/deploying_different_types_of_servers/index#assembly_setting-up-samba-as-a-standalone-server_assembly_using-samba-as-a-server[Setting up Samba as a standalone server].
endif::[]
endif::[]

ifeval::[{ProductNumber} == 9]
ifdef::network-file-services[]
For details about setting up Samba as a standalone server, see xref:assembly_setting-up-samba-as-a-standalone-server_assembly_using-samba-as-a-server[Setting up Samba as a standalone server].
endif::[]
ifndef::network-file-services[]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring-and-using-network-file-services/assembly_setting-up-samba-as-a-standalone-server_assembly_using-samba-as-a-server[Setting up Samba as a standalone server].
endif::[]
endif::[]

On an NT4 PDC or BDC, set `security = user`::
In this mode, Samba authenticates users to a local or LDAP database.

On an NT4 domain member, set `security = domain`::
In this mode, Samba authenticates connecting users to an NT4 PDC or BDC. You cannot use this mode on AD domain members.
+
ifeval::[{ProductNumber} == 8]
ifdef::differentserver-title[]
For details about setting up Samba as a domain member, see xref:assembly_setting-up-samba-as-an-ad-domain-member-server_assembly_using-samba-as-a-server[Setting up Samba as an AD domain member server].
endif::[]
ifndef::differentserver-title[]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/deploying_different_types_of_servers/index#assembly_setting-up-samba-as-an-ad-domain-member-server_assembly_using-samba-as-a-server[Setting up Samba as an AD domain member server].
endif::[]
endif::[]

ifeval::[{ProductNumber} == 9]
ifdef::network-file-services[]
For details about setting up Samba as a domain member, see xref:assembly_setting-up-samba-as-an-ad-domain-member-server_assembly_using-samba-as-a-server[Setting up Samba as an AD domain member server].
endif::[]
ifndef::network-file-services[]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring-and-using-network-file-services/assembly_setting-up-samba-as-an-ad-domain-member-server_assembly_using-samba-as-a-server[Setting up Samba as an AD domain member server].
endif::[]
endif::[]

[role="_additional-resources"]
.Additional resources
* `security` parameter in the `smb.conf(5)` man page
