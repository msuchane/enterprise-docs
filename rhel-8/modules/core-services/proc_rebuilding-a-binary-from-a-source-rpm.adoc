
:_mod-docs-content-type: PROCEDURE

[id="rebuilding-a-binary-from-a-source-rpm_{context}"]
= Rebuilding a binary RPM from a source RPM

[role="_abstract"]
To rebuild a binary RPM from a source RPM (SRPM), use the [command]`rpmbuild` command with the [option]`--rebuild` option. 

The output generated when creating the binary RPM is verbose, which is helpful for debugging. The output varies for different examples and corresponds to their `spec` files.

The resulting binary RPMs are located in the [filename]`~/rpmbuild/RPMS/YOURARCH` directory, where `YOURARCH` is your architecture,
or in the [filename]`~/rpmbuild/RPMS/noarch/` directory, if the package is not architecture-specific.

.Prerequisites

* You have installed the `rpmbuild` utility on your system:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *{PackageManagerCommand} install rpm-build*
....

.Procedure

. Navigate to the `~/rpmbuild/SRPMS/` directive, which contains the source RPM:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *cd ~/rpmbuild/SRPMS/*
....

. Rebuild the binary RPM from the source RPM:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *rpmbuild --rebuild _<srpm>_*
....
+
Replace _srpm_ with the name of the source RPM file.
+
For example, to rebuild `bello`, `pello`, and `cello` from their SRPMs, enter:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ **rpmbuild --rebuild bello-0.1-1.el8.src.rpm**
[output truncated]

$ **rpmbuild --rebuild pello-0.1.2-1.el8.src.rpm**
[output truncated]

$ **rpmbuild --rebuild cello-1.0-1.el8.src.rpm**
[output truncated]
....

[NOTE]
====

Invoking [command]`rpmbuild --rebuild` involves the following processes:

* Installing the contents of the SRPM (the `spec` file and the source code) into the [filename]`~/rpmbuild/` directory.
* Building an RPM by using the installed contents.
* Removing the `spec` file and the source code.

You can retain the `spec` file and the source code after building either of the following ways:

* When building the RPM, use the [command]`rpmbuild` command with the [option]`--recompile` option instead of the [option]`--rebuild` option.
* Install SRPMs for `bello`, `pello`, and `cello`:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ **rpm -Uvh ~/rpmbuild/SRPMS/bello-0.1-1.el8.src.rpm**
Updating / installing...
   1:bello-0.1-1.el8               [100%]

$ **rpm -Uvh ~/rpmbuild/SRPMS/pello-0.1.2-1.el8.src.rpm**
Updating / installing...
...1:pello-0.1.2-1.el8              [100%]

$ **rpm -Uvh ~/rpmbuild/SRPMS/cello-1.0-1.el8.src.rpm**
Updating / installing...
...1:cello-1.0-1.el8            [100%]
....
// For this tutorial, execute the ``rpm -Uvh`` commands above to continue interacting with the `spec` files and sources.

====