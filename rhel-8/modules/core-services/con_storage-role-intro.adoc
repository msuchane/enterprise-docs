:_mod-docs-content-type: CONCEPT

[id="storage-role-intro_{context}"]
= Introduction to the `storage` {RHELSystemRoles}

[role="_abstract"]
The `storage` role can manage:

* File systems on disks which have not been partitioned
* Complete LVM volume groups including their logical volumes and file systems
* MD RAID volumes and their file systems

With the `storage` role, you can perform the following tasks:

* Create a file system
* Remove a file system
* Mount a file system
* Unmount a file system
* Create LVM volume groups
* Remove LVM volume groups
* Create logical volumes
* Remove logical volumes
* Create RAID volumes
* Remove RAID volumes
* Create LVM volume groups with RAID
* Remove LVM volume groups with RAID
* Create encrypted LVM volume groups
* Create LVM logical volumes with RAID

[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file
* `/usr/share/doc/rhel-system-roles/storage/` directory

