:_newdoc-version: 2.17.0
:_template-generated: 2024-04-11

:_mod-docs-content-type: REFERENCE

[id="notable-differences-between-mariadb-10-3-and-mariadb-10-5_{context}"]
= Notable differences between MariaDB 10.3 and MariaDB 10.5

Significant changes between *MariaDB 10.3* and *MariaDB 10.5* include:

* *MariaDB* now uses the `unix_socket` authentication plugin by default. The plugin enables users to use operating system credentials when connecting to *MariaDB* through the local UNIX socket file.
	* `MariaDB` adds `pass:[mariadb-*]` named binaries and `pass:[mysql*]` symbolic links pointing to the `pass:[mariadb-*]` binaries. For example, the `mysqladmin`, `mysqlaccess`, and `mysqlshow` symlinks point to the `mariadb-admin`, `mariadb-access`, and `mariadb-show` binaries, respectively.
* The `SUPER` privilege has been split into several privileges to better align with each user role. As a result, certain statements have changed required privileges.
* In parallel replication, the `slave_parallel_mode` now defaults to `optimistic`.
* In the *InnoDB* storage engine, defaults of the following variables have been changed: `innodb_adaptive_hash_index` to `OFF` and `innodb_checksum_algorithm` to `full_crc32`.
* *MariaDB* now uses the `libedit` implementation of the underlying software managing the *MariaDB* command history (the `.mysql_history` file) instead of the previously used `readline` library. This change impacts users working directly with the `.mysql_history` file. Note that `.mysql_history` is a file managed by the *MariaDB* or *MySQL* applications, and users should not work with the file directly. The human-readable appearance is coincidental.
+
[NOTE]
====
To increase security, you can consider not maintaining a history file. To disable the command history recording:

. Remove the `.mysql_history` file if it exists.
. Use either of the following approaches:
+
** Set the `MYSQL_HISTFILE` variable to `/dev/null` and include this setting in any of your shell's startup files.
** Change the `.mysql_history` file to a symbolic link to `/dev/null`:
+
[literal,subs="+quotes,attributes"]
----
$ *ln -s /dev/null $HOME/.mysql_history*
----
+
====
//https://bugzilla.redhat.com/show_bug.cgi?id=1970829
//TODO: add to MySQL docs - from mschorm: MySQL has IMO a much cleaner approach.
//Some log lines can be ignored. They just set to ignore any for this case.
//https://dev.mysql.com/doc/refman/5.7/en/mysql-tips.html#mysql-history



*MariaDB Galera Cluster* has been upgraded to version 4 with the following notable changes:

* *Galera* adds a new streaming replication feature, which supports replicating transactions of unlimited size. During an execution of streaming replication, a cluster replicates a transaction in small fragments.
* *Galera* now fully supports Global Transaction ID (GTID).
* The default value for the `wsrep_on` option in the `/etc/my.cnf.d/galera.cnf` file has changed from `1` to `0` to prevent end users from starting `wsrep` replication without configuring required additional options.

Changes to the PAM plugin in *MariaDB 10.5* include:

* *MariaDB 10.5* adds a new version of the Pluggable Authentication Modules (PAM) plugin. The PAM plugin version 2.0 performs PAM authentication using a separate `setuid root` helper binary, which enables *MariaDB* to use additional PAM modules.
* The helper binary can be executed only by users in the `mysql` group. By default, the group contains only the `mysql` user. Red Hat recommends that administrators do not add more users to the `mysql` group to prevent password-guessing attacks without throttling or logging through this helper utility.
* In *MariaDB 10.5*, the Pluggable Authentication Modules (PAM) plugin and its related files have been moved to a new package, `mariadb-pam`. As a result, no new `setuid root` binary is introduced on systems that do not use PAM authentication for `MariaDB`.
* The `mariadb-pam` package contains both PAM plugin versions: version 2.0 is the default, and version 1.0 is available as the `auth_pam_v1` shared object library.
* The `mariadb-pam` package is not installed by default with the *MariaDB* server. To make the PAM authentication plugin available in *MariaDB 10.5*, install the `mariadb-pam` package manually.

