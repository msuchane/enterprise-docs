:_mod-docs-content-type: PROCEDURE
[id="checking-the-availability-of-language-support_{context}"]
= Checking the availability of language support

To check if language support is available for any language, use the following procedure.

.Procedure

* Execute the following command:

[literal,subs="+quotes,verbatim,normal"]
....
# **{PackageManagerCommand} list available langpacks***
....
