:_mod-docs-content-type: PROCEDURE
[id="configuring-postgresql_{context}"]
= Configuring PostgreSQL

In a *PostgreSQL* database, all data and configuration files are stored in a single directory called a database cluster. {RH} recommends storing all data, including configuration files, in the default [filename]`/var/lib/pgsql/data/` directory.

*PostgreSQL* configuration consists of the following files:

* [filename]`postgresql.conf` - is used for setting the database cluster parameters.
* [filename]`postgresql.auto.conf` - holds basic *PostgreSQL* settings similarly to [filename]`postgresql.conf`. However, this file is under the server control. It is edited by the `ALTER SYSTEM` queries, and cannot be edited manually.
* [filename]`pg_ident.conf` - is used for mapping user identities from external authentication mechanisms into the *PostgreSQL*  user identities.
* [filename]`pg_hba.conf` - is used for configuring client authentication for *PostgreSQL* databases.


To change the *PostgreSQL* configuration, use the following procedure.

.Procedure

. Edit the respective configuration file, for example, [filename]`/var/lib/pgsql/data/postgresql.conf`.
. Restart the `postgresql` service so that the changes become effective:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl restart postgresql.service*
----


.Configuring PostgreSQL database cluster parameters
====
This example shows basic settings of the database cluster parameters in the [filename]`/var/lib/pgsql/data/postgresql.conf` file.
[literal,subs="+quotes,attributes"]
----
# This is a comment
log_connections = yes
log_destination = 'syslog'
search_path = '"$user", public'
shared_buffers = 128MB
password_encryption = scram-sha-256
----
====



.Setting client authentication in PostgreSQL
====
This example demonstrates how to set client authentication in the [filename]`/var/lib/pgsql/data/pg_hba.conf` file.
[literal,subs="+quotes,attributes"]
----
# TYPE    DATABASE       USER        ADDRESS              METHOD
local     all            all                              trust
host      postgres       all         192.168.93.0/24      ident
host      all            all         .example.com         scram-sha-256
----
====
