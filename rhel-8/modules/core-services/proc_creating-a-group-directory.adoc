:_mod-docs-content-type: PROCEDURE
[id="creating-a-group-directory_{context}"]
= Creating a group directory

[role="_abstract"]
Under the UPG system configuration, you can apply the _set-group identification permission_ (*setgid* bit) to a directory. The `setgid` bit makes managing group projects that share a directory simpler. When you apply the `setgid` bit to a directory, files created within that directory are automatically assigned to a group that owns the directory. Any user that has the permission to write and execute within this group can now create, modify, and delete files in the directory.

The following section describes how to create group directories.

.Prerequisites

* `Root` access

.Procedure
. Create a directory:
+
[literal,subs="+quotes,attributes"]
+
----
# *mkdir _directory-name_*
----
Replace _directory-name_ with the name of the directory.

. Create a group:
+
[literal,subs="+quotes,attributes"]
----
# *groupadd _group-name_*
----
Replace _group-name_ with the name of the group.

. Add users to the group:
+
[literal,subs="+quotes,attributes"]
----
# *usermod --append -G _group-name username_*
----
Replace _group-name_ with the name of the group, and replace _username_ with the name of the user.

. Associate the user and group ownership of the directory with the _group-name_ group:
+
[literal,subs="+quotes,attributes"]
----
# *chgrp __group-name directory-name__*
----
+
Replace _group-name_ with the name of the group, and replace _directory-name_ with the name of the directory.

. Set the write permissions to allow the users to create and modify files and directories and set the `setgid` bit to make this permission be applied within the _directory-name_ directory:
+
[literal,subs="+quotes,attributes"]
----
# *chmod g+rwxs _directory-name_*
----
Replace _directory-name_ with the name of the directory.
+
Now all members of the `_group-name_` group can create and edit files in the [filename]`_directory-name_` directory. Newly created files retain the group ownership of `_group-name_` group.

.Verification steps

* To verify the correctness of set permissions, use:
+
[literal,subs="+quotes,attributes"]
----
# *ls -ld _directory-name_*
----
Replace _directory-name_ with the name of the directory.
+
The output returns:
+
[subs=+quotes]
----
*drwx__rws__r-x.* 2 root _group-name_ 6 Nov 25 08:45 _directory-name_
----
