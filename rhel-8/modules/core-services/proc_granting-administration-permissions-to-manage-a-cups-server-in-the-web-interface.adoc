:_mod-docs-content-type: PROCEDURE

[id="granting-administration-permissions-to-manage-a-cups-server-in-the-web-interface_{context}"]
= Granting administration permissions to manage a CUPS server in the web interface

By default, members of the `sys`, `root`, and `wheel` groups can perform administration tasks in the web interface. However, certain other services use these groups as well. For example, members of the `wheel` groups can, by default, execute commands with `root` permissions by using `sudo`. To avoid that CUPS administrators gain unexpected permissions in other services, use a dedicated group for CUPS administrators.



.Prerequisites

* xref:installing-and-configuring-cups_configuring-printing[CUPS is configured].
* The IP address of the client you want to use has permissions to access the administration area in the web interface.


.Procedure

. Create a group for CUPS administrators:
+
[literal,subs="+quotes"]
....
# *groupadd _cups-admins_*
....

. Add the users who should manage the service in the web interface to the `cups-admins` group:
+
[literal,subs="+quotes"]
....
# *usermod -a -G _cups-admins_ _<username>_*
....

. Update the value of the `SystemGroup` parameter in the `/etc/cups/cups-files.conf` file, and append the `cups-admin` group:
+
[literal,subs="+quotes"]
....
SystemGroup sys root wheel *cups-admins*
....
+
If only the `cups-admin` group should have administrative access, remove the other group names from the parameter.

. Restart CUPS:
+
[literal,subs="+quotes"]
....
# *systemctl restart cups*
....


.Verification

. Use a browser, and access `https://_<hostname_or_ip_address>_:631/admin/`.
+
[NOTE]
====
You can access the administration area in the web UI only if you use the HTTPS protocol.
====

. Start performing an administrative task. For example, click `Add printer`.

. The web interface prompts for a username and password. To proceed, authenticate by using credentials of a user who is a member of the `cups-admins` group.
+
If authentication succeeds, this user can perform administrative tasks.

