:_mod-docs-content-type: PROCEDURE

[id="proc_connecting-the-replica-server-to-the-source-server_{context}"]
= Connecting the replica server to the source server

[role="_abstract"]
On the *MySQL* replica server, you must configure credentials and the address of the source server. Use the following procedure to implement the replica server.

.Prerequisites

* The source server is installed and configured as described in xref:proc_configuring-a-mysql-source-server_{context}[Configuring a MySQL source server].
* The replica server is installed and configured as described in xref:proc_configuring-a-mysql-replica-server_{context}[Configuring a MySQL replica server].
* You have created a replication user. See xref:proc_creating-a-replication-user-on-the-mysql-source-server_{context}[Creating a replication user on the MySQL source server].
// Not needed with GTIDs:* You have configured the starting point for replication and saved the status of the source server. See xref:proc_configuring-a-starting-point-for-replication-on-the-mysql-source-server_{context}[Configuring a starting point for replication on the source server].

.Procedure

. Set the replica server to read-only state:
+
[literal,subs="+quotes"]
----
*mysql> SET @@GLOBAL.read_only = ON;*
----
+
. Configure the replication source:
+
[literal,subs="+quotes"]
----
*mysql> CHANGE REPLICATION SOURCE TO*
    *-> SOURCE_HOST='_source_ip_address_',*
    *-> SOURCE_USER='_replication_user_',*
    *-> SOURCE_PASSWORD='_password_',*
    *-> SOURCE_AUTO_POSITION=1;*
----
+
. Start the replica thread in the *MySQL* replica server:
+
[literal,subs="+quotes"]
----
*mysql> START REPLICA;*
----
+
. Unset the read-only state on both the source and replica servers:
+
[literal,subs="+quotes"]
----
*mysql> SET @@GLOBAL.read_only = OFF;*
----
+
. _Optional:_ Inspect the status of the replica server for debugging purposes:
+
[literal,subs="+quotes"]
----
*mysql> SHOW REPLICA STATUS\G;*
----
+
[NOTE]
====
If the replica server fails to start or connect, you can skip a certain number of events following the binary log file position displayed in the output of the `SHOW MASTER STATUS` command. For example, skip the first event from the defined position:
[literal,literal,subs="+quotes,verbatim,normal,normal,macros"]
----
*mysql> SET GLOBAL SQL_SLAVE_SKIP_COUNTER=1;*
----
and try to start the replica server again.
====
+
. _Optional:_ Stop the replica thread in the replica server:
+
[literal,subs="+quotes"]
----
*mysql> STOP REPLICA;*
----
