
:_mod-docs-content-type: PROCEDURE

[id="proc_listing-available-gem2rpm-templates_{context}"]
= Listing available gem2rpm templates

[role="_abstract"]
Use the following procedure describes to list all available `gem2rpm` templates.

.Procedure
* To see all available templates, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
$ *gem2rpm --templates*
....
