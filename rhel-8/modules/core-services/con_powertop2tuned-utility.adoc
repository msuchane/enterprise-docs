:_mod-docs-content-type: CONCEPT
[id="powertop2tuned-utility_{context}"]
= The powertop2tuned utility

The `powertop2tuned` utility allows you to create custom [application]*TuneD* profiles from [application]*PowerTOP* suggestions. 

By default, `powertop2tuned` creates profiles in the [filename]`/etc/tuned/` directory, and bases the custom profile on the currently selected [application]*TuneD* profile. For safety reasons, all [application]*PowerTOP* tunings are initially disabled in the new profile. 

To enable the tunings, you can:

* Uncomment them in the [filename]`/etc/tuned/profile_name/tuned.conf file`.
* Use the [option]`--enable` or [option]`-e` option to generate a new profile that enables most of the tunings suggested by [application]*PowerTOP*. 
+
Certain potentially problematic tunings, such as the USB autosuspend, are disabled by default and need to be uncommented manually.
