
:_mod-docs-content-type: PROCEDURE

[id="modifying-a-spec-file_{context}"]
= Modifying an original spec file

[role="_abstract"]
The original output `spec` file generated by the `rpmdev-newspec` utility represents a template that you must modify to provide necessary instructions for the `rpmbuild` utility. `rpmbuild` then uses these instructions to build an RPM package.

.Prerequisites

* The unpopulated [filename]`~/rpmbuild/SPECS/<name>.spec` `spec` file was created by using the `rpmdev-newspec` utility. For more information, see xref:creating-spec-files-with-rpmdev-newspec_working-with-spec-files[Creating a new spec file for sample Bash, C, and Python programs].

.Procedure

. Open the [filename]`~/rpmbuild/SPECS/<name>.spec` file provided by the `rpmdev-newspec` utility.

. Populate the following directives of the `spec` file _Preamble_ section:
+
`Name`::
`Name` was already specified as an argument to [command]`rpmdev-newspec`.

`Version`::
Set `Version` to match the upstream release version of the source code.

`Release`::
`Release` is automatically set to `1%{?dist}`, which is initially `1`.

`Summary`::
Enter a one-line explanation of the package.

`License`::
Enter the software license associated with the source code.
+
`URL`::
Enter the URL to the upstream software website. For consistency, utilize the `%{name}` RPM macro variable and use the `\https://example.com/%{name}` format.

+
`Source`::
Enter the URL to the upstream software source code. Link directly to the software version being packaged.
+
NOTE: The example URLs in this documentation include hard-coded values that could possibly change in the future. Similarly, the release version can change as well. To simplify these potential future changes, use the `%{name}` and `%{version}` macros. By using these macros, you need to update only one field in the `spec` file.

`BuildRequires`::
Specify build-time dependencies for the package.
+
`Requires`::
Specify run-time dependencies for the package.
+
`BuildArch`::
Specify the software architecture.

. Populate the following directives of the `spec` file _Body_ section. You can think of these directives as section headings, because these directives can define multi-line, multi-instruction, or scripted tasks to occur.
+
`%description`::
Enter the full description of the software.
+
`%prep`::
Enter a command or series of commands to prepare software for building.
+
`%build`::
Enter a command or series of commands for building software.
+
`%install`::
Enter a command or series of commands that instruct the `rpmbuild` command on how to install the software into the `BUILDROOT` directory.
+
`%files`::
Specify the list of files, provided by the RPM package, to be installed on your system.
+
`%changelog`::
Enter the list of datestamped entries for each `Version-Release` of the package.
+
Start the first line of the `%changelog` section with an asterisk (`*`) character followed by `Day-of-Week Month Day Year Name Surname <email> - Version-Release`.
+
For the actual change entry, follow these rules:
+
* Each change entry can contain multiple items, one for each change.
* Each item starts on a new line.
* Each item begins with a hyphen (`-`) character.

You have now written an entire `spec` file for the required program.

[role="_additional-resources"]
.Additional resources
* xref:ref_spec-file-preamble-items_assembly_what-a-spec-file-is[Preamble items]
* xref:ref_spec-file-body-items_assembly_what-a-spec-file-is[Body items]
* xref:an-example-spec-file-for-bello_working-with-spec-files[An example spec file for a sample Bash program]

ifeval::[{ProductNumber} == 8]
* xref:an-example-spec-file-for-pello_working-with-spec-files[An example spec file for a sample Python program]
endif::[]

ifeval::[{ProductNumber} == 9]
* xref:an-example-spec-file-for-a-program-written-in-python-rhel9_working-with-spec-files[An example spec file for a sample Python program]
endif::[]

* xref:an-example-spec-file-for-cello_working-with-spec-files[An example spec file for a sample C program]

* xref:building-rpms_packaging-software[Building RPMs]
