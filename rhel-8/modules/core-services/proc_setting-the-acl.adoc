:_mod-docs-content-type: PROCEDURE
[id="setting-the-acl_{context}"]

= Setting the Access Control List

[role="_abstract"]
You can use the `setfacl` utility to set the ACL for a file or directory.

.Prerequisites

* `root` access.

.Procedure

* To set the ACL for a file or directory, use:

[literal,subs="+quotes,attributes"]
----
# *setfacl -m u:__username__:__symbolic_value__ _file-name_*
----
Replace _username_ with the name of the user, _symbolic_value_ with a symbolic value, and _file-name_ with the name of the file or directory. For more information see the `setfacl` man page.

.Modifying permissions for a group project
====
The following example describes how to modify permissions for the `group-project` file owned by the `root` user that belongs to the `root` group so that this file is:

* Not executable by anyone.
* The user `andrew` has the `rw-` permissions.
* The user `susan` has the `---` permissions.
* Other users have the `r--` permissions.

.Procedure
[literal,subs="+quotes,attributes"]
----
# *setfacl -m u:andrew:rw- group-project*
# *setfacl -m u:susan:--- group-project*
----

.Verification steps

* To verify that the user `andrew` has the `rw-` permission, the user `susan` has the `---` permission, and other users have the `r--` permission, use:
+
[literal,subs="+quotes,attributes"]
----
$ *getfacl group-project*
----
+
The output returns:
+
----
# file: group-project
# owner: root
# group: root
user:andrew:rw-
user:susan:---
group::r--
mask::rw-
other::r--
----
====
