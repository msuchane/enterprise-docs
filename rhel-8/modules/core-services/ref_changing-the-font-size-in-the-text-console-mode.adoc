:_mod-docs-content-type: REFERENCE

[id="changing-the-font-size-in-text-console-mode_{context}"]
= Changing the font size in text console mode

[role="_abstract"]
You can change the font size in the virtual console by using the [command]`setfont` command.

* Enter the [command]`setfont` command with the name of the font, for example:
+
[subs="+quotes"]

----
# *setfont /usr/lib/kbd/consolefonts/LatArCyrHeb-19.psfu.gz*
----

[NOTE]
====
The `setfont` command searches for multiple hard-coded paths by default. Therefore, `setfont` does not require the full name and path to the font.
====

* To double the size of the font horizontally and vertically, enter the [command]`setfont` command with `-d` parameter:
+
[subs="+quotes"]
----
# *setfont -d LatArCyrHeb-16*
----

[NOTE]
====
The maximum font size that you can double is 16x16 pixel. 
====

* To preserve the selected font during the reboot of the system, use the `FONT` variable in the `/etc/vconsole.conf` file, for example:
+
[subs="+quotes"]
----
# *cat /etc/vconsole.conf* 
KEYMAP="us"
FONT="eurlatgr"
----

* You can find various fonts in the `kbd-misc` package, which is installed with the`kbd` package. For example, the font `LatArCyrHeb` has many variants:

+
[subs="+quotes"]
----
# *rpm -ql kbd-misc | grep LatAr*

/usr/lib/kbd/consolefonts/LatArCyrHeb-08.psfu.gz
/usr/lib/kbd/consolefonts/LatArCyrHeb-14.psfu.gz
/usr/lib/kbd/consolefonts/LatArCyrHeb-16+.psfu.gz
/usr/lib/kbd/consolefonts/LatArCyrHeb-16.psfu.gz
/usr/lib/kbd/consolefonts/LatArCyrHeb-19.psfu.gz
----

[NOTE]
====
The maximum supported font size by the virtual console is 32 pixels. You can reduce the font readability problem by using smaller resolution for the console.
====

