:_mod-docs-content-type: CONCEPT
[id="chrony-intro_{context}"]

= Introduction to chrony suite

[role="_abstract"]
[application]*chrony* is an implementation of the `Network Time Protocol (NTP)`. You can use [application]*chrony*:

* To synchronize the system clock with `NTP` servers

* To synchronize the system clock with a reference clock, for example a GPS receiver

* To synchronize the system clock with a manual time input

* As an `NTPv4(RFC 5905)` server or peer to provide a time service to other computers in the network

[application]*chrony* performs well in a wide range of conditions, including intermittent network connections, heavily congested networks, changing temperatures (ordinary computer clocks are sensitive to temperature), and systems that do not run continuously, or run on a virtual machine.

Typical accuracy between two machines synchronized over the Internet is within a few milliseconds, and for machines on a LAN within tens of microseconds. Hardware timestamping or a hardware reference clock may improve accuracy between two machines synchronized to a sub-microsecond level.

[application]*chrony* consists of `chronyd`, a daemon that runs in user space, and [application]*chronyc*, a command line program which can be used to monitor the performance of `chronyd` and to change various operating parameters when it is running.

The [application]*chrony* daemon, `chronyd`, can be monitored and controlled by the command line utility [application]*chronyc*. This utility provides a command prompt which allows entering a number of commands to query the current state of `chronyd` and make changes to its configuration. By default, `chronyd` accepts only commands from a local instance of [application]*chronyc*, but it can be configured to accept monitoring commands also from remote hosts. The remote access should be restricted.
