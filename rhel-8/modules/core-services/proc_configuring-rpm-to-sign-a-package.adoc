:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-rpm-to-sign-a-package_{context}"]
= Configuring RPM to sign a package

[role="_abstract"]
To be able to sign an RPM package, you need to specify the `%_gpg_name` RPM macro.

The following procedure describes how to configure RPM for signing a package.


.Procedure

* Define the `%_gpg_name` macro in your `$HOME/.rpmmacros` file as follows:
+
[literal,subs="+quotes,verbatim,normal"]
....
%_gpg_name __Key ID__
....
+
Replace _Key ID_ with the GNU Privacy Guard (GPG) key ID that you will use to sign a package. A valid GPG key ID value is either a full name or email address of the user who created the key.
