:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_notable-changes-in-tcl.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.

[id="notable-changes-in-tcl_{context}"]
= Notable changes in Tcl/Tk 8.6

RHEL 8 is distributed with [application]*Tcl/Tk version 8.6*, which provides multiple notable changes over [application]*Tcl/Tk version 8.5*:

* Object-oriented programming support
* Stackless evaluation implementation
* Enhanced exceptions handling
* Collection of third-party packages built and installed with Tcl
* Multi-thread operations enabled
* SQL database-powered scripts support
* IPv6 networking support
* Built-in Zlib compression
* List processing
+
Two new commands, [command]`lmap` and [command]`dict map` are available, which allow the expression of transformations over [application]*Tcl* containers.
* Stacked channels by script
+
Two new commands, [command]`chan push` and [command]`chan pop` are available, which allow to add or remove transformations to or from I/O channels.

For more detailed information about [application]*Tcl/Tk version 8.6* changes and new feaures, see the following resources:

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/configuring_basic_system_settings/index#tcl-notable-changes_getting-started-with-tcl[Configuring basic system settings]
* link:https://wiki.tcl-lang.org/page/Changes+in+Tcl%2FTk+8%2E6[Changes in Tcl/Tk 8.6]



