:_mod-docs-content-type: PROCEDURE
[id="changing-the-default-umask_{context}"]

= Changing the default umask for the non-login shell

[role="_abstract"]
You can change the default `bash` _umask_ for standard users by modifying the `/etc/bashrc` file.

.Prerequisites
* `root` access

.Procedure
. As `root`, open the `/etc/bashrc` file in the editor.

. Modify the following sections to set a new default `bash` _umask_:
+
[subs=+quotes]
----
    if [ $UID -gt 199 ] && [ "`id -gn`" = "`id -un`" ]; then
       *umask 002*
    else
       umask 022
    fi
----
+
Replace the default octal value of the _umask_ (`002`) with another octal value. See xref:user-file-creation-mode-mask_assembly_managing-file-permissions[User file-creation mode mask] for more details.

. Save the changes and exit the editor.
//Changes take effect after next login.
