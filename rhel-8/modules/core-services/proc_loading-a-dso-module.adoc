:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_working-with-apache-modules.adoc

[id="loading-a-dso-module_{context}"]
= Loading a DSO module

[role="_abstract"]
As an administrator, you can choose the functionality to include in the server by configuring which modules the server should load. To load a particular DSO module, use the [option]`LoadModule` directive. Note that modules provided by a separate package often have their own configuration file in the [filename]`/etc/httpd/conf.modules.d/` directory.


.Prerequisites

* You have installed the [package]`httpd` package.

.Procedure

. Search for the module name in the configuration files in the `/etc/httpd/conf.modules.d/` directory:
+
[literal,subs="+quotes",options="nowrap",role=white-space-pre]
----
# **grep mod_ssl.so /etc/httpd/conf.modules.d/***
----

. Edit the configuration file in which the module name was found, and uncomment the [option]`LoadModule` directive of the module:
+
[literal,subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
*LoadModule ssl_module modules/mod_ssl.so*
----

. If the module was not found, for example, because a RHEL package does not provide the module, create a configuration file, such as [filename]`/etc/httpd/conf.modules.d/30-example.conf` with the following directive:
+
[literal,subs="+quotes",options="nowrap",role=white-space-pre]
----
*LoadModule ssl_module modules/<custom_module>.so*
----

. Restart the [systemitem]`httpd` service:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *systemctl restart httpd*
----
