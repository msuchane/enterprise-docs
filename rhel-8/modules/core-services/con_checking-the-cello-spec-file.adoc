:_mod-docs-content-type: PROCEDURE

[id="checking-the-cello-spec-file_{context}"]
= Checking the cello spec file for sanity

Inspect the outputs of the following examples to learn how to check a `cello` `spec` file for sanity.

.Output of running the [command]`rpmlint` command on the `cello` `spec` file

[subs="+quotes"]
....
$ *rpmlint ~/rpmbuild/SPECS/cello.spec*
/home/admiller/rpmbuild/SPECS/cello.spec: W: *invalid-url Source0*: https://www.example.com/cello/releases/cello-1.0.tar.gz HTTP Error 404: Not Found
0 packages and 1 specfiles checked; 0 errors, 1 warnings.
....

For [filename]`cello.spec`, there is only one `invalid-url Source0` warning. This warning means that the URL listed in the `Source0` directive is unreachable. This is expected because the specified `example.com` URL does not exist. Assuming that this URL will be valid in the future, you can ignore this warning.

.Output of running the [command]`rpmlint` command on the `cello` SRPM

[subs="+quotes"]
....
$ *rpmlint ~/rpmbuild/SRPMS/cello-1.0-1.el8.src.rpm*
cello.src: W: *invalid-url URL*: https://www.example.com/cello HTTP Error 404: Not Found
cello.src: W: invalid-url Source0: https://www.example.com/cello/releases/cello-1.0.tar.gz HTTP Error 404: Not Found
1 packages and 0 specfiles checked; 0 errors, 2 warnings.
....

For the `cello` SRPM, there is a new `invalid-url URL` warning. This warning means that the URL specified in the `URL` directive is unreachable. Assuming that this URL will be valid in the future, you can ignore this warning.



