:_mod-docs-content-type: PROCEDURE
[id="proc_configuring-the-system-locale_{context}"]
= Configuring the system locale

[role="_abstract"]
System-wide locale settings are stored in the `/etc/locale.conf` file that is read at early boot by the `systemd` daemon. Every service or user inherits the locale settings configured in `/etc/locale.conf`, unless individual programs or individual users override them.

.Procedure

* To list available system locale settings:
+
[literal,subs="+quotes,attributes"]
----
$ *localectl list-locales*
C.utf8
aa_DJ
aa_DJ.iso88591
aa_DJ.utf8
...
----

* To display the current status of the system locales settings:
+
[literal,subs="+quotes,attributes"]
----
$ *localectl status*
----

* To set or change the default system locale settings, use a `localectl set-locale` sub-command as the `root` user. For example:
+
[literal,subs="+quotes,attributes"]
----
# *localectl set-locale LANG=_en_US_*
----


[role="_additional-resources"]
.Additional resources
* `man localectl(1)`, `man locale(7)`, and `man locale.conf(5)`
