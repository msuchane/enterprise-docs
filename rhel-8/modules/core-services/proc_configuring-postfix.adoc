:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-postfix_{context}"]
= Configuring Postfix

[role="_abstract"]
The `main.cf` is the global Postfix configuration file and specifies many configuration options.

.Procedure

Following are few options you can add in the `/etc/postfix/main.cf` file to configure Postfix:

. myhostname: replace `host.domain.tld` with the mail server’s hostname. For example:
+
[literal,subs="+quotes,attributes"]
----
myhostname = _<mail.example.com>_
----

. mydomain: replace `domain.tld` with the domain mail server. For example:
+
[literal,subs="+quotes,attributes"]
----
mydomain = _<example.com>_
----
+
[NOTE]
====
With correct DNS setups, the myhostname and mydomain options should be autodetected and set automatically without user intervention.
====

. mail_spool_directory: allows specifying the location of the mailbox files. For example:
+
[literal,subs="+quotes,attributes"]
----
mail_spool_directory = /var/mail
----

. mynetworks: add the list of valid and trusted remote SMTP servers. This option should include only local network IP addresses or networks separated by commas or whitespaces. Adding local network addresses avoids unauthorized access to mail servers.

. Uncomment the inet_interfaces = all line. This option allows Postfix to be accessible from the internet. With its default configuration, it will only receive emails from the local machine.

. Comment the inet_interfaces = localhost line. This option allows Postfix to be accessible from the internet. With its default configuration, it will only receive emails from the local machine.

. Restart the postfix service.
+
[literal,subs="+quotes,attributes"]
----
# *systemctl reload postfix*
----


.Verification

* To verify an email communication between local users on the system:
+
[literal,subs="+quotes,attributes"]
----
# *echo "This is a test message" | mail -s <SUBJECT> <name@mydomain.com>*
----
Press Ctrl+D to send the message.

* To verify the open relay is not active on your new mail server, send an email from your mail server to a domain that your new mail server does not accept mail:
+
[literal,subs="+quotes,attributes"]
----
hello _<mydomain.com>_
mail from: _<name@mydomain.com>_
rcpt to: _<name@notmydomain.com>_

554 Relay access denied - the server is not going to relay.

250 OK or similar - the server is going to relay.

The 'rcpt to' option should only accept mail addressed to addresses @mydomain.com
----

[IMPORTANT]
====
In case of errors, check the `/var/log/maillog` file.
====

[role="_additional-resources"]
.Additional resources
* `/etc/postfix/main.cf` configuration file
