:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_migrating-to-a-rhel-8-version-of-postgresql.adoc

[id="fast-upgrade-using-the-pg_upgrade-tool_{context}"]
= Fast upgrade using the pg_upgrade utility

During a fast upgrade, you must copy binary data files to the [filename]`/var/lib/pgsql/data/` directory and use the `pg_upgrade` utility.

You can use this method for migrating data:

* From the RHEL 7 system version of *PostgreSQL 9.2* to the RHEL 8 version of *PostgreSQL 10*
* From the RHEL 8 version of *PostgreSQL 10* to a RHEL version of *PostgreSQL 12*
* From the RHEL 8 version of *PostgreSQL 12* to a RHEL version of *PostgreSQL 13*
* From a RHEL version of *PostgreSQL 13* to a RHEL version of *PostgreSQL 15*
* From a RHEL version of *PostgreSQL 15* to a RHEL version of *PostgreSQL 16*

If you want to upgrade from an earlier `postgresql` stream within RHEL 8, follow the procedure described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/installing_managing_and_removing_user-space_components/index#switching-to-a-later-stream_managing-versions-of-appstream-content[Switching to a later stream] and then migrate your *PostgreSQL* data.

For migrating between other combinations of *PostgreSQL* versions within RHEL, and for migration from the {RH} Software Collections versions of *PostgreSQL* to RHEL, use xref:dump-and-restore-upgrade_{context}[Dump and restore upgrade].

The following procedure describes migration from the RHEL 7 system version of *PostgreSQL 9.2* to a RHEL 8 version of *PostgreSQL* using the fast upgrade method.


.Prerequisites

* Before performing the upgrade, back up all your data stored in the *PostgreSQL* databases. By default, all data is stored in the [filename]`/var/lib/pgsql/data/` directory on both the RHEL 7 and RHEL 8 systems.



.Procedure

. On the RHEL 8 system, enable the stream (version) to which you want to migrate:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **{PackageManagerCommand} module enable postgresql:__stream__**
....
+
Replace _stream_ with the selected version of the *PostgreSQL* server.
+
You can omit this step if you want to use the default stream, which provides *PostgreSQL 10*.

. On the RHEL 8 system, install the [package]`postgresql-server` and [package]`postgresql-upgrade` packages:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **{PackageManagerCommand} install postgresql-server postgresql-upgrade**
....
+
Optionally, if you used any *PostgreSQL* server modules on RHEL 7, install them also on the RHEL 8 system in two versions, compiled both against *PostgreSQL 9.2* (installed as the [package]`postgresql-upgrade` package) and the target version of *PostgreSQL* (installed as the [package]`postgresql-server` package). If you need to compile a third-party *PostgreSQL* server module, build it both against the [package]`postgresql-devel` and [package]`postgresql-upgrade-devel` packages.

. Check the following items:
+
* Basic configuration: On the RHEL 8 system, check whether your server uses the default [filename]`/var/lib/pgsql/data` directory and the database is correctly initialized and enabled. In addition, the data files must be stored in the same path as mentioned in the [filename]`/usr/lib/systemd/system/postgresql.service` file.
* *PostgreSQL* servers: Your system can run multiple *PostgreSQL* servers. Ensure that the data directories for all these servers are handled independently.
* *PostgreSQL* server modules: Ensure that the *PostgreSQL* server modules that you used on RHEL 7 are installed on your RHEL 8 system as well. Note that plugins are installed in the [filename]`/usr/lib64/pgsql/` directory (or in the [filename]`/usr/lib/pgsql/` directory on 32-bit systems).

. Ensure that the `postgresql` service is not running on either of the source and target systems at the time of copying data.
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **systemctl stop postgresql.service**
....

. Copy the database files from the source location to the [filename]`/var/lib/pgsql/data/` directory on the RHEL 8 system.


. Perform the upgrade process by running the following command as the *PostgreSQL* user:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **postgresql-setup --upgrade**
....
+
This launches the `pg_upgrade` process in the background.
+
In case of failure, [command]`postgresql-setup` provides an informative error message.

. Copy the prior configuration from [filename]`/var/lib/pgsql/data-old` to the new cluster.
+
Note that the fast upgrade does not reuse the prior configuration in the newer data stack and the configuration is generated from scratch. If you want to combine the old and new configurations manually, use the *.conf files in the data directories.

. Start the new *PostgreSQL* server:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **systemctl start postgresql.service**
....

. Analyze the new database cluster.
+
* For *PostgreSQL 13* or earlier:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
**su postgres -c '~/analyze_new_cluster.sh'**
....
+
* For *PostgreSQL 15* or later:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
**su postgres -c 'vacuumdb --all --analyze-in-stages'**
....
+
NOTE: You may need to use `ALTER COLLATION name REFRESH VERSION`, see the link:https://www.postgresql.org/docs/current/sql-altercollation.html[upstream documentation] for details.
// ^ this is a pre-GA hotfix, will be tested and written properly, possibly with examples

. If you want the new *PostgreSQL* server to be automatically started on boot, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **systemctl enable postgresql.service**
....
