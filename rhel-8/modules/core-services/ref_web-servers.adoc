:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_web-servers.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
[id="web-servers_{context}"]
= Web servers

include::con_apache-changes-to-rhel7.adoc[leveloffset=+1]

For instructions on deploying, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/deploying_different_types_of_servers/index#setting-apache-http-server_Deploying-different-types-of-servers[Setting up the Apache HTTP web server].


[id="nginx_{context}"]
== The `nginx` web server new in RHEL

RHEL 8 introduces `nginx 1.14`, a web and proxy server supporting HTTP and other protocols, with a focus on high concurrency, performance, and low memory usage. `nginx` was previously available only as a Software Collection. 

The `nginx` web server now supports loading TLS private keys from hardware security tokens directly from `PKCS#11` modules. As a result, an `nginx` configuration can use `PKCS#11` URLs to identify the TLS private key in the `ssl_certificate_key` directive.
//https://bugzilla.redhat.com/show_bug.cgi?id=1545526


[id="tomcat-removal_{context}"]
== Apache Tomcat removed in RHEL 8.0, reintroduced in RHEL 8.8

The Apache Tomcat server was removed from Red Hat Enterprise Linux 8.0 and reintroduced in RHEL 8.8. Tomcat is the servlet container that is used in the official Reference Implementation for the Java Servlet and JavaServer Pages technologies. The Java Servlet and JavaServer Pages specifications are developed by Sun under the Java Community Process. Tomcat is developed in an open and participatory environment and released under the Apache Software License version 2.0.

Users of earlier minor versions than RHEL 8.8 who require a servlet container can use the link:https://www.redhat.com/en/technologies/jboss-middleware/web-server[JBoss Web Server].

//https://bugzilla.redhat.com/show_bug.cgi?id=1700823
//https://bugzilla.redhat.com/show_bug.cgi?id=2160455


