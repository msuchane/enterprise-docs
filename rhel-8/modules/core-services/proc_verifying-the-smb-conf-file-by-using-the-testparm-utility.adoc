:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_using-samba-as-a-server.adoc
// assembly_frequently-used-samba-command-line-utilities.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_verifying-the-smb-conf-file-by-using-the-testparm-utility.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_my-concept-module-a.adoc
// * ID: [id='proc_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.


// This module is included two different assemblies. Depending in which
// assembly it is included, the section's title is different. That's why
// we need the "ifeval" conditions here.
[id="proc_verifying-the-smb-conf-file-by-using-the-testparm-utility_{context}"]
= Verifying the smb.conf file by using the testparm utility
The `testparm` utility verifies that the Samba configuration in the [filename]`/etc/samba/smb.conf` file is correct. The utility detects invalid parameters and values, but also incorrect settings, such as for ID mapping. If `testparm` reports no problem, the Samba services will successfully load the [filename]`/etc/samba/smb.conf` file. Note that `testparm` cannot verify that the configured services will be available or work as expected.

[IMPORTANT]
====
{RH} recommends that you verify the [filename]`/etc/samba/smb.conf` file by using `testparm` after each modification of this file.
====


.Prerequisites

* You installed Samba.
* The [filename]`/etc/samba/smb.conf` file exits.


.Procedure

. Run the `testparm` utility as the `root` user:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *testparm*
Load smb config files from /etc/samba/smb.conf
rlimit_max: increasing rlimit_max (1024) to minimum Windows limit (16384)
*Unknown parameter encountered: "log levell"*
Processing section "[example_share]"
Loaded services file OK.
*ERROR: The idmap range for the domain * (tdb) overlaps with the range of DOMAIN (ad)!*

Server role: ROLE_DOMAIN_MEMBER

Press enter to see a dump of your service definitions

# Global parameters
[global]
	...

[example_share]
	...
----
+
The previous example output reports a non-existent parameter and an incorrect ID mapping configuration.

. If `testparm` reports incorrect parameters, values, or other errors in the configuration, fix the problem and run the utility again.
