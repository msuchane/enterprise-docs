:_mod-docs-content-type: PROCEDURE

[id="building-software-from-natively-compiled-code_{context}"]
= Building software from Natively Compiled Code

[role="_abstract"]
You can build the `cello.c` program written in the
ifdef::community[https://en.wikipedia.org/wiki/C_%28programming_language%29[C]]
ifdef::rhel[C]
language into an executable.

`cello.c`

[literal,subs="+quotes,verbatim,macros,normal"]
....
#include <stdio.h>

int main(void) {
    printf("Hello World\n");
    return 0;
}
....

[id="manual-building_{context}"]
== Manual building

If you want to build the `cello.c` program manually, use this procedure:

.Procedure

. Invoke the
ifdef::community[https://en.wikipedia.org/wiki/C_%28programming_language%29[C]]
ifdef::rhel[C]
compiler from the link:https://gcc.gnu.org/[GNU Compiler Collection] to compile the source code into binary:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ *gcc -g -o cello cello.c*
....

. Execute the resulting output binary ``cello``:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ *./cello*
Hello World
....

// That is all. You have built and ran natively compiled software from source code.

== Automated building

Large-scale software commonly uses automated building that is done by creating the [filename]`Makefile` file and then running the link:http://www.gnu.org/software/make/[GNU ``make``] utility.

If you want to use the automated building to build the `cello.c` program, use this procedure:

.Procedure

. To set up automated building, create the [filename]`Makefile` file with the following content in the same directory as `cello.c`.
+
`Makefile`
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
cello:
	gcc -g -o cello cello.c
clean:
	rm cello
....
+
Note that the lines under `cello:` and `clean:` must begin with a tab space.

. To build the software, run the [command]`make` command:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ *make*
make: 'cello' is up to date.
....

. Since there is already a build available, run the [command]`make clean` command, and after run the [command]`make` command again:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ *make clean*
rm cello

$ *make*
gcc -g -o cello cello.c
....
+
[NOTE]
====
Trying to build the program after another build has no effect.

[literal,subs="+quotes,verbatim,macros,normal"]
....
$ *make*
make: 'cello' is up to date.
....
====

. Execute the program:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ *./cello*
Hello World
....

You have now compiled a program both manually and using a build tool.
