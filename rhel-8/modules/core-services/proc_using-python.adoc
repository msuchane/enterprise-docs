:_mod-docs-content-type: PROCEDURE
[id="proc_using-python_{context}"]
= Using Python

[role="_abstract"]
When running the Python interpreter or Python-related commands, always specify the version.

.Prerequisites

* Ensure that the required version of Python is installed.
* If you want to download and install third-party applications for Python 3.11 or Python 3.12, install the `python3.11-pip` or `python3.12-pip` package.

.Procedure

* To run the Python 3.6 interpreter or related commands, use, for example:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
$ *python3*
$ *python3 -m venv --help*
$ *python3 -m pip install _package_*
$ *pip3 install _package_*
----

* To run the Python 3.8 interpreter or related commands, use, for example:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
$ *python3.8*
$ *python3.8 -m venv --help*
$ *python3.8 -m pip install _package_*
$ *pip3.8 install _package_*
----


* To run the Python 3.9 interpreter or related commands, use, for example:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
$ *python3.9*
$ *python3.9 -m venv --help*
$ *python3.9 -m pip install _package_*
$ *pip3.9 install _package_*
----


* To run the Python 3.11 interpreter or related commands, use, for example:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
$ *python3.11*
$ *python3.11 -m venv --help*
$ *python3.11 -m pip install _package_*
$ *pip3.11 install _package_*
----

* To run the Python 3.12 interpreter or related commands, use, for example:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
$ *python3.12*
$ *python3.12 -m venv --help*
$ *python3.12 -m pip install _package_*
$ *pip3.12 install _package_*
----

* To run the Python 2 interpreter or related commands, use, for example:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
$ *python2*
$ *python2 -m pip install _package_*
$ *pip2 install _package_*
----
