:_mod-docs-content-type: PROCEDURE

[id="proc_installing-and-configuring-postfix_{context}"]
= Installing and configuring a Postfix SMTP server

[role="_abstract"]
You can configure your Postfix SMTP server to receive, store, and deliver email messages. If the mail server package is not selected during the system installation, Postfix will not be available by default. Perform the following steps to install Postfix:

.Prerequisites

* You have the root access.

* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#subman-rhel8-setup_post-installation-tasks[Register your system].

* Disable and remove Sendmail:
[literal,subs="+quotes,attributes"]
+
----
# *{PackageManagerCommand} remove sendmail*
----

.Procedure

. Install Postfix:
[literal,subs="+quotes,attributes"]
+
----
# *{PackageManagerCommand} install postfix*
----

. To configure Postfix, edit the `/etc/postfix/main.cf` file and make the following changes:

.. By default, Postfix receives emails only on the `loopback` interface. To configure Postfix to listen on specific interfaces, update the `inet_interfaces` parameter to the IP addresses of these interfaces:
+
[subs="+quotes"]
----
inet_interfaces = 127.0.0.1/32, [::1]/128, 192.0.2.1, [2001:db8:1::1]
----
+
To configure Postfix to listen on all interfaces, set:
+
[subs="+quotes"]
----
inet_interfaces = all
----

.. If you want that Postfix uses a different hostname than the fully-qualified domain name (FQDN) that is returned by the `gethostname()` function, add the `myhostname` parameter:
+
[subs="+quotes"]
----
myhostname = _<smtp.example.com>_
----
+
For example, Postfix adds this hostname to header of emails it processes.

.. If the domain name differs from the one in the `myhostname` parameter, add the `mydomain` parameter:
+
[subs="+quotes"]
----
mydomain = _<example.com>_
----

.. Add the `myorigin` parameter and set it to the value of `mydomain`:
+
[subs="+quotes"]
----
myorigin = $mydomain
----
+
With this setting, Postfix uses the domain name as origin for locally posted mails instead of the hostname.

.. Add the `mynetworks` parameter, and define the IP ranges of trusted networks that are allowed to send mails:
+
[subs="+quotes"]
----
mynetworks = 127.0.0.1/32, [::1]/128, 192.0.2.1/24, [2001:db8:1::1]/64
----
+
If clients from not trustworthy networks, such as the Internet, should be able to send mails through this server, you must configure relay restrictions in a later step.

. Verify if the Postfix configuration in the `main.cf` file is correct:
+
[literal,subs="+quotes,attributes"]
----
$ *postfix check*
----

. Enable the `postfix` service to start at boot and start it:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl enable --now postfix*
----

. Allow the smtp traffic through firewall and reload the firewall rules:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --permanent --add-service smtp*

# *firewall-cmd --reload*
----


.Verification

. Verify that the `postfix` service is running:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl status postfix*
----
* Optional: Restart the `postfix` service, if the output is stopped, waiting, or the service is not running:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl restart postfix*
----

* Optional: Reload the `postfix` service after changing any options in the configuration files in the `/etc/postfix/` directory to apply those changes:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl reload postfix*
----

. Verify the email communication between local users on your system:
+
[literal,subs="+quotes,attributes"]
----
# *echo "This is a test message" | mail -s _<SUBJECT>_ _<user@mydomain.com>_*
----

. Verify that your mail server is not an open relay by sending an email to an email address outside of your domain from a client (*server1*) to your mail server (*server2*):
+
.. Edit the `/etc/postfix/main.cf` file for *server1* as follows:
+
[literal,subs="+quotes,attributes"]
----
relayhost = _<ip_address_of_server2>_
----
+
.. Edit the `/etc/postfix/main.cf` file for *server2* as follows:
+
[literal,subs="+quotes,attributes"]
----
mynetworks = _<ip_address_of_server2>_
----
+
.. On *server1*, send the following mail: 
+
[literal,subs="+quotes,attributes"]
----
# *echo "This is an open relay test message" | mail -s _<SUBJECT>_ _<user@example.com>_*
----
+
.. Check the `/var/log/maillog` file:
+
[literal,subs="+quotes,attributes"]
----
554 Relay access denied - the server is not going to relay.
250 OK or similar - the server is going to relay.
----


.Troubleshooting

* In case of errors, check the `/var/log/maillog` file.

[role="_additional-resources"]
.Additional resources
* The `/etc/postfix/main.cf` configuration file
* The `/usr/share/doc/postfix/README_FILES` directory
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/securing_networks/using-and-configuring-firewalld_securing-networks[Using and configuring firewalld]
