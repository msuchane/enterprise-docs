// Module included in the following assemblies:
//setting-up-graphical-representation-of-pcp-metrics
// <List assemblies here, each on a new line>
:experimental:
:_mod-docs-content-type: PROCEDURE
[id="creating-panel-and-alerts-in-pcp-redis-data-source_{context}"]
= Creating panels and alert in PCP Redis data source

[role="_abstract"]
After adding the PCP Redis data source, you can view the dashboard with an overview of useful metrics, add a query to visualize the load graph, and create alerts that help you to view the system issues after they occur.

.Prerequisites

. The PCP Redis is configured. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#configuring-pcp-redis_setting-up-graphical-representation-of-pcp-metrics[Configuring PCP Redis].

. The `grafana-server` is accessible. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#accessing-the-grafana-web-ui_setting-up-graphical-representation-of-pcp-metrics[Accessing the Grafana web UI].

.Procedure

. Log into the Grafana web UI.

. In the Grafana *Home* page, click *Add your first data source*.

. In the *Add data source* pane, type redis in the *Filter by name or type* text box and then click *PCP Redis*.

. In the *Data Sources / PCP Redis* pane, perform the following:

.. Add `++http://localhost:44322++` in the *URL* field and then click btn:[Save & Test].

.. Click menu:Dashboards tab[Import > PCP Redis: Host Overview] to see a dashboard with an overview of any useful metrics.
+
.PCP Redis: Host Overview
image::pcp-redis-host-overview.png[]

. Add a new panel:
.. From the menu, hover over the &nbsp;&nbsp; image:grafana-plus-sign.png[] &nbsp;&nbsp; menu:Create icon[Dashboard > Add new panel icon] to add a panel.
.. In the *Query* tab, select the *PCP Redis* from the query list instead of the selected *default* option and in the text field of *A*, enter metric, for example, `kernel.all.load` to visualize the kernel load graph.
.. Optional: Add *Panel title* and *Description*, and update other options from the *Settings*.
.. Click btn:[Save] to apply changes and save the dashboard. Add *Dashboard name*.

.. Click btn:[Apply] to apply changes and go back to the dashboard.
+
.PCP Redis query panel
image::pcp-redis-query-panel.png[]

. Create an alert rule:
.. In the *PCP Redis query panel*, click &nbsp;&nbsp; image:redis-alert-icon.png[] &nbsp;&nbsp; *Alert* and then click *Create Alert*.

.. Edit the *Name*, *Evaluate query*, and *For* fields from the *Rule*, and specify the *Conditions* for your alert.

.. Click btn:[Save] to apply changes and save the dashboard. Click btn:[Apply] to apply changes and go back to the dashboard.
+
.Creating alerts in the PCP Redis panel
image::pcp-redis-query-alert-panel.png[]

.. Optional: In the same panel, scroll down and click btn:[Delete] icon to delete the created rule.

.. Optional: From the menu, click &nbsp;&nbsp; image:alerting-bell-icon.png[] &nbsp;&nbsp; *Alerting* icon to view the created alert rules with different alert statuses, to edit the alert rule, or to pause the existing rule from the *Alert Rules* tab.
+
To add a notification channel for the created alert rule to receive an alert notification from Grafana, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#adding-notification-channels-for-alerts_setting-up-graphical-representation-of-pcp-metrics[Adding notification channels for alerts].
