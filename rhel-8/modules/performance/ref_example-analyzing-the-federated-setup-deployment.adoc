// Module included in the following assemblies:
//setting-up-pcp
// <List assemblies here, each on a new line
:_mod-docs-content-type: REFERENCE
[id="example-analyzing-the-federated-setup-deployment_{context}"]
= Example: Analyzing the federated setup deployment

[role="_abstract"]
The following results were observed on a federated setup, also known as multiple `pmlogger` farms, consisting of three centralized logging (`pmlogger` farm) setups, where each `pmlogger` farm was monitoring 100 remote hosts, that is 300 hosts in total.

This setup of the `pmlogger` farms is identical to the configuration mentioned in the

xref:example-analyzing-the-centralized-logging-deployment_{context}[Example: Analyzing the centralized logging deployment]
for 60s logging interval, except that the Redis servers were operating in cluster mode.

.Used resources depending on federated hosts for 60s logging interval
[options="header"]
|====
|PCP Archives Storage per Day |`pmlogger` Memory |Network per Day (In/Out) | `pmproxy` Memory |Redis Memory per Day
|277 MB |1058 MB |15.6 MB / 12.3 MB |6-8 GB |5.5 GB
|====

Here, all values are per host.
The network bandwidth is higher due to the inter-node communication of the Redis cluster.
