// Module included in the following assemblies:
//setting-up-pcp
// <List assemblies here, each on a new line
:_mod-docs-content-type: PROCEDURE
[id="troubleshooting-high-memory-usage_{context}"]
= Troubleshooting high memory usage

[role="_abstract"]
The following scenarios can result in high memory usage:

* The `pmproxy` process is busy processing new PCP archives and does not have spare CPU cycles to process Redis requests and responses.
* The Redis node or cluster is overloaded and cannot process incoming requests on time.

The `pmproxy` service daemon uses Redis streams and supports the configuration parameters, which are PCP tuning parameters and affects Redis memory usage and key retention. The `/etc/pcp/pmproxy/pmproxy.conf` file lists the available configuration options for `pmproxy` and the associated APIs.

The following procedure describes how to troubleshoot high memory usage issue.

.Prerequisites

. Install the `pcp-pmda-redis` package:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install pcp-pmda-redis
----

. Install the redis PMDA:
+
----
# cd /var/lib/pcp/pmdas/redis && ./Install
----

.Procedure

* To troubleshoot high memory usage, execute the following command and observe the `inflight` column:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
$ pmrep :pmproxy
         backlog  inflight  reqs/s  resp/s   wait req err  resp err  changed  throttled
          byte     count   count/s  count/s  s/s  count/s   count/s  count/s   count/s
14:59:08   0         0       N/A       N/A   N/A    N/A      N/A      N/A        N/A
14:59:09   0         0    2268.9    2268.9    28     0        0       2.0        4.0
14:59:10   0         0       0.0       0.0     0     0        0       0.0        0.0
14:59:11   0         0       0.0       0.0     0     0        0       0.0        0.0
....
+
This column shows how many Redis requests are in-flight, which means they are queued or sent, and no reply was received so far.
+
A high number indicates one of the following conditions:

** The `pmproxy` process is busy processing new PCP archives and does not have spare CPU cycles to process Redis requests and responses.
** The Redis node or cluster is overloaded and cannot process incoming requests on time.

* To troubleshoot the high memory usage issue, reduce the number of `pmlogger` processes for this farm, and add another pmlogger farm. Use the federated - multiple pmlogger farms setup.
+
If the Redis node is using 100% CPU for an extended amount of time, move it to a host with better performance or use a clustered Redis setup instead.

* To view the `pmproxy.redis.*` metrics, use the following command:
+
----
$ pminfo -ftd pmproxy.redis
pmproxy.redis.responses.wait [wait time for responses]
    Data Type: 64-bit unsigned int  InDom: PM_INDOM_NULL 0xffffffff
    Semantics: counter  Units: microsec
    value 546028367374
pmproxy.redis.responses.error [number of error responses]
    Data Type: 64-bit unsigned int  InDom: PM_INDOM_NULL 0xffffffff
    Semantics: counter  Units: count
    value 1164
[...]
pmproxy.redis.requests.inflight.bytes [bytes allocated for inflight requests]
    Data Type: 64-bit int  InDom: PM_INDOM_NULL 0xffffffff
    Semantics: discrete  Units: byte
    value 0

pmproxy.redis.requests.inflight.total [inflight requests]
    Data Type: 64-bit unsigned int  InDom: PM_INDOM_NULL 0xffffffff
    Semantics: discrete  Units: count
    value 0
[...]
----
+
To view how many Redis requests are inflight, see the `pmproxy.redis.requests.inflight.total` metric and `pmproxy.redis.requests.inflight.bytes` metric to view how many bytes are occupied by all current inflight Redis requests.
+
In general, the redis request queue would be zero but can build up based on the usage of large pmlogger farms, which limits scalability and can cause high latency for `pmproxy` clients.

* Use the `pminfo` command to view information about performance metrics. For example, to view the `redis.*` metrics, use the following command:
+
----
$ pminfo -ftd redis
redis.redis_build_id [Build ID]
    Data Type: string  InDom: 24.0 0x6000000
    Semantics: discrete  Units: count
    inst [0 or "localhost:6379"] value "87e335e57cffa755"
redis.total_commands_processed [Total number of commands processed by the server]
    Data Type: 64-bit unsigned int  InDom: 24.0 0x6000000
    Semantics: counter  Units: count
    inst [0 or "localhost:6379"] value 595627069
[...]

redis.used_memory_peak [Peak memory consumed by Redis (in bytes)]
    Data Type: 32-bit unsigned int  InDom: 24.0 0x6000000
    Semantics: instant  Units: count
    inst [0 or "localhost:6379"] value 572234920
[...]
----
+
To view the peak memory usage, see the `redis.used_memory_peak` metric.


[role="_additional-resources"]
.Additional resources
* `pmdaredis(1)`, `pmproxy(1)`, and `pminfo(1)` man pages

* xref:pcp-deployment-architectures_{context}[PCP deployment architectures]
