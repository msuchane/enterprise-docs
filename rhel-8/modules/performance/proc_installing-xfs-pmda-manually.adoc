// Module included in the following assemblies:
//performance-analysis-of-xfs-with-pcp
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="installing-xfs-pmda-manually_{context}"]
= Installing XFS PMDA manually

[role="_abstract"]
If the XFS PMDA is not listed in the `pcp` configuration output, install the PMDA agent manually.

This procedure describes how to manually install the PMDA agent.

.Prerequisites


* PCP is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#installing-and-enabling-pcp_setting-up-pcp[Installing and enabling PCP].

.Procedure

. Navigate to the xfs directory:
+
----
# cd /var/lib/pcp/pmdas/xfs/
----

ifeval::[{ProductNumber} == 8]
. Install the XFS PMDA manually:
+
----
xfs]# ./Install

You will need to choose an appropriate configuration for install of
the “xfs” Performance Metrics Domain Agent (PMDA).

  collector     collect performance statistics on this system
  monitor       allow this system to monitor local and/or remote systems
  both          collector and monitor configuration for this system

Please enter c(ollector) or m(onitor) or (both) [b]
Updating the Performance Metrics Name Space (PMNS) ...
Terminate PMDA if already installed ...
Updating the PMCD control file, and notifying PMCD ...
Waiting for pmcd to terminate ...
Starting pmcd ...
Check xfs metrics have appeared ... 149 metrics and 149 values
----

. Select the intended PMDA role by entering `c` for collector, `m` for monitor, or `b` for both. The PMDA installation script prompts you to specify one of the following PMDA roles:
+
* The `collector` role allows the collection of performance metrics on the current system

* The `monitor` role allows the system to monitor local systems, remote systems, or both
+
The default option is both `collector` and `monitor`, which allows the XFS PMDA to operate correctly in most scenarios.
endif::[]
ifeval::[{ProductNumber} == 9]
. Install the XFS PMDA manually:
+
----
xfs]# ./Install
Updating the Performance Metrics Name Space (PMNS) ...
Terminate PMDA if already installed ...
Updating the PMCD control file, and notifying PMCD ...
Check xfs metrics have appeared ... 387 metrics and 387 values
----
endif::[]

.Verification steps

* Verify that the `pmcd` process is running on the host and the XFS PMDA is listed as enabled in the configuration:
+
----
# pcp

Performance Co-Pilot configuration on workstation:

platform: Linux workstation 4.18.0-80.el8.x86_64 #1 SMP Wed Mar 13 12:02:46 UTC 2019 x86_64
hardware: 12 cpus, 2 disks, 1 node, 36023MB RAM
timezone: CEST-2
services: pmcd
pmcd: Version 4.3.0-1, 8 agents
pmda: root pmcd proc xfs linux mmv kvm jbd2
----

[role="_additional-resources"]
.Additional resources
* `pmcd(1)` man page

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/index#system-services-distributed-with-pcp_setting-up-pcp[System services and tools distributed with PCP]
