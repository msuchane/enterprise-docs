:_newdoc-version: 2.16.0
:_mod-docs-content-type: PROCEDURE

[id="using-the-tuned-d-bus-interface-to-change-the-active-tuned-profile_{context}"]
= Using the TuneD D-Bus interface to change the active TuneD profile

[role="_abstract"]
You can replace the active TuneD profile with your desired TuneD profile by using the TuneD D-Bus interface.

.Prerequisites

* The TuneD service is running. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-tuned_monitoring-and-managing-system-status-and-performance#installing-and-enabling-tuned_getting-started-with-tuned[Installing and Enabling TuneD] for details.

.Procedure

* To change the active TuneD profile, run:
+
[literal,subs="+quotes,attributes",options="nowrap"]
----
$ busctl call com.redhat.tuned /Tuned com.redhat.tuned.control switch_profile s _profile_
(bs) true "OK"
----
+
Replace _profile_ with the name of your desired profile.

.Verification

* To view the current active TuneD profile, run:
+
[literal,subs="+quotes,attributes",options="nowrap"]
----
$ busctl call com.redhat.tuned /Tuned com.redhat.tuned.control active_profile
s "_profile_"
----
