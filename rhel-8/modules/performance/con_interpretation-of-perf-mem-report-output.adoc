:_mod-docs-content-type: CONCEPT
[id="interpretation-of-perf-mem-report-output_{context}"]
= Interpretation of perf mem report output

[role="_abstract"]
The table displayed by running the [command]`perf mem report` command without any modifiers sorts the data into several columns:

The 'Overhead' column::
Indicates percentage of overall samples collected in that particular function.

The 'Samples' column::
Displays the number of samples accounted for by that row.

The 'Local Weight' column::
Displays the access latency in processor core cycles.

The 'Memory Access' column::
Displays the type of memory access that occurred.

The 'Symbol' column::
Displays the function name or symbol.

The 'Shared Object' column::
Displays the name of the ELF image where the samples come from (the name [kernel.kallsyms] is used when the samples come from the kernel).

The 'Data Symbol' column::
Displays the address of the memory location that row was targeting.

IMPORTANT: Oftentimes, due to dynamic allocation of memory or stack memory being accessed, the 'Data Symbol' column will display a raw address.

The "Snoop" column::
Displays bus transactions.

The 'TLB Access' column::
Displays TLB memory accesses.

The 'Locked' column::
Indicates if a function was or was not memory locked.

In default mode, the functions are sorted in descending order with those with the highest overhead displayed first.
