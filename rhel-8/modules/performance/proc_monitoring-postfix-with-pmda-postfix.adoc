// Module included in the following assemblies:
//monitoring-performance-with-performance-co-pilot
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="monitoring-postfix-with-pmda-postfix_{context}"]
= Monitoring postfix with pmda-postfix

[role="_abstract"]
This procedure describes how to monitor performance metrics of the `postfix` mail server with `pmda-postfix`. It helps to check how many emails are received per second.

.Prerequisites


* PCP is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#installing-and-enabling-pcp_setting-up-pcp[Installing and enabling PCP].
* The `pmlogger` service is enabled. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/logging-performance-data-with-pmlogger_monitoring-and-managing-system-status-and-performance#enabling-the-pmlogger-service_logging-performance-data-with-pmlogger[Enabling the pmlogger service].

.Procedure

. Install the following packages:
.. Install the `pcp-system-tools`:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install pcp-system-tools
----
.. Install the `pmda-postfix` package to monitor `postfix`:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install pcp-pmda-postfix postfix
----
.. Install the logging daemon:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install rsyslog
----
.. Install the mail client for testing:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install mutt
----

. Enable the `postfix` and `rsyslog` services:
+
----
# systemctl enable postfix rsyslog
# systemctl restart postfix rsyslog
----

. Enable the SELinux boolean, so that `pmda-postfix` can access the required log files:
+
----
# setsebool -P pcp_read_generic_logs=on
----

. Install the `PMDA`:
+
----
# cd /var/lib/pcp/pmdas/postfix/

# ./Install

Updating the Performance Metrics Name Space (PMNS) ...
Terminate PMDA if already installed ...
Updating the PMCD control file, and notifying PMCD ...
Waiting for pmcd to terminate ...
Starting pmcd ...
Check postfix metrics have appeared ... 7 metrics and 58 values
----

.Verification steps

* Verify the `pmda-postfix` operation:
+
----
echo testmail | mutt root
----

* Verify the available metrics:
+
----
# pminfo postfix

postfix.received
postfix.sent
postfix.queues.incoming
postfix.queues.maildrop
postfix.queues.hold
postfix.queues.deferred
postfix.queues.active
----


[role="_additional-resources"]
.Additional resources
* `rsyslogd(8)`, `postfix(1)`, and `setsebool(8)` man pages

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/index#system-services-distributed-with-pcp_setting-up-pcp[System services and tools distributed with PCP]
