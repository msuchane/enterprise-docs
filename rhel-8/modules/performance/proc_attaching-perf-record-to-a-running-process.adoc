:_mod-docs-content-type: PROCEDURE
[id="attaching-perf-record-to-a-running-process_{context}"]
= Attaching perf record to a running process

[role="_abstract"]
You can attach `perf record` to a running process. This will instruct `perf record` to only sample and record performance data in the specified processes.

.Prerequisites

* You have the `perf` user space tool installed as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-perf_monitoring-and-managing-system-status-and-performance#installing-perf_getting-started-with-perf[Installing perf].


.Procedure

* Attach `perf record` to a running process:
+
[subs=+quotes]
----
$ perf record -p _ID1,ID2_ sleep _seconds_
----
+
The previous example samples and records performance data of the processes with the process ID's `_ID1_` and `_ID2_` for a time period of `_seconds_` seconds as dictated by using the `sleep` command. You can also configure `perf` to record events in specific threads:
+
[subs=+quotes]
----
$ perf record -t _ID1,ID2_ sleep _seconds_
----
NOTE: When using the `-t` flag and stipulating thread ID's, `perf` disables inheritance by default. You can enable inheritance by adding the `--inherit` option.
