:_mod-docs-content-type: CONCEPT
[id="systemtap-cross-instrumentation_{context}"]
= SystemTap cross-instrumentation

[role="_abstract"]
When you run a SystemTap script, a kernel module is built out of that script. SystemTap then loads the module into the kernel.

Normally, SystemTap scripts can run only on systems where SystemTap is deployed. To run SystemTap on ten systems, SystemTap needs to be deployed on all those systems. In some cases, this might be neither feasible nor desired. For example, corporate policy might prohibit you from installing packages that provide compilers or debug information about specific machines, which will prevent the deployment of SystemTap.

To work around this, use _cross-instrumentation_. Cross-instrumentation is the process of generating SystemTap instrumentation modules from a SystemTap script on one system to be used on another system. This process offers the following benefits:

* The kernel information packages for various machines can be installed on a single host machine.
+
[IMPORTANT]
====
Kernel packaging bugs may prevent the installation. In such cases, the `kernel-debuginfo` and `kernel-devel` packages for the _host system_ and _target system_ must match. If a bug occurs, report the bug at https://bugzilla.redhat.com/.
====

* Each _target machine_ needs only one package to be installed to use the generated SystemTap instrumentation module: `systemtap-runtime`.
+
[IMPORTANT]
====
The _host system_ must be the same architecture and running the same distribution of Linux as the _target system_ in order for the built _instrumentation module_ to work.
====

[NOTE]
.Terminology
====
_instrumentation module_::
The kernel module built from a SystemTap script; the SystemTap module is built on the _host system_, and will be loaded on the _target kernel_ of the _target system_.
_host system_::
The system on which the instrumentation modules (from SystemTap scripts) are compiled, to be loaded on _target systems_.
_target system_::
The system in which the _instrumentation module_ is being built (from SystemTap scripts).
_target kernel_::
The kernel of the _target system_. This is the kernel that loads and runs the _instrumentation module_.
====
