:_mod-docs-content-type: CONCEPT
[id="the-purpose-of-perf-stat_{context}"]
= The purpose of perf stat

[role="_abstract"]
The [command]`perf stat` command executes a specified command, keeps a running count of hardware and software event occurrences during the commands execution, and generates statistics of these counts. If you do not specify any events, then [command]`perf stat` counts a set of common hardware and software events.
