:_mod-docs-content-type: PROCEDURE

[id="enabling-pcp-version-3-archives_{context}"]
= Enabling PCP version 3 archives

//RHEL9.2 and on only

Performance Co-Pilot (PCP) archives store historical values of PCP metrics recorded from a single host and support retrospective performance analysis. PCP archives contain all the important metric data and metadata needed for offline or offsite analysis. These archives can be read by most PCP client tools or dumped raw by the `pmdumplog` tool.

From PCP 6.0, version 3 archives are supported in addition to version 2 archives. Version 2 archives remain the default and will continue to receive long-term support for backwards compatibility purposes in addition to version 3 archives receiving long-term support from RHEL 9.2 and on.

Using PCP version 3 archives offers the following benefits over version 2:

* Support for instance domain change-deltas
* Y2038-safe timestamps
* Nanosecond-precision timestamps
* Arbitrary timezones support
* 64-bit file offsets used for individual volumes larger than 2GB

.Prerequisites

* PCP is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#installing-and-enabling-pcp_setting-up-pcp[Installing and enabling PCP].

.Procedure

. Open the `/etc/pcp.conf` file in a text editor of your choice and set the PCP archive version:
+
[subs=+quotes]
----
PCP_ARCHIVE_VERSION=3
----

. Restart the `pmlogger` service to apply your configuration changes:
+
[subs=+quotes]
----
# systemctl restart pmlogger.service
----

. Create a new PCP archive log using your new configuration. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/monitoring_and_managing_system_status_and_performance/logging-performance-data-with-pmlogger_monitoring-and-managing-system-status-and-performance[Logging performance data with pmlogger].


.Verification

* Verify the version of the archive created with your new configuration:
+
[subs=+quotes]
----
# pmloglabel -l /var/log/pcp/pmlogger/_20230208_
Log Label (Log Format Version 3) 
Performance metrics from host _host1_ 
        commencing Wed Feb   08 00:11:09.396 2023 
        ending           Thu  Feb   07 00:13:54.347 2023
----


[role="_additional-resources"]
.Additional resources
* `logarchive(5)` man page
* `pmlogger(1)` man page
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/logging-performance-data-with-pmlogger_monitoring-and-managing-system-status-and-performance[Logging performance data with pmlogger]

