// Module included in the following assemblies:
//getting-started-with-flamegraphs
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="creating-flamegraphs-over-specific-processes_{context}"]
= Creating flamegraphs over specific processes

[role="_abstract"]
You can use `flamegraphs` to visualize performance data recorded over specific running processes.

.Prerequisites

* `flamegraphs` are installed as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-flamegraphs_monitoring-and-managing-system-status-and-performance#installing-flamegraphs_getting-started-with-flamegraphs[installing flamegraphs].

* The `perf` tool is installed as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-perf_monitoring-and-managing-system-status-and-performance#installing-perf_getting-started-with-perf[installing perf].

.Procedure

* Record the data and create the visualization:
+
[subs=+quotes]
----
# perf script flamegraph -a -F 99 -p `_ID1,ID2_` sleep 60
----
+
This command samples and records performance data of the processes with the process ID's `_ID1_` and `_ID2_` for 60 seconds, as stipulated by use of the `sleep` command, and then constructs the visualization which will be stored in the current active directory as `flamegraph.html`. The command samples call-graph data by default and takes the same arguments as the `perf` tool, in this particular case:
+
`-a`::
Stipulates to record data over the entire system.
`-F`::
To set the sampling frequency per second.
`-p`::
To stipulate specific process ID's to sample and record data over.

.Verification steps

* For analysis, view the generated visualization:
+
[subs=+quotes]
----
# xdg-open flamegraph.html
----
+
This command opens the visualization in the default browser:
+
image:flamegraph.png[]
