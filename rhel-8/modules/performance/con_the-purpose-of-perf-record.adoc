:_mod-docs-content-type: CONCEPT
[id="the-purpose-of-perf-record_{context}"]
= The purpose of perf record

[role="_abstract"]
The [command]`perf record` command samples performance data and stores it in a file, [file]`perf.data`, which can be read and visualized with other `perf` commands. [file]`perf.data` is generated in the current directory and can be accessed at a later time, possibly on a different machine.

If you do not specify a command for [command]`perf record` to record during, it will record until you manually stop the process by pressing `Ctrl+C`.  You can attach [command]`perf record` to specific processes by passing the [option]`-p` option followed by one or more process IDs. You can run [command]`perf record` without root access, however, doing so will only sample performance data in the user space. In the default mode, [command]`perf record` uses CPU cycles as the sampling event and operates in per-thread mode with inherit mode enabled.
