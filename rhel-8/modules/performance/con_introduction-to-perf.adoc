:_mod-docs-content-type: CONCEPT
[id="introduction-to-perf_{context}"]
= Introduction to perf

[role="_abstract"]
The `perf` user-space tool interfaces with the kernel-based subsystem _Performance Counters for Linux_ (PCL). `perf` is a powerful tool that uses the Performance Monitoring Unit (PMU) to measure, record, and monitor a variety of hardware and software events. `perf` also supports tracepoints, kprobes, and uprobes.
