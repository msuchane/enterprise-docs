// Module included in the following assemblies:
//setting-up-pcp
// <List assemblies here, each on a new line
:_mod-docs-content-type: CONCEPT
[id="sizing-factors_{context}"]
= Sizing factors

[role="_abstract"]
The following are the sizing factors required for scaling:

`Remote system size`::
The number of CPUs, disks, network interfaces, and other hardware resources affects the amount of data collected by each `pmlogger` on the centralized logging host.

`Logged Metrics`::
The number and types of logged metrics play an important role. In particular, the `per-process proc.*` metrics require a large amount of disk space, for example, with the standard `pcp-zeroconf` setup, 10s logging interval, 11 MB without proc metrics versus 155 MB with proc metrics - a factor of 10 times more. Additionally, the number of instances for each metric, for example the number of CPUs, block devices, and network interfaces also impacts the required storage capacity.

`Logging Interval`::
The interval how often metrics are logged, affects the storage requirements. The expected daily PCP archive file sizes are written to the `pmlogger.log` file for each `pmlogger` instance. These values are uncompressed estimates.
Since PCP archives compress very well, approximately 10:1, the actual long term disk space requirements can be determined for a particular site.

`pmlogrewrite`::
After every PCP upgrade, the `pmlogrewrite` tool is executed and rewrites old archives if there were changes in the metric metadata from the previous version and the new version of PCP. This process duration scales linear with the number of archives stored.


[role="_additional-resources"]
.Additional resources
* `pmlogrewrite(1)` and `pmlogger(1)` man pages
