:_mod-docs-content-type: PROCEDURE

:experimental:

[id="capturing-call-graph-data-with-perf-record_{context}"]
= Capturing call graph data with perf record

[role="_abstract"]
You can configure the `perf record` tool so that it records which function is calling other functions in the performance profile. This helps to identify a bottleneck if several processes are calling the same function.

.Prerequisites

* You have the `perf` user space tool installed as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-perf_monitoring-and-managing-system-status-and-performance#installing-perf_getting-started-with-perf[Installing perf].


.Procedure

* Sample and record performance data with the `--call-graph` option:
+
[subs=+quotes]
----
$ perf record --call-graph _method_ _command_
----
+
** Replace `_command_` with the command you want to sample data during. If you do not specify a command, then `perf record` will sample data until you manually stop it by pressing kbd:[Ctrl+C].
+
** Replace _method_ with one of the following unwinding methods:
+
`fp`::
Uses the frame pointer method. Depending on compiler optimization, such as with binaries built with the GCC option [option]`--fomit-frame-pointer`, this may not be able to unwind the stack.
`dwarf`::
Uses DWARF Call Frame Information to unwind the stack.
`lbr`::
Uses the last branch record hardware on Intel processors.

[role="_additional-resources"]
.Additional resources
* `perf-record(1)` man page
