// Module included in the following assemblies:
//setting-up-graphical-representation-of-pcp-metrics
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="setting-up-pcp-with-pcp-zeroconf_{context}"]
= Setting up PCP with pcp-zeroconf

[role="_abstract"]
This procedure describes how to set up PCP on a system with the `pcp-zeroconf` package. Once the `pcp-zeroconf` package is installed, the system records the default set of metrics into archived files.

.Procedure

* Install the `pcp-zeroconf` package:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install pcp-zeroconf
----

.Verification steps

* Ensure that the `pmlogger` service is active, and starts archiving the metrics:
+
[literal,subs="quotes,attributes", options="nowrap",role=white-space-pre]
....
# pcp | grep pmlogger
 pmlogger: primary logger: /var/log/pcp/pmlogger/_localhost.localdomain/20200401.00.12_

....

[role="_additional-resources"]
.Additional resources
* `pmlogger` man page

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/index#monitoring-performance-with-performance-co-pilot_monitoring-and-managing-system-status-and-performance[Monitoring performance with Performance Co-Pilot]
