// Module included in the following assemblies:
//tuning-scheduling-policy
// <List assemblies here, each on a new line>
:_mod-docs-content-type: CONCEPT
[id="con_categories-of-scheduling-policies_{context}"]
= Categories of scheduling policies

[role="_abstract"]
Performance sensitive applications often benefit from the designer or administrator determining where threads are run. The Linux scheduler implements a number of scheduling policies which determine where and for how long a thread runs.

The following are the two major categories of scheduling policies:

`Normal policies`::
Normal threads are used for tasks of normal priority.

`Realtime policies`::
Realtime policies are used for time-sensitive tasks that must complete without interruptions. Realtime threads are not subject to time slicing. This means the thread runs until they block, exit, voluntarily yield, or are preempted by a higher priority thread.
+
The lowest priority realtime thread is scheduled before any thread with a normal policy. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/tuning-scheduling-policy_monitoring-and-managing-system-status-and-performance#static-priority-scheduling-with-SCHED_FIFO_tuning-scheduling-policy[Static priority scheduling with SCHED_FIFO] and link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/tuning-scheduling-policy_monitoring-and-managing-system-status-and-performance#round-robin-priority-scheduling-with-sched_rr_tuning-scheduling-policy[Round robin priority scheduling with SCHED_RR].

[role="_additional-resources"]
.Additional resources
* `sched(7)`, `sched_setaffinity(2)`, `sched_getaffinity(2)`, `sched_setscheduler(2)`, and `sched_getscheduler(2)` man pages
