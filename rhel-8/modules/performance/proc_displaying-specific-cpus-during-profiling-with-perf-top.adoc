:_mod-docs-content-type: PROCEDURE
[id="displaying-specific-cpus-during-profiling-with-perf-top_{context}"]
= Displaying specific CPUs during profiling with perf top

[role="_abstract"]
You can configure `perf top` to display specific CPUs and their relative usage while profiling your system in real time.

.Prerequisites

* You have the `perf` user space tool installed as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-perf_monitoring-and-managing-system-status-and-performance#installing-perf_getting-started-with-perf[Installing perf].


.Procedure

* Start the `perf top` interface while sorting by CPU:
+
[subs=+quotes]
----
# perf top --sort cpu
----
+
This example will list CPUs and their respective overhead in descending order of overhead usage in real time.

** You can sort by CPU and command for more detailed information of where CPU time is being spent:
+
[subs=+quotes]
----
# perf top --sort cpu,comm
----
+
This example will list commands by total overhead in descending order of overhead usage and identify the CPU the command was executed on in real time.
