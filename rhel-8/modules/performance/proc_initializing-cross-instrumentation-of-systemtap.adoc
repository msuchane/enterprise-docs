:_mod-docs-content-type: PROCEDURE
[id="initializing-cross-instrumentation-of-systemtap_{context}"]

= Initializing cross-instrumentation of SystemTap

[role="_abstract"]
Initialize cross-instrumentation of SystemTap to build SystemTap instrumentation modules from a SystemTap script on one system and use them on another system that does not have SystemTap fully deployed.

.Prerequisites
* SystemTap is installed on the _host system_ as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-systemtap_monitoring-and-managing-system-status-and-performance#installing-systemtap_getting-started-with-systemtap[Installing Systemtap].

* The `systemtap-runtime` package is installed on each _target system_:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install systemtap-runtime
----
* Both the _host system_ and _target system_ are the same architecture.

ifeval::[{ProductNumber} == 8]
* Both the _host system_ and _target system_ are running the same major version of {RHEL} (such as {RHEL} {ProductNumber}), they _can_ be running different minor versions (such as 8.1 and 8.2).
endif::[]

ifeval::[{ProductNumber} == 9]
* Both the _host system_ and _target system_ are running the same major version of {RHEL} (such as {RHEL} {ProductNumber}).
endif::[]

IMPORTANT: Kernel packaging bugs may prevent multiple `kernel-debuginfo` and `kernel-devel` packages from being installed on one system. In such cases, the minor version for the _host system_ and _target system_ must match. If a bug occurs, report it at https://bugzilla.redhat.com/.

.Procedure
. Determine the kernel running on each _target system_:
+
[subs=+quotes]
----
$ uname -r
----
+
Repeat this step for each _target system_.

. On the _host system_, install the _target kernel_ and related packages for each _target system_ by the method described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-systemtap_monitoring-and-managing-system-status-and-performance#installing-systemtap_getting-started-with-systemtap[Installing Systemtap].


. Build an instrumentation module on the _host system_, copy this module to and run this module on on the _target system_ either:

.. Using remote implementation:
+
[subs=+quotes]
----
# stap --remote _target_system_ _script_
----
+
This command remotely implements the specified script on the _target system_. You must ensure an SSH connection can be made to the _target system_ from the _host system_ for this to be successful.

.. Manually:

... Build the instrumentation module on the _host system_:
+
[subs=+quotes]
----
# stap -r _kernel_version_ _script_ -m _module_name_ -p 4
----
+
Here, _kernel_version_ refers to the version of the _target kernel_ determined in step 1, _script_ refers to the script to be converted into an _instrumentation module_, and _module_name_ is the desired name of the _instrumentation module_. The `-p4` option tells SystemTap to not load and run the compiled module.

... Once the _instrumentation module_ is compiled, copy it to the target system and load it using the following command:
+
[subs=+quotes]
----
# staprun _module_name_.ko
----
