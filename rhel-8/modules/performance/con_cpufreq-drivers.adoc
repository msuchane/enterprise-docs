// included in assembly tuning-cpu-frequency-to-optimize-energy-consumption
:_mod-docs-content-type: CONCEPT

[id="cpufreq-drivers_{context}"]
= CPUfreq drivers

[role="_abstract"]
Using the `cpupower frequency-info --driver` command as root, you can view the current CPUfreq driver.

The following are the two available drivers for CPUfreq that can be used:

`ACPI CPUfreq`::
Advanced Configuration and Power Interface (ACPI) CPUfreq driver is a kernel driver that controls the frequency of a particular CPU through ACPI, which ensures the communication between the kernel and the hardware.

`Intel P-state`::
In {RHEL} 8, Intel P-state driver is supported. The driver provides an interface for controlling the P-state selection on processors based on the Intel Xeon E series architecture or newer architectures.
+
Currently, Intel P-state is used by default for supported CPUs. You can switch to using ACPI CPUfreq by adding the `intel_pstate=disable` command to the kernel command line.
+
Intel P-state implements the `setpolicy()` callback. The driver decides what P-state to use based on the policy requested from the `cpufreq` core. If the processor is capable of selecting its next P-state internally, the driver offloads this responsibility to the processor. If not, the driver implements algorithms to select the next P-state.
+
Intel P-state provides its own `sysfs` files to control the P-state selection. These files are located in the `/sys/devices/system/cpu/intel_pstate/` directory. Any changes made to the files are applicable to all CPUs.
+
This directory contains the following files that are used for setting P-state parameters:

* `max_perf_pct` limits the maximum P-state requested by the driver expressed in a percentage of available performance. The available P-state performance can be reduced by the `no_turbo` setting.

* `min_perf_pct` limits the minimum P-state requested by the driver, expressed in a percentage of the maximum `no-turbo` performance level.

* `no_turbo` limits the driver to selecting P-state below the turbo frequency range.

* `turbo_pct` displays the percentage of the total performance supported by hardware that is in the turbo range. This number is independent of whether `turbo` has been disabled or not.

* `num_pstates` displays the number of P-states that are supported by hardware. This number is independent of whether turbo has been disabled or not.


[role="_additional-resources"]
.Additional resources
* `cpupower-frequency-info(1)` man page
