// Module included in the following assemblies:
//configuring-an-operating-system-to-optimize-cpu-utilization
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="configuring-kernel-tick-time_{context}"]
= Configuring kernel tick time

[role="_abstract"]
ifeval::[{ProductNumber} == 8]
By default, {RHEL8} uses a tickless kernel, which does not interrupt idle CPUs in order to reduce power usage and allow new processors to take advantage of deep sleep states.

{RHEL8} also offers a dynamic tickless option, which is useful for latency-sensitive workloads, such as high performance computing or realtime computing. By default, the dynamic tickless option is disabled. {RH} recommends using the `cpu-partitioning` *TuneD* profile to enable the dynamic tickless option for cores specified as `isolated_cores`.
endif::[]

ifeval::[{ProductNumber} == 9]
By default, {RHEL9} uses a tickless kernel, which does not interrupt idle CPUs in order to reduce power usage and allow new processors to take advantage of deep sleep states.

{RHEL9} also offers a dynamic tickless option, which is useful for latency-sensitive workloads, such as high performance computing or realtime computing. By default, the dynamic tickless option is disabled. {RH} recommends using the `cpu-partitioning` *TuneD* profile to enable the dynamic tickless option for cores specified as `isolated_cores`.
endif::[]

This procedure describes how to manually persistently enable dynamic tickless behavior.


.Procedure

. To enable dynamic tickless behavior in certain cores, specify those cores on the kernel command line with the `nohz_full` parameter. On a 16 core system, enable the `nohz_full=1-15` kernel option:
+
----
# grubby --update-kernel=ALL --args="nohz_full=1-15"
----
+
This enables dynamic tickless behavior on cores `1` through `15`, moving all timekeeping to the only unspecified core (core `0`).

. When the system boots, manually move the `rcu` threads to the non-latency-sensitive core, in this case core `0`:
+
----
# for i in `pgrep rcu[^c]` ; do taskset -pc 0 $i ; done
----

. Optional: Use the `isolcpus` parameter on the kernel command line to isolate certain cores from user-space tasks.

. Optional: Set the CPU affinity for the kernel's `write-back bdi-flush` threads to the housekeeping core:
+
----
echo 1 > /sys/bus/workqueue/devices/writeback/cpumask
----

.Verification steps

* Once the system is rebooted, verify if `dynticks` are enabled:
+
----
# journalctl -xe | grep dynticks
Mar 15 18:34:54 rhel-server kernel: NO_HZ: Full dynticks CPUs: 1-15.
----

* Verify that the dynamic tickless configuration is working correctly:
+
----
# perf stat -C 1 -e irq_vectors:local_timer_entry taskset -c 1 sleep 3
----
+
This command measures ticks on CPU 1 while telling CPU 1 to sleep for 3 seconds.

* The default kernel timer configuration shows around 3100 ticks on a regular CPU:
+
----
# perf stat -C 0 -e irq_vectors:local_timer_entry taskset -c 0 sleep 3

 Performance counter stats for 'CPU(s) 0':

             3,107      irq_vectors:local_timer_entry

       3.001342790 seconds time elapsed
----

* With the dynamic tickless kernel configured, you should see around 4 ticks instead:
+
----
# perf stat -C 1 -e irq_vectors:local_timer_entry taskset -c 1 sleep 3

 Performance counter stats for 'CPU(s) 1':

                 4      irq_vectors:local_timer_entry

       3.001544078 seconds time elapsed
----

[role="_additional-resources"]
.Additional resources
* `perf(1)` and `cpuset(7)` man pages
* link:https://access.redhat.com/solutions/2273531[All about nohz_full kernel parameter Red Hat Knowledgebase article]
* link:https://access.redhat.com/solutions/3875421[How to verify the list of "isolated" and "nohz_full" CPU information from sysfs? Red Hat Knowledgebase article]
