// Module included in the following assemblies:
//setting-up-graphical-representation-of-pcp-metrics
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="installing-pcp-bpftrace_{context}"]
= Installing PCP bpftrace

[role="_abstract"]
Install the PCP `bpftrace` agent to introspect a system and to gather metrics from the kernel and user-space tracepoints.

The `bpftrace` agent uses bpftrace scripts to gather the metrics. The `bpftrace` scripts use the enhanced Berkeley Packet Filter (`eBPF`).

This procedure describes how to install a `pcp bpftrace`.

.Prerequisites

. PCP is configured. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#setting-up-pcp-with-pcp-zeroconf_setting-up-graphical-representation-of-pcp-metrics[Setting up PCP with pcp-zeroconf].

. The `grafana-server` is configured. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#setting-up-a-grafana-server_setting-up-graphical-representation-of-pcp-metrics[Setting up a grafana-server].

. The `scram-sha-256` authentication mechanism is configured. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#setting-up-authentication-between-pcp-components_setting-up-graphical-representation-of-pcp-metrics[Setting up authentication between PCP components].

.Procedure

. Install the `pcp-pmda-bpftrace` package:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install pcp-pmda-bpftrace
----

. Edit the `bpftrace.conf` file and add the user that you have created in the {setting-up-authentication-between-pcp-components}:
+
[subs=quotes]
----
# vi /var/lib/pcp/pmdas/bpftrace/bpftrace.conf

[dynamic_scripts]
enabled = true
auth_enabled = true
allowed_users = root,_metrics_
----
+
Replace _metrics_ by your user name.

. Install `bpftrace` PMDA:
+
----
# cd /var/lib/pcp/pmdas/bpftrace/
# ./Install
Updating the Performance Metrics Name Space (PMNS) ...
Terminate PMDA if already installed ...
Updating the PMCD control file, and notifying PMCD ...
Check bpftrace metrics have appeared ... 7 metrics and 6 values
----
+
The `pmda-bpftrace` is now installed, and can only be used after authenticating your user. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#viewing-the-pcp-bpftrace-system-analysis-dashboard_setting-up-graphical-representation-of-pcp-metrics[Viewing the PCP bpftrace System Analysis dashboard].


[role="_additional-resources"]
.Additional resources
* `pmdabpftrace(1)` and `bpftrace` man pages
