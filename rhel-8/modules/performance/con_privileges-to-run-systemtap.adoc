:_mod-docs-content-type: CONCEPT
[id="privileges-to-run-systemtap_{context}"]
= Privileges to run SystemTap

[role="_abstract"]
Running SystemTap scripts requires elevated system privileges but, in some instances, non-privileged users might need to run SystemTap instrumentation on their machine.

To allow users to run SystemTap without root access, add users to *both* of these user groups:

`stapdev`::
Members of this group can use `stap` to run SystemTap scripts, or `staprun` to run SystemTap instrumentation modules.
+
Running `stap` involves compiling SystemTap scripts into kernel modules and loading them into the kernel. This requires elevated privileges to the system, which are granted to `stapdev` members. Unfortunately, such privileges also grant effective root access to `stapdev` members. As such, only grant `stapdev` group membership to users who can be trusted with root access.

`stapusr`::
Members of this group can only use `staprun` to run SystemTap instrumentation modules. In addition, they can only run those modules from the `/lib/modules/_kernel_version_/systemtap/` directory. This directory must be owned only by the root user, and must only be writable by the root user.
