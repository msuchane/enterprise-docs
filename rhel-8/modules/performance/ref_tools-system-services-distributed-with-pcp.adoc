:_mod-docs-content-type: REFERENCE
[id="system-services-distributed-with-pcp_{context}"]
= System services and tools distributed with PCP

[role="_abstract"]
Performance Co-Pilot (PCP) includes various system services and tools you can use for measuring performance. The basic package `pcp` includes the system services and basic tools. Additional tools are provided with the `pcp-system-tools`, `pcp-gui`, and `pcp-devel` packages.

.Roles of system services distributed with PCP
`pmcd`:: The Performance Metric Collector Daemon (PMCD).
`pmie`:: The Performance Metrics Inference Engine.
`pmlogger`:: The performance metrics logger.
`pmproxy`:: The realtime and historical performance metrics proxy, time series query and REST API service.

.Tools distributed with base PCP package
`pcp`:: Displays the current status of a Performance Co-Pilot installation.
`pcp-vmstat`:: Provides a high-level system performance overview every 5 seconds. Displays information about processes, memory, paging, block IO, traps, and CPU activity.
`pmconfig`::  Displays the values of configuration parameters.
`pmdiff`::  Compares the average values for every metric in either one or two archives, in a given time window, for changes that are likely to be of interest when searching for performance regressions.
`pmdumplog`::  Displays control, metadata, index, and state information from a Performance Co-Pilot archive file.
`pmfind`:: Finds PCP services on the network.
`pmie`:: An inference engine that periodically evaluates a set of arithmetic, logical, and rule expressions. The metrics are collected either from a live system, or from a Performance Co-Pilot archive file.
`pmieconf`:: 	Displays or sets configurable `pmie` variables.
`pmiectl`:: Manages non-primary instances of `pmie`.
`pminfo`:: Displays information about performance metrics. The metrics are collected either from a live system, or from a Performance Co-Pilot archive file.
`pmlc`:: Interactively configures active `pmlogger` instances.
`pmlogcheck`:: Identifies invalid data in a Performance Co-Pilot archive file.
`pmlogconf`:: Creates and modifies a `pmlogger` configuration file.
`pmlogctl`:: Manages non-primary instances of `pmlogger`.
`pmloglabel`:: Verifies, modifies, or repairs the label of a Performance Co-Pilot archive file.
`pmlogsummary`:: Calculates statistical information about performance metrics stored in a Performance Co-Pilot archive file.
`pmprobe`:: Determines the availability of performance metrics.
`pmsocks`:: Allows access to a Performance Co-Pilot hosts through a firewall.
`pmstat`::	Periodically displays a brief summary of system performance.
`pmstore`:: Modifies the values of performance metrics.
`pmtrace`:: Provides a command line interface to the trace PMDA.
`pmval`:: Displays the current value of a performance metric.

.Tools distributed with the separately installed `pcp-system-tools` package
`pcp-atop`:: Shows the system-level occupation of the most critical hardware resources from the performance point of view: CPU, memory, disk, and network.
`pcp-atopsar`:: Generates a system-level activity report over a variety of system resource utilization. The report is generated from a raw logfile previously recorded using `pmlogger` or the `-w` option of `pcp-atop`.
`pcp-dmcache`:: Displays information about configured Device Mapper Cache targets, such as: device IOPs, cache and metadata device utilization, as well as hit and miss rates and ratios for both reads and writes for each cache device.
`pcp-dstat`:: Displays metrics of one system at a time. To display metrics of multiple systems, use `--host` option.
`pcp-free`:: Reports on free and used memory in a system.
`pcp-htop`:: Displays all processes running on a system along with their command line arguments in a manner similar to the `top` command, but allows you to scroll vertically and horizontally as well as interact using a mouse. You can also view processes in a tree format and select and act on multiple processes at once.
`pcp-ipcs`:: Displays information about the inter-process communication (IPC) facilities that the calling process has read access for.
`pcp-mpstat`:: Reports CPU and interrupt-related statistics.
`pcp-numastat`:: Displays NUMA allocation statistics from the kernel memory allocator.
`pcp-pidstat`:: Displays information about individual tasks or processes running on the system, such as CPU percentage, memory and stack usage, scheduling, and priority. Reports live data for the local host by default.
`pcp-shping`:: Samples and reports on the shell-ping service metrics exported by the `pmdashping` Performance Metrics Domain Agent (PMDA).
`pcp-ss`:: Displays socket statistics collected by the `pmdasockets` PMDA.
`pcp-tapestat`:: Reports I/O statistics for tape devices.
`pcp-uptime`:: Displays how long the system has been running, how many users are currently logged on, and the system load averages for the past 1, 5, and 15 minutes.
`pcp-verify`:: Inspects various aspects of a Performance Co-Pilot collector installation and reports on whether it is configured correctly for certain modes of operation.
`pmiostat`:: Reports I/O statistics for SCSI devices (by default) or device-mapper devices (with the `-x` device-mapper option).
`pmrep`:: Reports on selected, easily customizable, performance metrics values.

.Tools distributed with the separately installed `pcp-gui` package
`pmchart`::  Plots performance metrics values available through the facilities of the Performance Co-Pilot.
`pmdumptext`::  Outputs the values of performance metrics collected live or from a Performance Co-Pilot archive.

.Tools distributed with the separately installed `pcp-devel` package
`pmclient`::  Displays high-level system performance metrics by using the Performance Metrics Application Programming Interface (PMAPI).
`pmdbg`:: Displays available Performance Co-Pilot debug control flags and their values.
`pmerr`:: Displays available Performance Co-Pilot error codes and their corresponding error messages.
