:_mod-docs-content-type: CONCEPT
[id="what-is-gcc-toolset_{context}"]
= What is GCC Toolset

ifeval::[{ProductNumber} == 8]
Red Hat Enterprise Linux 8 introduces {gcct}, an Application Stream containing more up-to-date versions of development and performance analysis tools. {gcct} is similar to link:https://access.redhat.com/documentation/en-us/red_hat_developer_toolset/[Red Hat Developer Toolset] for RHEL 7.
endif::[]

ifeval::[{ProductNumber} == 9]
Red Hat Enterprise Linux 9 continues support for {gcct}, an Application Stream containing more up-to-date versions of development and performance analysis tools. {gcct} is similar to link:https://access.redhat.com/documentation/en-us/red_hat_developer_toolset/[Red Hat Developer Toolset] for RHEL 7.
endif::[]

{gcct} is available as an Application Stream in the form of a software collection in the `AppStream` repository. {gcct} is fully supported under Red Hat Enterprise Linux Subscription Level Agreements, is functionally complete, and is intended for production use. Applications and libraries provided by {gcct} do not replace the Red Hat Enterprise Linux system versions, do not override them, and do not automatically become default or preferred choices. Using a framework called software collections, an additional set of developer tools is installed into the `/opt/` directory and is explicitly enabled by the user on demand using the `scl` utility.  Unless noted otherwise for specific tools or features, {gcct} is available for all architectures supported by {RHEL}.
