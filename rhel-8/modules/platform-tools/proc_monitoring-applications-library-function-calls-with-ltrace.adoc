:_mod-docs-content-type: PROCEDURE

:experimental:

[id="monitoring-applications-library-function-calls-with-ltrace_{context}"]
= Monitoring application's library function calls with ltrace

ifdef::internal[]
Story: __As a devops person, I want to see what my app is doing without full-out debugging__ +
SMEs: _dj?_ +
Revised by: _mcermak_ +
Peer review: _asteflova_ +
Module type: _procedure_ +
Components: _ltrace_ +
endif::internal[]

The [command]`ltrace` tool enables monitoring an application's calls to functions available in libraries (shared objects).

NOTE: In {ProductName} {ProductNumber}, a known issue prevents [command]`ltrace` from tracing system executable files. This limitation does not apply to executable files built by users.

.Prerequisites

ifdef::devguide[* xref:setting-up-to-debug-applications_setting-up-a-development-workstation[You must have [command]`ltrace` installed on the system.]]
ifndef::devguide[* You must have [command]`ltrace` installed on the system.]

.Procedure

. Identify the libraries and functions of interest, if possible.

. Start `ltrace` and attach it to the program.
+
====
* If the program you want to monitor is not running, start [command]`ltrace` and specify __program__:
+
[subs="quotes,attributes,macros"]
----
$ ltrace -f -l __library__ -e __function__ __program__
----

* If the program is already running, find its process id (__pid__) and attach [command]`ltrace` to it:
+
[subs="quotes,attributes,macros"]
----
$ ps -C __program__
__(...)__
$ ltrace -f -l __library__ -e __function__ __program__ -p__pid__
----

* Use the [option]`-e`, [option]`-f` and [option]`-l` options to filter the output:
+
======
* Supply the function names to be displayed as __function__. The [option]`-e __function__` option can be used multiple times. If left out, [command]`ltrace` displays calls to all functions.
* Instead of specifying functions, you can specify whole libraries with the [option]`-l __library__` option. This option behaves similarly to the [option]`-e __function__` option.
* If you do not want to trace any forked processes or threads, leave out the [option]`-f` option.
======
+
See the _ltrace_(1)_ manual page for more information.
====

. [command]`ltrace` displays the library calls made by the application.
+
In most cases, an application makes a large number of calls and [command]`ltrace` output displays immediately, if no filter is set.

. [command]`ltrace` exits when the program exits.
+
To terminate the monitoring before the traced program exits, press btn:[ctrl+C].
+
* If [command]`ltrace` started the program, the program terminates together with [command]`ltrace`.
* If you attached [command]`ltrace` to an already running program, the program terminates together with [command]`ltrace`.

. Analyze the list of library calls done by the application.
+
* If the application crashes, the important information is probably at the end of log.
* The output contains a lot of unnecessary information. However, you can construct a more precise filter and repeat the procedure.

[NOTE]
====
It is advantageous to both see the output and save it to a file. Use the [command]`tee` command to achieve this:

[subs="quotes,attributes,macros"]
----
$ ltrace ... |&amp; tee __your_log_file.log__
----
// ltrace -o hides the terminal output, so "nope"
// note also trace output is on stderr, therefore |&
====


[role="_additional-resources"]
.Additional resources
* The _ltrace(1)_ manual page:
+
----
$ man ltrace
----

* {dts} User Guide &mdash; link:https://access.redhat.com/documentation/en-us/red_hat_developer_toolset/{dts-ver}/html-single/user_guide/index#chap-ltrace[Chapter ltrace]

// there is no upstream docs!
