:_mod-docs-content-type: REFERENCE

[id="ref_specifics-of-annobin-in-gcc-toolset-12_{context}"]
= Specifics of annobin in GCC Toolset 12
//

Under some circumstances, due to a synchronization issue between `annobin` and `gcc` in {gcct} 12, your compilation can fail with an error message that looks similar to the following:

[literal,subs="quotes"]
----
cc1: fatal error: inaccessible plugin file
opt/rh/gcc-toolset-12/root/usr/lib/gcc/_architecture_-linux-gnu/12/plugin/gcc-annobin.so
expanded from short plugin name gcc-annobin: No such file or directory
----

To work around the problem, create a symbolic link in the plugin directory from the `annobin.so` file to the `gcc-annobin.so` file:

[literal,subs="quotes"]
----
# cd /opt/rh/gcc-toolset-12/root/usr/lib/gcc/_architecture_-linux-gnu/12/plugin
# ln -s annobin.so gcc-annobin.so
----

Replace _architecture_ with the architecture you use in your system:

* `aarch64`
* `i686`
* `ppc64le`
* `s390x`
* `x86_64`
