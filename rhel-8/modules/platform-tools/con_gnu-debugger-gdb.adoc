:_mod-docs-content-type: CONCEPT
[id="gnu-debugger-gdb_{context}"]
= GNU debugger (GDB)
// itowey suggested: GDB for debugging at the command line

ifdef::internal[]
Story: __As a developer, I need to know what to expect from GDB as opposed to GUI debuggers__ +
SMEs: _pmuldoon, jkratoch, fkrska_ +
Revised by: _pmuldoon_ +
Peer review: _asteflova, msuchane_ +
Module type: _concept_ +
Components: _gdb_ +
endif::internal[]

{ProductName} contains the GNU debugger (GDB) which lets you investigate what is happening inside a program through a command-line user interface.

//For a graphical front end to GDB, install the Eclipse integrated development environment. See link:https://access.redhat.com/documentation/en-us/red_hat_developer_tools/2018.1/html/using_eclipse/[Using Eclipse].

.GDB capabilities

A single GDB session can debug the following types of programs:

* Multithreaded and forking programs
* Multiple programs at once
* Programs on remote machines or in containers with the [command]`gdbserver` utility connected over a TCP/IP network connection

.Debugging requirements

To debug any executable code, GDB requires debugging information for that particular code:

* For programs developed by you, you can create the debugging information while building the code.
* For system programs installed from packages, you must install their debuginfo packages.
