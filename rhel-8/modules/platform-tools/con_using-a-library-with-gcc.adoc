:_mod-docs-content-type: CONCEPT
[id="using-a-library-with-gcc_{context}"]
= Using a library with GCC

ifdef::internal[]
Story: __As a developer, I want to reuse some functionality - use a library in my program__ +
SMEs: _mpolacek, ptisnovs_ +
Revised by: _ptisnovs_ +
Peer review: _bfallonf_ +
Module type: _concept_ +
Components: _gcc_ +
endif::internal[]

A library is a package of code which can be reused in your program. A C or {cpp} library consists of two parts:

* The library code
* Header files

.Compiling code that uses a library

The header files describe the interface of the library: the functions and variables available in the library. Information from the header files is needed for compiling the code.

Typically, header files of a library will be placed in a different directory than your application's code. To tell GCC where the header files are, use the [option]`-I` option:

[subs="quotes,macros"]
----
$ gcc ... -I__include_path__ ...
----

Replace __include_path__ with the actual path to the header file directory.

////
For example, to specify a relative path [filename]`some/interesting/directory`:
----
$ gcc ... -Isome/interesting/directory ...
----
////

The [option]`-I` option can be used multiple times to add multiple directories with header files. When looking for a header file, these directories are searched in the order of their appearance in the [option]`-I` options.

.Linking code that uses a library

When linking the executable file, both the object code of your application and the binary code of the library must be available. The code for static and dynamic libraries is present in different forms:

* Static libraries are available as archive files. They contain a group of object files. The archive file has a file name extension `.a`.
* Dynamic libraries are available as shared objects. They are a form of an executable file. A shared object has a file name extension `.so`.

To tell GCC where the archives or shared object files of a library are, use the [option]`-L` option:

[subs="quotes,macros"]
----
$ gcc ... -L__library_path__ -l__foo__ ...
----

Replace __library_path__ with the actual path to the library directory.

The [option]`-L` option can be used multiple times to add multiple directories. When looking for a library, these directories are searched in the order of their [option]`-L` options.

The order of options matters: GCC cannot link against a library *foo* unless it knows the directory with this library. Therefore, use the [option]`-L` options to specify library directories before using the [option]`-l` options for linking against libraries.

.Compiling and linking code which uses a library in one step

When the situation allows the code to be compiled and linked in one [command]`gcc` command, use the options for both situations mentioned above at once.

[role="_additional-resources"]
.Additional resources
* Using the GNU Compiler Collection (GCC) &mdash; link:https://gcc.gnu.org/onlinedocs/gcc/Directory-Options.html[Options for Directory Search]

* Using the GNU Compiler Collection (GCC) &mdash; link:https://gcc.gnu.org/onlinedocs/gcc/Link-Options.html[Options for Linking]
