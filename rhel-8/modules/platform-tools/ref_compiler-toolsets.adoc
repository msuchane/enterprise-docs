:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_compiler-toolsets.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
[id="compiler-toolsets_{context}"]
= Compiler toolsets

RHEL {ProductNumber} provides the following compiler toolsets as Application Streams:

* LLVM Toolset provides the LLVM compiler infrastructure framework, the Clang compiler for the C and C++ languages, the LLDB debugger, and related tools for code analysis.

* Rust Toolset provides the Rust programming language compiler `rustc`, the `cargo` build tool and dependency manager, the `cargo-vendor` plugin, and required libraries.

* Go Toolset provides the Go programming language tools and libraries. Go is alternatively known as `golang`.

For more details and information about usage, see the compiler toolsets user guides on the link:https://access.redhat.com/documentation/en-us/red_hat_developer_tools/1[Red Hat Developer Tools] page.
