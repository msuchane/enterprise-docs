:_mod-docs-content-type: CONCEPT
[id="gnu-make-and-makefile-overview_{context}"]
= GNU [command]`make` and `Makefile` overview

ifdef::internal[]
Story: _As a developer, I want to capture and automate the complex set of rules for building my project_ +
SMEs: _pfrankli_ +
Revised by: _pfrankli_ +
Peer review: _samccann_ +
Module type: _concept_ +
Components: _make_ +
endif::internal[]

To create a usable form (usually executable files) from the source files of a particular project, perform several necessary steps. Record the actions and their sequence to be able to repeat them later.

{ProductName} contains GNU [command]`make`, a build system designed for this purpose.

.Prerequisites

* Understanding the concepts of compiling and linking

.GNU [command]`make`

GNU [command]`make` reads Makefiles which contain the instructions describing the build process. A Makefile contains multiple _rules_ that describe a way to satisfy a certain condition (_target_) with a specific action (_recipe_). Rules can hierarchically depend on another rule.

Running [command]`make` without any options makes it look for a Makefile in the current directory and attempt to reach the default target. The actual Makefile file name can be one of [filename]`Makefile`, [filename]`makefile`, and [filename]`GNUmakefile`. The default target is determined from the Makefile contents.

////
To run [command]`make` with a specific target:

[subs="quotes,attributes"]
----
$ make __target__
----
////

// NOTE: Makefile is a static hierarchy of recipes without flexibility. If the project can be built in a large number of different ways, typically depending on the individual system configuration, another tool creating Makefiles is the preferred way to handle this variability. The *GNU autotools* project, available on {ProductName}, offers this function.

.Makefile details

Makefiles use a relatively simple syntax for defining _variables_ and _rules_, which consists of a _target_ and a _recipe_. The target specifies what is the output if a rule is executed. The lines with recipes must start with the TAB character.

Typically, a Makefile contains rules for compiling source files, a rule for linking the resulting object files, and a target that serves as the entry point at the top of the hierarchy.

Consider the following [filename]`Makefile` for building a C program which consists of a single file, [filename]`hello.c`.

[source,make]
----
all: hello

hello: hello.o
        gcc hello.o -o hello

hello.o: hello.c
        gcc -c hello.c -o hello.o
----

This example shows that to reach the target `all`, file [filename]`hello` is required. To get [filename]`hello`, one needs [filename]`hello.o` (linked by [command]`gcc`), which in turn is created from [filename]`hello.c` (compiled by [command]`gcc`).

The target `all` is the default target because it is the first target that does not start with a period (.). Running [command]`make` without any arguments is then identical to running [command]`make all`, when the current directory contains this [filename]`Makefile`.

.Typical makefile

A more typical Makefile uses variables for generalization of the steps and adds a target "clean" - remove everything but the source files.

[source,make]
----
CC=gcc
CFLAGS=-c -Wall
SOURCE=hello.c
OBJ=$(SOURCE:.c=.o)
EXE=hello

all: $(SOURCE) $(EXE)

$(EXE): $(OBJ)
        $(CC) $(OBJ) -o $@

%.o: %.c
        $(CC) $(CFLAGS) $< -o $@

clean:
        rm -rf $(OBJ) $(EXE)
----

Adding more source files to such Makefile requires only adding them to the line where the SOURCE variable is defined.

// compiling single source to single object file as a building block of the build ?

[role="_additional-resources"]
.Additional resources
* GNU make: Introduction &mdash; link:++https://www.gnu.org/software/make/manual/html_node/Introduction.html#Introduction++[2 An Introduction to Makefiles]

ifdef::devguide[* xref:building-code-with-gcc_creating-c-or-cpp-applications[]]
