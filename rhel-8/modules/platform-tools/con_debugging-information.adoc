:_mod-docs-content-type: CONCEPT
[id="debugging-information_{context}"]
= Debugging information

ifdef::internal[]
Story: __As a total noob, I need to be told what is debugging information__ +
SMEs: _mpolacek, mjw_ +
Revised by: _mjw_ +
Peer review: - +
Module type: _concept_ +
Components: _gcc, binutils, elfutils_ +
endif::internal[]

While debugging any executable code, two types of information allow the tools, and by extension the programmer, to comprehend the binary code:

* the source code text
* a description of how the source code text relates to the binary code

Such information is called debugging information.

{ProductName} uses the ELF format for executable binaries, shared libraries, or `debuginfo` files. Within these ELF files, the DWARF format is used to hold the debug information.

To display DWARF information stored within an ELF file, run the [command]`readelf -w __file__` command.

[IMPORTANT]
STABS is an older, less capable format, occasionally used with UNIX. Its use is discouraged by Red Hat. GCC and GDB provide STABS production and consumption on a best effort basis only. Some other tools such as Valgrind and `elfutils` do not work with STABS.


[role="_additional-resources"]
.Additional resources
* link:http://www.dwarfstd.org/[The DWARF Debugging Standard]
