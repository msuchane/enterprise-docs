:_mod-docs-content-type: REFERENCE
[id="cpp-compatibility-in-gcc-toolset-10_{context}"]
= {cpp} compatibility in {gcct} {gcct-ver}

IMPORTANT: The compatibility information presented here apply only to the GCC from {gcct} {gcct-ver}.

The GCC compiler in {gcct} can use the following {cpp} standards:

{cpp}14::
This is the *default* language standard setting for {gcct} {gcct-ver}, with GNU extensions, equivalent to explicitly using option `-std=gnu++14`.
+
Using the {cpp}14 language version is supported when all {cpp} objects compiled with the respective flag have been built using GCC version 6 or later.

{cpp}11::
This language standard is available in {gcct} {gcct-ver}.
+
Using the {cpp}11 language version is supported when all {cpp} objects compiled with the respective flag have been built using GCC version 5 or later.

{cpp}98::
This language standard is available in {gcct} {gcct-ver}. Binaries, shared libraries and objects built using this standard can be freely mixed regardless of being built with GCC from {gcct}, Red Hat Developer Toolset, and RHEL 5, 6, 7 and 8.

{cpp}17::
This language standard is available in {gcct} {gcct-ver}.

{cpp}20::
This language standard is available in {gcct} {gcct-ver} only as an experimental, unstable, and unsupported capability. Additionally, compatibility of objects, binary files, and libraries built using this standard cannot be guaranteed.

All of the language standards are available in both the standard compliant variant or with GNU extensions.

When mixing objects built with {gcct} with those built with the RHEL toolchain (particularly `.o` or `.a` files), {gcct} toolchain should be used for any linkage. This ensures any newer library features provided only by {gcct} are resolved at link time.

