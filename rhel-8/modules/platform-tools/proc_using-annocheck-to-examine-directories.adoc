:_mod-docs-content-type: PROCEDURE
[id="using-annocheck-to-examine-directories_{context}"]

= Using annocheck to examine directories

The following section describes how to examine ELF files in a directory using `annocheck`.

.Procedure

* To scan a directory, use:
+
[subs=+quotes]
----
$ annocheck _directory-name_
----
Replace _directory-name_ with the name of a directory. `annocheck` automatically examines the contents of the directory, its sub-directories, and any archives and RPM packages within the directory.

NOTE: `annocheck` only looks for ELF files. Other file types are ignored.

.Additional information

* For more information about `annocheck` and possible command line options, see the `annocheck` man page.
