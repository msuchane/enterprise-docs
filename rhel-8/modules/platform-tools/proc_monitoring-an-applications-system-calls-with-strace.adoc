:_mod-docs-content-type: PROCEDURE

:experimental:

[id="monitoring-an-applications-system-calls-with-strace_{context}"]
= Monitoring an application's system calls with strace

ifdef::internal[]
Story: __As a devops person, I want to see what my app is doing without full-out debugging__ +
SMEs: _dj, fkrska, esyr_ +
Revised by: _fkrska_ +
Peer review: _asteflova_ +
Module type: _procedure_ +
Components: _strace_ +
endif::internal[]


The [command]`strace` tool enables monitoring the system (kernel) calls performed by an application.

.Prerequisites

ifdef::devguide[* xref:setting-up-to-debug-applications_setting-up-a-development-workstation[You must have [command]`strace` installed on the system.]]
ifndef::devguide[* You must have [command]`strace` installed on the system.]

.Procedure

. Identify the system calls to monitor.

. Start `strace` and attach it to the program.
+
====
* If the program you want to monitor is not running, start [command]`strace` and specify the __program__:
+
[subs="quotes,attributes,macros"]
----
$ strace -fvttTyy -s 256 -e trace=__call__ __program__
----

* If the program is already running, find its process id (__pid__) and attach `strace` to it:
+
[subs="quotes,attributes,macros"]
----
$ ps -C __program__
__(...)__
$ strace -fvttTyy -s 256 -e trace=__call__ -p__pid__
----

* Replace __call__ with the system calls to be displayed. You can use the [option]`-e trace=__call__` option multiple times. If left out, [command]`strace` will display all system call types. See the _strace(1)_ manual page for more information.

* If you do not want to trace any forked processes or threads, leave out the [option]`-f` option.
====

. The [command]`strace` tool displays the system calls made by the application and their details.
+
In most cases, an application and its libraries make a large number of calls and [command]`strace` output appears immediately, if no filter for system calls is set.

. The [command]`strace` tool exits when the program exits.
+
To terminate the monitoring before the traced program exits, press btn:[Ctrl+C].
+
* If [command]`strace` started the program, the program terminates together with [command]`strace`.
* If you attached [command]`strace` to an already running program, the program terminates together with [command]`strace`.

. Analyze the list of system calls done by the application.
+
* Problems with resource access or availability are present in the log as calls returning errors.
* Values passed to the system calls and patterns of call sequences provide insight into the causes of the application's behaviour.
* If the application crashes, the important information is probably at the end of log.
* The output contains a lot of unnecessary information. However, you can construct a more precise filter for the system calls of interest and repeat the procedure.

[NOTE]
====
It is advantageous to both see the output and save it to a file. Use the [command]`tee` command to achieve this:

[subs="quotes,attributes,macros"]
----
$ strace ... |&amp; tee __your_log_file.log__
----
// strace -o hides the terminal output, so "nope"
// note also trace output is on stderr, therefore |&
====


[role="_additional-resources"]
.Additional resources
* The _strace(1)_ manual page:
+
----
$ man strace
----

* link:https://access.redhat.com/articles/2483[How do I use strace to trace system calls made by a command?] &mdash; Knowledgebase article

* {rhdts} User Guide &mdash; link:https://access.redhat.com/documentation/en-us/red_hat_developer_toolset/{dts-ver}/html-single/user_guide/index#chap-strace[Chapter strace]
