:_mod-docs-content-type: PROCEDURE
[id="dumping-protected-process-memory-with-gdb_{context}"]
= Dumping protected process memory with GDB

ifdef::internal[]
Story: __As a devops, I want to dump a core (memory) of a process with everything__ +
SMEs: _fkrska, pmuldoon, jkratoch_ +
Revised by: _jkratoch (7.5 RN bug), pmuldoon_ +
Peer review: _asteflova_ +
Module type: _procedure_ +
Components: _gdb_ +
endif::internal[]

// based on https://bugzilla.redhat.com/show_bug.cgi?id=1518243

You can mark the memory of processes as not to be dumped. This can save resources and ensure additional security when the process memory contains sensitive data: for example, in banking or accounting applications or on whole virtual machines. Both kernel core dumps (`kdump`) and manual core dumps ([command]`gcore`, GDB) do not dump memory marked this way.

In some cases, you must dump the whole contents of the process memory regardless of these protections. This procedure shows how to do this using the GDB debugger.


.Prerequisites

ifdef::devguide[]
* xref:core-dumps_debugging-a-crashed-application[You must understand what core dumps are.]
* xref:setting-up-to-debug-applications_setting-up-a-development-workstation[GDB must be installed on the system.]
* xref:attaching-gdb-to-a-process_inspecting-application-internal-state-with-gdb[GDB must be already attached to the process with protected memory.]
endif::[]
ifndef::devguide[]
* You must understand what core dumps are.
* GDB must be installed on the system.
* GDB must be already attached to the process with protected memory.
endif::[]


.Procedure

. Set GDB to ignore the settings in the [filename]`/proc/PID/coredump_filter` file:
+
----
(gdb) set use-coredump-filter off
----

. Set GDB to ignore the memory page flag `VM_DONTDUMP`:
+
----
(gdb) set dump-excluded-mappings on
----

. Dump the memory:
+
[subs=quotes,macros]
----
(gdb) gcore __core-file__
----
+
Replace __core-file__ with name of file where you want to dump the memory.

[role="_additional-resources"]
.Additional resources
* Debugging with GDB - link:https://sourceware.org/gdb/onlinedocs/gdb/Core-File-Generation.html[How to Produce a Core File from Your Program]

