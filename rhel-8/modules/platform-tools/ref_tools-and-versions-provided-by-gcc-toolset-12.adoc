:_mod-docs-content-type: REFERENCE
[id="tools-and-versions-provided-by-gcc-toolset-12_{context}"]
= Tools and versions provided by {gcct} 12

{gcct} 12 provides the following tools and versions:

.Tool versions in {gcct} 12
[options="header",cols="3,2,10"]
|====
|Name | Version | Description
| GCC | 12.1.1 | A portable compiler suite with support for C, {cpp}, and Fortran.
| GDB | 11.2 | A command-line debugger for programs written in C, {cpp}, and Fortran.
| binutils | 2.38 | A collection of binary tools and other utilities to inspect and manipulate object files and binaries.
| dwz | 0.14 | A tool to optimize DWARF debugging information contained in ELF shared libraries and ELF executables for size.
| annobin | 10.76 | A build security checking tool.
|====
