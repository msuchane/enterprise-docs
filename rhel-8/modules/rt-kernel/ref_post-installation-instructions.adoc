:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-reference-a.adoc
// * ID: [id="my-reference-a"]
// * Title: = My reference A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="post-installation-instructions_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Post installation instructions

[role="_abstract"]
After you install the real-time kernel ensure that:

* To achieve optimal low-latency determinism, you perform RHEL for Real Time specific system tuning.
* You know about the module compatibility of the real-time kernel and the standard kernel.
* To enable `kdump`, you must configure RHEL for Real Time to provide crash dump information by enabling `kexec/kdump`.
* Verify that the Real Time kernel is the default kernel.



.Module compatibility of the real-time kernel and the standard kernel

The real-time kernel differs substantially from the standard Red Hat Enterprise Linux {ProductNumber} kernel. As a consequence, third-party kernel modules are incompatible with RHEL for Real Time.

Kernel modules are inherently specific to the kernel they are built for. The real time kernel is substantially different from the standard kernel, and so are the modules. Therefore, you cannot take third-party modules from Red Hat Enterprise Linux {ProductNumber} and use them as-is on the real-time kernel.

If you must use a third-party module, you must recompile it with the RHEL for Real Time header files, which are available in the RHEL for Real Time development and test packages.

The third-party drivers for the standard Red Hat Enterprise Linux {ProductNumber} but do not currently have a custom build for RHEL for Real Time are:

* EMC Powerpath
* NVidia graphics
* Advanced storage adapter configuration utilities from Qlogic

The user space `syscall` interface is compatible with RHEL for Real Time.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_for_real_time/{ProductNumberLink}/html/installing_rhel_{ProductNumber}_for_real_time/assembly_specifying-the-kernel-to-run_installing-rhel{ProductNumber}-for-real-time[Specifying the RHEL kernel to run]

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_for_real_time/{ProductNumberLink}/html/optimizing_rhel_{ProductNumber}_for_real_time_for_low_latency_operation/assembly_improving-latency-using-the-tuna-interface_optimizing-rhel{ProductNumber}-for-real-time-for-low-latency-operation[Improving latency using the tuna CLI]

