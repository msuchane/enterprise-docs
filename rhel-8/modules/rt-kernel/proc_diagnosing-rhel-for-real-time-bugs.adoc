:_mod-docs-content-type: PROCEDURE

[id="diagnosing-rhel-for-real-time-bugs_{context}"]


= Diagnosing RHEL for Real Time bugs
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]

Identifying which kernel, the RHEL for Real Time or the standard kernel, is the source of the problem can increase the chances of having your bug fixed faster. By following the procedure steps, you can diagnose the source of the problem before submitting a bug report.


.Prerequisite:

* The latest version of RHEL for Real Time kernel is installed.

.Procedure:

. Verify that you have the latest version of the RHEL for Real Time kernel.
. Boot into RHEL for Real Time kernel using the `GRUB` menu.
. If the problem occurs, report a bug against RHEL for Real Time.
. Try to reproduce the problem with the standard kernel.
+
This troubleshooting step assists in identifying the problem location.

[NOTE]
====
If the problem does not occur with the standard kernel, then the bug is probably the result of changes introduced in the RHEL for Real Time specific enhancements, which Red Hat has applied on top of the baseline (4.18.0) kernel.
====
