:_mod-docs-content-type: PROCEDURE

[id="proc_tuning-containers-for-rhel-for-real-time_{context}"]
= Tuning containers for RHEL for real-time

[role="_abstract"]

ifeval::[{ProductNumber} >= 9]
When testing the real-time workload in a container running on the main RHEL kernel, add the following options to the `podman run` command as necessary:

* `--cpuset-cpus=_<cpu_list>_` specifies the list of isolated CPU cores to use. If you have more than one CPU, use a comma-separated or a hyphen-separated range of CPUs that a container can use. 

* `--cpuset-mems=_<number-of-memory-nodes>_` specifies Non-Uniform Memory Access (NUMA) memory nodes to use, and, therefore avoids cross-NUMA node memory access. 

* `--memory-reservation=_<limit> <my_rt_container_image>_` verifies that the minimal amount of memory required by the real-time workload running on the container, is available at container start time.

.Procedure

* Start the real-time workloads in a container:  
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# podman run --cpuset-cpus=_<cpu_list>_  --cpuset-mems=_<number_of_memory_nodes>_  --memory-reservation=_<limit> <my_rt_container_image>_
----
endif::[]


ifeval::[{ProductNumber} == 8]

The main RHEL kernels enable the real time group scheduling feature, `CONFIG_RT_GROUP_SCHED`, by default. However, for real-time kernels, this feature is disabled.

The `CONFIG_RT_GROUP_SCHED` feature was developed independently of the `PREEMPT_RT` patchset used in the `kernel-rt` package and is intended to operate on real time processes on the main RHEL kernel.
The `CONFIG_RT_GROUP_SCHED` feature might cause latency spikes and is therefore disabled on `PREEMPT_RT` enabled kernels. Therefore, when testing your workload in a container running on the main RHEL kernel, some real-time bandwidth must be allocated to the container to be able to run the `SCHED_FIFO` or `SCHED_RR` tasks inside it.

.Procedure

. Configure the following global setting before using podman’s `--cpu-rt-runtime` command line option:
+
`# *echo 950000 > /sys/fs/cgroup/cpu,cpuacct/machine.slice/cpu.rt_runtime_us*`

. For CPU isolation, use the existing recommendations for setting aside a set of cores for the RT workload.

. Run `*podman run --cpuset-cpus*` with the list of isolated CPU cores to be used.

. Specify the Non-Uniform Memory Access (NUMA) memory nodes to use.
+
`*podman run --cpuset-mems=number-of-memory-nodes`
+
This avoids cross-NUMA node memory access.

. To verify that the minimal amount of memory required by the real-time workload running on the container is available at container start time, use the `*podman run --memory-reservation=limit` command.
endif::[]

[role="_additional-resources"]
.Additional resources
* `podman-run(1)` man page
