////
Base the file name and the ID on the module title. For example:
* file name: con-my-concept-module-a.adoc
* ID: [id="con-my-concept-module-a_{context}"]
* Title: = My concept module A
////

////
Indicate the module type in one of the following
ways:
Add the prefix con- or con_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: CONCEPT

////
The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID so you can include it multiple times in the same guide.
////

[id="con_mlock-system-calls_{context}"]
= mlock() system calls


[role="_abstract"]
The memory lock (`mlock()`) system calls enable calling processes to lock or unlock a specified range of the address space and prevents Linux from paging the locked memory to swap space. After you allocate the physical page to the page table entry, references to that page are relatively fast. The memory lock system calls fall into `mlock()` and `munlock()` categories.

The `mlock()` and `munlock()` system calls lock and unlock a specified range of process address pages. When successful, the pages in the specified range remain resident in the memory until the `munlock()` system call unlocks the pages.

The `mlock()` and `munlock()` system calls take following parameters:

* `addr`: specifies the start of an address range.
* `len`: specifies the length of the address space in bytes.

When successful, `mlock()` and `munlock()` system calls return 0. In case of an error, they return -1 and set a `errno` to indicate the error.

The `mlockall()` and `munlockall()` system calls locks or unlocks all of the program space.

[NOTE]
====
The `mlock()` system call does not ensure that the program will not have a page I/O. It ensures that the data stays in the memory but cannot ensure it stays on the same page. Other functions such as `move_pages` and memory compactors can move data around regardless of the use of `mlock()`.
====

Memory locks are made on a page basis and do not stack. If two dynamically allocated memory segments share the same page locked twice by `mlock()` or `mlockall()`, they unlock by using a single `munlock()` or `munlockall()` system call. As such, it is important to be aware of the pages that the application unlocks to avoid double-locking or single-unlocking problems.

The following are two most common workarounds to mitigate double-lock or single-unlock problems:

* Tracking the allocated and locked  memory areas and creating a wrapper function that verifies the number of page allocations before unlocking a page. This is the resource counting principle used in device drivers.
* Making memory allocations based on page size and alignment to avoid double-lock on a page.



[role="_additional-resources"]
.Additional resources
* `capabilities(7)` man page
* `mlock(2)` man page
* `mlock(3)` man page
* `mlockall(2)` man page
* `mmap(2)` man page
* `move_pages(2)` man page
* `posix_memalign(3)` man page
* `posix_memalign(3p)` man page
