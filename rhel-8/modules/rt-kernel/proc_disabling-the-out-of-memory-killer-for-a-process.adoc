:_mod-docs-content-type: PROCEDURE
:experimental:
[id="proc_disabling-the-out-of-memory-killer-for-a-process_{context}"]
= Disabling the Out of Memory killer for a process

[role="_abstract"]
You can disable the [function]`oom_killer()` function for a process by setting `oom_adj` to the reserved value of `-17`. This will keep the process alive, even in an OOM state.


.Procedure

* Set the value in `oom_adj` to `-17`.
+
[subs="quotes"]
----
# *echo -17 > /proc/12465/oom_adj*
----

.Verification steps

. Display the current `oom_score` for the process.
+
[subs="quotes"]
----
# *cat /proc/12465/oom_score*
0
----

. Verify that the displayed value is `0`.
