:_mod-docs-content-type: CONCEPT
[id="con_rhel-for-real-time-for-optimizing-latency_{context}"]
= RHEL for Real Time for optimizing latency

[role="_abstract"]
RHEL for Real Time is designed to be used on well-tuned systems for applications with extremely high determinism requirements. Kernel system tuning offers the vast majority of the improvement in determinism.

For example, in many workloads, thorough system tuning improves consistency of results by around 90%. This is why, before using RHEL for Real Time, we recommend that customers first perform system tuning of standard RHEL to see if it meets their objectives.

System tuning is just as important when using the Real Time kernel as it is for the standard kernel. Installing the Real Time kernel on an untuned system running the standard kernel supplied as part of RHEL is not likely to result in any noticeable benefit. Tuning the standard kernel will yield 90% of the possible latency gains. The Real Time kernel provides the last 10% of latency reduction required by the most demanding workloads.

[WARNING]
Before tuning Real Time kernel systems, ensure that the base platform is properly tuned and the system BIOS parameters are adjusted. Failure to perform these tasks may prevent getting consistent performance from a RHEL Real Time deployment.

The objective of the Real Time kernel consistent, low-latency determinism offering predictable response times. There is some additional kernel overhead associated with the real time kernel. This is due primarily to handling hardware interrupts in separately scheduled threads. The increased overhead in some workloads results in some degradation in overall throughput. The exact amount is very workload dependent, ranging from 0% to 30%.

For typical workloads with kernel latency requirements in the millisecond (ms) range, the standard RHEL kernel is sufficient. However,if your workload has stringent low-latency determinism requirements for core kernel features such as interrupt handling and process scheduling in the microsecond (μs) range, then the Real Time kernel is for you.

image::rhel-rt.png[Benefit of realtime over standard kernel system tuning]

This graph compares a million samples of machines that use the RHEL and the RHEL for Real Time kernel, respectively.

* The blue points in this graph represent the system response time (in microseconds) of machines running a tuned RHEL kernel.

* The green points in the graph represent the system response time of machines running a tuned real-time kernel.

It is clear from this graph that the response time of the Real Time kernel is very consistent, in contrast to the standard kernel, which has greater variability.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_for_real_time/8/html-single/optimizing_rhel_8_for_real_time_for_low_latency_operation/[Optimizing RHEL 8 for Real Time for low latency operation]


// * _RHEL Tuning Guide_.
// * _RHEL for Real Time Tuning Guide_.
