:_mod-docs-content-type: PROCEDURE

[id="proc_displaying-the-tcp-timestamp-status_{context}"]
= Displaying the TCP timestamp status

[role="_abstract"]
You can view the status of TCP timestamp generation.

.Procedure

* Display the TCP timestamp generation status:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
# *sysctl net.ipv4.tcp_timestamps*
net.ipv4.tcp_timestamps = 0
----
+
The value `1` indicates that timestamps are being generated. The value `0` indicates timestamps are being not generated.
