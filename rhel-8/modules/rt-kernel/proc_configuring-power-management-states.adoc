:_mod-docs-content-type: PROCEDURE
:experimental:
[id="proc_configuring-power-management-states_{context}"]
= Configuring power management states

[role="_abstract"]
You can control power management transitions by configuring power management states with one of the following ways:

* Write a value to the [filename]`/dev/cpu_dma_latency` file to change the maximum response time for processes in microseconds and hold the file descriptor open until low latency is required.
* Reference the [filename]`/dev/cpu_dma_latency` file in an application or a script.

.Prerequisites

* You have administrator privileges.

.Procedure


* Specify latency tolerance by writing a 32-bit number that represents a maximum response time in microseconds in `/dev/cpu_dma_latency` and keep the file descriptor open through the low-latency operation. A value of `0` disables C-state completely.
+
For example:
+
[literal,subs="+quotes,attributes",options="nowrap"]
----
import os
import os.path
import signal
import sys
if not os.path.exists('/dev/cpu_dma_latency'):
 	print("no PM QOS interface on this system!")
 	sys.exit(1)  
fd = os.open('/dev/cpu_dma_latency', os.O_WRONLY)
 	 os.write(fd, b'\0\0\0\0')
 	 print("Press ^C to close /dev/cpu_dma_latency and exit")
    signal.pause()
except KeyboardInterrupt:
    print("closing /dev/cpu_dma_latency")
    os.close(fd)
    sys.exit(0)
----
+
[NOTE]
====
The Power Management Quality of Service interface (`pm_qos`) interface is only active while it has an open file descriptor. Therefore, any script or program you use to access `/dev/cpu_dma_latency` must hold the file open until power-state transitions are allowed.
====


