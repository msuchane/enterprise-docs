:_mod-docs-content-type: REFERENCE
[id="ref_priority-map_{context}"]
= Priority map

[role="_abstract"]
Scheduler priorities are defined in groups, with some groups dedicated to particular kernel functions.

.Thread priority table
[options="header"]
[cols="1,2,4"]
|====
|Priority|Threads|Description
|1|Low priority kernel threads|This priority is usually reserved for the tasks that need to be just above `SCHED_OTHER`.

|2 - 49|Available for use|The range used for typical application priorities.
|50|Default hard-IRQ value|This priority is the default value  for hardware-based interrupts.
|51 - 98|High priority threads|Use this range for threads that execute periodically and must have quick response times. Do `not` use this range for CPU-bound threads, because it will prevent responses to lower level interrupts.
|99|Watchdogs and migration|System threads that must run at the highest priority.

|====
