:_mod-docs-content-type: REFERENCE
[id="ref_interpreting-hardware-and-firmware-latency-test-results_{context}"]
= Interpreting hardware and firmware latency test results
:experimental:
[role="_abstract"]
The hardware latency detector (`hwlatdetect`) uses the tracer mechanism to detect latencies introduced by the hardware architecture or BIOS/EFI firmware. By checking the latencies measured by `hwlatdetect`, you can determine if a potential hardware is suitable to support the RHEL for Real Time kernel. 

.Examples

* The example result represents a system tuned to minimize system interruptions from firmware. In this situation, the output of `hwlatdetect` looks like this:
+
[subs="quotes"]
....
# *hwlatdetect --duration=60s*
hwlatdetect:  test duration 60 seconds
	detector: tracer
	parameters:
		Latency threshold: 10us
		Sample window:     1000000us
		Sample width:      500000us
		Non-sampling period:  500000us
		Output File:       None

Starting test
test finished
Max Latency: Below threshold
Samples recorded: 0
Samples exceeding threshold: 0
....


* The example result represents a system that could not be tuned to minimize system interruptions from firmware. In this situation, the output of `hwlatdetect` looks like this:
+
[subs="quotes"]
....
# *hwlatdetect --duration=10s*
hwlatdetect:  test duration 10 seconds
	detector: tracer
	parameters:
		Latency threshold: 10us
		Sample window:     1000000us
		Sample width:      500000us
		Non-sampling period:  500000us
		Output File:       None

Starting test
test finished
Max Latency: 18us
Samples recorded: 10
Samples exceeding threshold: 10
SMIs during run: 0
ts: 1519674281.220664736, inner:17, outer:15
ts: 1519674282.721666674, inner:18, outer:17
ts: 1519674283.722667966, inner:16, outer:17
ts: 1519674284.723669259, inner:17, outer:18
ts: 1519674285.724670551, inner:16, outer:17
ts: 1519674286.725671843, inner:17, outer:17
ts: 1519674287.726673136, inner:17, outer:16
ts: 1519674288.727674428, inner:16, outer:18
ts: 1519674289.728675721, inner:17, outer:17
ts: 1519674290.729677013, inner:18, outer:17----
....
+
The output shows that during the consecutive reads of the system `clocksource`, there were 10 delays that showed up in the 15-18 us range.
+

[NOTE]
Previous versions used a kernel module rather than the `ftrace` tracer.

.Understanding the results

The information on testing method, parameters, and results helps you understand the latency parameters and the latency values detected by the `hwlatdetect` utility.

The table for Testing method, parameters, and results, lists the parameters and the latency values detected by the `hwlatdetect` utility.

.Testing method, parameters, and results
[options="header"]
[cols="2,1,4"]
|====
| Parameter| Value| Description
| `test duration`| `10 seconds`| The duration of the test in seconds
| `detector`| `tracer`| The utility that runs the `detector` thread
| `*parameters*`||
| `Latency threshold`| `10us`| The maximum allowable latency
| `Sample window`| `1000000us`| 1 second
| `Sample width`| `500000us`| 0.05 seconds
| `Non-sampling period`| `500000us`| 0.05 seconds
| `Output File`| `None`| The file to which the output is saved.
| `*Results*`| |
| `Max Latency`| `18us`| The highest latency during the test that exceeded the `Latency threshold`. If no sample exceeded the `Latency threshold`, the report shows `Below threshold`.
| `Samples recorded` | `10`| The number of samples recorded by the test.
| `Samples exceeding threshold`| `10`| The number of samples recorded by the test where the latency exceeded the `Latency threshold`.
| `SMIs during run`| `0` | The number of System Management Interrupts (SMIs) that occurred during the test run.
|====

////
The detector thread runs a loop which does the following pseudocode:

----
t1 = timestamp()
	loop:
		t0 = timestamp()
		if (t0 - t1) > threshold
		   outer = (t0 - t1)
		t1 = timestamp
		if (t1 - t0) > threshold
		   inner = (t1 - t0)
		if inner or outer:
		   print
		if t1 > duration:
		   goto out
		goto loop
	out:
----

`t0` is the timestamp at the start of each loop. `t1` is the timestamp at the end of each loop. The inner loop comparison checks that t0 - t1 does not exceed the specified threshold (10 us default). The outer loop comparison checks the time between the bottom of the loop and the top t1 - t0. The time between consecutive reads of the timestamp register should be dozens of nanoseconds (essentially a register read, a comparison, and a conditional jump) so any other delay between consecutive reads is introduced by firmware or by the way the system components were connected.

////

[NOTE]
====
The values printed by the `hwlatdetect` utility for inner and outer are the maximum latency values. They are deltas between consecutive reads of the current system clocksource (usually the TSC or TSC register, but potentially the HPET or ACPI power management clock) and any delays between consecutive reads introduced by the hardware-firmware combination.
====

After finding the suitable hardware-firmware combination, the next step is to test the real-time performance of the system while under a load.
