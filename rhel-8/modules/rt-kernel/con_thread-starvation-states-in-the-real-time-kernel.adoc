:_mod-docs-content-type: CONCEPT

[id="thread-starvation-states-in-the-real-time-kernel_{context}"]
= Thread starvation in the real-time kernel

Thread starvation occurs when a thread is on a CPU run queue for longer than the starvation threshold and does not make progress. A common cause of thread starvation is to run a fixed-priority polling application, such as `SCHED_FIFO` or `SCHED_RR` bound to a CPU. Since the polling application does not block for I/O, this can prevent other threads, such as `kworkers`, from running on that CPU.

An early attempt to reduce thread starvation is called as real-time throttling. In real-time throttling, each CPU has a portion of the execution time dedicated to non real-time tasks. The default setting for throttling is on with 95% of the CPU for real-time tasks and 5% reserved for non real-time tasks. This works if you have a single real-time task causing starvation but does not work if there are multiple real-time tasks assigned to a CPU. You can work around the problem by using:

The `stalld` mechanism:: The `stalld` mechanism is an alternative for real-time throttling and avoids some of the throttling drawbacks. `stalld` is a daemon to periodically monitor the state of each thread in the system and looks for threads that are on the run queue for a specified length of time without being run. `stalld` temporarily changes that thread to use the `SCHED_DEADLINE` policy and allocates the thread a small slice of time on the specified CPU. The thread then runs, and when the time slice is used, the thread returns to its original scheduling policy and `stalld` continues to monitor thread states. 
+
Housekeeping CPUs are CPUs that run all daemons, shell processes, kernel threads, interrupt handlers, and all work that can be dispatched from an isolated CPU. For housekeeping CPUs with real-time throttling disabled, `stalld` monitors the CPU that runs the main workload and assigns the CPU with the `SCHED_FIFO` busy loop, which helps to detect stalled threads and improve the thread priority as required with a previously defined acceptable added noise. `stalld` can be a preference if the real-time throttling mechanism causes an unreasonable noise in the main workload.
+
With `stalld`, you can more precisely control the noise introduced by boosting starved threads. The shell script `/usr/bin/throttlectl` automatically disables real-time throttling when `stalld` is run. You can list the current throttling values by using the `/usr/bin/throttlectl show` script.

Disabling real-time throttling:: The following parameters in the `/proc` filesystem control real-time throttling:

* The `/proc/sys/kernel/sched_rt_period_us` parameter specifies the number of microseconds in a period and defaults to 1 million, which is 1 second. 
* The `/proc/sys/kernel/sched_rt_runtime_us` parameter specifies the number of microseconds that can be used by a real-time task before throttling occurs and it defaults to 950,000 or 95% of the available CPU cycles. You can disable throttling by passing a value of `-1` into the `sched_rt_runtime_us` file by using the `echo -1 > /proc/sys/kernel/sched_rt_runtime_us` command. 








