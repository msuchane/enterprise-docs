:_mod-docs-content-type: PROCEDURE

[id="proc_creating-a-mutex-attribute-object_{context}"]
= Creating a mutex attribute object

[role="_abstract"]
To define any additional capabilities for the `mutex`, create a `pthread_mutexattr_t` object. This object stores the defined attributes for the futex. This is a basic safety procedure that you must always perform.

.Procedure

* Create the mutex attribute object using one of the following:

** `*pthread_mutex_t(_my_mutex_)*;`

** `*pthread_mutexattr_t(_&my_mutex_attr_)*;`

** `*pthread_mutexattr_init(_&my_mutex_attr_)*;`

For more information about advanced mutex attributes, see xref:ref_advanced-mutex-options_assembly_preventing-resource-overuse-by-using-mutex[Advanced mutex attributes].


