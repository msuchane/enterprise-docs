:_mod-docs-content-type: PROCEDURE

[id="proc_disabling-graphics-console-logging-to-graphics-adapter_{context}"]

= Disabling graphics console logging to graphics adapter

[role="_abstract"]
The `teletype` (`tty`) default kernel console enables your interaction with the system by passing input data to the system and displaying the output information about the graphics console.

Not configuring the graphics console, prevents it from logging on the graphics adapter. This makes `tty0` unavailable to the system and helps disable printing messages on the graphics console.

[NOTE]
====
Disabling graphics console output does not delete information. The information prints in the system log and you can access them using the `journalctl` or `dmesg` utilities.
====

.Procedure
* Remove the `console=tty0` option from the kernel configuration:
+
----
# grubby --update-kernel=ALL --remove-args="console=tty0"
----
