////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="proc-doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix proc- or proc_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: PROCEDURE

[id="proc_testing-large-interrupts-loads-on-a-device_{context}"]
= Testing large interrupts loads on a device


[role="_abstract"]
Running timers at high frequency can generate a large interrupt load. The `–timer` stressor with an appropriately selected timer frequency can force many interrupts per second.


.Prerequisites

* You have root permissions on the system.

.Procedure

* To generate an interrupt load, use the `--timer` option:
+
[literal,subs="+quotes",options="nowrap"]
----
# *stress-ng --timer 32 --timer-freq 1000000*
----
+
In this example, `stress-ng` tests 32 instances at 1MHz.
