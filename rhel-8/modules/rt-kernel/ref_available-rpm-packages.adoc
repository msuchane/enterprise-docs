:_mod-docs-content-type: REFERENCE
[id="ref_available-rpm-packages_{context}"]
= Available RPM packages in the RHEL for Real Time repository

[role="_abstract"]
The Red Hat Package Manager (RPM) for RHEL for Real Time repository includes the following packages:

* `kernel-rt` package, which is the RHEL for Real Time kernel package. 
* RHEL for Real Time kernel test packages, which contains test programs for the real-time kernel.
* RHEL for Real Time debugging packages, which are for debugging and code tracing. 


.Basic RHEL for Real Time kernel packages
[options="header"]
|====
|RPM package name|Description|RT-specific|Required
|`kernel-rt`|Low latency and preemption functionality|Yes|Yes
|====

// The package was renamed in RHEL 9
ifeval::[{ProductNumber} == 8]
:tests: rt-tests
endif::[]
ifeval::[{ProductNumber} > 8]
:tests: realtime-tests
endif::[]

.RHEL for Real Time kernel test packages
[options="header"]
|====
|RPM package name|Description|RT-specific|Required
|`kernel-rt-devel`|Headers and libraries for kernel development|Yes|No
|`kernel-rt-debug`|RHEL for Real Time kernel with debugging functions compiled in (slow)|Yes|No
|`kernel-rt-debug-devel`|Headers and libraries for development on debug kernel|Yes|No
|`{tests}`|Utilities for measuring system latencies and for proving that priority-inheritance mutex functions properly|No|No
|====

The debugging packages are provided to use with the `perf`, `trace-cmd`, and `crash` utilities for analyzing kernel crash dumps. The debugging packages include symbol tables and are quite large. For this reason, the debugging packages are separately delivered from the other RHEL for Real Time packages. You can download the debugging packages from `RHEL for Real Time - Debug RPMs` repository.

.Basic RHEL for Real Time debugging packages
[options="header"]
|====
|RPM package name|Description|RT-specific|Required
|`kernel-rt-debuginfo`|Symbols for profiling and debugging use, such as `perf` or `trace-cmd`|Yes|No
|`kernel-rt-debug-debuginfo`|Symbols for profiling and tracing|Yes|No
|====
