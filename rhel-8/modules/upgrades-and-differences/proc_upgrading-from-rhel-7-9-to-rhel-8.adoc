:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="upgrading-from-rhel-7-9-to-rhel-8_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Upgrading from RHEL 7.9 to RHEL 8

Similarly to the in-place upgrade from RHEL 6 to RHEL 7, the in-place upgrade from RHEL 7 to RHEL 8 consists of two major stages, a pre-upgrade assessment of the system in which the system remains unchanged, and the actual in-place upgrade. In case of a RHEL 7 to RHEL 8 upgrade, both phases are handled by the Leapp utility. Note that RHEL version 7.9 is a prerequisite for upgrading to RHEL 8.

To perform an in-place upgrade from RHEL 7.9 to RHEL 8:

. Assess the upgradability of your system and fix reported problems as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/reviewing-the-pre-upgrade-report_upgrading-from-rhel-7-to-rhel-8[Reviewing the pre-upgrade report] of the [citetitle]`Upgrading from RHEL 7 to RHEL 8` document.

. Upgrade your RHEL 7 system to RHEL 8 per instructions in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/performing-the-upgrade-from-rhel-7-to-rhel-8_upgrading-from-rhel-7-to-rhel-8[Performing the upgrade from RHEL 7 to RHEL 8] of the [citetitle]`Upgrading from RHEL 7 to RHEL 8` document.



[discrete]
[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/troubleshooting_upgrading-from-rhel-7-to-rhel-8[Troubleshooting] in the [citetitle]`Upgrading from RHEL 7 to RHEL 8` document


