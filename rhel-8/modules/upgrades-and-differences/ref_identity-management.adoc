:_mod-docs-content-type: REFERENCE
[id="identity-management_{context}"]
= Identity Management

[id="idm-module_{context}"]
== Identity Management packages are installed as a module

In RHEL 8, the packages necessary for installing an Identity Management (IdM) server and client are distributed as a module. The [stream]`client` stream is the default stream of the [package]`idm` module, and you can download the packages necessary for installing the client without enabling the stream.

The IdM server module stream is called `DL1` and contains multiple profiles that correspond to the different types of IdM servers:

* `server`: an IdM server without integrated DNS
* `dns`: an IdM server with integrated DNS
* `adtrust`: an IdM server that has a trust agreement with Active Directory
* `client`: an IdM client

To download the packages in a specific profile of the [stream]`DL1` stream:

. Enable the stream:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module enable idm:DL1*
....

. Switch to the RPMs delivered through the stream:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} distro-sync*
....

. Install the selected profile:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module install idm:DL1/_profile_*
....
+
Replace _profile_ with one of the specific profiles defined above.

For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/installing_identity_management/index#installing-packages-required-for-an-idm-server_preparing-the-system-for-ipa-server-installation[Installing packages required for an Identity Management server] and link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/installing_identity_management/index#installing-idm-client-packages-from-the-idm-client-stream_preparing-the-system-for-ipa-client-installation[Packages required to install an Identity Management client].


[id="overrides-to-allow-ad-user-to-administer-idm_{context}"]
== Active Directory users can now administer Identity Management

In Red Hat Enterprise Linux (RHEL) 7, external group membership allows AD users and groups to access IdM resources in a POSIX environment with the help of the System Security Services Daemon (SSSD).

The IdM LDAP server has its own mechanisms to grant access control. RHEL 8 introduces an update that allows adding an ID user override for an AD user as a member of an IdM group. An ID override is a record describing what a specific Active Directory user or group properties should look like within a specific ID view, in this case the Default Trust View. As a consequence of the update, the IdM LDAP server is able to apply access control rules for the IdM group to the AD user.

AD users are now able to use the self service features of IdM UI, for example to upload their SSH keys, or change their personal data. An AD administrator is able to fully administer IdM without having two different accounts and passwords.

[NOTE]
====
Currently, selected features in IdM may still be unavailable to AD users. For example, setting passwords for IdM users as an AD user from the IdM [replaceable]`admins` group might fail.
====


[id="idm-now-supports-ansible-roles-and-modules-for-installation-and-management_{context}"]
== IdM supports Ansible roles and modules for installation and management

{RHEL} 8.1 introduces the `ansible-freeipa` package, which provides Ansible roles and modules for Identity Management (IdM) deployment and management. You can use Ansible roles to install and uninstall IdM servers, replicas, and clients. You can use Ansible modules to manage IdM groups, topology, and users. There are also example playbooks available.

This update simplifies the installation and configuration of IdM based solutions.


[id="ansible-freeipa-is-now-available-in-the-appstream-repository-with-all-dependencies_{context}"]
== `ansible-freeipa` is available in the AppStream repository with all dependencies

Starting with RHEL 8.6, installing the `ansible-freeipa` package automatically installs the `ansible-core` package, a more basic version of `ansible`, as a dependency. Both `ansible-freeipa` and `ansible-core` are available in the `rhel-9-for-x86_64-appstream-rpms` repository.

`ansible-freeipa` in RHEL 8.6 contains all the modules that it contained prior to RHEL 8.6.

Prior to RHEL 8.6, you first had to enable the Ansible repository and install the `ansible` package. Only then could you install `ansible-freeipa`.


[id="an-alternative-to-the-traditional-rhel-ansible-freeipa-repository-ansible-automation-hub_{context}"]
== An alternative to the traditional RHEL `ansible-freeipa` repository: Ansible Automation Hub

As of {RHEL} 8.6, you can download `ansible-freeipa` modules from the Ansible Automation Hub (AAH) instead of downloading them from the standard RHEL repository. By using AAH, you can benefit from the faster updates of the `ansible-freeipa` modules available in this repository.

In AAH, `ansible-freeipa` roles and modules are distributed in the collection format. Note that you need an Ansible Automation Platform (AAP) subscription to access the content on the AAH portal. You also need `ansible` version 2.14 or later.

The `redhat.rhel_idm` collection has the same content as the traditional `ansible-freeipa` package. However, the collection format uses a fully qualified collection name (FQCN) that consists of a namespace and the collection name. For example, the `redhat.rhel_idm.ipadnsconfig` module corresponds to the `ipadnsconfig` module in `ansible-freeipa` provided by a RHEL repository. The combination of a namespace and a collection name ensures that the objects are unique and can be shared without any conflicts.


[id="identity-management-users-can-use-external-identity-providers-to-authenticate-to-idm_{context}"]
== Identity{nbsp}Management users can use external identity providers to authenticate to IdM

As of RHEL 8.10, you can associate Identity{nbsp}Management (IdM) users with external identity providers (IdPs) that support the OAuth 2 device authorization flow. Examples of such IdPs include Red{nbsp}Hat build of Keycloak, Azure Entra ID, Github, Google, and Facebook.

If an IdP reference and an associated IdP user ID exist in IdM, you can use them to enable an IdM user to authenticate at the external IdP. After performing authentication and authorization at the external IdP, the IdM user receives a Kerberos ticket with single sign-on capabilities. The user must authenticate with the SSSD version available in RHEL 8.7 or later.

You can also use the `idp` `ansible-freeipa` module to configure IdP authentication for IdM users.


[id="session-recording_{context}"]
== Session recording solution for RHEL 8 added


A session recording solution has been added to Red Hat Enterprise Linux 8 (RHEL 8). A new `tlog` package and its associated web console session player enable to record and playback the user terminal sessions. The recording can be configured per user or user group via the System Security Services Daemon (SSSD) service. All terminal input and output is captured and stored in a text-based format in a system journal. The input is inactive by default for security reasons not to intercept raw passwords and other sensitive information.

The solution can be used for auditing of user sessions on security-sensitive systems. In the event of a security breach, the recorded sessions can be reviewed as a part of a forensic analysis. The system administrators are now able to configure the session recording locally and view the result from the RHEL 8 web console interface or from the Command-Line Interface using the `tlog-play` utility.



[id="idm-removed-functionality_{context}"]
== Removed Identity Management functionality

[id="no_ntp_server_role_{context}"]
=== No `NTP Server` IdM server role
Because `ntpd` has been deprecated in favor of `chronyd` in RHEL 8, IdM servers are no longer configured as Network Time Protocol (NTP) servers and are only configured as NTP clients. The RHEL 7 `NTP Server` IdM server role has also been deprecated in RHEL 8.

[id="nss_db_{context}"]
=== NSS databases not supported in OpenLDAP
The OpenLDAP suite in previous versions of Red Hat Enterprise Linux (RHEL) used the Mozilla Network Security Services (NSS) for cryptographic purposes. With RHEL 8, OpenSSL, which is supported by the OpenLDAP community, replaces NSS. OpenSSL does not support NSS databases for storing certificates and keys. However, it still supports privacy enhanced mail (PEM) files that serve the same purpose.
//(BZ#1570056)

[id="python-kerberos_{context}"]
=== Selected Python Kerberos packages have been replaced
In Red Hat Enterprise Linux (RHEL) 8, the `python-gssapi` package has replaced Python Kerberos packages such as `python-krbV`, `python-kerberos`, `python-requests-kerberos`, and `python-urllib2_kerberos`. Notable benefits include:

* `python-gssapi` is easier to use than `python-kerberos` and `python-krbV`.
* `python-gssapi` supports both `python 2` and `python 3` whereas `python-krbV` does not.
*  Additional Kerberos packages, `python-requests-gssapi` and `python-urllib-gssapi`, are currently available in the Extra Packages for Enterprise Linux (EPEL) repository.

The GSSAPI-based packages allow the use of other Generic Security Services API (GSSAPI) mechanisms in addition to Kerberos, such as the NT LAN Manager `NTLM` for backward compatibility reasons.

This update improves the maintainability and debuggability of GSSAPI in RHEL 8.
//(JIRA:RHELPLAN-10444)

[id="idm-sssd_{context}"]
== SSSD

[id="ad-gpos-enforced_{context}"]
=== AD GPOs are now enforced by default

In RHEL 8, the default setting for the `ad_gpo_access_control` option is `enforcing`, which ensures that access control rules based on Active Directory Group Policy Objects (GPOs) are evaluated and enforced.

In contrast, the default for this option in RHEL 7 is `permissive`, which evaluates but does not enforce  GPO-based access control rules. With `permissive` mode, a syslog message is recorded every time a user would be denied access by a GPO, but those users are still allowed to log in.

[NOTE]
====
Red Hat recommends ensuring GPOs are configured correctly in Active Directory before upgrading from RHEL 7 to RHEL 8.

Misconfigured GPOs that do not affect authorization in default RHEL 7 hosts may affect default RHEL 8 hosts.
====

For more information about GPOs, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/integrating_rhel_systems_directly_with_windows_active_directory/managing-direct-connections-to-ad_integrating-rhel-systems-directly-with-active-directory#applying-group-policy-object-access-control-in-rhel_managing-direct-connections-to-ad[Applying Group Policy Object access control in RHEL] and the `ad_gpo_access_control` entry in the `sssd-ad` Manual page.


[id="authselect_{context}"]
=== `authselect` replaces `authconfig`

In RHEL 8, the [systemitem]`authselect` utility replaces the  [systemitem]`authconfig` utility. [systemitem]`authselect` comes with a safer approach to PAM stack management that makes the PAM configuration changes simpler for system administrators. [systemitem]`authselect` can be used to configure authentication methods such as passwords, certificates, smart cards and fingerprint. [systemitem]`authselect` does not configure services required to join remote domains. This task is performed by specialized tools, such as [utility]`realmd` or [utility]`ipa-client-install`.


[id="kcm-replace-keyring-default-cache_{context}"]
=== KCM replaces KEYRING as the default credential cache storage

In RHEL 8, the default credential cache storage is the Kerberos Credential Manager (KCM) which is backed by the [systemitem]`sssd-kcm` deamon. KCM overcomes the limitations of the previously used KEYRING, such as its being difficult to use in containerized environments because it is not namespaced, and to view and manage quotas.

With this update, RHEL 8 contains a credential cache that is better suited for containerized environments and that provides a basis for building more features in future releases.


[id="sssctl-print-hbac-rules-report_{context}"]
=== `sssctl` prints an HBAC rules report for an IdM domain

With this update, the [systemitem]`sssctl` utility of the System Security Services Daemon (SSSD) can print an access control report for an Identity Management (IdM) domain. This feature meets the need of certain environments to see, for regulatory reasons, a list of users and groups that can access a specific client machine. Running [command]`sssctl access-report` [replaceable]`domain_name` on an IdM client prints the parsed subset of host-based access control (HBAC) rules in the IdM domain that apply to the client machine.

Note that no other providers than IdM support this feature.


[id="local-users-cached-by-sssd-nsssss_{context}"]
=== As of RHEL 8.8, SSSD no longer caches local users by default nor serves them through the `nss_sss` module

In RHEL 8.8 and later, the System Security Services Daemon (SSSD) `files` provider, which serves users and groups from the `/etc/passwd` and `/etc/group` files, is disabled by default. The default value of the `enable_files_domain` setting in the `/etc/sssd/sssd.conf` configuration file is `false`.

For RHEL 8.7 and earlier versions, the SSSD `files` provider is enabled by default. The default value of the `enable_files_domain` setting in the `sssd.conf` configuration file is `true`, and the `sss` nsswitch module precedes `files` in the `/etc/nsswitch.conf` file.


[id="sssd-allow-multiple-smart-card-auth-devs_{context}"]
=== SSSD now allows you to select one of the multiple smart-card authentication devices

By default, the System Security Services Daemon (SSSD) tries to detect a device for smart-card authentication automatically. If there are multiple devices connected, SSSD selects the first one it detects. Consequently, you cannot select a particular device, which sometimes leads to failures.

With this update, you can configure a new `p11_uri` option for the `[pam]` section of the `sssd.conf` configuration file. This option enables you to define which device is used for smart-card authentication.

For example, to select a reader with the slot id `2` detected by the OpenSC PKCS#11 module, add:

----
p11_uri = library-description=OpenSC%20smartcard%20framework;slot-id=2
----

to the `[pam]` section of `sssd.conf`.

For details, see the `man sssd.conf` page.

[id="sssd-removed-functionality_{context}"]
== Removed SSSD functionality

[id="sssd-secrets_{context}"]
=== `sssd-secrets` has been removed
The [systemitem]`sssd-secrets` component of the System Security Services Daemon (SSSD) has been removed in Red Hat Enterprise Linux 8. This is because Custodia, a secrets service provider, is no longer actively developed. Use other Identity Management tools to store secrets, for example the Identity Management Vault.
//(JIRA:RHELPLAN-10441)

[id="sssd_libwbclient_removed_{context}"]
=== The SSSD version of libwbclient has been removed
The SSSD implementation of the `libwbclient` package allowed the Samba `smbd` service to retrieve user and group information from AD without the need to run the `winbind` service. As Samba now requires that the `winbind` service is running and handling communication with AD, the related code has been removed from `smdb` for security reasons. As this additional required functionality is not part of SSSD and the SSSD implementation of `libwbclient` cannot be used with recent versions of Samba, the SSSD implementation of `libwbclient` has been removed in RHEL 8.5.
// (BZ1947671)
