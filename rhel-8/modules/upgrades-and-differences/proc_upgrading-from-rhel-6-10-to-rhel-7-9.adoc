:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="upgrading-from-rhel-6-10-to-rhel-7-9_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Upgrading from RHEL 6.10 to RHEL 7.9
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

The in-place upgrade from RHEL 6 to RHEL 7 consists of two major stages, a pre-upgrade assessment of the system, and the actual in-place upgrade:

* In the pre-upgrade phase, the Preupgrade Assistant collects information from the system, analyzes it, and suggests possible corrective actions. The Preupgrade Assistant does not make any changes to your system.
* In the in-place upgrade phase, the Red Hat Upgrade Tool installs RHEL 7 packages and adjusts basic configuration where possible.

To perform an in-place upgrade from RHEL 6 to RHEL 7:

. Assess the upgradability of your system using the Preupgrade Assistant, and fix problems identified in the report before you proceed with the upgrade. For detailed instructions, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/upgrading_from_rhel_6_to_rhel_7/index#assessing-upgrade-suitability_upgrading-from-rhel-6-to-rhel-7[Assessing upgrade suitability] section in the `Upgrading from RHEL 6 to RHEL 7` documentation.
. Use the Red Hat Upgrade Tool to upgrade to RHEL 7.9. For a detailed procedure, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/upgrading_from_rhel_6_to_rhel_7/index#upgrading-your-system-from-rhel-6-to-rhel-7_upgrading-from-rhel-6-to-rhel-7[Upgrading your system from RHEL 6 to RHEL 7] section in the `Upgrading from RHEL 6 to RHEL 7` documentation.

