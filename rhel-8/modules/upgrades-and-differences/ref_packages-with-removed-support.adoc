:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
// assembly_changes-to-packages.adoc
// include::modules/upgrades-and-differences/ref_packages-with-removed-support.adoc[leveloffset=+1]

[id="packages-with-removed-support_{context}"]
= Packages with removed support

Certain packages in RHEL 8 are distributed through the CodeReady Linux Builder repository, which contains unsupported packages for use by developers. For a complete list of packages in this repository, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/package_manifest/codereadylinuxbuilder-repository[Package manifest].
//not from Optional (RHEL 7) -> CRB (RHEL 8) = unsupported to unsupported

The following packages are distributed in a supported channel in RHEL 7 and are unsupported and part of the CodeReady Linux Builder repository in RHEL 8:

[options="header", cols="3,2"]
|====
|Package|RHEL 7 channel

|antlr-tool | rhel7-base

|bcel | rhel7-base

|cal10n | rhel7-base

|cdi-api-javadoc | rhel7-base

|codemodel | rhel7-base

|dejagnu | rhel7-base

|docbook-style-dsssl | rhel7-base

|docbook-utils | rhel7-base

|docbook5-schemas | rhel7-base

|elinks | rhel7-base

|flex-devel | rhel7-base

|geronimo-jms | rhel7-base

|gnome-common | rhel7-base

|hamcrest | rhel7-base

|imake | rhel7-base

|isorelax | rhel7-base

|jakarta-oro | rhel7-base

|javamail | rhel7-base

|jaxen | rhel7-base

|jdom | rhel7-base

|jna | rhel7-base

|junit | rhel7-base

|jvnet-parent | rhel7-base

|libdbusmenu-doc | rhel7-base

|libdbusmenu-gtk3-devel | rhel7-base

|libfdt | rhel7-base

|libgit2-devel | rhel7-extras

|libindicator-gtk3-devel | rhel7-base

|libmodulemd-devel | rhel7-extras

|libseccomp-devel | rhel7-base

|libstdc++-static | rhel7-base

|nasm | rhel7-base

|objectweb-asm | rhel7-base

|openjade | rhel7-base

|openldap-servers | rhel7-base

|opensp | rhel7-base

|perl-Class-Singleton | rhel7-base

|perl-DateTime | rhel7-base

|perl-DateTime-Locale | rhel7-base

|perl-DateTime-TimeZone | rhel7-base

|perl-Devel-Symdump | rhel7-base

|perl-Digest-SHA1 | rhel7-base

|perl-HTML-Tree | rhel7-base

|perl-HTTP-Daemon | rhel7-base

|perl-IO-stringy | rhel7-base

|perl-List-MoreUtils | rhel7-base

|perl-Module-Implementation | rhel7-base

|perl-Package-DeprecationManager | rhel7-base

|perl-Package-Stash | rhel7-base

|perl-Package-Stash-XS | rhel7-base

|perl-Params-Validate | rhel7-base

|perl-Pod-Coverage | rhel7-base

|perl-SGMLSpm | rhel7-base

|perl-Test-Pod | rhel7-base

|perl-Test-Pod-Coverage | rhel7-base

|perl-XML-Twig | rhel7-base

|perl-YAML-Tiny | rhel7-base

|perltidy | rhel7-base

|qdox | rhel7-base

|regexp | rhel7-base

|texinfo | rhel7-base

|ustr | rhel7-base

|weld-parent | rhel7-base

|xmltoman | rhel7-base

|xorg-x11-apps | rhel7-base

|====


The following packages have been moved to the CodeReady Linux Builder repository within RHEL 8:

[options="header", cols="3,2,1"]
|====
|Package|Original RHEL 8 repository| Changed since
|apache-commons-collections-javadoc|rhel8-AppStream |RHEL 8.1
|apache-commons-collections-testframework|rhel8-AppStream |RHEL 8.1
|apache-commons-lang-javadoc|rhel8-AppStream |RHEL 8.1
|jakarta-commons-httpclient-demo|rhel8-AppStream |RHEL 8.1
|jakarta-commons-httpclient-javadoc|rhel8-AppStream |RHEL 8.1
|jakarta-commons-httpclient-manual|rhel8-AppStream |RHEL 8.1
|libcomps-devel | rhel8-BaseOS | RHEL 8.5
|openldap-servers | rhel8-BaseOS | RHEL 8.0
|velocity-demo|rhel8-AppStream |RHEL 8.1
|velocity-javadoc|rhel8-AppStream |RHEL 8.1
|velocity-manual|rhel8-AppStream |RHEL 8.1
|xerces-j2-demo|rhel8-AppStream |RHEL 8.1
|xerces-j2-javadoc|rhel8-AppStream |RHEL 8.1
|xml-commons-apis-javadoc|rhel8-AppStream |RHEL 8.1
|xml-commons-apis-manual|rhel8-AppStream |RHEL 8.1
|xml-commons-resolver-javadoc|rhel8-AppStream |RHEL 8.1
|====
