// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-concept-module-a.adoc
// * ID: [id="my-concept-module-a-{context}"]
// * Title: = My concept module A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="planning-an-upgrade-from-rhel-6-to-rhel-7{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Planning an upgrade
// In the title of concept modules, include nouns or noun phrases that are used in the body text. This helps readers and search engines find the information quickly.
// Do not start the title of concept modules with a verb. See also _Wording of headings_ in _The IBM Style Guide_.

An in-place upgrade is the recommended way to upgrade your system to a later major version of RHEL.

To ensure that you are aware of all major changes between RHEL 6 and RHEL 7, consult the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/migration_planning_guide/index[Migration Planning Guide] before beginning the in-place upgrade process. You can also verify whether your system is supported for an in-place upgrade by running the
ifdef:::upgrade-6-to-7-title[]
xref:assessing-upgrade-suitability_upgrading-from-rhel-6-to-rhel-7[Preupgrade Assistant].
endif::[]
ifndef:::upgrade-6-to-7-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/upgrading_from_rhel_6_to_rhel_7/index#assessing-upgrade-suitability_upgrading-from-rhel-6-to-rhel-7[Preupgrade Assistant].
endif::[]
The Preupgrade Assistant assesses your system for potential problems that could interfere or inhibit the upgrade before any changes are made to your system. See also 
ifdef:::upgrade-6-to-7-title[]
xref:known-issues_troubleshooting-rhel-6-to-rhel-7[Known Issues].
endif::[]
ifndef:::upgrade-6-to-7-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/upgrading_from_rhel_6_to_rhel_7/index#known-issues_troubleshooting-rhel-6-to-rhel-7[Known Issues].
endif::[] 

[NOTE]
====
Once you perform an in-place upgrade on your system, it is possible to get the previous working system back in limited configurations of the system by using the Red Hat Upgrade Tool integrated rollback capability or by using suitable custom backup and recovery solution, for example, by using the Relax-and-Recover (ReaR) utility. For more information, see link:https://access.redhat.com/solutions/3534561[Rolling back the upgrade].
====

This RHEL 6 to RHEL 7 upgrade procedure is fully supported if your RHEL system meets the following criteria:

* Red Hat Enterprise Linux 6.10: Your system must have the latest RHEL 6.10 packages installed.
* Architecture and variant: Only the indicated combinations of architecture and variant from the following matrix are supported:
+
[%header,cols=5*]
|===
| Product Variant
| Intel 64-bit architecture 
| IBM POWER, big endian
| IBM Z 64-bit architecture
| Intel 32-bit architecture 

| *Server Edition*
| Supported 
| Supported 
| Supported 
| Unsupported 

| *HPC Compute Node*
| Supported 
| N/A
| N/A
| Unsupported 

| *Desktop Edition* 
| Unsupported 
| N/A
| N/A
| Unsupported 

| *Workstation Edition*
| Unsupported 
| N/A
| N/A
| Unsupported 

| *Server running CloudForms software*
| Unsupported
| N/A
| N/A
| N/A

| *Server running Satellite software*
| Unsupported. To upgrade Satellite environments from RHEL 6 to RHEL 7, see the link:https://access.redhat.com/documentation/en-us/red_hat_satellite/6.2/html/installation_guide/upgrading_satellite_server_and_capsule_server#migrating_from_rhel_6_to_rhel_7[Red Hat Satellite Installation Guide]. 
| N/A
| N/A
| N/A
|===
+
[NOTE]
====
Upgrades of 64-bit IBM Z systems are supported unless Direct Access Storage Device (DASD) with Linux Disk Layout (LDL) is used.
====
+
* Supported packages: The in-place upgrade is supported for the following packages:
** Packages installed from the base repository, for example, the `rhel-6-server-rpms` if the system is on the RHEL 6 Server for the Intel architecture. 
** The Preupgrade Assistant, Red Hat Upgrade Tool, and any other packages that are required for the upgrade. 
+
[NOTE]
====
It is recommended to perform the upgrade with a minimum number of packages installed.
====
+
* File systems: File systems formats are intact. As a result, file systems have the same limitations as when they were originally created.
* Desktop: System upgrades with GNOME and KDE installs are unsupported. For more information, see link:https://access.redhat.com/solutions/976053[Upgrading from RHEL 6 to RHEL 7 on Gnome Desktop Environment failed].
* Virtualization: Upgrades with KVM or VMware virtualization are supported. Upgrades of RHEL on Microsoft Hyper-V are unsupported.
* High Availability: Upgrades of systems using the High Availability add-on are unsupported.
* Public Clouds: The in-place upgrade is unsupported for on-demand instances on Public Clouds.
* Third-party packages: The in-place upgrade is unsupported on systems using third-party packages, especially packages with third-party drivers that are needed for booting.
* The `/usr` directory: The in-place upgrade is unsupported on systems where the `/usr` directory is on a separate partition. For more information, see link:https://access.redhat.com/solutions/909603[Why does Red Hat Enterprise Linux 6 to 7 in-place upgrade fail if /usr is on separate partition?].

