:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_architectures.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.

[id="architectures_{context}"]
= Architectures

Red Hat Enterprise Linux 8 is distributed with the kernel version 4.18, which provides support for the following architectures:

* AMD and Intel 64-bit architectures
* The 64-bit ARM architecture
* IBM Power Systems, little endian
* 64-bit IBM Z

Make sure you purchase the appropriate subscription for each architecture. For more information, see link:https://access.redhat.com/products/red-hat-enterprise-linux/#addl-arch[Get Started with Red Hat Enterprise Linux - additional architectures]. For a list of available subscriptions, see https://access.redhat.com/management/products/[Subscription Utilization] on the Customer Portal.

Note that all architectures are supported by the standard `kernel` packages in RHEL 8; no `kernel-alt` package is needed.
