:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-concept-module-a.adoc
// * ID: [id="my-concept-module-a-{context}"]
// * Title: = My concept module A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="the-service-and-conf-files_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Anaconda services and configuration files

Anaconda services and configuration files are included in data/ directory. These files are required to start the add-ons service and to configure D-Bus.

Following are some examples of Anaconda Hello World add-on:

[[exam-addon-name-conf]]
.Example of _addon-name_.conf:
====
[literal,subs="+quotes,attributes,verbatim"]
....

<!DOCTYPE busconfig PUBLIC 
"-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN"
"http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>
       <policy user="root">
               <allow own="org.fedoraproject.Anaconda.Addons.HelloWorld"/>
               <allow send_destination="org.fedoraproject.Anaconda.Addons.HelloWorld"/>
       </policy>
       <policy context="default">
               <deny own="org.fedoraproject.Anaconda.Addons.HelloWorld"/>
               <allow send_destination="org.fedoraproject.Anaconda.Addons.HelloWorld"/>
       </policy>
</busconfig>

....

====

This file must be placed in the `/usr/share/anaconda/dbus/confs/` directory in the installation environment. The string `org.fedoraproject.Anaconda.Addons.HelloWorld` must correspond to the location of addon’s service on D-Bus.

[[exam-addon-name-service]]
.Example of _addon-name_.service:
====
[literal,subs="+quotes,attributes,verbatim"]
....

[D-BUS Service]
# Start the org.fedoraproject.Anaconda.Addons.HelloWorld service.
# Runs org_fedora_hello_world/service/__main__.py
Name=org.fedoraproject.Anaconda.Addons.HelloWorld
Exec=/usr/libexec/anaconda/start-module org_fedora_hello_world.service
User=root

....

====

This file must be placed in the `/usr/share/anaconda/dbus/services/` directory in the installation environment. The string `org.fedoraproject.Anaconda.Addons.HelloWorld` must correspond to the location of addon’s service on D-Bus. The value on the line starting with `Exec=` must be a valid command that starts the service in the installation environment.
