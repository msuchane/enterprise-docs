:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-concept-module-a.adoc
// * ID: [id="my-concept-module-a-{context}"]
// * Title: = My concept module A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="anaconda-add-on-structure_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Anaconda add-on structure

An [application]*Anaconda* add-on is a Python package that contains a directory with an `++__init__.py++` and other source directories (subpackages). Because Python allows you to import each package name only once, specify a unique name for the package top-level directory. You can use an arbitrary name, because add-ons are loaded regardless of their name - the only requirement is that they must be placed in a specific directory.

The suggested naming convention for add-ons is similar to Java packages or D-Bus service names. 

To make the directory name a unique identifier for a Python package, prefix the add-on name with the reversed domain name of your organization, using underscores (`_`) instead of dots. For example, `com_example_hello_world`. 

[IMPORTANT]
====

Make sure to create an `++__init__.py++` file in each directory. Directories missing this file are considered as invalid Python packages.

====

When writing an add-on,  ensure the following:

//* Every function that is supported in the installer is supported in Kickstart; GUI and TUI support is optional. 

* Support for each interface (graphical interface and text interface) is available in a separate subpackage and these subpackages are named `gui` for the graphical interface and `tui` for the text-based interface. 

* The `gui` and `tui` packages contain a `spokes` subpackage.
footnote:[The [package]*gui* package may also contain a [package]*categories* subpackage if the add-on needs to define a new category, but this is not recommended.]

* Modules contained in the packages have an arbitrary name.

* The `gui/` and `tui/` directories contain Python modules with any name.

* There is a service that performs the actual work of the addon. This service can be written in Python or any other language.

* The service implements support for D-Bus and Kickstart.

* The addon contains files that enable automatic startup of the service.

Following is a sample directory structure for an add-on which supports every interface (Kickstart, GUI and TUI):

[[exam-sample-addon-structure]]
.Sample add-on structure
====

[literal,subs="+quotes,attributes,verbatim"]
....

com_example_hello_world
├─ gui
│  ├─ __init__.py
│  └─ spokes
│     └─ __init__.py
└─ tui
   ├─ __init__.py
   └─ spokes
   └─ __init__.py
      
....

====

Each package must contain at least one module with an arbitrary name defining the classes that are inherited from one or more classes defined in the API. 

NOTE: For all add-ons, follow Python's link:http://www.python.org/dev/peps/pep-0008/[PEP 8] and link:http://www.python.org/dev/peps/pep-0257/[PEP 257] guidelines for docstring conventions. There is no consensus on the format of the actual content of docstrings in [application]*Anaconda*pass:attributes[{blank}]; the only requirement is that they are human-readable. If you plan to use auto-generated documentation for your add-on, docstrings should follow the guidelines for the toolkit you use to accomplish this.

You can include a category subpackage if an add-on needs to define a new category, but this is not recommended.

