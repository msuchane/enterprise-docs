:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

:experimental:

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="creating-a-product-img-file_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Creating a product.img file
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

A `product.img` image file is an archive containing new installer files that replace the existing ones at runtime.

During a system boot, [application]*Anaconda* loads the product.img file from the images/ directory on the boot media. It then uses the files that are present in this directory to replace identically named files in the installer's file system. The files when replaced customizes the installer (for example, for replacing default images with custom ones).

Note: The `product.img` image must contain a directory structure identical to the installer. For more information about the installer directory structure, see the table below.

[[tabl-product-img-directories]]
.Installer directory structure and custom contents

[options="header"]
|===
|Type of custom content|File system location
|Pixmaps (logo, sidebar, top bar, and so on.)|`/usr/share/anaconda/pixmaps/`
|GUI stylesheet|`/usr/share/anaconda/anaconda-gtk.css`
|Anaconda add-ons|`/usr/share/anaconda/addons/`
|Product configuration files|`/etc/anaconda/product.d/`
|Custom configuration files|`/etc/anaconda/conf.d/`
|Anaconda DBus service conf files|`/usr/share/anaconda/dbus/confs/`
|Anaconda DBus service files|`/usr/share/anaconda/dbus/services/`
|===

The procedure below explains how to create a `product.img` file.

.Procedure
. Navigate to a working directory such as `/tmp`, and create a subdirectory named `product/`:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
`$` [command]`cd /tmp`
....

. Create a subdirectory product/
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
`$` [command]`mkdir product/`
....

. Create a directory structure identical to the location of the file you want to replace. For example, if you want to test an add-on that is present in the `/usr/share/anaconda/addons` directory on the installation system, create the same structure in your working directory:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
`$` [command]`mkdir -p product/usr/share/anaconda/addons`
....
+
[NOTE]
====

To view the installer's runtime file, boot the installation and switch to virtual console 1 (kbd:[Ctrl + Alt + F1]) and then switch to the second [application]*tmux* window (kbd:[Ctrl + b + 2]). A shell prompt that can be used to browse a file system opens.

====

. Place your customized files (in this example, custom add-on for [application]*Anaconda*) into the newly created directory:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
`$` [command]`cp -r _~/path/to/custom/addon/_ product/usr/share/anaconda/addons/`
....

. Repeat steps 3 and 4  (create a directory structure and place the custom files into it) for every file you want to add to the installer.

. Create a `.buildstamp` file in the root of the directory. The `.buildstamp` file describes the system version, the product and several other parameters. The following is an example of a `.buildstamp` file from {productname}{nbsp}8.4:
+
[literal,subs="+quotes,attributes,verbatim"]
....

[Main]
Product=Red Hat Enterprise Linux
Version=8.4
BugURL=https://bugzilla.redhat.com/
IsFinal=True
UUID=202007011344.x86_64
[Compose]
Lorax=28.14.49-1

....
+
The `IsFinal` parameter specifies whether the image is for a release (GA) version of the product (`True`), or a pre-release such as Alpha, Beta, or an internal milestone (`False`).

. Navigate to the `product/` directory, and create the `product.img` archive:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
`$` [command]`cd product`
....
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
`$` [command]`find . | cpio -c -o | gzip -9cv > ../product.img`
....
+
This creates a `product.img` file one level above the `product/` directory.

. Move the `product.img` file to the `images/` directory of the extracted ISO image.

The product.img file is now created and the customizations that you want to make are placed in the respective directories.

[NOTE]
====
Instead of adding the `product.img` file on the boot media, you can place this file into a different location and use the [option]`inst.updates=` boot option at the boot menu to load it. In that case, the image file can have any name, and it can be placed in any location (USB flash drive, hard disk, HTTP, FTP or NFS server), as long as this location is reachable from the installation system.
====

ifeval::[{ProductNumber} == 8]
See the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/chap-anaconda-boot-options[Anaconda Boot Options] for more information about Anaconda boot options.
endif::[]
