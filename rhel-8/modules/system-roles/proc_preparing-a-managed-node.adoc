:_mod-docs-content-type: PROCEDURE

[id="proc_preparing-a-managed-node_{context}"]
= Preparing a managed node
Managed nodes are the systems listed in the inventory and which will be configured by the control node according to the playbook. You do not have to install Ansible on managed hosts.


.Prerequisites

* You prepared the control node. For more information, see
ifdef::automating-system-administration-by-using-rhel-system-roles[]
xref:proc_preparing-a-control-node_assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[].
endif::[]
ifndef::automating-system-administration-by-using-rhel-system-roles[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles#proc_preparing-a-control-node_assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[Preparing a control node on RHEL {ProductNumber}].
endif::[]

* You have SSH access from the control node.
+
[IMPORTANT]
Direct SSH access as the `root` user is a security risk. To reduce this risk, you will create a local user on this node and configure a `sudo` policy when preparing a managed node. Ansible on the control node can then use the local user account to log in to the managed node and run playbooks as different users, such as `root`.


.Procedure

. Create a user named `ansible`:
+
[literal,subs="+quotes"]
....
[root@managed-node-01]# *useradd ansible*
....
+
The control node later uses this user to establish an SSH connection to this host.

. Set a password for the `ansible` user:
+
[literal,subs="+quotes"]
....
[root@managed-node-01]# *passwd ansible*
Changing password for user ansible.
New password: *_<password>_*
Retype new password: *_<password>_*
passwd: all authentication tokens updated successfully.
....
+
You must enter this password when Ansible uses `sudo` to perform tasks as the `root` user.

. Install the `ansible` user's SSH public key on the managed node:

.. Log in to the control node as the `ansible` user, and copy the SSH public key to the managed node:
+
[literal,subs="+quotes"]
....
[ansible@control-node]$ *ssh-copy-id managed-node-01.example.com*
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/ansible/.ssh/id_rsa.pub"
The authenticity of host 'managed-node-01.example.com (192.0.2.100)' can't be established.
ECDSA key fingerprint is SHA256:9bZ33GJNODK3zbNhybokN/6Mq7hu3vpBXDrCxe7NAvo.
....

.. When prompted, connect by entering `yes`:
+
[literal,subs="+quotes"]
....
Are you sure you want to continue connecting (yes/no/[fingerprint])? *yes*
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
....

.. When prompted, enter the password:
+
[literal,subs="+quotes"]
....
ansible@managed-node-01.example.com's password: *_<password>_*

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'managed-node-01.example.com'"
and check to make sure that only the key(s) you wanted were added.
....

.. Verify the SSH connection by remotely executing a command on the control node:
+
[literal,subs="+quotes"]
....
[ansible@control-node]$ *ssh managed-node-01.example.com whoami*
ansible
....

. Create a `sudo` configuration for the `ansible` user:

.. Create and edit the `/etc/sudoers.d/ansible` file by using the `visudo` command:
+
[literal,subs="+quotes"]
....
[root@managed-node-01]# *visudo /etc/sudoers.d/ansible*
....
+
The benefit of using `visudo` over a normal editor is that this utility provides basic sanity checks and checks for parse errors before installing the file.

.. Configure a `sudoers` policy in the `/etc/sudoers.d/ansible` file that meets your requirements, for example:

*** To grant permissions to the `ansible` user to run all commands as any user and group on this host after entering the `ansible` user's password, use:
+
[literal,subs="+quotes"]
....
ansible   ALL=(ALL) ALL
....

*** To grant permissions to the `ansible` user to run all commands as any user and group on this host without entering the `ansible` user's password, use:
+
[literal,subs="+quotes"]
....
ansible   ALL=(ALL) NOPASSWD: ALL
....

+
Alternatively, configure a more fine-granular policy that matches your security requirements. For further details on `sudoers` policies, see the `sudoers(5)` manual page.

.Verification

. Verify that you can execute commands from the control node on an all managed nodes:
+
[literal,subs="+quotes",options="nowrap",::,role=white-space-pre]
....
[ansible@control-node]$ *ansible all -m ping*
BECOME password: *_<password>_*
managed-node-01.example.com | *SUCCESS* => {
    	"ansible_facts": {
    	    "discovered_interpreter_python": "/usr/bin/python3"
    	},
    	"changed": false,
    	"ping": "pong"
}
...
....
+
The hard-coded all group dynamically contains all hosts listed in the inventory file.

. Verify that privilege escalation works correctly by running the `whoami` utility on a managed host by using the Ansible `command` module:
+
[literal,subs="+quotes"]
....
[ansible@control-node]$ *ansible managed-node-01.example.com -m command -a whoami*
BECOME password: *_<password>_*
managed-node-01.example.com | CHANGED | rc=0 >>
*root*
....
+
If the command returns root, you configured `sudo` on the managed nodes correctly.

[role="_additional-resources"]
.Additional resources

* {blank}
ifdef::automating-system-administration-by-using-rhel-system-roles[]
xref:proc_preparing-a-control-node_assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[]
endif::[]
ifndef::automating-system-administration-by-using-rhel-system-roles[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles#proc_preparing-a-control-node_assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[Preparing a control node on RHEL {ProductNumber}]
endif::[]
* `sudoers(5)` manual page
