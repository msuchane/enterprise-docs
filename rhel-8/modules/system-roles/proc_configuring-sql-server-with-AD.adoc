:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-sql-server-with-AD_{context}"]
= Configuring SQL Server to authenticate with Active Directory (AD) Server 

The following procedure shows how to configure SQL Server to authenticate with Active Directory (AD) Server.

.Prerequisites

* An Active Directory domain controller is configured on your network.

* An applicable RDNS (Reverse DNS) zone exists for both the domain controller and the IP address of the Linux machine that will be running SQL Server.

* A PTR record that points to your domain controllers exists.

* The SQL Server host resolves relative domain name, fully qualified domain name, and the IP of the domain controller to the fully qualified domain name of the domain controller.
 
.Procedure

. Log in to your AD server through the web UI.
. Navigate to *Tools* > *Active Directory Users and Computers* > *domain.com* > *Users* > *sqluser* > *Account*.
. In the *Account options* list, select *This account supports Kerberos AES 128 bit encryption* and *This account supports Kerberos AES 256 bit encryption*.
. Click *Apply*


.Verification

. Use `ssh` to log in to the _client.domain.com_ machine:
+
[subs="quotes"]
----
# ssh -l _&lt;sqluser&gt;_@_&lt;domain.com&gt;_ _&lt;client.domain.com&gt;_
----
. Obtain the Kerberos ticket for the Administrator user:
+
[subs="quotes"]
----
# kinit Administrator@_&lt;domain.com&gt;_
----
+
. Use the `sqlcmd` utility to log in to the SQL Server and, for example, run the following query to view the current user:
+
[subs="quotes"]
----
# /opt/mssql-tools/bin/sqlcmd -S. -Q 'SELECT SYSTEM_USER'
----


