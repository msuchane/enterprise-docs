:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="connecting-a-rhel-system-directly-to-ad-using-the-ad-integration-system-role_{context}"]
= Connecting a RHEL system directly to AD by using the `ad_integration` RHEL system role
You can use the `ad_integration` system role to configure a direct integration between a RHEL system and an AD domain by running an Ansible playbook.

[NOTE]
====
Starting with RHEL8, RHEL no longer supports RC4 encryption by default. If it is not possible to enable AES in the AD domain, you must enable the `AD-SUPPORT` crypto policy and allow RC4 encryption in the playbook.
====

[IMPORTANT]
====
Time between the RHEL server and AD must be synchronized. You can ensure this by using the `timesync` system role in the playbook.
====

In this example, the RHEL system joins the `domain.example.com` AD domain, by using the AD `Administrator` user and the password for this user stored in the Ansible vault. The playbook also sets the `AD-SUPPORT` crypto policy and allows RC4 encryption. To ensure time synchronization between the RHEL system and AD, the playbook sets the `adserver.domain.example.com` server as the `timesync` source.


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]
+
* The following ports on the AD domain controllers are open and accessible from the RHEL server:
+
.Ports Required for Direct Integration of Linux Systems into AD Using the `ad_integration` system role
[options="header"]
|===
|Source Port|Destination Port|Protocol|Service
|1024:65535|53|UDP and TCP|DNS
|1024:65535|389|UDP and TCP|LDAP
|1024:65535|636|TCP|LDAPS
|1024:65535|88|UDP and TCP|Kerberos
|1024:65535|464|UDP and TCP|Kerberos change/set password (`kadmin`)
|1024:65535|3268|TCP|LDAP Global Catalog
|1024:65535|3269|TCP|LDAP Global Catalog SSL/TLS
|1024:65535|123|UDP|NTP/Chrony (Optional)
|1024:65535|323|UDP|NTP/Chrony (Optional)
|===



.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Configure a direct integration between a RHEL system and an AD domain
  hosts: managed-node-01.example.com
  roles:
    - rhel-system-roles.ad_integration
  vars:
    ad_integration_realm: "domain.example.com"
    ad_integration_password: !vault | vault encrypted password
    ad_integration_manage_crypto_policies: true
    ad_integration_allow_rc4_crypto: true
    ad_integration_timesync_source: "adserver.domain.example.com"
....

. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.

. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification
* Display an AD user details, such as the `administrator` user:
+
[subs="+quotes"]
....
$ *getent passwd administrator@ad.example.com*
administrator@ad.example.com:*:1450400500:1450400513:Administrator:/home/administrator@ad.example.com:/bin/bash
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.ad_integration/README.md` file
* `/usr/share/doc/rhel-system-roles/ad_integration/` directory

