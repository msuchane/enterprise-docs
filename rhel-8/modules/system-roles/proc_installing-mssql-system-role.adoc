:_mod-docs-content-type: PROCEDURE
[id="proc_installing-mssql-system-role_{context}"]
= Installing the `microsoft.sql.server` Ansible role

[role="_abstract"]
The `microsoft.sql.server` Ansible role is part of the [package]`ansible-collection-microsoft-sql` package.

.Prerequisites
* `root` access

.Procedure

. Install Ansible Core which is available in the RHEL 8 AppStream repository:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install ansible-core*
----

. Install the `microsoft.sql.server` Ansible role:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install ansible-collection-microsoft-sql*
----
