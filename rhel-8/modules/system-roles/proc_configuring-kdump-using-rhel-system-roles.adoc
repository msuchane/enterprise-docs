:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="configuring-kdump-using-rhel-system-roles_{context}"]
= Configuring the kernel crash dumping mechanism by using the `kdump` {RHELSystemRoles}
You can set basic kernel dump parameters on multiple systems by using the `kdump` system role by running an Ansible playbook.

[WARNING]
====
The `kdump` Systeme Role replaces the `kdump` configuration of the managed hosts entirely by replacing the `/etc/kdump.conf` file. Additionally, if the `kdump` role is applied, all previous `kdump` settings are also replaced, even if they are not specified by the role variables, by replacing the `/etc/sysconfig/kdump` file.
====


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- hosts: managed-node-01.example.com
  roles:
    - rhel-system-roles.kdump
  vars:
    kdump_path: /var/crash
....


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.kdump/README.md` file
* `/usr/share/doc/rhel-system-roles/kdump/` directory

