:_mod-docs-content-type: PROCEDURE
:experimental:

[id="configuring-server-logging-with-tls_{context}"]
= Configuring server logging with TLS

[role="_abstract"]
You can use an Ansible playbook with the `logging` {RHELSystemRoles} to configure logging on RHEL servers and set them to receive logs from a remote logging system using TLS encryption.

This procedure creates a private key and certificate, and configures TLS on all hosts in the server group in the Ansible inventory.

[NOTE]
====
You do not have to call the `certificate` {RHELSystemRoles} in the playbook to create the certificate. The `logging` {RHELSystemRoles} calls it automatically.

In order for the CA to be able to sign the created certificate, the managed nodes must be enrolled in an IdM domain.
====


.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]
// Specific prerequisites
* The managed nodes are enrolled in an IdM domain.
ifeval::[{ProductNumber} >= 9]
* If the logging server you want to configure on the manage node runs RHEL 9.2 or later and the FIPS mode is enabled, clients must either support the Extended Master Secret (EMS) extension or use TLS 1.3. TLS 1.2 connections without EMS fail. For more information, see the link:https://access.redhat.com/solutions/7018256[TLS extension "Extended Master Secret" enforced] Knowledgebase article.
endif::[]


.Procedure

. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Deploying remote input and remote_files output with certs
  hosts: managed-node-01.example.com
  roles:
    - rhel-system-roles.logging
  vars:
    logging_certificates:
      - name: logging_cert
        dns: ['localhost', 'www.example.com']
        ca: ipa
    logging_pki_files:
      - ca_cert: /local/path/to/ca_cert.pem
        cert: /local/path/to/logging_cert.pem
        private_key: /local/path/to/logging_cert.pem
    logging_inputs:
      - name: input_name
        type: remote
        tcp_ports: 514
        tls: true
        permitted_clients: ['clients.example.com']
    logging_outputs:
      - name: output_name
        type: remote_files
        remote_log_path: /var/log/remote/%FROMHOST%/%PROGRAMNAME:::secpath-replace%.log
        async_writing: true
        client_count: 20
        io_buffer_size: 8192
    logging_flows:
      - name: flow_name
        inputs: [input_name]
        outputs: [output_name]
....
+
The playbook uses the following parameters:

`logging_certificates`:: The value of this parameter is passed on to `certificate_requests` in the `certificate` {RHELSystemRoles} and used to create a private key and certificate.
`logging_pki_files`:: Using this parameter, you can configure the paths and other settings that logging uses to find the CA, certificate, and key files used for TLS, specified with one or more of the following sub-parameters: `ca_cert`, `ca_cert_src`, `cert`, `cert_src`, `private_key`, `private_key_src`, and `tls`.
+
[NOTE]
====
If you are using `logging_certificates` to create the files on the target node, do not use `ca_cert_src`, `cert_src`, and `private_key_src`, which are used to copy files not created by `logging_certificates`.
====

`ca_cert`:: Represents the path to the CA certificate file on the target node. Default path is `/etc/pki/tls/certs/ca.pem` and the file name is set by the user.
`cert`:: Represents the path to the certificate file on the target node. Default path is `/etc/pki/tls/certs/server-cert.pem` and the file name is set by the user.
`private_key`:: Represents the path to the private key file on the target node. Default path is `/etc/pki/tls/private/server-key.pem` and the file name is set by the user.
`ca_cert_src`:: Represents the path to the CA certificate file on the control node which is copied to the target host to the location specified by `ca_cert`. Do not use this if using `logging_certificates`.
`cert_src`:: Represents the path to a certificate file on the control node which is copied to the target host to the location specified by `cert`. Do not use this if using `logging_certificates`.
`private_key_src`:: Represents the path to a private key file on the control node which is copied to the target host to the location specified by `private_key`. Do not use this if using `logging_certificates`.
`tls`:: Setting this parameter to `true` ensures secure transfer of logs over the network. If you do not want a secure wrapper, you can set `tls: false`.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.logging/README.md` file
* `/usr/share/doc/rhel-system-roles/logging/` directory
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/automating_system_administration_by_using_rhel_system_roles/index#requesting-certificates-using-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[Requesting certificates using RHEL system roles].
