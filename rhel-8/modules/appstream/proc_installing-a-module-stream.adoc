:_mod-docs-content-type: PROCEDURE
[id="installing-a-module-stream_{context}"]
= Installing modular content
// Changing the subtitle but not the ID to prevent broken links
// User Story: As a sysadmin, I need to know how to find and install RPMs, SCLs, and modules using AppStream so I can ensure that I have the modules I need.

To install modular content provided by a module stream or a profile, complete the following steps.

.Prerequisites

ifdef::appstream-book[]
* You understand the xref:module-streams_introduction-to-modules[concept of an _active module stream_].
endif::[]
ifndef::appstream-book[]
* You must understand the concept of an _active module stream_.
endif::[]
* You do not have any packages installed from another stream of the same module.


.Procedure


* To install a selected module stream, use:
+
[subs="quotes,attributes"]
----
# *yum module install __module-name__:__stream__*
----
+
By running this command, you automatically enable selected stream. Note that if a default profile is defined for the stream, this profile is automatically installed.
+
IMPORTANT: Always consider the module stream's link:https://access.redhat.com/support/policy/updates/rhel-app-streams-life-cycle[life cycle].
+
* To install a selected profile of the module stream, use:
+
[subs="quotes,attributes"]
----
# *yum module install __module-name__:__stream__/__profile__*
----
+
By running this command, you enable the stream and install the recommended set of packages for a given stream (version) and profile (purpose) of the module.


ifdef::appstream-book[]

[role="_additional-resources"]
.Additional resources
* xref:introduction-to-modules_using-appstream[Introduction to modules]
* xref:commands-for-installing-rhel-8-content_{context}[Commands for installing RHEL 8 content]
* link:https://access.redhat.com/support/policy/updates/rhel-app-streams-life-cycle[Red Hat Enterprise Linux Application Streams Life Cycle]
endif::[]
