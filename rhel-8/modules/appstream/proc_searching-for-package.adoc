:_mod-docs-content-type: PROCEDURE
[id="searching-for-package_{context}"]
= Searching for a package

// User Story: As a sysadmin, I need to know what is available through the AppStream so I can determine what RPMs, SCLs, and modules to download and install.


To find a package providing a particular application or other content, complete the following steps.

.Procedure

. Search for a package with a text string, such as application name:
+
[subs="quotes,attributes"]
----
$ *yum search _"text string"_*
----

. View details about a package:
+
[subs="quotes,attributes"]
----
$ *yum info __package__*
----

// NOTE: Software Collections are technically packages and are listed and displayed as such.
