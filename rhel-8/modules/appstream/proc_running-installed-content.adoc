:_mod-docs-content-type: PROCEDURE
[id="running-installed-content_{context}"]
= Running installed content

// User Story: As a sysadmin, I need to know how to run the RPMs, SCLs, and modules I've pulled from the AppStream.

New commands are usually enabled after you install content from RHEL 8 repositories. If the commands originated from RPM packages that were enabled by a module, the experience of using these command should be no different. 

.Procedure

* To run the new commands, enter them directly:
+
[subs="quotes,attributes"]
----
$ _command_
----
+
Replace _command_ with the name of the command you want to run. 

[NOTE]
====
In RHEL 8, GCC Toolset is packaged as a Software Collection. To run a command from a component packaged as a Software Collection, use:

[subs="quotes,attributes"]
----
$ scl enable __collection__ '__command__'
----

Replace _collection_ with the name of the Software Collection. 

For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/developing_c_and_cpp_applications_in_rhel_8/additional-toolsets-for-development_developing-applications#gcc-toolset_assembly_additional-toolsets-for-development[Using GCC Toolset].
====

