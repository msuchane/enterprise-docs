:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-logging-of-dropped-packets-to-a-file_{context}"]
= Configuring logging of dropped packets to a file
By default, `systemd` logs kernel messages, such as for dropped packets, to the journal. Additionally, you can configure the `rsyslog` service to log such entries to a separate file. To ensure that the log file does not grow infinitely, configure a rotation policy.


.Prerequisites

* The `rsyslog` package is installed.
* The `rsyslog` service is running.


.Procedure

. Create the `/etc/rsyslog.d/nftables.conf` file with the following content:
+
[source,bash]
....
:msg, startswith, "nft drop" -/var/log/nftables.log
& stop
....
+
Using this configuration, the `rsyslog` service logs dropped packets to the `/var/log/nftables.log` file instead of `/var/log/messages`.

. Restart the `rsyslog` service:
+
[literal,subs="+quotes",options="nowrap",role=white-space-pre]
....
# **systemctl restart rsyslog**
....

. Create the `/etc/logrotate.d/nftables` file with the following content to rotate `/var/log/nftables.log` if the size exceeds 10 MB:
+
[source,bash]
....
/var/log/nftables.log {
  size +10M
  maxage 30
  sharedscripts
  postrotate
    /usr/bin/systemctl kill -s HUP rsyslog.service >/dev/null 2>&1 || true
  endscript
}
....
+
The `maxage 30` setting defines that `logrotate` removes rotated logs older than 30 days during the next rotation operation.


[role="_additional-resources"]
.Additional resources
* `rsyslog.conf(5)` man page
* `logrotate(8)` man page

