:_mod-docs-content-type: PROCEDURE
:experimental:

[id="restoring-the-nftables-rule-set-from-a-file_{context}"]
= Restoring the nftables rule set from a file

[role="_abstract"]
You can restore the `nftables` rule set from a file.


.Procedure

* To restore `nftables` rules:
+
** If the file to restore is in the format produced by `nft list ruleset` or contains `nft` commands directly:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *nft -f _file_.nft*
----
** If the file to restore is in JSON format:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *nft -j -f _file_.json*
----
  
