:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
// assembly_controlling-network-traffic-using-firewalld.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_disabling-all-traffic-in-case-of-emergency-using-cli.adoc[leveloffset=+1]

[id="controlling-traffic-through-the-cli_{context}"]
= Controlling traffic through the CLI

[role="_abstract"]

You can use the `firewall-cmd` command to:

* disable networking traffic
* enable networking traffic

As a result, you can for example enhance your system defenses, ensure data privacy or optimize network resources.

[IMPORTANT]
====
Enabling panic mode stops all networking traffic. For this reason, it should be used only when you have the physical access to the machine or if you are logged in using a serial console.
====


.Procedure

. To immediately disable networking traffic, switch panic mode on:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --panic-on*
----


. Switching off panic mode reverts the firewall to its permanent settings. To switch panic mode off, enter:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --panic-off*
----

.Verification

* To see whether panic mode is switched on or off, use:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --query-panic*
----
  
