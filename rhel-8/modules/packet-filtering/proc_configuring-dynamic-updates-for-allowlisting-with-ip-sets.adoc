:_mod-docs-content-type: PROCEDURE

[id="configuring-dynamic-updates-for-allowlisting-with-ip-sets_{context}"]
= Configuring dynamic updates for allowlisting with IP sets

[role="_abstract"]
You can make near real-time updates to flexibly allow specific IP addresses or ranges in the IP sets even in unpredictable conditions. These updates can be triggered by various events, such as detection of security threats or changes in the network behavior. Typically, such a solution leverages automation to reduce manual effort and improve security by responding quickly to the situation.


.Prerequisites

* The `firewalld` service is running.



.Procedure

. Create an IP set with a meaningful name:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --permanent --new-ipset=__allowlist__ --type=hash:ip*
----
+
The new IP set called `allowlist` contains IP addresses that you want your firewall to allow.


. Add a dynamic update to the IP set:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --permanent --ipset=__allowlist__ --add-entry=__198.51.100.10__*
----
+
This configuration updates the `allowlist` IP set with a newly added IP address that is allowed to pass network traffic by your firewall.


. Create a firewall rule that references the previously created IP set:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --permanent --zone=public --add-source=ipset:__allowlist__*
----
+
Without this rule, the IP set would not have any impact on network traffic. The default firewall policy would prevail.


. Reload the firewall configuration to apply the changes:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --reload*
----



.Verification

. List all IP sets:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --get-ipsets*
allowlist
----


. List the active rules:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --list-all*
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s1
  **sources: ipset:allowlist**
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  ...
----
+
The `sources` section of the command-line output provides insights to what origins of traffic (hostnames, interfaces, IP sets, subnets, and others) are permitted or denied access to a particular firewall zone. In this case, the IP addresses contained in the `allowlist` IP set are allowed to pass traffic through the firewall for the `public` zone.


. Explore the contents of your IP set:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *cat /etc/firewalld/ipsets/allowlist.xml*
<?xml version="1.0" encoding="utf-8"?>
<ipset type="hash:ip">
  <entry>198.51.100.10</entry>
</ipset>
----


.Next steps

* Use a script or a security utility to fetch your threat intelligence feeds and update `allowlist` accordingly in an automated fashion.


[role="_additional-resources"]
.Additional resources

* `firewall-cmd(1)` manual page

