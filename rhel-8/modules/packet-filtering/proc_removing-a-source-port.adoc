:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_using-and-configuring-firewalld.adoc

[id="removing-a-source-port_{context}"]
= Removing a source port

[role="_abstract"]
By removing a source port you disable sorting the traffic based on a port of origin.

.Procedure

* To remove a source port:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *firewall-cmd --zone=zone-name --remove-source-port=<port-name>/<tcp|udp|sctp|dccp>*
----
  
