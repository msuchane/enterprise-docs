:_mod-docs-content-type: REFERENCE
:experimental:
// included in assembly_managing-icmp-requests.adoc

[id="listing-and-blocking-icmp-requests_{context}"]
= Listing and blocking ICMP requests

[role="_abstract"]
.Listing `ICMP` requests

The `ICMP` requests are described in individual XML files that are located in the `/usr/lib/firewalld/icmptypes/` directory. You can read these files to see a description of the request. The `firewall-cmd` command controls the `ICMP` requests manipulation.

* To list all available `ICMP` types:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --get-icmptypes`
----

* The `ICMP` request can be used by IPv4, IPv6, or by both protocols. To see for which protocol the `ICMP` request has used:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --info-icmptype=<icmptype>`
----

* The status of an `ICMP` request shows `yes` if the request is currently blocked or `no` if it is not. To see if an `ICMP` request is currently blocked:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --query-icmp-block=<icmptype>`
----

.Blocking or unblocking `ICMP` requests

When your server blocks `ICMP` requests, it does not provide the information that it normally would. However, that does not mean that no information is given at all. The clients receive information that the particular `ICMP` request is being blocked (rejected). Blocking the `ICMP` requests should be considered carefully, because it can cause communication problems, especially with IPv6 traffic.

* To see if an `ICMP` request is currently blocked:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --query-icmp-block=<icmptype>`
----

* To block an `ICMP` request:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --add-icmp-block=<icmptype>`
----

* To remove the block for an `ICMP` request:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --remove-icmp-block=<icmptype>`
----

.Blocking `ICMP` requests without providing any information at all

Normally, if you block `ICMP` requests, clients know that you are blocking it. So, a potential attacker who is sniffing for live IP addresses is still able to see that your IP address is online. To hide this information completely, you have to drop all `ICMP` requests.

* To block and drop all `ICMP` requests:

* Set the target of your zone to `DROP`:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --permanent --set-target=DROP`
----

Now, all traffic, including `ICMP` requests, is dropped, except traffic which you have explicitly allowed.

To block and drop certain `ICMP` requests and allow others:

. Set the target of your zone to `DROP`:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --permanent --set-target=DROP`
----

. Add the ICMP block inversion to block all `ICMP` requests at once:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --add-icmp-block-inversion`
----

. Add the ICMP block for those `ICMP` requests that you want to allow:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --add-icmp-block=<icmptype>`
----

. Make the new settings persistent:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --runtime-to-permanent`
----

The _block inversion_ inverts the setting of the `ICMP` requests blocks, so all requests, that were not previously blocked, are blocked because of the target of your zone changes to `DROP`. The requests that were blocked are not blocked. This means that if you want to unblock a request, you must use the blocking command.

To revert the block inversion to a fully permissive setting:

. Set the target of your zone to `default` or `ACCEPT`:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --permanent --set-target=default`
----

. Remove all added blocks for `ICMP` requests:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --remove-icmp-block=<icmptype>`
----

. Remove the `ICMP` block inversion:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --remove-icmp-block-inversion`
----

. Make the new settings persistent:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# `firewall-cmd --runtime-to-permanent`
----
  
