[id="configuring-a-dmz-firewalld-zone-by-using-the-firewalld-rhel-system-role_{context}"]
= Configuring a `firewalld` DMZ zone by using the `firewall` {RHELSystemRoles}

As a system administrator, you can use the `firewall` {RHELSystemRoles} to configure a `dmz` zone on the *enp1s0* interface to permit `HTTPS` traffic to the zone. In this way, you enable external users to access your web servers.

Perform this procedure on the Ansible control node.


.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]
// The following are prerequisites specific for this task:
// none


.Procedure

. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Configure firewalld
  hosts: managed-node-01.example.com
  tasks:
    - name: Creating a DMZ with access to HTTPS port and masquerading for hosts in DMZ
      ansible.builtin.include_role:
        name: rhel-system-roles.firewall
      vars:
        firewall:
          - zone: dmz
            interface: enp1s0
            service: https
            state: enabled
            runtime: true
            permanent: true
....


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification

* On the managed node, view detailed information about the `dmz` zone:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# *firewall-cmd --zone=dmz --list-all*
dmz (active)
  target: default
  icmp-block-inversion: no
  *interfaces: enp1s0*
  sources:
  *services: https* ssh
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.firewall/README.md` file
* `/usr/share/doc/rhel-system-roles/firewall/` directory
