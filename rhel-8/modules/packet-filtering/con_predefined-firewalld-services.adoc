:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// getting-started-with-firewalld

// This module can be included from assemblies using the following include statement:
// include::<path>/con_predefined-services.adoc[leveloffset=+1]

[id="predefined-firewalld-services_{context}"]
= Predefined firewalld services

[role="_abstract"]

The `firewalld` service is a predefined set of firewall rules that define access to a specific application or network service. Each service represents a combination of the following elements:

* Local port
* Network protocol
* Associated firewall rules
* Source ports and destinations
* Firewall helper modules that load automatically if a service is enabled

A service simplifies packet filtering and saves you time because it achieves several tasks at once. For example, `firewalld` can perform the following tasks at once:
 
 * Open a port
 * Define network protocol
 * Enable packet forwarding 
 
Service configuration options and generic file information are described in the `firewalld.service(5)` man page. The services are specified by means of individual XML configuration files, which are named in the following format: `pass:attributes[{blank}]_service-name_.xml`. Protocol names are preferred over service or application names in `firewalld`.

You can configure `firewalld` in the following ways:

* Use utilities:
+
** `firewall-config` - graphical utility
** `firewall-cmd` - command-line utility
** `firewall-offline-cmd` - command-line utility

* Edit the XML files in the `/etc/firewalld/services/` directory.
+
If you do not add or change the service, no corresponding XML file exists in `/etc/firewalld/services/`. You can use the files in `/usr/lib/firewalld/services/` as templates.

[role="_additional-resources"]
.Additional resources
* The `firewalld.service(5)` man page
