:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/con_including-files-in-an-nftables-script.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.
[id="including-files-in-nftables-scripts_{context}"]
= Including files in nftables scripts

[role="_abstract"]
In the `nftables` scripting environment, you can include other scripts by using the `include` statement.

If you specify only a file name without an absolute or relative path, `nftables` includes files from the default search path, which is set to `/etc` on {RHEL}.

.Including files from the default search directory
====
To include a file from the default search directory:

[source]
----
include "example.nft"
----
====

.Including all *.nft files from a directory
====
To include all files ending with `*.nft` that are stored in the `/etc/nftables/rulesets/` directory:

[source]
----
include "/etc/nftables/rulesets/*.nft"
----

Note that the `include` statement does not match files beginning with a dot.
====


[role="_additional-resources"]
.Additional resources
* The `Include files` section in the `nft(8)` man page
