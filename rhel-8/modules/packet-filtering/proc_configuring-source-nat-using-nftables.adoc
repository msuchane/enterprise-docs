:_mod-docs-content-type: PROCEDURE
:experimental:

[id="configuring-source-nat-using-nftables_{context}"]
= Configuring source NAT using nftables

[role="_abstract"]
On a router, Source NAT (SNAT) enables you to change the IP of packets sent through an interface to a specific IP address. The router then replaces the source IP of outgoing packets.


.Procedure

. Create a table:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# *nft add table nat*
....

. Add the `prerouting` and `postrouting` chains to the table:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# *nft add chain nat postrouting { type nat hook postrouting priority 100 \; }*
....
+
[IMPORTANT]
====
Even if you do not add a rule to the `postrouting` chain, the `nftables` framework requires this chain to match outgoing packet replies.
====
+
Note that you must pass the `--` option to the `nft` command to prevent the shell from interpreting the negative priority value as an option of the `nft` command.

. Add a rule to the `postrouting` chain that replaces the source IP of outgoing packets through `ens3` with `192.0.2.1`:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
# *nft add rule nat postrouting oifname "__ens3__" snat to __192.0.2.1__*
....


[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
ifdef::networking-title[]
* xref:forwarding-incoming-packets-on-a-specific-local-port-to-a-different-host_configuring-port-forwarding-using-nftables[Forwarding incoming packets on a specific local port to a different host]
endif::[]
ifndef::networking-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/getting-started-with-nftables_configuring-and-managing-networking#forwarding-incoming-packets-on-a-specific-local-port-to-a-different-host_configuring-port-forwarding-using-nftables[Forwarding incoming packets on a specific local port to a different host]
endif::[]
endif::[]
ifeval::[{ProductNumber} >= 9]
ifdef::firewall-title[]
* xref:forwarding-incoming-packets-on-a-specific-local-port-to-a-different-host_configuring-port-forwarding-using-nftables[Forwarding incoming packets on a specific local port to a different host]
endif::[]
ifndef::firewall-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring-firewalls-and-packet-filters/getting-started-with-nftables_configuring-firewalls-and-packet-filters#forwarding-incoming-packets-on-a-specific-local-port-to-a-different-host_configuring-port-forwarding-using-nftables[Forwarding incoming packets on a specific local port to a different host]
endif::[]
endif::[]

