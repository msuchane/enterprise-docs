:_mod-docs-content-type: PROCEDURE

[id="proc_disconnecting-a-container-from-a-network_{context}"]
= Disconnecting a container from a network

Use the `podman network disconnect` command to disconnect the container from the network.

.Prerequisites

include::snip_container_tools.adoc[]
* A network has been created using the `podman network create` command.
* A container is connected to a network.

.Procedure
* Disconnect the container named `mycontainer` from the network named `mynet`:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *podman network disconnect mynet mycontainer*
----

.Verification
* Verify that the `mycontainer` is disconnected from the `mynet` network:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *podman inspect --format='{{.NetworkSettings.Networks}}' mycontainer*
map[podman:0xc000537440]
----
+
You can see that `mycontainer` is disconnected from the `mynet` network, `mycontainer` is only connected to the default `podman` network.



[role="_additional-resources"]
.Additional resources
* `podman-network-disconnect` man page
