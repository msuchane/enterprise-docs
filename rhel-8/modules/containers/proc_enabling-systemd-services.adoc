:_mod-docs-content-type: PROCEDURE
[id="proc_enabling-systemd-services_{context}"]
= Enabling systemd services

[role="_abstract"]
When enabling the service, you have different options.

//.Prerequisites


.Procedure

* Enable the service:

** To enable a service at system start, no matter if user is logged in or not, enter:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *systemctl enable <service>*
----
+
You have to copy the `systemd` unit files to the `/etc/systemd/system` directory.

** To start a service at  user login and stop it at user logout, enter:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *systemctl --user enable <service>*
----
+
You have to copy the `systemd` unit files to the `$HOME/.config/systemd/user` directory.

** To enable users to start a service at system start and persist over logouts, enter:
+

[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *loginctl enable-linger <username>*
----


//.Verification steps



[role="_additional-resources"]
.Additional resources
* `systemctl` man page
* `loginctl` man page
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/managing-systemd_configuring-basic-system-settings#enabling-a-system-service_managing-system-services-with-systemctl[Enabling a system service to start at boot]

