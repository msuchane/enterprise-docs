:_mod-docs-content-type: CONCEPT
[id="con_understanding-the-ubi-minimal-images_{context}"]
= Understanding the UBI minimal images

[role="_abstract"]
The UBI minimal images, named `ubi-minimal` offer a minimized pre-installed content set and a package manager (`microdnf``). As a result, you can use a `Containerfile` while minimizing the dependencies included in the image.

The key features of UBI minimal images include:

* **Small size**: Minimal images are about 92M on disk and 32M, when compressed.
This makes it less than half the size of the standard images.

* **Software installation (`microdnf`)**: Instead of including the fully-developed
`{PackageManagerCommand}` facility for working with software repositories and RPM software packages,
the minimal images includes the `microdnf` utility.
The `microdnf` is a scaled-down version of `dnf` allowing you to enable and disable repositories, remove and update packages, and clean out cache after packages have been installed.

* **Based on {ProductShortName} packaging**: Minimal images incorporate regular {ProductShortName} software RPM packages, with a few features removed. Minimal images do not include initialization and service management system, such as `systemd` or System V init, Python run-time environment, and some shell utilities. You can rely on {ProductShortName} repositories for building your images, while carrying the smallest possible amount of overhead.


* **Modules for `microdnf` are  supported**: Modules used with `microdnf` command let you install multiple versions of the same software, when available. You can use `microdnf module enable`, `microdnf module disable`, and `microdnf module reset` to enable, disable, and reset a module stream, respectively.

** For example, to enable the `nodejs:14` module stream inside the UBI minimal container, enter:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *microdnf module enable nodejs:14*
Downloading metadata...
...
Enabling module streams:
    nodejs:14

Running transaction test...
----

{RH} only supports the latest version of UBI and does not support parking on a dot release. If you need to park on a specific dot release, please take a look at link:https://access.redhat.com/articles/rhel-eus[Extended Update Support].


//[role="_additional-resources"]
//.Additional resources
