:_newdoc-version: 2.15.1
:_template-generated: 2024-01-25
:_mod-docs-content-type: PROCEDURE

[id="stopping-the-toolbx-container_{context}"]
= Stopping the Toolbx container

[role="_abstract"]
Use the `exit` command to leave the Toolbox container and the `podman stop` commmand to stop the container. 

//.Prerequisites

.Procedure

. Leave the container and return to the host:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
⬢ [user@toolbox ~]$ *exit*
----

. Stop the toolbox container:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
⬢ [user@toolbox ~]$ *podman stop _<mytoolbox>_*
----

. Optional: Remove the toolbox container:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
⬢ [user@toolbox ~]$ *toolbox rm _<mytoolbox>_*
----
+
Alternatively, you can also use the `podman rm` command to remove the container. 




