:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="creating-a-rootful-container-with-podman-volume_{context}"]
= Creating a rootful container with Podman volume
You can use the `podman` RHEL system role to create a rootful container with a Podman volume by running an Ansible playbook and with that, manage your application configuration.


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
- hosts: managed-node-01.example.com
  vars:
    podman_firewall:
      - port: 8080/tcp
        state: enabled
    podman_kube_specs:
      - state: started
        kube_file_content:
          apiVersion: v1
          kind: Pod
          metadata:
            name: ubi8-httpd
          spec:
            containers:
              - name: ubi8-httpd
                image: registry.access.redhat.com/ubi8/httpd-24
                ports:
                  - containerPort: 8080
                    hostPort: 8080
                volumeMounts:
                  - mountPath: /var/www/html:Z
                    name: ubi8-html
            volumes:
              - name: ubi8-html
                persistentVolumeClaim:
                  claimName: ubi8-html-volume
  roles:
    - linux-system-roles.podman
....
+
The procedure creates a pod with one container. The `podman_kube_specs` role variable describes a pod.  

* By default, the `podman` role creates rootful containers. 
* The `kube_file_content` field containing a Kubernetes YAML file defines the container named `ubi8-httpd`.
** The `ubi8-httpd` container is based on the `registry.access.redhat.com/ubi8/httpd-24` container image.
*** The `ubi8-html-volume` maps the `/var/www/html` directory on the host to the container. The `Z` flag labels the content with a private unshared label, therefore, only the `ubi8-httpd` container can access the content.   
*** The pod mounts the existing persistent volume named `ubi8-html-volume` with the mount path `/var/www/html`.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.podman/README.md` file
* `/usr/share/doc/rhel-system-roles/podman/` directory

