:_mod-docs-content-type: PROCEDURE

[id="signing-container-images-with-sigstore-signatures-with-a-private-key-and-rekor_{context}"]
= Signing container images with sigstore signatures with a private key and Rekor

Starting with Podman version 4.4, you can use the sigstore format of container signatures together with Rekor servers. 
You can also upload public signatures to the public rekor.sigstore.dev server, which increases the interoperability with Cosign. You can then use the `cosign verify` command to verify your signatures without having to explicitly disable Rekor. 

.Prerequisites

include::snip_container_tools.adoc[]

.Procedure
. Generate a sigstore public/private key pair:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ skopeo generate-sigstore-key --output-prefix _myKey_
----
+
* The public and private keys `_myKey.pub_` and `_myKey.private_` are generated.

. Add the following content to the `/etc/containers/registries.conf.d/default.yaml` file: 
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
---- 
docker:
    <registry>:
        use-sigstore-attachments: true
----
+
* By setting the `use-sigstore-attachments` option, Podman and Skopeo can read and write the container sigstore signatures together with the image and save them in the same repository as the signed image. 
+
NOTE: You can edit the registry or repository configuration section in any YAML file in the `/etc/containers/registries.d` directory. A single scope (default-docker, registry, or namespace) can only exist in one file within the `/etc/containers/registries.d` directory. 
You can also edit the system-wide registry configuration in the `/etc/containers/registries.d/default.yaml` file. Please note that all YAML files are read and the filename is arbitrary.

. Build the container image using `Containerfile` in the current directory: 
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
---- 			
$ podman build -t _<registry>/<namespace>/<image>_  	 
----

. Create the `_file.yml_` file:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
---- 	
privateKeyFile: "/home/user/sigstore/_myKey.private_"
privateKeyPassphraseFile: "/mnt/user/sigstore-_myKey_-passphrase"
rekorURL: "https://<your-rekor-server>"
----
+
* The `_file.yml_` is the sigstore signing parameter YAML file used to store options required to create sigstore signatures. 

. Sign the image and push it to the registry: 
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----  
$ *podman push --sign-by-sigstore=_file.yml_ _<registry>/<namespace>/<image>_*
----
+
* You can alternatively use the `skopeo copy` command with similar `--sign-by-sigstore` options to sign existing images while moving them across container registries. 

WARNING: Note that your submission for public servers includes data about the public key and metadata about the signature. 


.Verification
* Use one of the following methods to verify that a container image is correctly signed: 
** Use the `cosign verify` command: 
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----  
$ *cosign verify _<registry>/<namespace>/<image>_ --key _myKey.pub_*
----

* Use the `podman pull` command: 
** Add the `rekorPublicKeyPath` or `rekorPublicKeyData` fields  in the `/etc/containers/policy.json` file:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----  
{
  ...
  "transports": {
	"docker": {
        "<registry>/<namespace>": [
            {
                "type": "sigstoreSigned",
                "rekorPublicKeyPath": "/path/to/local/public/key/file",
            }
        ]
	...
	}
  }
  ...
}
----

** Pull the image: 
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----  
$ *podman pull _<registry>/<namespace>/<image>_*
----
+
*** The `podman pull` command enforces signature presence as configured, no extra options are required.


[role="_additional-resources"]
.Additional resources
* `podman-push` man page
* `podman-build` man page 
* `container-registries.d` man page
* link:https://www.redhat.com/en/blog/sigstore-open-answer-software-supply-chain-trust-and-security[Sigstore: An open answer to software supply chain trust and security]
 


