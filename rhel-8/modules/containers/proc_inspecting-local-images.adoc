:_mod-docs-content-type: PROCEDURE

[id="proc_inspecting-local-images_{context}"]
= Inspecting local images

[role="_abstract"]
After you pull an image to your local system and run it, you can use the `podman inspect` command to investigate the image. For example, use it to understand what the image does and check what software is inside the image. The `podman inspect` command displays information about containers and images identified by name or ID.

.Prerequisites

include::snip_container_tools.adoc[]
* A pulled image is available on the local system.

.Procedure

* Inspect the `registry.redhat.io/ubi{ProductNumberLink}/ubi` image:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *podman inspect registry.redhat.io/ubi{ProductNumberLink}/ubi*
…
 "Cmd": [
        "/bin/bash"
    ],
    "Labels": {
        "architecture": "x86_64",
        "build-date": "2020-12-10T01:59:40.343735",
        "com.redhat.build-host": "cpt-1002.osbs.prod.upshift.rdu2.redhat.com",
        "com.redhat.component": "ubi{ProductNumberLink}-container",
        "com.redhat.license_terms": "https://www.redhat.com/...,
    "description": "The Universal Base Image is ...
    }
...
----
+
The `"Cmd"` key specifies a default command to run within a container. You can override this command by specifying a command as an argument to the `podman run` command.
This ubi{ProductNumberLink}/ubi container will execute the bash shell if no other argument is given when you start it with `podman run`. If an `"Entrypoint"` key was set, its value would be used instead of the `"Cmd"` value, and the value of `"Cmd"` is used as an argument to the Entrypoint command.


//.Verification steps

[role="_additional-resources"]
.Additional resources
* `podman-inspect` man page
