:_mod-docs-content-type: PROCEDURE
[id="proc_using-the-ubi-micro-images_{context}"]
= Using the UBI micro images

[role="_abstract"]
You can build a `ubi-micro` container image using the Buildah tool.

.Prerequisites

include::snip_container_tools.adoc[]

.Prerequisites

ifeval::[{ProductNumber} >= 9]
* The `podman` tool, provided by the `containers-tool` meta-package, is installed.
endif::[]

ifeval::[{ProductNumber} == 8]
* The `podman` tool, provided by the `containers-tool` module, is installed.
endif::[]

.Procedure

ifeval::[{ProductNumber} == 8]
. Pull and build the `registry.access.redhat.com/ubi8/ubi-micro` image:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *microcontainer=$(buildah from registry.access.redhat.com/ubi8/ubi-micro)*
----
endif::[]
ifeval::[{ProductNumber} == 9]
. Pull and build the `registry.access.redhat.com/ubi8/ubi-micro` image:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *microcontainer=$(buildah from registry.access.redhat.com/ubi9/ubi-micro)*
----
endif::[]

. Mount a working container root filesystem:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *micromount=$(buildah mount $microcontainer)*
----

ifeval::[{ProductNumber} == 8]
. Install the `httpd` service to the `micromount` directory:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *{PackageManagerCommand} install \*
    *--installroot $micromount \*
    *--releasever 8 \*
    *--setopt install_weak_deps=false \*
    *--nodocs -y \*
    *httpd*
# *{PackageManagerCommand} clean all \*
    *--installroot $micromount*
----
endif::[]
ifeval::[{ProductNumber} == 9]
. Install the `httpd` service to the `micromount` directory:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *{PackageManagerCommand} install \*
    *--installroot $micromount \*
    *--releasever=/ \*
    *--setopt install_weak_deps=false \*
    *--setopt=reposdir=/etc/yum.repos.d/ \*
    *--nodocs -y \*
    *httpd*
# *{PackageManagerCommand} clean all \*
    *--installroot $micromount*
----
endif::[]

. Unmount the root file system on the working container:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *buildah umount $microcontainer*
----

. Create the `ubi-micro-httpd` image from a working container:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *buildah commit $microcontainer ubi-micro-httpd*
----

.Verification steps
. Display details about the `ubi-micro-httpd` image:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *podman images ubi-micro-httpd*
localhost/ubi-micro-httpd latest 7c557e7fbe9f  22 minutes ago  151 MB
----


//[role="_additional-resources"]
//.Additional resources
