:_mod-docs-content-type: PROCEDURE

[id="restoring-a-container-checkpoint-in-the-web-console_{context}"]
= Restoring a container checkpoint in the web console

You can use data saved to restore the container after a reboot at the same point in time it was checkpointed. 		

NOTE: Creating a checkpoint is available only for system containers.

.Prerequisites

* The container was checkpointed.

* The web console is installed and accessible. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumberLink}_web_console/getting-started-with-the-rhel-{ProductNumberLink}-web-console_system-management-using-the-rhel-{ProductNumberLink}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumberLink}-web-console[Installing the web console] and link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumberLink}_web_console/getting-started-with-the-rhel-{ProductNumberLink}-web-console_system-management-using-the-rhel-{ProductNumberLink}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumberLink}-web-console[Logging in to the web console]. 

* The `cockpit-podman` add-on is installed: 
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install cockpit-podman*
----


.Procedure

. Click *Podman containers* in the main menu.
. In the *Containers* table, select the container you want to modify and click the overflow menu and select *Restore*. 
. Optional: In the *Restore container* form, check the options you need:
* *Keep all temporary checkpoint files*: Keep all temporary log and statistics files created by CRIU during checkpointing. These files are not deleted if checkpointing fails for further debugging.
* *Restore with established TCP connections*
* *Ignore IP address if set statically*: If the container was started with IP address the restored container also tries to use that IP address and restore fails if that IP address is already in use. This option is applicable if you added port mapping in the Integration tab when you create the container.
* *Ignore MAC address if set statically*: If the container was started with MAC address the restored container also tries to use that MAC address and restore fails if that MAC address is already in use.
. Click btn:[Restore].


.Verification

* Click the *Podman containers* in the main menu. You can see that the restored container in the *Containers* table is running.  



