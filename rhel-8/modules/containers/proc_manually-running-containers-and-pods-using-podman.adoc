:_mod-docs-content-type: PROCEDURE

[id="proc_manually-running-containers-and-pods-using-podman_{context}"]
= Manually running containers and pods using Podman

[role="_abstract"]
The following procedure shows how to manually create a WordPress content management system paired with a MariaDB database using Podman.

Suppose the following directory layout:
----
├── mariadb-conf
│   ├── Containerfile
│   ├── my.cnf
----

.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Display the `mariadb-conf/Containerfile` file:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *cat mariadb-conf/Containerfile*
FROM docker.io/library/mariadb
COPY my.cnf /etc/mysql/my.cnf
----

. Display the `mariadb-conf/my.cnf` file:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
[client-server]
# Port or socket location where to connect
port = 3306
socket = /run/mysqld/mysqld.sock

# Import all .cnf files from the configuration directory
[mariadbd]
skip-host-cache
skip-name-resolve
bind-address = 127.0.0.1

!includedir /etc/mysql/mariadb.conf.d/
!includedir /etc/mysql/conf.d/
----

. Build the `docker.io/library/mariadb` image using `mariadb-conf/Containerfile`:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *cd  mariadb-conf*
$ *podman build -t mariadb-conf .*
$ *cd ..*
STEP 1: FROM docker.io/library/mariadb
Trying to pull docker.io/library/mariadb:latest...
Getting image source signatures
Copying blob 7b1a6ab2e44d done
...
Storing signatures
STEP 2: COPY my.cnf /etc/mysql/my.cnf
STEP 3: COMMIT mariadb-conf
--> ffae584aa6e
Successfully tagged localhost/mariadb-conf:latest
ffae584aa6e733ee1cdf89c053337502e1089d1620ff05680b6818a96eec3c17
----

. Optional: List all images:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *podman images*
LIST IMAGES
REPOSITORY                                                       TAG         IMAGE ID      CREATED             SIZE
localhost/mariadb-conf                                           latest      b66fa0fa0ef2  57 seconds ago      416 MB
----

. Create the pod named `wordpresspod` and configure port mappings between the container and the host system:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *podman pod create --name wordpresspod -p 8080:80*
----

. Create the `mydb` container inside the `wordpresspod` pod:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *podman run --detach --pod wordpresspod \*
    *-e MYSQL_ROOT_PASSWORD=1234 \*
    *-e MYSQL_DATABASE=mywpdb \*
    *-e MYSQL_USER=mywpuser \*
    *-e MYSQL_PASSWORD=1234 \*
    *--name mydb localhost/mariadb-conf*
----

. Create the `myweb` container inside the `wordpresspod` pod:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *podman run --detach --pod wordpresspod \*
    *-e WORDPRESS_DB_HOST=127.0.0.1 \*
    *-e WORDPRESS_DB_NAME=mywpdb \*
    *-e WORDPRESS_DB_USER=mywpuser \*
    *-e WORDPRESS_DB_PASSWORD=1234 \*
    *--name myweb docker.io/wordpress*
----

. Optional. List all pods and containers associated with them:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *podman ps --pod -a*
CONTAINER ID  IMAGE                               COMMAND               CREATED                 STATUS                     PORTS                 NAMES               POD ID        PODNAME
9ea56f771915  k8s.gcr.io/pause:3.5                                      Less than a second ago  Up Less than a second ago  0.0.0.0:8080->80/tcp  4b7f054a6f01-infra  4b7f054a6f01  wordpresspod
60e8dbbabac5  localhost/mariadb-conf:latest       mariadbd              Less than a second ago  Up Less than a second ago  0.0.0.0:8080->80/tcp  mydb                4b7f054a6f01  wordpresspod
045d3d506e50  docker.io/library/wordpress:latest  apache2-foregroun...  Less than a second ago  Up Less than a second ago  0.0.0.0:8080->80/tcp  myweb               4b7f054a6f01  wordpresspod
----

.Verification
* Verify that the pod is running: Visit the \http://localhost:8080/wp-admin/install.php page or use the `curl` command:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *curl  http://localhost:8080/wp-admin/install.php*
<!DOCTYPE html>
<html lang="en-US" xml:lang="en-US">
<head>
...
</head>
<body class="wp-core-ui">
<p id="logo">WordPress</p>
    <h1>Welcome</h1>
...
----


[role="_additional-resources"]
.Additional resources
* link:https://www.redhat.com/sysadmin/podman-play-kube-updates[Build Kubernetes pods with Podman play kube]
* `podman-play-kube` man page
