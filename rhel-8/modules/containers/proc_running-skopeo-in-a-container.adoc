:_mod-docs-content-type: PROCEDURE
[id="proc_running-skopeo-in-a-container_{context}"]
= Running Skopeo in a container

[role="_abstract"]
You can inspect a remote container image using Skopeo. Running Skopeo in a container means that the container root filesystem is isolated from the host root filesystem. To share or copy files between the host and container, you have to mount files and directories.


.Prerequisites

include::snip_container_tools.adoc[]


.Procedure
. Log in to the registry.redhat.io registry:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *podman login registry.redhat.io*
Username: myuser@mycompany.com
Password: <password>
Login Succeeded!
----

. Get the `registry.redhat.io/rhel{ProductNumberLink}/skopeo` container image:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *podman pull registry.redhat.io/rhel{ProductNumberLink}/skopeo*
----

. Inspect a remote container image `registry.access.redhat.com/ubi{ProductNumberLink}/ubi` using Skopeo:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *podman run --rm registry.redhat.io/rhel{ProductNumberLink}/skopeo \*
  *skopeo inspect docker://registry.access.redhat.com/ubi{ProductNumberLink}/ubi*
{
    "Name": "registry.access.redhat.com/ubi{ProductNumberLink}/ubi",
    ...
    "Labels": {
        "architecture": "x86_64",
        ...
        "name": "ubi{ProductNumberLink}",
        ...
        "summary": "Provides the latest release of Red Hat Universal Base Image {ProductNumberLink}.",
        "url": "https://access.redhat.com/containers/#/registry.access.redhat.com/ubi{ProductNumberLink}/images/8.2-347",
        ...
    },
    "Architecture": "amd64",
    "Os": "linux",
    "Layers": [
    ...
    ],
    "Env": [
        "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
        "container=oci"
    ]
}
----
+
The `--rm` option removes the `registry.redhat.io/rhel{ProductNumberLink}/skopeo` image after the container exits.

//.Verification steps



[role="_additional-resources"]
.Additional resources
* link:https://www.redhat.com/sysadmin/how-run-skopeo-container[How to run skopeo in a container]
