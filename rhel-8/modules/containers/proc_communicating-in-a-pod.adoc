:_mod-docs-content-type: PROCEDURE

[id="proc_communicating-in-a-pod_{context}"]
= Communicating in a pod

You must publish the ports for the container in a pod when a pod is created.

.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Create a pod named `web-pod`:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *podman pod create --name=web-pod-publish -p 80:80*
----

. List all pods:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *podman pod ls*

POD ID        NAME         STATUS   CREATED        INFRA ID      # OF CONTAINERS
26fe5de43ab3  publish-pod  Created  5 seconds ago  7de09076d2b3  1
----


. Run the web container named `web-container` inside the `web-pod`:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *podman container run -d --pod web-pod-publish --name=web-container docker.io/library/httpd*
----

. List containers
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# *podman ps*

CONTAINER ID  IMAGE                    COMMAND           CREATED             STATUS             PORTS               NAMES
7de09076d2b3  k8s.gcr.io/pause:3.5                       About a minute ago  Up 23 seconds ago  0.0.0.0:80->80/tcp  26fe5de43ab3-infra
088befb90e59  docker.io/library/httpd  httpd-foreground  23 seconds ago      Up 23 seconds ago  0.0.0.0:80->80/tcp  web-container
----

. Verify that the `web-container` can be reached:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
$ *curl localhost:80*

<html><body><h1>It works!</h1></body></html>
----

//.Verification

//[role="_additional-resources"]
//.Additional resources
