:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// securing-virtual-machines-in-rhel


[id="virtualization-booleans-in-rhel_{context}"]
= SELinux booleans for virtualization

{ProductShortName}{nbsp}{ProductNumber} provides the `sVirt` feature, which is a set of specialized SELinux booleans that are automatically enabled on a host with SELinux in Enforcing mode.

For fine-grained configuration of virtual machines security on a {ProductShortName}{nbsp}{ProductNumber} system, you can configure SELinux booleans on the host to ensure the hypervisor acts in a specific way.

To list all virtualization-related booleans and their statuses, use the `getsebool -a | grep virt` command:

[literal,subs=quotes]
--
$ *getsebool -a | grep virt*
[...]
virt_sandbox_use_netlink --> off
virt_sandbox_use_sys_admin --> off
virt_transition_userdomain --> off
virt_use_comm --> off
virt_use_execmem --> off
virt_use_fusefs --> off
[...]
--

To enable a specific boolean, use the `setsebool -P _boolean_name_ on` command as root. To disable a boolean, use `setsebool -P _boolean_name_ off`.

The following table lists virtualization-related booleans available in {ProductShortName}{nbsp}{ProductNumber} and what they do when enabled:

.SELinux virtualization booleans
[options="header"]
|====
|SELinux Boolean|Description
|staff_use_svirt|Enables non-root users to create and transition VMs to sVirt.
|unprivuser_use_svirt|Enables unprivileged users to create and transition VMs to sVirt.
|virt_sandbox_use_audit|Enables sandbox containers to send audit messages.
|virt_sandbox_use_netlink|Enables sandbox containers to use netlink system calls.
|virt_sandbox_use_sys_admin|Enables sandbox containers to use sys_admin system calls, such as mount.
|virt_transition_userdomain|Enables virtual processes to run as user domains.
|virt_use_comm|Enables virt to use serial/parallel communication ports.
|virt_use_execmem|Enables confined virtual guests to use executable memory and executable stack.
|virt_use_fusefs|Enables virt to read FUSE mounted files.
|virt_use_nfs|Enables virt to manage NFS mounted files.
|virt_use_rawip|Enables virt to interact with rawip sockets.
|virt_use_samba|Enables virt to manage CIFS mounted files.
|virt_use_sanlock|Enables confined virtual guests to interact with the sanlock.
|virt_use_usb|Enables virt to use USB devices.
|virt_use_xserver|Enables virtual machine to interact with the X Window System.
|====

//For the RHEL 7 version, see https://doc-stage.usersys.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/virtualization_security_guide/#sect-Virtualization_Security_Guide-sVirt-Configuration
