:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Managing SR-IOV devices - context "managing-sr-iov-devices"

[id="attaching-sr-iov-networking-devices-to-virtual-machines_{context}"]
= Attaching SR-IOV networking devices to virtual machines

// Maybe change into "assigning"^?

[role="_abstract"]
To attach an SR-IOV networking device to a virtual machine (VM) on an Intel or AMD host, you must create a virtual function (VF) from an SR-IOV capable network interface on the host and assign the VF as a device to a specified VM. For details, see the following instructions.

.Prerequisites

* The CPU and the firmware of your host support the I/O Memory Management Unit (IOMMU).
** If using an Intel CPU, it must support the Intel Virtualization Technology for Directed I/O (VT-d).
** If using an AMD CPU, it must support the AMD-Vi feature.

* The host system uses Access Control Service (ACS) to provide direct memory access (DMA) isolation for PCIe topology. Verify this with the system vendor.
+
For additional information, see link:https://access.redhat.com/documentation/en-us/red_hat_virtualization/4.0/html/hardware_considerations_for_implementing_sr-iov/index[Hardware Considerations for Implementing SR-IOV].

* The physical network device supports SR-IOV. To verify if any network devices on your system support SR-IOV, use the `lspci -v` command and look for `Single Root I/O Virtualization (SR-IOV)` in the output.
+
[subs="+quotes,attributes"]
----
# *lspci -v*
[...]
02:00.0 Ethernet controller: Intel Corporation 82576 Gigabit Network Connection (rev 01)
	Subsystem: Intel Corporation Gigabit ET Dual Port Server Adapter
	Flags: bus master, fast devsel, latency 0, IRQ 16, NUMA node 0
	Memory at fcba0000 (32-bit, non-prefetchable) [size=128K]
[...]
	Capabilities: [150] Alternative Routing-ID Interpretation (ARI)
	Capabilities: *[160] Single Root I/O Virtualization (SR-IOV)*
	Kernel driver in use: igb
	Kernel modules: igb
[...]
----

* The host network interface you want to use for creating VFs is running. For example, to activate the _eth1_ interface and verify it is running:
+
[subs="+quotes,attributes"]
----
# *ip link set eth1 up*
# *ip link show eth1*
8: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DEFAULT qlen 1000
   link/ether a0:36:9f:8f:3f:b8 brd ff:ff:ff:ff:ff:ff
   vf 0 MAC 00:00:00:00:00:00, spoof checking on, link-state auto
   vf 1 MAC 00:00:00:00:00:00, spoof checking on, link-state auto
   vf 2 MAC 00:00:00:00:00:00, spoof checking on, link-state auto
   vf 3 MAC 00:00:00:00:00:00, spoof checking on, link-state auto
----

* For SR-IOV device assignment to work, the IOMMU feature must be enabled in the host BIOS and kernel. To do so:
** On an Intel host, enable VT-d:

... Regenerate the GRUB configuration with the `intel_iommu=on` and `iommu=pt` parameters:
+
[subs="quotes"]
----
# *grubby --args="intel_iommu=on iommu=pt" --update-kernel=ALL*
----

... Reboot the host.

** On an AMD host, enable AMD-Vi:

... Regenerate the GRUB configuration with the `iommu=pt` parameter:
+
[subs="quotes"]
----
# *grubby --args="iommu=pt" --update-kernel=ALL*
----

... Reboot the host.


.Procedure

. *Optional:* Confirm the maximum number of VFs your network device can use. To do so, use the following command and replace _eth1_ with your SR-IOV compatible network device.
+
[subs="quotes"]
----
# *cat /sys/class/net/eth1/device/sriov_totalvfs*
7
----

. Use the following command to create a virtual function (VF):
+
[subs="quotes"]
----
# *echo _VF-number_ > /sys/class/net/_network-interface_/device/sriov_numvfs*
----
+
In the command, replace:
+
--
* _VF-number_ with the number of VFs you want to create on the PF.
* _network-interface_ with the name of the network interface for which the VFs will be created.
--
+
The following example creates 2 VFs from the eth1 network interface:
+
[subs="quotes"]
----
# *echo 2 > /sys/class/net/eth1/device/sriov_numvfs*
----

. Verify the VFs have been added:
+
[subs="+quotes,attributes"]
----
# *lspci | grep Ethernet*
82:00.0 Ethernet controller: Intel Corporation 82599ES 10-Gigabit SFI/SFP+ Network Connection (rev 01)
82:00.1 Ethernet controller: Intel Corporation 82599ES 10-Gigabit SFI/SFP+ Network Connection (rev 01)
82:10.0 Ethernet controller: Intel Corporation 82599 Ethernet Controller Virtual Function (rev 01)
82:10.2 Ethernet controller: Intel Corporation 82599 Ethernet Controller Virtual Function (rev 01)
----

. Make the created VFs persistent by creating a udev rule for the network interface you used to create the VFs. For example, for the _eth1_ interface, create the `/etc/udev/rules.d/eth1.rules` file, and add the following line:
+
[source,bash]
----
ACTION=="add", SUBSYSTEM=="net", ENV{ID_NET_DRIVER}=="ixgbe", ATTR{device/sriov_numvfs}="2"
----
+
This ensures that the two VFs that use the `ixgbe` driver will automatically be available for the `eth1` interface when the host starts. If you do not require persistent SR-IOV devices, skip this step.
+
[WARNING]
====
Currently, the setting described above does not work correctly when attempting to make VFs persistent on Broadcom NetXtreme II BCM57810 adapters. In addition, attaching VFs based on these adapters to Windows VMs is currently not reliable.
====

. Hot-plug one of the newly added VF interface devices to a running VM.
+
[subs="+quotes,attributes"]
----
# *virsh attach-interface testguest1 hostdev 0000:82:10.0 --managed --live --config*
----
+
//Would this perhaps be better (or even possible) to do with a non-live VM? -> TODO look into

.Verification

* If the procedure is successful, the guest operating system detects a new network interface card.

////
[discrete]
.Additional resources

* TBD?

////
