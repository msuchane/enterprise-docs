:_newdoc-version: 2.17.0
:_template-generated: 2024-04-15
:_mod-docs-content-type: SNIPPET

// A technology preview warning box with specific wording that's used in the RHEL 8 nested virtualization guide.

ifeval::[{ProductNumber} == 8]
[WARNING]
====
{RH} currently provides nested virtualization only as a link:https://access.redhat.com/support/offerings/techpreview/[Technology Preview], and it is therefore unsupported. 

Additionally, nested virtualization has only been tested on a limited set of architectures and operating system versions. Before you use this feature in your environment, see xref:restrictions-and-limitations-for-nested-virtualization_creating-nested-virtual-machines[Restrictions and limitations for nested virtualization].
====
endif::[]

// A technology preview warning box with specific wording that's used in the RHEL 9 nested virtualization guide.

ifeval::[{ProductNumber} == 9]
[WARNING]
====
In the majority of environments, nested virtualization is only available as a link:https://access.redhat.com/support/offerings/techpreview/[Technology Preview] in {ProductShortName} 9.

For detailed descriptions of the supported and unsupported environments, see xref:support-limitations-for-nested-virtualization_creating-nested-virtual-machines[Support limitations for nested virtualization].
====
endif::[]

