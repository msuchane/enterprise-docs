:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// securing-virtual-machines-in-rhel

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_best-practices-for-securing-virtual-machines.adoc[leveloffset=+1]

[id="best-practices-for-securing-virtual-machines_{context}"]
= Best practices for securing virtual machines

[role="_abstract"]
Following the instructions below significantly decreases the risk of your virtual machines being infected with malicious code and used as attack vectors to infect your host system.

*On the guest side:*

* Secure the virtual machine as if it was a physical machine. The specific methods available to enhance security depend on the guest OS.
+
If your VM is running {ProductShortName}{nbsp}{ProductNumber}, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/[Securing {ProductName}{nbsp}{ProductNumber}] for detailed instructions on improving the security of your guest system.

*On the host side:*

* When managing VMs remotely, use cryptographic utilities such as *SSH* and network protocols such as *SSL* for connecting to the VMs.

* Ensure SELinux is in Enforcing mode:
+
[literal,subs="+quotes,attributes"]
--
# *getenforce*
Enforcing
--
+
If SELinux is disabled or in _Permissive_ mode, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/changing-selinux-states-and-modes_using-selinux#changing-to-enforcing-mode_changing-selinux-states-and-modes[Using SELinux] document for instructions on activating Enforcing mode.
+
[NOTE]
====
SELinux Enforcing mode also enables the sVirt {ProductShortName}{nbsp}{ProductNumber} feature. This is a set of specialized SELinux booleans for virtualization, which can be xref:virtualization-booleans-in-rhel_securing-virtual-machines-in-rhel[manually adjusted] for fine-grained VM security management.
====

* Use VMs with _SecureBoot_:
+
SecureBoot is a feature that ensures that your VM is running a cryptographically signed OS. This prevents VMs whose OS has been altered by a malware attack from booting.
+
SecureBoot can only be applied when installing a Linux VM that uses OVMF firmware on an AMD64 or Intel 64 host. For instructions, see xref:creating-a-secureboot-virtual-machine_securing-virtual-machines-in-rhel[Creating a SecureBoot virtual machine].

* Do not use `qemu-*` commands, such as `qemu-kvm`.
+
QEMU is an essential component of the virtualization architecture in {ProductShortName}{nbsp}{ProductNumber}, but it is difficult to manage manually, and improper QEMU configurations may cause security vulnerabilities. Therefore, using most `qemu-*` commands is not supported by Red Hat. Instead, use _libvirt_ utilities, such as `virsh`, `virt-install`, and `virt-xml`, as these orchestrate QEMU according to the best practices.
+
Note, however, that the `qemu-img` utility is supported for xref:managing-virtual-disk-images-by-using-the-cli_managing-storage-for-virtual-machines[management of virtual disk images].

// When using an AMD EPYC host, use the Secure Encrypted Virtualization Feature (SEV), which makes it possible to encrypt VM memory so that the host cannot access data on the VM. - LEAVE OUT FOR THE TIME BEING, WILL LIKELY BE JUST A TP IN 8.0 -> TBD WHEN IT'S READY

[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::virt-title[]
xref:virtualization-booleans-in-rhel_securing-virtual-machines-in-rhel[SELinux booleans for virtualization in RHEL]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/securing-virtual-machines-in-rhel_configuring-and-managing-virtualization#virtualization-booleans-in-rhel_securing-virtual-machines-in-rhel[SELinux booleans for virtualization]
endif::[]
