:_mod-docs-content-type: PROCEDURE
:experimental:

// Module included in the following assemblies:
//
// assembly_managing-virtual-devices-using-the-web-console

[id="proc_removing-devices-from-virtual-machines-using-the-web-console_{context}"]
= Removing devices from virtual machines by using the web console

[role="_abstract"]
To free up resources, modify the functionalities of your VM, or both, you can use the web console to modify the VM and remove host devices that are no longer required.

[WARNING]
====
Removing attached USB host devices by using the web console may fail because of incorrect correlation between the device and bus numbers of the USB device.

For more information, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/8.6_release_notes/known-issues#known-issue_virtualization[{ProductShortName} {ProductNumber} Known Issues.]
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/9.0_release_notes/known-issues#known-issue_virtualization[{ProductShortName} {ProductNumber} Known Issues.]
endif::[]

As a workaround, remove the <hostdev> part of the USB device, from the XML configuration of VM by using the _virsh_ utility.
The following example opens the XML configuration of the `example-VM-1` VM:

[literal,subs="+quotes"]
----
# *virsh edit __<example-VM-1>__*
----
====

.Prerequisites

* {blank}
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[The web console VM plug-in is installed on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[The web console VM plug-in is installed on your system].
endif::virt-title[]

* *Optional:* Back up the XML configuration of your VM by using `virsh dumpxml _example-VM-1_` and sending the output to a file. For example, the following backs up the configuration of your _testguest1_ VM as the `testguest1.xml` file:
+
[literal,subs="+quotes,attributes"]
----
# *virsh dumpxml testguest1 > testguest1.xml*
# *cat testguest1.xml*
<domain type='kvm' xmlns:qemu='http://libvirt.org/schemas/domain/qemu/1.0'>
  <name>testguest1</name>
  <uuid>ede29304-fe0c-4ca4-abcd-d246481acd18</uuid>
  [...]
</domain>
----

.Procedure

. In the menu:Virtual Machines[] interface, click the VM  from which you want to remove a host device.
+
A new page opens with an *Overview* section with basic information about the selected VM and a *Console* section to access the VM's graphical interface.

. Scroll to menu:Host devices[].
+
The *Host devices* section displays information about the devices attached to the VM as well as options to *Add* or *Remove* devices.
+
image::virt-cockpit-host-devices.png[Image displaying the host dvices section of the selected VM.]

. Click the btn:[Remove] button next to the device you want to remove from the VM.
+
A remove device confirmation dialog appears.
+
image::virt-cockpit-host-remove-device.png[Image displaying the option to remove an attached virtual device.]

. Click btn:[Remove].
+
The device is removed from the VM.

.Troubleshooting

* If removing a host device causes your VM to become unbootable, use the `virsh define` utility to restore the XML configuration by reloading the XML configuration file you backed up previously.
+
[literal,subs="+quotes,attributes"]
----
# *virsh define testguest1.xml*
----
