:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// assembly_viewing-information-about-virtual-machines

[id="sample-virtual-machine-xml-configuration_{context}"]
= Sample virtual machine XML configuration

The XML configuration of a VM, also referred to as a _domain XML_, determines the VM's settings and components. The following table shows sections of a sample XML configuration of a virtual machine (VM) and explains the contents.

To obtain the XML configuration of a VM, you can use the `virsh dumpxml` command followed by the VM's name.

[subs="+quotes"]
----
# *virsh dumpxml _testguest1_*
----

.Sample XML configuration
[options="header",  cols="65%,35%"]
|===
|Domain XML Section|Description
a|
[subs="+quotes"]
----
<domain type='kvm'>
 <name>Testguest1</name>
 <uuid>ec6fbaa1-3eb4-49da-bf61-bb02fbec4967</uuid>
 <memory unit='KiB'>1048576</memory>
 <currentMemory unit='KiB'>1048576</currentMemory>
----
|This is a KVM virtual machine called _Testguest1_, with 1024 MiB allocated RAM.

a|
[subs="+quotes"]
----
 <vcpu placement='static'>1</vcpu>
----
|The VM is allocated with a single virtual CPU (vCPU).

For information about configuring vCPUs, see
ifdef::virt-title[]
xref:optimizing-virtual-machine-cpu-performance_optimizing-virtual-machine-performance-in-rhel[Optimizing virtual machine CPU performance].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/optimizing-virtual-machine-performance-in-rhel_configuring-and-managing-virtualization#optimizing-virtual-machine-cpu-performance_optimizing-virtual-machine-performance-in-rhel[Optimizing virtual machine CPU performance].
endif::[]

a|
ifeval::[{ProductNumber} == 8]
[subs="+quotes"]
----
 <os>
  <type arch='x86_64' machine='pc-q35-4.1'>hvm</type>
  <boot dev='hd'/>
 </os>
----
endif::[]
ifeval::[{ProductNumber} == 9]
[subs="+quotes"]
----
 <os>
  <type arch='x86_64' machine='pc-q35-rhel9.0.0'>hvm</type>
  <boot dev='hd'/>
 </os>
----
endif::[]
|The machine architecture is set to the AMD64 and Intel 64 architecture, and uses the Intel Q35 machine type to determine feature compatibility. The OS is set to be booted from the hard disk drive.

For information about creating a VM with an installed OS, see
ifdef::virt-title[]
xref:creating-vms-and-installing-an-os-using-the-rhel-web-console_assembly_creating-virtual-machines[Creating virtual machines and installing guest operating systems by using the web console].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#creating-vms-and-installing-an-os-using-the-rhel-web-console_assembly_creating-virtual-machines[Creating virtual machines and installing guest operating systems by using the web console].
endif::[]
a|
[subs="+quotes"]
----
 <features>
  <acpi/>
  <apic/>
 </features>
----
|The *acpi* and *apic* hypervisor features are disabled.

// For information about configuring hypervisor features, see xref:enabling-hyper-v-enlightenments_optimizing-windows-virtual-machines-on-rhel[].
a|
[subs="+quotes"]
----
 <cpu mode='host-model' check='partial'/>
----
|The host CPU definitions from capabilities XML (obtainable with
ifeval::[{ProductNumber} == 8]
`virsh capabilities`)
endif::[]
ifeval::[{ProductNumber} == 9]
`virsh domcapabilities`)
endif::[]
are automatically copied into the VM's XML configuration. Therefore, when the VM is booted, `libvirt` picks a CPU model that is similar to the host CPU, and then adds extra features to approximate the host model as closely as possible.

a|
[subs="+quotes"]
----
 <clock offset='utc'>
  <timer name='rtc' tickpolicy='catchup'/>
  <timer name='pit' tickpolicy='delay'/>
  <timer name='hpet' present='no'/>
 </clock>
----
|The VM's virtual hardware clock uses the UTC time zone. In addition, three different timers are set up for synchronization with the QEMU hypervisor.

// For more information about configuring clock settings, see xref:enabling-hyper-v-enlightenments_optimizing-windows-virtual-machines-on-rhel[].
a|
[subs="+quotes"]
----
 <on_poweroff>destroy</on_poweroff>
 <on_reboot>restart</on_reboot>
 <on_crash>destroy</on_crash>
----
|When the VM powers off, or its OS terminates unexpectedly, `libvirt` terminates the VM and releases all its allocated resources. When the VM is rebooted, `libvirt` restarts it with the same configuration.
a|
[subs="+quotes"]
----
 <pm>
  <suspend-to-mem enabled='no'/>
  <suspend-to-disk enabled='no'/>
 </pm>
----
|The S3 and S4 ACPI sleep states are disabled for this VM.

ifeval::[{ProductNumber} == 8]
a|
[subs="+quotes"]
----
 <devices>
  <emulator>/usr/bin/qemu-kvm</emulator>
  <disk type='file' device='disk'>
   <driver name='qemu' type='qcow2'/>
   <source file='/var/lib/libvirt/images/Testguest.qcow2'/>
   <target dev='hda' bus='ide'/>
  </disk>
  <disk type='file' device='cdrom'>
   <driver name='qemu' type='raw'/>
   <target dev='hdb' bus='ide'/>
   <readonly/>
  </disk>
----
|The VM uses the `/usr/bin/qemu-kvm` binary file for emulation and it has two disk devices attached.

The first disk is a virtualized hard-drive based on the `/var/lib/libvirt/images/Testguest.qcow2` stored on the host, and its logical device name is set to `hda`.

The second disk is a virtualized CD-ROM and its logical device name is set to `hdb`.
endif::[]
ifeval::[{ProductNumber} == 9]
a|
[subs="+quotes"]
--
 <devices>
  <emulator>/usr/libexec/qemu-kvm</emulator>
  <disk type='file' device='disk'>
   <driver name='qemu' type='qcow2'/>
   <source file='/var/lib/libvirt/images/Testguest.qcow2'/>
   <target dev='vda' bus='virtio'/>
  </disk>
  <disk type='file' device='cdrom'>
   <driver name='qemu' type='raw'/>
   <target dev='sdb' bus='sata'/>
   <readonly/>
  </disk>
--
|The VM uses the `/usr/libexec/qemu-kvm` binary file for emulation and it has two disk devices attached.

The first disk is a virtualized hard-drive based on the `/var/lib/libvirt/images/Testguest.qcow2` stored on the host, and its logical device name is set to `vda`. In windows guests, it is recommended to use `sata` bus instead of `virtio`.

The second disk is a virtualized CD-ROM and its logical device name is set to `sdb`.
endif::[]

a|
[subs="+quotes"]
----
  <controller type='usb' index='0' model='qemu-xhci' ports='15'/>
  <controller type='sata' index='0'/>
  <controller type='pci' index='0' model='pcie-root'/>
  <controller type='pci' index='1' model='pcie-root-port'>
   <model name='pcie-root-port'/>
   <target chassis='1' port='0x10'/>
  </controller>
  <controller type='pci' index='2' model='pcie-root-port'>
   <model name='pcie-root-port'/>
   <target chassis='2' port='0x11'/>
  </controller>
  <controller type='pci' index='3' model='pcie-root-port'>
   <model name='pcie-root-port'/>
   <target chassis='3' port='0x12'/>
  </controller>
  <controller type='pci' index='4' model='pcie-root-port'>
   <model name='pcie-root-port'/>
   <target chassis='4' port='0x13'/>
  </controller>
  <controller type='pci' index='5' model='pcie-root-port'>
   <model name='pcie-root-port'/>
   <target chassis='5' port='0x14'/>
  </controller>
  <controller type='pci' index='6' model='pcie-root-port'>
   <model name='pcie-root-port'/>
   <target chassis='6' port='0x15'/>
  </controller>
  <controller type='pci' index='7' model='pcie-root-port'>
   <model name='pcie-root-port'/>
   <target chassis='7' port='0x16'/>
  </controller>
  <controller type='virtio-serial' index='0'/>
----
|The VM uses a single controller for attaching USB devices, and a root controller for PCI-Express (PCIe) devices. In addition, a `virtio-serial` controller is available, which enables the VM to interact with the host in a variety of ways, such as the serial console.

For more information about virtual devices, see
ifdef::virt-title[]
xref:types-of-virtual-devices_managing-virtual-devices[Types of virtual devices].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-devices_configuring-and-managing-virtualization#types-of-virtual-devices_managing-virtual-devices[Types of virtual devices].
endif::[]

a|
ifeval::[{ProductNumber} == 8]
[subs="+quotes"]
----
 <interface type='network'>
  <mac address='52:54:00:65:29:21'/>
  <source network='default'/>
  <model type='rtl8139'/>
 </interface>
----
|A network interface is set up in the VM that uses the `default` virtual network and the `rtl8139` network device model.
endif::[]
ifeval::[{ProductNumber} == 9]
[subs="+quotes"]
----
 <interface type='network'>
  <mac address='52:54:00:65:29:21'/>
  <source network='default'/>
  <model type='virtio'/>
 </interface>
----
|A network interface is set up in the VM that uses the `default` virtual network and the `virtio` network device model. In windows guests, it is recommended to use `e1000e` model instead of `virtio`.
endif::[]

For information about configuring the network interface, see
ifdef::virt-title[]
xref:optimizing-virtual-machine-network-performance_optimizing-virtual-machine-performance-in-rhel[Optimizing virtual machine network performance].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/optimizing-virtual-machine-performance-in-rhel_configuring-and-managing-virtualization#optimizing-virtual-machine-network-performance_optimizing-virtual-machine-performance-in-rhel[Optimizing virtual machine network performance].
endif::[]

ifeval::[{ProductNumber} == 8]
a|
[subs="+quotes"]
----
  <serial type='pty'>
   <target type='isa-serial' port='0'>
    <model name='isa-serial'/>
   </target>
  </serial>
  <console type='pty'>
   <target type='serial' port='0'/>
  </console>
  <channel type='unix'>
   <target type='virtio' name='org.qemu.guest_agent.0'/>
   <address type='virtio-serial' controller='0' bus='0' port='1'/>
  </channel>
  <channel type='spicevmc'>
   <target type='virtio' name='com.redhat.spice.0'/>
    <address type='virtio-serial' controller='0' bus='0' port='2'/>
  </channel>
----
|A `pty` serial console is set up on the VM, which enables rudimentary VM communication with the host. The console uses the `UNIX` channel on port 1, and the paravirtualized `SPICE` on port 2. This is set up automatically and changing these settings is not recommended.

For more information about interacting with VMs, see
ifdef::virt-title[]
xref:viewing-vm-consoles-using-the-rhel-web-console_assembly_connecting-to-virtual-machines[Interacting with virtual machines by using the web console].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#viewing-vm-consoles-using-the-rhel-web-console_assembly_connecting-to-virtual-machines[Interacting with virtual machines by using the web console].
endif::[]
endif::[]

ifeval::[{ProductNumber} == 9]
a|
[subs="+quotes"]
----
  <serial type='pty'>
   <target type='isa-serial' port='0'>
    <model name='isa-serial'/>
   </target>
  </serial>
  <console type='pty'>
   <target type='serial' port='0'/>
  </console>
  <channel type='unix'>
   <target type='virtio' name='org.qemu.guest_agent.0'/>
   <address type='virtio-serial' controller='0' bus='0' port='1'/>
  </channel>
----
|A `pty` serial console is set up on the VM, which enables rudimentary VM communication with the host. The console uses the `UNIX` channel on port 1. This is set up automatically and changing these settings is not recommended.

For more information about interacting with VMs, see
ifdef::virt-title[]
xref:viewing-vm-consoles-using-the-rhel-web-console_assembly_connecting-to-virtual-machines[Interacting with virtual machines by using the web console].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#viewing-vm-consoles-using-the-rhel-web-console_assembly_connecting-to-virtual-machines[Interacting with virtual machines by using the web console].
endif::[]
endif::[]

a|
[subs="+quotes"]
----
  <input type='tablet' bus='usb'>
   <address type='usb' bus='0' port='1'/>
  </input>
  <input type='mouse' bus='ps2'/>
  <input type='keyboard' bus='ps2'/>
----
|The VM uses a virtual *usb* port, which is set up to receive tablet input, and a virtual *ps2* port set up to receive mouse and keyboard input. This is set up automatically and changing these settings is not recommended.

ifeval::[{ProductNumber} == 9]
a|
[subs="+quotes"]
----
  <graphics type='vnc' port='-1' autoport='yes' listen='127.0.0.1'>
   <listen type='address' address='127.0.0.1'/>
  </graphics>
----
|The VM uses the `vnc` protocol for rendering its graphical output.
endif::[]
ifeval::[{ProductNumber} == 8]
a|
[subs="+quotes"]
----
  <graphics type='spice' autoport='yes' listen='127.0.0.1'>
   <listen type='address' address='127.0.0.1'/>
   <image compression='off'/>
  </graphics>
  <graphics type='vnc' port='-1' autoport='yes' listen='127.0.0.1'>
   <listen type='address' address='127.0.0.1'/>
  </graphics>
----
|The VM uses the `VNC` and `SPICE` protocols for rendering its graphical output, and image compression is turned off.
a|
[subs="+quotes"]
----
  <sound model='ich6'>
   <address type='pci' domain='0x0000' bus='0x00' slot='0x04' function='0x0'/>
  </sound>
  <video>
   <model type='qxl' ram='65536' vram='65536' vgamem='16384' heads='1' primary='yes'/>
   <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
  </video>
----
|An `ICH6` HDA sound device is set up for the VM, and the QEMU `QXL` paravirtualized framebuffer device is set up as the video accelerator. This is set up automatically and changing these settings is not recommended.
endif::[]

ifeval::[{ProductNumber} == 8]
a|
[subs="+quotes"]
----
  <redirdev bus='usb' type='spicevmc'>
   <address type='usb' bus='0' port='1'/>
  </redirdev>
  <redirdev bus='usb' type='spicevmc'>
   <address type='usb' bus='0' port='2'/>
  </redirdev>
  <memballoon model='virtio'>
   <address type='pci' domain='0x0000' bus='0x00' slot='0x07' function='0x0'/>
  </memballoon>
 </devices>
</domain>
----
|The VM has two re-directors for attaching USB devices remotely, and memory ballooning is turned on. This is set up automatically and changing these settings is not recommended.
endif::[]
ifeval::[{ProductNumber} == 9]
a|
[subs="+quotes"]
----
  <redirdev bus='usb' type='tcp'>
   <source mode='connect' host='localhost' service='4000'/>
   <protocol type='raw'/>
  </redirdev>
  <memballoon model='virtio'>
   <address type='pci' domain='0x0000' bus='0x00' slot='0x07' function='0x0'/>
  </memballoon>
 </devices>
</domain>
----
|The VM uses `tcp` re-director for attaching USB devices remotely, and memory ballooning is turned on. This is set up automatically and changing these settings is not recommended.
endif::[]
|===
