:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_managing-storage-for-virtual-machines-using-the-web-console.adoc

:experimental:

[id="removing-storage-pools-using-the-web-console_{context}"]
= Removing storage pools by using the web console

You can remove storage pools to free up resources on the host or on the network to improve system performance. Deleting storage pools also frees up resources that can then be used by other virtual machines (VMs).

[IMPORTANT]
====
Unless explicitly specified, deleting a storage pool does not simultaneously delete the storage volumes inside that pool.
====

To temporarily deactivate a storage pool instead of deleting it, see xref:deactivating-storage-pools-using-the-web-console_assembly_managing-virtual-machine-storage-pools-using-the-web-console[Deactivating storage pools by using the web console]

.Prerequisites

* The web console VM plug-in
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[is installed on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[is installed on your system].
endif::virt-title[]

* xref:detaching-disks-from-virtual-machines_assembly_managing-virtual-machine-storage-disks-using-the-web-console[Detach the disk] from the VM.
* If you want to delete the associated storage volumes along with the pool, activate the pool.

.Procedure

. Click btn:[Storage Pools] on the *Virtual Machines* tab.
+
The *Storage Pools* window appears, showing a list of configured storage pools.
+
image::web-console-storage-pools-window.png[Image displaying all the storage pools currently configured on the host.]

. Click the Menu button btn:[⋮] of the storage pool you want to delete and click btn:[Delete].
+
A confirmation dialog appears.
+
image::virt-cockpit-storage-pool-delete-confirm.png[Image displaying the Delete Storage Pool default dialog box., width=100%]

. *Optional:* To delete the storage volumes inside the pool, select the corresponding check boxes in the dialog.

. Click btn:[Delete].
+
The storage pool is deleted. If you had selected the checkbox in the previous step, the associated storage volumes are deleted as well.


[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::rhel8-virt[]
xref:understanding-storage-pools_understanding-virtual-machine-storage[Understanding storage pools]
endif::[]
ifndef::rhel8-virt[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-storage-for-virtual-machines_configuring-and-managing-virtualization#understanding-storage-pools_understanding-virtual-machine-storage[Understanding storage pools]
endif::[]
* {blank}
ifdef::virt-title[]
xref:viewing-storage-pool-information-using-the-web-console_assembly_managing-virtual-machine-storage-pools-using-the-web-console[Viewing storage pool information by using the web console]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-storage-for-virtual-machines_configuring-and-managing-virtualization#viewing-storage-pool-information-using-the-web-console_assembly_managing-virtual-machine-storage-pools-using-the-web-console[Viewing storage pool information by using the web console]
endif::[]
