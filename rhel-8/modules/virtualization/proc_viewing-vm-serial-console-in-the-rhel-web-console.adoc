:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <viewing-vm-consoles-using-the-rhel-web-console>

// :context: proc_viewing-serial-console-using-the-rhel-8-web-console

:experimental:

[id="viewing-vm-serial-console-in-the-rhel-web-console_{context}"]

= Viewing the virtual machine serial console in the web console

You can view the serial console of a selected virtual machine (VM) in the {ProductShortName}{nbsp}{ProductNumber} web console. This is useful when the host machine or the VM is not configured with a graphical interface.

For more information about the serial console, see  xref:proc_opening-a-virtual-machine-serial-console_assembly_connecting-to-virtual-machines[Opening a virtual machine serial console].

.Prerequisites

* The web console VM plug-in
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[is installed on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[is installed on your system].
endif::virt-title[]

.Procedure

. In the menu:Virtual Machines[] pane, click the VM whose serial console you want to view.
+
A new page opens with an *Overview* and a *Console* section for the VM.

. Select btn:[Serial console] in the console drop down menu.
+
The graphical console appears in the web interface.
+
image::virt-cockpit-serial-console.png[Page displaying the virtual machine serial console along with other VM details.]


You can disconnect and reconnect the serial console from the VM.

* To disconnect the serial console from the VM, click btn:[Disconnect].

* To reconnect the serial console to the VM, click btn:[Reconnect].

[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::virt-title[]
xref:viewing-guest-graphical-console-in-the-rhel-web-console_viewing-vm-consoles-using-the-rhel-web-console[Viewing the virtual machine graphical console in the web console]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#viewing-guest-graphical-console-in-the-rhel-web-console_viewing-vm-consoles-using-the-rhel-web-console[Viewing the virtual machine graphical console in the web console]
endif::[]
* {blank}
ifdef::virt-title[]
xref:viewing-graphical-console-in-remote-viewer_viewing-vm-consoles-using-the-rhel-web-console[Viewing the graphical console in a remote viewer by using the web console]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#viewing-graphical-console-in-remote-viewer_viewing-vm-consoles-using-the-rhel-web-console[Viewing the graphical console in a remote viewer by using the web console]
endif::[]

// Commented out for the time being to allow for Cockpit title building
// For more information about the serial console, see  xref:proc_opening-a-virtual-machine-serial-console_assembly_connecting-to-virtual-machines[].
