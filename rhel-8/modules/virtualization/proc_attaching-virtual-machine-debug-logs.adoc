:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_generating-debug-logs.adoc

[id="attaching-virtual-machine-debug-logs_{context}"]
=  Attaching libvirt debug logs to support requests

You may have to request additional support to diagnose and resolve virtual machine (VM) problems. Attaching the debug logs to the support request is highly recommended to ensure that the support team has access to all the information they need to provide a quick resolution of the VM-related problem.

.Procedure

* To report a problem and request support, link:https://access.redhat.com/support/cases/#/case/new?intcmp=hp|a|a3|case&[open a support case].

* Based on the encountered problems, attach the following logs along with your report:

** For problems with the libvirt service, attach the `/var/log/libvirt/libvirt.log` file from the host.
** For problems with a specific VM, attach its respective log file.
+
For example, for the _testguest1_ VM, attach the `testguest1.log` file, which can be found at `/var/log/libvirt/qemu/testguest1.log`.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/2112[How to provide log files to {RH} Support?]

