:_mod-docs-content-type: CONCEPT
// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/con_creating-virtual-machine-snapshots.adoc[leveloffset=+1]

[id="creating-virtual-machine-snapshots_{context}"]
= Understanding virtual machine snapshots

You can create a snapshot from any virtual machine (VM) on the host. The snapshot metadata can be supplied in an XML file or on the command line. You can also create a snapshot by cloning an existing snapshot.

Snapshots can include:

* Only the disk of the VM

* The disk and memory state of the VM

Snapshots can be used to:

* Save a clean copy of an operating system

* Save a VM's state before performing a potentially destructive operation

There are two kinds of snapshots:

* _Internal snapshots_ are contained completely within a qcow2 file and are fully supported by `libvirt`, allowing for creating, deleting, and reverting of snapshots. This is the default setting used by `libvirt` when creating a snapshot, especially when no option is specified. This file type take slightly longer than others for creating the snapshot. In addition, internal snapshots require qcow2 disks.
+
[IMPORTANT]
Internal snapshots are not being actively developed, and Red Hat discourages their use.

* _External snapshots_ work with any type of original disk image, can be taken with no VM downtime, and are more stable and reliable. As such, external snapshots are recommended for use on KVM machines. However, external snapshots are currently not fully implemented. A workaround is described on  link:https://wiki.libvirt.org/page/I_created_an_external_snapshot,_but_libvirt_will_not_let_me_delete_or_revert_to_it[ libvirt upstream pages].

[IMPORTANT]
{ProductName}{nbsp}{ProductNumber} only supports creating snapshots while the VM is paused or powered down. Creating snapshots of running guests (also known as _live snapshots_) is available on Red Hat Virtualization. For details, call your service representative.
