:_mod-docs-content-type: CONCEPT
// This module can be included from assemblies using the following include statement:
// include::<path>/con_virtual-networking-default-configuration.adoc[leveloffset=+1]


[id="virtual-networking-default-configuration_{context}"]
= Virtual networking default configuration

When the
ifeval::[{ProductNumber} == 8]
`libvirtd`
endif::[]
ifeval::[{ProductNumber} == 9]
`virtnetworkd`
endif::[]
service is first installed on a virtualization host, it contains an initial virtual network configuration in network address translation (NAT) mode. By default, all VMs on the host are connected to the same `libvirt` virtual network, named *default*. VMs on this network can connect to locations both on the host and on the network beyond the host, but with the following limitations:

* VMs on the network are visible to the host and other VMs on the host, but the network traffic is affected by the firewalls in the guest operating system's network stack and by the `libvirt` network filtering rules attached to the guest interface.

* VMs on the network can connect to locations outside the host but are not visible to them. Outbound traffic is affected by the NAT rules, as well as the host system's firewall.

The following diagram illustrates the default VM network configuration:

image::vn-08-network-overview.png[]


// OUT OF PLACE HERE, FIND A BETTER SPOT? Note that a virtual network can be restricted to a specific physical interface. This may be useful on a physical system that has several interfaces (for example, *eth0*, *eth1*, and *eth2*). This is only useful in routed and NAT modes, and can be defined in the XML configuration `dev=<interface>` option, or in the web console when creating a new virtual network.
