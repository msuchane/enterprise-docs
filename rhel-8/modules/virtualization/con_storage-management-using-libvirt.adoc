:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// Understanding virtual machine storage


[id="con_storage-management-using-libvirt_{context}"]
= Storage management by using libvirt

[role="_abstract"]
By using the `libvirt` remote protocol, you can manage all aspects of VM storage. These operations can also be performed on a remote host. Consequently, a management application that uses `libvirt`, such as the {ProductShortName} web console, can be used to perform all the required tasks of configuring the storage of a VM.

You can use the `libvirt` API to query the list of volumes in a storage pool or to get information regarding the capacity, allocation, and available storage in that storage pool. For storage pools that support it, you can also use the `libvirt` API to create, clone, resize, and delete storage volumes. Furthermore, you can use the `libvirt`  API to upload data to storage volumes, download data from storage volumes, or wipe data from storage volumes.
