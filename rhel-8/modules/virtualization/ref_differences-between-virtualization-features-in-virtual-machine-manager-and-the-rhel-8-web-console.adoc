// Module included in the following assemblies:
//
// managing-virtual-machines-in-the-web-console

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_differences-between-virtualization-features-in-virtual-machine-manager-and-the-rhel-8-web-console.adoc[leveloffset=+1]
:_mod-docs-content-type: REFERENCE
[id="differences-between-virtualization-features-in-virtual-machine-manager-and-the-rhel-8-web-console_{context}"]
= Differences between virtualization features in Virtual Machine Manager and the web console

[role="_abstract"]
The Virtual Machine Manager (*virt-manager*) application is supported in {ProductShortName}{nbsp}{ProductNumber}, but has been deprecated. The web console is intended to become its replacement in a subsequent major release. It is, therefore, recommended that you get familiar with the web console for managing virtualization in a GUI.

However, in {ProductShortName}{nbsp}{ProductNumber}, some VM management tasks can only be performed in *virt-manager* or the command line. The following table highlights the features that are available in *virt-manager* but not available in the {ProductShortName} 8.0 web console.

If a feature is available in a later minor version of {ProductShortName}{nbsp}{ProductNumber}, the minimum {ProductShortName}{nbsp}{ProductNumber} version appears in the _Support in web console introduced_ column.


[options="header"]
.VM managemennt tasks that cannot be performed using the web console in {ProductShortName} 8.0
|===
|Task |Support in web console introduced |Alternative method by using CLI
|Setting a virtual machine to start when the host boots |{ProductShortName} 8.1 | `virsh autostart`
|Suspending a virtual machine |{ProductShortName} 8.1 | `virsh suspend`
|Resuming a suspended virtual machine |{ProductShortName} 8.1 | `virsh resume`
|Creating file-system directory storage pools | {ProductShortName} 8.1 |`virsh pool-define-as`
|Creating NFS storage pools | {ProductShortName} 8.1 |`virsh pool-define-as`
|Creating physical disk device storage pools | {ProductShortName} 8.1 |`virsh pool-define-as`
|Creating LVM volume group storage pools | {ProductShortName} 8.1 |`virsh pool-define-as`
|Creating partition-based storage pools | _CURRENTLY UNAVAILABLE_|`virsh pool-define-as`
|Creating GlusterFS-based storage pools | _CURRENTLY UNAVAILABLE_|`virsh pool-define-as`
|Creating vHBA-based storage pools with SCSI devices | _CURRENTLY UNAVAILABLE_|`virsh pool-define-as`
|Creating Multipath-based storage pools | _CURRENTLY UNAVAILABLE_|`virsh pool-define-as`
|Creating RBD-based storage pools | _CURRENTLY UNAVAILABLE_|`virsh pool-define-as`
|Creating a new storage volume |{ProductShortName} 8.1 |`virsh vol-create`
|Adding a new virtual network |{ProductShortName} 8.1 |`virsh net-create` or `virsh net-define`
|Deleting a virtual network |{ProductShortName} 8.1 |`virsh net-undefine`
|Creating a bridge from a host machine's interface to a virtual machine | _CURRENTLY UNAVAILABLE_ |`virsh iface-bridge`
|Creating a snapshot | _CURRENTLY UNAVAILABLE_ |`virsh snapshot-create-as`
|Reverting to a snapshot | _CURRENTLY UNAVAILABLE_ |`virsh snapshot-revert`
|Deleting a snapshot | _CURRENTLY UNAVAILABLE_ |`virsh snapshot-delete`
|Cloning a virtual machine |{ProductShortName} 8.4 |`virt-clone`
|Migrating a virtual machine to another host machine | {ProductShortName} 8.5 |`virsh migrate`
|Attaching a host device to a VM | {ProductShortName} 8.5 |`virt-xml --add-device`
|Removing a host device from a VM | {ProductShortName} 8.5 |`virt-xml --remove-device`
|===
// Deleting a virtual network will be added in 8.2 (possibly)

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_getting_started_guide/chap-virtualization_manager-introduction[Getting started with Virtual Machine Manager in RHEL 7 (_Deprecated in RHEL 8 and later_)]
