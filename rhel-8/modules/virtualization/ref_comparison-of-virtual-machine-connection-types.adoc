:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// types-of-virtual-machine-network-connections


[id="comparison-of-virtual-machine-connection-types_{context}"]
= Comparison of virtual machine connection types

The following table provides information about the locations to which selected types of virtual machine (VM) network configurations can connect, and to which they are visible.

.Virtual machine connection types
[options="header", cols="h,d,d,d,d"]
|====
||Connection to the host|Connection to other VMs on the host|Connection to outside locations|Visible to outside locations
|Bridged mode|YES|YES|YES|YES
|NAT|YES|YES|YES|_no_
|Routed mode|YES|YES|YES|YES
|Isolated mode|YES|YES|_no_|_no_
|Open mode 4+^|_Depends on the host's firewall rules_
|====
