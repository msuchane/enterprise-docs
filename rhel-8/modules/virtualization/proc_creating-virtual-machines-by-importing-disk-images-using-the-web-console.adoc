:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// creating-vms-and-installing-an-os-using-the-rhel-web-console

[id="creating-virtual-machines-by-importing-disk-images-using-the-web-console_{context}"]
= Creating virtual machines by importing disk images by using the web console

You can create a virtual machine (VM) by importing a disk image of an existing VM installation in the {ProductShortName}{nbsp}{ProductNumber} web console. 

.Prerequisites

ifdef::virt-title[]
* xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[The web console VM plug-in is installed on your system].
endif::[]
ifndef::virt-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[The web console VM plug-in is installed on your system].
endif::virt-title[]

* You have a sufficient amount of system resources to allocate to your VMs, such as disk space, RAM, or CPUs. The recommended values can vary significantly depending on the intended tasks and workload of the VMs.

* You have downloaded a disk image of an existing VM installation.

.Procedure

. In the *Virtual Machines* interface of the web console, click *btn:[Import VM]*.
+
The *Import a virtual machine dialog* appears.
+
image::virt-cockpit-import.png[Image displaying the Import a virtual machine dialog box.]
+
. Enter the basic configuration of the VM you want to create:
* *Name* - The name of the VM.
* *Disk image* - The path to the existing disk image of a VM on the host system.
* *Operating system* - The operating system running on a VM disk. Note that {RH} provides support only for
ifdef::virt-title[]
xref:recommended-features-in-rhel-{ProductNumber}-virtualization_feature-support-and-limitations-in-rhel-{ProductNumber}-virtualization[a limited set of guest operating systems].
endif::virt-title[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/feature-support-and-limitations-in-rhel-{ProductNumber}-virtualization_configuring-and-managing-virtualization#recommended-features-in-rhel-{ProductNumber}-virtualization_feature-support-and-limitations-in-rhel-{ProductNumber}-virtualization[a limited set of guest operating systems].
endif::virt-title[]

* *Memory* - The amount of memory to allocate for use by the VM.

. Import the VM:
** To install the operating system on the VM without additional edits to the VM settings, click btn:[Import and run].
** To edit the VM settings before the installation of the operating system, click btn:[Import and edit].
