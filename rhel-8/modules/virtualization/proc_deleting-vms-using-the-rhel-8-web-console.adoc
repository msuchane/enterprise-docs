:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_deleting-virtual-machines
// creating-vms-and-installing-an-os-using-the-rhel-web-console
// getting-started-with-virtualization-in-rhel8
// managing-virtual-machines-in-the-web-console


:experimental:

[id="deleting-vms-using-the-rhel-web-console_{context}"]

= Deleting virtual machines by using the web console

To delete a virtual machine (VM) and its associated storage files from the host to which the {ProductShortName} {ProductNumber} web console is connected with, follow the procedure below:

.Prerequisites

* The web console VM plug-in
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[is installed on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[is installed on your system].
endif::virt-title[]

* Back up important data from the VM.
* Make sure no other VM uses the same associated storage.
* *Optional:* Shut down the VM.

.Procedure

. In the menu:Virtual Machines[] interface, click the Menu button btn:[⋮] of the VM that you want to delete.
+
A drop down menu appears with controls for various VM operations.
// Removing image to reduce overhead and doesn't provide much value
//+
//image::virt-cockpit-shut-VM-operations.png[Image displaying the VM operations available when it is shut down.]

. Click btn:[Delete].
+
A confirmation dialog appears.
+
image::virt-cockpit-vm-delete-confirm.png[Image displaying the Confirm deletion of VM dialog box. ,width=100%]

. *Optional:* To delete all or some of the storage files associated with the VM, select the checkboxes next to the storage files you want to delete.

. Click btn:[Delete].
+
The VM and any selected storage files are deleted.
