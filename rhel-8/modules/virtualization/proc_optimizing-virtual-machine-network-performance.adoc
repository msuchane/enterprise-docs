:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// optimizing-virtual-machine-performance-in-rhel

[id="optimizing-virtual-machine-network-performance_{context}"]
= Optimizing virtual machine network performance

Due to the virtual nature of a VM's network interface card (NIC), the VM loses a portion of its allocated host network bandwidth, which can reduce the overall workload efficiency of the VM. The following tips can minimize the negative impact of virtualization on the virtual NIC (vNIC) throughput.

.Procedure

Use any of the following methods and observe if it has a beneficial effect on your VM network performance:
// TBD add link to network diagnostics?

Enable the vhost_net module::
On the host, ensure the `vhost_net` kernel feature is enabled:
+
[subs="+quotes,attributes"]
----
# *lsmod | grep vhost*
vhost_net              32768  1
vhost                  53248  1 vhost_net
tap                    24576  1 vhost_net
tun                    57344  6 vhost_net
----
+
If the output of this command is blank, enable the `vhost_net` kernel module:
+
[subs="+quotes,attributes"]
----
# *modprobe vhost_net*
----

Set up multi-queue virtio-net::
To set up the _multi-queue virtio-net_ feature for a VM, use the `virsh edit` command to edit to the XML configuration of the VM. In the XML, add the following to the `<devices>` section, and replace `N` with the number of vCPUs in the VM, up to 16:
+
[subs="+quotes,attributes"]
----
<interface type='network'>
      <source network='default'/>
      <model type='virtio'/>
      <driver name='vhost' queues='N'/>
</interface>
----
+
////
*TBD: Create a virt-xml command to do this. Concept:*
For example, the following configures the _wubwubhub_ VM to use a virtio-net interface with 8 queues
+
[subs="+quotes,attributes"]
--
# *virt-xml wubwubhub --add-device --network --model type=virtio --driver name=vhost,queues=N*
--
////
+
If the VM is running, restart it for the changes to take effect.

Batching network packets:: In Linux VM configurations with a long transmission path, batching packets before submitting them to the kernel may improve cache utilization. To set up packet batching, use the following command on the host, and replace _tap0_ with the name of the network interface that the VMs use:
+
[subs="+quotes,attributes"]
----
# *ethtool -C _tap0_ rx-frames 64*
----


SR-IOV::
If your host NIC supports SR-IOV, use SR-IOV device assignment for your vNICs. For more information, see
ifdef::virt-title[]
xref:managing-sr-iov-devices_managing-virtual-devices[Managing SR-IOV devices].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-devices_configuring-and-managing-virtualization#managing-sr-iov-devices_managing-virtual-devices[Managing SR-IOV devices].
endif::[]

////
To add?

* OVS-DPDK in the Host and DPDK in the Guest - probably a special module would be required
* Vhost-net support busy polling by passing poll-us parameter to qemu command line like: -netdev tap,id=hn0,vhost=on,poll-us=N (N=50 is a value that can get the best latency). Unfortunately, this is not supported by libvirt right now. This can improves the latency and PPS with the cost of extra CPU cycles. (source:jasowang)
* For virtio-net guest driver, it can support napi_tx mode. In this mode, driver can do proper socket accounting that can help to reduce the buffer bloat. This may help for the performance of TCP at the cost of more tx interrupts. It can also help for qdiscs that want to do fair queuing in guest. It can be enabled through module parameter napi_tx. (source:Jason Wang)
////


[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::virt-title[]
xref:understanding-virtual-networking-overview_configuring-virtual-machine-network-connections[Understanding virtual networking]
endif::virt-title[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/configuring-virtual-machine-network-connections_configuring-and-managing-virtualization#understanding-virtual-networking-overview_configuring-virtual-machine-network-connections[Understanding virtual networking]
endif::virt-title[]


// * TBD? How to measure virt networking performance?
