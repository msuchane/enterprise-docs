:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// feature-support-and-limitations-in-rhel-8-virtualization

[id="an-overview-of-virtualization-features-support-in-rhel-8_{context}"]
= An overview of virtualization features support in {ProductShortName}{nbsp}8

The following tables provide comparative information about the support state of selected virtualization features in {ProductShortName}{nbsp}8 across the supported system architectures.

.Device hot plug and hot unplug
[options="header", cols="h,d,d,d"]
|====
||Intel 64 and AMD64|IBM Z|IBM POWER
|CPU hot plug |Supported|Supported|Supported
|CPU hot unplug|_UNSUPPORTED_|_UNSUPPORTED_|_UNSUPPORTED_
|Memory hot plug|Supported|_UNSUPPORTED_|Supported
|Memory hot unplug|_UNSUPPORTED_|_UNSUPPORTED_|_UNSUPPORTED_
|PCI hot plug|Supported|Supported footnote:[Requires using `virtio-_*_-ccw` devices instead of `virtio-_*_-pci`]|Supported
|PCI hot unplug|Supported|Supported footnote:[Requires using `virtio-_*_-ccw` devices instead of `virtio-_*_-pci`]|Supported
|====

.Other selected features
[options="header",cols="h,d,d,d"]
|====
||Intel 64 and AMD64|IBM Z|IBM POWER
|NUMA tuning|Supported|_UNSUPPORTED_|Supported
|SR-IOV devices|Supported|_UNSUPPORTED_|Supported
|virt-v2v and p2v|Supported|_UNSUPPORTED_|_UNSUPPORTED_
|====

Note that some of the unsupported features are supported on other {RH} products, such as {RH}{nbsp}Virtualization and {RH}{nbsp}OpenStack platform. For more information, see xref:unsupported-features-in-rhel-8-virtualization_feature-support-and-limitations-in-rhel-8-virtualization[Unsupported features in {ProductShortName}{nbsp}8 virtualization].

.Additional sources

* For a complete list of unsupported features of virtual machines in {ProductShortName}{nbsp}8, see xref:unsupported-features-in-rhel-8-virtualization_feature-support-and-limitations-in-rhel-8-virtualization[Unsupported features in {ProductShortName}{nbsp}8 virtualization].

* For details on the specifics for virtualization on the IBM Z architecture, see xref:how-virtualization-on-ibm-z-differs-from-amd64-and-intel64_getting-started-with-virtualization-in-rhel-8-on-ibm-z[How virtualization on IBM Z differs from AMD64 and Intel 64].

* For details on the specifics for virtualization on the IBM POWER architecture, see xref:how-virtualization-on-ibm-power-differs-from-amd64-and-intel64_getting-started-with-virtualization-in-rhel-8-on-ibm-power[How virtualization on IBM POWER differs from AMD64 and Intel 64].
