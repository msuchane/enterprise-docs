:_mod-docs-content-type: PROCEDURE
:experimental:

// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/proc_securing-iscsi-storage-pools-with-libvirt-secrets.adoc[leveloffset=+1]

[id="securing-iscsi-storage-pools-with-libvirt-secrets_{context}"]
= Securing iSCSI storage pools with libvirt secrets

Username and password parameters can be configured with `virsh` to secure an iSCSI storage pool. You can configure this before or after you define the pool, but the pool must be started for the authentication settings to take effect.

The following provides instructions for securing iSCSI-based storage pools with [command]`libvirt` secrets.

[NOTE]
====
This procedure is required if a `_user_ID_` and `_password_` were defined when creating the iSCSI target.
====

.Prerequisites
* Ensure that you have created an iSCSI-based storage pool. For more information, see xref:creating-iscsi-based-storage-pools-using-the-cli_assembly_managing-virtual-machine-storage-pools-using-the-cli[Creating iSCSI-based storage pools by using the CLI].

.Procedure

. Create a libvirt secret file with a challenge-handshake authentication protocol (CHAP) user name. For example:
+
[source,xml]
----
<secret ephemeral='no' private='yes'>
    <description>Passphrase for the iSCSI example.com server</description>
    <usage type='iscsi'>
        <target>iscsirhel7secret</target>
    </usage>
</secret>
----

. Define the libvirt secret with the `virsh secret-define` command:
+
[literal, subs="+quotes"]
----
# *virsh secret-define _secret.xml_*
----

. Verify the UUID with the `virsh secret-list` command:
+
[literal, subs="+quotes"]
----
# *virsh secret-list*
UUID                                       Usage
--------------------------------------------------------------
2d7891af-20be-4e5e-af83-190e8a922360      iscsi iscsirhel7secret
----

. Assign a secret to the UUID in the output of the previous step using the `virsh secret-set-value` command. This ensures that the CHAP username and password are in a libvirt-controlled secret list. For example:
+
[literal, subs="+quotes"]
----
# *virsh secret-set-value --interactive _2d7891af-20be-4e5e-af83-190e8a922360_*
Enter new value for secret:
Secret value set
----

. Add an authentication entry in the storage pool's XML file using the `virsh edit` command, and add an `<auth>` element, specifying `authentication type`, `username`, and `secret usage`. For example:
+
[source,xml]
----
<pool type='iscsi'>
  <name>iscsirhel7pool</name>
    <source>
       <host name='192.0.2.1'/>
       <device path='iqn.2010-05.com.example.server1:iscsirhel7guest'/>
       <auth type='chap' username='_example-user_'>
          <secret usage='iscsirhel7secret'/>
       </auth>
    </source>
  <target>
    <path>/dev/disk/by-path</path>
  </target>
</pool>
----
+
[NOTE]
====
The `*<auth>*` sub-element exists in different locations within the virtual machine's `*<pool>*` and `*<disk>*` XML elements. For a `*<pool>*`, `*<auth>*` is specified within the `*<source>*` element, as this describes where to find the pool sources, since authentication is a property of some pool sources (iSCSI and RBD). For a `*<disk>*`, which is a sub-element of a domain, the authentication to the iSCSI or RBD disk is a property of the disk. In addition, the `*<auth>*` sub-element for a disk differs from that of a storage pool.

[source,xml]
----
<auth username='redhat'>
  <secret type='iscsi' usage='iscsirhel7secret'/>
</auth>
----
====

. To activate the changes, activate the storage pool. If the pool has already been started, stop and restart the storage pool:
+
[literal, subs="+quotes"]
----
# *virsh pool-destroy _iscsirhel7pool_*
# *virsh pool-start _iscsirhel7pool_*
----
