:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// installing-kvm-paravirtualized-drivers-for-rhel-virtual-machines

[id="proc_installing-qemu-guest-agent-on-windows-guests_{context}"]
= Enabling QEMU Guest Agent on Windows guests

To allow a {ProductShortName} host to perform xref:virtualization-features-that-require-qemu-guest-agent_enabling-qemu-guest-agent-features-on-your-virtual-machines[a certain subset of operations] on a Windows virtual machine (VM), you must enable the QEMU Guest Agent (GA). To do so, add a storage device that contains the QEMU Guest Agent installer to an existing VM or when creating a new VM, and install the drivers on the Windows guest operating system.

To install the Guest Agent (GA) by using the graphical interface, see the procedure below. To install the GA in a command-line interface, use the link:https://docs.microsoft.com/en-us/windows/win32/msi/about-windows-installer[Microsoft Windows Installer (MSI)].

.Prerequisites

* An installation medium with the Guest Agent is attached to the VM. For instructions on preparing the medium, see xref:installing-kvm-drivers-on-a-host-machine_installing-kvm-paravirtualized-drivers-for-rhel-virtual-machines[Preparing virtio driver installation media on a host machine].

.Procedure

. In the Windows guest operating system, open the `File Explorer` application.

. Click `This PC`.

. In the `Devices and drives` pane, open the `virtio-win` medium.

. Open the `guest-agent` folder.

. Based on the operating system installed on the VM, run one of the following installers:
* If using a 32-bit operating system, run the `qemu-ga-i386.msi` installer.
* If using a 64-bit operating system, run the `qemu-ga-x86_64.msi` installer.

. *Optional*: If you want to use the para-virtualized serial driver (`virtio-serial`) as the communication interface between the host and the Windows guest, verify that the `virtio-serial` driver is installed on the Windows guest. For more information about installing `virtio` drivers, see: xref:installing-kvm-drivers-on-a-windows-guest_installing-kvm-paravirtualized-drivers-for-rhel-virtual-machines[Installing virtio drivers on a Windows guest].

.Verification

. On your Windows VM, navigate to the *Services* window.
+
*Computer Management > Services*
. Ensure that the status of the `QEMU Guest Agent` service is `Running`.


[role="_additional-resources"]
.Additional resources

* xref:virtualization-features-that-require-qemu-guest-agent_enabling-qemu-guest-agent-features-on-your-virtual-machines[Virtualization features that require QEMU Guest Agent]