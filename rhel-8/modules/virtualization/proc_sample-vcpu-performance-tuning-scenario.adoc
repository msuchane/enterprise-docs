:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_optimizing-virtual-machine-cpu-performance

[id="sample-vcpu-performance-tuning-scenario_{context}"]
= Sample vCPU performance tuning scenario

To obtain the best vCPU performance possible, {RH} recommends by using manual `vcpupin`, `emulatorpin`, and `numatune` settings together, for example like in the following scenario.

.Starting scenario

* Your host has the following hardware specifics:
+
--
** 2 NUMA nodes
** 3 CPU cores on each node
** 2 threads on each core
--
+
The output of `virsh nodeinfo` of such a machine would look similar to:
+
[literal, subs="+quotes"]
--
# *virsh nodeinfo*
CPU model:           x86_64
CPU(s):              12
CPU frequency:       3661 MHz
CPU socket(s):       2
Core(s) per socket:  3
Thread(s) per core:  2
NUMA cell(s):        2
Memory size:         31248692 KiB
--

* You intend to modify an existing VM to have 8 vCPUs, which means that it will not fit in a single NUMA node.
+
Therefore, you should distribute 4 vCPUs on each NUMA node and make the vCPU topology resemble the host topology as closely as possible. This means that vCPUs that run as sibling threads of a given physical CPU should be pinned to host threads on the same core. For details, see the _Solution_ below:

.Solution


. Obtain the information about the host topology:
+
[literal,subs="quotes",options="nowrap"]
--
# *virsh capabilities*
--
+
The output should include a section that looks similar to the following:
+
[source,xml]
--
<topology>
  <cells num="2">
    <cell id="0">
      <memory unit="KiB">15624346</memory>
      <pages unit="KiB" size="4">3906086</pages>
      <pages unit="KiB" size="2048">0</pages>
      <pages unit="KiB" size="1048576">0</pages>
      <distances>
        <sibling id="0" value="10" />
        <sibling id="1" value="21" />
      </distances>
      <cpus num="6">
        <cpu id="0" socket_id="0" core_id="0" siblings="0,3" />
        <cpu id="1" socket_id="0" core_id="1" siblings="1,4" />
        <cpu id="2" socket_id="0" core_id="2" siblings="2,5" />
        <cpu id="3" socket_id="0" core_id="0" siblings="0,3" />
        <cpu id="4" socket_id="0" core_id="1" siblings="1,4" />
        <cpu id="5" socket_id="0" core_id="2" siblings="2,5" />
      </cpus>
    </cell>
    <cell id="1">
      <memory unit="KiB">15624346</memory>
      <pages unit="KiB" size="4">3906086</pages>
      <pages unit="KiB" size="2048">0</pages>
      <pages unit="KiB" size="1048576">0</pages>
      <distances>
        <sibling id="0" value="21" />
        <sibling id="1" value="10" />
      </distances>
      <cpus num="6">
        <cpu id="6" socket_id="1" core_id="3" siblings="6,9" />
        <cpu id="7" socket_id="1" core_id="4" siblings="7,10" />
        <cpu id="8" socket_id="1" core_id="5" siblings="8,11" />
        <cpu id="9" socket_id="1" core_id="3" siblings="6,9" />
        <cpu id="10" socket_id="1" core_id="4" siblings="7,10" />
        <cpu id="11" socket_id="1" core_id="5" siblings="8,11" />
      </cpus>
    </cell>
  </cells>
</topology>
--

. *Optional:* Test the performance of the VM by using xref:virtual-machine-performance-monitoring-tools_optimizing-virtual-machine-performance-in-rhel[the applicable tools and utilities].

. Set up and mount 1 GiB huge pages on the host:
+
[NOTE]
====
1 GiB huge pages might not be available on some architectures and configurations, such as ARM 64 hosts.
====

.. Add the following line to the host's kernel command line:
+
// Replace with a link to an appropriate module, when there is one?
+
[literal]
--
default_hugepagesz=1G hugepagesz=1G
--
.. Create the `/etc/systemd/system/hugetlb-gigantic-pages.service` file with the following content:
+
[literal]
--
[Unit]
Description=HugeTLB Gigantic Pages Reservation
DefaultDependencies=no
Before=dev-hugepages.mount
ConditionPathExists=/sys/devices/system/node
ConditionKernelCommandLine=hugepagesz=1G

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/etc/systemd/hugetlb-reserve-pages.sh

[Install]
WantedBy=sysinit.target
--
.. Create the `/etc/systemd/hugetlb-reserve-pages.sh` file with the following content:
+
[literal,subs="+quotes,attributes"]
--
#!/bin/sh

nodes_path=/sys/devices/system/node/
if [ ! -d $nodes_path ]; then
	echo "ERROR: $nodes_path does not exist"
	exit 1
fi

reserve_pages()
{
	echo $1 > $nodes_path/$2/hugepages/hugepages-1048576kB/nr_hugepages
}

reserve_pages 4 node1
reserve_pages 4 node2
--
+
This reserves four 1GiB huge pages from _node1_ and four 1GiB huge pages from _node2_.
.. Make the script created in the previous step executable:
+
[literal,subs="quotes"]
--
# *chmod +x /etc/systemd/hugetlb-reserve-pages.sh*
--
.. Enable huge page reservation on boot:
+
[literal,subs="quotes"]
--
# *systemctl enable hugetlb-gigantic-pages*
--

. Use the `virsh edit` command to edit the XML configuration of the VM you wish to optimize, in this example _super-VM_:
+
[literal,subs="+quotes,attributes"]
--
# *virsh edit super-vm*
--

. Adjust the XML configuration of the VM in the following way:
.. Set the VM to use 8 static vCPUs. Use the `<vcpu/>` element to do this.
.. Pin each of the vCPU threads to the corresponding host CPU threads that it mirrors in the topology. To do so, use the `<vcpupin/>` elements in the `<cputune>` section.
+
Note that, as shown by the `virsh capabilities` utility above, host CPU threads are not ordered sequentially in their respective cores. In addition, the vCPU threads should be pinned to the highest available set of host cores on the same NUMA node. For a table illustration, see the *Sample topology* section below.
+
The XML configuration for steps a. and b. can look similar to:
+
[source,xml]
--
<cputune>
  <vcpupin vcpu='0' cpuset='1'/>
  <vcpupin vcpu='1' cpuset='4'/>
  <vcpupin vcpu='2' cpuset='2'/>
  <vcpupin vcpu='3' cpuset='5'/>
  <vcpupin vcpu='4' cpuset='7'/>
  <vcpupin vcpu='5' cpuset='10'/>
  <vcpupin vcpu='6' cpuset='8'/>
  <vcpupin vcpu='7' cpuset='11'/>
  <emulatorpin cpuset='6,9'/>
</cputune>
--
.. Set the VM to use 1 GiB huge pages:
+
[source,xml]
--
<memoryBacking>
  <hugepages>
    <page size='1' unit='GiB'/>
  </hugepages>
</memoryBacking>
--
.. Configure the VM's NUMA nodes to use memory from the corresponding NUMA nodes on the host. To do so, use the `<memnode/>` elements in the `<numatune/>` section:
+
[source,xml]
--
<numatune>
  <memory mode="preferred" nodeset="1"/>
  <memnode cellid="0" mode="strict" nodeset="0"/>
  <memnode cellid="1" mode="strict" nodeset="1"/>
</numatune>
--

.. Ensure the CPU mode is set to `host-passthrough`, and that the CPU uses cache in `passthrough` mode:
+
[source,xml]
--
<cpu mode="host-passthrough">
  <topology sockets="2" cores="2" threads="2"/>
  <cache mode="passthrough"/>
--
+
ifeval::[{ProductNumber} == 9]
On an ARM 64 system, omit the `<cache mode="passthrough"/>` line.
endif::[]

.Verification

. Confirm that the resulting XML configuration of the VM includes a section similar to the following:
+
[source,xml,subs="+quotes"]
--
[...]
  <memoryBacking>
    <hugepages>
      <page size='1' unit='GiB'/>
    </hugepages>
  </memoryBacking>
  <vcpu placement='static'>8</vcpu>
  <cputune>
    <vcpupin vcpu='0' cpuset='1'/>
    <vcpupin vcpu='1' cpuset='4'/>
    <vcpupin vcpu='2' cpuset='2'/>
    <vcpupin vcpu='3' cpuset='5'/>
    <vcpupin vcpu='4' cpuset='7'/>
    <vcpupin vcpu='5' cpuset='10'/>
    <vcpupin vcpu='6' cpuset='8'/>
    <vcpupin vcpu='7' cpuset='11'/>
    <emulatorpin cpuset='6,9'/>
  </cputune>
  <numatune>
    <memory mode="preferred" nodeset="1"/>
    <memnode cellid="0" mode="strict" nodeset="0"/>
    <memnode cellid="1" mode="strict" nodeset="1"/>
  </numatune>
  <cpu mode="host-passthrough">
    <topology sockets="2" cores="2" threads="2"/>
    <cache mode="passthrough"/>
    <numa>
      <cell id="0" cpus="0-3" memory="2" unit="GiB">
        <distances>
          <sibling id="0" value="10"/>
          <sibling id="1" value="21"/>
        </distances>
      </cell>
      <cell id="1" cpus="4-7" memory="2" unit="GiB">
        <distances>
          <sibling id="0" value="21"/>
          <sibling id="1" value="10"/>
        </distances>
      </cell>
    </numa>
  </cpu>
</domain>
--

. *Optional:* Test the performance of the VM by using xref:virtual-machine-performance-monitoring-tools_optimizing-virtual-machine-performance-in-rhel[the applicable tools and utilities] to evaluate the impact of the VM's optimization.


.Sample topology
* The following tables illustrate the connections between the vCPUs and the host CPUs they should be pinned to:
+
.Host topology
|===
3+^| *CPU threads* | 0 | 3 | 1 | 4 | 2 | 5 | 6 | 9 | 7 | 10 | 8 | 11
3+^| *Cores* 2+^| 0 2+^| 1 2+^| 2 2+^| 3 2+^| 4 2+^| 5
3+^| *Sockets* 6+^| 0 6+^| 1
3+^| *NUMA nodes* 6+^| 0 6+^| 1
|===
+
.VM topology
|===
3+^| *vCPU threads* | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7
3+^| *Cores* 2+^| 0 2+^| 1 2+^| 2 2+^| 3
3+^| *Sockets* 4+^| 0 4+^| 1
3+^| *NUMA nodes* 4+^| 0 4+^| 1
|===
+
.Combined host and VM topology
|===
3+^| *vCPU threads* 2+^| | 0 | 1 | 2 | 3 2+^| | 4 | 5 | 6 | 7
3+^| *Host CPU threads* | 0 | 3 | 1 | 4 | 2 | 5 | 6 | 9 | 7 | 10 | 8 | 11
3+^| *Cores* 2+^| 0 2+^| 1 2+^| 2 2+^| 3 2+^| 4 2+^| 5
3+^| *Sockets* 6+^| 0 6+^| 1
3+^| *NUMA nodes* 6+^| 0 6+^| 1
|===
+
In this scenario, there are 2 NUMA nodes and 8 vCPUs. Therefore, 4 vCPU threads should be pinned to each node.
+
In addition, {RH} recommends leaving at least a single CPU thread available on each node for host system operations.
+
Because in this example, each NUMA node houses 3 cores, each with 2 host CPU threads, the set for node 0 translates as follows:
+
[source, xml]
--
<vcpupin vcpu='0' cpuset='1'/>
<vcpupin vcpu='1' cpuset='4'/>
<vcpupin vcpu='2' cpuset='2'/>
<vcpupin vcpu='3' cpuset='5'/>
--
