:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_managing-gpu-devices-in-virtual-machines


[id="proc_assigning-a-gpu-to-a-virtual-machine_{context}"]
= Assigning a GPU to a virtual machine

[role="_abstract"]
To access and control GPUs that are attached to the host system, you must configure the host system to pass direct control of the GPU to the virtual machine (VM).

[NOTE]
====
If you are looking for information about assigning a virtual GPU, see
ifdef::virt-title[]
xref:assembly_managing-nvidia-vgpu-devices_assembly_managing-gpu-devices-in-virtual-machines[Managing NVIDIA vGPU devices].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/assembly_managing-gpu-devices-in-virtual-machines_configuring-and-managing-virtualization#assembly_managing-nvidia-vgpu-devices_assembly_managing-gpu-devices-in-virtual-machines[Managing NVIDIA vGPU devices].
endif::[]
====

.Prerequisites

* You must enable IOMMU support on the host machine kernel.
** On an Intel host, you must enable VT-d:
. Regenerate the GRUB configuration with the `intel_iommu=on` and `iommu=pt` parameters:
+
[subs="+quotes,attributes"]
----
# *grubby --args="intel_iommu=on iommu_pt" --update-kernel DEFAULT*
----
+
////
NOT NEEDED IF YOU USE PT IN THE PREVIOUS STEP?
. If `intel_iommu=on` works, you can try replacing it with `iommu=pt` to switch it to passthrough mode.
+
[subs="+quotes,attributes"]
----
# *grubby --args="iommu=pt" --update-kernel DEFAULT*
----
+
[NOTE]
====
The `pt` option only enables IOMMU for devices used in pass-through mode and provides better host performance. However, not all hardware supports the option. Revert to the `intel_iommu=on` option if the `iommu=pt` option does not work on your host.
====
////

. Reboot the host.

** On an AMD host, you must enable AMD-Vi.
+
Note that on AMD hosts, IOMMU is enabled by default, you can add `iommu=pt` to switch it to pass-through mode:

. Regenerate the GRUB configuration with the `iommu=pt` parameter:
+
[subs="+quotes,attributes"]
----
# *grubby --args="iommu=pt" --update-kernel DEFAULT*
----
+
[NOTE]
====
The `pt` option only enables IOMMU for devices used in pass-through mode and provides better host performance. However, not all hardware supports the option. You can still assign devices even when this option is not enabled.
====
. Reboot the host.


.Procedure

. Prevent the driver from binding to the GPU.
.. Identify the PCI bus address to which the GPU is attached.
+
[subs="+quotes,attributes"]
----
# *lspci -Dnn | grep VGA*
0000:02:00.0 VGA compatible controller [0300]: NVIDIA Corporation GK106GL [Quadro K4000] [*10de:11fa*] (rev a1)
----

.. Prevent the host's graphics driver from using the GPU. To do so, use the GPU PCI ID with the pci-stub driver.
+
For example, the following command prevents the driver from binding to the GPU attached at the *10de:11fa* bus:
+
[subs="+quotes,attributes"]
----
# *grubby --args="pci-stub.ids=10de:11fa" --update-kernel  DEFAULT*
----
.. Reboot the host.

. *Optional:* If certain GPU functions, such as audio, cannot be passed through to the VM due to support limitations, you can modify the driver bindings of the endpoints within an IOMMU group to pass through only the necessary GPU functions.
.. Convert the GPU settings to XML and note the PCI address of the endpoints that you want to prevent from attaching to the host drivers.
+
To do so, convert the GPU’s PCI bus address to a libvirt-compatible format by adding the  `pci_` prefix to the address, and converting the delimiters to underscores.
+
For example, the following command displays the XML configuration of the GPU attached at the `0000:02:00.0` bus address.
+
[subs="+quotes,attributes"]
----
# *virsh nodedev-dumpxml pci_0000_02_00_0*
----
+
[source,xml]
----
<device>
 <name>pci_0000_02_00_0</name>
 <path>/sys/devices/pci0000:00/0000:00:03.0/0000:02:00.0</path>
 <parent>pci_0000_00_03_0</parent>
 <driver>
  <name>pci-stub</name>
 </driver>
 <capability type='pci'>
  <domain>0</domain>
  <bus>2</bus>
  <slot>0</slot>
  <function>0</function>
  <product id='0x11fa'>GK106GL [Quadro K4000]</product>
  <vendor id='0x10de'>NVIDIA Corporation</vendor>
  <iommuGroup number='13'>
   <address domain='0x0000' bus='0x02' slot='0x00' function='0x0'/>
   <address domain='0x0000' bus='0x02' slot='0x00' function='0x1'/>
  </iommuGroup>
  <pci-express>
   <link validity='cap' port='0' speed='8' width='16'/>
   <link validity='sta' speed='2.5' width='16'/>
  </pci-express>
 </capability>
</device>
----

.. Prevent the endpoints from attaching to the host driver.
+
In this example, to assign the GPU to a VM, prevent the endpoints that correspond to the audio function, `<address domain='0x0000' bus='0x02' slot='0x00' function='0x1'/>`, from attaching to the host audio driver, and instead attach the endpoints to VFIO-PCI.
+
[subs="+quotes,attributes"]
----
# *driverctl set-override 0000:02:00.1 vfio-pci*
----

. Attach the GPU to the VM
.. Create an XML configuration file for the GPU by using the PCI bus address.
+
For example, you can create the following XML file, GPU-Assign.xml, by using parameters from the GPU’s bus address.
+
[source,xml]
----
<hostdev mode='subsystem' type='pci' managed='yes'>
 <driver name='vfio'/>
 <source>
  <address domain='0x0000' bus='0x02' slot='0x00' function='0x0'/>
 </source>
</hostdev>
----
.. Save the file on the host system.
.. Merge the file with the VM's XML configuration.
+
For example,  the following command merges the GPU XML file, GPU-Assign.xml, with the XML configuration file of the `System1` VM.
+
[subs="+quotes,attributes"]
----
# *virsh attach-device System1 --file /home/GPU-Assign.xml --persistent*
Device attached successfully.
----
+
[NOTE]
====
The GPU is attached as a secondary graphics device to the VM. Assigning a GPU as the primary graphics device is not supported, and {RH} does not recommend removing the primary emulated graphics device in the VM’s XML configuration.
====

.Verification
* The device appears under the `<devices>` section in VM's XML configuration. For more information, see
ifdef::virt-title[]
xref:sample-virtual-machine-xml-configuration_viewing-information-about-virtual-machines[Sample virtual machine XML configuration].
endif::virt-title[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/viewing-information-about-virtual-machines_configuring-and-managing-virtualization#sample-virtual-machine-xml-configuration_viewing-information-about-virtual-machines[Sample virtual machine XML configuration].
endif::virt-title[]

.Known Issues

* The number of GPUs that can be attached to a VM is limited by the maximum number of assigned PCI devices, which in {ProductShortName}{nbsp}{ProductNumber} is currently 64. However, attaching multiple GPUs to a VM is likely to cause problems with memory-mapped I/O (MMIO) on the guest, which may result in the GPUs not being available to the VM.
+
To work around these problems, set a larger 64-bit MMIO space and configure the vCPU physical address bits to make the extended 64-bit MMIO space addressable.

* Attaching an NVIDIA GPU device to a VM that uses a {ProductShortName}{nbsp}{ProductNumber} guest operating system currently disables the Wayland session on that VM, and loads an Xorg session instead. This is because of incompatibilities between NVIDIA drivers and Wayland.
