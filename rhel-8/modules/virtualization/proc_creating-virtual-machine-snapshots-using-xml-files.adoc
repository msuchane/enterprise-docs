:_mod-docs-content-type: PROCEDURE
// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/proc_creating-virtual-machine-snapshots-using-xml-files.adoc[leveloffset=+1]

[id="creating-virtual-machine-snapshots-using-xml-files_{context}"]
= Creating virtual machine snapshots by using XML files

The `virsh snapshot-create` command creates a snapshot for the specified virtual machine with the snapshot metadata in a named XML file. The command can use the virtual machine _name_, _id_, or _uid_ to specify the virtual machine.

The named XML file contains the snapshot metadata. The snapshot `_name_`, `_description_`, and `_disks_` elements must be specified in the XML file.

// .Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

.Procedure

. Create an XML file with the virtual machine snapshot metadata.
+
The following is an example of a minimal XML file for creating a snapshot:
+
[source,xml]
----
<domainsnapshot>
  <name>Snapshot1</name>
  <description>First snapshot of the domain</description>
  <disks>
    <disk name='vda' snapshot='external'/>
  </disks>
</domainsnapshot>
----
// +
// For information about the XML format for snapshots, see link:https://libvirt.org/formatsnapshot.html[the libvirt upstream website].
+
There are several methods for creating the XML file:
+
* By using a text editor.
* By using the `virsh snapshot-dumpxml` command to output the metadata of the current snapshot to an XML file. You can edit the file, if necessary.
* By using the `virsh snapshot-create-as` command with the `print-xml` option to output the metadata of a specified snapshot to an XML file. You can edit the file, if necessary.

. Run the `virsh snapshot-create` command.
+
---
# `virsh snapshot-create _domain_ _xml-file_`
+
`Domain snapshot Snapshot1 created`
+
The virtual machine snapshot is created from the specified virtual machine by using the metadata in the specified XML file.


[role="_additional-resources"]
.Additional resources
* For information about the available options for the `virsh create-snapshot` command, see the relevant man page.
