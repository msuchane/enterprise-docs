// Module included in the following assemblies:
//
//assembly_managing-virtual-devices

:_mod-docs-content-type: PROCEDURE

[id="Attaching-a-watchdog-device-to-a-virtual-machine-using-the-web-console_{context}"]
= Attaching a watchdog device to a virtual machine by using the web console

To force the virtual machine (VM) to perform a specified action when it stops responding, you can attach virtual watchdog devices to a VM.

.Prerequisites

* You have installed the web console VM plug-in on your system. For more information, see
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[].
endif::virt-title[]

.Procedure

. In the command line interface, install the watchdog service.
+
[subs="+quotes"]
--
# *{PackageManagerCommand} install watchdog*
--

. Shut down the VM.

. Add the watchdog service to the VM.
+
[subs="+quotes"]
--
# *virt-xml _vmname_  --add-device --watchdog action=reset --update*
--

. Run the VM.

. Open the web console and in the menu:Virtual Machines[] interface of the web console, click on the VM to which you want to add the watchdog device.

. Click btn:[add] next to the *Watchdog* field in the Overview pane.
+
The *Add watchdog device type* dialog appears.

. Select the action that you want the watchdog device to perform if the VM stops responding.
+
image::virt-cockpit-add-watchdog.png[Image displaying the add watchdog device type dialog box., width=100%]

. Click btn:[Add].

.Verification

* The action you selected is visible next to the *Watchdog* field in the Overview pane.
