:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Creating and assigning filesystem-based storage for virtual machines using the CLI

// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/proc_creating-filesystem-based-storage-pools-using-the-cli.adoc[leveloffset=+1]

[id="creating-filesystem-based-storage-pools-using-the-cli_{context}"]
= Creating filesystem-based storage pools by using the CLI

[role="_abstract"]
When you want to create a storage pool on a file system that is not mounted, use the filesystem-based storage pool. This storage pool is based on a given file-system mountpoint. You can use the `virsh` utility to create filesystem-based storage pools.

.Prerequisites

* Ensure your hypervisor supports filesystem-based storage pools:
+
[subs="+quotes"]
----
# *virsh pool-capabilities | grep "'fs' supported='yes'"*
----
+
If the command displays any output, file-based pools are supported.

* Prepare a device on which you will base the storage pool. For this purpose, prefer partitions (for example, [filename]`/dev/sdb1`) or LVM volumes. If you provide a VM with write access to an entire disk or block device (for example, [filename]`/dev/sdb`), the VM will likely partition it or create its own LVM groups on it. This can result in system errors on the host.
+
However, if you require using an entire block device for the storage pool, {RH} recommends protecting any important partitions on the device from GRUB's `os-prober` function. To do so, edit the `/etc/default/grub` file and apply one of the following configurations:

** Disable `os-prober`.
+
[subs="+quotes"]
----
GRUB_DISABLE_OS_PROBER=true
----

** Prevent `os-prober` from discovering a specific partition. For example:
+
[subs="+quotes"]
----
GRUB_OS_PROBER_SKIP_LIST="5ef6313a-257c-4d43@/dev/sdb1"
----

.Procedure

. *Create a storage pool*
+
Use the [command]`virsh pool-define-as` command to define and create a filesystem-type storage pool. For example, to create a storage pool named `guest_images_fs` that uses the */dev/sdc1* partition, and is mounted on the /guest_images directory:
+
[subs="+quotes,attributes"]
----
# *virsh pool-define-as guest_images_fs fs --source-dev /dev/sdc1 --target /guest_images*
Pool guest_images_fs defined
----
+
If you already have an XML configuration of the storage pool you want to create, you can also define the pool based on the XML. For details, see xref:filesystem-based-storage-pool-parameters_assembly_parameters-for-creating-storage-pools[Filesystem-based storage pool parameters].

. *Define the storage pool target path*
+
Use the [command]`virsh pool-build` command to create a storage pool target path for a pre-formatted file-system storage pool, initialize the storage source device, and define the format of the data.
+
[subs="+quotes"]
----
# *virsh pool-build guest_images_fs*
  Pool guest_images_fs built

# *ls -la /guest_images*
  total 8
  drwx------.  2 root root 4096 May 31 19:38 .
  dr-xr-xr-x. 25 root root 4096 May 31 19:38 ..
----

. *Verify that the pool was created*
+
Use the [command]`virsh pool-list` command to verify that the pool was created.
+
[subs="+quotes"]
----
# *virsh pool-list --all*

  Name                 State      Autostart
  -----------------------------------------
  default              active     yes
  guest_images_fs      inactive   no
----

. *Start the storage pool*
+
Use the [command]`virsh pool-start` command to mount the storage pool.
+
[subs="+quotes"]
----
# *virsh pool-start guest_images_fs*
  Pool guest_images_fs started
----
+
[NOTE]
====
The [command]`virsh pool-start` command is only necessary for persistent storage pools. Transient storage pools are automatically started when they are created.
====

. *Optional:* Turn on autostart
+
By default, a storage pool defined with the [command]`virsh` command is not set to automatically start each time virtualization services start. Use the [command]`virsh pool-autostart` command to configure the storage pool to autostart.
+
[subs="+quotes"]
----
# *virsh pool-autostart guest_images_fs*
  Pool guest_images_fs marked as autostarted
----

.Verification

. Use the [command]`virsh pool-info` command to verify that the storage pool is in the `_running_` state. Check if the sizes reported are as expected and if autostart is configured correctly.
+
[subs="+quotes"]
----
# *virsh pool-info guest_images_fs*
  Name:           guest_images_fs
  UUID:           c7466869-e82a-a66c-2187-dc9d6f0877d0
  State:          running
  Persistent:     yes
  Autostart:      yes
  Capacity:       458.39 GB
  Allocation:     197.91 MB
  Available:      458.20 GB
----

. Verify there is a [filename]`lost+found` directory in the target path on the file system, indicating that the device is mounted.
+
[subs="+quotes"]
----
# *mount | grep /guest_images*
  /dev/sdc1 on /guest_images type ext4 (rw)

# *ls -la /guest_images*
  total 24
  drwxr-xr-x.  3 root root  4096 May 31 19:47 .
  dr-xr-xr-x. 25 root root  4096 May 31 19:38 ..
  drwx------.  2 root root 16384 May 31 14:18 lost+found
----
