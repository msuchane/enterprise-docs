:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Deleting storage for virtual machines using the CLI

// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/proc_deleting-storage-volumes-using-the-cli.adoc[leveloffset=+1]

[id="deleting-storage-volumes-using-the-cli_{context}"]
= Deleting storage volumes by using the CLI

To remove a storage volume from your host system, you must stop the pool and remove its XML definition.

.Prerequisites

* Any virtual machine that uses the storage volume you want to delete is shut down.

.Procedure

. Use the [command]`virsh vol-list` command to list the storage volumes in a specified storage pool.
+
[literal, subs="+quotes"]
--
# *virsh vol-list --pool RHEL-SP*
 Name                 Path
---------------------------------------------------------------
 .bash_history        /home/VirtualMachines/.bash_history
 .bash_logout         /home/VirtualMachines/.bash_logout
 .bash_profile        /home/VirtualMachines/.bash_profile
 .bashrc              /home/VirtualMachines/.bashrc
 .git-prompt.sh       /home/VirtualMachines/.git-prompt.sh
 .gitconfig           /home/VirtualMachines/.gitconfig
 vm-disk1             /home/VirtualMachines/vm-disk1
--

. *Optional*: Use the [command]`virsh vol-wipe` command to wipe a storage volume. For example, to wipe a storage volume named `vm-disk1` associated with the storage pool `RHEL-SP`:
+
[literal, subs="+quotes"]
--
# *virsh vol-wipe --pool RHEL-SP vm-disk1*
Vol vm-disk1 wiped
--

. Use the [command]`virsh vol-delete` command to delete a storage volume. For example, to delete a storage volume named `vm-disk1` associated with the storage pool `RHEL-SP`:
+
[literal, subs="+quotes"]
--
# *virsh vol-delete --pool RHEL-SP vm-disk1*
Vol vm-disk1 deleted
--

.Verification

* Use the [command]`virsh vol-list` command again to verify that the storage volume was deleted.
+
[literal, subs="+quotes"]
--
# *virsh vol-list --pool RHEL-SP*
 Name                 Path
---------------------------------------------------------------
 .bash_history        /home/VirtualMachines/.bash_history
 .bash_logout         /home/VirtualMachines/.bash_logout
 .bash_profile        /home/VirtualMachines/.bash_profile
 .bashrc              /home/VirtualMachines/.bashrc
 .git-prompt.sh       /home/VirtualMachines/.git-prompt.sh
 .gitconfig           /home/VirtualMachines/.gitconfig
--
