:_mod-docs-content-type: PROCEDURE

[id="reconfiguring-an-existing-nvdimm-namespace-to-file-system-dax-mode_{context}"]
= Reconfiguring an existing NVDIMM namespace to file system DAX mode

You can reconfigure an existing Non-Volatile Dual In-line Memory Modules (NVDIMM) namespace to file system DAX mode.

WARNING: Reconfiguring a namespace deletes previously stored data on the namespace.

.Prerequisites

* The `ndctl` utility is installed. For more information, see
ifdef::managing-storage-devices[]
xref:installing-ndctl_using-nvdimm-persistent-memory-storage[Installing ndctl].
endif::[]
ifndef::managing-storage-devices[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_storage_devices/using-nvdimm-persistent-memory-storage_managing-storage-devices#installing-ndctl_using-nvdimm-persistent-memory-storage[Installing ndctl].
endif::[]

.Procedure

. List all namespaces on your system:
+
[subs=+quotes]
....
# ndctl list --namespaces --idle
[
  {
    "dev":"namespace1.0",
    "mode":"raw",
    "size":34359738368,
    "uuid":"ac951312-b312-4e76-9f15-6e00c8f2e6f4"
    "state":"disabled",
    "numa_node":1
  },
  {
    "dev":"namespace0.0",
    "mode":"raw",
    "size":38615912448,
    "uuid":"ff5a0a16-3495-4ce8-b86b-f0e3bd9d1817",
    "state":"disabled",
    "numa_node":0
  }
]
....

. Reconfigure any namespace:
+
[subs=quotes]
....
# ndctl create-namespace --force --mode=fsdax --reconfig=_namespace-ID_
....
+
.Reconfiguring a namespace as file system DAX
====
To use `namespace0.0` for a file system that supports DAX, use the following command:
....
# ndctl create-namespace --force --mode=fsdax --reconfig=namespace0.0
{
  "dev":"namespace0.0",
  "mode":"fsdax",
  "map":"dev",
  "size":"11.81 GiB (12.68 GB)",
  "uuid":"f8153ee3-c52d-4c6e-bc1d-197f5be38483",
  "sector_size":512,
  "align":2097152,
  "blockdev":"pmem0"
}
....
====
+
The namespace is now available at the `/dev/pmem0` path.

.Verification

* Verify if the existing namespaces on your system is reconfigured:
+
....
# ndctl list --namespace namespace0.0
[
  {
    "dev":"namespace0.0",
    "mode":"fsdax",
    "map":"dev",
    "size":12681478144,
    "uuid":"f8153ee3-c52d-4c6e-bc1d-197f5be38483",
    "sector_size":512,
    "align":2097152,
    "blockdev":"pmem0"
  }
]
....

[role="_additional-resources"]
.Additional resources
* The `ndctl-create-namespace(1)` man page

