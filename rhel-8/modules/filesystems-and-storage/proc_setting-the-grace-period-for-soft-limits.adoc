:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_configuring-disk-quotas.adoc

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="setting-the-grace-period-for-soft-limits_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Setting the grace period for soft limits

[role="_abstract"]
If a given quota has soft limits, you can edit the grace period, which is the amount of time for which a soft limit can be exceeded.
You can set the grace period for users, groups, or projects.

.Procedure

* Edit the grace period:
+
....
# edquota -t
....

[IMPORTANT]
====
While other `edquota` commands operate on quotas for a particular user, group, or project, the `-t` option operates on every file system with quotas enabled.
====

[role="_additional-resources"]
.Additional resources
* `edquota(8)` man page.
