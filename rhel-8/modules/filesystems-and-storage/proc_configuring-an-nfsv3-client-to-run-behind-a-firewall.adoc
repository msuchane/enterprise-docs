
:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-an-nfsv3-client-to-run-behind-a-firewall_{context}"]
= Configuring an NFSv3 client to run behind a firewall

The procedure to configure an NFSv3 client to run behind a firewall is similar to the procedure to configure an NFSv3 server to run behind a firewall.

ifeval::[{ProductNumber} == 8]
If the machine you are configuring is both an NFS client and an NFS server, follow the procedure described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/deploying-an-nfs-server_deploying-different-types-of-servers#configuring-an-nfsv3-server-with-optional-nfsv4-support_deploying-an-nfs-server[Configuring an NFSv3 server with optional NFSv4 support].
endif::[]
ifeval::[{ProductNumber} >= 9]
If the machine you are configuring is both an NFS client and an NFS server, follow the procedure described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_using_network_file_services/deploying-an-nfs-server_configuring-and-using-network-file-services#configuring-an-nfsv3-server-with-optional-nfsv4-support_deploying-an-nfs-server[Configuring an NFSv3 server with optional NFSv4 support].
endif::[]

The following procedure describes how to configure a machine that is an NFS client only to run behind a firewall.

.Procedure

. To allow the NFS server to perform callbacks to the NFS client when the client is behind a firewall, add the `rpc-bind` service to the firewall by running the following command on the NFS client:
+
[literal,subs="+quotes,verbatim,normal"]
----
*firewall-cmd --permanent --add-service rpc-bind*
----

. Specify the ports to be used by the RPC service `nlockmgr` in the `/etc/nfs.conf` file as follows:
+
[literal,subs="+quotes,verbatim,normal"]
----
[lockd]

port=_port-number_
udp-port=_upd-port-number_
----
+
Alternatively, you can specify `nlm_tcpport` and `nlm_udpport` in the `/etc/modprobe.d/lockd.conf` file.

. Open the specified ports in the firewall by running the following commands on the NFS client:
+
[literal,subs="+quotes,verbatim,normal"]
----
*firewall-cmd --permanent --add-port=_<lockd-tcp-port>_/tcp*
*firewall-cmd --permanent --add-port=_<lockd-udp-port>_/udp*
----

. Add static ports for `rpc.statd` by editing the `[statd]` section of the `/etc/nfs.conf` file as follows:
+
[literal,subs="+quotes,verbatim,normal"]
----
[statd]

port=_port-number_
----

. Open the added ports in the firewall by running the following commands on the NFS client:
+
[literal,subs="+quotes,verbatim,normal"]
----
*firewall-cmd --permanent --add-port=_<statd-tcp-port>_/tcp*
*firewall-cmd --permanent --add-port=_<statd-udp-port>_/udp*
----

. Reload the firewall configuration:
+
[literal,subs="+quotes,verbatim,normal"]
----
*firewall-cmd --reload*
----

. Restart the `rpc-statd` service:
+
[literal,subs="+quotes,verbatim,normal"]
----
# *systemctl restart rpc-statd.service*
----
+
Alternatively, if you specified the `lockd` ports in the `/etc/modprobe.d/lockd.conf` file:
+
.. Update the current values of `/proc/sys/fs/nfs/nlm_tcpport` and `/proc/sys/fs/nfs/nlm_udpport`:
+
[literal,subs="+quotes,verbatim,normal"]
----
# *sysctl -w fs.nfs.nlm_tcpport=_<tcp-port>_*
# *sysctl -w fs.nfs.nlm_udpport=_<udp-port>_*
----

.. Restart the `rpc-statd` service:
+
[literal,subs="+quotes,verbatim,normal"]
----
# *systemctl restart rpc-statd.service*
----
