:_mod-docs-content-type: PROCEDURE
[id="enabling-online-block-discard_{context}"]
= Enabling online block discard

[role="_abstract"]
You can perform online block discard operations to automatically discard unused blocks on all supported file systems.

.Procedure

* Enable online discard at mount time:

** When mounting a file system manually, add the [option]`-o discard` mount option:
+
[subs=+quotes]
----
# mount -o discard [replaceable]_device_ [replaceable]_mount-point_
----

** When mounting a file system persistently, add the [option]`discard` option to the mount entry in the [filename]`/etc/fstab` file.

[role="_additional-resources"]
.Additional resources
* `mount(8)` man page.
* `fstab(5)` man page.
