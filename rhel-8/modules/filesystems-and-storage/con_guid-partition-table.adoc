:_mod-docs-content-type: CONCEPT

[id="guid-partition-table_{context}"]
= GUID partition table

The GUID partition table (GPT) is a partitioning scheme based on the Globally Unique Identifier (GUID).

GPT deals with the limitations of the Mater Boot Record (MBR) partition table. The MBR partition table cannot address storage larger than 2 TiB, equal to approximately 2.2 TB. Instead, GPT supports hard disks with larger capacity. The maximum addressable disk size is 8 ZiB, when using 512b sector drives, and 64 ZiB, when using 4096b sector drives. In addition, by default, GPT supports creation of up to 128 primary partitions. Extend the maximum amount of primary partitions by allocating more space to the partition table.

[NOTE]
====
A GPT has partition types based on GUIDs. Certain partitions require a specific GUID. For example, the system partition for Extensible Firmware Interface (EFI) boot loaders require GUID
`C12A7328-F81F-11D2-BA4B-00A0C93EC93B`.
====

GPT disks use logical block addressing (LBA) and a partition layout as follows:

* For backward compatibility with MBR disks, the system reserves the first sector (LBA 0) of GPT for MBR data, and applies the name "protective MBR".
* Primary GPT
** The header begins on the second logical block (LBA 1) of the device. The header contains the disk GUID, the location of the primary partition table, the location of the secondary GPT header, and CRC32 checksums of itself, and the primary partition table. It also specifies the number of partition entries on the table.
** By default, the primary GPT includes 128 partition entries. Each partition has an entry size of 128 bytes, a partition type GUID and a unique partition GUID.
* Secondary GPT
** For recovery, it is useful as a backup table in case the primary partition table is corrupted.
** The last logical sector of the disk contains the secondary GPT header and recovers GPT information, in case the primary header is corrupted.
** It contains:
*** The disk GUID
*** The location of the secondary partition table and the primary GPT header
*** CRC32 checksums of itself
*** The secondary partition table
*** The number of possible partition entries

.Disk with a GUID Partition Table
// image::gpt-partition.svg[]
image::gpt-partition.png[]

[IMPORTANT]
====
For a successful installation of the boot loader onto a GPT disk a BIOS boot partition must be present. Reuse is possible only if the disk already contains a BIOS boot partition. This includes disks initialized by the *Anaconda* installation program.
====
