:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/con_file-system-checking-and-repair.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.
:_module-type: CONCEPT
[id="file-system-checking-and-repair_{context}"]
= Scenarios that require a file system check

[role="_abstract"]
The relevant `fsck` tools can be used to check your system if any of the following occurs:

* System fails to boot
* Files on a specific disk become corrupt
* The file system shuts down or changes to read-only due to inconsistencies
* A file on the file system is inaccessible

File system inconsistencies can occur for various reasons, including but not limited to hardware errors, storage administration errors, and software bugs.

[IMPORTANT]
====
File system check tools cannot repair hardware problems. A file system must be fully readable and writable if repair is to operate successfully. If a file system was corrupted due to a hardware error, the file system must first be moved to a good disk, for example with the `dd(8)` utility.
====

For journaling file systems, all that is normally required at boot time is to replay the journal if required and this is usually a very short operation.

However, if a file system inconsistency or corruption occurs, even for journaling file systems, then the file system checker must be used to repair the file system.

[IMPORTANT]
====
It is possible to disable file system check at boot by setting the sixth field in `/etc/fstab` to `0`.  However, Red Hat does not recommend doing so unless you are having issues with `fsck` at boot time, for example with extremely large or remote file systems.
====

[role="_additional-resources"]
.Additional resources
* `fstab(5)` man page.
* `fsck(8)` man page.
* `dd(8)` man page.
