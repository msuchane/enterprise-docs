:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

[id="proc_defining-lvm-host-tags_{context}"]
= Defining LVM host tags
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
This procedure describes how to define LVM host tags in a cluster configuration. You can define host tags in the configuration files.

.Procedure

* Set `hosttags = 1` in the `tags` section to automatically define host tag using the machine's host name.
+
This allows you to use a common configuration file which can be replicated on all your machines so they hold identical copies of the file, but the behavior can differ between machines according to the host name.

For each host tag, an extra configuration file is read if it exists: `lvm_hosttag.conf`. If that file defines new tags, then further configuration files will be appended to the list of files to read in.

For example, the following entry in the configuration file always defines `tag1`, and defines `tag2` if the host name is `host1`:
----
tags { tag1 { }  tag2 { host_list = ["host1"] } }
----
