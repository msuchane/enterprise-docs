:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_snapshot-logical-volumes.adoc
// <List assemblies here, each on a new line>

[id="merging-snapshot-to-its-original-volume_{context}"]
= Merging snapshot to its original volume

Use the `lvconvert` command with the `--merge` option to merge a snapshot into its original (the origin) volume. You can perform a system rollback if you have lost data or files, or otherwise you have to restore your system to a previous state. After you merge the snapshot volume, the resulting logical volume has the origin volume’s name, minor number, and UUID. While the merge is in progress, reads or writes to the origin appear as they were directed to the snapshot being merged. When the merge finishes, the merged snapshot is removed.

If both the origin and snapshot volume are not open and active, the merge starts immediately. Otherwise, the merge starts after either the origin or snapshot are activated and both are closed. You can merge a snapshot into an origin that cannot be closed, for example a `root` file system, after the origin volume is activated.

.Procedure

. Merge the snapshot volume. The following command merges snapshot volume _vg001/snap_ into its _origin_:
+
[subs=quotes]
....
# lvconvert --merge _vg001/snap_
Merging of volume _vg001/snap_ started.
  _vg001/origin_: Merged: 100.00%
....

. View the origin volume:
+
[subs=quotes]
....
# lvs -a -o +devices
  LV      VG    Attr       LSize  Pool Origin Data% Meta% Move Log Cpy%Sync Convert Devices
  _origin  vg001_  owi-a-s---  1.00g                                                  /dev/sde1(0)
....


[role="_additional-resources"]
.Additional resources
* `lvconvert(8)` man page
