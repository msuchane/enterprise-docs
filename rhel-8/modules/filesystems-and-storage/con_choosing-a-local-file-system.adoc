:_mod-docs-content-type: CONCEPT
[id="choosing-a-local-file-system_{context}"]
= Choosing a local file system

[role="_abstract"]
To choose a file system that meets your application requirements, you need to understand the target system on which you are going to deploy the file system. You can use the following questions to inform your decision:

* Do you have a large server?
* Do you have large storage requirements or have a local, slow SATA drive?
* What kind of I/O workload do you expect your application to present?
* What are your throughput and latency requirements?
* How stable is your server and storage hardware?
* What is the typical size of your files and data set?
* If the system fails, how much downtime can you suffer?

If both your server and your storage device are large, XFS is the best choice. Even with smaller storage arrays, XFS performs very well when the average file sizes are large (for example, hundreds of megabytes in size).

If your existing workload has performed well with ext4, staying with ext4 should provide you and your applications with a very familiar environment.

The ext4 file system tends to perform better on systems that have limited I/O capability. It performs better on limited bandwidth (less than 200MB/s) and up to around 1000 IOPS capability. For anything with higher capability, XFS tends to be faster.

XFS consumes about twice the CPU-per-metadata operation compared to ext4, so if you have a CPU-bound workload with little concurrency, then ext4 will be faster. In general, ext4 is better if an application uses a single read/write thread and small files, while XFS shines when an application uses multiple read/write threads and bigger files.

You cannot shrink an XFS file system. If you need to be able to shrink the file system, consider using ext4, which supports offline shrinking.

In general, {RH} recommends that you use XFS unless you have a specific use case for ext4. You should also measure the performance of your specific application on your target server and storage system to make sure that you choose the appropriate type of file system.

.Summary of local file system recommendations
[options="header",cols="3,2"]
|====
| Scenario | Recommended file system
| No special use case | XFS
| Large server | XFS
| Large storage devices | XFS
| Large files | XFS
| Multi-threaded I/O | XFS
| Single-threaded I/O | ext4
| Limited I/O capability (under 1000 IOPS) | ext4
| Limited bandwidth (under 200MB/s) | ext4
| CPU-bound workload | ext4
| Support for offline shrinking | ext4
|====

// .Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
