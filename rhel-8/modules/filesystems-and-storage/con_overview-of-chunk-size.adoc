:_mod-docs-content-type: CONCEPT

[id="overview-of-chunk-size_{context}"]
= Overview of chunk size

A chunk is the largest unit of physical disk dedicated to snapshot storage.

Use the following criteria for using the chunk size:

** A smaller chunk size requires more metadata and hinders performance, but provides better space utilization with snapshots.
** A bigger chunk size requires less metadata manipulation, but makes the snapshot less space efficient. 

Be default, `lvm2` starts with a  64KiB chunk size and estimates good metadata size for such chunk size. The minimal metadata size `lvm2` can create and use is 2 MiB. If the metadata size needs to be larger than 128 MiB it begins to increase the chunk size, so the metadata size stays compact.
However, this may result in some big chunk size values, which are less space efficient for snapshot usage. In such cases, a smaller chunk size and bigger metadata size is a better option.

To specify the chunk size according to your requirement, use the `-c` or `--chunksize` parameter to overrule `lvm2` estimated chunk size. Be aware that you cannot change the chunk size once the thinpool is created.

If the volume data size is in the range of TiB, use ~15.8GiB as the metadata size, which is the maximum supported size, and set the chunk size according to your requirement. But, note that it is not possible to increase the metadata size if you need to extend the volume’s data size and have a small chunk size.

[NOTE]
====
Using the inappropriate combination of chunk size and metadata size may result in potentially problematic situation, when user runs out of space in
`metadata` or they may not further grow their thin-pool size because of limited maximum addressable thin-pool data size. 
====

[role="_additional-resources"]
.Additional resources
* `lvmthin(7)` man page 