:_mod-docs-content-type: PROCEDURE

[id="determining-device-mapper-entries-with-the-dmsetup-command_{context}"]
= Determining device mapper entries with the dmsetup command

You can use the `dmsetup` command to find out which device mapper entries match the multipathed devices.

.Procedure

* Display all the device mapper devices and their major and minor numbers. The minor numbers determine the name of the dm device. For example, a minor number of 3 corresponds to the multipathed device `/dev/dm-3`.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....

# *pass:quotes[`dmsetup ls`]*
mpathd  (253:4)
mpathep1        (253:12)
mpathfp1        (253:11)
mpathb  (253:3)
mpathgp1        (253:14)
mpathhp1        (253:13)
mpatha  (253:2)
mpathh  (253:9)
mpathg  (253:8)
VolGroup00-LogVol01     (253:1)
mpathf  (253:7)
VolGroup00-LogVol00     (253:0)
mpathe  (253:6)
mpathbp1        (253:10)
mpathd  (253:5)

....
