// Module included in the following assemblies:
//assembly_managing-lvm-volume-groups.adoc
:_mod-docs-content-type: PROCEDURE

[id="splitting-a-lvm-volume-group_{context}"]
= Splitting a LVM volume group

If there is enough unused space on the physical volumes, a new volume group can be created without adding new disks.

In the initial setup, the volume group _myvg_ consists of  _/dev/vdb1_, _/dev/vdb2_, and _/dev/vdb3_. After completing this procedure, the volume group _myvg_ will consist of _/dev/vdb1_ and _/dev/vdb2_, and the second volume group, _yourvg_, will consist of _/dev/vdb3_.

.Prerequisites

* You have sufficient space in the volume group. Use the `vgscan` command to determine how much free space is currently available in the volume group.
* Depending on the free capacity in the existing physical volume, move all the used physical extents to other physical volume using the `pvmove` command. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/managing-lvm-volume-groups_configuring-and-managing-logical-volumes#removing-physical-volumes-from-a-volume-group_managing-lvm-volume-groups[Removing physical volumes from a volume group].


.Procedure

. Split the existing volume group _myvg_ to the new volume group _yourvg_:
+
[subs=quotes]
....
# vgsplit _myvg yourvg /dev/vdb3_
  Volume group "_yourvg_" successfully split from "_myvg_"
....
+
[NOTE]
====
If you have created a logical volume using the existing volume group, use the following command to deactivate the logical volume:
[subs=quotes]
....
# lvchange -a n _/dev/myvg/mylv_
....
For more information about creating logical volumes, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/managing-lvm-logical-volumes_configuring-and-managing-logical-volumes[Managing LVM logical volumes].
====

. View the attributes of the two volume groups:
+
[subs=quotes]
....
# vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  _myvg_     2   1   0 wz--n- 34.30G 10.80G
  _yourvg_   1   0   0 wz--n- 17.15G 17.15G
....

.Verification

* Verify that the newly created volume group _yourvg_ consists of _/dev/vdb3_ physical volume:
+
[subs=quotes]
....
# pvs
  PV           VG      Fmt   Attr   PSize        PFree      Used
  _/dev/vdb1   myvg_   lvm2   a--    1020.00m    0          1020.00m
  _/dev/vdb2   myvg_   lvm2   a--    1020.00m    0          1020.00m
  _/dev/vdb3   yourvg_ lvm2   a--    1020.00m   1008.00m    12.00m
....

[role="_additional-resources"]
.Additional resources
* `vgsplit(8)`, `vgs(8)`, and `pvs(8)` man pages
