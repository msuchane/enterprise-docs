:_mod-docs-content-type: CONCEPT

[id="nvdimm-in-device-direct-access-mode_{context}"]
= NVDIMM in device direct access mode

Device direct access (device DAX, `devdax`) provides a means for applications to directly access storage, without the involvement of a file system. The benefit of device DAX is that it provides a guaranteed fault granularity, which can be configured using the `--align` option of the `ndctl` utility.

For the Intel 64 and AMD64 architecture, the following fault granularities are supported:

* 4 KiB
* 2 MiB
* 1 GiB 

Device DAX nodes support only the following system calls:

* `open()`
* `close()`
* `mmap()`

You can view the supported alignments of your NVDIMM device using the `ndctl list --human --capabilities` command. For example, to view it for the _region0_ device, use the `ndctl list --human --capabilities -r region0` command.

NOTE: The `read()` and `write()` system calls are not supported because the device DAX use case is tied to the SNIA Non-Volatile Memory Programming Model.