:_mod-docs-content-type: PROCEDURE

[id="creating-custom-devices-files_{context}"]
= Creating custom devices files

Logical Volume Manager (LVM) commands use the default `system.devices` file of the system. You can also create and use custom devices files by specifying the new file name in the LVM commands. Custom devices files are useful in cases when only certain applications need to use certain devices.

.Procedure

. Create a custom devices file in the `/etc/lvm/devices/` directory.

. Include the new devices file name in the LVM command:
+
[subs="+quotes"]
----
$ lvmdevices --devicesfile _<devices_file_name>_
----

. Optional: Display the new devices file to verify that the name of the new device is present:
+
[subs="+quotes"]
----
$ cat /etc/lvm/devices/<devices_file_name>
----

[role="_additional-resources"]
.Additional resources
* `lvmdevices(8)` man page
