:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//

[id='proc_moving-volume-group-to-another-system-{context}']
= Moving a volume group to another system

You can move an entire LVM volume group (VG) to another system using the following commands:

`vgexport`::
Use this command on an existing system to make an inactive VG inaccessible to the system. Once the VG is inaccessible, you can detach its physical volumes (PV).

`vgimport`::
Use this command on the other system to make the VG, which was inactive in the old system, accessible in the new system.

.Prerequisites

* No users are accessing files on the active volumes in the volume group that you are moving.

.Procedure

. Unmount the _mylv_ logical volume:
+
[subs=quotes]
----
# *umount /dev/mnt/_mylv_*
----
. Deactivate all logical volumes in the volume group, which prevents any further activity on the
volume group:
+
[subs=quotes]
----
# *vgchange -an _myvg_*
vgchange -- volume group "myvg" successfully deactivated
----
. Export the volume group to prevent it from being accessed by the system from which you are removing it.
+
[subs=quotes]
----
# *vgexport _myvg_*
vgexport -- volume group "myvg" successfully exported
----

. View the exported volume group:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....

# *pvscan*
  PV /dev/sda1    is in exported VG myvg [17.15 GB / 7.15 GB free]
  PV /dev/sdc1    is in exported VG myvg [17.15 GB / 15.15 GB free]
  PV /dev/sdd1   is in exported VG myvg [17.15 GB / 15.15 GB free]
  ...

....
+
. Shut down your system and unplug the disks that make up  the volume group and connect them to the new system.
+
. Plug the disks into the new system and import the volume group to make it accessible to the new system:
+
[subs=quotes]
----
# *vgimport _myvg_*
----
+
[NOTE]
====
You can use the `--force` argument of the `vgimport` command to import volume groups that are missing physical volumes and subsequently run the `vgreduce --removemissing` command.
====
+
. Activate the volume group:
+
[subs=quotes]
----
# *vgchange -ay _myvg_*
----
+
. Mount the file system to make it available for use:
+
[subs=quotes]
----
# *mkdir -p /mnt/_myvg_/users*
# *mount /dev/_myvg_/users /mnt/_myvg_/users*
----

.Additional resources
* `vgimport(8)`, `vgexport(8)`, and `vgchange(8)` man pages

