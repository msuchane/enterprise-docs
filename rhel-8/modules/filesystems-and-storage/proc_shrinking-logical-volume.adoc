// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_modifying-logical-volume-size.adoc

:_mod-docs-content-type: PROCEDURE
[id="reducing-a-logical-volume-and-file-system_{context}"]
= Reducing a logical volume and file system
[[shrinking-logical-volumes_modifying-the-size-of-a-logical-volume]]

You can reduce a logical volume and its file system by using the `lvreduce` command and the `resizefs` option. 

If the logical volume you are reducing contains a file system, to prevent data loss you must ensure that the file system is not using the space in the logical volume that is being reduced. For this reason, use the `--resizefs` option of the `lvreduce` command when the logical volume contains a file system.

When you use `--resizefs`, `lvreduce` attempts to reduce the file system before shrinking the logical volume. If shrinking the file system fails because it is full or does not support shrinking, then the `lvreduce` command fails and does not attempt to reduce the logical volume.


[WARNING]
====
In most cases, the `lvreduce` command warns about possible data loss and asks for confirmation. However, you should not rely on these confirmation prompts to prevent data loss because in some cases you will not see these prompts, such as when the logical volume is inactive or the `--resizefs` option is not used.

Note that using the `--test` option of the `lvreduce` command does not indicate if the operation is safe because this option does not check the file system or test the file system resize.
====

.Prerequisites
* File system of the logical volume supports shrinking. Determine the file system type and size using the `df -Th` command.
+
[NOTE]
====
For example, the GFS2 and XFS filesystems do not support shrinking.
====

* Underlying file system is not using the space in the LV that is being reduced.

.Procedure

. Shrink the _mylv_ logical volume and its filesystem in the _myvg_ volume group using one of the following options:

** Reduce the LV and its file system to a desired value:
+
[subs=quotes]
....
# *lvreduce --resizefs -L 500M _myvg/mylv_*
File system ext4 found on myvg/mylv.
File system size (2.00 GiB) is larger than the requested size (500.00 MiB).
File system reduce is required using resize2fs.
...
Logical volume myvg/mylv successfully resized.
....
+
** Reduce 64 megabytes from the logical volume and filesystem:
+
[subs=quotes]
....
# *lvreduce --resizefs -L -64M _myvg/mylv_*
File system ext4 found on myvg/mylv.
File system size (500.00 MiB) is larger than the requested size (436.00 MiB).
File system reduce is required using resize2fs.
...
Logical volume myvg/mylv successfully resized
....

[role="_additional-resources"]
.Additional resources
* `lvreduce(8)` man page
