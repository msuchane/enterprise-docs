:_mod-docs-content-type: PROCEDURE
[id="reclaiming-space-for-vdo-on-fibre-channel-or-ethernet-network_{context}"]
= Reclaiming space for VDO on Fibre Channel or Ethernet network

This procedure reclaims storage space on VDO volumes (or portions of volumes) that are provisioned to hosts on a Fibre Channel storage fabric or an Ethernet network using SCSI target frameworks such as LIO or SCST. 

// .Prerequisites
// 
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

.Procedure

// TODO: Please improve this procedure with actual guidance and explanation when there is time. This isn't very good.
// Signed: @msuchane

* SCSI initiators can use the `UNMAP` command to free space on thinly provisioned storage targets, but the SCSI target framework needs to be configured to advertise support for this command. This is typically done by enabling thin provisioning on these volumes.
+
Verify support for `UNMAP` on Linux-based SCSI initiators by running the following command:
+
[subs=+quotes]
----
# sg_vpd --page=0xb0 /dev/[replaceable]__device__
----
+
In the output, verify that the _Maximum unmap LBA count_ value is greater than zero.

// .Additional resources
// 
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
