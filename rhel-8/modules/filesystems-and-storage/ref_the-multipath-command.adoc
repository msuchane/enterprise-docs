:_mod-docs-content-type: REFERENCE

[id="the-multipath-command_{context}"]
= The multipath command

The `multipath` command is used to detect and combine multiple paths to devices. It provides a variety of options you can use to administer your multipathed devices.

The following table describes some options of the `multipath` command that you may find useful.

[[tb-table-multipath-options]]
.Useful `multipath` command options

[options="header"]
|===
|Option|Description
|[option]`-l`|Display the current multipath topology gathered
              from `sysfs` and the device mapper.
|[option]`-ll`|Display the current multipath topology
              gathered from `sysfs`, the device
              mapper, and all other available components on the system.
|[option]`-f _device_pass:attributes[{blank}]`|Remove the named multipath device.
|[option]`-F`|Remove all unused multipath
              devices.
|[option]`-w _device_pass:attributes[{blank}]`|Remove the `wwid` of the specified device from
              the `wwids` file.
|[option]`-W`|Reset the `wwids` file to include
              only the current multipath devices.
|[option]`-r`|Force reload of the multipath device.
|===
