:_newdoc-version: 2.15.1
:_template-generated: 2024-01-19
:_mod-docs-content-type: PROCEDURE

[id="configuring-an-nfsv3-server-with-optional-nfsv4-support_{context}"]
= Configuring an NFSv3 server with optional NFSv4 support

[role="_abstract"]
In a network which still uses NFSv3 clients, configure the server to provide shares by using the NFSv3 protocol. If you also have newer clients in your network, you can, additionally, enable NFSv4. By default, {ProductName} NFS clients use the latest NFS version that the server provides.

.Procedure

. Install the `nfs-utils` package:
+
[literal,subs="+quotes"]
----
# *dnf install nfs-utils*
----

. Optional: By default, NFSv3 and NFSv4 are enabled. If you do not require NFSv4 or only specific minor versions, uncomment all `vers4.<minor_version>` parameters and set them accordingly:
+
----
[nfsd]
# vers3=y
# vers4=y
vers4.0=n
vers4.1=n
vers4.2=y
----
+
With this configuration, the server provides only the NFS version 3 and 4.2.
+
[IMPORTANT]
====
If you require only a specific NFSv4 minor version, set only the parameters for the minor versions. Do not uncomment the `vers4` parameter to avoid an unpredictable activation or deactivation of minor versions.
By default, the `vers4` parameter enables or disables all NFSv4 minor versions. However, this behavior changes if you set `vers4` in conjunction with other `vers` parameters.
====

. By default, NFSv3 RPC services use random ports. To enable a firewall configuration, configure fixed port numbers in the `/etc/nfs.conf` file:

.. In the `[lockd]` section, set a fixed port number for the `nlockmgr` RPC service, for example:
+
[literal,subs="+quotes"]
----
[lockd]
port=_5555_
----
+
With this setting, the service automatically uses this port number for both the UDP and TCP protocol.

.. In the `[statd]` section, set a fixed port number for the `rpc.statd` service, for example:
+
[literal,subs="+quotes"]
----
[statd]
port=_6666_
----
+
With this setting, the service automatically uses this port number for both the UDP and TCP protocol.

. Optional: Create a directory that you want to share, for example:
+
[literal,subs="+quotes"]
----
# *mkdir -p _/nfs/projects/_*
----
+
If you want to share an existing directory, skip this step.

. Set the permissions you require on the `/nfs/projects/` directory:
+
[literal,subs="+quotes"]
----
# *chmod 2770 /nfs/projects/*
# *chgrp users /nfs/projects/*
----
+
These commands set write permissions for the `users` group on the `/nfs/projects/` directory and ensure that the same group is automatically set on new entries created in this directory.

. Add an export point to the `/etc/exports` file for each directory that you want to share:
+
----
/nfs/projects/     192.0.2.0/24(rw) 2001:db8::/32(rw)
----
+
This entry shares the `/nfs/projects/` directory to be accessible with read and write access to clients in the `192.0.2.0/24` and `2001:db8::/32` subnets.

. Open the relevant ports in `firewalld`:
+
[literal,subs="+quotes"]
----
# *firewall-cmd --permanent --add-service={nfs,rpc-bind,mountd}*
# *firewall-cmd --permanent --add-port={_5555_/tcp,_5555_/udp,_6666_/tcp,_6666_/udp}*
# *firewall-cmd --reload*
----

. Enable and start the NFS server:
+
[literal,subs="+quotes"]
----
# *systemctl enable --now rpc-statd nfs-server*
----

.Verification

* On the server, verify that the server provides only the NFS versions that you have configured:
+
[literal,subs="+quotes"]
----
# *cat /proc/fs/nfsd/versions*
+3 +4 -4.0 -4.1 +4.2
----

* On a client, perform the following steps:

. Install the `nfs-utils` package:
+
[literal,subs="+quotes"]
----
# *dnf install nfs-utils*
----

. Mount an exported NFS share:
+
[literal,subs="+quotes"]
----
# *mount -o vers=<version> _server.example.com:/nfs/projects/_ _/mnt/_*
----

. Verify that the share was mounted with the specified NFS version:
+
[literal,subs="+quotes"]
----
# *mount | grep "_/mnt_"*
server.example.com:/nfs/projects/ on /mnt type nfs (rw,relatime,*vers=3*,...
----

. As a user which is a member of the `users` group, create a file in `/mnt/`:
+
[literal,subs="+quotes"]
----
# *touch _/mnt/file_*
----

. List the directory to verify that the file was created:
+
[literal,subs="+quotes"]
----
# *ls -l _/mnt/_*
total 0
-rw-r--r--. 1 demo users 0 Jan 16 14:18 file
----

//[role="_additional-resources"]
//.Additional resources
//
//* The `/etc/exports` configuration file