:_mod-docs-content-type: PROCEDURE

[id="adding-exceptions-for-devices-with-disabled-multipathing_{context}"]
= Adding exceptions for devices with disabled multipathing

You can enable multipathing by adding exceptions on devices where multipathing is currently disabled.

.Prerequisites

* Multipathing is disabled on certain devices.

.Procedure

. Enable multipathing on the devices using the `blacklist_exceptions` section of the `/etc/multipath.conf` configuration file.
+
When specifying devices in the `blacklist_exceptions` section of the configuration file, you must specify the exceptions using the same criteria as they were specified in the `blacklist` section. For example, a WWID exception does not apply to devices disabled by a `devnode` entry, even if the disabled device is associated with that WWID. Similarly, `devnode` exceptions apply only to `devnode` entries, and `device` exceptions apply only to device entries.
+
.An exception by WWID
====

If you have a large number of devices and want to multipath only one of them with the WWID of `3600d0230000000000e13955cc3757803`, instead of individually disabling each of the devices except the one you want, you could disable all of them, and then enable only the one you want by adding the following lines to the `/etc/multipath.conf` file:

----
blacklist {
        wwid ".*"
}

blacklist_exceptions {
        wwid "3600d0230000000000e13955cc3757803"
}
----

Alternatively, you could use an exclamation mark (`!`) to invert the `blacklist` entry, which disables all devices except the specified WWID:

----
blacklist {
        wwid "!3600d0230000000000e13955cc3757803"
}
----

====
+
.An exception by udev property
====

The `property` parameter works differently than the other `blacklist_exception` parameters. The value of the `property` parameter must match the name of a variable in the `udev` database. Otherwise, the device is disabled. Using this parameter, you can disable multipathing on certain SCSI devices, such as USB sticks and local hard drives.

To enable multipathing only on SCSI devices that could reasonably be multipathed, set this parameter to `(SCSI_IDENT_|ID_WWN)` as in the following example:

----
blacklist_exceptions {
        property "(SCSI_IDENT_|ID_WWN)"
}
----

====

. Validate the `/etc/multipath.conf` file after modifying the multipath configuration file by running one of the following commands:

* To display any configuration errors, run:
+
[subs=+quotes]
----
# *multipath -t > /dev/null*
----

* To display the new configuration with the changes added, run:
+
[subs=+quotes]
----
# *multipath -t*
----

. Reload the `/etc/multipath.conf` file and reconfigure the `multipathd` daemon for changes to take effect:
+
[subs=+quotes]
----
# *service multipathd reload*
----

// .Verification

// . Start each step with an active verb.

// . Include one command or action per step.

// . Use an unnumbered bullet (*) if the procedure includes only one step.


// [role="_additional-resources"]
// .Additional resources
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * Currently, modules cannot include xrefs, so you cannot include links to other content in your collection. If you need to link to another assembly, add the xref to the assembly that includes this module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
