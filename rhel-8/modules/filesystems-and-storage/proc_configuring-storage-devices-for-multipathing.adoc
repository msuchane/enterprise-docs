:_mod-docs-content-type: PROCEDURE

[id="configuring-additional-storage-devices_{context}"]
= Configuring additional storage devices

By default, DM Multipath includes support for the most common storage arrays, which support DM Multipath.

.Procedure

* View the default configuration value, including supported devices:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....

# multipathd show config
# multipath -t

....

* Optional: To add an additional storage device that is not supported by default as a known multipath device, edit the `/etc/multipath.conf` file and insert the appropriate device information.
+
The following example ilustrates how to add information about the HP Open-V series. This sets the device to queue for a minute or 12 retries and 5 seconds per retry after all paths have failed.
+
[literal,subs="+quotes,attributes,verbatim"]
....

devices {
        device {
                vendor "HP"
                product "OPEN-V"
                no_path_retry 12
        }
}

....

// [role="_additional-resources"]
// .Additional resources
