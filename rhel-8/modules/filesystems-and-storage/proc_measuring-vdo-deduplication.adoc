:_mod-docs-content-type: PROCEDURE
[id="measuring-vdo-deduplication_{context}"]
= Measuring VDO deduplication

This procedure tests the efficiency of VDO data deduplication on a VDO test volume.

.Prerequisites

* A newly created VDO test volume is mounted. For details, see xref:creating-a-vdo-test-volume_{context}[].

.Procedure

. Prepare a table where you can record the test results:
+
[options="header"]
|====
| Statistic             | Bare file system | After seed | After 10 copies
| File system used size | | |
| VDO data used         | | |
| VDO logical used      | | |
|====

. Create 10 directories on the VDO volume to hold 10 copies of the test data set:
+
----
$ mkdir /mnt/vdo-test/vdo{01..10}
----

. Examine the disk usage reported by the file system:
+
----
$ df --human-readable /mnt/vdo-test
----
+
.Disk usage
====

----
Filesystem            Size  Used Avail Use% Mounted on
/dev/mapper/vdo-test  1.5T  198M  1.4T   1% /mnt/vdo-test
----

====

. Record the following values:
+
----
# vdostats --verbose | grep "blocks used"
----
+
.Used blocks
====

----
data blocks used                : 1090
overhead blocks used            : 538846
logical blocks used             : 6059434
----

====

** The `data blocks used` value is the number of blocks used by user data after optimization on the physical device running under VDO.
** The `logical blocks used` value is the number of blocks used before optimization. It will be used as the starting point for measurements.

. Create a data source file on the VDO volume:
+
----
$ dd if=/dev/urandom of=/mnt/vdo-test/sourcefile bs=4096 count=1048576

4294967296 bytes (4.3 GB) copied, 540.538 s, 7.9 MB/s
----

. Re-examine the amount of used physical disk space:
+
----
$ df --human-readable /mnt/vdo-test
----
+
.Disk usage with the data source file
====

----
Filesystem            Size  Used Avail Use% Mounted on
/dev/mapper/vdo-test  1.5T  4.2G  1.4T   1% /mnt/vdo-test
----

====
+
----
# vdostats --verbose | grep "blocks used"
----
+
.Used blocks with the data source file
====

----
data blocks used                : 1050093  # Increased by 4GiB
overhead blocks used            : 538846   # Did not significantly change
logical blocks used             : 7108036  # Increased by 4GiB
----

====
+
This command should show an increase in the number of blocks used, corresponding to the size of the written file.

. Copy the file to each of the 10 subdirectories:
+
----
$ for i in {01..10}; do
  cp /mnt/vdo-test/sourcefile /mnt/vdo-test/vdo$i
  done
----

. Re-examine the amount of used physical disk space:
+
----
$ df -h /mnt/vdo-test
----
+
.Disk usage after copying the file
====

----
Filesystem            Size  Used Avail Use% Mounted on
/dev/mapper/vdo-test  1.5T   45G  1.3T   4% /mnt/vdo-test
----

====
+
----
# vdostats --verbose | grep "blocks used"
----
+
.Used blocks after copying the file
====

----
data blocks used                : 1050836   # Increased by 3 MiB
overhead blocks used            : 538846
logical blocks used             : 17594127  # Increased by 41 GiB
----

====
+
The `data blocks used` value should be similar to the result of the earlier listing, with only a slight increase due to file system journaling and metadata.

. Subtract this new value of the space used by the file system from the value found before writing the test data. This is the amount of space consumed by this test from the perspective of the file system.

. Observe the space savings in your recorded statistics:
+
.Recorded values
====

[options="header"]
|====
| Statistic | Bare file system | After seed | After 10 copies
| File system used size | 198 MiB | 4.2 GiB | 45 GiB
| VDO data used | 4 MiB | 4.1 GiB | 4.1 GiB
| VDO logical used | 23.6 GiB _(file system overhead for 1.6 TiB formatted drive)_ | 27.8 GiB | 68.7 GiB
|====

NOTE: In the table, values have been converted to MiB or GiB. Blocks in the `vdostats` output are 4,096 B in size.

====

.Cleanup steps

* Remove the VDO test volume as described in xref:cleaning-up-the-vdo-test-volume_{context}[].


// .Additional resources
// 
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * Currently, modules cannot include xrefs, so you cannot include links to other content in your collection. If you need to link to another assembly, add the xref to the assembly that includes this module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
