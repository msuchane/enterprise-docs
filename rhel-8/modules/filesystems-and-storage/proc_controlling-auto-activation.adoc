:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_assembly_lvm-activation.adoc

[id='proc_controlling-auto-activation-{context}']
= Controlling autoactivation of logical volumes and volume groups

ifeval::[{ProductNumber} == 8]
Autoactivation of a logical volume refers to the event-based automatic activation of a logical volume during system startup. As devices become available on the system (device online events), `systemd/udev` runs the `lvm2-pvscan` service for each device. This service runs the `pvscan --cache -aay _device_` command, which reads the named device. If the device belongs to a volume group, the `pvscan` command will check if all of the physical volumes for that volume group are present on the system. If so, the command will activate logical volumes in that volume group.
endif::[]
ifeval::[{ProductNumber} == 9]
Autoactivation of a logical volume refers to the event-based automatic activation of a logical volume during system startup.
endif::[]

You can set the autoactivation property on a VG or LV. When the autoactivation property is disabled, the VG or LV will not be activated by a command doing autoactivation, such as `vgchange`, `lvchange`, or `pvscan` using `-aay` option. If autoactivation is disabled on a VG, no LVs will be autoactivated in that VG, and the autoactivation property has no effect. If autoactivation is enabled on a VG, autoactivation can be disabled for individual LVs.

.Procedure
* You can update the autoactivation settings in one of the following ways:

** Control autoactivation of a VG using the command line:
+
[subs=+quotes]
----
# vgchange --setautoactivation _<y|n>_
----
+
** Control autoactivation of a LV using the command line:
+
[subs=+quotes]
----
# lvchange --setautoactivation _<y|n>_
----

ifeval::[{ProductNumber} == 8]
** Control autoactivation of a LV in the `/etc/lvm/lvm.conf` configuration file using one of the following configuration options:

*** `global/event_activation`
+
When `event_activation` is disabled, `systemd/udev` will autoactivate logical volume only on whichever physical volumes are present during system startup. If all physical volumes have not appeared yet, then some logical volumes may not be autoactivated.

*** `activation/auto_activation_volume_list`
+
Setting `auto_activation_volume_list` to an empty list disables autoactivation entirely. Setting `auto_activation_volume_list` to specific logical volumes and volume groups limits autoactivation to those logical volumes.
endif::[]
ifeval::[{ProductNumber} == 9]
** You can use the `activation/auto_activation_volume_list` configuration option in the `/etc/lvm/lvm.conf` configuration file to control autoactivation of specific LVs and VGs:
+
[subs=+quotes]
`auto_activation_volume_list = [ "_<VG_name>_", "_<VG_name>/<LV_name>_", "_<@tag1>_", "_<...>_" ]`
+
If you set the `auto_activation_volume_list` to `[ ]` (empty list), autoactivation is disabled entirely.
endif::[]

[role="_additional-resources"]
.Additional resources
* `/etc/lvm/lvm.conf` configuration file
* `lvmautoactivation(7)` man page
