:_mod-docs-content-type: PROCEDURE
[id="activating-a-vdo-volume_{context}"]
= Activating a VDO volume

This procedure activates a VDO volume to enable it to start automatically.

// .Prerequisites
// 
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

.Procedure

* To activate a specific volume:
+
[subs=+quotes]
----
# vdo activate --name=[replaceable]__my-vdo__
----

* To activate all volumes:
+
----
# vdo activate --all
----

[role="_additional-resources"]
.Additional resources
* The `vdo(8)` man page

