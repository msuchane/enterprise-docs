:_mod-docs-content-type: CONCEPT
[id="files-and-directories-that-always-retain-write-permissions_{context}"]
= Files and directories that always retain write permissions

[role="_abstract"]
For the system to function properly, some files and directories need to retain write permissions. When the root file system is mounted in read-only mode, these files are mounted in RAM using the `tmpfs` temporary file system.

The default set of such files and directories is read from the [filename]`/etc/rwtab` file. Note that the `readonly-root` package is required to have this file present in your system.

[subs=+quotes]
----
dirs	/var/cache/man
dirs	/var/gdm
_<content truncated>_

empty	/tmp
empty	/var/cache/foomatic
_<content truncated>_

files	/etc/adjtime
files	/etc/ntp.conf
_<content truncated>_
----

Entries in the [filename]`/etc/rwtab` file follow this format:

[subs=+quotes]
----
[replaceable]_copy-method_    [replaceable]_path_
----

In this syntax:

* Replace [replaceable]_copy-method_ with one of the keywords specifying how the file or directory is copied to tmpfs.
* Replace [replaceable]_path_ with the path to the file or directory.

The [filename]`/etc/rwtab` file recognizes the following ways in which a file or directory can be copied to `tmpfs`:

`empty`:: An empty path is copied to `tmpfs`. For example:
+
----
empty /tmp
----

`dirs`:: A directory tree is copied to `tmpfs`, empty. For example:
+
----
dirs /var/run
----

`files`:: A file or a directory tree is copied to `tmpfs` intact. For example:
+
----
files /etc/resolv.conf
----

The same format applies when adding custom paths to [filename]`/etc/rwtab.d/`.

// .Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
