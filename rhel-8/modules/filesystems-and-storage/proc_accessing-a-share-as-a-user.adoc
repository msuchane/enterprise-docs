:_mod-docs-content-type: PROCEDURE
[id="proc_accessing-a-share-as-a-user_{context}"]
= Accessing a share as a user

[role="_abstract"]
If an SMB share is mounted with the `multiuser`  option, users can provide their credentials for the server to the kernel's keyring:

[literal,subs="+quotes,attributes"]
----
# cifscreds add -u __SMB_user_name__ _server_name_
Password: _password_
----

When the user performs operations in the directory that contains the mounted SMB share, the server applies the file system permissions for this user, instead of the one initially used when the share was mounted.

[NOTE]
====
Multiple users can perform operations using their own credentials on the mounted share at the same time.
====
