:experimental:
:_mod-docs-content-type: CONCEPT

[id="recommended-system-swap-space_{context}"]
= Recommended system swap space

[role="_abstract"]
The recommended size of a swap partition depends on the amount of RAM in your system and whether you want sufficient memory for your system to hibernate. The recommended swap partition size is set automatically during installation. To allow for hibernation, however, you need to edit the swap space in the custom partitioning stage.

The following recommendations are especially important on systems with low memory, such as 1 GB or less. Failure to allocate sufficient swap space on these systems can cause issues, such as instability or even render the installed system unbootable.

.Recommended swap space
[options="header"]
|===
|Amount of RAM in the system|Recommended swap space|Recommended swap space if allowing for hibernation
|&les;{nbsp}2 GB|2 times the amount of RAM|3 times the amount of RAM
|>{nbsp}2 GB &ndash; 8 GB|Equal to the amount of RAM|2 times the amount of RAM
|>{nbsp}8 GB &ndash; 64 GB|At least 4 GB|1.5 times the amount of RAM
|>{nbsp}64 GB|At least 4 GB|Hibernation not recommended
|===

For border values such as 2 GB, 8 GB, or 64 GB of system RAM, choose swap size based on your needs or preference. If your system resources allow for it, increasing the swap space can lead to better performance.

Note that distributing swap space over multiple storage devices also improves swap space performance, particularly on systems with fast drives, controllers, and interfaces.

[IMPORTANT]
====

File systems and LVM2 volumes assigned as swap space _should not_ be in use when being modified. Any attempts to modify swap fail if a system process or the kernel is using swap space. Use the `free` and `cat /proc/swaps` commands to verify how much and where swap is in use.

Resizing swap space requires temporarily removing it from the system. This can be problematic if running applications rely on the additional swap space and might run into low-memory situations. Preferably, perform swap resizing from rescue mode,
see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_an_advanced_rhel_{ProductNumber}_installation/index#debug-boot-options_kickstart-and-advanced-boot-options[Debug boot options] in the [citetitle]_Performing an advanced RHEL {ProductNumber} installation_. When prompted to mount the file system, select btn:[Skip].

====
