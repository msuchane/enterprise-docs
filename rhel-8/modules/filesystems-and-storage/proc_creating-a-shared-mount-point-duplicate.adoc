:_mod-docs-content-type: PROCEDURE
[id="creating-a-shared-mount-point-duplicate_{context}"]
= Creating a shared mount point duplicate

[role="_abstract"]
This procedure duplicates a mount point as a shared mount. File systems that you later mount under the original directory or the duplicate are always reflected in the other.

// .Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

.Procedure

. Create a virtual file system (VFS) node from the original mount point:
+
[subs=+quotes]
----
# mount --bind [replaceable]_original-dir_ [replaceable]_original-dir_
----

. Mark the original mount point as shared:
+
[subs=+quotes]
----
# mount --make-shared [replaceable]_original-dir_
----
+
Alternatively, to change the mount type for the selected mount point and all mount points under it, use the [option]`--make-rshared` option instead of [option]`--make-shared`.

. Create the duplicate:
+
[subs=+quotes]
----
# mount --bind [replaceable]_original-dir_ [replaceable]_duplicate-dir_
----

.Duplicating /media into /mnt as a shared mount point
====
To make the [filename]`/media` and [filename]`/mnt` directories share the same content:

. Create a VFS node from the [filename]`/media` directory:
+
[subs=+quotes]
----
# mount --bind /media /media
----

. Mark the [filename]`/media` directory as shared:
+
----
# mount --make-shared /media
----

. Create its duplicate in [filename]`/mnt`:
+
----
# mount --bind /media /mnt
----

. It is now possible to verify that a mount within [filename]`/media` also appears in [filename]`/mnt`. For example, if the CD-ROM drive contains non-empty media and the [filename]`/media/cdrom/` directory exists, use:
+
----
# mount /dev/cdrom /media/cdrom
# ls /media/cdrom
EFI  GPL  isolinux  LiveOS
# ls /mnt/cdrom
EFI  GPL  isolinux  LiveOS
----

. Similarly, it is possible to verify that any file system mounted in the [filename]`/mnt` directory is reflected in [filename]`/media`. For example, if a non-empty USB flash drive that uses the [filename]`/dev/sdc1` device is plugged in and the [filename]`/mnt/flashdisk/` directory is present, use:
+
----
# mount /dev/sdc1 /mnt/flashdisk
# ls /media/flashdisk
en-US  publican.cfg
# ls /mnt/flashdisk
en-US  publican.cfg
----

====

[role="_additional-resources"]
.Additional resources
* `mount(8)` man page
