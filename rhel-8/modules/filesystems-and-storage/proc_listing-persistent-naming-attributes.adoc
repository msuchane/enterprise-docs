:_mod-docs-content-type: PROCEDURE
[id="proc_listing-persistent-naming-attributes_{context}"]
= Listing persistent naming attributes

[role="_abstract"]
This procedure describes how to find out the persistent naming attributes of non-persistent storage devices.

// .Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

.Procedure

* To list the UUID and Label attributes, use the `lsblk` utility:
+
[subs=+quotes]
----
$ lsblk --fs [replaceable]_storage-device_
----
+
For example:
+
.Viewing the UUID and Label of a file system
====
[subs=+quotes]
----
$ lsblk --fs /dev/sda1

NAME FSTYPE *LABEL* *UUID*                                 MOUNTPOINT
sda1 xfs    *Boot*  *afa5d5e3-9050-48c3-acc1-bb30095f3dc4* /boot
----
====

* To list the PARTUUID attribute, use the `lsblk` utility with the [option]`--output +PARTUUID` option:
+
[subs=+quotes]
----
$ lsblk --output +PARTUUID
----
+
For example:
+
.Viewing the PARTUUID attribute of a partition
====
[subs=+quotes]
----
$ lsblk --output +PARTUUID /dev/sda1

NAME MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT *PARTUUID*
sda1   8:1    0  512M  0 part /boot      *4cd1448a-01*
----
====

* To list the WWID attribute, examine the targets of symbolic links in the [filename]`/dev/disk/by-id/` directory. For example:
+
.Viewing the WWID of all storage devices on the system
====
[subs=+quotes]
----
$ file /dev/disk/by-id/*

/dev/disk/by-id/ata-QEMU_HARDDISK_QM00001
symbolic link to ../../sda
/dev/disk/by-id/ata-QEMU_HARDDISK_QM00001-part1
symbolic link to ../../sda1
/dev/disk/by-id/ata-QEMU_HARDDISK_QM00001-part2
symbolic link to ../../sda2
/dev/disk/by-id/dm-name-rhel_rhel8-root
symbolic link to ../../dm-0
/dev/disk/by-id/dm-name-rhel_rhel8-swap
symbolic link to ../../dm-1
/dev/disk/by-id/dm-uuid-LVM-QIWtEHtXGobe5bewlIUDivKOz5ofkgFhP0RMFsNyySVihqEl2cWWbR7MjXJolD6g
symbolic link to ../../dm-1
/dev/disk/by-id/dm-uuid-LVM-QIWtEHtXGobe5bewlIUDivKOz5ofkgFhXqH2M45hD2H9nAf2qfWSrlRLhzfMyOKd
symbolic link to ../../dm-0
/dev/disk/by-id/lvm-pv-uuid-atlr2Y-vuMo-ueoH-CpMG-4JuH-AhEF-wu4QQm
symbolic link to ../../sda2
----

====

// .Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
