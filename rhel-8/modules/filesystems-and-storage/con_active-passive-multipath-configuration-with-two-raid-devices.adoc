:_mod-docs-content-type: CONCEPT

[id="active-passive-multipath-configuration-with-two-raid-devices_{context}"]
= Active/Passive multipath configuration with two RAID devices

[role="_abstract"]
In this configuration, there are two HBAs on the server, two SAN switches, and two RAID devices with two RAID controllers each. With DM Multipath configured, a failure at any of the points of the I/O path to either of the RAID devices causes DM Multipath to switch to the alternate I/O path for that device. The following image describes the configuration with two I/O paths to each RAID device. Here, there are two I/O paths to each RAID device.

[id="active-passive-two-raid_{context}"]
.Active/Passive multipath configuration with two RAID device
image::active-passive-multipath-config-with-two-raid-devices.png[Active/Passive multipath configuration with two RAID device]
