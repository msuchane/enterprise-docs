:_mod-docs-content-type: PROCEDURE
[id="persistently-mounting-a-stratis-file-system_{context}"]
= Persistently mounting a Stratis file system

[role="_abstract"]
This procedure persistently mounts a Stratis file system so that it is available automatically after booting the system.

.Prerequisites

* Stratis is installed. See
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/managing_file_systems/index#installing-stratis_setting-up-stratis-file-systems[Installing Stratis].

* The `stratisd` service is running.
* You have created a Stratis file system. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/managing_file_systems/index#creating-a-stratis-file-system_setting-up-stratis-file-systems[Creating a Stratis filesystem].

.Procedure

. Determine the UUID attribute of the file system:
+
[subs=+quotes]
----
$ lsblk --output=UUID /dev/stratis/[replaceable]_my-pool_/[replaceable]_my-fs_
----
+
For example:
+
.Viewing the UUID of Stratis file system
====
[subs=+quotes]
----
$ lsblk --output=UUID /dev/stratis/my-pool/fs1

UUID
a1f0b64a-4ebb-4d4e-9543-b1d79f600283
----
====

. If the mount point directory does not exist, create it:
+
[subs=+quotes]
----
# mkdir --parents [replaceable]_mount-point_
----

. As root, edit the [filename]`/etc/fstab` file and add a line for the file system, identified by the UUID. Use `xfs` as the file system type and add the `x-systemd.requires=stratisd.service` option.
+
For example:
+
.The /fs1 mount point in /etc/fstab
====
----
UUID=a1f0b64a-4ebb-4d4e-9543-b1d79f600283 /fs1 xfs defaults,x-systemd.requires=stratisd.service 0 0
----
====

. Regenerate mount units so that your system registers the new configuration:
+
----
# systemctl daemon-reload
----

. Try mounting the file system to verify that the configuration works:
+
[subs=+quotes]
----
# mount [replaceable]_mount-point_
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/assembly_persistently-mounting-file-systems_managing-file-systems[Persistently mounting file systems]
