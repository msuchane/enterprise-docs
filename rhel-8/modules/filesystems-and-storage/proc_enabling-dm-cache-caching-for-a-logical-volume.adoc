:_mod-docs-content-type: PROCEDURE
[id="enabling-dm-cache-caching-for-a-logical-volume_{context}"]
= Enabling dm-cache caching for a logical volume

This procedure enables caching of commonly used data on a logical volume using the `dm-cache` method.

// Using a cachevol, dm-cache metadata and data are placed next to each other on the same device, offering less control of placement.

.Prerequisites

* A slow logical volume that you want to speed up using `dm-cache` exists on your system.

* The volume group that contains the slow logical volume also contains an unused physical volume on a fast block device.

.Procedure

. Create a `cachevol` volume on the fast device:
+
[subs=+quotes]
----
# *lvcreate --size _cachevol-size_ --name _<fastvol>_ _<vg>_ _</dev/fast-pv>_*
----
+
Replace the following values:
+
`_cachevol-size_`::
The size of the `cachevol` volume, such as `5G`
`_fastvol_`::
A name for the `cachevol` volume
`_vg_`::
The volume group name
`_/dev/fast-pv_`::
The path to the fast block device, such as `/dev/sdf`
+
.Creating a `cachevol` volume
+
====
----
# lvcreate --size 5G --name fastvol vg /dev/sdf
Logical volume "fastvol" created.
----
====

. Attach the `cachevol` volume to the main logical volume to begin caching:
+
[subs=+quotes,options=nowrap]
----
# *lvconvert --type cache --cachevol _<fastvol>_ __<vg__/__main-lv>__*
----
+
Replace the following values:
+
`_fastvol_`::
The name of the `cachevol` volume
`_vg_`::
The volume group name
`__main-lv__`::
The name of the slow logical volume
+
.Attaching the `cachevol` volume to the main LV
====
----
# lvconvert --type cache --cachevol fastvol vg/main-lv
Erase all existing data on vg/fastvol? [y/n]: y
Logical volume vg/main-lv is now cached.
----
====


.Verification steps

* Verify if the newly created logical volume has `dm-cache` enabled:
+
[subs=+quotes,options=nowrap]
....
# lvs --all --options +devices [replaceable]_<vg>_

LV              Pool           Type   Devices
main-lv         [fastvol_cvol] cache  main-lv_corig(0)
[fastvol_cvol]                 linear /dev/fast-pv
[main-lv_corig]                linear /dev/slow-pv
....

[role="_additional-resources"]
.Additional resources
* `lvmcache(7)` man page
