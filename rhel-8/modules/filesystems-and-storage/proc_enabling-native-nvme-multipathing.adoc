:_mod-docs-content-type: PROCEDURE
[id="proc_enabling-native-nvme-multipathing_{context}"]
= Enabling native NVMe multipathing

ifeval::[{ProductNumber} == 8]
The default kernel setting for the `nvme_core.multipath` option is set to `N`, which means that the native Non-volatile Memory Express™ (NVMe™) multipathing is disabled. You can enable native NVMe multipathing using the native NVMe multipathing solution.
endif::[]
ifeval::[{ProductNumber} >= 9]
If native NVMe multipathing is disabled, you can enable it using the following solution.
endif::[]

.Prerequisites

* The NVMe devices are connected to your system. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-nvme-over-fabrics-using-nvme-rdma_managing-storage-devices#overview-of-nvme-over-fabric-devices_configuring-nvme-over-fabrics-using-nvme-rdma[Overview of NVMe over fabric devices].

.Procedure

. Check if native NVMe multipathing is enabled in the kernel:
+
[subs=+quotes]
----
# **cat /sys/module/nvme_core/parameters/multipath**
----
+
The command displays one of the following:
+
`N`:: Native NVMe multipathing is disabled.
`Y`:: Native NVMe multipathing is enabled.

. If native NVMe multipathing is disabled, enable it by using one of the following methods:

** Using a kernel option:

ifeval::[{ProductNumber} == 8]
.. Add the `nvme_core.multipath=Y` option to the command line:
+
[subs=+quotes]
----
# **grubby --update-kernel=ALL --args="nvme_core.multipath=Y"**
----
endif::[]

ifeval::[{ProductNumber} >= 9]
.. Remove the `nvme_core.multipath=N` option from the kernel command line:
+
[subs=+quotes]
----
# **grubby --update-kernel=ALL --remove-args="nvme_core.multipath=N"**
----
endif::[]

.. On the 64-bit IBM Z architecture, update the boot menu:
+
[subs=+quotes]
----
# **zipl**
----

.. Reboot the system.

** Using a kernel module configuration file:

ifeval::[{ProductNumber} == 8]
.. Create the `/etc/modprobe.d/nvme_core.conf` configuration file with the following content:
+
[subs=+quotes]
----
**options nvme_core multipath=Y**
----
endif::[]

ifeval::[{ProductNumber} >= 9]
.. Remove the `/etc/modprobe.d/nvme_core.conf` configuration file:
+
[subs=+quotes]
----
# **rm /etc/modprobe.d/nvme_core.conf**
----
endif::[]

.. Back up the `initramfs` file:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# **cp /boot/initramfs-$(uname -r).img /boot/initramfs-$(uname -r).bak.$(date +%m-%d-%H%M%S).img**
----

.. Rebuild the `initramfs`:
+
[subs=+quotes]
----
# **dracut --force --verbose**
----

.. Reboot the system.

. Optional: On the running system, change the I/O policy on NVMe devices to distribute the I/O on all available paths:
+
[subs=+quotes]
----
# **echo "round-robin" > /sys/class/nvme-subsystem/nvme-subsys0/iopolicy**
----

. Optional: Set the I/O policy persistently using `udev` rules. Create the `/etc/udev/rules.d/71-nvme-io-policy.rules` file with the following content:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
**ACTION=="add|change", SUBSYSTEM=="nvme-subsystem", ATTR{iopolicy}="round-robin"**
----


.Verification

. Verify if your system recognizes the NVMe devices. The following example assumes you have a connected NVMe over fabrics storage subsystem with two NVMe namespaces:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# **nvme list**

Node             SN                   Model                                    Namespace Usage                      Format           FW Rev
---------------- -------------------- ---------------------------------------- --------- -------------------------- ---------------- --------
/dev/nvme0n1     a34c4f3a0d6f5cec     Linux                                    1         250.06  GB / 250.06  GB    512   B +  0 B   4.18.0-2
/dev/nvme0n2     a34c4f3a0d6f5cec     Linux                                    2         250.06  GB / 250.06  GB    512   B +  0 B   4.18.0-2
----

. List all connected NVMe subsystems:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
----
# **nvme list-subsys**

nvme-subsys0 - NQN=testnqn
\
 +- nvme0 fc traddr=nn-0x20000090fadd597a:pn-0x10000090fadd597a host_traddr=nn-0x20000090fac7e1dd:pn-0x10000090fac7e1dd live
 +- nvme1 fc traddr=nn-0x20000090fadd5979:pn-0x10000090fadd5979 host_traddr=nn-0x20000090fac7e1dd:pn-0x10000090fac7e1dd live
 +- nvme2 fc traddr=nn-0x20000090fadd5979:pn-0x10000090fadd5979 host_traddr=nn-0x20000090fac7e1de:pn-0x10000090fac7e1de live
 +- nvme3 fc traddr=nn-0x20000090fadd597a:pn-0x10000090fadd597a host_traddr=nn-0x20000090fac7e1de:pn-0x10000090fac7e1de live
----
+
Check the active transport type. For example, `nvme0 fc` indicates that the device is connected over the Fibre Channel transport, and `nvme tcp` indicates that the device is connected over TCP.

. If you edited the kernel options, verify if native NVMe multipathing is enabled on the kernel command line:
+
[subs=+quotes]
----
# **cat /proc/cmdline**

BOOT_IMAGE=[...] nvme_core.multipath=Y
----

. If you changed the I/O policy, verify if `round-robin` is the active I/O policy on NVMe devices:
+
[subs=+quotes]
----
# **cat /sys/class/nvme-subsystem/nvme-subsys0/iopolicy**

round-robin
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_monitoring_and_updating_the_kernel/configuring-kernel-command-line-parameters_managing-monitoring-and-updating-the-kernel[Configuring kernel command-line parameters]
