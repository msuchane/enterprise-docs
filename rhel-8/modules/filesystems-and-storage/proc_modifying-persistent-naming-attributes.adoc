:_mod-docs-content-type: PROCEDURE
[id="proc_modifying-persistent-naming-attributes_{context}"]
= Modifying persistent naming attributes

[role="_abstract"]
This procedure describes how to change the UUID or Label persistent naming attribute of a file system.

NOTE: Changing `udev` attributes happens in the background and might take a long time. The [command]`udevadm settle` command waits until the change is fully registered, which ensures that your next command will be able to utilize the new attribute correctly.

In the following commands:

* Replace [replaceable]_new-uuid_ with the UUID you want to set; for example, `1cdfbc07-1c90-4984-b5ec-f61943f5ea50`. You can generate a UUID using the [command]`uuidgen` command.
* Replace [replaceable]_new-label_ with a label; for example, `backup_data`.

.Prerequisites

* If you are modifying the attributes of an XFS file system, unmount it first.

.Procedure

* To change the UUID or Label attributes of an *XFS* file system, use the `xfs_admin` utility:
+
[subs=+quotes]
----
# xfs_admin -U [replaceable]__new-uuid__ -L [replaceable]__new-label__ [replaceable]_storage-device_
# udevadm settle
----

* To change the UUID or Label attributes of an *ext4*, *ext3*, or *ext2* file system, use the `tune2fs` utility:
+
[subs=+quotes]
----
# tune2fs -U [replaceable]__new-uuid__ -L [replaceable]__new-label__ [replaceable]_storage-device_
# udevadm settle
----

* To change the UUID or Label attributes of a swap volume, use the `swaplabel` utility:
+
[subs=+quotes]
----
# swaplabel --uuid [replaceable]_new-uuid_ --label [replaceable]_new-label_ [replaceable]_swap-device_
# udevadm settle
----

// .Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
