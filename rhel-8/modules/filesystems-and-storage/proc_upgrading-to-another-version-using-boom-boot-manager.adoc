:_mod-docs-content-type: PROCEDURE

[id="upgrading-to-another-version-using-boom-boot-manager_{context}"]
= Upgrading to another version using Boom Boot Manager

Perform an upgrade of your {RHEL} operating system using the Boom boot manager.

.Prerequisites

* You are running a current version of {RHEL}.
ifeval::[{ProductNumber} == 8]
* You have installed the current version of the `boom-boot` package (version *boom-0.9*, ideally *boom-1.3-2.* or later).
endif::[]
ifeval::[{ProductNumber} == 9]
* You have installed the current version of the `boom-boot` package (version *boom-1.3-3.el9*, ideally *boom-1.4-4.el9* or later).
endif::[]
* You have sufficient space available for the snapshot. Make a size estimate based on the size of the original installation. List all the mounted logical volumes.
* You have installed the `leapp` package.
* You have enabled software repositories.
* You have activated the snapshot volume. If it is not active, the `boom` command fails.

[NOTE]
====
Additional boot entries might include */usr* or */var*.
====

.Procedure

. Create a snapshot of your _root_ logical volume:
* If your root file system uses thin provisioning, create a thin snapshot:
+
[subs=quotes]
----
# lvcreate -s rhel/root -kn -n _root_snapshot_before_changes_
----
+
Here:
+
**  `-s` creates the snapshot.
** `rhel/root` copies the file system to the logical volume.
** `-n` _root_snapshot_before_changes_ shows the name of the snapshot.
+
While creating a thin snapshot, do not define the snapshot size. The snapshot is allocated from the thin pool.

* If your root file system uses thick provisioning, create a thick snapshot:
+
[subs=quotes]
----
# lvcreate -s  rhel/root -n _root_snapshot_before_changes_ _-L_ _25g_
----
+
Here:

** `-s` creates the snapshot.
** `rhel/root` copies the file system to the logical volume.
** `-n` _root_snapshot_before_changes_ shows the name of the snapshot.
** `-L` _25g_ is the snapshot size. Make a size estimate based on the size of the original installation.
+
While creating a thick snapshot, define the snapshot size that can hold all the changes during the upgrade.
+
[IMPORTANT]
====
The created snapshot does not include any additional system changes.
====

. Create the profile:
+
ifeval::[{ProductNumber} == 8]
[subs=+quotes]
----
# boom profile create --from-host --uname-pattern el8
----
endif::[]

ifeval::[{ProductNumber} == 9]
[subs=+quotes]
----
# boom profile create --from-host --uname-pattern el9
----
endif::[]

. Create a snapshot boot entry of the original system using backup copies of the original boot images:
+
[subs=quotes]
----
# boom create --backup --title _"Root LV snapshot before changes"_ --rootlv rhel/_root_snapshot_before_changes_
----
+
Here:

* `--title` *__Root LV snapshot before changes__* is the name of the boot entry, that shows in the boot entry list during system startup.
* `--rootlv` is the root logical volume that corresponds to the new boot entry.
* After you complete the previous step, you have a boot entry that enables access to the original system, before the upgrade.

. Upgrade to {RHEL}
ifeval::[{ProductNumber} == 8]
{ProductNumber}
endif::[]
ifeval::[{ProductNumber} == 9]
{ProductNumber}
endif::[]
using the Leapp utility:
+
[subs=quotes]
----
# leapp upgrade
----
+
* Review and resolve any indicated blockers from the `leapp upgrade` command report.

. Reboot into the upgraded boot entry:
+
[subs=quotes]
----
# leapp upgrade --reboot
----
+
* Select the *{RHEL} Upgrade Initramfs* entry from the GRUB boot screen.

* The `leapp` utility creates the upgrade boot entry. Run the above mentioned command to reboot into the upgrade boot entry, and proceed to execute the in-place upgrade to {RHEL} {ProductNumberLink}. After the upgrade process, the reboot argument initiates an automatic system restart. The GRUB screen shows during reboot.
+
[NOTE]
====
The Snapshots submenu from the GRUB boot screen is not available in {RHEL} {ProductNumberLink}.
====

.Verification steps

* Continue the upgrade and install the new {RHEL} {ProductNumberLink} RPM packages. After completing the upgrade, the system automatically reboots. The GRUB screen shows the upgraded and the older version of the available operating system. The upgraded system version is the default selection.

* Check if the *Root LV snapshot before changes* boot entry is in the GRUB menu. If present, it provides instant access to the operating system state, prior to the upgrade.

[role="_additional-resources"]
.Additional resources
* `boom(1)` man page
* link:https://access.redhat.com/solutions/3750001[What is BOOM and how to install it?]
* link:https://access.redhat.com/solutions/3772101[How to create a BOOM boot entry]
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/articles/3664871[Data required by the Leapp utility for an in-place upgrade from RHEL 7 to RHEL 8]
endif::[]
