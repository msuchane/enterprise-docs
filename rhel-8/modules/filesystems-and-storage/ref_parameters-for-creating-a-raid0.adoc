:_mod-docs-content-type: REFERENCE

[id="parameters-for-creating-a-raid0_{context}"]
= Parameters for creating a RAID0

You can create a RAID0 striped logical volume using the `lvcreate --type raid0[_meta] --stripes _Stripes_ --stripesize _StripeSize_ _VolumeGroup [PhysicalVolumePath_]` command.

The following table describes different parameters, which you can use while creating a RAID0 striped logical volume.

.Parameters for creating a RAID0 striped logical volume

[options="header"cols="1,2a"]
|===
|Parameter|Description

|`--type raid0[_meta]`|Specifying `raid0` creates a RAID0 volume without metadata volumes. Specifying `raid0_meta` creates a RAID0 volume with metadata volumes. Since RAID0 is non-resilient, it does not store any mirrored data blocks as RAID1/10 or calculate and store any parity blocks as RAID4/5/6 do. Hence, it does not need metadata volumes to keep state about resynchronization progress of mirrored or parity blocks. Metadata volumes become mandatory on a conversion from RAID0 to RAID4/5/6/10. Specifying `raid0_meta` preallocates those metadata volumes to prevent a respective allocation failure.

|`--stripes _Stripes_`|Specifies the number of devices to spread the logical volume across.

|`--stripesize _StripeSize_`|Specifies the size of each stripe in kilobytes.  This is the amount of data that is written to one device before moving to the next device.

|`_VolumeGroup_`|Specifies the volume group to use.

|`_PhysicalVolumePath_`|Specifies the devices to use. If this is not specified, LVM will choose the number of devices specified by the _Stripes_ option, one for each stripe.
|===


