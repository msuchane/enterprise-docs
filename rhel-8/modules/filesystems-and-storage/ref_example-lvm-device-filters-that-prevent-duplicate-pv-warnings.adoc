:_mod-docs-content-type: REFERENCE
[id="example-lvm-device-filters-that-prevent-duplicate-pv-warnings_{context}"]
= Example LVM device filters that prevent duplicate PV warnings

The following examples show LVM device filters that avoid the duplicate physical volume warnings that are caused by multiple storage paths to a single logical unit (LUN).

You can configure the filter for logical volume manager (LVM) to check metadata for all devices. Metadata includes local hard disk drive with the root volume group on it and any multipath devices. By rejecting the underlying paths to a multipath device (such as `/dev/sdb`, `/dev/sdd`), you can avoid these duplicate PV warnings, because LVM finds each unique metadata area once on the multipath device itself.

* To accept the second partition on the first hard disk drive and any device mapper (DM) Multipath devices and reject everything else, enter:
+
----
filter = [ "a|/dev/sda2$|", "a|/dev/mapper/mpath.*|", "r|.*|" ]
----

* To accept all HP SmartArray controllers and any EMC PowerPath devices, enter:
+
----
filter = [ "a|/dev/cciss/.*|", "a|/dev/emcpower.*|", "r|.*|" ]
----

* To accept any partitions on the first IDE drive and any multipath devices, enter:
+
----
filter = [ "a|/dev/hda.*|", "a|/dev/mapper/mpath.*|", "r|.*|" ]
----

[role="_additional-resources"]
.Additional resources

ifdef::lvm-guide[]
* xref:examples-of-lvm-device-filter-configurations_the-lvm-device-filter[Examples of LVM device filter configurations]
endif::[]

