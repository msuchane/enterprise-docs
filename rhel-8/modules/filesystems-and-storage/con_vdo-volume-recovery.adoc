:_mod-docs-content-type: CONCEPT
[id="vdo-volume-recovery_{context}"]
= VDO volume recovery

When a VDO volume restarts after an unclean shutdown, VDO performs the following actions:

* Verifies the consistency of the metadata on the volume.
* Rebuilds a portion of the metadata to repair it if necessary.

Rebuilds are automatic and do not require user intervention.

VDO might rebuild different writes depending on the active write mode:

`sync`::
If VDO was running on synchronous storage and write policy was set to `sync`, all data written to the volume are fully recovered.

`async`::
If the write policy was `async`, some writes might not be recovered if they were not made durable. This is done by sending VDO a `FLUSH` command or a write I/O tagged with the FUA (force unit access) flag. You can accomplish this from user mode by invoking a data integrity operation like `fsync`, `fdatasync`, `sync`, or `umount`.

In either mode, some writes that were either unacknowledged or not followed by a flush might also be rebuilt.

[discrete]
== Automatic and manual recovery

When a VDO volume enters `recovering` operating mode, VDO automatically rebuilds the unclean VDO volume after the it comes back online. This is called _online recovery_.

If VDO cannot recover a VDO volume successfully, it places the volume in `read-only` operating mode that persists across volume restarts. You need to fix the problem manually by forcing a rebuild.

[role="_additional-resources"]
.Additional resources
* For more information about automatic and manual recovery and VDO operating modes, see xref:vdo-operating-modes_{context}[].

