:_mod-docs-content-type: PROCEDURE

[id="enabling-discard-mount-option-on-vdo_{context}"]
= Enabling discard mount option on VDO

[role="_abstract"]
This procedure enables the `discard` option on your VDO volume.

.Prerequisites

* An LVM-VDO volume exists on your system.

.Procedure

* Enable the `discard` on your volume:
+
[subs=+quotes]
----
# mount -o discard /dev/_vg-name/vdo-name_ _mount-point_
----


[role="_additional-resources"]
.Additional resources
* `xfs(5)`, `mount(8)`, and `lvmvdo(7)` man pages
