:_mod-docs-content-type: PROCEDURE
[id="cleaning-up-the-vdo-performance-testing-volume_{context}"]
= Cleaning up the VDO performance testing volume

This procedure removes the VDO volume used for testing VDO performance from the system.

.Prerequisites

* A VDO test volume exists on the system.

.Procedure

* Remove the VDO test volume from the system:
+
----
# vdo remove --name=vdo-test
----

.Verification steps

* Verify that the volume has been removed:
+
----
# vdo list --all | grep vdo-test
----
+
The command should not list the VDO test partition.

// .Additional resources
// 
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * Currently, modules cannot include xrefs, so you cannot include links to other content in your collection. If you need to link to another assembly, add the xref to the assembly that includes this module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
