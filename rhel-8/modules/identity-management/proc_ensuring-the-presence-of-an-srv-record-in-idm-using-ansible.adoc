:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-an-srv-record-in-idm-using-ansible_{context}"]
= Ensuring the presence of an SRV record in IdM using Ansible

[role="_abstract"]
A DNS service (SRV) record defines the hostname, port number, transport protocol, priority and weight of a service available in a domain. In {IPA} (IdM), you can use SRV records to locate IdM servers and replicas.

Follow this procedure to use an Ansible playbook to ensure that an SRV record is present in IdM DNS. In the example used in the procedure below, an IdM administrator ensures the presence of the *_kerberos._udp.idm.example.com* SRV record with the value of *10 50 88 idm.example.com*. This sets the following values:

* It sets the priority of the service to 10.
* It sets the weight of the service to 50.
* It sets the port to be used by the service to 88.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
* You know the IdM administrator password.
* The *idm.example.com* zone exists and is managed by IdM DNS. For more information about adding a primary DNS zone in IdM DNS, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/working_with_dns_in_identity_management/using-ansible-playbooks-to-manage-idm-dns-zones_working-with-dns-in-identity-management[Using Ansible playbooks to manage IdM DNS zones].

.Procedure

. Navigate to the `/usr/share/doc/ansible-freeipa/playbooks/dnsrecord` directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd /usr/share/doc/ansible-freeipa/playbooks/dnsrecord*
....


. Open your inventory file and ensure that the IdM server that you want to configure is listed in the `[ipaserver]` section. For example, to instruct Ansible to configure *server.idm.example.com*, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....


. Make a copy of the *ensure-SRV-record-is-present.yml* Ansible playbook file. For example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp ensure-SRV-record-is-present.yml ensure-SRV-record-is-present-copy.yml*
....

. Open the *ensure-SRV-record-is-present-copy.yml* file for editing.

. Adapt the file by setting the following variables in the `ipadnsrecord` task section:

* Set the `ipaadmin_password` variable to your IdM administrator password.
* Set the `name` variable to *_kerberos._udp.idm.example.com*.
* Set the `srv_rec` variable to *'10 50 88 idm.example.com'*.
* Set the `zone_name` variable to *idm.example.com*.

+
This the modified Ansible playbook file for the current example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Test multiple DNS Records are present.
  hosts: ipaserver
  become: true
  gather_facts: false

  tasks:
  # Ensure a SRV record is present
  - ipadnsrecord:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: _kerberos._udp.idm.example.com
      srv_rec: ’10 50 88 idm.example.com’
      zone_name: idm.example.com
      state: present
....

. Save the file.

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory.file ensure-SRV-record-is-present.yml*
....

[role="_additional-resources"]
.Additional resources
* See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/working_with_dns_in_identity_management/managing-dns-records-in-idm_working-with-dns-in-identity-management#dns-records-in-idm_managing-dns-records-in-idm[DNS records in IdM].
* See the `README-dnsrecord.md` file in the `/usr/share/doc/ansible-freeipa/` directory.
* See sample Ansible playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/dnsrecord` directory.
