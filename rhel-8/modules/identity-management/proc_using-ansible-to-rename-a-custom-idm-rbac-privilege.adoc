:_mod-docs-content-type: PROCEDURE
[id="using-ansible-to-rename-a-custom-idm-rbac-privilege_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using Ansible to rename a custom IdM RBAC privilege
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
As a system administrator of {IPA} (IdM), you can customize the IdM role-based access control.

The following procedure describes how to rename a privilege because, for example, you have removed a few permissions from it. As a result, the name of the privilege is no longer accurate. In the example, the administrator renames a *full_host_administration* privilege to *limited_host_administration*.

.Prerequisites

* You know the IdM administrator password.
* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

* The *__full_host_administration__* privilege exists. For more information about how to add a privilege, see xref:using-ansible-to-ensure-a-custom-idm-rbac-privilege-is-present_{context}[Using Ansible to ensure a custom IdM RBAC privilege is present].

.Procedure


. Navigate to the *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Make a copy of the `privilege-present.yml` file located in the `/usr/share/doc/ansible-freeipa/playbooks/privilege/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/privilege/privilege-present.yml rename-privilege.yml*
....

. Open the `rename-privilege.yml` Ansible playbook file for editing.

. Adapt the file by setting the following variables in the `ipaprivilege` task section:

* Set the `ipaadmin_password` variable to the password of the IdM administrator.
* Set the `name` variable to the current name of the privilege.
* Add the `rename` variable and set it to the new name of the privilege.
* Add the `state` variable and set it to `renamed`.

. Rename the playbook itself, for example:
+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- *name: Rename a privilege*
  hosts: ipaserver
....

. Rename the task in the playbook, for example:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[...]
tasks:
- *name: Ensure the full_host_administration privilege is renamed to limited_host_administration*
  ipaprivilege:
  [...]
....

+
--
This is the modified Ansible playbook file for the current example:

[literal,subs="+quotes,attributes,verbatim"]
....
---
- *name: Rename a privilege*
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - *name: Ensure the full_host_administration privilege is renamed to limited_host_administration*
    ipaprivilege:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: full_host_administration*
      *rename: limited_host_administration*
      *state: renamed*
....
--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory rename-privilege.yml*
....
