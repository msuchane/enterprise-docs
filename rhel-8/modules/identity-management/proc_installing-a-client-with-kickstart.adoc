:_mod-docs-content-type: PROCEDURE
[id='installing-a-client-with-kickstart_{context}']
= Installing a client with Kickstart

[role="_abstract"]
Follow this procedure to use a Kickstart file to install an {IPA} (IdM) client.

.Prerequisites

* Do not start the `sshd` service prior to the kickstart enrollment. Starting `sshd` before enrolling the client generates the SSH keys automatically, but the Kickstart file in xref:kickstart-file-for-client-installation_{context}[] uses a script for the same purpose, which is the preferred solution.

.Procedure

. Pre-create the host entry on the IdM server, and set a temporary password for the entry:
+
[literal,subs="+quotes,attributes"]
....
$ *ipa host-add _client.example.com_ --password=_secret_*
....
+
The password is used by Kickstart to authenticate during the client installation and expires after the first authentication attempt. After the client is successfully installed, it authenticates using its keytab.

. Create a Kickstart file with the contents described in xref:kickstart-file-for-client-installation_{context}[]. Make sure that network is configured properly in the Kickstart file using the `network` command.
//For details, see the link:++https://pykickstart.readthedocs.io/en/latest/kickstart-docs.html#network[Kickstart documentation].

. Use the Kickstart file to install the IdM client.
//For details, see link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Installation_Guide/sect-kickstart-howto.html++[How Do You Perform a Kickstart Installation?] in the [citetitle]_Installation Guide_.
