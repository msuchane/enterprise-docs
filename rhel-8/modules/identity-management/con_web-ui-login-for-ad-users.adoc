:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// logging-in-to-ipa-in-the-web-ui-using-a-password.adoc
// logging-in-to-ipa-in-the-web-ui-using-a-kerberos-ticket.adoc

[id='web-ui-login-for-ad-users-{context}']
= Web UI login for Active Directory users

[role="_abstract"]
To enable Web UI login for {AD} users, define an ID override for each {AD} user in the *Default Trust View*. For example:

[literal,subs="+quotes,attributes,verbatim"]
....
[admin@server ~]$ *ipa idoverrideuser-add 'Default Trust View' _ad_user_@_ad.example.com_*
....



[role="_additional-resources"]
.Additional resources
ifdef::c-m-idm[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/assembly_using-id-views-for-active-directory-users_configuring-and-managing-idm[Using ID views for Active Directory users]
endif::[]
ifdef::managing-users-groups-hosts[]
* xref:assembly_using-id-views-for-active-directory-users_managing-users-groups-hosts[Using ID views for Active Directory users]
endif::[]
ifdef::accessing-idm-services[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_idm_users_groups_hosts_and_access_control_rules/assembly_using-id-views-for-active-directory-users_managing-users-groups-hosts[Using ID views for Active Directory users]
endif::[]
