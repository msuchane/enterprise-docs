[id="removing-a-certificate-issued-by-an-external-ca-from-an-idm-user-host-or-service-account-by-using-the-idm-web-ui_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Removing a certificate issued by an external CA from an IdM user, host, or service account by using the IdM Web UI
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

As an {IPA} (IdM) administrator, you can remove an externally signed certificate from the account of an IdM user, host, or service by using the {IPA} (IdM) Web UI.


.Prerequisites

* You are logged in to the {IPA} (IdM) Web UI as an administrative user.

.Procedure

. Open the `Identity` tab, and select the `Users`, `Hosts`, or `Services` subtab.

. Click the name of the user, host, or service to open its configuration page.

. Click the btn:[Actions] next to the certificate to delete, and select btn:[Delete].

. Click btn:[Save] to store the changes.
