:_mod-docs-content-type: PROCEDURE
// Module included in the following titles/assemblies:
//
// assembly_managing-dns-forwarding-in-idm.adoc

[id="ensuring-the-presence-of-a-dns-forward-zone-in-idm-using-ansible_{context}"]
= Ensuring the presence of a DNS Forward Zone in IdM using Ansible

[role="_abstract"]
Follow this procedure to use an Ansible playbook to ensure the presence of a DNS Forward Zone in IdM. In the example procedure below, the IdM administrator ensures the presence of a DNS forward zone for `example.com` to a DNS server with an Internet Protocol (IP) address of `8.8.8.8`.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
* You know the IdM administrator password.

.Procedure

. Navigate to the `/usr/share/doc/ansible-freeipa/playbooks/dnsconfig` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ cd /usr/share/doc/ansible-freeipa/playbooks/dnsconfig
....

. Open your inventory file and make sure that the IdM server that you want to configure is listed in the `[ipaserver]` section. For example, to instruct Ansible to configure `server.idm.example.com`, enter:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....

. Make a copy of the `forwarders-absent.yml` Ansible playbook file. For example:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ cp forwarders-absent.yml ensure-presence-forwardzone.yml
....

. Open the `ensure-presence-forwardzone.yml` file for editing.

. Adapt the file by setting the following variables:
.. Change the `name` variable for the playbook to `Playbook to ensure the presence of a dnsforwardzone in IdM DNS`.
.. In the `tasks` section, change the `name` of the task to `Ensure presence of a dnsforwardzone for example.com to 8.8.8.8`.
.. In the `tasks` section, change the `ipadnsconfig` heading to `ipadnsforwardzone`.
.. In the `ipadnsforwardzone` section:
... Add the `ipaadmin_password` variable and set it to your IdM administrator password.
... Add the `name` variable and set it to `example.com`.
... In the `forwarders` section:
.... Remove the `ip_address` and `port` lines.
.... Add the IP address of the DNS server to receive forwarded requests by specifying it after a dash:
+
[literal,subs="+quotes,attributes,verbatim"]
....
`- 8.8.8.8`
....
... Add the `forwardpolicy` variable and set it to `first`.
... Add the `skip_overlap_check` variable and set it to `true`.
... Change the `state` variable to `present`.

+
This the modified Ansible playbook file for the current example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to ensure the presence of a dnsforwardzone in IdM DNS
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure the presence of a dnsforwardzone for example.com to 8.8.8.8
  ipadnsforwardzone:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: example.com
      forwarders:
          - 8.8.8.8
      forwardpolicy: first
      skip_overlap_check: true
      state: present
....

. Save the file.

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ ansible-playbook --vault-password-file=password_file -v -i inventory.file ensure-presence-forwardzone.yml
....

[role="_additional-resources"]
.Additional resources
* See the `README-dnsforwardzone.md` file in the `/usr/share/doc/ansible-freeipa/` directory.
