:_mod-docs-content-type: CONCEPT

[id="how-the-ad-provider-handles-trusted-domains_{context}"]

= How the AD provider handles trusted domains

If you set the `id_provider = ad` option in the `/etc/sssd/sssd.conf` configuration file, SSSD handles trusted domains as follows:

* SSSD only supports domains in a single AD forest. If SSSD requires access to multiple domains from multiple forests, consider using IPA with trusts (preferred) or the `winbindd` service instead of SSSD.

* By default, SSSD discovers all domains in the forest and, if a request for an object in a trusted domain arrives, SSSD tries to resolve it.
+
If the trusted domains are not reachable or geographically distant, which makes them slow, you can set the `ad_enabled_domains` parameter in `/etc/sssd/sssd.conf` to limit from which trusted domains SSSD resolves objects.

* By default, you must use fully-qualified user names to resolve users from trusted domains.

[role="_additional-resources"]
.Additional resources
* The `sssd.conf(5)` man page.
