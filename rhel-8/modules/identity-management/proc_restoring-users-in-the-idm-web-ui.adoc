:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// include::assemblies/assembly_managing-user-accounts-in-the-idm-web-ui.adoc[leveloffset=+1]


[id="restoring-users-in-the-idm-web-ui_{context}"]
= Restoring users in the IdM Web UI

[role="_abstract"]
IdM (Identity Management) enables you to restore preserved user accounts back to the active state. You can restore a preserved user to an active user or a stage user.

.Prerequisites

* Administrator privileges for managing the IdM Web UI or User Administrator role.

.Procedure

. Log in to the IdM Web UI.
+
ifeval::[{ProductNumber} == 8]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/accessing-the-ipa-web-ui-in-a-web-browser_configuring-and-managing-idm[Accessing the IdM Web UI in a web browser].
endif::[]

. Go to *Users -> Preserved users* tab.

. Click the check-box at the user accounts you want to restore.

. Click on the *Restore* button.
+
image:idm-users-preserved-restore.png[A screenshot of the "Preserved users" page displaying a table of users and their attributes. The checkbox next to one user entry is checked and the "Restore" button at the top right is highlighted.]

. In the *Confirmation* dialog box, click on the *OK* button.

The IdM Web UI displays a green confirmation and moves the user accounts to the *Active users* tab.
