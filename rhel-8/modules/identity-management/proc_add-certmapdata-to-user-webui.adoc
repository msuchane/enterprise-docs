:_mod-docs-content-type: PROCEDURE
// [[proc-add-certmapdata-to-user-webui_{context}]]
[id="proc-add-certmapdata-to-user-webui_{context}"]
= Adding certificate mapping data to a user entry in the IdM web UI

[role="_abstract"]
. Log into the IdM web UI as an administrator.

. Navigate to `Users` -> `Active users` -> `idm_user`.

. Find the `Certificate mapping data` option and click `Add`.

. Choose one of the following options:

* If you have the certificate of `idm_user`:
+
--
.. In the command-line interface, display the certificate using the `cat` utility or a text editor:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *cat idm_user_certificate.pem*
-----BEGIN CERTIFICATE-----
MIIFFTCCA/2gAwIBAgIBEjANBgkqhkiG9w0BAQsFADA6MRgwFgYDVQQKDA9JRE0u
RVhBTVBMRS5DT00xHjAcBgNVBAMMFUNlcnRpZmljYXRlIEF1dGhvcml0eTAeFw0x
ODA5MDIxODE1MzlaFw0yMDA5MDIxODE1MzlaMCwxGDAWBgNVBAoMD0lETS5FWEFN
[...output truncated...]
....

+
.. Copy the certificate.

+
.. In the IdM web UI, click `Add` next to `Certificate` and paste the certificate into the window that opens up.

+
[#{context}-add-user-certmapdata-cert]
.Adding a user's certificate mapping data: certificate
image::user-add-cert.png[Screenshot of a page displaying settings for the user "demouser" with an Identity Settings column on the left with entries such as Job Title - First name - Last name - Full name - Display name. The "Account Settings" column is on the right with entries such as User login - Password - UID - GID. The "Add" button for the "Certificates" entry is highlighted.]

+
* If you do not have the certificate of `idm_user` at your disposal but know the `Issuer` and the `Subject` of the certificate, check the radio button of `Issuer and subject` and enter the values in the two respective boxes.

+
[#{context}-add-user-certmapdata-data]
.Adding a user's certificate mapping data: issuer and subject
image::user-add-certdata.png[Screenshot of the "Add Certificate Mapping Data" pop-up window with two radial button options: "Certificate mapping data" and "Issuer and subject." "Issuer and subject" is selected and its two fields (Issuer and Subject) have been filled out.]
--

+
. Click `Add`.

.Verification steps

If you have access to the whole certificate in the `.pem` format, verify that the user and certificate are linked:

--
. Use the `sss_cache` utility to invalidate the record of `idm_user` in the SSSD cache and force a reload of the `idm_user` information:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *sss_cache -u idm_user*
....

. Run the `ipa certmap-match` command with the name of the file containing the certificate of the IdM user:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *ipa certmap-match idm_user_cert.pem*
--------------
1 user matched
--------------
 Domain: IDM.EXAMPLE.COM
 User logins: idm_user
----------------------------
Number of entries returned 1
----------------------------
....

+
The output confirms that now you have certificate mapping data added to `idm_user` and that a corresponding mapping rule exists. This means that you can use any certificate that matches the defined certificate mapping data to authenticate as `idm_user`.
--
