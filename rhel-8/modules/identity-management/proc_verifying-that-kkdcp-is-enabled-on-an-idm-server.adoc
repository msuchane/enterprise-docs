:_mod-docs-content-type: PROCEDURE

[id="verifying-that-kkdcp-is-enabled-on-an-idm-server_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Verifying that KKDCP is enabled on an IdM server

[role="_abstract"]
On an {IPA} (IdM) server, the Kerberos Key Distribution Center Proxy (KKDCP) is automatically enabled each time the Apache web server starts if the attribute and value pair [command]`ipaConfigString=kdcProxyEnabled` exists in the directory. In this situation, the symbolic link `/etc/httpd/conf.d/ipa-kdc-proxy.conf` is created.

You can verify if the KKDCP is enabled on the IdM server, even as an unprivileged user.

.Procedure

* Check that the symbolic link exists:

[literal,subs="+quotes,verbatim"]
....
$ *ls -l /etc/httpd/conf.d/ipa-kdc-proxy.conf*
lrwxrwxrwx. 1 root root 36 Jun 21  2020 /etc/httpd/conf.d/ipa-kdc-proxy.conf -> /etc/ipa/kdcproxy/ipa-kdc-proxy.conf
....

The output confirms that KKDCP is enabled.
