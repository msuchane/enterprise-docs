:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
// assembly_tuning-sssd-performance-for-large-idm-ad-trust-deployments.adoc

:experimental:

[id="options-in-sssdconf-for-tuning-servers-and-clients_{context}"]
= Options in `sssd.conf` for tuning IdM servers and clients for large IdM-AD trust deployments

[role="_abstract"]
You can use the following options in the [filename]`/etc/sssd/sssd.conf` configuration file to tune the performance of SSSD in IdM servers and clients when you have a large IdM-AD trust deployment.

== Tuning options for IdM servers

ignore_group_members:: Knowing which groups a user belongs to, as opposed to all the users that belong to a group, is important when authenticating and authorizing a user. When `ignore_group_members` is set to `true`, SSSD only retrieves information about the group objects themselves and not their members, providing a significant performance boost.
+
[NOTE]
====
The `id user@ad-domain.com` command still returns the correct list of groups, but `getent group ad-group@ad-domain.com` returns an empty list.
====
+
|===
| Default value | `false`
| Recommended value | `true`
|===
+
[NOTE]
====
You should not set this option to `true` when the deployment involves an IdM server with the compat tree.
====

subdomain_inherit::
With the `subdomain_inherit` option, you can apply the `ignore_group_members` setting to the trusted AD domains’ configuration. Settings listed in the `subdomain_inherit` options apply to both the main (IdM) domain as well as the AD subdomain.
+
|===
| Default value | `none`
| Recommended value | `subdomain_inherit = ignore_group_members`
|===


== Tuning options for IdM clients

pam_id_timeout:: This parameter controls how long results from a PAM session are cached, to avoid excessive round-trips to the identity provider during an identity lookup. The default value of `5` seconds might not be enough in environments where complex group memberships are populated on the IdM Server and IdM client side. Red Hat recommends setting `pam_id_timeout` to the number of seconds a single un-cached login takes.
+
|===
| Default value | `5`
| Recommended value | `the number of seconds a single un-cached login takes`
|===

krb5_auth_timeout:: Increasing `krb5_auth_timeout` allows more time to process complex group information in environments where users are members of a large number of groups. Red Hat recommends setting this value to the number of seconds a single un-cached login takes.
+
|===
| Default value | `6`
| Recommended value | `the number of seconds a single un-cached login takes`
|===

ldap_deref_threshold:: A dereference lookup is a means of fetching all group members in a single LDAP call. The `ldap_deref_threshold` value specifies the number of group members that must be missing from the internal cache to trigger a dereference lookup. If less members are missing, they are looked up individually.
Dereference lookups may take a long time in large environments and decrease performance. To disable dereference lookups, set this option to `0`.
+
|===
| Default value | `10`
| Recommended value | `0`
|===
