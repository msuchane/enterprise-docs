:_mod-docs-content-type: REFERENCE

[id="ref_directory-server-log-files_{context}"]
= Directory Server log files

The following table presents directories and files that the {IPA} (IdM) Directory Server (DS) instance uses to log information. You can use the files and directories for troubleshooting DS-related problems.

.Directory Server log files

[cols="3,5a",options="header"]
|===
|Directory or file|Description
|`/var/log/dirsrv/slapd-_REALM_NAME_pass:attributes[{blank}]/`|Log files associated with the DS instance used by the IdM server. Most operational data recorded here are related to server-replica interactions.

|`/var/log/dirsrv/slapd-_REALM_NAME_pass:attributes[{blank}]/audit`|Contains audit trails of all DS operations when auditing is enabled in the DS configuration.

[NOTE]
====
You can also audit the Apache error logs, where the IdM API logs access. However, because changes can be made directly over LDAP too, {RH} recommends enabling the more comprehensive `/var/log/dirsrv/slapd-REALM_NAME/audit` log for auditing purposes.
====

|`/var/log/dirsrv/slapd-_REALM_NAME_pass:attributes[{blank}]/access`

|Contains detailed information about attempted access for the domain DS instance.

|`/var/log/dirsrv/slapd-_REALM_NAME_pass:attributes[{blank}]/errors`

|Contains detailed information about failed operations for the domain DS instance.
|===


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_directory_server/11/html/administration_guide/monitoring_server_and_database_activity[Monitoring Server and Database Activity]
* link:https://access.redhat.com/documentation/en-us/red_hat_directory_server/11/html/configuration_command_and_file_reference/logs-reference[Log File Reference]
