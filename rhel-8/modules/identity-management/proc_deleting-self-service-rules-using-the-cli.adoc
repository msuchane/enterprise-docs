:_mod-docs-content-type: PROCEDURE
[id="deleting-self-service-rules-using-the-cli_{context}"]
= Deleting self-service rules using the CLI

[role="_abstract"]
Follow this procedure to delete self-service access rules in IdM using the command-line interface (CLI).

.Prerequisites

* Administrator privileges for managing IdM or the *User Administrator* role.
* An active Kerberos ticket. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/logging-in-to-ipa-from-the-command-line_configuring-and-managing-idm#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/logging-in-to-ipa-from-the-command-line_accessing-idm-services#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]

.Procedure

* Use the [command]`ipa selfservice-del` command to delete a self-service rule.

For example: +
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa selfservice-del "Users can manage their own name details"
-----------------------------------------------------------
Deleted selfservice "Users can manage their own name details"
-----------------------------------------------------------
....

.Verification steps

* Use the `ipa selfservice-find` command to display all self-service rules. The rule you just deleted should be missing.
