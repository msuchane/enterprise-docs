:_mod-docs-content-type: CONCEPT

[id="what-is-the-expiring-password-notification-tool_{context}"]

= What is the Expiring Password Notification tool

[role="_abstract"]
The Expiring Password Notification (EPN) tool is a standalone tool you can use to build a list of Identity Management (IdM) users whose passwords are expiring in a configured amount of time.

IdM administrators can use EPN to:

* Display a list of affected users in JSON format, which is created when run in dry-run mode.
* Calculate how many emails will be sent for a given day or date range.
* Send password expiration email notifications to users.
* Configure the `ipa-epn.timer` to run the EPN tool daily and send an email to users whose passwords are expiring within the defined future date ranges.
* Customize the email notification to send to users.

[NOTE]
====
If a user account is disabled, no email notifications are sent if the password is going to expire.
====
