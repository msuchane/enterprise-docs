:_mod-docs-content-type: PROCEDURE
[id="installing-packages-required-for-an-idm-server_{context}"]

= Installing packages required for an IdM server

[role="_abstract"]
In RHEL8, the packages necessary for installing an {IPA} (IdM) server are shipped as a module. The IdM server module stream is called the [stream]`DL1` stream, and you need to enable this stream before downloading packages from this stream. The following procedure shows how to download the packages necessary for setting up the IdM environment of your choice.

.Prerequisites

* You have a newly installed RHEL system.
* You have made the required repositories available:

** If your RHEL system is not running in the cloud, you have registered your system with the {RH} Subscription Manager (RHSM). For details, see link:https://access.redhat.com/documentation/en-us/red_hat_subscription_management/1/html/rhsm/reg-cli[Registration, attaching, and removing subscriptions in the Subscription Manager command line]. You have also enabled the `BaseOS` and `AppStream` repositories that IdM uses:

+
[literal,subs="+quotes,attributes"]
....
# *subscription-manager repos --enable=rhel-8-for-x86_64-baseos-rpms*
# *subscription-manager repos --enable=rhel-8-for-x86_64-appstream-rpms*
....

+
For details on how to enable and disable specific repositories using RHSM, see link:https://access.redhat.com/documentation/en-us/red_hat_subscription_management/1/html/rhsm/repos[Configuring options in Red Hat Subscription Manager].

** If your RHEL system is running in the cloud, skip the registration. The required repositories are already available via the Red Hat Update Infrastructure (RHUI).

* You have not previously enabled an IdM module stream.


.Procedure
////
. Enable the `BaseOS` and `AppStream` repositories that IdM uses:

+
[literal,subs="+quotes,attributes"]
----
# *subscription-manager repos --enable=rhel-8-for-x86_64-baseos-rpms*
# *subscription-manager repos --enable=rhel-8-for-x86_64-appstream-rpms*
----

+
For details on how to enable and disable specific repositories using RHSM, see https://access.redhat.com/documentation/en-us/red_hat_subscription_management/1/html/rhsm/repos[Configuring options in Red Hat Subscription Manager].
////

. Enable the [stream]`idm:DL1` stream:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module enable idm:DL1*
....

+
. Switch to the RPMs delivered through the [stream]`idm:DL1` stream:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} distro-sync*
....

+
. Choose one of the following options, depending on your IdM requirements:
+
--
* To download the packages necessary for installing an IdM server without an integrated DNS:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module install idm:DL1/server*
....
+
* To download the packages necessary for installing an IdM server with an integrated DNS:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module install idm:DL1/dns*
....

+
* To download the packages necessary for installing an IdM server that has a trust agreement with Active Directory:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module install idm:DL1/adtrust*
....

+
* To download the packages from multiple profiles, for example the [stream]`adtrust` and [stream]`dns` profiles:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module install idm:DL1/{dns,adtrust}*
....

+
* To download the packages necessary for installing an IdM client:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module install idm:DL1/client*
....
--

[IMPORTANT]
====
When switching to a new module stream once you have already enabled a different stream and downloaded packages from it, you need to first explicitly remove all the relevant installed content and disable the current module stream before enabling the new module stream. Trying to enable a new stream without disabling the current one results in an error. For details on how to proceed, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_managing_and_removing_user-space_components/managing-versions-of-appstream-content_using-appstream#switching-to-a-later-stream_managing-versions-of-appstream-content[Switching to a later stream].
====



[WARNING]
====
While it is possible to install packages from modules individually, be aware that if you install any package from a module that is not listed as "API" for that module, it is only going to be supported by {RH} in the context of that module. For example, if you install [package]`bind-dyndb-ldap` directly from the repository to use with your custom 389 Directory Server setup, any problems that you have will be ignored unless they occur for IdM, too.
====
