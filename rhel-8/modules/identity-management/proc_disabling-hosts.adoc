:_mod-docs-content-type: PROCEDURE

[id="proc_disabling-hosts_{context}"]
= Disabling Hosts

Complete this procedure to disable a host entry in IdM.

Domain services, hosts, and users can access an active host. There can be situations when it is necessary to remove an active host temporarily, for maintenance reasons, for example. Deleting the host in such situations is not desired as it removes the host entry and all the associated configuration permanently. Instead, choose the option of disabling the host.

Disabling a host prevents domain users from accessing it without permanently removing it from the domain.

.Procedure

* Disable a host using the [command]`host-disable` command. Disabling a host kills the host's current, active keytabs. For example:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *kinit admin*
$ *ipa host-disable client.example.com*
....

As a result of disabling a host, the host becomes unavailable to all IdM users, hosts and services.

[IMPORTANT]
====
Disabling a host entry not only disables that host. It disables every configured service on that host as well.
====
