:_mod-docs-content-type: CONCEPT
[id="what-is-a-password-policy_{context}"]
= What is a password policy

[role="_abstract"]
A password policy is a set of rules that passwords must meet. For example, a password policy can define the minimum password length and the maximum password lifetime. All users affected by this policy are required to set a sufficiently long password and change it frequently enough to meet the specified conditions. In this way, password policies help reduce the risk of someone discovering and misusing a user's password. 
