:_mod-docs-content-type: PROCEDURE
[id="retrieving-IdM-configuration-using-an-Ansible-playbook_{context}"]
= Retrieving IdM configuration using an Ansible playbook

[role="_abstract"]
The following procedure describes how you can use an Ansible playbook to retrieve information about the current global IdM configuration.

.Prerequisites

* You know the IdM administrator password.
* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.




.Procedure

. Open the `/usr/share/doc/ansible-freeipa/playbooks/config/retrieve-config.yml` Ansible playbook file for editing:
+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to handle global IdM configuration
  hosts: ipaserver
  become: no
  gather_facts: no

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Query IPA global configuration
    ipaconfig:
      ipaadmin_password: "{{ ipaadmin_password }}"
    register: serverconfig

  - debug:
      msg: "{{ serverconfig }}"
....

. Adapt the file by changing the following:

* The password of IdM administrator.
* Other values, if necessary.

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i __path_to_inventory_directory__/inventory.file /usr/share/doc/ansible-freeipa/playbooks/config/retrieve-config.yml*
[...]
TASK [debug]
ok: [server.idm.example.com] => {
    "msg": {
        "ansible_facts": {
            "discovered_interpreter_
        },
        "changed": false,
        "config": {
            "ca_renewal_master_server": "server.idm.example.com",
            "configstring": [
                "AllowNThash",
                "KDC:Disable Last Success"
            ],
            "defaultgroup": "ipausers",
            "defaultshell": "/bin/bash",
            "emaildomain": "idm.example.com",
            "enable_migration": false,
            "groupsearch": [
                "cn",
                "description"
            ],
            "homedirectory": "/home",
            "maxhostname": "64",
            "maxusername": "64",
            "pac_type": [
                "MS-PAC",
                "nfs:NONE"
            ],
            "pwdexpnotify": "4",
            "searchrecordslimit": "100",
            "searchtimelimit": "2",
            "selinuxusermapdefault": "unconfined_u:s0-s0:c0.c1023",
            "selinuxusermaporder": [
                "guest_u:s0$xguest_u:s0$user_
            ],
            "usersearch": [
                "uid",
                "givenname",
                "sn",
                "telephonenumber",
                "ou",
                "title"
            ]
        },
        "failed": false
    }
}
....
