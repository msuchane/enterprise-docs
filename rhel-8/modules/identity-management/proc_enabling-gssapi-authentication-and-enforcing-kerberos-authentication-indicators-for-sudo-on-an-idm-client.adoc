:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies:
//
// assembly_granting-sudo-access-to-an-IdM-user-on-an-IdM-client.adoc


[id="proc_enabling-gssapi-authentication-and-enforcing-kerberos-authentication-indicators-for-sudo-on-an-idm-client_{context}"]
= Enabling GSSAPI authentication and enforcing Kerberos authentication indicators for sudo on an IdM client

[role="_abstract"]
The following procedure describes enabling Generic Security Service Application Program Interface (GSSAPI) authentication on an IdM client for the `sudo` and `sudo -i` commands via the `pam_sss_gss.so` PAM module. Additionally, only users who have logged in with a smart card will authenticate to those commands with their Kerberos ticket.

[NOTE]
====
You can use this procedure as a template to configure GSSAPI authentication with SSSD for other PAM-aware services, and further restrict access to only those users that have a specific authentication indicator attached to their Kerberos ticket.
====


.Prerequisites

* You have created a `sudo` rule for an IdM user that applies to an IdM host. For this example, you have created the `idm_user_reboot` `sudo` rule to grant the `idm_user` account the permission to run the `/usr/sbin/reboot` command on the `idmclient` host.
* You have configured smart card authentication for the `idmclient` host.
// Only display the following if this is the RHEL 8 Conf&Man guide
ifeval::[{ProductNumber} == 8]
* The `idmclient` host is running RHEL 8.4 or later.
endif::[]
* You need `root` privileges to modify the `/etc/sssd/sssd.conf` file and PAM files in the `/etc/pam.d/` directory.


.Procedure

. Open the `/etc/sssd/sssd.conf` configuration file.

. Add the following entries to the `[domain/_<domain_name>_]` section.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[domain/_<domain_name>_]
*pam_gssapi_services = sudo, sudo-i*
*pam_gssapi_indicators_map = sudo:pkinit, sudo-i:pkinit*
....

. Save and close the `/etc/sssd/sssd.conf` file.

. Restart the SSSD service to load the configuration changes.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *systemctl restart sssd*
....

. Open the `/etc/pam.d/sudo` PAM configuration file.

. Add the following entry as the first line of the `auth` section in the `/etc/pam.d/sudo` file.
+
[literal,subs="+quotes,attributes,verbatim"]
....
#%PAM-1.0
*auth       sufficient   pam_sss_gss.so*
auth       include      system-auth
account    include      system-auth
password   include      system-auth
session    include      system-auth
....

. Save and close the `/etc/pam.d/sudo` file.

. Open the `/etc/pam.d/sudo-i` PAM configuration file.

. Add the following entry as the first line of the `auth` section in the `/etc/pam.d/sudo-i` file.
+
[literal,subs="+quotes,attributes,verbatim"]
....
#%PAM-1.0
*auth       sufficient   pam_sss_gss.so*
auth       include      sudo
account    include      sudo
password   include      sudo
session    optional     pam_keyinit.so force revoke
session    include      sudo
....

. Save and close the `/etc/pam.d/sudo-i` file.


.Verification steps

. Log into the host as the `idm_user` account and authenticate with a smart card.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ssh -l _idm_user@idm.example.com_ localhost*
PIN for smart_card
....

. Verify that you have a ticket-granting ticket as the smart card user.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm_user@idmclient ~]$ *klist*
Ticket cache: KEYRING:persistent:1358900015:krb_cache_TObtNMd
Default principal: *idm_user@IDM.EXAMPLE.COM*

Valid starting       Expires              Service principal
02/15/2021 16:29:48  02/16/2021 02:29:48  krbtgt/IDM.EXAMPLE.COM@IDM.EXAMPLE.COM
	renew until 02/22/2021 16:29:44
....

. Display which `sudo` rules the `idm_user` account is allowed to perform.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm_user@idmclient ~]$ *sudo -l*
Matching Defaults entries for *idmuser* on *idmclient*:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin,
    env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS",
    env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE",
    env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES",
    env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE",
    env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY KRB5CCNAME",
    secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User *idm_user* may run the following commands on *idmclient*:
    *(root) /usr/sbin/reboot*
....

. Reboot the machine using `sudo`, without specifying a password.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm_user@idmclient ~]$ *sudo /usr/sbin/reboot*
....


[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/granting-sudo-access-to-an-idm-user-on-an-idm-client_configuring-and-managing-idm#ref_sssd-options-controlling-gssapi-authentication-for-pam-services_granting-sudo-access-to-an-IdM-user-on-an-IdM-client[SSSD options controlling GSSAPI authentication for PAM services]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/granting-sudo-access-to-an-idm-user-on-an-idm-client_using-ansible-to-install-and-manage-identity-management#ref_sssd-options-controlling-gssapi-authentication-for-pam-services_granting-sudo-access-to-an-IdM-user-on-an-IdM-client[SSSD options controlling GSSAPI authentication for PAM services]
endif::[]
* The GSSAPI entry in the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/planning_identity_management/overview-of-planning-for-identity-management-and-access-control-planning-identity-management#IdM_terminology_overview-of-planning-idm-and-access-control[IdM terminology] listing
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication[Configuring Identity Management for smart card authentication]
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/managing-kerberos-ticket-policies_configuring-and-managing-idm#kerberos-authentication-indicators_managing-kerberos-ticket-policies[Kerberos authentication indicators]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_idm_users_groups_hosts_and_access_control_rules/managing-kerberos-ticket-policies_managing-users-groups-hosts#kerberos-authentication-indicators_managing-kerberos-ticket-policies[Kerberos authentication indicators]
endif::[]
* xref:using-IdM-Web-UI-to-grant-sudo-access-to-an-IdM-user-on-an-IdM-client_granting-sudo-access-to-an-IdM-user-on-an-IdM-client[Granting sudo access to an IdM user on an IdM client using IdM Web UI]
* xref:proc_granting-sudo-access-to-an-idm-user-on-an-idm-client-using-the-cli_granting-sudo-access-to-an-IdM-user-on-an-IdM-client[Granting sudo access to an IdM user on an IdM client using the CLI].
* `pam_sss_gss (8)` man page
* `sssd.conf (5)` man page
