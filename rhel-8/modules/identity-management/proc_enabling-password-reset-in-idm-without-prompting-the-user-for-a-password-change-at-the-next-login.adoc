[id="enabling-password-reset-in-idm-without-prompting-the-user-for-a-password-change-at-the-next-login_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Enabling password reset in IdM without prompting the user for a password change at the next login

By default, when an administrator resets another user's password, the password expires after the first successful login.
// <<changing-pwds>> for details.
As IdM Directory Manager, you can specify the following privileges for individual IdM administrators:

* They can perform password change operations without requiring users to change their passwords subsequently on their first login.

* They can bypass the password policy so that no strength or history enforcement is applied.

[WARNING]
====

Bypassing the password policy can be a security threat. Exercise caution when selecting users to whom you grant these additional privileges.

====


.Prerequisites

* You know the Directory Manager password.

.Procedure


. On every {IPA} (IdM) server in the domain, make the following changes:

+
--

.. Enter the `ldapmodify` command to modify LDAP entries. Specify the name of the IdM server and the 389 port and press Enter:

+
[literal,subs="+quotes,verbatim"]
....
$ ldapmodify -x -D "cn=Directory Manager" -W -h server.idm.example.com -p 389
Enter LDAP Password:
....


.. Enter the Directory Manager password.


.. Enter the distinguished name for the `ipa_pwd_extop` password synchronization entry and press Enter:

+
[literal,subs="+quotes,verbatim"]
....
dn: cn=ipa_pwd_extop,cn=plugins,cn=config
....

.. Specify the `modify` type of change and press Enter:

+
[literal,subs="+quotes,verbatim"]
....
changetype: modify
....


.. Specify what type of modification you want LDAP to execute and to which attribute. Press Enter:

+
[literal,subs="+quotes,verbatim"]
....
add: passSyncManagersDNs
....


.. Specify the administrative user accounts in the `passSyncManagersDNs` attribute. The attribute is multi-valued. For example, to grant the `admin` user the password resetting powers of Directory Manager:

+
[literal,subs="+quotes,verbatim"]
....
passSyncManagersDNs: \
uid=admin,cn=users,cn=accounts,dc=example,dc=com
....


.. Press Enter twice to stop editing the entry.

--


The whole procedure looks as follows:

[literal,subs="+quotes,verbatim"]
....
$ *ldapmodify -x -D "cn=Directory Manager" -W -h server.idm.example.com -p 389*
Enter LDAP Password:
*dn: cn=ipa_pwd_extop,cn=plugins,cn=config*
*changetype: modify*
*add: passSyncManagersDNs*
*passSyncManagersDNs: uid=admin,cn=users,cn=accounts,dc=example,dc=com*
....




The `admin` user, listed under `passSyncManagerDNs`, now has the additional privileges.
