:_mod-docs-content-type: CONCEPT
// This module is included in the following assemblies and titles:
//
// assembly_time-service-requirements-for-idm.adoc

[id="con_how-idm-uses-chronyd-for-synchronization_{context}"]
= How IdM uses `chronyd` for synchronization

[role="_abstract"]
You can use `chronyd` to keep your IdM hosts in sync with a central time source as described here.

Kerberos, the underlying authentication mechanism in IdM, uses time stamps as part of its protocol. Kerberos authentication fails if the system time of an IdM client differs by more than five minutes from the system time of the Key Distribution Center (KDC).

To ensure that IdM servers and clients stay in sync with a central time source, IdM installation scripts automatically configure `chronyd` Network Time Protocol (NTP) client software.

If you do not pass any NTP options to the IdM installation command, the installer searches for `_ntp._udp` DNS service (SRV) records that point to the NTP server in your network and configures `chrony` with that IP address. If you do not have any `_ntp._udp` SRV records, `chronyd` uses the configuration shipped with the `chrony` package.

// Only display the following note if this is the RHEL 8 Installation guide
ifeval::[{ProductNumber} > 7]
[NOTE]
====
Because `ntpd` has been deprecated in favor of `chronyd` in RHEL 8, IdM servers are no longer configured as Network Time Protocol (NTP) servers and are only configured as NTP clients. The RHEL 7 `NTP Server` IdM server role has also been deprecated in RHEL 8.
====
endif::[]

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/considerations_in_adopting_rhel_8/infrastructure-services_considerations-in-adopting-rhel-8#implementation-of-ntp_time-synchronization[Implementation of NTP]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/configuring-time-synchronization_configuring-basic-system-settings#using-chrony-to-configure-ntp_configuring-time-synchronization[Using the Chrony suite to configure NTP]
