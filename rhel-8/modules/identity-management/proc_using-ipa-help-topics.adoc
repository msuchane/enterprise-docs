:_mod-docs-content-type: PROCEDURE

//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="using-ipa-help-topics_{context}"]
= Using IPA help topics

[role="_abstract"]
The following procedure describes how to use the IPA help in the command-line interface.

.Procedure

. Open a terminal and connect to the IdM server.

. Enter `ipa help topics` to display a list of topics covered by help.
+
[literal,subs="+quotes,attributes,verbatim"]
....

$ *ipa help topics*
....

. Select one of the topics and create a command according to the following pattern: `ipa help _[topic_name]_`. Instead of the `topic_name` string, add one of the topics you listed in the previous step.
+
In the example, we use the following topic: `user`
+
[literal,subs="+quotes,attributes,verbatim"]
....

$ *ipa help user*
....

. If the IPA help output is too long and you cannot see the whole text, use the following syntax:
+
[literal,subs="+quotes,attributes,verbatim"]
....

$ *ipa help user | less*
....
+
You can then scroll down and read the whole help.

The IPA CLI displays a help page for the `user` topic. After reading the overview, you can see many examples with patterns for working with topic commands.
