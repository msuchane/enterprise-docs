:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="adding-a-member-to-a-user-group-using-idm-web-ui_{context}"]
= Adding a member to a user group using IdM Web UI

[role="_abstract"]
You can add both users and user groups as members of a user group. For more information, see xref:the-different-group-types-in-idm_{context}[The different group types in IdM] and xref:direct-and-indirect-group-members_{context}[Direct and indirect group members].

.Prerequisites

* You are logged in to the IdM Web UI.

.Procedure

. Click *Identity -> Groups* and select *User Groups* in the left sidebar.
. Click the name of the group.
. Select the type of group member you want to add: *Users, User Groups,* or *External*.
+
image::groups_add_member_updated.png[A screenshot of the "User Group" page highlighting the three buttons for the three categories of group members you can add: "Users" - "User Groups" - "External users".]
+
. Click *Add*.
. Select the check box next to one or more members you want to add.
. Click the rightward arrow to move the selected members to the group.
+
image::groups_add_member_dialog.png[A screenshot of the "Add users into user group group_a" pop-up window with a column to the left with "Available users" logins that can be checked. There is a right-arrow you can click to add users to the "Prospective" list on the right.]
+
. Click *Add* to confirm.
