:_mod-docs-content-type: PROCEDURE

[id="testing-hbac-rules-in-the-idm-webui_{context}"]
= Testing HBAC rules in the IdM WebUI

[role="_abstract"]
IdM allows you to test your HBAC configuration in various situations using simulated scenarios. Performing these simulated tests, you can discover misconfiguration problems or security risks before deploying HBAC rules in production.

[IMPORTANT]
====
Always test custom HBAC rules before you start using them in production.
====

Note that IdM does not test the effect of HBAC rules on trusted Active Directory (AD) users. Because the IdM LDAP directory does not store the AD data, IdM cannot resolve group membership of AD users when simulating HBAC scenarios.


.Procedure

. Select *Policy>Host-Based Access Control>HBAC Test*.

. On the *Who* window, specify the user under whose identity you want to perform the test, and click btn:[Next].

. On the *Accessing* window, specify the host that the user will attempt to access, and click btn:[Next].

. On the *Via Service* window, specify the service that the user will attempt to use, and click btn:[Next].

. On the *Rules* window, select the HBAC rules you want to test, and click btn:[Next]. If you do not select any rule, all rules are tested.
+
Select *Include Enabled* to run the test on all rules whose status is *Enabled*. Select *Include Disabled* to run the test on all rules whose status is *Disabled*. To view and change the status of HBAC rules, select *Policy>Host-Based Access Control>HBAC Rules*.
+
[IMPORTANT]
====
If the test runs on multiple rules, it passes successfully if at least one of the selected rules allows access.
====

. On the *Run Test* window, click btn:[Run Test].

. Review the test results:

* If you see *ACCESS DENIED*, the user is not granted access in the test.

* If you see *ACCESS GRANTED*, the user is able to access the host successfully.

+
By default, IdM lists all the tested HBAC rules when displaying the test results.

* Select *Matched* to display the rules that allowed successful access.
* Select *Unmatched* to display the rules that prevented access.

