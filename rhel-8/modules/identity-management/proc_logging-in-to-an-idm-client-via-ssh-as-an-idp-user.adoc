:_mod-docs-content-type: PROCEDURE

[id="proc_logging-in-to-an-idm-client-via-ssh-as-an-idp-user_{context}"]
= Logging in to an IdM client via SSH as an IdP user

To log in to an IdM client via SSH as an external identity provider (IdP) user, begin the login process on the command linel. When prompted, perform the authentication process at the website associated with the IdP, and finish the process at the Identity Management (IdM) client.

.Prerequisites

ifeval::[{ProductNumber} == 8]
* Your IdM client and IdM servers are using RHEL 8.7 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
* Your IdM client and IdM servers are using RHEL 9.1 or later.
endif::[]
* Your IdM client and IdM servers are using SSSD 2.7.0 or later.

ifdef::external-idp-manual[]
* You have created a reference to an external IdP in IdM. See 
xref:proc_creating-a-reference-to-an-external-identity-provider_{context}[Creating a reference to an external identity provider].
* You have associated an external IdP reference with the user account. See xref:proc_enabling-an-idm-user-to-authenticate-via-an-external-idp_{context}[Enabling an IdM user to authenticate via an external IdP].
endif::[]

ifdef::external-idp-ansible[]
* You have created a reference to an external IdP in IdM. See 
xref:using-ansible-to-create-a-reference-to-an-external-identity-provider_{context}[Using Ansible to create a reference to an external identity provider].
* You have associated an external IdP reference with the user account. See 
xref:using-ansible-to-enable-an-idm-user-to-authenticate-via-an-external-idp_{context}[Using Ansible to enable an IdM user to authenticate via an external IdP].
endif::[]


.Procedure

. Attempt to log in to the IdM client via SSH.
+
[literal,subs="+quotes,verbatim"]
....
[user@client ~]$ *ssh idm-user-with-external-idp@client.idm.example.com*
(idm-user-with-external-idp@client.idm.example.com) Authenticate at https://oauth2.idp.com:8443/auth/realms/main/device?user_code=XYFL-ROYR and press ENTER.
....

. In a browser, authenticate as the user at the website provided in the command output.

. At the command line, press the kbd:[Enter] key to finish the authentication process.


.Verification

* Display your Kerberos ticket information and confirm that the line `config: pa_type` shows `152` for pre-authentication with an external IdP.
+
[literal,subs="+quotes,verbatim"]
....
[idm-user-with-external-idp@client ~]$ klist -C
Ticket cache: KCM:0:58420
Default principal: idm-user-with-external-idp@IDM.EXAMPLE.COM

Valid starting     Expires            Service principal
05/09/22 07:48:23  05/10/22 07:03:07  krbtgt/IDM.EXAMPLE.COM@IDM.EXAMPLE.COM
config: fast_avail(krbtgt/IDM.EXAMPLE.COM@IDM.EXAMPLE.COM) = yes
08/17/2022 20:22:45  08/18/2022 20:22:43  krbtgt/IDM.EXAMPLE.COM@IDM.EXAMPLE.COM
*config: pa_type(krbtgt/IDM.EXAMPLE.COM@IDM.EXAMPLE.COM) = 152*
....
