:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// planning-your-dns-services-and-host-names.adoc
// planning-server-services-on-the-ipa-domain.adoc
[id="guidelines-for-planning-the-dns-domain-name-and-kerberos-realm-name_{context}"]
= Guidelines for planning the DNS domain name and Kerberos realm name

When installing the first {IPA} (IdM) server, the installation prompts for a primary DNS name of the IdM domain and Kerberos realm name. These guidelines can help you set the names correctly.

WARNING: You will not be able to change the IdM primary domain name and Kerberos realm name after the server is already installed. Do not expect to be able to move from a testing environment to a production environment by changing the names, for example from `_lab.example.com_` to `_production.example.com_`.

A separate DNS domain for service records:: Ensure that the _primary DNS domain_ used for IdM is not shared with any other system. This helps avoid conflicts on the DNS level.

Proper DNS domain name delegation:: Ensure you have valid delegation in the public DNS tree for the DNS domain. Do not use a domain name that is not delegated to you, not even on a private network.

Multi-label DNS domain:: Do not use single-label domain names, for example `.company`. The IdM domain must be composed of one or more subdomains and a top level domain, for example `example.com` or `company.example.com`.

A unique Kerberos realm name:: Ensure the realm name is not in conflict with any other existing Kerberos realm name, such as a name used by {AD} (AD).

Kerberos realm name as an upper-case version of the primary DNS name:: Consider setting the realm name to an upper-case (`_EXAMPLE.COM_`) version of the primary DNS domain name (`_example.com_`).
+
WARNING: If you do not set the Kerberos realm name to be the upper-case version of the primary DNS name, you will not be able to use AD trusts.

[discrete]
== Additional notes on planning the DNS domain name and Kerberos realm name

* One IdM deployment always represents one Kerberos realm.

* You can join IdM clients from multiple distinct DNS domains (`_example.com_`, `_example.net_`, `_example.org_`) to a single Kerberos realm (`_EXAMPLE.COM_`).

* IdM clients do not need to be in the primary DNS domain. For example, if the IdM domain is `_idm.example.com_`, the clients can be in the `_clients.example.com_` domain, but clear mapping must be configured between the DNS domain and the Kerberos realm.
+
NOTE: The standard method to create the mapping is using the *_kerberos* TXT DNS records. The IdM integrated DNS adds these records automatically.

[discrete]
== Planning DNS forwarding

* If you want to use only one forwarder for your entire IdM deployment, configure a *global forwarder*.

* If your company is spread over multiple sites in geographically distant regions, global forwarders might be impractical. Configure *per-server forwarders*.

* If your company has an internal DNS network that is not resolvable from the public internet, configure a *forward zone* and *zone forwarders* so that the hosts in the IdM domain can resolve hosts from this other internal DNS network.
