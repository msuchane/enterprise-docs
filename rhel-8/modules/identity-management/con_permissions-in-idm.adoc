:_mod-docs-content-type: CONCEPT
[id="permissions-in-idm_{context}"]
= Permissions in IdM

[role="_abstract"]
Permissions are the lowest level unit of role-based access control, they define operations together with the LDAP entries to which those operations apply. Comparable to building blocks, permissions can be assigned to as many privileges as needed. +
One or more *rights* define what operations are allowed:

* `write`
* `read`
* `search`
* `compare`
* `add`
* `delete`
* `all`

These operations apply to three basic *targets*:

* `subtree`: a domain name (DN); the subtree under this DN
* `target filter`: an LDAP filter
* `target`: DN with possible wildcards to specify entries

Additionally, the following convenience options set the corresponding attribute(s):

* `type`: a type of object (user, group, etc); sets `subtree` and `target filter`
* `memberof`: members of a group; sets a `target filter`
* `targetgroup`: grants access to modify a specific group (such as granting the rights to manage group membership); sets a `target`

With IdM permissions, you can control which users have access to which objects and even which attributes of these objects.
IdM enables you to allow or block individual attributes or change the entire visibility of a specific IdM function, such as users, groups, or sudo, to all anonymous users, all authenticated users, or just a certain group of privileged users. +
For example, the flexibility of this approach to permissions is useful for an administrator who wants to limit access of users or groups only to the specific sections these users or groups need to access and to make the other sections completely hidden to them.

NOTE: A permission cannot contain other permissions.
