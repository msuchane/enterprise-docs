:_mod-docs-content-type: PROCEDURE
[id="proc_gathering-debugging-logs-from-the-sssd-service-to-troubleshoot-authentication-issues-with-an-idm-client_{context}"]
= Gathering debugging logs from the SSSD service to troubleshoot authentication issues with an IdM client

[role="_abstract"]
If you experience issues when attempting to authenticate as an IdM user to an IdM client, verify that you can retrieve user information about the IdM server. If you cannot retrieve the user information about an IdM server, you will not be able to retrieve it on an IdM client (which retrieves information from the IdM server).

After you have confirmed that authentication issues do not originate from the IdM server, gather SSSD debugging logs from both the IdM server and IdM client.


.Prerequisites

* The user only has authentication issues on IdM clients, not IdM servers.
* You need the root password to run the `sssctl` command and restart the SSSD service.

.Procedure

. *On the client:* Open the `/etc/sssd/sssd.conf` file in a text editor.

. *On the client:* Add the `ipa_server` option to the `[domain]` section of the file and set it to an IdM server. This avoids the IdM client autodiscovering other IdM servers, thus limiting this test to just one client and one server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[domain/example.com]
ipa_server = server.example.com
...
....

. *On the client:* Save and close the `sssd.conf` file.

. *On the client:* Restart the SSSD service to load the configuration changes.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client ~]# systemctl restart sssd
....

. *On the server and client:* Enable detailed SSSD debug logging.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# sssctl debug-level 6
....
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client ~]# sssctl debug-level 6
....

. *On the server and client:* Invalidate objects in the SSSD cache for the user experiencing authentication issues, so you do not bypass the LDAP database and retrieve information SSSD has already cached.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# sssctl cache-expire -u _idmuser_
....
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client ~]# sssctl cache-expire -u _idmuser_
....

. *On the server and client:* Minimize the troubleshooting dataset by removing older SSSD logs.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# sssctl logs-remove
....
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# sssctl logs-remove
....

. *On the client:* Attempt to switch to the user experiencing authentication problems while gathering timestamps before and after the attempt. These timestamps further narrow the scope of the dataset.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client sssd]# date; su idmuser; date
Mon Mar 29 16:20:13 EDT 2021
su: user idmuser does not exist
Mon Mar 29 16:20:14 EDT 2021
....

. _(Optional)_ *On the server and client:* Lower the debug level if you do not wish to continue gathering detailed SSSD logs.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# sssctl debug-level 0
....
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client ~]# sssctl debug-level 0
....

. *On the server and client:* Review SSSD logs for information about the failed request.
.. Review the request from the client in the client logs.
.. Review the request from the client  in the server logs.
.. Review the result of the request in the server logs.
.. Review the outcome of the client receiving the results of the request from the server.


. If you are unable to determine the cause of the authentication issue:
.. Collect the SSSD logs you recently generated on the IdM server and IdM client. Label them according to their hostname or role.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# sssctl logs-fetch _sssd-logs-server-Mar29.tar_
....
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client ~]# sssctl logs-fetch _sssd-logs-client-Mar29.tar_
....

.. Open a Red Hat Technical Support case and provide:
... The SSSD debug logs:
.... `sssd-logs-server-Mar29.tar` from the server
.... `sssd-logs-client-Mar29.tar` from the client
... The console output, including the time stamps and user name, of the request that corresponds to the logs:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client sssd]# date; su _idmuser_; date
Mon Mar 29 16:20:13 EDT 2021
su: user idmuser does not exist
Mon Mar 29 16:20:14 EDT 2021
....
