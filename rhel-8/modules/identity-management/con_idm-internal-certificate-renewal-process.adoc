:_mod-docs-content-type: CONCEPT

[id="idm-internal-certificate-renewal-process_{context}"]
= IdM internal certificate renewal process

By default, `certmonger` tracks the internal certificates and triggers the renewal and requests the IdM CA to issue a new certificate.

If you are using an external CA and your internal certificates were issued by this CA, they are not automatically renewed. In this case, you should monitor the expiry dates of your certificates to ensure you renew them before they expire. The renewal process is time consuming and if you do not track the expiry dates carefully, your certificates will expire and some services will no longer be available.

[WARNING]
====
If your internal Red Hat Identity Management (IdM) certificates expire, IdM fails to start.
====

The IdM CA renewal server renews the shared internal certificates 28 days before their expiration date. `certmonger` triggers this renewal and uploads the new certificate into `cn=<nickname>,cn=ca_renewal,cn=ipa,cn=etc,$BASEDN`. `certmonger` also triggers the renewal process on the other IdM servers but as it is executed on an non-CA renewal server, it does not request a new certificate but downloads the certificate from LDAP. Note that the `Server-Cert cert-pki-ca`, HTTP, LDAP, and PKINIT certificates are specific to each replica, containing the hostname in their subject.

[NOTE]
====
If you manually renew a shared certificate with `getcert` before a certificate expires, the renewal process is not triggered on the other replicas and you must run `getcert` on the other replicas to perform the download of the renewed certificate from LDAP.
====
