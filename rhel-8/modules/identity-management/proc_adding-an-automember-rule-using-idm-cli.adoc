:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="adding-an-automember-rule-using-idm-cli_{context}"]
= Adding an automember rule using IdM CLI

[role="_abstract"]
Follow this procedure to add an automember rule using the IdM CLI. For information about automember rules, see xref:automember-rules_{context}[Automember rules].

After adding an automember rule, you can add conditions to it using the procedure described in xref:adding-a-condition-to-an-automember-rule-using-idm-cli_{context}[Adding a condition to an automember rule].

NOTE: Existing entries are *not* affected by the new rule. If you want to change existing entries, see xref:applying-automember-rules-to-existing-entries-using-idm-cli_{context}[Applying automember rules to existing entries using IdM CLI].

.Prerequisites

* You must be logged in as the administrator. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/logging-in-to-ipa-from-the-command-line_configuring-and-managing-idm#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/logging-in-to-ipa-from-the-command-line_accessing-idm-services#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]

* The target group of the new rule must exist in IdM.

.Procedure

. Enter the [command]`ipa automember-add` command to add an automember rule.

. When prompted, specify:
+
--
* *Automember rule*. This is the target group name.

* *Grouping Type*. This specifies whether the rule targets a user group or a host group. To target a user group, enter *group*. To target a host group, enter *hostgroup*.
--
+
For example, to add an automember rule for a user group named *user_group*:
+
[literal, subs="+quotes,verbatim"]
--
$ *ipa automember-add*
Automember Rule: *user_group*
Grouping Type: *group*
--------------------------------
Added automember rule "user_group"
--------------------------------
    Automember Rule: user_group
--

.Verification steps

* You can display existing automember rules and conditions in IdM using xref:viewing-existing-automember-rules-using-idm-cli_{context}[Viewing existing automember rules using IdM CLI].
