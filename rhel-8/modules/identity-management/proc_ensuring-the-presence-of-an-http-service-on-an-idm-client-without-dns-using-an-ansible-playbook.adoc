:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-an-http-service-on-an-idm-client-without-dns-using-an-ansible-playbook_{context}"]
= Ensuring the presence of an HTTP service on an IdM client without DNS using an Ansible playbook

[role="_abstract"]
Follow this procedure to ensure the presence of an HTTP server running on an IdM client that has no DNS entry using an Ansible playbook. The scenario implied is that the IdM host has no DNS A entry available - or no DNS AAAA entry if IPv6 is used instead of IPv4.

.Prerequisites

* The system to host the HTTP service is enrolled in IdM.
* The DNS A or DNS AAAA record for the host may not exist. Otherwise, if the DNS record for the host does exist, follow the procedure in
ifdef::c-m-idm[]
xref:ensuring-the-presence-of-an-http-service-in-idm-using-an-ansible-playbook_{context}[Ensuring the presence of an HTTP service in IdM using an Ansible playbook.]
endif::[]
ifndef::c-m-idm[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/ensuring-the-presence-and-absence-of-services-in-idm-using-ansible_configuring-and-managing-idm#ensuring-the-presence-of-an-http-service-in-idm-using-an-ansible-playbook_ensuring-the-presence-and-absence-of-services-in-idm-using-ansible[Ensuring the presence of an HTTP service in IdM using an Ansible playbook.]
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/ensuring-the-presence-and-absence-of-services-in-idm-using-ansible_using-ansible-to-install-and-manage-identity-management#ensuring-the-presence-of-an-http-service-in-idm-using-an-ansible-playbook_ensuring-the-presence-and-absence-of-services-in-idm-using-ansible[Ensuring the presence of an HTTP service in IdM using an Ansible playbook.]
endif::[]
endif::[]
* You have the IdM administrator password.

.Procedure

. Create an inventory file, for example `inventory.file`:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *touch inventory.file*
....

. Open the `inventory.file` and define the IdM server that you want to configure in the `[ipaserver]` section. For example, to instruct Ansible to configure *server.idm.example.com*, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....


. Make a copy of the `/usr/share/doc/ansible-freeipa/playbooks/service/service-is-present-with-host-force.yml` Ansible playbook file. For example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/service/service-is-present-with-host-force.yml /usr/share/doc/ansible-freeipa/playbooks/service/service-is-present-with-host-force-copy.yml*
....

. Open the copied file, `/usr/share/doc/ansible-freeipa/playbooks/service/service-is-present-with-host-force-copy.yml`, for editing. Locate the `ipaadmin_password` and `name` variables in the `ipaservice` task:
+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to manage IPA service.
  hosts: ipaserver
  gather_facts: false

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  # Ensure service is present
  - ipaservice:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: HTTP/ihavenodns.info*
      force: yes
....

. Adapt the file:

* Set the `ipaadmin_password` variable to your IdM administrator password.
* Set the `name` variable to the name of the host on which the HTTP service is running.

. Save and exit the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i __path_to_inventory_directory__/inventory.file /usr/share/doc/ansible-freeipa/playbooks/service/service-is-present-with-host-force-copy.yml*
....


.Verification steps

. Log into the IdM Web UI as IdM administrator.

. Navigate to `Identity` -> `Services`.

You can now see *HTTP/client.idm.example.com@IDM.EXAMPLE.COM* listed in the *Services* list.

[role="_additional-resources"]
.Additional resources
* To secure the communication, see
ifdef::c-m-idm[]
xref:proc_adding-tls-encryption-to-an-apache-http-server-configuration_restricting-an-application-to-trust-a-subset-of-certs[adding TLS encryption to the Apache HTTP Server].
endif::[]
ifndef::c-m-idm[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/restricting-an-application-to-trust-only-a-subset-of-certificates_configuring-and-managing-idm#proc_adding-tls-encryption-to-an-apache-http-server-configuration_restricting-an-application-to-trust-a-subset-of-certs[adding TLS encryption to an Apache HTTP Server].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/restricting-an-application-to-trust-only-a-subset-of-certificates_managing-certificates-in-idm#proc_adding-tls-encryption-to-an-apache-http-server-configuration_restricting-an-application-to-trust-a-subset-of-certs[adding TLS encryption to an Apache HTTP Server].
endif::[]
endif::[]
