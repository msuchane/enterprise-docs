:_newdoc-version: 2.17.0
:_template-generated: 2024-04-16
:_mod-docs-content-type: PROCEDURE

[id="displaying-kerberos-flags-from-the-command-line_{context}"]
= Displaying Kerberos flags from the command line

You can display Kerberos flag setting by using the command line. The following procedure displays the `OK_AS_DELEGATE` flag for the `demo/ipa.example.com@EXAMPLE.COM` principal.

.Procedure

To find out if `OK_AS_DELEGATE` is set for a principal:

. Run the `kvno` utility:
+
[literal,subs="+quotes"]
----
$ *kvno _demo/ipa.example.com@EXAMPLE.COM_*
----

. To display the flag setting, run the `klist -f` command. The `0` character means that the `OK_AS_DELEGATE` flag is disabled:
+
[literal,subs="+quotes"]
----
$ *klist -f*
Ticket cache: KEYRING:persistent:0:0
Default principal: admin@EXAMPLE.COM

Valid starting		Expires			Service principal
02/19/2024 09:59:02	02/20/2024 08:21:33	demo/ipa/example.com@EXAMPLE.COM
    Flags: FAT**O**
----


