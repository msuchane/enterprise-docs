:_newdoc-version: 2.15.1
:_template-generated: 2024-03-05

:_mod-docs-content-type: REFERENCE

[id="the-provider-option-in-the-ipaidp-ansible-module_{context}"]
= The provider option in the ipaidp Ansible module

[role="_abstract"]

The following identity providers (IdPs) support OAuth 2.0 device authorization grant flow:

* Microsoft Identity Platform, including Azure AD
* Google
* GitHub
* Keycloak, including Red Hat Single Sign-On (SSO)
* Okta

When using the `idp` `ansible-freeipa` module to create a reference to one of these external IdPs, you can specify the IdP type with the `provider` option in your `ipaidp` `ansible-freeipa` playbook task, which expands into additional options as described below:

`provider: microsoft`::
Microsoft Azure IdPs allow parametrization based on the Azure tenant ID, which you can specify with the `organization` option. If you need support for the live.com IdP, specify the option `organization common`.
+
Choosing `provider: microsoft` expands to use the following options. The value of the `organization` option replaces the string `${ipaidporg}` in the table.
+
[options="header",cols="1,2"]
|====
|Option|Value
|`auth_uri: URI` |`\https://login.microsoftonline.com/${ipaidporg}/oauth2/v2.0/authorize`
|`dev_auth_uri: URI` |`\https://login.microsoftonline.com/${ipaidporg}/oauth2/v2.0/devicecode`
|`token_uri: URI` |`\https://login.microsoftonline.com/${ipaidporg}/oauth2/v2.0/token`
|`userinfo_uri: URI` |`\https://graph.microsoft.com/oidc/userinfo`
|`keys_uri: URI` |`\https://login.microsoftonline.com/common/discovery/v2.0/keys`
|`scope: STR` |`openid email`
|`idp_user_id: STR` |`email`
|====



`provider: google`::
Choosing `provider: google` expands to use the following options:
+
[options="header",cols="1,2"]
|====
|Option|Value
|`auth_uri: URI` |`\https://accounts.google.com/o/oauth2/auth`
|`dev_auth_uri: URI` |`\https://oauth2.googleapis.com/device/code`
|`token_uri: URI` |`\https://oauth2.googleapis.com/token`
|`userinfo_uri: URI` |`\https://openidconnect.googleapis.com/v1/userinfo`
|`keys_uri: URI` |`\https://www.googleapis.com/oauth2/v3/certs`
|`scope: STR` |`openid email`
|`idp_user_id: STR` |`email`
|====


`provider: github`::
Choosing `provider: github` expands to use the following options:
+
[options="header",cols="1,2"]
|====
|Option|Value
|`auth_uri: URI` |`\https://github.com/login/oauth/authorize`
|`dev_auth_uri: URI` |`\https://github.com/login/device/code`
|`token_uri: URI` |`\https://github.com/login/oauth/access_token`
|`userinfo_uri: URI` |`\https://openidconnect.googleapis.com/v1/userinfo`
|`keys_uri: URI` |`\https://api.github.com/user`
|`scope: STR` |`user`
|`idp_user_id: STR` |`login`
|====


`provider: keycloak`::
With Keycloak, you can define multiple realms or organizations. Since it is often a part of a custom deployment, both base URL and realm ID are required, and you can specify them with the `base_url` and `organization` options in your `ipaidp` playbook task:
+
[literal,subs="+quotes,verbatim"]
....
---
- name: Playbook to manage IPA idp
  hosts: ipaserver
  become: false

  tasks:
  - name: Ensure keycloak idp my-keycloak-idp is present using provider
    ipaidp:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: my-keycloak-idp
      provider: keycloak
      organization: *main*
      base_url: *keycloak.domain.com:8443/auth*
      client_id: my-keycloak-client-id
....
+
Choosing `provider: keycloak` expands to use the following options. The value you specify in the `base_url` option replaces the string `${ipaidpbaseurl}` in the table, and the value you specify for the `organization `option replaces the string `${ipaidporg}`.
+
[options="header",cols="1,2"]
|====
|Option|Value
|`auth_uri: URI` |`\https://${ipaidpbaseurl}/realms/${ipaidporg}/protocol/openid-connect/auth`
|`dev_auth_uri: URI` |`\https://${ipaidpbaseurl}/realms/${ipaidporg}/protocol/openid-connect/auth/device`
|`token_uri: URI` |`\https://${ipaidpbaseurl}/realms/${ipaidporg}/protocol/openid-connect/token`
|`userinfo_uri: URI` |`\https://${ipaidpbaseurl}/realms/${ipaidporg}/protocol/openid-connect/userinfo`
|`scope: STR` |`openid email`
|`idp_user_id: STR` |`email`
|====


`provider: okta`::
After registering a new organization in Okta, a new base URL is associated with it. You can specify this base URL with the `base_url` option in the `ipaidp` playbook task:
+
[literal,subs="+quotes,verbatim"]
....
---
- name: Playbook to manage IPA idp
  hosts: ipaserver
  become: false

  tasks:
  - name: Ensure okta idp my-okta-idp is present using provider
    ipaidp:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: my-okta-idp
      provider: okta
      base_url: *dev-12345.okta.com*
      client_id: my-okta-client-id
....
+
Choosing `provider: okta` expands to use the following options. The value you specify for the `base_url` option replaces the string `${ipaidpbaseurl}` in the table.
+
[options="header",cols="1,2"]
|====
|Option|Value
|`auth_uri: URI` |`\https://${ipaidpbaseurl}/oauth2/v1/authorize`
|`dev_auth_uri: URI` |`\https://${ipaidpbaseurl}/oauth2/v1/device/authorize`
|`token_uri: URI` |`\https://${ipaidpbaseurl}/oauth2/v1/token`
|`userinfo_uri: URI` |`\https://${ipaidpbaseurl}/oauth2/v1/userinfo`
|`scope: STR` |`openid email`
|`idp_user_id: STR` |`email`
|====



[role="_additional-resources"]
.Additional resources
* link:https://freeipa.readthedocs.io/en/latest/designs/external-idp/idp-api.html#pre-populated-idp-templates[Pre-populated IdP templates]
