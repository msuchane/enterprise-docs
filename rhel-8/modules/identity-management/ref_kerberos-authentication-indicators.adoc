:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// assembly_managing-kerberos-ticket-policies.adoc


[id="kerberos-authentication-indicators_{context}"]
= Kerberos authentication indicators

[role="_abstract"]
The Kerberos Key Distribution Center (KDC) attaches _authentication indicators_ to a ticket-granting ticket (TGT) based on which pre-authentication mechanism the client used to prove its identity:

`otp`:: two-factor authentication (password + One-Time Password)
`radius`:: RADIUS authentication (commonly for 802.1x authentication)
`pkinit`:: PKINIT, smart card, or certificate authentication
`hardened`:: hardened passwords (SPAKE or FAST)footnote:[A hardened password is protected against brute-force password dictionary attacks by using Single-Party Public-Key Authenticated Key Exchange (SPAKE) pre-authentication and/or Flexible Authentication via Secure Tunneling (FAST) armoring.]

The KDC then attaches the authentication indicators from the TGT to any service ticket requests that stem from it. The KDC enforces policies such as service access control, maximum ticket lifetime, and maximum renewable age based on the authentication indicators.


.Authentication indicators and IdM services

If you associate a service or a host with an authentication indicator, only clients that used the corresponding authentication mechanism to obtain a TGT will be able to access it. The KDC, not the application or service, checks for authentication indicators in service ticket requests, and grants or denies requests based on Kerberos connection policies.

For example, to require two-factor authentication to connect to a Virtual Private Network (VPN), associate the `otp` authentication indicator with that service. Only users who used a One-Time password to obtain their initial TGT from the KDC will be able to log in to the VPN:

.Example of a VPN service requiring the otp authentication indicator

image::auth_indicators.png[]

If a service or a host has no authentication indicators assigned to it, it will accept tickets authenticated by any mechanism.


[role="_additional-resources"]
.Additional resources
* xref:enforcing-authentication-indicators-for-an-idm-service_managing-kerberos-ticket-policies[Enforcing authentication indicators for an IdM service]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/granting-sudo-access-to-an-idm-user-on-an-idm-client_configuring-and-managing-idm#proc_enabling-gssapi-authentication-and-enforcing-kerberos-authentication-indicators-for-sudo-on-an-idm-client_granting-sudo-access-to-an-IdM-user-on-an-IdM-client[Enabling GSSAPI authentication and enforcing Kerberos authentication indicators for sudo on an IdM client]
