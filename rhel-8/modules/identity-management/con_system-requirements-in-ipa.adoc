:_mod-docs-content-type: CONCEPT
[id="system-requirements-in-ipa_{context}"]
= Custom configuration requirements for IdM

[role="_abstract"]
Install an {IPA} (IdM) server on a clean system without any custom configuration for services such as DNS, Kerberos, Apache, or Directory{nbsp}Server.

The IdM server installation overwrites system files to set up the IdM domain. IdM backs up the original system files to `/var/lib/ipa/sysrestore/`. When an IdM server is uninstalled at the end of the lifecycle, these files are restored.

.IPv6 requirements in IdM

The IdM system must have the IPv6 protocol enabled in the kernel and localhost (::1) is able to use it. If IPv6 is disabled, then the CLDAP plug-in used by the IdM services fails to initialize.

[NOTE]
====
IPv6 does not have to be enabled on the network. It is possible to enable IPv6 stack without enabling IPv6 addresses if required.
====


.Support for encryption types in IdM

Red Hat Enterprise Linux (RHEL) uses Version 5 of the Kerberos protocol, which supports encryption types such as Advanced Encryption Standard (AES), Camellia, and Data Encryption Standard (DES).

.List of supported encryption types

While the Kerberos libraries on IdM servers and clients might support more encryption types, the IdM Kerberos Distribution Center (KDC) only supports the following encryption types:

* `aes256-cts:normal`
* `aes256-cts:special` (default)
* `aes128-cts:normal`
* `aes128-cts:special` (default)
* `aes128-sha2:normal`
* `aes128-sha2:special`
* `aes256-sha2:normal`
* `aes256-sha2:special`
* `camellia128-cts-cmac:normal`
* `camellia128-cts-cmac:special`
* `camellia256-cts-cmac:normal`
* `camellia256-cts-cmac:special`

.RC4 encryption types are disabled by default

The following RC4 encryption types have been deprecated and disabled by default in RHEL 8, as they are considered less secure than the newer AES-128 and AES-256 encryption types:

* `arcfour-hmac:normal`
* `arcfour-hmac:special`

For more information about manually enabling RC4 support for compatibility with legacy Active Directory environments, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-trust-between-idm-and-ad_installing-identity-management#ensuring-support-for-common-encryption-types-in-ad-and-rhel_installing-trust-between-idm-and-ad[Ensuring support for common encryption types in AD and RHEL].

.Support for DES and 3DES encryption has been removed

Due to security reasons, support for the DES algorithm was deprecated in RHEL 7. The recent rebase of Kerberos packages in RHEL 8.3.0 removes support for single-DES (DES) and triple-DES (3DES) encryption types from RHEL 8.

[NOTE]
====
Standard RHEL 8 IdM installations do not use DES or 3DES encryption types by default and are unaffected by the Kerberos upgrade.
====

If you manually configured any services or users to *only* use DES or 3DES encryption (for example, for legacy clients), you might experience service interruptions after updating to the latest Kerberos packages, such as:

* Kerberos authentication errors
* `unknown enctype` encryption errors
* KDCs with DES-encrypted Database Master Keys (`K/M`) fail to start

Red Hat recommends you do not use DES or 3DES encryption in your environment.

[NOTE]
====
You only need to disable DES and 3DES encryption types if you configured your environment to use them.
====

// Additional info in modules/identity-management/proc_ensuring-you-do-not-use-des-encryption.adoc

.Support for system-wide cryptographic policies in IdM

IdM uses the `DEFAULT` system-wide cryptographic policy. This policy offers secure settings for current threat models. It allows the TLS 1.2 and 1.3 protocols, as well as the IKEv2 and SSH2 protocols. The RSA keys and Diffie-Hellman parameters are accepted if they are at least 2048 bits long. This policy does not allow DES, 3DES, RC4, DSA, TLS v1.0, and other weaker algorithms.

[NOTE]
====
You cannot install an IdM server while using the `FUTURE` system-wide cryptographic policy. When installing an IdM server, ensure you are using the `DEFAULT` system-wide cryptographic policy.
====

.Additional Resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[System-wide cryptographic policies]
* man IPV6(7)



////
== IPv6 requirements in IdM

IPv6 must be enabled on the network to enable installing and running an IdM server. Most of IdM servers listen on TCP6 or UDP6 on all network interfaces. This
allows to implement double stack (TCP/TCP6 or UDP/UDP6) with the same code base and is recommended for contemporary networking applications.

IPv6 is enabled by default on {RHEL}{nbsp}7 systems. If you have disabled it, re-enable it following the steps in link:++https://access.redhat.com/solutions/8709++[How do I disable or enable the IPv6 protocol in Red Hat Enterprise Linux?].
// in Red{nbsp}Hat Knowledgebase.
//NOTE: IPv6 is enabled by default on {RHEL}{nbsp}7 systems.

//.Additional resources
//* If you disabled IPv6 before, re-enable it as described in link:++https://access.redhat.com/solutions/8709++[How do I disable or enable the IPv6 protocol in Red Hat Enterprise Linux?] in Red{nbsp}Hat Knowledgebase.
////
