:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies and titles:
//
// assembly_applying-group-policy-object-access-control-in-rhel.adoc

[id="changing-the-gpo-access-control-mode_{context}"]
= Changing the GPO access control mode

This procedure changes how GPO-based access control rules are evaluated and enforced on a RHEL host joined to an Active Directory (AD) environment.

In this example, you will change the GPO operation mode from `enforcing` (the default) to `permissive` for testing purposes.

[IMPORTANT]
====
If you see the following errors, Active Directory users are unable to log in due to GPO-based access controls:

* In `/var/log/secure`:
+
[literal,subs="+quotes,attributes"]
....
Oct 31 03:00:13 client1 sshd[124914]: pam_sss(sshd:account): *Access denied for user aduser1: 6 (Permission denied)*
Oct 31 03:00:13 client1 sshd[124914]: Failed password for aduser1 from 127.0.0.1 port 60509 ssh2
Oct 31 03:00:13 client1 sshd[124914]: fatal: *Access denied for user aduser1 by PAM account configuration [preauth]*
....

* In `/var/log/sssd/sssd__example.com_.log`:
+
[literal,subs="+quotes,attributes"]
....
(Sat Oct 31 03:00:13 2020) [sssd[be[example.com]]] [ad_gpo_perform_hbac_processing] (0x0040): *GPO access check failed: [1432158236](Host Access Denied)*
(Sat Oct 31 03:00:13 2020) [sssd[be[example.com]]] [ad_gpo_cse_done] (0x0040): HBAC processing failed: [1432158236](Host Access Denied}
(Sat Oct 31 03:00:13 2020) [sssd[be[example.com]]] [ad_gpo_access_done] (0x0040): *GPO-based access control failed.*
....

If this is undesired behavior, you can temporarily set `ad_gpo_access_control` to `permissive` as described in this procedure while you troubleshoot proper GPO settings in AD.
====

.Prerequisites

* You have joined a RHEL host to an AD environment using SSSD.
* Editing the `/etc/sssd/sssd.conf` configuration file requires `root` permissions.

.Procedure

. Stop the SSSD service.
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *systemctl stop sssd*
....

. Open the `/etc/sssd/sssd.conf` file in a text editor.

. Set `ad_gpo_access_control` to `permissive` in the `domain` section for the AD domain.
+
[literal,subs="+quotes,attributes"]
....
[domain/_example.com_]
ad_gpo_access_control=*permissive*
...
....

. Save the `/etc/sssd/sssd.conf` file.

. Restart the SSSD service to load configuration changes.
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *systemctl restart sssd*
....

[role="_additional-resources"]
.Additional resources
* For the list of different GPO access control modes, see xref:list-of-sssd-options-to-control-gpo-enforcement_applying-group-policy-object-access-control-in-rhel[List of SSSD options to control GPO enforcement].
