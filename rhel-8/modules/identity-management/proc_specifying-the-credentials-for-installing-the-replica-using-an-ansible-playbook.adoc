:_mod-docs-content-type: PROCEDURE
[id="specifying-the-credentials-for-installing-the-replica-using-an-ansible-playbook_{context}"]
= Specifying the credentials for installing the IdM replica using an Ansible playbook

Complete this procedure to configure the authorization for installing the IdM replica.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.


.Procedure

. Specify the *password of a user authorized to deploy replicas*, for example the IdM `admin`.


** {RH} recommends using the Ansible Vault to store the password, and referencing the Vault file from the playbook file, for example `install-replica.yml`:

+
.Example playbook file using principal from inventory file and password from an Ansible Vault file
[literal,subs="+quotes,attributes"]
....
- name: Playbook to configure IPA replicas
  hosts: ipareplicas
  become: true
  vars_files:
  - `playbook_sensitive_data.yml`

  roles:
  - role: ipareplica
    state: present
....

+
For details how to use Ansible Vault, see the official https://docs.ansible.com/ansible/latest/user_guide/vault.html[Ansible Vault] documentation.

+

--

** Less securely, provide the credentials of `admin` directly in the inventory file. Use the `ipaadmin_password` option in the `[ipareplicas:vars]` section of the inventory file. The inventory file and the `install-replica.yml` playbook file can then look as follows:

+
.Example inventory hosts.replica file
[literal,subs="+quotes,attributes"]
....
[...]
[ipareplicas:vars]
*ipaadmin_password=Secret123*
....


+
[#example-playbook-password-1_{context}]
.Example playbook using principal and password from inventory file
[literal,subs="+quotes,attributes"]
....
- name: Playbook to configure IPA replicas
  hosts: ipareplicas
  become: true

  roles:
  - role: ipareplica
    state: present
....

** Alternatively but also less securely, provide the credentials of another user authorized to deploy a replica directly in the inventory file. To specify a different authorized user, use the `ipaadmin_principal` option for the user name, and the `ipaadmin_password` option for the password. The inventory file and the `install-replica.yml` playbook file can then look as follows:

+
.Example inventory hosts.replica file
[literal,subs="+quotes,attributes"]
....
[...]
[ipareplicas:vars]
*ipaadmin_principal=my_admin*
*ipaadmin_password=my_admin_secret123*
....


+
[#example-playbook-password-2_{context}]
.Example playbook using principal and password from inventory file
[literal,subs="+quotes,attributes"]
....
- name: Playbook to configure IPA replicas
  hosts: ipareplicas
  become: true

  roles:
  - role: ipareplica
    state: present
....


--

[role="_additional-resources"]
.Additional resources
* For details on the options accepted by the `ipareplica` Ansible role, see the `/usr/share/ansible/roles/ipareplica/README.md` Markdown file.
