:_mod-docs-content-type: REFERENCE
[id="templates-for-managing-IdM-user-accounts-externally_{context}"]
= Templates for managing IdM user accounts externally

[role="_abstract"]
The following templates can be used for various user management operations in IdM. The templates show which attributes you must modify using `ldapmodify` to achieve the following goals:

* Adding a new stage user
* Modifying a user's attribute
* Enabling a user
* Disabling a user
* Preserving a user

The templates are formatted in the LDAP Data Interchange Format (LDIF). LDIF is a standard plain text data interchange format for representing LDAP directory content and update requests.

Using the templates, you can configure the LDAP provider of your provisioning system to manage IdM user accounts.

For detailed example procedures, see the following sections:

* xref:adding-an-IdM-stage-user-defined-in-an-LDIF-file_configuring-IdM-for-external-provisioning-of-users[Adding an IdM stage user defined in an LDIF file]
* xref:adding-an-IdM-stage-user-directly-from-the-CLI-using-ldapmodify_configuring-IdM-for-external-provisioning-of-users[Adding an IdM stage user directly from the CLI using ldapmodify]
* xref:preserving-an-IdM-user-with-ldapmodify_{context}[Preserving an IdM user with ldapmodify]

.Templates for adding a new stage user

* A template for adding a user with *UID and GID assigned automatically*. The distinguished name (DN) of the created entry must start with `uid=user_login`:

+
[literal,subs="+quotes,attributes,verbatim"]
....
dn: *uid=user_login*,cn=staged users,cn=accounts,cn=provisioning,dc=idm,dc=example,dc=com
changetype: add
objectClass: top
objectClass: inetorgperson
uid: user_login
sn: surname
givenName: first_name
cn: full_name
....

* A template for adding a user with *UID and GID assigned statically*:

+
....
dn: uid=user_login,cn=staged users,cn=accounts,cn=provisioning,dc=idm,dc=example,dc=com
changetype: add
objectClass: top
objectClass: person
objectClass: inetorgperson
objectClass: organizationalperson
objectClass: posixaccount
uid: user_login
uidNumber: UID_number
gidNumber: GID_number
sn: surname
givenName: first_name
cn: full_name
homeDirectory: /home/user_login
....

+
You are not required to specify any IdM object classes when adding stage users. IdM adds these classes automatically after the users are activated.


.Templates for modifying existing users


* *Modifying a user's attribute*:

+
....
dn: distinguished_name
changetype: modify
replace: attribute_to_modify
attribute_to_modify: new_value
....


* *Disabling a user*:

+
....
dn: distinguished_name
changetype: modify
replace: nsAccountLock
nsAccountLock: TRUE
....

* *Enabling a user*:

+
....
dn: distinguished_name
changetype: modify
replace: nsAccountLock
nsAccountLock: FALSE
....

+
Updating the `nssAccountLock` attribute has no effect on stage and preserved users. Even though the update operation completes successfully, the attribute value remains `nssAccountLock: TRUE`.

* *Preserving a user*:

+
....
dn: distinguished_name
changetype: modrdn
newrdn: uid=user_login
deleteoldrdn: 0
newsuperior: cn=deleted users,cn=accounts,cn=provisioning,dc=idm,dc=example,dc=com
....



[NOTE]
====
Before modifying a user, obtain the user's distinguished name (DN) by searching using the user's login. In the following example, the _user_allowed_to_modify_user_entries_ user is a user allowed to modify user and group information, for example *activator* or IdM administrator. The password in the example is this user's password:

[literal,subs="+quotes,attributes,verbatim"]
....
[...]
# *ldapsearch -LLL -x -D "uid=_user_allowed_to_modify_user_entries_,cn=users,cn=accounts,dc=idm,dc=example,dc=com" -w "Secret123" -H ldap://r8server.idm.example.com -b "cn=users,cn=accounts,dc=idm,dc=example,dc=com" uid=test_user*
dn: uid=test_user,cn=users,cn=accounts,dc=idm,dc=example,dc=com
memberOf: cn=ipausers,cn=groups,cn=accounts,dc=idm,dc=example,dc=com
....
====
