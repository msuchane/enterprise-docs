:_mod-docs-content-type: PROCEDURE
[id="adding-otp-tokens-in-the-web-ui_{context}"]
= Adding OTP tokens in the Web UI

[role="_abstract"]
The following section helps you to add token to the IdM Web UI and to your software token generator.

.Prerequisites

* Active user account on the IdM server.

* Administrator has enabled OTP for the particular user account in the IdM Web UI.

* A software device generating OTP tokens, for example FreeOTP.

.Procedure

. Log in to the IdM Web UI with your user name and password.

. To create the token in your mobile phone, open the *Authentication -> OTP Tokens* tab.

. Click *Add*.
+
image:web_ui_tokens_tab.png[Screenshot of the IdM Web UI highlighting the Add button near the upper-right of the OTP Tokens page which is a sub-page of the Authentication section]

. In the *Add OTP token* dialog box, leave everything unfilled and click *Add*.
+
At this stage, the IdM server creates a token with default parameters at the server and opens a page with a QR code.

. Copy the QR code into your mobile phone.

. Click *OK* to close the QR code.

Now you can generate one time passwords and log in with them to the IdM Web UI.

image:freeotp_token.png[Screenshot of the FreeOTP application from a mobile telephone displaying two entries for OTP tokens. The first OTP token is for the example.user@IDM.EXAMPLE.COM domain and its entry displays a 6-digit OTP while its timer is running out.]
