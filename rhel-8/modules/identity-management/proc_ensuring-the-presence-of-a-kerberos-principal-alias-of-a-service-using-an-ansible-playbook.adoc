:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-a-kerberos-principal-alias-of-a-service-using-an-ansible-playbook_{context}"]
= Ensuring the presence of a Kerberos principal alias of a service using an Ansible playbook

[role="_abstract"]
In some scenarios, it is beneficial for IdM administrator to enable IdM users, hosts, or services to authenticate against Kerberos applications using a Kerberos principal alias. These scenarios include:

* The user name changed, but the user should be able to log into the system using both the previous and new user names.
* The user needs to log in using the email address even if the IdM Kerberos realm differs from the email domain.

Follow this procedure to create the principal alias of *HTTP/mycompany.idm.example.com* for the HTTP service running on *client.idm.example.com*.

.Prerequisites

* You know the IdM administrator password.
* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
* You have
ifdef::c-m-idm[]
xref:setting-up-a-single-instance-apache-http-server_restricting-an-application-to-trust-a-subset-of-certs[set up an HTTP service]
endif::[]
ifndef::c-m-idm[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/restricting-an-application-to-trust-only-a-subset-of-certificates_configuring-and-managing-idm#setting-up-a-single-instance-apache-http-server_restricting-an-application-to-trust-a-subset-of-certs[set up an HTTP service] on your host.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/restricting-an-application-to-trust-only-a-subset-of-certificates_managing-certificates-in-idm#setting-up-a-single-instance-apache-http-server_restricting-an-application-to-trust-a-subset-of-certs[set up an HTTP service] on your host.
endif::[]
endif::[]

* You have
ifdef::c-m-idm[]
xref:ensuring-the-presence-of-an-http-service-in-idm-using-an-ansible-playbook_{context}[enrolled the HTTP service to IdM.]
endif::[]
ifndef::c-m-idm[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/ensuring-the-presence-and-absence-of-services-in-idm-using-ansible_configuring-and-managing-idm#ensuring-the-presence-of-an-http-service-in-idm-using-an-ansible-playbook_ensuring-the-presence-and-absence-of-services-in-idm-using-ansible[enrolled the HTTP service into IdM.]
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/ko-kr/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/ensuring-the-presence-and-absence-of-services-in-idm-using-ansible_using-ansible-to-install-and-manage-identity-management#ensuring-the-presence-of-an-http-service-in-idm-using-an-ansible-playbook_ensuring-the-presence-and-absence-of-services-in-idm-using-ansible[enrolled the HTTP service into IdM.]
endif::[]
endif::[]

* The host on which you have set up HTTP is an IdM client.


.Procedure


. Create an inventory file, for example `inventory.file`:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *touch inventory.file*
....

. Open the `inventory.file` and define the IdM server that you want to configure in the `[ipaserver]` section. For example, to instruct Ansible to configure *server.idm.example.com*, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....


. Make a copy of the `/usr/share/doc/ansible-freeipa/playbooks/service/service-member-principal-present.yml` Ansible playbook file. For example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/service/service-member-principal-present.yml /usr/share/doc/ansible-freeipa/playbooks/service/service-member-principal-present-copy.yml*
....

. Open the `/usr/share/doc/ansible-freeipa/playbooks/service/service-member-principal-present-copy.yml` Ansible playbook file for editing.

. Adapt the file by changing the following:

* The IdM administrator password specified by the `ipaadmin_password` variable.
* The name of the service specified by the `name` variable. This is the canonical principal name of the service. In the current example, it is *HTTP/client.idm.example.com*.
* The Kerberos principal alias specified by the `principal` variable. This is the alias you want to add to the service defined by the `name` variable. In the current example, it is *host/mycompany.idm.example.com*.
* The name of the task specified by the `name` variable in the `tasks` section.

+
After being adapted for the current example, the copied file looks like this:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Service member principal present
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Service HTTP/client.idm.example.com member principals host/mycompany.idm.exmaple.com present
    ipaservice:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: HTTP/client.idm.example.com*
      principal:
        - *host/mycompany.idm.example.com*
      action: member
....


. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i __path_to_inventory_directory__/inventory.file /usr/share/doc/ansible-freeipa/playbooks/service/service-member-principal-present-copy.yml*
....

If running the playbook results in 0 unreachable and 0 failed tasks, you have successfully created the *host/mycompany.idm.example.com* Kerberos principal for the *HTTP/client.idm.example.com* service.

[role="_additional-resources"]
.Additional resources
* See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/managing-kerberos-aliases[Managing Kerberos principal aliases for users, hosts, and services].
