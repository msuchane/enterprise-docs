:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/con_how-the-trust-works.adoc[leveloffset=+1]

[id="how-the-trust-works_{context}"]
= How the trust works

[role="_abstract"]
The trust between {IPA} IdM and {AD} (AD) is established on the Cross-realm Kerberos trust.
This solution uses the Kerberos capability to establish trusts between different identity sources.
Therefore, all AD users can:

* Log in to access Linux systems and resources.
* Use single sign-on (SSO).

All IdM objects are managed in IdM in the trust.

All AD objects are managed in AD in the trust.

In complex environments, a single IdM forest can be connected to multiple AD forests.
This setup enables better separation of duties for different functions in the organization.
AD administrators can focus on users and policies related to users while Linux administrators have full control over the Linux infrastructure.
In such a case, the Linux realm controlled by IdM is analogous to an AD resource domain or realm but with Linux systems in it.

From the perspective of AD, Identity Management represents a separate AD forest with a single AD domain.
When cross-forest trust between an AD forest root domain and an IdM domain is established,
users from the AD forest domains can interact with Linux machines and services from the IdM domain.

NOTE: In trust environments, IdM enables you to use ID views to configure POSIX attributes for AD users on the IdM server.

//For details, see: ***

////
.Additional resources

* A bulleted list of links to other material closely related to the contents of the concept module.
* For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
* Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
////
