:_mod-docs-content-type: PROCEDURE
[id="using-ansible-to-ensure-a-replication-agreement-exists-in-idm_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using Ansible to ensure a replication agreement exists in IdM
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
Data stored on an {IPA} (IdM) server is replicated based on replication agreements: when two servers have a replication agreement configured, they share their data.
Replication agreements are always bilateral: the data is replicated from the first replica to the other one as well as from the other replica to the first one.

Follow this procedure to use an Ansible playbook to ensure that a replication agreement of the `domain` type exists between *server.idm.example.com* and *replica.idm.example.com*.

.Prerequisites

* Ensure that you understand the recommendations for designing your IdM topology listed in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/planning_identity_management/planning-the-replica-topology_planning-identity-management#guidelines-for-connecting-idm-replicas-in-a-topology_planning-the-replica-topology[Guidelines for connecting IdM replicas in a topology].
* You know the IdM `admin` password.
* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. Navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Copy the `add-topologysegment.yml` Ansible playbook file located in the `/usr/share/doc/ansible-freeipa/playbooks/topology/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/topology/add-topologysegment.yml add-topologysegment-copy.yml*
....

. Open the `add-topologysegment-copy.yml` file for editing.

. Adapt the file by setting the following variables in the `ipatopologysegment` task section:

* Set the `ipaadmin_password` variable to the password of the IdM `admin`.
* Set the `suffix` variable to either `domain` or `ca`, depending on what type of segment you want to add.
* Set the `left` variable to the name of the IdM server that you want to be the left node of the replication agreement.
* Set the `right` variable to the name of the IdM server that you want to be the right node of the replication agreement.
* Ensure that the `state` variable is set to `present`.

+
--
This is the modified Ansible playbook file for the current example:

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to handle topologysegment
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
- name: Add topology segment
    ipatopologysegment:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *suffix: domain*
      *left: server.idm.example.com*
      *right: replica.idm.example.com*
      *state: present*
....
--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory add-topologysegment-copy.yml*
....

[role="_additional-resources"]
.Additional resources
* See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/assembly_managing-replication-topology_installing-identity-management#assembly_explaining-replication-agreements-topology-suffixes-and-topology-segments_assembly_managing-replication-topology[Explaining Replication Agreements, Topology Suffixes, and Topology Segments].
* See the `README-topology.md` file in the `/usr/share/doc/ansible-freeipa/` directory.
* See the sample playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/topology` directory.
