:_mod-docs-content-type: PROCEDURE
[id="creating-self-service-rules-using-the-cli_{context}"]
= Creating self-service rules using the CLI

[role="_abstract"]
Follow this procedure to create self-service access rules in IdM using the command-line interface (CLI).

.Prerequisites

* Administrator privileges for managing IdM or the *User Administrator* role.
* An active Kerberos ticket. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/logging-in-to-ipa-from-the-command-line_configuring-and-managing-idm#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/logging-in-to-ipa-from-the-command-line_accessing-idm-services#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]


.Procedure

* To add a self-service rule, use the [command]`ipa selfservice-add` command and specify the following two options:

`--permissions`:: sets the *read* and *write* permissions the Access Control Instruction (ACI) grants.
`--attrs`:: sets the complete list of attributes to which this ACI grants permission.

For example, to create a self-service rule allowing users to modify their own name details: +
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa selfservice-add "Users can manage their own name details" --permissions=write --attrs=givenname --attrs=displayname --attrs=title --attrs=initials
-----------------------------------------------------------
Added selfservice "Users can manage their own name details"
-----------------------------------------------------------
    Self-service name: Users can manage their own name details
    Permissions: write
    Attributes: givenname, displayname, title, initials
....
