:_mod-docs-content-type: PROCEDURE

[id="getting-help-for-ID-view-commands_{context}"]
= Getting help for ID view commands

[role="_abstract"]
You can get help for commands involving {IPA} (IdM) ID views on the IdM command-line interface (CLI).

.Prerequisites

* You have obtained a Kerberos ticket for an IdM user.


.Procedure

* To display all commands used to manage ID views and overrides:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa help idviews*
ID Views

Manage ID Views

IPA allows to override certain properties of users and groups[...]
[...]
Topic commands:
  idoverridegroup-add          Add a new Group ID override
  idoverridegroup-del          Delete a Group ID override
[...]
....

* To display detailed help for a particular command, add the [option]`--help` option to the command:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa idview-add --help*
Usage: ipa [global-options] idview-add NAME [options]

Add a new ID View.
Options:
  -h, --help      show this help message and exit
  --desc=STR      Description
[...]
....
