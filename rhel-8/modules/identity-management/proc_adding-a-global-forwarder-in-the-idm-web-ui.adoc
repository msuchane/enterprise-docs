:_mod-docs-content-type: PROCEDURE
// Module included in the following titles/assemblies:
//
// assembly_managing-dns-forwarding-in-idm.adoc

[id="adding-a-global-forwarder-in-the-idm-web-ui_{context}"]
= Adding a global forwarder in the IdM Web UI

[role="_abstract"]
Follow this procedure to add a global DNS forwarder in the Identity Management (IdM) Web UI.


.Prerequisites

* You are logged in to the IdM WebUI as IdM administrator.
* You know the Internet Protocol (IP) address of the DNS server to forward queries to.


.Procedure

. In the IdM Web UI, select `Network Services` -> `DNS Global Configuration` -> `DNS`.
+
image::idm-dns-global-add1.png[Selecting DNS Forward Zones from the DNS menu]

. In the `DNS Global Configuration` section, click `Add`.
+
image::idm-dns-global-add2.png[Selecting the Add button]

. Specify the IP address of the DNS server that will receive forwarded DNS queries.
+
image::idm-dns-global-add3.png[Entering the IP address of the global forwarder]

. Select the `Forward policy`.
+
image::idm-dns-global-add4.png[Selecting the DNS forward policy and saving the DNS global configuration]

. Click `Save` at the top of the window.

.Verification steps

. Select `Network Services` -> `DNS Global Configuration` -> `DNS`.
+
image::idm-dns-global-add1.png[Selecting DNS Global Configuration in IdM Web UI]

. Verify that the global forwarder, with the forward policy you specified, is present and enabled in the IdM Web UI.
+
image::idm-dns-global-add5.png[Verifying the presence of the global forwarder]
