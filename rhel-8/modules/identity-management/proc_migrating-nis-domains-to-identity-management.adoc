:_mod-docs-content-type: PROCEDURE

[id="proc_migrating-nis-domains-to-identity-management_{context}"]
= Migrating NIS domains to Identity Management

[role="_abstract"]
You can use ID views to set host specific UIDs and GIDs for existing hosts to prevent changing permissions for files and directories when migrating NIS domains into IdM.

.Prerequisites
* You authenticated yourself as an admin using the `kinit admin` command.

.Procedure

. Add users and groups in the IdM domain.
  .. Create users using the `ipa user-add` command. For more information see: xref:using-an-ipa-command-to-add-a-user-account-to-idm_introduction-to-the-ipa-command-line-utilities[Adding users to IdM].
  .. Create groups using the `ipa group-add` command. For more information see: xref:adding-a-user-group-using-idm-cli_managing-user-groups-in-idm-cli[Adding groups to IdM].
. Override IDs IdM generated during the user creation:
  .. Create a new ID view using `ipa idview-add` command. For more information see: xref:getting-help-for-ID-view-commands_using-an-id-view-to-override-a-user-attribute-value-on-an-IdM-client[Getting help for ID view commands].
  .. Add ID overrides for the users and groups to the ID view using `ipa idoverrideuser-add` and `idoverridegroup-add` respectively.
. Assign the ID view to the specific hosts using `ipa idview-apply` command.
. Decommission the NIS domains.

.Verification
. To check if all users and groups were added to the ID view correctly, use the `ipa idview-show` command.
+
[literal]
----
$ ipa idview-show example-view
  ID View Name: example-view
  User object overrides: example-user1
  Group object overrides: example-group
----
