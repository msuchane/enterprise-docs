:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="rebuilding-automatic-membership-for-a-single-user-or-host-only_{context}"]
= Rebuilding automatic membership for a single user or host only

[role="_abstract"]
Follow this procedure to rebuild automatic membership for a specific user or host entry.

.Prerequisites
* You are logged in to the IdM Web UI.
* You must be a member of the `admins` group.

.Procedure
. Select *Identity* → *Users* or *Hosts*.
. Click on the required user or host name.
. Click *Actions* → *Rebuild auto membership*.
+
image::automember-rebuild-single.png[A screenshot highlighting the "Rebuild auto membership" option among many others in the contents of the "Actions" drop-down menu.]
