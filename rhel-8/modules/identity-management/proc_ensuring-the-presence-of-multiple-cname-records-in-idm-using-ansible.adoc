:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-multiple-cname-records-in-idm-using-ansible_{context}"]
= Ensuring the presence of multiple CNAME records in IdM using Ansible

[role="_abstract"]
A Canonical Name record (CNAME record) is a type of resource record in the Domain Name System (DNS) that maps one domain name, an alias, to another name, the canonical name.

You may find CNAME records useful when running multiple services from a single IP address: for example, an FTP service and a web service, each running on a different port.

Follow this procedure to use an Ansible playbook to ensure that multiple CNAME records are present in IdM DNS. In the example used in the procedure below, *host03* is both an HTTP server and an FTP server. The IdM administrator ensures the presence of the *www* and *ftp* CNAME records for the *host03* A record in the *idm.example.com* zone.


.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
* You know the IdM administrator password.
* The *idm.example.com* zone exists and is managed by IdM DNS. For more information about adding a primary DNS zone in IdM DNS, see xref:using-ansible-playbooks-to-manage-idm-dns-zones_{parent-context-of-using-ansible-playbooks-to-manage-idm-dns-zones}[Using Ansible playbooks to manage IdM DNS zones].
* The *host03* A record exists in the *idm.example.com* zone.


.Procedure

. Navigate to the `/usr/share/doc/ansible-freeipa/playbooks/dnsrecord` directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd /usr/share/doc/ansible-freeipa/playbooks/dnsrecord*
....


. Open your inventory file and ensure that the IdM server that you want to configure is listed in the `[ipaserver]` section. For example, to instruct Ansible to configure *server.idm.example.com*, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....


. Make a copy of the *ensure-CNAME-record-is-present.yml* Ansible playbook file. For example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp ensure-CNAME-record-is-present.yml ensure-CNAME-record-is-present-copy.yml*
....

. Open the *ensure-CNAME-record-is-present-copy.yml* file for editing.

. Adapt the file by setting the following variables in the `ipadnsrecord` task section:

* (Optional) Adapt the description provided by the `name` of the play.
* Set the `ipaadmin_password` variable to your IdM administrator password.
* Set the `zone_name` variable to *idm.example.com*.
* In the `records` variable section, set the following variables and values:
** Set the `name` variable to *www*.
** Set the `cname_hostname` variable to *host03*.
** Set the `name` variable to *ftp*.
** Set the `cname_hostname` variable to *host03*.

+
This is the modified Ansible playbook file for the current example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Ensure that 'www.idm.example.com' and 'ftp.idm.example.com' CNAME records point to 'host03.idm.example.com'.
  hosts: ipaserver
  become: true
  gather_facts: false

  tasks:
  - ipadnsrecord:
      ipaadmin_password: "{{ ipaadmin_password }}"
      zone_name: idm.example.com
      records:
      - name: www
        cname_hostname: host03
      - name: ftp
        cname_hostname: host03
....

. Save the file.

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory.file ensure-CNAME-record-is-present.yml*
....

[role="_additional-resources"]
.Additional resources
* See the `README-dnsrecord.md` file in the `/usr/share/doc/ansible-freeipa/` directory.
* See sample Ansible playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/dnsrecord` directory.
