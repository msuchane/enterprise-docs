:_mod-docs-content-type: PROCEDURE
// This module included in the following Assemblies:
//
// assembly_troubleshooting-idm-replica-installation.adoc

:experimental:

[id="reviewing-idm-replica-installation-errors_{context}"]
= Reviewing IdM replica installation errors

[role="_abstract"]
To troubleshoot a failing IdM replica installation, review the errors at the end of the installation error log files on the new replica and the server, and use this information to resolve any corresponding issues.

.Prerequisites

* You must have `root` privileges to display the contents of IdM log files.


.Procedure

. Use the `tail` command to display the latest errors from the primary log file [filename]`/var/log/ipareplica-install.log`. The following example displays the last 10 lines.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[user@replica ~]$ *sudo tail -n 10 /var/log/ipareplica-install.log*
[sudo] password for user:
  func(installer)
File "/usr/lib/python3.6/site-packages/ipaserver/install/server/replicainstall.py", line 424, in decorated
  func(installer)
File "/usr/lib/python3.6/site-packages/ipaserver/install/server/replicainstall.py", line 785, in promote_check
  ensure_enrolled(installer)
File "/usr/lib/python3.6/site-packages/ipaserver/install/server/replicainstall.py", line 740, in ensure_enrolled
  raise ScriptError("Configuration of client side components failed!")

2020-05-28T18:24:51Z *DEBUG The ipa-replica-install command failed, exception: ScriptError: Configuration of client side components failed!*
2020-05-28T18:24:51Z *ERROR Configuration of client side components failed!*
2020-05-28T18:24:51Z *ERROR The ipa-replica-install command failed. See /var/log/ipareplica-install.log for more information*
....

. To review the log file interactively, open the end of the log file using the `less` utility and use the kbd:[↑] and kbd:[↓] arrow keys to navigate.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[user@replica ~]$ *sudo less -N +G /var/log/ipareplica-install.log*
....

. (Optional) While [filename]`/var/log/ipareplica-install.log` is the primary log file for a replica installation, you can gather additional troubleshooting information by repeating this review process with additional files on the replica and the server.
+
.On the replica:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[user@replica ~]$ *sudo less -N +G /var/log/ipareplica-conncheck.log*
[user@replica ~]$ *sudo less -N +G /var/log/ipaclient-install.log*
[user@replica ~]$ *sudo less -N +G /var/log/httpd/error_log*
[user@replica ~]$ *sudo less -N +G /var/log/dirsrv/slapd-_INSTANCE-NAME_/access*
[user@replica ~]$ *sudo less -N +G /var/log/dirsrv/slapd-_INSTANCE-NAME_/errors*
[user@replica ~]$ *sudo less -N +G /var/log/ipaserver-install.log*
....
+
.On the server:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[user@server ~]$ *sudo less -N +G /var/log/httpd/error_log*
[user@server ~]$ *sudo less -N +G /var/log/dirsrv/slapd-_INSTANCE-NAME_/access*
[user@server ~]$ *sudo less -N +G /var/log/dirsrv/slapd-_INSTANCE-NAME_/errors*
....

[role="_additional-resources"]
.Additional resources
* xref:ref_idm-replica-installation-error-log-files_troubleshooting-idm-replica-installation[IdM replica installation error log files]
* If you are unable to resolve a failing replica installation, and you have a Red Hat Technical Support subscription, open a Technical Support case at the link:https://access.redhat.com/support/cases/#/[Red Hat Customer Portal] and provide an `sosreport` of the replica and an `sosreport` of the server.
* The `sosreport` utility collects configuration details, logs and system information from a RHEL system. For more information about the `sosreport` utility, see link:https://access.redhat.com/solutions/3592[What is an sosreport and how to create one in Red Hat Enterprise Linux?].
