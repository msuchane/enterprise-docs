:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="viewing-existing-delegation-rules-using-idm-webui_{context}"]
= Viewing existing delegation rules using IdM WebUI

[role="_abstract"]
Follow this procedure to view existing delegation rules using the IdM WebUI.

.Prerequisites

* You are logged in to the IdM Web UI as a member of the `admins` group.

.Procedure

* From the *IPA Server* menu, click *Role-Based Access Control* → *Delegations*.
+
image::delegation-list.png[A screenshot of the IdM Web UI displaying the "Delegations" page from the "Role-Based Access Control" submenu of the "IPA Server" tab. There is a table displaying Delegations organized by their "Delegation name."]
