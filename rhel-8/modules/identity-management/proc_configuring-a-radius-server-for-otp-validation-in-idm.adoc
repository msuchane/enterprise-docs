:_newdoc-version: 2.15.1
:_template-generated: 2024-02-08
:_mod-docs-content-type: PROCEDURE

[id="configuring-a-radius-server-for-otp-validation-in-idm_{context}"]
= Configuring a RADIUS server for OTP validation in IdM

[role="_abstract"]
To enable the migration of a large deployment from a proprietary one-time password (OTP) solution to the {IPA} (IdM)-native OTP solution, IdM offers a way to offload OTP validation to a third-party RADIUS server for a subset of users. The administrator creates a set of RADIUS proxies where each proxy can only reference a single RADIUS server. If more than one server needs to be addressed, it is recommended to create a virtual IP solution that points to multiple RADIUS servers. 

Such a solution must be built outside of RHEL IdM with the help of the `keepalived` daemon, for example. The administrator then assigns one of these proxy sets to a user. As long as the user has a RADIUS proxy set assigned, IdM bypasses all other authentication mechanisms.

[NOTE]
====
IdM does not provide any token management or synchronization support for tokens in the third-party system.
====

Complete the procedure to configure a RADIUS server for OTP validation and to add a user to the proxy server:

.Prerequisites

* The radius user authentication method is enabled. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/accessing_identity_management_services/index#enabling-the-one-time-password-in-the-web-ui_logging-in-to-ipa-in-the-web-ui-using-a-password[Enabling the one-time password in the Web UI] for details.

.Procedure

. Add a RADIUS proxy:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ *ipa radiusproxy-add proxy_name --secret secret*
....
+
The command prompts you for inserting the required information.
+
The configuration of the RADIUS proxy requires the use of a common secret between the client and the server to wrap credentials. Specify this secret in the `+--secret+` parameter.

. Assign a user to the added proxy:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
*ipa user-mod radiususer --radius=proxy_name*
....

. If required, configure the user name to be sent to RADIUS:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
*ipa user-mod radiususer --radius-username=radius_user*
....

As a result, the RADIUS proxy server starts to process the user OTP authentication.

When the user is ready to be migrated to the IdM native OTP system, you can simply remove the RADIUS proxy assignment for the user.

== Changing the timeout value of a KDC when running a RADIUS server in a slow network

In certain situations, such as running a RADIUS proxy in a slow network, the {IPA} (IdM) Kerberos Distribution Center (KDC) closes the connection before the RADIUS server responds because the connection timed out while waiting for the user to enter the token.

To change the timeout settings of the KDC:

. Change the value of the `+timeout+` parameter in the `+[otp]+` section in the `+/var/kerberos/krb5kdc/kdc.conf+` file. For example, to set the timeout to `+120+` seconds:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[otp]
DEFAULT = {
  timeout = 120
  ...
}
....

. Restart the `+krb5kdc+` service:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *systemctl restart krb5kdc*
....


[role="_additional-resources"]
.Additional resources

* The link:https://access.redhat.com/solutions/4650511[How to configure FreeRADIUS authentication in FIPS mode] Knowledgebase article
