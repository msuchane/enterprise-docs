:_mod-docs-content-type: CONCEPT
// Module included in the following titles/assemblies:
//
// title planning/preparing-for-disaster-recovery-with-identity-management/
// title configuring-and-maintaining/performing-disaster-recovery-with-identity-management/


[id="disaster-scenarios-in-idm_{context}"]
= Disaster scenarios in IdM

[role="_abstract"]
There are two main classes of disaster scenarios: _server loss_ and _data loss_.

.Server loss vs. data loss
|===
|Disaster type |Example causes |
ifeval::["{context}" == "preparing-for-disaster-recovery"]
How to prepare
endif::[]
ifeval::["{context}" == "performing-disaster-recovery"]
How to respond
endif::[]


|*Server loss*: The IdM deployment loses one or several servers.
a|
* Hardware malfunction
a|
ifeval::["{context}" == "preparing-for-disaster-recovery"]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/preparing_for_disaster_recovery_with_identity_management/preparing-for-server-loss-with-replication_preparing-for-disaster-recovery[Preparing for server loss with replication]
endif::[]
ifeval::["{context}" == "performing-disaster-recovery"]
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_disaster_recovery_with_identity_management/recovering-a-single-server-with-replication_performing-disaster-recovery[Recovering a single server with replication]
endif::[]
|*Data loss*: IdM data is unexpectedly modified on a server, and the change is propagated to other servers.
a|
* A user accidentally deletes data
* A software bug modifies data
a|
ifeval::["{context}" == "preparing-for-disaster-recovery"]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/preparing_for_disaster_recovery_with_identity_management/preparing-for-data-loss-with-snapshots_preparing-for-disaster-recovery[Preparing for data loss with VM snapshots]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/preparing_for_disaster_recovery_with_identity_management/preparing-for-data-loss-with-idm-backups_preparing-for-disaster-recovery[Preparing for data loss with IdM backups]
endif::[]
ifeval::["{context}" == "performing-disaster-recovery"]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_disaster_recovery_with_identity_management/recovering-from-data-loss-with-snapshots_performing-disaster-recovery[Recovering from data loss with VM snapshots]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_disaster_recovery_with_identity_management/recovering-from-data-loss-with-backups_performing-disaster-recovery[Recovering from data loss with IdM backups]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_disaster_recovery_with_identity_management/managing-data-loss_performing-disaster-recovery[Managing data loss]
endif::[]
|===
