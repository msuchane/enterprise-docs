:_mod-docs-content-type: PROCEDURE
[id="proc_adjusting-how-sssd-interprets-full-user-names_{context}"]
= Adjusting how SSSD interprets full user names
//= Parsing full user names

[role="_abstract"]
SSSD parses full user name strings into the user name and domain components. By default, SSSD interprets full user names in the format `user_name@domain_name` based on the following regular expression in Python syntax:

....
(?P<name>[^@]+)@?(?P<domain>[^@]*$)
....

[NOTE]
====
For Identity Management and Active Directory providers, the default user name format is `user_name@domain_name` or `NetBIOS_name\user_name`.
====

You can adjust how SSSD interprets full user names by adding the `re_expression` option to the `/etc/sssd/sssd.conf` file and defining a custom regular expression.

* To define the regular expression globally, add the regular expression to the `[sssd]` section of the `sssd.conf` file as shown in the xref:defining-regular-expressions-globally_{context}[Defining regular expressions globally] example.
* To define the regular expression for a particular domain, add the regular expression to the corresponding domain section (for example, `[domain/LDAP]`) of the `sssd.conf` file as shown in the xref:defining-regular-expressions-for-a-particular-domain_{context}[Defining regular expressions a particular domain] example.

.Prerequisites
* `root` access

.Procedure

. Open the `/etc/sssd/sssd.conf` file.

. Use the `re_expression` option to define a custom regular expression.
+
[id="defining-regular-expressions-globally_{context}"]
.Defining regular expressions globally
====
To define the regular expressions globally for all domains, add `re_expression` to the `[sssd]` section of the `sssd.conf` file.

You can use the following global expression to define the username in the format of `domain\\username` or `domain@username`:

....
[sssd]
[... file truncated ...]
re_expression = (?P<domain>[^\\]*?)\\?(?P<name>[^\\]+$)
....

====
+
[id="defining-regular-expressions-for-a-particular-domain_{context}"]
.Defining regular expressions a particular domain
====
To define the regular expressions individually for a particular domain, add `re_expression` to the corresponding domain section of the `sssd.conf` file.

You can use the following global expression to define the username in the format of `domain\\username` or `domain@username` for the LDAP domain:
....
[domain/LDAP]
[... file truncated ...]
re_expression = (?P<domain>[^\\]*?)\\?(?P<name>[^\\]+$)
....
====

For more details, see the descriptions for `re_expression` in the `SPECIAL SECTIONS` and `DOMAIN SECTIONS` parts of the `sssd.conf(5)` man page.

////
.Verification

[role="_additional-resources"]
.Additional resources
////
