:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// searching-ipa-entries-from-the-command-line.adoc
// assembly_adjusting-the-search-size-and-time-limit.adoc

:experimental:

[id="adjusting-the-search-size-and-time-limit-in-the-command-line_{context}"]
= Adjusting the search size and time limit in the command line

[role="_abstract"]
The following procedure describes adjusting search size and time limits in the command line:

* Globally
* For a specific entry

.Procedure

. To display current search time and size limits in CLI, use the `ipa config-show` command:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa config-show*

Search time limit: 2
Search size limit: 100
....

. To adjust the limits *globally* for all queries, use the [command]`ipa config-mod` command and add the [option]`--searchrecordslimit` and [option]`--searchtimelimit` options. For example:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa config-mod --searchrecordslimit=500 --searchtimelimit=5*
....
+
. To *temporarily* adjust the limits only for a specific query, add the [option]`--sizelimit` or [option]`--timelimit` options to the command. For example:

[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa user-find --sizelimit=200 --timelimit=120*
....
