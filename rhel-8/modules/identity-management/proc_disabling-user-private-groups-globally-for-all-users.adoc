:_mod-docs-content-type: PROCEDURE
[id="disabling-user-private-groups-globally-for-all-users_{context}"]
= Disabling user private groups globally for all users

[role="_abstract"]
You can disable user private groups (UPGs) globally. This prevents the creation of UPGs for all new users. Existing users are unaffected by this change.

.Procedure

. Obtain administrator privileges:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ kinit admin
....

. IdM uses the Directory{nbsp}Server Managed Entries Plug-in to manage UPGs. List the instances of the plug-in:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ ipa-managed-entries --list
....

. To ensure IdM does not create UPGs, disable the plug-in instance responsible for managing user private groups:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ ipa-managed-entries -e "UPG Definition" disable
Disabling Plugin
....
+
[NOTE]
====

To re-enable the `UPG Definition` instance later, use the [command]`ipa-managed-entries -e "UPG Definition" enable` command.

====

. Restart Directory{nbsp}Server to load the new configuration.
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ sudo systemctl restart dirsrv.target
....
+
To add a user after UPGs have been disabled, you need to specify a GID. For more information, see xref:adding-a-user-when-user-private-groups-are-globally-disabled_{context}[Adding a user when user private groups are globally disabled]

.Verification steps

* To check if UPGs are globally disabled, use the disable command again:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ ipa-managed-entries -e "UPG Definition" disable
Plugin already disabled
....
