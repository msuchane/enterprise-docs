:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_renewing-expired-system-certificates-when-idm-is-offline.adoc

[id="verifying-other-idm-servers-in-the-domain-after-renewal_{context}"]
= Verifying other IdM servers in the IdM domain after renewal

[role="_abstract"]
After the renewing the CA renewal server's certificates with the `ipa-cert-fix` tool, you must:

* Restart all other Identity Management (IdM) servers in the domain.
* Check if certmonger renewed certificates.
* If there are other Certificate Authority (CA) replicas with expired system certificates, renew those certificates with the `ipa-cert-fix` tool as well.

.Prerequisites

* Log in to the server with administration rights.

.Procedure

. Restart IdM with the `--force` parameter:
+
[literal]
--
# ipactl restart --force
--
+
With the `--force` parameter, the `ipactl` utility ignores individual service startup failures.
For example, if the server is also a CA with expired certificates, the `pki-tomcat` service fails to start.
This is expected and ignored because of using the `--force` parameter.

. After the restart, verify that the `certmonger` service renewed the certificates (certificate status says MONITORING):
+
[literal]
--
# getcert list | egrep '^Request|status:|subject:'
Request ID '20190522120745':
        status: MONITORING
        subject: CN=IPA RA,O=EXAMPLE.COM 201905222205
Request ID '20190522120834':
        status: MONITORING
        subject: CN=Certificate Authority,O=EXAMPLE.COM 201905222205
...
--
+
It can take some time before `certmonger` renews the shared certificates on the replica.

. If the server is also a CA, the previous command reports `CA_UNREACHABLE` for the certificate the `pki-tomcat` service uses:
+
[literal]
--
Request ID '20190522120835':
        status: CA_UNREACHABLE
        subject: CN=ca2.example.com,O=EXAMPLE.COM 201905222205
...
--

. To renew this certificate, use the `ipa-cert-fix` utility:
+
[literal]
--
# ipa-cert-fix
Dogtag sslserver certificate:
  Subject: CN=ca2.example.com,O=EXAMPLE.COM
  Serial:  3
  Expires: 2019-05-11 12:07:11

Enter "yes" to proceed: yes
Proceeding.
Renewed Dogtag sslserver certificate:
  Subject: CN=ca2.example.com,O=EXAMPLE.COM 201905222205
  Serial:  15
  Expires: 2019-08-14 04:25:05

The ipa-cert-fix command was successful
--

Now, all IdM certificates have been renewed and work correctly.
