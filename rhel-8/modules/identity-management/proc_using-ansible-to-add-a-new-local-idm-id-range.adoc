
:_mod-docs-content-type: PROCEDURE

[id="proc_using-ansible-to-add-a-new-local-idm-id-range_{context}"]
= Using Ansible to add a new local IdM ID range


[role="_abstract"]
In some cases, you may want to create a new {IPA} (IdM) ID range in addition to the original one; for example, when a replica has run out of IDs and the original IdM ID range is depleted. The following example describes how to create a new IdM ID range by using an Ansible playbook.

NOTE: Adding a new IdM ID range does not create new DNA ID ranges automatically. You need to assign new DNA ID ranges manually as needed. For more information about how to do this, see xref:proc_assigning-dna-id-ranges-manually_{context}[Assigning DNA ID ranges manually].

.Prerequisites

* You know the IdM `admin` password.
* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. Navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Create the `idrange-present.yml` playbook with the following content:
+
[literal,subs="+quotes,attributes",options="nowrap",role=white-space-pre]
....
---
- name: Playbook to manage idrange
  hosts: ipaserver
  become: no

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure local idrange is present
    ipaidrange:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: new_id_range
      base_id: 12000000
      range_size: 200000
      rid_base: 1000000
      secondary_rid_base: 200000000
....


. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory idrange-present.yml*
....

. `SSH` to `ipaserver` and restart the Directory Server:

+
[literal,subs="+quotes,attributes"]
--
# *systemctl restart dirsrv@IDM.EXAMPLE.COM.service*
--

+
This ensures that when you create users with UIDs from the new range, they have security identifiers (SIDs) assigned.

. Optional: Update the ID range immediately:

.. On `ipaserver`, clear the System Security Services Daemon (SSSD) cache:

+
[literal,subs="+quotes,attributes"]
--
# *sss_cache -E*
--

.. On `ipaserver`, restart the SSSD daemon:

+
[literal,subs="+quotes,attributes"]
--
# *systemctl restart sssd*
--


+
[NOTE]
====
If you do not clear the SSSD cache and restart the service, SSSD only detects the new ID range when it updates the domain list and other configuration data stored on the IdM server.
====


.Verification steps

* You can check if the new range is set correctly by using the [command]`ipa idrange-find` command:

[literal,subs="+quotes,attributes"]
--
# *ipa idrange-find*
----------------
2 ranges matched
----------------
  Range name: IDM.EXAMPLE.COM_id_range
  First Posix ID of the range: 882200000
  Number of IDs in the range: 200000
  Range type: local domain range

  *Range name: IDM.EXAMPLE.COM_new_id_range*
  *First Posix ID of the range: 12000000*
  *Number of IDs in the range: 200000*
  *Range type: local domain range*
----------------------------
Number of entries returned 2
----------------------------
--



[role="_additional-resources"]
.Additional resources
////
Optional. Delete if not used.
////
* xref:con_the-role-of-security-and-relative-identifiers-in-idm-id-ranges_{context}[The role of security and relative identifiers in IdM ID ranges]
