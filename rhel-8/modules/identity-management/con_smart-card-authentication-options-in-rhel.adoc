:_mod-docs-content-type: CONCEPT


[id="con_smart-card-authentication-options-in-rhel_{context}"]
= Smart card authentication options in RHEL

You can configure how you want smart card authentication to work in a particular {IPA} (IdM) client by using the `authselect` command, `authselect enable-feature <smartcardoption>`. The following smart card options are available:

* `with-smartcard`: Users can authenticate with the user name and password or with their smart card.
* `with-smartcard-required`: Users can authenticate with their smart cards, and password authentication is disabled. You cannot access the system without your smart card. Once you have authenticated with your smart card, you can stay logged in even if your smart card is removed from its reader.
+
[NOTE]
====
The `with-smartcard-required` option only enforces exclusive smart card authentication for login services, such as `login`, `gdm`, `xdm`, `xscreensaver`, and `gnome-screensaver`. For other services, such as `su` or `sudo` for switching users, smart card authentication is not enforced and if your smart card is not inserted, you are prompted for a password.
====

* `with-smartcard-lock-on-removal`: Users can authenticate with their smart card. However, if you remove your smart card from its reader, you are automatically locked out of the system. You cannot use password authentication.
+
[NOTE]
====
The `with-smartcard-lock-on-removal` option only works on systems with the GNOME desktop environment. If you are using a system that is `tty` or console based and you remove your smart card from its reader, you are not automatically locked out of the system.
====

For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_smart_card_authentication/configuring-smart-cards-using-authselect_managing-smart-card-authentication[Configuring smart cards using authselect].
