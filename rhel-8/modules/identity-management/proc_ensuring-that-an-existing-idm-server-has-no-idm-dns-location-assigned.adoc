:_mod-docs-content-type: PROCEDURE
[id="ensuring-that-an-existing-idm-server-has-no-idm-dns-location-assigned_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Ensuring that an existing IdM server has no IdM DNS location assigned

[role="_abstract"]
Use the `ipaserver` `ansible-freeipa` module in an Ansible playbook to ensure that an existing {IPA} (IdM) server has no IdM DNS location assigned to it. Do not assign a DNS location to servers that change geographical location frequently. Note that the playbook does not install the IdM server.

.Prerequisites

* You know the IdM `admin` password.
* You have `root` access to the server. The example server is *server123.idm.example.com*.
* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
** The `SSH` connection from the control node to the IdM server defined in the inventory file is working correctly.


.Procedure

. Navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Copy the `server-no-location.yml` Ansible playbook file located in the `/usr/share/doc/ansible-freeipa/playbooks/server/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/server/server-no-location.yml server-no-location-copy.yml*
....

. Open the `server-no-location-copy.yml` file for editing.

. Adapt the file by setting the following variables in the `ipaserver` task section and save the file:

* Set the `ipaadmin_password` variable to the password of the IdM `admin`.
* Set the `name` variable to *server123.idm.example.com*.
* Ensure that the `location` variable is set to *””*.

+
--

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Server no location example
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure server server123.idm.example.com is present with no location
    ipaserver:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: server123.idm.example.com*
      *location: “”*
....
--

. Run the Ansible playbook and specify the playbook file and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory server-no-location-copy.yml*
....

. Connect to *server123.idm.example.com* as `root` using `SSH`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
*ssh root@server123.idm.example.com*
....


. Restart the `named-pkcs11` service on the server for the updates to take effect immediately:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server123.idm.example.com ~]# *systemctl restart named-pkcs11*
....



[role="_additional-resources"]
.Additional resources
* See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management[Installing an Identity Management server using an Ansible playbook].
* See xref:using-ansible-to-manage-dns-locations-in-idm_{ProjectNameID}[Using Ansible to manage DNS locations in IdM].
* See the `README-server.md` file in the `/usr/share/doc/ansible-freeipa/` directory.
* See sample playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/server` directory.
