:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="modifying-a-delegation-rule-using-idm-webui_{context}"]
= Modifying a delegation rule using IdM WebUI

[role="_abstract"]
Follow this procedure to modify an existing delegation rule using the IdM WebUI.

.Prerequisites

* You are logged in to the IdM Web UI as a member of the `admins` group.

.Procedure

. From the *IPA Server* menu, click *Role-Based Access Control* → *Delegations*.
+
image::delegation-list.png[A screenshot of the IdM Web UI displaying the Role-Based Access Control sub page from the IPA Server tab. The Delegations page displays a table with an entry for the "basic manager attributes" Delegation name.]

. Click on the rule you want to modify.
. Make the desired changes:
* Change the name of the rule.
* Change granted permissions by selecting the check boxes that indicate whether users will have the right to view the given attributes (_read_) and add or change the given attributes (_write_).
* In the User group drop-down menu, select the group _who is being granted permissions_ to view or edit the entries of users in the member group.
* In the *Member user group* drop-down menu, select the group _whose entries can be edited_ by members of the delegation group.
* In the attributes box, select the check boxes by the attributes to which you want to grant permissions. To remove permissions to an attribute, uncheck the relevant check box.
+
image::delegation-modify.png[The Delegation page displays details of the "basic manager attributes" Delegation such as the Delegation name - Permissions (which is required - such as "read" and "write") - User group (required such as "managers") - Member user group (required such as "employees") and Attributes (required such as employeetype - businesscategory - departmentnumber - displayname - employeenumber - homedirectory). The "save" button at the top left is highlighted.]
+
* Click the *Save* button to save the changes.
