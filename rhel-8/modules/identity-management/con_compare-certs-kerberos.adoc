:_mod-docs-content-type: CONCEPT
//:parent-context: {context}
[[compare-certs-kerberos_{context}]]

= Comparison of certificates and Kerberos

[role="_abstract"]
Certificates perform a similar function to that performed by Kerberos tickets. Kerberos is a computer network authentication protocol that works on the basis of tickets to allow nodes communicating over a non-secure network to prove their identity to one another in a secure manner. The following table shows a comparison of Kerberos and X.509 certificates:


[id="tab-compare-cert-kerberos_{context}"]
.Comparison of certificates and Kerberos
[cols="2,2,2"]
|===
|*Characteristic*
|*Kerberos*
|*X.509*

|`Authentication`
|Yes
|Yes

|`Privacy`
|Optional
|Yes

|`Integrity`
|Optional
|Yes

|`Type of cryptography involved`
|Symmetrical
|Asymmetrical

|`Default validity`
|Short (1 day)
|Long(2 years)
|===

By default, Kerberos in Identity Management only ensures the identity of the communicating parties.

//:context: {parent-context}
