:_mod-docs-content-type: PROCEDURE
[id="using-ansible-to-ensure-a-service-is-a-member-of-an-idm-rbac-role_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using Ansible to ensure a service is a member of an IdM RBAC role

[role="_abstract"]
As a system administrator managing role-based access control (RBAC) in {IPA} (IdM), you may want to ensure that a specific service that is enrolled into IdM is a member of a particular role.
The following example describes how to ensure that the custom *web_administrator* role can manage the `HTTP` service that is running on the *client01.idm.example.com* server.

.Prerequisites

* You know the IdM administrator password.
* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
* The *web_administrator* role exists in IdM.
* The *HTTP/client01.idm.example.com@IDM.EXAMPLE.COM* service exists in IdM.


.Procedure

. Navigate to the *~/__<MyPlaybooks>__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__<MyPlaybooks>__/*
....

. Make a copy of the `role-member-service-present.yml` file located in the `/usr/share/doc/ansible-freeipa/playbooks/role/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/role/role-member-service-present-absent.yml role-member-service-present-copy.yml*
....

. Open the `role-member-service-present-copy.yml` Ansible playbook file for editing.

. Adapt the file by setting the following variables in the `iparole` task section:

* Set the `ipaadmin_password` variable to the password of the IdM administrator.
* Set the `name` variable to the name of the role you want to assign.
* Set the `service` list to the name of the service.
* Set the `action` variable to `member`.

+
--
This is the modified Ansible playbook file for the current example:

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to manage IPA role with members.
  hosts: ipaserver
  become: true
  gather_facts: no

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - iparole:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: web_administrator*
      *service:*
      *- HTTP/client01.idm.example.com*
      *action: member*
....
--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i ~/__<MyPlaybooks>__/inventory role-member-service-present-copy.yml*
....

[role="_additional-resources"]
.Additional resources
* See link:https://docs.ansible.com/ansible/latest/user_guide/vault.html[Encrypting content with Ansible Vault].
* See xref:roles-in-idm_{context}[Roles in IdM].
* See the `README-role` Markdown file in the `/usr/share/doc/ansible-freeipa/` directory.
* See the sample playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/iparole` directory.
