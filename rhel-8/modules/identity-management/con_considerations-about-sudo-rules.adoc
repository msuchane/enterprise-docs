[id="considerations-about-sudo-rules_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Considerations about sudo rules

If you are using `sudo` with LDAP, you must migrate the `sudo` rules stored in LDAP to {IPA} (IdM) manually. Red{nbsp}Hat recommends that you recreate netgroups in IdM as hostgroups. IdM presents hostgroups automatically as traditional netgroups for `sudo` configurations that do not use the SSSD `sudo` provider.
