:_mod-docs-content-type: CONCEPT
//:parent-context: {context}
// Module included in the following assemblies:
//
// starting-and-stopping-the-ipa-server.adoc

[id='starting-and-stopping-an-individual-ipa-service-the-systemctl-utility_{context}']
= Starting and stopping an individual Identity Management service

[role="_abstract"]
Changing IdM configuration files manually is generally not recommended. However, certain situations require that an administrator performs a manual configuration of specific services. In such situations, use the `systemctl` utility to stop, start, or restart an individual IdM service.

For example, use `systemctl` after customizing the {DS} behavior, without modifying the other IdM services:

[literal,subs="+quotes,attributes,verbatim"]
....
# `systemctl restart dirsrv@REALM-NAME.service`
....


Also, when initially deploying an IdM trust with Active Directory, modify the `/etc/sssd/sssd.conf` file, adding:

* Specific parameters to tune the timeout configuration options in an environment where remote servers have a high latency
* Specific parameters to tune the Active Directory site affinity
* Overrides for certain configuration options that are not provided by the global IdM settings

To apply the changes you have made in the `/etc/sssd/sssd.conf` file:

[literal,subs="+quotes,attributes,verbatim"]
....
# `systemctl restart sssd.service`
....

Running `systemctl restart sssd.service` is required because the System Security Services Daemon (SSSD) does not automatically re-read or re-apply its configuration.

Note that for changes that affect IdM identity ranges, a complete server reboot is recommended.

[IMPORTANT]
====
To restart multiple IdM domain services, always use `systemctl restart ipa`. Because of dependencies between the services installed with the IdM server, the order in which they are started and stopped is critical. The `ipa` systemd service ensures that the services are started and stopped in the appropriate order.
====

.Useful `systemctl` commands

To start a particular IdM service:

[literal,subs="+quotes,attributes,verbatim"]
....
# *systemctl start _name_.service*
....

To stop a particular IdM service:

[literal,subs="+quotes,attributes,verbatim"]
....
# *systemctl stop _name_.service*
....

To restart a particular IdM service:

[literal,subs="+quotes,attributes,verbatim"]
....
# *systemctl restart _name_.service*
....

To view the status of a particular IdM service:

[literal,subs="+quotes,attributes,verbatim"]
....
# *systemctl status _name_.service*
....

[IMPORTANT]
====
You cannot use the IdM web UI to start or stop the individual services running on IdM servers. You can only use the web UI to modify the settings of a Kerberized service by navigating to `Identity` -> `Services` and selecting the service.
====

//TBD: In RHEL 7, we only had a link here: To stop, start, or restart an individual service only, use the `systemctl` utility, described in the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/chap-managing_services_with_systemd[System Administrator's Guide].

[role="_additional-resources"]
.Additional resources
* xref:starting-and-stopping-the-entire-ipa-server-the-ipactl-utility_{context}[Starting and stopping the entire Identity Management server]

//:context: {parent-context}
