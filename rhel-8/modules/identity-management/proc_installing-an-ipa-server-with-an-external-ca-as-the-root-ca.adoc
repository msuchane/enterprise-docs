:_mod-docs-content-type: PROCEDURE
:experimental:

[id='installing-an-ipa-server-with-an-external-ca-as-the-root-ca_{context}']
= Interactive installation
//Installing an {IPA} Server with an External CA as the Root CA

[role="_abstract"]
During the interactive installation using the `*ipa-server-install*` utility, you are asked to supply basic configuration of the system, for example the realm, the administrator's password and the Directory Manager's password.

The `ipa-server-install` installation script creates a log file at `/var/log/ipaserver-install.log`. If the installation fails, the log can help you identify the problem.

Follow this procedure to install a server:

* With integrated DNS

* With an external certificate authority (CA) as the root CA

// include::Snippet_install_determine.adoc[]

.Prerequisites

* You have determined the type of the external CA to specify with the [option]`--external-ca-type` option. See the `*ipa-server-install*`(1) man page for details.
* If you are using a Microsoft Certificate Services certificate authority (MS CS CA) as your external CA: you have determined the certificate profile or template to specify with the `--external-ca-profile` option. By default, the `SubCA` template is used.
+
For more information about the [option]`--external-ca-type` and [option]`--external-ca-profile` options, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/assembly_installing-an-ipa-server-without-dns-with-external-ca_installing-identity-management#options-used-when-installing-an-idm-ca-with-an-external-ca-as-the-root-ca_assembly_installing-an-ipa-server-without-dns-with-external-ca[Options used when installing an IdM CA with an external CA as the root CA].

//+
//For details, see link:https://frasertweedale.github.io/blog-redhat/posts/2017-08-14-ad-cs.html[this upstream article].

.Procedure


ifeval::["{assembly}" == "installing-server-with-dns"]
. Run the *ipa-server-install* utility.
+
[literal,subs="+quotes,attributes"]
....
# *ipa-server-install*
....
endif::[]

ifeval::["{assembly}" == "installing-server-external-ca"]
. Run the *ipa-server-install* utility with the `--external-ca` option.
+
[literal,subs="+quotes,attributes"]
....
# *ipa-server-install --external-ca*
....
+
--
* If you are using the Microsoft Certificate Services (MS CS) CA, also use the `--external-ca-type` option and, optionally, the `--external-ca-profile` option:
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *ipa-server-install --external-ca --external-ca-type=ms-cs --external-ca-profile=_<oid>/<name>/default_*
....

* If you are not using MS CS to generate the signing certificate for your IdM CA, no other option may be necessary:
+
[literal,subs="+quotes,attributes"]
....
# *ipa-server-install --external-ca*
....
--
endif::[]

ifeval::["{assembly}" == "installing-server-without-ca"]
. Run the *ipa-server-install* utility and provide all the required certificates. For example:
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *ipa-server-install \*
    *--http-cert-file _/tmp/server.crt_ \*
    *--http-cert-file _/tmp/server.key_ \*
    *--http-pin _secret_ \*
    *--dirsrv-cert-file _/tmp/server.crt_ \*
    *--dirsrv-cert-file _/tmp/server.key_ \*
    *--dirsrv-pin _secret_ \*
    *--ca-cert-file _ca.crt_*
....
+
See xref:certificates-required-to-install-ipa-server-no-ca_{context}[] for details on the provided certificates.
endif::[]

. The script prompts to configure an integrated DNS service.
//
// The following is displayed only in assemblies that have {assembly} set to "installing-server-with-dns" (==).
//
ifeval::["{assembly}" == "installing-server-with-dns"]
Enter `yes`.
+
[subs="quotes,attributes,macros"]
----
Do you want to configure integrated DNS (BIND)? [no]: `yes`
----
endif::[]
//
// The following is displayed only in assemblies that DO NOT have {assembly} set to "installing-server-with-dns" (!=).
//
ifeval::["{assembly}" != "installing-server-with-dns"]
Enter `yes` or `no`. In this procedure, we are installing a server with integrated DNS.
+
[subs="quotes,attributes,macros"]
----
Do you want to configure integrated DNS (BIND)? [no]: `yes`
----
+
NOTE: If you want to install a server without integrated DNS, the installation script will not prompt you for DNS configuration as described in the steps below. See xref:installing-an-ipa-server-without-integrated-dns_{ProjectNameID}[] for details on the steps for installing a server without DNS.
endif::[]


. The script prompts for several required settings and offers recommended default values in brackets.
+
* To accept a default value, press kbd:[Enter].
+
* To provide a custom value, enter the required value.
+
[subs="quotes,attributes"]
----
Server host name [`_server.idm.example.com_`]:
Please confirm the domain name [`_idm.example.com_`]:
Please provide a realm name [`_IDM.EXAMPLE.COM_`]:
----
+
WARNING: Plan these names carefully. You will not be able to change them after the installation is complete.
//See link:{planning-url}planning-the-installation-and-distribution-of-server-services[Planning the installation and distribution of server services] in _{PlanningIPA}_.

. Enter the passwords for the Directory Server superuser (`cn=Directory Manager`) and for the {IPA} (IdM) administration system user account (`admin`).
+
[subs="quotes,attributes"]
----
Directory Manager password:
IPA admin password:
----


. The script prompts for per-server DNS forwarders.
+
[subs="quotes,attributes"]
----
Do you want to configure DNS forwarders? [yes]:
----
+
* To configure per-server DNS forwarders, enter `yes`, and then follow the instructions on the command line. The installation process will add the forwarder IP addresses to the IdM LDAP.
+
** For the forwarding policy default settings, see the [option]`--forward-policy` description in the *ipa-dns-install*(1) man page.

+
* If you do not want to use DNS forwarding, enter `no`.
+
With no DNS forwarders, hosts in your IdM domain will not be able to resolve names from other, internal, DNS domains in your infrastructure. The hosts will only be left with public DNS servers to resolve their DNS queries.


. The script prompts to check if any DNS reverse (PTR) records for the IP addresses associated with the server need to be configured.
+
[subs="quotes,attributes"]
----
Do you want to search for missing reverse zones? [yes]:
----
+
If you run the search and missing reverse zones are discovered, the script asks you whether to create the reverse zones along with the PTR records.
+
[subs="quotes,attributes"]
----
Do you want to create reverse zone for IP 192.0.2.1 [yes]:
Please specify the reverse zone name [2.0.192.in-addr.arpa.]:
Using reverse zone(s) 2.0.192.in-addr.arpa.
----
+
NOTE: Using IdM to manage reverse zones is optional. You can use an external DNS service for this purpose instead.

. Enter `yes` to confirm the server configuration.
+
[subs="quotes,attributes,macros"]
----
Continue to configure the system with these values? [no]: `yes`
----

ifeval::["{assembly}" == "installing-server-external-ca"]
. During the configuration of the {CS} instance, the utility prints the location of the certificate signing request (CSR): `/root/ipa.csr`:
+
[subs="+quotes,attributes"]
----
...

Configuring certificate server (pki-tomcatd): Estimated time 3 minutes 30 seconds
  [1/8]: creating certificate server user
  [2/8]: configuring certificate server instance
The next step is to get /root/ipa.csr signed by your CA and re-run /sbin/ipa-server-install as:
/sbin/ipa-server-install --external-cert-file=/path/to/signed_certificate --external-cert-file=/path/to/external_ca_certificate
----
+
When this happens:
+
.. Submit the CSR located in `/root/ipa.csr` to the external CA. The process differs depending on the service to be used as the external CA.
//+
//IMPORTANT: In some cases, it is necessary to request the appropriate extensions for the certificate. The CA signing certificate generated for the IdM server must be a valid CA certificate. This requires either that the Basic Constraint be set to *CA=true* or that the Key Usage Extension be set on the signing certificate to allow it to sign certificates.
+
.. Retrieve the issued certificate and the CA certificate chain for the issuing CA in a base 64-encoded blob (either a PEM file or a Base_64 certificate from a Windows CA). Again, the process differs for every certificate service. Usually, a download link on a web page or in the notification email allows the administrator to download all the required certificates.
+
IMPORTANT: Be sure to get the full certificate chain for the CA, not just the CA certificate.

+
.. Run `ipa-server-install` again, this time specifying the locations and names of the newly-issued CA certificate and the CA chain files. For example:
+
[subs="+quotes,attributes"]
----
# ipa-server-install --external-cert-file=_/tmp/servercert20170601.pem_ --external-cert-file=_/tmp/cacert.pem_
----
endif::[]

. The installation script now configures the server. Wait for the operation to complete.

. After the installation script completes, update your DNS records in the following way:

.. Add DNS delegation from the parent domain to the IdM DNS domain. For example, if the IdM DNS domain is `_idm.example.com_`, add a name server (NS) record to the `example.com` parent domain.
+
IMPORTANT: Repeat this step each time after an IdM DNS server is installed.

.. Add an `_ntp._udp` service (SRV) record for your time server to your IdM DNS. The presence of the SRV record for the time server of the newly-installed IdM server in IdM DNS ensures that future replica and client installations are automatically configured to synchronize with the time server used by this primary IdM server.


[NOTE]
====
The [command]`ipa-server-install --external-ca` command can sometimes fail with the following error:

[subs="macros,attributes"]
....
ipa         : CRITICAL failed to configure ca instance Command '/usr/sbin/pkispawn -s CA -f /tmp/pass:quotes[_configuration_file_]' returned non-zero exit status 1
Configuration of CA failed
....

This failure occurs when the `*_proxy` environmental variables are set. For a solution of the problem, see xref:troubleshooting-external-ca-installation-fails[Troubleshooting: External CA installation fails].
====
