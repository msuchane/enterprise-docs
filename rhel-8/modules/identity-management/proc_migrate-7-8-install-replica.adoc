:_mod-docs-content-type: PROCEDURE
[id=install-replica_{context}]

= Installing the RHEL 8 replica

[role="_abstract"]
. List which server roles are present in your RHEL 7 environment:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel7 ~]# *ipa server-role-find --status enabled --server rhel7.example.com*
----------------------
3 server roles matched
----------------------
  Server name: rhel7.example.com
  Role name: CA server
  Role status: enabled

  Server name: rhel7.example.com
  Role name: DNS server
  Role status: enabled

  Server name: rhel7.example.com
  Role name: NTP server
  Role status: enabled
[... output truncated ...]
....

. [Optional] If you want to use the same per-server forwarders for `rhel8.example.com` that `rhel7.example.com` is using, view the per-server forwarders for `rhel7.example.com`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel7 ~]# *ipa dnsserver-show rhel7.example.com*
-----------------------------
1 DNS server matched
-----------------------------
  Server name: rhel7.example.com
  SOA mname: rhel7.example.com.
  *Forwarders:  192.0.2.20*
  Forward policy: only
--------------------------------------------------
Number of entries returned 1
--------------------------------------------------
....

. Install the IdM server on `rhel8.example.com` as a replica of the IdM RHEL 7 server, including all the server roles present on your `rhel7.example.com` except the NTP server role. To install the roles from the example above, use these options with the `ipa-replica-install` command:

+
--

** [option]`--setup-ca` to set up the Certificate&nbsp;System component
+
** [option]`--setup-dns` and [option]`--forwarder` to configure an integrated DNS server and set a per-server forwarder to take care of DNS queries that go outside the IdM domain

+
[NOTE]
====
Additionally, if your IdM deployment is in a trust relationship with {AD} (AD), add the [option]`--setup-adtrust` option to the `ipa-replica-install` command to configure AD trust capability on `rhel8.example.com`.
====

+
To set up an IdM server with the IP address of 192.0.2.1 that uses a per-server forwarder with the IP address of 192.0.2.20:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel8 ~]# *ipa-replica-install --setup-ca --ip-address 192.0.2.1 --setup-dns --forwarder 192.0.2.20*
....

+
You do not need to specify the RHEL 7 IdM server itself because if DNS is working correctly, `rhel8.example.com` will find it using DNS autodiscovery.

--

+
. [Optional] Add an `_ntp._udp` service (SRV) record for your external `NTP` time server to the DNS of the newly-installed IdM server, *rhel8.example.com*. Doing this is recommended because IdM in RHEL 8 does not provide its own time service. The presence of the SRV record for the time server in IdM DNS ensures that future RHEL 8 replica and client installations are automatically configured to synchronize with the time server used by *rhel8.example.com*. This is because `ipa-client-install` looks for the `_ntp._udp` DNS entry unless `--ntp-server` or `--ntp-pool` options are provided on the install command-line interface (CLI).

.Verification

. Verify that the IdM services are running on `rhel8.example.com`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel8 ~]# *ipactl status*
Directory Service: RUNNING
[... output truncated ...]
ipa: INFO: The ipactl command was successful
....

. Verify that server roles for `rhel8.example.com` are the same as for `rhel7.example.com` except the NTP server role:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@rhel8 ~]$ *kinit admin*
[root@rhel8 ~]$ *ipa server-role-find --status enabled --server rhel8.example.com*
----------------------
2 server roles matched
----------------------
  Server name: rhel8.example.com
  Role name: CA server
  Role status: enabled

  Server name: rhel8.example.com
  Role name: DNS server
  Role status: enabled
....

. [Optional] Display details about the replication agreement between `rhel7.example.com` and `rhel8.example.com`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel8 ~]# *ipa-csreplica-manage list --verbose rhel8.example.com*
Directory Manager password:

rhel7.example.com
last init status: None
last init ended: 1970-01-01 00:00:00+00:00
last update status: Error (0) Replica acquired successfully: Incremental update succeeded
last update ended: 2019-02-13 13:55:13+00:00
....

. [Optional] If your IdM deployment is in a trust relationship with AD, verify that it is working:
.. link:
ifeval::[{ProductNumber} == 8]
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_identity_management/installing-trust-between-idm-and-ad_installing-identity-management#verifying-the-kerberos-configuration_setting-up-a-trust[Verify the Kerberos configuration]
endif::[]
ifeval::[{ProductNumber} == 9]
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/installing_trust_between_idm_and_ad/setting-up-a-trust_installing-trust-between-idm-and-ad#verifying-the-kerberos-configuration_setting-up-a-trust[Verify the Kerberos configuration]
endif::[]

.. Attempt to resolve an AD user on `rhel8.example.com`:
+
[literal, subs="+quotes,verbatim"]
....
[root@rhel8 ~]# *id aduser@ad.domain*
....

. Verify that `rhel8.example.com` is synchronized with the `NTP` server:
+
[literal,subs="+quotes,verbatim"]
....
[root@rhel8 ~]# *chronyc tracking*
Reference ID    : CB00710F (*ntp.example.com*)
Stratum         : 3
Ref time (UTC)  : Tue Nov 16 09:49:17 2021
[... output truncated ...]
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/working_with_dns_in_identity_management/managing-dns-zones-in-idm_working-with-dns-in-identity-management#dns-configuration-priorities_managing-dns-zones-in-idm[DNS configuration priorities]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/preparing-the-system-for-ipa-server-installation_installing-identity-management#assembly_time-service-requirements-for-idm_preparing-the-system-for-ipa-server-installation[Time service requirements for IdM]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/configuring-time-synchronization_configuring-basic-system-settings#proc_migrating-to-chrony_using-chrony-to-configure-ntp[Migrating to chrony]
