:_mod-docs-content-type: CONCEPT

[id="non-posix-external-groups-and-sid-mapping_{context}"]
= Non-POSIX external groups and SID mapping

{IPA} (IdM) uses LDAP for managing groups. {AD} (AD) entries are not synchronized or copied over to IdM, which means that AD users and groups have no LDAP objects in the LDAP server, so they cannot be directly used to express group membership in the IdM LDAP. For this reason, administrators in IdM need to create non-POSIX external groups, referenced as normal IdM LDAP objects to signify group membership for AD users and groups in IdM.
// For an example of how to create groups in IdM for AD users, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/windows_integration_guide/trust-managing#trust-groups[Creating IdM Groups for Active Directory Users] in the _Windows Integration Guide_.

Security IDs (SIDs) for non-POSIX external groups are processed by SSSD, which maps the SIDs of groups in Active Directory to POSIX groups in IdM. In Active Directory, SIDs are associated with user names. When an AD user name is used to access IdM resources, SSSD uses the user's SID to build up a full group membership information for the user in the IdM domain.
