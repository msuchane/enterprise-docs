:_mod-docs-content-type: PROCEDURE
[id="configuring-the-default-shell-for-IdM-users-using-an-Ansible-playbook_{context}"]
= Configuring the default shell for IdM users using an Ansible playbook

[role="_abstract"]
The shell is a program that accepts and interprets commands. Several shells are available in {RHEL} (RHEL), such as `bash`, `sh`, `ksh`, `zsh`, `fish`, and others. `Bash`, or `/bin/bash`, is a popular shell on most Linux systems, and it is normally the default shell for user accounts on RHEL.

The following procedure describes how you can use an Ansible playbook to configure `sh`, an alternative shell, as the default shell for IdM users.


.Prerequisites

* You know the IdM administrator password.
* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

.Procedure

. Optional: Use the `retrieve-config.yml` Ansible playbook to identify the current shell for IdM users. See
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/configuring-global-idm-settings-using-ansible-playbooks_using-ansible-to-install-and-manage-idm#retrieving-IdM-configuration-using-an-Ansible-playbook_configuring-global-idm-settings-using-ansible-playbooks[Retrieving IdM configuration using an Ansible playbook] for details.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/configuring-global-idm-settings-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management#retrieving-IdM-configuration-using-an-Ansible-playbook_configuring-global-idm-settings-using-ansible-playbooks[Retrieving IdM configuration using an Ansible playbook] for details.
endif::[]



. Create an inventory file, for example `inventory.file`, and define `ipaserver` in it:

+
[literal,subs="+quotes,attributes,verbatim"]
----
[ipaserver]
server.idm.example.com
----

. Open the `/usr/share/doc/ansible-freeipa/playbooks/config/ensure-config-options-are-set.yml` Ansible playbook file for editing:

+
[literal,subs="+quotes,attributes,verbatim"]
----
---
- name: Playbook to ensure some config options are set
  hosts: ipaserver
  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml

  tasks:
  # Set defaultlogin and maxusername
  - ipaconfig:
      ipaadmin_password: "{{ ipaadmin_password }}"
      defaultshell: /bin/bash
      maxusername: 64
----

. Adapt the file by changing the following:

* The password of IdM administrator set by the `ipaadmin_password` variable.
* The default shell of the IdM users set by the `defaultshell` variable into `/bin/sh`.


. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
----

$ *ansible-playbook --vault-password-file=password_file -v -i __path_to_inventory_directory__/inventory.file /usr/share/doc/ansible-freeipa/playbooks/config/ensure-config-options-are-set.yml*
----


.Verification steps

You can verify that the default user shell has been changed by starting a new session in IdM:

. Log into `ipaserver` as IdM administrator:

+
[literal,subs="+quotes,attributes,verbatim"]
----
$ *ssh admin@server.idm.example.com*
Password:
[admin@server /]$
----

. Display the current shell:

+
[literal,subs="+quotes,attributes,verbatim"]
----
[admin@server /]$ *echo "$SHELL"*
/bin/sh
----


+
The logged-in user is using the `sh` shell.
