:_mod-docs-content-type: CONCEPT
[id="ca-renewal-server_{context}"]
= Explanation of IdM CA renewal server

[role="_abstract"]
In an Identity Management (IdM) deployment that uses an embedded certificate authority (CA), the CA renewal server maintains and renews IdM system certificates. It ensures robust IdM deployments.

IdM system certificates include:

* `IdM CA` certificate
* `OCSP` signing certificate
* `IdM CA subsystem` certificates
* `IdM CA audit signing` certificate
* `IdM renewal agent` (RA) certificate
* `KRA` transport and storage certificates

What characterizes system certificates is that their keys are shared by all CA replicas. In contrast, the IdM service certificates (for example, `LDAP`, `HTTP` and `PKINIT` certificates), have different keypairs and subject names on different IdM CA servers.

In IdM topology, by default, the first IdM CA server is the CA renewal server.

[NOTE]
====
In upstream documentation, the IdM CA is called `Dogtag`.
====

.The role of the CA renewal server

The `IdM CA`, `IdM CA subsystem`, and `IdM RA` certificates are crucial for IdM deployment. Each certificate is stored in an NSS database in the `/etc/pki/pki-tomcat/` directory and also as an LDAP database entry. The certificate stored in LDAP must match the certificate stored in the NSS database. If they do not match, authentication failures occur between the IdM framework and IdM CA, and between IdM CA and LDAP.

All IdM CA replicas have tracking requests for every system certificate. If an IdM deployment with integrated CA does not contain a CA renewal server, each IdM CA server requests the renewal of system certificates independently. This results in different CA replicas having various system certificates and authentication failures occurring.

Appointing one CA replica as the renewal server allows the system certificates to be renewed exactly once, when required, and thus prevents authentication failures.

.The role of the `certmonger` service on CA replicas

The `certmonger` service running on all IdM CA replicas uses the `dogtag-ipa-ca-renew-agent` renewal helper to keep track of IdM system certificates. The renewal helper program reads the CA renewal server configuration.
On each CA replica that is not the CA renewal server, the renewal helper retrieves the latest system certificates from the `ca_renewal` LDAP entries. Due to non-determinism in when exactly `certmonger` renewal attempts occur, the `dogtag-ipa-ca-renew-agent` helper sometimes attempts to update a system certificate before the CA renewal server has actually renewed the certificate. If this happens, the old, soon-to-expire certificate is returned to the `certmonger` service on the CA replica. The `certmonger` service, realizing it is the same certificate that is already stored in its database, keeps attempting to renew the certificate with some delay between individual attempts until it can retrieve the updated certificate from the CA renewal server.

.The correct functioning of IdM CA renewal server

An IdM deployment with an embedded CA is an IdM deployment that was installed with an IdM CA - or whose IdM CA server was installed later. An IdM deployment with an embedded CA must at all times have exactly one CA replica configured as the renewal server. The renewal server must be online and fully functional, and must replicate properly with the other servers.

If the current CA renewal server is being deleted using the `ipa server-del`, `ipa-replica-manage del`, `ipa-csreplica-manage del` or `ipa-server-install --uninstall` commands, another CA replica is automatically assigned as the CA renewal server. This policy ensures that the renewal server configuration remains valid.


// IdM has a simple protection in place to ensure that the renewal server configuration stays valid. If a system administrator decides to delete a server from the topology using the `ipa server-del`, `ipa-replica-manage del`, `ipa-csreplica-manage del` or `ipa-server-install --uninstall` command, then if the server being deleted is the current CA renewal server, a different CA replica is automatically elected as the new CA renewal server.

This policy does not cover the following situations:

* *Offline renewal server*

+
If the renewal server is offline for an extended duration, it may miss a renewal window. In this situation, all nonrenewal CA servers keep reinstalling the current system certificates until the certificates expire. When this occurs, the IdM deployment is disrupted because even one expired certificate can cause renewal failures for other certificates.
+
// Only display the following if this is the Configuring & Managing IdM guide
ifeval::["{ProjectNameID}" == "configuring-and-managing-idm"]
To prevent this situation: if your current renewal server is offline and unavailable for an extended period of time, consider xref:changing-ca-renewal_ipa-ca-renewal[assigning a new CA renewal server manually].
endif::[]
// Only display the following if this is the Installing IdM guide
ifeval::["{ProjectNameID}" == "installing-identity-management"]
To prevent this situation: if your current renewal server is offline and unavailable for an extended period of time, consider
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_certificates_in_idm/ipa-ca-renewal_working-with-idm-certificates#changing-ca-renewal_ipa-ca-renewal[assigning a new CA renewal server manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/ipa-ca-renewal_managing-certificates-in-idm#changing-ca-renewal_ipa-ca-renewal[assigning a new CA renewal server manually].
endif::[]
endif::[]
+
* *Replication problems*

+
If replication problems exist between the renewal server and other CA replicas, renewal might succeed, but the other CA replicas might not be able to retrieve the updated certificates before they expire.
+
To prevent this situation, make sure that your replication agreements are working correctly. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/trouble-gen-replication[general] or link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/trouble-replica[specific] replication troubleshooting guidelines in the RHEL 7 __Linux Domain Identity, Authentication, and Policy Guide__.
