:_mod-docs-content-type: CONCEPT

[id="con_tools-for-managing-smart-cards-and-their-contents_{context}"]
= Tools for managing smart cards and their contents

You can use many different tools to manage the keys and certificates stored on your smart cards.  You can use these tools to do the following:

* List available smart card readers connected to a system.
* List available smart cards and view their contents.
* Manipulate the smart card content, that is the keys and certificates.

There are many tools that provide similar functionality but some work at different layers of your system. Smart cards are managed on multiple layers by multiple components. On the lower level, the operating system communicates with the smart card reader using the PC/SC protocol, and this communication is handled by the pcsc-lite daemon. The daemon forwards the commands received to the smart card reader typically over USB, which is handled by low-level CCID driver. The PC/SC low level communication is rarely seen on the application level. The main method in RHEL for applications to access smart cards is via a higher level application programming interface (API), the OASIS PKCS#11 API, which abstracts the card communication to specific commands that operate on cryptographic objects,  for example, private keys. Smart card vendors provide a shared module, such as an `.so` file, which follows the PKCS#11 API and serves as a driver for the smart card.

You can use the following tools to manage your smart cards and their contents:

* OpenSC tools: work with the drivers implemented in `opensc`.
** opensc-tool: perform smart card operations.
** pkcs15-tool: manage the PKCS#15 data structures on smart cards, such as listing and reading PINs, keys, and certificates stored on the token.
** pkcs11-tool: manage the PKCS#11 data objects on smart cards, such as listing and reading PINs, keys, and certificates stored on the token.

* GnuTLS utils: an API for applications to enable secure communication over the network transport layer, as well as interfaces to access X.509, PKCS#12, OpenPGP, and other structures.
** p11tool: perform operations on PKCS#11 smart cards and security modules.
** certtool: parse and generate X.509 certificates, requests, and private keys.

* Network Security Services (NSS) Tools: a set of libraries designed to support the cross-platform development of security-enabled client and server applications. Applications built with NSS can support SSL v3, TLS, PKCS #5, PKCS #7, PKCS #11, PKCS #12, S/MIME, X.509 v3 certificates, and other security standards.
** modutil: manage PKCS#11 module information with the security module database.
** certutil: manage keys and certificates in both NSS databases and other NSS tokens.

For more information about using these tools to troubleshoot issues with authenticating using a smart card, see
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/assembly_troubleshooting-authentication-with-smart-cards_managing-smart-card-authentication[Troubleshooting authentication with smart cards].

[role="_additional-resources"]
.Additional resources
* `opensc-tool` man page
* `pkcs15-tool` man page
* `pkcs11-tool` man page
* `p11tool` man page
* `certtool` man page
* `modutil` man page
* `certutil` man page
