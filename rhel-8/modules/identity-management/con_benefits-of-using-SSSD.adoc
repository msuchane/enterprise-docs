:_mod-docs-content-type: CONCEPT
[id="con_benefits-of-using-SSSD_{context}"]
= Benefits of using SSSD

[role="_abstract"]
Using the System Security Services Daemon (SSSD) provides multiple benefits regarding user identity retrieval and user authentication.

Offline authentication::
+
SSSD optionally keeps a cache of user identities and credentials retrieved from remote providers. In this setup, a user - provided they have already authenticated once against the remote provider at the start of the session - can successfully authenticate to resources even if the remote provider or the client are offline.

A single user account: improved consistency of the authentication process::
+
With SSSD, it is not necessary to maintain both a central account and a local user account for offline authentication. The conditions are:

** In a particular session, the user must have logged in at least once: the client must be connected to the remote provider when the user logs in for the first time.
** Caching must be enabled in SSSD.
+
Without SSSD, remote users often have multiple user accounts. For example, to connect to a virtual private network (VPN), remote users have one account for the local system and another account for the VPN system. In this scenario, you must first authenticate on the private network to fetch the user from the remote server and cache the user credentials locally.
+
With SSSD, thanks to caching and offline authentication, remote users can connect to network resources simply by authenticating to their local machine. SSSD then maintains their network credentials.

Reduced load on identity and authentication providers::
+
When requesting information, the clients first check the local SSSD cache. SSSD contacts the remote providers only if the information is not available in the cache.
