// This module included in the following assemblies:
// rhel-8/assemblies/assembly_planning-a-cross-forest-trust-between-ipa-and-ad.adoc
// rhel-8/assemblies/assembly_setting-up-a-trust-using-the-command-line.adoc

[id="proc_enabling-automatic-private-group-mapping-for-a-posix-id-range-on-the-cli_{context}"]
= Enabling automatic private group mapping for a POSIX ID range on the CLI

[role="_abstract"]
By default, SSSD does not map private groups for Active Directory (AD) users if you have established a POSIX trust that relies on POSIX data stored in AD. If any AD users do not have primary groups configured, IdM is not be able to resolve them.

This procedure explains how to enable automatic private group mapping for an ID range by setting the `hybrid` option for the `auto_private_groups` SSSD parameter on the command line. As a result, IdM is able to resolve AD users that do not have primary groups configured in AD.


.Prerequisites

* You have successfully established a POSIX cross-forest trust between your IdM and AD environments.

.Procedure

. Display all ID ranges and make note of the AD ID range you want to modify.
+
[literal,subs="+quotes,verbatim"]
....
[root@server ~]# *ipa idrange-find*
----------------
2 ranges matched
----------------
  Range name: IDM.EXAMPLE.COM_id_range
  First Posix ID of the range: 882200000
  Number of IDs in the range: 200000
  Range type: local domain range

  Range name: *AD.EXAMPLE.COM_id_range*
  First Posix ID of the range: 1337000000
  Number of IDs in the range: 200000
  Domain SID of the trusted domain: S-1-5-21-4123312420-990666102-3578675309
  Range type: *Active Directory trust range with POSIX attributes*
----------------------------
Number of entries returned 2
----------------------------
....

. Adjust the automatic private group behavior for the AD ID range with the `ipa idrange-mod` command.
+
[literal,subs="+quotes,verbatim"]
....
[root@server ~]# *ipa idrange-mod --auto-private-groups=hybrid AD.EXAMPLE.COM_id_range*
....

. Reset the SSSD cache to enable the new setting.
+
[literal,subs="+quotes,verbatim"]
....
[root@server ~]# *sss_cache -E*
....


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/planning_identity_management/planning-a-cross-forest-trust-between-idm-and-ad_planning-identity-management#ref_options-for-automatically-mapping-private-groups-for-ad-users-posix_planning-a-cross-forest-trust-between-idm-and-ad[Options for automatically mapping private groups for AD users]
