:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="the-certificate-system-role_{context}"]
= The `certificate` RHEL system role

[role="_abstract"]

Using the `certificate` system role, you can manage issuing and renewing TLS and SSL certificates using Ansible Core.

The role uses `certmonger` as the certificate provider, and currently supports issuing and renewing self-signed certificates and using the IdM integrated certificate authority (CA).

You can use the following variables in your Ansible playbook with the `certificate` system role:

`certificate_wait`:: to specify if the task should wait for the certificate to be issued.
`certificate_requests`:: to represent each certificate to be issued and its parameters.

[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.certificate/README.md` file
* `/usr/share/doc/rhel-system-roles/certificate/` directory

