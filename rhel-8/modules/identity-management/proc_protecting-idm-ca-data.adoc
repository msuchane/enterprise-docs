:_mod-docs-content-type: PROCEDURE
// Module included in the following titles/assemblies:
//
// title /planning/preparing-for-disaster-recovery-with-identity-management


[id="special-considerations-for-idm-ca_{context}"]
= Protecting IdM CA data

[role="_abstract"]
If your deployment contains the integrated IdM Certificate Authority (CA), install several CA replicas so you can create additional CA replicas if one is lost.

.Procedure
. Configure three or more replicas to provide CA services.
.. To install a new replica with CA services, run `ipa-replica-install` with the `--setup-ca` option.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ipa-replica-install --setup-ca*
....
.. To install CA services on a preexisting replica, run `ipa-ca-install`.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@replica ~]# *ipa-ca-install*
....
. Create CA replication agreements between your CA replicas.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@careplica1 ~]# *ipa topologysegment-add*
Suffix name: *ca*
Left node: ca-replica1.example.com
Right node: ca-replica2.example.com
Segment name [ca-replica1.example.com-to-ca-replica2.example.com]: new_segment
---------------------------
Added segment "new_segment"
---------------------------
  Segment name: new_segment
  Left node: ca-replica1.example.com
  Right node: ca-replica2.example.com
  Connectivity: both
....

WARNING: If only one server provides CA services and it is damaged, the entire environment will be lost. If you use the IdM CA, Red Hat *strongly recommends* having three or more replicas with CA services installed, with CA replication agreements between them.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/planning_identity_management/planning-your-ca-services_planning-identity-management[Planning your CA services].
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-ipa-replica_installing-identity-management[Installing an IdM replica].
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/planning_identity_management/planning-the-replica-topology_planning-identity-management[Planning the replica topology].
