// Module included in the following assemblies:
//
// assembly_configuring-idm-for-smart-card-auth.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_configuring-gdm-access-using-smart-card-authentication.adoc[leveloffset=+1]

:_mod-docs-content-type: PROCEDURE
[id="configuring-gdm-access-using-smart-card-authentication_{context}"]
= Logging in to GDM using smart card authentication on an IdM client

[role="_abstract"]
The GNOME Desktop Manager (GDM) requires authentication. You can use your password; however, you can also use a smart card for authentication.

Follow this procedure to use smart card authentication to access GDM.

.Prerequisites

* The system has been configured for smart card authentication. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#conf-idm-client-for-smart-card-auth_configuring-idm-for-smart-card-auth[Configuring the IdM client for smart card authentication].
* The smart card contains your certificate and private key.
* The user account is a member of the IdM domain.
* The certificate on the smart card maps to the user entry through:
** Assigning the certificate to a particular user entry. For details, see, link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#proc-add-cert-idm-user-webui_configuring-idm-for-smart-card-auth[Adding a certificate to a user entry in the IdM Web UI] or link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#proc-add-cert-idm-user-cli_configuring-idm-for-smart-card-auth[Adding a certificate to a user entry in the IdM CLI].
** The certificate mapping data being applied to the account. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/con-idm-certmapdata_managing-smart-card-authentication[Certificate mapping rules for configuring authentication on smart cards].

.Procedure

. Insert the smart card in the reader.

. Enter the smart card PIN.

. Click *Sign In*.

You are successfully logged in to the RHEL system and you have a TGT provided by the IdM server.

.Verification steps

* In the *Terminal* window, enter `klist` and check the result:
+
[literal]
....
$ klist
Ticket cache: KEYRING:persistent:1358900015:krb_cache_TObtNMd
Default principal: example.user@REDHAT.COM

Valid starting       Expires              Service principal
04/20/2020 13:58:24  04/20/2020 23:58:24  krbtgt/EXAMPLE.COM@EXAMPLE.COM
	renew until 04/27/2020 08:58:15

....
