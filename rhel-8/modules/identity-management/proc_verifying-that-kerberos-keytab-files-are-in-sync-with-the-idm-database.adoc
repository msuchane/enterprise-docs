:_mod-docs-content-type: PROCEDURE

[id="proc_verifying-that-kerberos-keytab-files-are-in-sync-with-the-idm-database_{context}"]
= Verifying that Kerberos keytab files are in sync with the IdM database

When you change a Kerberos password, IdM automatically generates a new corresponding Kerberos key and increments its Key Version Number (KVNO). If a Kerberos keytab is not updated with the new key and KVNO, any services that depend on that keytab to retrieve a valid key might not be able to authenticate to the Kerberos Key Distribution Center (KDC).

If one of your IdM services cannot communicate with another service, use the following procedure to verify that your Kerberos keytab files are in sync with the keys stored in the IdM database. If they are out of sync, retrieve a Kerberos keytab with an updated key and KVNO. This example compares and retrieves an updated `DNS` principal for an IdM server.

.Prerequisites

* You must authenticate as the IdM admin account to retrieve keytab files
* You must authenticate as the `root` account to modify keytab files owned by other users

.Procedure

. Display the KVNO of the principals in the keytab you are verifying. In the following example, the `/etc/named.keytab` file has the key for the `DNS/server1.idm.example.com@EXAMPLE.COM` principal with a KVNO of 2.
+
[literal,subs="+quotes,verbatim"]
....
[root@server1 ~]# klist -ekt /etc/named.keytab
Keytab name: FILE:/etc/named.keytab
KVNO Timestamp           Principal
---- ------------------- ------------------------------------------------------
   *2* 11/26/2021 13:51:11 DNS/server1.idm.example.com@EXAMPLE.COM (aes256-cts-hmac-sha1-96)
   *2* 11/26/2021 13:51:11 DNS/server1.idm.example.com@EXAMPLE.COM (aes128-cts-hmac-sha1-96)
   *2* 11/26/2021 13:51:11 DNS/server1.idm.example.com@EXAMPLE.COM (camellia128-cts-cmac)
   *2* 11/26/2021 13:51:11 DNS/server1.idm.example.com@EXAMPLE.COM (camellia256-cts-cmac)
....

. Display the KVNO of the principal stored in the IdM database. In this example, the KVNO of the key in the IdM database does not match the KVNO in the keytab.
+
[literal,subs="+quotes,verbatim"]
....
[root@server1 ~]# kvno DNS/server1.idm.example.com@EXAMPLE.COM
DNS/server1.idm.example.com@EXAMPLE.COM: kvno = *3*
....

. Authenticate as the IdM admin account.
+
[literal,subs="+quotes,verbatim"]
....
[root@server1 ~]# kinit admin
Password for admin@IDM.EXAMPLE.COM:
....


. Retrieve an updated Kerberos key for the principal and store it in its keytab. Perform this step as the `root` user so you can modify the `/etc/named.keytab` file, which is owned by the `named` user.
+
[literal,subs="+quotes,verbatim"]
....
[root@server1 ~]# ipa-getkeytab -s server1.idm.example.com -p DNS/server1.idm.example.com -k /etc/named.keytab
....


.Verification

. Display the updated KVNO of the principal in the keytab.
+
[literal,subs="+quotes,verbatim"]
....
[root@server1 ~]# klist -ekt /etc/named.keytab
Keytab name: FILE:/etc/named.keytab
KVNO Timestamp           Principal
---- ------------------- ------------------------------------------------------
   *4* 08/17/2022 14:42:11 DNS/server1.idm.example.com@EXAMPLE.COM (aes256-cts-hmac-sha1-96)
   *4* 08/17/2022 14:42:11 DNS/server1.idm.example.com@EXAMPLE.COM (aes128-cts-hmac-sha1-96)
   *4* 08/17/2022 14:42:11 DNS/server1.idm.example.com@EXAMPLE.COM (camellia128-cts-cmac)
   *4* 08/17/2022 14:42:11 DNS/server1.idm.example.com@EXAMPLE.COM (camellia256-cts-cmac)
....

. Display the KVNO of the principal stored in the IdM database and ensure it matches the KVNO from the keytab.
+
[literal,subs="+quotes,verbatim"]
....
[root@server1 ~]# kvno DNS/server1.idm.example.com@EXAMPLE.COM
DNS/server1.idm.example.com@EXAMPLE.COM: kvno = *4*
....


[role="_additional-resources"]
.Additional resources
* xref:con_how-identity-management-uses-kerberos-keytab-files_assembly_maintaining-idm-kerberos-keytab-files[How Identity Management uses Kerberos keytab files]
* xref:ref_list-of-idm-kerberos-keytab-files-and-their-contents_assembly_maintaining-idm-kerberos-keytab-files[List of IdM Kerberos keytab files and their contents]
