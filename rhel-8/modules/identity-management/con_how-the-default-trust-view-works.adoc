:_mod-docs-content-type: CONCEPT

// This module is included in the following assemblies
// rhel-8/assemblies/assembly_using-id-views-for-active-directory-users.adoc

[id="con_how-the-default-trust-view-works_{context}"]
= How the Default Trust View works

[role="_abstract"]
The *Default Trust View* is the default ID view that is always applied to AD users and groups in trust-based setups. It is created automatically when you establish the trust using the `ipa-adtrust-install` command and cannot be deleted.

[NOTE]
====
The Default Trust View only accepts overrides for AD users and groups, not for IdM users and groups.
====

Using the Default Trust View, you can define custom POSIX attributes for AD users and groups, thus overriding the values defined in AD.

[id='applying-the-default-trust-view_{context}']
.Applying the Default Trust View
[options="header"]
|===
||Values in AD|Default Trust View|Result

a|*Login*
|ad_user
|ad_user
|ad_user

a|*UID*
|111
|222
|222

a|*GID*
|111
|(no value)
|111

|===

You can also configure additional ID Views to override the Default Trust View on IdM clients. IdM applies the values from the host-specific ID view on top of the Default Trust View:

* If an attribute is defined in the host-specific ID view, IdM applies the value from this ID view.
* If an attribute is not defined in the host-specific ID view, IdM applies the value from the Default Trust View.

[id='applying-a-host-specific-id-view-on-top-of-the-default-trust-view_{context}']
.Applying a host-specific ID view on top of the Default Trust View
[options="header"]
|===
||Values in AD|Default Trust View|Host-specific ID view|Result

a|*Login*
|ad_user
|ad_user
|(no value)
|ad_user

a|*UID*
|111
|222
|333
|333

a|*GID*
|111
|(no value)
|333
|333

|===

[NOTE]
====
You can only apply host-specific ID views to override the Default Trust View on IdM clients. IdM servers and replicas always apply the values from the Default Trust View.
====

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/using-an-id-view-to-override-a-user-attribute-value-on-an-idm-client_configuring-and-managing-idm[Using an ID view to override a user attribute value on an IdM client]
