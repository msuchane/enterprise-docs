:_mod-docs-content-type: PROCEDURE
[id="configuring-automatic-activation-of-IdM-stage-user-accounts_{context}"]
= Configuring automatic activation of IdM stage user accounts

[role="_abstract"]
This procedure shows how to create a script for activating stage users. The system runs the script automatically at specified time intervals. This ensures that new user accounts are automatically activated and available for use shortly after they are created.

[IMPORTANT]
====
The procedure assumes that the owner of the external provisioning system has already validated the users and that they do not require additional validation on the IdM side before the script adds them to IdM.
====

It is sufficient to enable the activation process on only one of your IdM servers.


.Prerequisites

* The *provisionator* and *activator* accounts exist in IdM. For details, see xref:preparing-IdM-accounts-for-automatic-activation-of-stage-user-accounts_{context}[Preparing IdM accounts for automatic activation of stage user accounts].

* You have root privileges on the IdM server on which you are running the procedure.

* You are logged in as IdM administrator.

* You trust your external provisioning system.


.Procedure

. Generate a keytab file for the activation account:

+
....
# ipa-getkeytab -s server.idm.example.com -p "activator" -k /etc/krb5.ipa-activation.keytab
....

+
If you want to enable the activation process on more than one IdM server, generate the keytab file on one server only. Then copy the keytab file to the other servers.

. Create a script, `/usr/local/sbin/ipa-activate-all`, with the following contents to activate all users:

+
....
#!/bin/bash

kinit -k -i activator

ipa stageuser-find --all --raw | grep "  uid:" | cut -d ":" -f 2 | while read uid; do ipa stageuser-activate ${uid}; done
....

. Edit the permissions and ownership of the `ipa-activate-all` script to make it executable:

+
....
# chmod 755 /usr/local/sbin/ipa-activate-all
# chown root:root /usr/local/sbin/ipa-activate-all
....

. Create a systemd unit file, `/etc/systemd/system/ipa-activate-all.service`, with the following contents:

+
....
[Unit]
Description=Scan IdM every minute for any stage users that must be activated

[Service]
Environment=KRB5_CLIENT_KTNAME=/etc/krb5.ipa-activation.keytab
Environment=KRB5CCNAME=FILE:/tmp/krb5cc_ipa-activate-all
ExecStart=/usr/local/sbin/ipa-activate-all
....


. Create a systemd timer, `/etc/systemd/system/ipa-activate-all.timer`, with the following contents:

+
....
[Unit]
Description=Scan IdM every minute for any stage users that must be activated

[Timer]
OnBootSec=15min
OnUnitActiveSec=1min

[Install]
WantedBy=multi-user.target
....


. Reload the new configuration:

+
....
# systemctl daemon-reload
....


. Enable `ipa-activate-all.timer`:

+
....
# systemctl enable ipa-activate-all.timer
....


. Start `ipa-activate-all.timer`:

+
....
# systemctl start ipa-activate-all.timer
....


. (Optional) Verify that the `ipa-activate-all.timer` daemon is running:

+
....
# systemctl status ipa-activate-all.timer
● ipa-activate-all.timer - Scan IdM every minute for any stage users that must be activated
   Loaded: loaded (/etc/systemd/system/ipa-activate-all.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Wed 2020-06-10 16:34:55 CEST; 15s ago
  Trigger: Wed 2020-06-10 16:35:55 CEST; 44s left

Jun 10 16:34:55 server.idm.example.com systemd[1]: Started Scan IdM every minute for any stage users that must be activated.
....
