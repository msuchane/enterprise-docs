:_mod-docs-content-type: PROCEDURE
[id="archiving-a-secret-in-a-standard-user-vault-in-idm-using-ansible_{context}"]
= Archiving a secret in a standard user vault in IdM using Ansible

[role="_abstract"]
Follow this procedure to use an Ansible playbook to store sensitive information in a personal vault. In the example used, the *idm_user* user archives a file with sensitive information named *password.txt* in a vault named *my_vault*.


.Prerequisites

* You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[ansible-freeipa] package on the Ansible controller, that is the host on which you execute the steps in the procedure.
* You know the password of *idm_user*.
* *idm_user* is the owner, or at least a member user of *my_vault*.
* You have access to *password.txt*, the secret that you want to archive in *my_vault*.


.Procedure

. Navigate to the `/usr/share/doc/ansible-freeipa/playbooks/vault` directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd /usr/share/doc/ansible-freeipa/playbooks/vault*
....


. Open your inventory file and make sure that the IdM server that you want to configure is listed in the `[ipaserver]` section. For example, to instruct Ansible to configure *server.idm.example.com*, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....


. Make a copy of the *data-archive-in-symmetric-vault.yml* Ansible playbook file but replace "symmetric" by "standard". For example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp data-archive-in-symmetric-vault.yml data-archive-in-standard-vault-copy.yml*
....

. Open the *data-archive-in-standard-vault-copy.yml* file for editing.

. Adapt the file by setting the following variables in the `ipavault` task section:

* Set the `ipaadmin_principal` variable to *idm_user*.
* Set the `ipaadmin_password` variable to the password of *idm_user*.
* Set the `user` variable to *idm_user*.
* Set the `name` variable to *my_vault*.
* Set the `in` variable to the full path to the file with sensitive information.
* Set the `action` variable to *member*.

+
This the modified Ansible playbook file for the current example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Tests
  hosts: ipaserver
  gather_facts: false

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - ipavault:
      ipaadmin_principal: __idm_user__
      ipaadmin_password: __idm_user_password__
      user: __idm_user__
      name: __my_vault__
      in: __/usr/share/doc/ansible-freeipa/playbooks/vault/password.txt__
      action: member
....

. Save the file.

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory.file data-archive-in-standard-vault-copy.yml*
....
