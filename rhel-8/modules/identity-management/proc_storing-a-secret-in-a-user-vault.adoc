:_mod-docs-content-type: PROCEDURE
[id="storing-a-secret-in-a-user-vault_{context}"]
= Storing a secret in a user vault

[role="_abstract"]
Follow this procedure to create a vault container with one or more private vaults to securely store files with sensitive information.
In the example used in the procedure below, the *idm_user* user creates a vault of the standard type. The standard vault type ensures that *idm_user* will not be required to authenticate when accessing the file. *idm_user* will be able to retrieve the file from any IdM client to which the user is logged in.


In the procedure:

* *idm_user* is the user who wants to create the vault.

* *my_vault* is the vault used to store the user's certificate.

* The vault type is `standard`, so that accessing the archived certificate does not require the user to provide a vault password.

* *secret.txt* is the file containing the certificate that the user wants to store in the vault.


.Prerequisites

* You know the password of *idm_user*.

* You are logged in to a host that is an IdM client.



.Procedure

. Obtain the Kerberos ticket granting ticket (TGT) for `idm_user`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *kinit idm_user*
....


. Use the [command]`ipa vault-add` command with the `--type standard` option to create a standard vault:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa vault-add my_vault --type standard*
----------------------
Added vault "my_vault"
----------------------
  Vault name: my_vault
  Type: standard
  Owner users: idm_user
  Vault user: idm_user
....
+
[IMPORTANT]
====

Make sure the first user vault for a user is created by the same user. Creating the first vault for a user also creates the user's vault container. The agent of the creation becomes the owner of the vault container.

For example, if another user, such as `admin`, creates the first user vault for `user1`, the owner of the user's vault container will also be `admin`, and `user1` will be unable to access the user vault or create new user vaults.
//See also users-cannot-access-their-vault.

====

. Use the [command]`ipa vault-archive` command with the `--in` option to archive the `secret.txt` file into the vault:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa vault-archive my_vault --in secret.txt*
-----------------------------------
Archived data into vault "my_vault"
-----------------------------------
....
