:_mod-docs-content-type: PROCEDURE

[id="using-ansible-to-configure-hbac-and-sudo-rules-in-idm_{context}"]
= Using Ansible to configure HBAC and sudo rules in IdM

[role="_abstract"]

Using host-based access control (HBAC) in {IPA} (IdM), you can define policies that restrict access to hosts or services based on the following:

* The user attempting to log in and this user's groups
* The host that a user is trying to access and the host groups to which that host belongs
* The service that is being used to access a host

Using `sudo`, a user can run programs as another user, with different privileges, for example `root` privileges. In IdM, you can manage sudo rules centrally. You can define `sudo` rules based on user groups, host groups and command groups, as well as individual users, hosts and commands.

Complete this procedure to ensure the presence of the following HBAC and `sudo` rules for IdM users:

* *jane* can only access host *client01.idm.example.com*.
* *john* can only access host *client02.idm.example.com*.
* Members of the `admins` group, which includes the default `admin` user as well as the regular *alice* user, can access any IdM host.
* Members of the `admins` group can run `sudo` with the following commands on any IdM host:
** `/usr/sbin/reboot`
** `/usr/bin/less`
** `/usr/sbin/setenforce`

The following diagram represents the desired configuration described above:

.IdM HBAC and SUDO rules diagram
image::IdM_HBAC_and_SUDO_rules_diagram.png[An image of IdM users and hosts showing different types of host access and sudo privileges for different users]

.Prerequisites
* On the control node:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** You have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
endif::[]
** You have stored your `ipaadmin_password` in the *secret.yml* Ansible vault.
* The users *jane*, *john* and *alice* exist in IdM. Passwords are configured for these accounts.

.Procedure

. Create your Ansible playbook file *add-hbac-and-sudo-rules-to-idm.yml* with the following content:

+
[subs="+quotes,attributes"]
----
---
- name: Playbook to manage IPA HBAC and SUDO rules
  hosts: ipaserver
  become: false
  gather_facts: false

  vars_files:
  - /home/<user_name>/MyPlaybooks/secret.yml

  module_defaults:
    ipahbacrule:
      ipaadmin_password: "{{ ipaadmin_password }}"
    ipagroup:
      ipaadmin_password: "{{ ipaadmin_password }}"
    ipasudocmd:
      ipaadmin_password: "{{ ipaadmin_password }}"
    ipasudocmdgroup:
      ipaadmin_password: "{{ ipaadmin_password }}"
    ipasudorule:
      ipaadmin_password: "{{ ipaadmin_password }}"

  tasks:
  - name: HBAC Rule for Jane - can log in to client01
    ipahbacrule: # Creates the rule
      name: Jane_rule
      hbacsvc:
      - sshd
      - login
      host: # Host name
      - client01.idm.example.com
      user:
      - jane

  - name: HBAC Rule for John - can log in to client02
    ipahbacrule: # Creates the rule
      name: john_rule
      hbacsvc:
      - sshd
      - login
      host: # Host name
      - client02.idm.example.com
      user:
      - john

  - name: Add user member alice to group admins
    ipagroup:
      name: admins
      action: member
      user:
      - alice

  - name: HBAC Rule for IdM administrators
    ipahbacrule: # Rule to allow admins full access
      name: admin_access # Rule name
      servicecat: all # All services
      hostcat: all # All hosts
      group: # User group
      - admins

    - name: Add reboot command to SUDO
      ipasudocmd:
        name: /usr/sbin/reboot
        state: present
    - name: Add less command to SUDO
      ipasudocmd:
        name: /usr/bin/less
        state: present
    - name: Add setenforce command to SUDO
      ipasudocmd:
        name: /usr/sbin/setenforce
        state: present

  - name: Create a SUDO command group
    ipasudocmdgroup:
      name: cmd_grp_1
      description: "Group of important commands"
      sudocmd:
      - /usr/sbin/setenforce
      - /usr/bin/less
      - /usr/sbin/reboot
      action: sudocmdgroup
      state: present

  - name: Create a SUDO rule with a SUDO command group
    ipasudorule:
      name: sudo_rule_1
      allow_sudocmdgroup:
      - cmd_grp_1
      group: admins
      state: present

  - name: Disable allow_all HBAC Rule
    ipahbacrule: # Rule to allow admins full access
      name: allow_all # Rule name
      state: disabled # Disables rule to allow everyone the ability to login
----

+
. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -i inventory add-hbac-and-sudo-rules-to-idm.yml*
....




.Verification

. Connect to client01 as the jane user:
+
[literal,subs="+quotes,attributes,verbatim"]
....
*~]$ ssh jane@client01*
Password:

Last login: Fri Aug 11 15:32:18 2023 from 192.168.122.1
[jane@client01 ~]$
....

+
The output verifies that jane has logged in to client01.

. Try to connect to client02 as the jane user:
+
[literal,subs="+quotes,attributes,verbatim"]
....
*~]$ ssh jane@client02*
Password:
Connection closed by 192.168.122.47 port 22
....

+
The output verifies that jane cannot log in to client02.

. Connect to client02 as the alice user:

+
[literal,subs="+quotes,attributes,verbatim"]
....
*~]$ ssh alice@client02*
Password:

Last login: Fri Aug 10 16:13:43 2023 from 192.168.122.1
....
+
The output verifies that alice has logged in to client02.

. Try to view the contents of the `/etc/sssd/sssd.conf` file using `less` without invoking the superuser privileges:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[alice@client02 ~]$ *less /etc/sssd/sssd.conf*
/etc/sssd/sssd.conf: Permission denied
....
+
The attempt fails as the file is not readable by anyone except the owner of the file, which is `root`.

. Invoke the `root` privileges to view the contents of the `/etc/sssd/sssd.conf` file using `less`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[alice@client02 ~]$ *sudo less /etc/sssd/sssd.conf*
[sudo] password for alice:

[domain/idm.example.com]

id_provider = ipa
ipa_server_mode = True
[...]
....

+
The output verifies that alice can execute the `less` command on the `/etc/sssd/sssd.conf` file.

[role="_additional-resources"]
.Additional resources
* xref:host-based-access-control-rules-in-idm_ensuring-the-presence-of-host-based-access-control-rules-in-idm-using-Ansible-playbooks[Host-based access control rules in IdM]
* xref:sudo-access-on-an-IdM-client_granting-sudo-access-to-an-IdM-user-on-an-IdM-client[Sudo access on an IdM client]
