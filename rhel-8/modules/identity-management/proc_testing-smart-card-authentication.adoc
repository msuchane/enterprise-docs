:_module-type: PROCEDURE

[id="proc_testing-smart-card-authentication_{context}"]
= Testing smart card access on the system

[role="_abstract"]
Follow this procedure to test whether you can access your smart card.

.Prerequisites

* You have installed and configured your IdM Server and client for use with smart cards.
* You have installed the `certutil` tool from the `nss-tools` package.
* You have the PIN or password for your smart card.

.Procedure

. Using the `lsusb` command, verify that the smart card reader is visible to the operating system:
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
$ lsusb
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
*Bus 001 Device 003: ID 072f:b100 Advanced Card Systems, Ltd ACR39U*
Bus 001 Device 002: ID 0627:0001 Adomax Technology Co., Ltd
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
----
+
For more information about the smart cards and readers tested and supported in RHEL, see link:https://access.redhat.com/articles/4253861[Smart Card support in RHEL {ProductNumber}].

. Ensure that the `pcscd` service and socket are enabled and running:
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
$ systemctl status pcscd.service pcscd.socket

● pcscd.service - PC/SC Smart Card Daemon
      *Loaded: loaded* (/usr/lib/systemd/system/pcscd.service; indirect;
vendor preset: disabled)
      *Active: active (running)* since Fri 2021-09-24 11:05:04 CEST; 2
weeks 6 days ago
TriggeredBy: ● pcscd.socket
        Docs: man:pcscd(8)
    Main PID: 3772184 (pcscd)
       Tasks: 12 (limit: 38201)
      Memory: 8.2M
         CPU: 1min 8.067s
      CGroup: /system.slice/pcscd.service
              └─3772184 /usr/sbin/pcscd --foreground --auto-exit

● pcscd.socket - PC/SC Smart Card Daemon Activation Socket
      *Loaded: loaded* (/usr/lib/systemd/system/pcscd.socket; enabled;
vendor preset: enabled)
      *Active: active (running)* since Fri 2021-09-24 11:05:04 CEST; 2
weeks 6 days ago
    Triggers: ● pcscd.service
      Listen: /run/pcscd/pcscd.comm (Stream)
      CGroup: /system.slice/pcscd.socket
----

. Using the `p11-kit list-modules` command, display information about the configured smart card and the tokens present on the smart card:
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
$ p11-kit list-modules
p11-kit-trust: p11-kit-trust.so
[...]
opensc: opensc-pkcs11.so
    library-description: OpenSC smartcard framework
    library-manufacturer: OpenSC Project
    library-version: 0.20
    *token: MyEID (sctest)*
        manufacturer: Aventra Ltd.
        model: PKCS#15
        serial-number: 8185043840990797
        firmware-version: 40.1
        flags:
               rng
               login-required
               user-pin-initialized
               token-initialized
----

. Verify you can access the contents of your smart card:
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
$ pkcs11-tool --list-objects --login
Using slot 0 with a present token (0x0)
Logging in to "MyEID (sctest)".
Please enter User PIN:
Private Key Object; RSA
  label:      Certificate
  ID:         01
  Usage:      sign
  Access:     sensitive
Public Key Object; RSA 2048 bits
  label:      Public Key
  ID:         01
  Usage:      verify
  Access:     none
Certificate Object; type = X.509 cert
  label:      Certificate
  subject:    DN: O=IDM.EXAMPLE.COM, CN=idmuser1
  ID:         01
----

. Display the contents of the certificate on your smart card using the `certutil` command:

.. Run the following command to determine the correct name of your certificate:
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
$ certutil -d /etc/pki/nssdb -L -h all

Certificate Nickname                                         Trust Attributes
                                                             SSL,S/MIME,JAR/XPI

Enter Password or Pin for "MyEID (sctest)":
Smart Card CA 0f5019a8-7e65-46a1-afe5-8e17c256ae00           CT,C,C
*MyEID (sctest):Certificate*                                   u,u,u
----

.. Display the contents of the certificate on your smart card:
+
[NOTE]
====
Ensure the name of the certificate is an exact match for the output displayed in the previous step, in this example `MyEID (sctest):Certificate`.
====
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
$ certutil -d /etc/pki/nssdb -L -n "MyEID (sctest):Certificate"

Enter Password or Pin for "MyEID (sctest)":
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 15 (0xf)
        Signature Algorithm: PKCS #1 SHA-256 With RSA Encryption
        Issuer: "CN=Certificate Authority,O=IDM.EXAMPLE.COM"
        Validity:
            Not Before: Thu Sep 30 14:01:41 2021
            Not After : Sun Oct 01 14:01:41 2023
        Subject: "CN=idmuser1,O=IDM.EXAMPLE.COM"
        Subject Public Key Info:
            Public Key Algorithm: PKCS #1 RSA Encryption
            RSA Public Key:
                Modulus:
                    [...]
                Exponent: 65537 (0x10001)
        Signed Extensions:
            Name: Certificate Authority Key Identifier
            Key ID:
                e2:27:56:0d:2f:f5:f2:72:ce:de:37:20:44:8f:18:7f:
                2f:56:f9:1a

            Name: Authority Information Access
            Method: PKIX Online Certificate Status Protocol
            Location:
                URI: "http://ipa-ca.idm.example.com/ca/ocsp"

            Name: Certificate Key Usage
            Critical: True
            Usages: Digital Signature
                    Non-Repudiation
                    Key Encipherment
                    Data Encipherment

            Name: Extended Key Usage
                TLS Web Server Authentication Certificate
                TLS Web Client Authentication Certificate

            Name: CRL Distribution Points
            Distribution point:
                URI: "http://ipa-ca.idm.example.com/ipa/crl/MasterCRL.bin"
                CRL issuer:
                    Directory Name: "CN=Certificate Authority,O=ipaca"

            Name: Certificate Subject Key ID
            Data:
                43:23:9f:c1:cf:b1:9f:51:18:be:05:b5:44:dc:e6:ab:
                be:07:1f:36

    Signature Algorithm: PKCS #1 SHA-256 With RSA Encryption
    Signature:
        [...]
    Fingerprint (SHA-256):
        6A:F9:64:F7:F2:A2:B5:04:88:27:6E:B8:53:3E:44:3E:F5:75:85:91:34:ED:48:A8:0D:F0:31:5D:7B:C9:E0:EC
    Fingerprint (SHA1):
        B4:9A:59:9F:1C:A8:5D:0E:C1:A2:41:EC:FD:43:E0:80:5F:63:DF:29

    Mozilla-CA-Policy: false (attribute missing)
    Certificate Trust Flags:
        SSL Flags:
            User
        Email Flags:
            User
        Object Signing Flags:
            User
----

[role="_additional-resources"]
.Additional resources
* See `certutil(1)` man page.
