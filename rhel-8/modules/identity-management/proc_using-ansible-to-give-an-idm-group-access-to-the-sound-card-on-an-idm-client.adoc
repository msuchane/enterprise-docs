:_newdoc-version: 2.15.1
:_template-generated: 2024-02-22
:_mod-docs-content-type: PROCEDURE

[id="using-ansible-to-give-an-idm-group-access-to-the-sound-card-on-an-idm-client_{context}"]
= Using Ansible to give an IdM group access to the sound card on an IdM client

[role="_abstract"]
You can use the `ansible-freeipa` `idview` and `idoverridegroup` modules to make {IPA} (IdM) or Active Directory (AD) users members of the local `audio` group on an IdM client. This grants the IdM or AD users privileged access to the sound card on the host. 

The procedure uses the example of the *idview_for_host01* ID view to which the *audio* group ID override is added with the `GID`` of *63*, which corresponds to the GID of local `audio` groups on RHEL hosts. The *idview_for_host01* ID view is applied to an IdM client named *host01.idm.example.com*.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** You are using RHEL 
ifeval::[{ProductNumber} == 8]
8.10 
endif::[]
ifeval::[{ProductNumber} == 9]
9.4 
endif::[]
or later.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.


.Procedure

. [Optional] Identify the GID of the local `audio` group on a RHEL host:
+
[literal,subs="+quotes,attributes,verbatim"]
----
$ *getent group audio*
---------------------
audio:x:63
----

. On your Ansible control node, create an *give-idm-group-access-to-sound-card-on-idm-client.yml* playbook with the following tasks:
+
[literal,subs="+quotes,attributes,verbatim"]
----
---
- name: Playbook to give IdM group access to sound card on IdM client
  hosts: ipaserver
  become: false

  tasks:
  - name: Ensure the *audio* group exists in IdM
    ipagroup:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: *audio*

  - name: Ensure idview_for_host01 exists and is applied to host01.idm.example.com
    ipaidview:
      ipaadmin_password:  ”{{ ipaadmin_password }}"
      name: *idview_for_host01*
      host: *host01.idm.example.com*

  - name: Add an override for the IdM audio group with GID 63 to idview_for_host01 
    ipaidoverridegroup:
      ipaadmin_password: "{{ ipaadmin_password }}"
      idview: *idview_for_host01*
      anchor: *audio*
      GID: *63*
----

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory give-idm-group-access-to-sound-card-on-idm-client.yml*
....



.Verification

. On an IdM client, obtain IdM administrator’s credentials:
+
[literal,subs="+quotes,attributes,verbatim"]
----
$ kinit admin
Password:
----

. Create a test IdM user:
+
[literal,subs="+quotes,attributes,verbatim"]
----
$ ipa user-add testuser --first test --last user --password
User login [tuser]: 
Password: 
Enter Password again to verify: 
------------------
Added user "tuser"
------------------
----

. Add the user to the IdM audio group:
+
[literal,subs="+quotes,attributes,verbatim"]
----
$ ipa group-add-member --tuser audio
----

. Log in to host01.idm.example.com as tuser:
+
[literal,subs="+quotes,attributes,verbatim"]
----
$ *ssh tuser@host01.idm.example.com*
----

. Verify the group membership of the user:
+
[literal,subs="+quotes,attributes,verbatim"]
----
$ *id tuser*
uid=702801456(tuser) gid=63(audio) groups=63(audio)
----


[role="_additional-resources"]
.Additional resources

* The link:https://github.com/freeipa/ansible-freeipa/blob/master/README-idoverridegroup.md[idoverridegroup], link:https://github.com/freeipa/ansible-freeipa/blob/master/README-idview.md[idview] and link:https://github.com/freeipa/ansible-freeipa/blob/master/README-group.md[ipagroup] `ansible-freeipa` upstream documentation
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_idm_users_groups_hosts_and_access_control_rules/managing-user-groups-in-idm-cli_managing-users-groups-hosts#enabling-group-merging-for-local-and-remote-groups-in-idm_managing-user-groups-in-idm-cli[Enabling group merging for local and remote groups in IdM]

