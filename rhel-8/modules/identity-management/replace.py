import re
import glob

def nahrad(expr):
    files = glob.glob(expr)
    for i in range(len(files)):
        print(files[i])
        nahradREAL(files[i])


def nahradREAL(somefileTXT):
    text = ''.join(open(somefileTXT).readlines())
    #textNEW = text.replace("xref:using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line","link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_and_managing_identity_management/index#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line")
    #textNEW = text.replace("You have installed the IdM replicas and clients .*","The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.")
    #textALT = re.sub("You have installed the IdM replicas and clients .*", "The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.", text)
    spatnySeznam = re.findall(r'\n\* [a-z]', text)
    print("spatnySeznam", spatnySeznam)
    try:
        castVtextu = "\n* "+spatnySeznam[0][-1]
        print("castVtextu", castVtextu)
        uppercasePismeno = (castVtextu[-1]).upper()
        if uppercasePismeno != "L" and uppercasePismeno != "X":
            textALT = text.replace(castVtextu, castVtextu[:-1]+uppercasePismeno)
            print("textALT", textALT)
            #print(textALT)
            outfile = open(somefileTXT+"updated", 'w')
            outfile.write(textALT)
            outfile.close()
    except IndexError:
        pass

if __name__ == "__main__":
    # execute only if run as a script
    nahrad("*.adoc")
