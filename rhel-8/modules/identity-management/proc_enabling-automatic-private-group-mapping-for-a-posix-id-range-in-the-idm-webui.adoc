// This module included in the following assemblies:
// rhel-8/assemblies/assembly_planning-a-cross-forest-trust-between-ipa-and-ad.adoc
// rhel-8/assemblies/assembly_setting-up-a-trust-using-the-command-line.adoc

[id="proc_enabling-automatic-private-group-mapping-for-a-posix-id-range-in-the-idm-webui_{context}"]
= Enabling automatic private group mapping for a POSIX ID range in the IdM WebUI

[role="_abstract"]
By default, SSSD does not map private groups for Active Directory (AD) users if you have established a POSIX trust that relies on POSIX data stored in AD. If any AD users do not have primary groups configured, IdM is not be able to resolve them.

This procedure explains how to enable automatic private group mapping for an ID range by setting the `hybrid` option for the `auto_private_groups` SSSD parameter in the Identity Management (IdM) WebUI. As a result, IdM is able to resolve AD users that do not have primary groups configured in AD.


.Prerequisites

* You have successfully established a POSIX cross-forest trust between your IdM and AD environments.

.Procedure

. Log into the IdM Web UI with your user name and password.

. Open the *IPA Server* → *ID Ranges* tab.

. Select the ID range you want to modify, such as `AD.EXAMPLE.COM_id_range`.

. From the *Auto private groups* drop down menu, select the `hybrid` option.
+
image:idm-auto-private-group-posix.png[Screenshot of the ID Ranges tab of the IPA Server section of the IdM WebUI. A user selects the hybrid option from the Auth private groups dropdown menu.]

. Click the *Save* button to save your changes.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/planning_identity_management/planning-a-cross-forest-trust-between-idm-and-ad_planning-identity-management#ref_options-for-automatically-mapping-private-groups-for-ad-users-id-mapping_planning-a-cross-forest-trust-between-idm-and-ad[Options for automatically mapping private groups for AD users]
