:_mod-docs-content-type: PROCEDURE
[id="using-an-Ansible-playbook-to-ensure-sudo-access-for-an-IdM-user-on-an-IdM-client_{context}"]
= Using an Ansible playbook to ensure sudo access for an IdM user on an IdM client

[role="_abstract"]
In {IPA} (IdM), you can ensure `sudo` access to a specific command is granted to an IdM user account on a specific IdM host.

Complete this procedure to ensure a `sudo` rule named *idm_user_reboot* exists. The rule grants *idm_user* the permission to run the `/usr/sbin/reboot` command on the *idmclient* machine.


.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

* You have xref:ensuring-the-presence-of-an-IdM-user-using-an-Ansible-playbook_managing-idm-users-using-Ansible-playbooks[ensured the presence of a user account for *idm_user* in IdM and unlocked the account by creating a password for the user]. For details on adding a new IdM user using the command-line interface, see link:
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/managing-user-accounts-using-the-command-line_configuring-and-managing-idm#adding-users-using-the-command-line_managing-idm-users-using-the-command-line[Adding users using the command line].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_idm_users_groups_hosts_and_access_control_rules/managing-user-accounts-using-the-command-line_managing-users-groups-hosts#adding-users-using-the-command-line_managing-idm-users-using-the-command-line[Adding users using the command line].
endif::[]

* No local *idm_user* account exists on *idmclient*. The *idm_user* user is not listed in the `/etc/passwd` file on *idmclient*.


.Procedure

. Create an inventory file, for example `inventory.file`, and define `ipaservers` in it:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaservers]
server.idm.example.com
....

.  Add one or more `sudo` commands:

+
--
.. Create an `ensure-reboot-sudocmd-is-present.yml` Ansible playbook that ensures the presence of the `/usr/sbin/reboot` command in the IdM database of `sudo` commands. To simplify this step, you can copy and modify the example in the `/usr/share/doc/ansible-freeipa/playbooks/sudocmd/ensure-sudocmd-is-present.yml` file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to manage sudo command
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  # Ensure sudo command is present
  - ipasudocmd:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: /usr/sbin/reboot
      state: present
....

+
.. Run the playbook:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i __path_to_inventory_directory/inventory.file path_to_playbooks_directory__/ensure-reboot-sudocmd-is-present.yml*
....
--

. Create a `sudo` rule that references the commands:

+
--
.. Create an `ensure-sudorule-for-idmuser-on-idmclient-is-present.yml` Ansible playbook that uses the `sudo` command entry to ensure the presence of a sudo rule. The sudo rule allows *idm_user* to reboot the *idmclient* machine. To simplify this step, you can copy and modify the example in the `/usr/share/doc/ansible-freeipa/playbooks/sudorule/ensure-sudorule-is-present.yml` file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Tests
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  # Ensure a sudorule is present granting idm_user the permission to run /usr/sbin/reboot on idmclient
  - ipasudorule:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: idm_user_reboot
      description: A test sudo rule.
      allow_sudocmd: /usr/sbin/reboot
      host: idmclient.idm.example.com
      user: idm_user
      state: present
....

+
.. Run the playbook:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook -v -i __path_to_inventory_directory/inventory.file path_to_playbooks_directory__/ensure-sudorule-for-idmuser-on-idmclient-is-present.yml*
....
--

.Verification steps

Test that the `sudo` rule whose presence you have ensured on the IdM server works on *idmclient* by verifying that *idm_user* can reboot *idmclient* using `sudo`. Note that it can take a few minutes for the changes made on the server to take effect on the client.

. Log in to *idmclient* as *idm_user*.

. Reboot the machine using `sudo`. Enter the password for *idm_user* when prompted:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *sudo /usr/sbin/reboot*
[sudo] password for idm_user:
....

If `sudo` is configured correctly, the machine reboots.

[role="_additional-resources"]
.Additional resources
* See the `README-sudocmd.md`, `README-sudocmdgroup.md`, and `README-sudorule.md` files in the `/usr/share/doc/ansible-freeipa/` directory.
