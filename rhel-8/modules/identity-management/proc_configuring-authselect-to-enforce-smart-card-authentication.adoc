:_mod-docs-content-type: PROCEDURE
[id="configuring-authselect-to-enforce-smart-card-authentication_{context}"]
= Configuring your system to enforce smart card authentication

[role="_abstract"]
The `authselect` tool enables you to configure smart card authentication on your system and to disable the default password authentication.
The `authselect` command includes the following options:

* `with-smartcard` -- enables smart card authentication in addition to password authentication
* `with-smartcard-required`  -- enables smart card authentication and disables password authentication

[NOTE]
====
The `with-smartcard-required` option only enforces exclusive smart card authentication for login services, such as `login`, `gdm`, `xdm`, `kdm`, `xscreensaver`, `gnome-screensaver`, and `kscreensaver`. Other services, such as `su` or `sudo` for switching users, do not use smart card authentication by default and will continue to prompt you for a password.
====


.Prerequisites

* Smart card contains your certificate and private key.
* The card is inserted into the reader and connected to the computer.
* The `authselect` tool is installed on your local system.

.Procedure

* Enter the following command to enforce smart card authentication:
+
[literal]
....
# authselect select sssd with-smartcard  with-smartcard-required --force
....

[NOTE]
====
Once you run this command, password authentication will no longer work and you can only log in with a smart card. Ensure smart card authentication is working before running this command or you may be locked out of your system.
====
