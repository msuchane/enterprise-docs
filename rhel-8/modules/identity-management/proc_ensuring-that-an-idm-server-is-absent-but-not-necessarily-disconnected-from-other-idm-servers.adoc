:_mod-docs-content-type: PROCEDURE
[id="ensuring-that-an-idm-server-is-absent-but-not-necessarily-disconnected-from-other-idm-servers_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Ensuring that an IdM server is absent but not necessarily disconnected from other IdM servers

[role="_abstract"]
If you are removing an {IPA} (IdM) server from the topology, you can keep its replication agreements intact with an Ansible playbook. The playbook also ensures that the IdM server does not exist in IdM, even as a host.

[IMPORTANT]
====
Ignoring a server's replication agreements when removing it is only recommended when the other servers are dysfunctional servers that you are planning to remove anyway. Removing a server that serves as a central point in the topology can split your topology into two disconnected clusters.

You can remove a dysfunctional server from the topology with the `ipa server-del` command.
====

NOTE: If you remove the last server that serves as a certificate authority (CA), key recovery authority (KRA), or DNS server, you seriously disrupt the {IPA} (IdM) functionality. To prevent this problem, the playbook makes sure these services are running on another server in the domain before it uninstalls a server that serves as a CA, KRA, or DNS server.

In contrast to the `ansible-freeipa` `ipaserver` role, the `ipaserver` module used in this playbook does not uninstall IdM services from the server.


.Prerequisites

* You know the IdM `admin` password.
* You have configured your Ansible control node to meet the following requirements:
** You are using Ansible version 2.14 or later.
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
** The `SSH` connection from the control node to the IdM server defined in the inventory file is working correctly.


.Procedure

. Navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Copy the `server-absent-ignore_topology_disconnect.yml` Ansible playbook file located in the `/usr/share/doc/ansible-freeipa/playbooks/server/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/server/server-absent-ignore_topology_disconnect.yml server-absent-ignore_topology_disconnect-copy.yml*
....

. Open the `server-absent-ignore_topology_disconnect-copy.yml` file for editing.

. Adapt the file by setting the following variables in the `ipaserver` task section and save the file:

* Set the `ipaadmin_password` variable to the password of the IdM `admin`.
* Set the `name` variable to the `FQDN` of the server. The `FQDN` of the example server is *server123.idm.example.com*.
* Ensure that the `ignore_topology_disconnect` variable is set to `yes`.
* Ensure that the `state` variable is set to `absent`.

+
--

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Server absent with ignoring topology disconnects example
  hosts: ipaserver
  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure server “server123.idm.example.com” with ignoring topology disconnects
    ipaserver:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: server123.idm.example.com*
      *ignore_topology_disconnect: yes*
      *state: absent*
....
--

. Run the Ansible playbook and specify the playbook file and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory server-absent-ignore_topology_disconnect-copy.yml*
....

. [Optional] Make sure all name server (NS) DNS records pointing to *server123.idm.example.com* are deleted from your DNS zones. This applies regardless of whether you use integrated DNS managed by IdM or external DNS.


[role="_additional-resources"]
.Additional resources
* See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/uninstalling-an-ipa-server_installing-identity-management[Uninstalling an IdM server].
* See the `README-server.md` file in the `/usr/share/doc/ansible-freeipa/` directory.
* See sample playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/server` directory.
