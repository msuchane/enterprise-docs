:_mod-docs-content-type: PROCEDURE
//:parent-context: {context}
// Module included in the following assemblies:
//
// logging-in-to-ipa-from-the-command-line.adoc

[id="destroying-the-users-active-kerberos-ticket-idm_{context}"]
= Destroying a user's active Kerberos ticket

[role="_abstract"]
Follow this procedure to clear the credentials cache that contains the user's active Kerberos ticket.

.Procedure

. To destroy your Kerberos ticket:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[example_user@server ~]$ *kdestroy*
....

+
. Optionally, to check that the Kerberos ticket has been destroyed:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[example_user@server ~]$ *klist*
*klist: Credentials cache keyring 'persistent:0:0' not found*
....

//:context: {parent-context}
