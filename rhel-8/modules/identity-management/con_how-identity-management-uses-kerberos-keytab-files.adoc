:_mod-docs-content-type: CONCEPT

[id="con_how-identity-management-uses-kerberos-keytab-files_{context}"]
= How Identity Management uses Kerberos keytab files

A Kerberos keytab is a file containing Kerberos principals and their corresponding encryption keys. Hosts, services, users, and scripts can use keytabs to authenticate to the Kerberos Key Distribution Center (KDC) securely, without requiring human interaction.

Every IdM service on an IdM server has a unique Kerberos principal stored in the Kerberos database. For example, if IdM servers `east.idm.example.com` and `west.idm.example.com` provide DNS services, IdM creates 2 unique DNS Kerberos principals to identify these services, which follow the naming convention `<service>/host.domain.com@REALM.COM`:

* `DNS/east.idm.example.com@IDM.EXAMPLE.COM`
* `DNS/west.idm.example.com@IDM.EXAMPLE.COM`

IdM creates a keytab on the server for each of these services to store a local copy of the Kerberos keys, along with their Key Version Numbers (KVNO). For example, the default keytab file `/etc/krb5.keytab` stores the `host` principal, which represents that machine in the Kerberos realm and is used for login authentication. The KDC generates encryption keys for the different encryption algorithms it supports, such as `aes256-cts-hmac-sha1-96` and `aes128-cts-hmac-sha1-96`.

You can display the contents of a keytab file with the `klist` command:

[literal,subs="+quotes,verbatim"]
....
[root@idmserver ~]# klist -ekt /etc/krb5.keytab
Keytab name: FILE:/etc/krb5.keytab
KVNO Timestamp           Principal
---- ------------------- ------------------------------------------------------
   2 02/24/2022 20:28:09 host/idmserver.idm.example.com@IDM.EXAMPLE.COM (aes256-cts-hmac-sha1-96)
   2 02/24/2022 20:28:09 host/idmserver.idm.example.com@IDM.EXAMPLE.COM (aes128-cts-hmac-sha1-96)
   2 02/24/2022 20:28:09 host/idmserver.idm.example.com@IDM.EXAMPLE.COM (camellia128-cts-cmac)
   2 02/24/2022 20:28:09 host/idmserver.idm.example.com@IDM.EXAMPLE.COM (camellia256-cts-cmac)
....

[role="_additional-resources"]
.Additional resources
* xref:proc_verifying-that-kerberos-keytab-files-are-in-sync-with-the-idm-database_assembly_maintaining-idm-kerberos-keytab-files[Verifying that Kerberos keytab files are in sync with the IdM database]
* xref:ref_list-of-idm-kerberos-keytab-files-and-their-contents_assembly_maintaining-idm-kerberos-keytab-files[List of IdM Kerberos keytab files and their contents]
