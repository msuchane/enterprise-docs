:_mod-docs-content-type: CONCEPT

[id="direct-integration-of-linux-systems-into-active-directory_{context}"]
= Direct integration of Linux systems into Active Directory


In direct integration, Linux systems are connected directly to {AD} (AD). The following types of integration are possible:

Integration with the System Security Services Daemon (SSSD):: SSSD can connect a Linux system with various identity and authentication stores: AD, {IPA} (IdM), or a generic LDAP or Kerberos server.
+
Notable requirements for integration with SSSD:
+
--
* When integrating with AD, SSSD works only within a single AD forest by default. For multi-forest setup, configure manual domain enumeration.
// as described in this Knowledgebase solution: link:++https://access.redhat.com/solutions/2710131++[Joining SSSD to domains in different forests].
+
* Remote AD forests must trust the local forest to ensure that the `idmap_ad` plug-in handles remote forest users correctly.
--
+
SSSD supports both direct and indirect integration. It also enables switching from one integration approach to the other without significant migration costs.

Integration with Samba Winbind:: The Winbind component of the Samba suite emulates a Windows client on a Linux system and communicates with AD servers.
+
Notable requirements for integration with Samba Winbind:
+
* Direct integration with Winbind in a multi-forest AD setup requires bidirectional trusts.
+
* A bidirectional path from the local domain of a Linux system must exist to the domain of a user in a remote AD forest to allow full information about the user from the remote AD domain to be available to the `idmap_ad` plug-in.

[discrete]
== Recommendations

* SSSD satisfies most of the use cases for AD integration and provides a robust solution as a generic gateway between a client system and different types of identity and authentication providers - AD, IdM, Kerberos, and LDAP.
* Winbind is recommended for deployment on those AD domain member servers on which you plan to deploy Samba FS.

////
.Additional resources

For more information in direct integration, see the link:++https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/windows_integration_guide/index++[Red Hat Enterprise Linux 7 Windows Integration Guide].
////
