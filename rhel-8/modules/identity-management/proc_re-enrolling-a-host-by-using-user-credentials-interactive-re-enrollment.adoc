:_mod-docs-content-type: PROCEDURE
[id="re-enrolling-a-client-by-using-user-credentials-interactive-re-enrollment_{context}"]
= Re-enrolling a client by using user credentials: Interactive re-enrollment

[role="_abstract"]
Follow this procedure to re-enroll an {IPA} client interactively by using the credentials of an authorized user.

. Re-create the client machine with the same host name.

. Run the [command]`ipa-client-install --force-join` command on the client machine:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *ipa-client-install --force-join*
....

. The script prompts for a user whose identity will be used to re-enroll the client. This could be, for example, a `hostadmin` user with the Enrollment Administrator role:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
User authorized to enroll computers: `hostadmin`
Password for `hostadmin`@`EXAMPLE.COM`:
....

[role="_additional-resources"]
.Additional resources
* See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/assembly_installing-an-idm-client_installing-identity-management#proc_installing-a-client-by-using-user-credentials-interactive-installation_assembly_installing-an-idm-client[Installing a client by using user credentials: Interactive installation] in _Installing {IPA}_.
