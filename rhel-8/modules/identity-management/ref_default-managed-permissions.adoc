:_mod-docs-content-type: REFERENCE
[id="default-managed-permissions_{context}"]
= Default managed permissions

[role="_abstract"]
Managed permissions are permissions that come by default with IdM. They behave like other permissions created by the user, with the following differences:

* You cannot delete them or modify their name, location, and target attributes.
* They have three sets of attributes:
  - *Default* attributes, the user cannot modify them, as they are managed by IdM
  - *Included* attributes, which are additional attributes added by the user
  - *Excluded* attributes, which are attributes removed by the user

A managed permission applies to all attributes that appear in the default and included attribute sets but not in the excluded set.

NOTE: While you cannot delete a managed permission, setting its bind type to permission and removing the managed permission from all privileges effectively disables it.

Names of all managed permissions start with `System:`, for example `System: Add Sudo rule` or `System: Modify Services`.
Earlier versions of IdM used a different scheme for default permissions. For example, the user could not delete them and was only able to assign them to privileges.
Most of these default permissions have been turned into managed permissions, however, the following permissions still use the previous scheme:

* Add Automember Rebuild Membership Task
* Add Configuration Sub-Entries
* Add Replication Agreements
* Certificate Remove Hold
* Get Certificates status from the CA
* Read DNA Range
* Modify DNA Range
* Read PassSync Managers Configuration
* Modify PassSync Managers Configuration
* Read Replication Agreements
* Modify Replication Agreements
* Remove Replication Agreements
* Read LDBM Database Configuration
* Request Certificate
* Request Certificate ignoring CA ACLs
* Request Certificates from a different host
* Retrieve Certificates from the CA
* Revoke Certificate
* Write IPA Configuration

NOTE: If you attempt to modify a managed permission from the command line, the system does not allow you to change the attributes that you cannot modify, the command fails.
If you attempt to modify a managed permission from the Web UI, the attributes that you cannot modify are disabled.

////
Disabled Attributes
If you attempt to modify a managed permission from the command line, the system will not allow you to change the attributes that you cannot modify. For example, attempting to change a default System: Modify Users permission to apply to groups fails:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa permission-mod 'System: Modify Users' --type=group
ipa: ERROR: invalid 'ipapermlocation': not modifiable on managed permissions
You can, however, make the System: Modify Users permission not to apply to an attribute:
$ ipa permission-mod 'System: Modify Users' --excludedattrs=example_attribute
------------------------------------------
Modified permission "System: Modify Users"
////
