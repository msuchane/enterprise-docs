:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_recovering-from-data-loss-with-snapshots.adoc


[id="recovering-from-snapshot-partially-working_{context}"]
= Recovering from a VM snapshot among a partially-working environment

[role="_abstract"]
If a disaster affects some IdM servers while others are still operating properly, you may want to restore the deployment to the state captured in a Virtual Machine (VM) snapshot. For example, if all Certificate Authority (CA) Replicas are lost while other replicas are still in production, you will need to bring a CA Replica back into the environment.

In this scenario, remove references to the lost replicas, restore the CA replica from the snapshot, verify replication, and deploy new replicas.


.Prerequisites

* You have prepared a VM snapshot of a CA replica VM. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/preparing_for_disaster_recovery_with_identity_management/preparing-for-data-loss-with-snapshots_preparing-for-disaster-recovery[Preparing for data loss with VM snapshots].


.Procedure

. Remove all replication agreements to the lost servers. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/uninstalling-an-ipa-server_installing-identity-management[Uninstalling an IdM server].

. Boot the desired snapshot of the CA replica VM.

. Remove any replication agreements between the restored server and any lost servers.
+
[literal,subs="+quotes,attributes"]
....
[root@restored-CA-replica ~]# *ipa server-del lost-server1.example.com*
[root@restored-CA-replica ~]# *ipa server-del lost-server2.example.com*
...
....

. If the restored server does not have replication agreements to any of the servers still in production, connect the restored server with one of the other servers to update the restored server.
+
[literal,subs="+quotes,attributes"]
....
[root@restored-CA-replica ~]# *ipa topologysegment-add*
Suffix name: *domain*
Left node: *restored-CA-replica.example.com*
Right node: *server3.example.com*
Segment name [restored-CA-replica.com-to-server3.example.com]: *new_segment*
---------------------------
Added segment "new_segment"
---------------------------
  Segment name: new_segment
  Left node: restored-CA-replica.example.com
  Right node: server3.example.com
  Connectivity: both
....

. Review Directory Server error logs at `/var/log/dirsrv/slapd-YOUR-INSTANCE/errors` to see if the CA replica from the snapshot correctly synchronizes with the remaining IdM servers.

. If replication on the restored server fails because its database is too outdated, reinitialize the restored server.
+
[literal,subs="+quotes,attributes"]
....
[root@restored-CA-replica ~]# *ipa-replica-manage re-initialize --from server2.example.com*
....

. If the database on the restored server is correctly synchronized, continue by deploying additional replicas with the desired services (CA, DNS) according to link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-ipa-replica_installing-identity-management[Installing an IdM replica].


.Verification steps

. Test the Kerberos server on every replica by successfully retrieving a Kerberos ticket-granting ticket as an IdM user.
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *kinit admin*
Password for admin@EXAMPLE.COM:

[root@server ~]# *klist*
Ticket cache: KCM:0
Default principal: admin@EXAMPLE.COM

Valid starting       Expires              Service principal
10/31/2019 15:51:37  11/01/2019 15:51:02  HTTP/server.example.com@EXAMPLE.COM
10/31/2019 15:51:08  11/01/2019 15:51:02  *krbtgt/EXAMPLE.COM@EXAMPLE.COM*
....

. Test the Directory Server and SSSD configuration on every replica by retrieving user information.
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *ipa user-show admin*
  User login: admin
  Last name: Administrator
  Home directory: /home/admin
  Login shell: /bin/bash
  Principal alias: admin@EXAMPLE.COM
  UID: 1965200000
  GID: 1965200000
  Account disabled: False
  Password: True
  Member of groups: admins, trust admins
  Kerberos keys available: True
....

. Test the CA server on every CA replica with the `ipa cert-show` command.
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *ipa cert-show 1*
  Issuing CA: ipa
  Certificate: MIIEgjCCAuqgAwIBAgIjoSIP...
  Subject: CN=Certificate Authority,O=EXAMPLE.COM
  Issuer: CN=Certificate Authority,O=EXAMPLE.COM
  Not Before: Thu Oct 31 19:43:29 2019 UTC
  Not After: Mon Oct 31 19:43:29 2039 UTC
  Serial number: 1
  Serial number (hex): 0x1
  Revoked: False
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_disaster_recovery_with_identity_management/recovering-from-data-loss-with-snapshots_performing-disaster-recovery#recovering-from-snapshot-new_recovering-from-data-loss-with-snapshots[Recovering from a VM snapshot to establish a new IdM environment].
