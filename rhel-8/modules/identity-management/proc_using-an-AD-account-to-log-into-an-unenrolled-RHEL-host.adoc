:_mod-docs-content-type: PROCEDURE
[id="configuring-a-RHEL-host-to-use-AD-as-an-authentication-provider_{context}"]
= Configuring a RHEL host to use AD as an authentication provider

[role="_abstract"]
As a system administrator, you can use {AD} (AD) as the authentication provider for a {RHEL} (RHEL) host without joining the host to AD.

This can be done if, for example:

* You do not want to grant AD administrators the control over enabling and disabling the host.
* The host, which can be a corporate PC, is only meant to be used by one user in your company.

[IMPORTANT]
====
Implement this procedure only in the rare cases where this approach is preferred.

Consider fully joining the system to AD or {RH} {IPA} (IdM) instead. Joining the RHEL host to a domain makes the setup easier to manage. If you are concerned about client access licences related to joining clients into AD directly, consider leveraging an IdM server that is in a trust agreement with AD. For more information about an IdM-AD trust, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/planning_identity_management/planning-a-cross-forest-trust-between-idm-and-ad_planning-identity-management[Planning a cross-forest trust between IdM and AD] and
ifeval::[{ProductNumber} == 8]
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_identity_management/installing-trust-between-idm-and-ad_installing-identity-management[Installing a trust between IdM and AD].
endif::[]
ifeval::[{ProductNumber} == 9]
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/installing_trust_between_idm_and_ad/index[Installing a trust between IdM and AD].
endif::[]
====

This procedure enables the user named *AD_user* to log in to the *rhel_host* system using the password set in the {AD} (AD) user database in the *example.com* domain. In this example, the *EXAMPLE.COM* Kerberos realm corresponds to the *example.com* domain.

.Prerequisites

* You have root access to *rhel_host*.
* The *AD_user* user account exists in the *example.com* domain.
* The Kerberos realm is *EXAMPLE.COM*.
* *rhel_host* has not been joined to AD using the `realm join` command.

.Procedure

. Create the *AD_user* user account locally without assigning a password to it:

+
[literal,subs="+quotes,attributes"]
....
# useradd AD_user
....

. Open the `/etc/nsswitch.conf` file for editing, and make sure that it contains the following lines:
+
[literal,subs="+quotes,attributes"]
....
passwd:     sss files systemd
group:      sss files systemd
shadow:     files sss
....

. Open the `/etc/krb5.conf` file for editing, and make sure that it contains the following sections and items:
+
[literal,subs="+quotes,attributes"]
....
# To opt out of the system crypto-policies configuration of krb5, remove the
# symlink at /etc/krb5.conf.d/crypto-policies which will not be recreated.
includedir /etc/krb5.conf.d/

[logging]
    default = FILE:/var/log/krb5libs.log
    kdc = FILE:/var/log/krb5kdc.log
    admin_server = FILE:/var/log/kadmind.log

[libdefaults]
    dns_lookup_realm = false
    ticket_lifetime = 24h
    renew_lifetime = 7d
    forwardable = true
    rdns = false
    pkinit_anchors = /etc/pki/tls/certs/ca-bundle.crt
    spake_preauth_groups = edwards25519
    default_realm = EXAMPLE.COM
    default_ccache_name = KEYRING:persistent:%{uid}

[realms]
 EXAMPLE.COM = {
     kdc = ad.example.com
     admin_server = ad.example.com
 }

[domain_realm]
 .example.com = EXAMPLE.COM
 example.com = EXAMPLE.COM
....


. Create the `/etc/sssd/sssd.conf` file and insert the following sections and lines into it:
+
[literal,subs="+quotes,attributes"]
....
[sssd]
    services = nss, pam
    domains = EXAMPLE.COM

[domain/EXAMPLE.COM]
    id_provider = files
    auth_provider = krb5
    krb5_realm = EXAMPLE.COM
    krb5_server = ad.example.com
....

. Change the permissions on the `/etc/sssd/sssd.conf` file:
+
[literal,subs="+quotes,attributes"]
....
# chmod 600 /etc/sssd/sssd.conf
....

. Start the Security System Services Daemon (SSSD):
+
[literal,subs="+quotes,attributes"]
....
# systemctl start sssd
....

. Enable SSSD:
+
[literal,subs="+quotes,attributes"]
....
# systemctl enable sssd
....

. Open the `/etc/pam.d/system-auth` file, and modify it so that it contains the following sections and lines:
+
[literal,subs="+quotes,attributes"]
....
# Generated by authselect on Wed May  8 08:55:04 2019
# Do not modify this file manually.

auth        required                                     pam_env.so
auth        required                                     pam_faildelay.so delay=2000000
auth        [default=1 ignore=ignore success=ok]         pam_succeed_if.so uid >= 1000 quiet
auth        [default=1 ignore=ignore success=ok]         pam_localuser.so
auth        sufficient                                   pam_unix.so nullok try_first_pass
auth        requisite                                    pam_succeed_if.so uid >= 1000 quiet_success
auth        sufficient                                   pam_sss.so forward_pass
auth        required                                     pam_deny.so

account     required                                     pam_unix.so
account     sufficient                                   pam_localuser.so
account     sufficient                                   pam_succeed_if.so uid < 1000 quiet
account     [default=bad success=ok user_unknown=ignore] pam_sss.so
account     required                                     pam_permit.so

password    requisite                                    pam_pwquality.so try_first_pass local_users_only
password    sufficient                                   pam_unix.so sha512 shadow nullok try_first_pass use_authtok
password    sufficient                                   pam_sss.so use_authtok
password    required                                     pam_deny.so

session     optional                                     pam_keyinit.so revoke
session     required                                     pam_limits.so
-session    optional                                     pam_systemd.so
session     [success=1 default=ignore]                   pam_succeed_if.so service in crond quiet use_uid
session     required                                     pam_unix.so
session     optional                                     pam_sss.so
....

. Copy the contents of the `/etc/pam.d/system-auth` file into the `/etc/pam.d/password-auth` file. Enter *yes* to confirm the overwriting of the current contents of the file:
+
[literal,subs="+quotes,attributes"]
....
# cp /etc/pam.d/system-auth /etc/pam.d/password-auth
cp: overwrite '/etc/pam.d/password-auth'? yes
....

.Verification steps


. Request a Kerberos ticket-granting ticket (TGT) for *AD_user*. Enter the password of *AD_user* as requested:
+
[literal,subs="+quotes,attributes"]
....

# kinit AD_user
Password for AD_user@EXAMPLE.COM:
....

. Display the obtained TGT:
+
[literal,subs="+quotes,attributes"]
....

# klist
Ticket cache: KEYRING:persistent:0:0
Default principal: AD_user@EXAMPLE.COM

Valid starting     Expires            Service principal
11/02/20 04:16:38  11/02/20 14:16:38  krbtgt/EXAMPLE.COM@EXAMPLE.COM
	renew until 18/02/20 04:16:34
....


*AD_user* has successfully logged in to *rhel_host* using the credentials from the *EXAMPLE.COM* Kerberos domain.
