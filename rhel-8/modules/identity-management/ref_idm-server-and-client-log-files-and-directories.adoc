:_mod-docs-content-type: REFERENCE

[id="ref_idm-server-and-client-log-files-and-directories_{context}"]
= IdM server and client log files and directories

The following table presents directories and files that the {IPA} (IdM) server and client use to log information. You can use the files and directories for troubleshooting installation errors.

[options="header"]
[cols="2,3"]
|===
|Directory or File|Description
|`/var/log/ipaserver-install.log`|The installation log for the IdM server.
|`/var/log/ipareplica-install.log`|The installation log for the IdM replica.
|`/var/log/ipaclient-install.log`|The installation log for the IdM client.
|`/var/log/sssd/`|Log files for SSSD. You can link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_authentication_and_authorization_in_rhel/assembly_troubleshooting-authentication-with-sssd-in-idm_restricting-domains-for-pam-services-using-sssd#proc_enabling-detailed-logging-for-sssd-in-the-sssdconf-file_assembly_troubleshooting-authentication-with-sssd-in-idm[enable detailed logging for SSSD in the sssd.conf file] or link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_authentication_and_authorization_in_rhel/assembly_troubleshooting-authentication-with-sssd-in-idm_restricting-domains-for-pam-services-using-sssd#proc_enabling-detailed-logging-for-sssd-with-the-sssctl-command_assembly_troubleshooting-authentication-with-sssd-in-idm[with the sssctl command].
|`~/.ipa/log/cli.log`|The log file for errors returned by remote procedure calls (RPCs) and responses by the `ipa` utility. Created in the home directory for the *effective user* that runs the tools. This user might have a different user name than the IdM user principal, that is the IdM user whose ticket granting ticket (TGT) has been obtained before attempting to perform the failed `ipa` commands. For example, if you are logged in to the system as `root` and have obtained the TGT of IdM `admin`, then the errors are logged in to the `/root/.ipa/log/cli.log` file.
|`/etc/logrotate.d/`|The log rotation policies for DNS, SSSD, Apache, Tomcat, and Kerberos.
|`/etc/pki/pki-tomcat/logging.properties`|This link points to the default Certificate Authority logging configuration at `/usr/share/pki/server/conf/logging.properties`.
|===



[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/troubleshooting-idm-server-installation_installing-identity-management[Troubleshooting IdM server installation]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/troubleshooting-idm-client-installation_installing-identity-management[Troubleshooting IdM client installation]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/troubleshooting-idm-replica-installation_installing-identity-management[Troubleshooting IdM replica installation]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_authentication_and_authorization_in_rhel/assembly_troubleshooting-authentication-with-sssd-in-idm_configuring-authentication-and-authorization-in-rhel[Troubleshooting authentication with SSSD in IdM]
