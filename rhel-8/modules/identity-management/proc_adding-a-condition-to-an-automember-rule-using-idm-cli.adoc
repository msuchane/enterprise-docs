:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="adding-a-condition-to-an-automember-rule-using-idm-cli_{context}"]
= Adding a condition to an automember rule using IdM CLI

[role="_abstract"]
After configuring automember rules, you can then add a condition to that automember rule using the IdM CLI. For information about automember rules, see xref:automember-rules_{context}[Automember rules].

.Prerequisites

* You must be logged in as the administrator. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/logging-in-to-ipa-from-the-command-line_configuring-and-managing-idm#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/logging-in-to-ipa-from-the-command-line_accessing-idm-services#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]

* The target rule must exist in IdM. For details, see xref:adding-an-automember-rule-using-idm-cli_{context}[Adding an automember rule using IdM CLI].

.Procedure

. Define one or more inclusive or exclusive conditions using the [command]`ipa automember-add-condition` command.

. When prompted, specify:
+
--
* *Automember rule*. This is the target rule name. See xref:automember-rules_{context}[Automember rules] for details.

* *Attribute Key*. This specifies the entry attribute to which the filter will apply. For example, *uid* for users.

* *Grouping Type*. This specifies whether the rule targets a user group or a host group. To target a user group, enter *group*. To target a host group, enter *hostgroup*.

* *Inclusive regex* and *Exclusive regex*. These specify one or more conditions as regular expressions. If you only want to specify one condition, press *Enter* when prompted for the other.
--

+
For example, the following condition targets all users with any value pass:[(.*)] in their user login attribute (*uid*).
+
[literal, subs="+quotes,verbatim"]
--
$ *ipa automember-add-condition*
Automember Rule: *user_group*
Attribute Key: *uid*
Grouping Type: *group*
[Inclusive Regex]: .*
[Exclusive Regex]:
----------------------------------
Added condition(s) to "user_group"
----------------------------------
  Automember Rule: user_group
  Inclusive Regex: uid=.*
----------------------------
Number of conditions added 1
----------------------------
--
+
As another example, you can use an automembership rule to target all Windows users synchronized from Active Directory (AD). To achieve this, create a condition that that targets all users with *ntUser* in their *objectClass* attribute, which is shared by all AD users:
+
[literal, subs="+quotes,verbatim"]
--
$ *ipa automember-add-condition*
Automember Rule: *ad_users*
Attribute Key: *objectclass*
Grouping Type: *group*
[Inclusive Regex]: *ntUser*
[Exclusive Regex]:
-------------------------------------
Added condition(s) to "ad_users"
-------------------------------------
  Automember Rule: ad_users
  Inclusive Regex: objectclass=ntUser
----------------------------
Number of conditions added 1
----------------------------
--

.Verification steps

* You can display existing automember rules and conditions in IdM using xref:viewing-existing-automember-rules-using-idm-cli_{context}[Viewing existing automember rules using IdM CLI].
