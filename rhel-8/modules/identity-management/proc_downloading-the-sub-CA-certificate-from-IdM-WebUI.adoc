:_mod-docs-content-type: PROCEDURE
[id="downloading-the-sub-CA-certificate-from-IdM-WebUI_{context}"]
= Downloading the sub-CA certificate from IdM WebUI

[role="_abstract"]
.Prerequisites

* Make sure that you have obtained the IdM administrator's credentials.

.Procedure

. In the *Authentication* menu, click *Certificates* > *Certificates*.

+
[id="sub-CA-certificate-in-the-list-of-certificates_{context}"]
.sub-CA certificate in the list of certificates
+
image::download-sub-CA-certificate.png[Screenshot of a table displaying two certificates.]

+
. Click the serial number of the sub-CA certificate to open the certificate information page.

. In the certificate information page, click *Actions* > *Download*.

. In the CLI, move the sub-CA certificate to the `/etc/pki/tls/private/` directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *mv _path/to/the/downloaded/certificate_ /etc/pki/tls/private/sub-ca.crt*
....
