:_mod-docs-content-type: PROCEDURE

[id="proc_granting-sudo-access-to-an-idm-user-on-an-idm-client-using-the-cli_{context}"]
= Granting sudo access to an IdM user on an IdM client using the CLI

[role="_abstract"]
In {IPA} (IdM), you can grant `sudo` access for a specific command to an IdM user account on a specific IdM host. First, add a `sudo` command and then create a `sudo` rule for one or more commands.

For example, complete this procedure to create the *idm_user_reboot* `sudo` rule to grant the *idm_user* account the permission to run the `/usr/sbin/reboot` command on the *idmclient* machine.

.Prerequisites

* You are logged in as IdM administrator.

* You have created a user account for *idm_user* in IdM and unlocked the account by creating a password for the user. For details on adding a new IdM user using the CLI, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_idm_users_groups_hosts_and_access_control_rules/managing-user-accounts-using-the-command-line_managing-users-groups-hosts#adding-users-using-the-command-line_managing-idm-users-using-the-command-line[Adding users using the command line].

* No local *idm_user* account is present on the *idmclient* host. The *idm_user* user is not listed in the local `/etc/passwd` file.

.Procedure

. Retrieve a Kerberos ticket as the IdM `admin`.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *kinit admin*
....

. Add the `/usr/sbin/reboot` command to the IdM database of `sudo` commands:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudocmd-add /usr/sbin/reboot*
-------------------------------------
Added Sudo Command "/usr/sbin/reboot"
-------------------------------------
  Sudo Command: /usr/sbin/reboot
....

. Create a `sudo` rule named *idm_user_reboot*:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-add _idm_user_reboot_*
---------------------------------
Added Sudo Rule "idm_user_reboot"
---------------------------------
  Rule name: idm_user_reboot
  Enabled: TRUE
....

. Add the `/usr/sbin/reboot` command to the *idm_user_reboot* rule:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-add-allow-command idm_user_reboot --sudocmds '/usr/sbin/reboot'*
  Rule name: idm_user_reboot
  Enabled: TRUE
  Sudo Allow Commands: /usr/sbin/reboot
-------------------------
Number of members added 1
-------------------------
....

. Apply the *idm_user_reboot* rule to the IdM *idmclient* host:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-add-host idm_user_reboot --hosts idmclient.idm.example.com*
Rule name: idm_user_reboot
Enabled: TRUE
Hosts: idmclient.idm.example.com
Sudo Allow Commands: /usr/sbin/reboot
-------------------------
Number of members added 1
-------------------------
....

. Add the *idm_user* account to the *idm_user_reboot* rule:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-add-user idm_user_reboot --users idm_user*
Rule name: idm_user_reboot
Enabled: TRUE
Users: idm_user
Hosts: idmclient.idm.example.com
Sudo Allow Commands: /usr/sbin/reboot
-------------------------
Number of members added 1
-------------------------
....

. Optionally, define the validity of the *idm_user_reboot* rule:

+
--
.. To define the time at which a `sudo` rule starts to be valid, use the `ipa sudorule-mod _sudo_rule_name_` command with the `--setattr sudonotbefore=_DATE_` option. The _DATE_ value must follow the *yyyymmddHHMMSSZ* format, with seconds specified explicitly. For example, to set the start of the validity of the *idm_user_reboot* rule to 31 December 2025 12:34:00, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-mod idm_user_reboot --setattr sudonotbefore=20251231123400Z*
....

.. To define the time at which a sudo rule stops being valid, use the `--setattr sudonotafter=DATE` option. For example, to set the end of the *idm_user_reboot* rule validity to 31 December 2026 12:34:00, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-mod idm_user_reboot --setattr sudonotafter=20261231123400Z*
....
--


[NOTE]
====
Propagating the changes from the server to the client can take a few minutes.
====

.Verification steps

. Log in to the *idmclient* host as the *idm_user* account.

. Display which `sudo` rules the *idm_user* account is allowed to perform.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm_user@idmclient ~]$ *sudo -l*
Matching Defaults entries for *idm_user* on *idmclient*:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin,
    env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS",
    env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE",
    env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES",
    env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE",
    env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY KRB5CCNAME",
    secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User *idm_user* may run the following commands on *idmclient*:
    *(root) /usr/sbin/reboot*
....

. Reboot the machine using `sudo`. Enter the password for *idm_user* when prompted:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm_user@idmclient ~]$ *sudo /usr/sbin/reboot*
[sudo] password for idm_user:
....
