:_mod-docs-content-type: REFERENCE
[id="hosts-actions_{context}"]
= Host Operations

[role="_abstract"]
The most common operations related to host enrollment and enablement, and the prerequisites, the context, and the consequences of performing those operations are outlined in the following sections.

[id='tab-managing-host-operations_{context}']
.Host operations part 1
[cols="1,1,1,3"]
|===
|Action|What are the prerequisites of the action?|When does it make sense to run the command?|How is the action performed by a system administrator? What command(s) does he run?

|`Enrolling a client`|see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/preparing-the-system-for-ipa-client-installation_installing-identity-management[Preparing the system for Identity Management client installation] in _Installing {IPA}_|When you want the host to join the IdM realm.|Enrolling machines as clients in the IdM domain is a two-part process. A host entry is created for the client (and stored in the 389 Directory Server instance) when the `ipa host-add` command is run, and then a keytab is created to provision the client.
Both parts are performed automatically by the `ipa-client-install` command. It is also possible to perform those steps separately; this allows for administrators to prepare machines and IdM in advance of actually configuring the clients. This allows more flexible setup scenarios, including bulk deployments.
//When performing a manual enrollment, the host entry is created separately, and then enrollment is completed when the client script is run, which creates the requisite keytab.
|`Disabling a client`|The host must have an entry in IdM. The host needs to have an active keytab.|When you want to remove the host from the IdM realm temporarily, perhaps for maintenance purposes.|`ipa host-disable host_name`
|`Enabling a client`|The host must have an entry in IdM.|When you want the temporarily disabled host to become active again.|`ipa-getkeytab`
|`Re-enrolling a client`|The host must have en entry in IdM.|When the original host has been lost but you have installed a host with the same host name.|`ipa-client-install --keytab` or `ipa-client-install --force-join`
|`Un-enrolling a client`|The host must have an entry in IdM.|When you want to remove the host from the IdM realm permanently.|`ipa-client-install --uninstall`
|===

[id='tab-managing-hosts-operations-2_{context}']
.Host operations part 2
[cols="1,1,3"]
|===
|Action|On which machine can the administrator run the command(s)?|What happens when the action is performed? What are the consequences for the host’s functioning in IdM? What limitations are introduced/removed?

|`Enrolling a client`|In the case of a two-step enrollment: `ipa host-add` can be run on any IdM client; the second step of `ipa-client-install` must be run on the client itself|By default this configures SSSD to connect to an IdM server for authentication and authorization. Optionally one can instead configure the Pluggable Authentication Module (PAM) and the Name Switching Service (NSS) to work with an IdM server over Kerberos and LDAP.
|`Disabling a client`|Any machine in IdM, even the host itself|The host's Kerberos key and SSL certificate are invalidated, and all services running on the host are disabled.
|`Enabling a client`|Any machine in IdM. If run on the disabled host, LDAP credentials need to be supplied.|The host's Kerberos key and the SSL certificate are made valid again, and all IdM services running on the host are re-enabled.
|`Re-enrolling a client`|The host to be re-enrolled. LDAP credentials need to be supplied.|A new Kerberos key is generated for the host, replacing the previous one.
//The host's Kerberos key and the SSL certificate are made valid again, and all IdM services running on the host are re-enabled.
|`Un-enrolling a client`|The host to be un-enrolled.|The command unconfigures IdM and attempts to return the machine to its previous state. Part of this process is to unenroll the host from the IdM server. Unenrollment consists of disabling the principal key on the IdM server. The machine principal in `/etc/krb5.keytab` (`host/<fqdn>@REALM`) is used to authenticate to the IdM server to unenroll itself. If this principal does not exist then unenrollment will fail and an administrator will need to disable the host principal (`ipa host-disable <fqdn>`).
|===
