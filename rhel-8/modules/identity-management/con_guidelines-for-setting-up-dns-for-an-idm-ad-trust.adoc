:_mod-docs-content-type: CONCEPT

[id="guidelines-for-setting-up-dns-for-an-idm-ad-trust_{context}"]
= Guidelines for setting up DNS for an IdM-AD trust


These guidelines can help you achieve the right DNS configuration for establishing a cross-forest trust between {IPA} (IdM) and {AD} (AD).

Unique primary DNS domains:: Ensure both AD and IdM have their own unique primary DNS domains configured. For example:
+
--
* `_ad.example.com_` for AD and `_idm.example.com_` for IdM
* `_example.com_` for AD and `_idm.example.com_` for IdM
--
+
The most convenient management solution is an environment where each DNS domain is managed by integrated DNS servers, but you can also use any other standard-compliant DNS server.

IdM and AD DNS Domains:: Systems joined to IdM can be distributed over multiple DNS domains. Red Hat recommends that you deploy IdM clients in a DNS zone different to the ones owned by Active Directory. The primary IdM DNS domain must have proper SRV records to support AD trusts.

[NOTE]
====
ifeval::[{ProductNumber} == 8]

In some environments with trusts between IdM and Active Directory, you can install an IdM client on a host that is part of the Active Directory DNS domain. The host can then benefit from the Linux-focused features of IdM. This is not a recommended configuration and has some limitations. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_identity_management/installing-trust-between-idm-and-ad_installing-identity-management#assembly_configuring-idm-clients-in-an-active-directory-dns-domain_installing-trust-between-idm-and-ad[Configuring IdM clients in an Active Directory DNS domain] for more details.

endif::[]

ifeval::[{ProductNumber} == 9]

In some environments with trusts between IdM and Active Directory, you can install an IdM client on a host that is part of the Active Directory DNS domain. The host can then benefit from the Linux-focused features of IdM. This is not a recommended configuration and has some limitations. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/installing_trust_between_idm_and_ad/assembly_configuring-idm-clients-in-an-active-directory-dns-domain_installing-trust-between-idm-and-ad[Configuring IdM clients in an Active Directory DNS domain] for more details.

endif::[]
====

Proper SRV records:: Ensure the primary IdM DNS domain has proper SRV records to support AD trusts.
+
For other DNS domains that are part of the same IdM realm, the SRV records do not have to be configured when the trust to AD is established. The reason is that AD domain controllers do not use SRV records to discover Kerberos key distribution centers (KDCs) but rather base the KDC discovery on name suffix routing information for the trust.

DNS records resolvable from all DNS domains in the trust:: Ensure all machines can resolve DNS records from all DNS domains involved in the trust relationship:
+
* When configuring the IdM DNS, follow the instructions described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-ipa-server-with-external-ca_installing-identity-management[Installing an IdM server with an external CA].
//xref:installing-an-ipa-server-with-external-ca[].
//the section on link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/install-server#install-server-dns[configuring DNS services within the IdM domain] and section on link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/managing-dns-forwarding[managing DNS forwarding] in the _Linux Domain Identity, Authentication, and Policy Guide_.
* If you are using IdM without integrated DNS, follow the instructions described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-ipa-server-without-integrated-dns_installing-identity-management[Installing an IdM server without integrated DNS].
// xref:installing-an-ipa-server-without-integrated-dns[].
//the section describing link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/install-server#install-server-without-dns[the server installation without integrated DNS] in the _Linux Domain Identity, Authentication, and Policy Guide_.

Kerberos realm names as upper-case versions of primary DNS domain names:: Ensure Kerberos realm names are the same as the primary DNS domain names, with all letters uppercase. For example, if the domain names are `_ad.example.com_` for AD and `_idm.example.com_` for IdM, the Kerberos realm names must be `_AD.EXAMPLE.COM_` and `_IDM.EXAMPLE.COM_`.
