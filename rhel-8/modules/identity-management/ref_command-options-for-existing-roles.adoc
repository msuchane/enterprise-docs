:_mod-docs-content-type: REFERENCE
[id="command-options-for-existing-roles{context}"]
= Command options for existing roles

[role="_abstract"]
Use the following variants to modify existing roles as needed:

* To modify existing roles, use the [command]`ipa role-mod` command.
* To find existing roles, use the [command]`ipa role-find` command.
* To view a specific role, use the [command]`ipa role-show` command.
* To remove a member from the role, use the [command]`ipa role-remove-member` command.
* The [command]`ipa role-remove-privilege` command removes one or more privileges from a role.
* The [command]`ipa role-del` command deletes a role completely.

[role="_additional-resources"]
.Additional resources
* See the `ipa` man page
* See the [command]`ipa help` command.
