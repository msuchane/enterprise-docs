[id="options-used-when-installing-an-idm-ca-with-an-external-ca-as-the-root-ca_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Options used when installing an IdM CA with an external CA as the root CA
// In the title of a reference module, include nouns that are used in the body text. For example, "Keyboard shortcuts for ___" or "Command options for ___." This helps readers and search engines find the information quickly.

You may want to install an {IPA} IdM certificate authority (CA) with an external CA as the root CA if one of the following conditions applies:

* You are installing a new IdM server or replica by using the `ipa-server-install` command.
* You are installing the CA component into an existing IdM server by using the `ipa-ca-install` command.

You can use following options for both commands that you can use for creating a certificate signing request (CSR) during the installation of an IdM CA with an external CA as the root CA.

--external-ca-type=__TYPE__::

Type of the external CA. Possible values are `_generic_` and `_ms-cs_`. The default value is `_generic_`. Use `_ms-cs_` to include a template name required by Microsoft Certificate Services (MS CS) in the generated CSR. To use a non-default profile, use the `--external-ca-profile` option in conjunction with `--external-ca-type=ms-cs`.

--external-ca-profile=__PROFILE_SPEC__::

Specify the certificate profile or template that you want the MS CS to apply when issuing the certificate for your IdM CA.

+
Note that the `--external-ca-profile` option can only be used if `--external-ca-type` is ms-cs.

+
You can identify the MS CS template in one of the following ways:

* `__<oid>:<majorVersion>[:<minorVersion>]__`. You can specify a certificate template by its object identifier (OID) and major version. You can optionally also specify the minor version.

* `_<name>_`. You can specify a certificate template by its name. The name cannot contain any *:* characters and cannot be an OID, otherwise the OID-based template specifier syntax takes precedence.

* `_default_`. If you use this specifier, the template name `__SubCA__` is used.

In certain scenarios, the {AD} (AD) administrator can use the `Subordinate Certification Authority` (SCA) template, which is a built-in template in AD CS, to create a unique template to better suit the needs of the organization. The new template can, for example, have a customized validity period and customized extensions. The associated Object Identifier (OID) can be found in the AD `Certificates Template` console.

If the AD administrator has disabled the original, built-in template, you must specify the OID or name of the new template when requesting a certificate for your IdM CA. Ask your AD administrator to provide you with the name or OID of the new template.

If the original SCA AD CS template is still enabled, you can use it by specifying `--external-ca-type=ms-cs` without additionally using the `--external-ca-profile` option. In this case, the `subCA` external CA profile is used, which is the default IdM template corresponding to the SCA AD CS template.
