:_mod-docs-content-type: PROCEDURE
[id="requesting-new-certificates-for-a-user-host-or-service-from-idm-ca-using-certutil_{context}"]
= Requesting new certificates for a user, host, or service from IdM CA using certutil

[role="_abstract"]
You can use the `certutil` utility to request a certificate for an {IPA} (IdM) user, host or service in standard IdM situations. To ensure that a host or service Kerberos alias can use a certificate, xref:requesting-new-certificates-for-a-user-host-or-service-from-idm-ca-using-openssl_{context}[use the openssl utility to request a certificate] instead.

Follow this procedure to request a certificate for an IdM user, host, or service from `ipa`, the IdM certificate authority (CA), using `certutil`.

[IMPORTANT]
====
Services typically run on dedicated service nodes on which the private keys are stored. Copying a service's private key to the IdM server is considered insecure. Therefore, when requesting a certificate for a service, create the certificate signing request (CSR) on the service node.
====


.Prerequisites

* Your IdM deployment contains an integrated CA.
* You are logged into the IdM command-line interface (CLI) as the IdM administrator.

.Procedure

. Create a temporary directory for the certificate database:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# mkdir ~/certdb/
....

. Create a new temporary certificate database, for example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# certutil -N -d pass:quotes[_~/certdb/_]
....

. Create the CSR and redirect the output to a file. For example, to create a CSR for a 4096 bit certificate and to set the subject to _CN=server.example.com,O=EXAMPLE.COM_:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# certutil -R -d pass:quotes[_~/certdb/_] -a -g pass:quotes[_4096_] -s "pass:quotes[_CN=server.example.com,O=EXAMPLE.COM_]" -8 pass:quotes[_server.example.com_] > pass:quotes[_certificate_request.csr_]
....

. Submit the certificate request file to the CA running on the IdM server. Specify the Kerberos principal to associate with the newly-issued certificate:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# ipa cert-request pass:quotes[_certificate_request.csr_] --principal=pass:quotes[_host/server.example.com_]
....

+
The [command]`ipa cert-request` command in IdM uses the following defaults:

+
* The `caIPAserviceCert` certificate profile
+
To select a custom profile, use the [option]`--profile-id` option.

+
* The integrated IdM root CA, `ipa`
+
To select a sub-CA, use the [option]`--ca` option.


[role="_additional-resources"]
.Additional resources
* See the output of the [command]`ipa cert-request --help` command.
* See xref:creating-and-managing-certificate-profiles-in-identity-management_{parent-context-of-managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca}[Creating and managing certificate profiles in Identity Management].
