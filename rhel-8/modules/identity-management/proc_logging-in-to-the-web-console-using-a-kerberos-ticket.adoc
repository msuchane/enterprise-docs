:_mod-docs-content-type: PROCEDURE
[id="logging-in-to-the-web-console-using-a-kerberos-ticket_{context}"]
= Logging in to the web console using Kerberos authentication

[role="_abstract"]
The following procedure describes steps on how to set up the {ProductShortName} {ProductNumber} system to use Kerberos authentication.

IMPORTANT:  With SSO you usually do not have any administrative privileges in the web console. This only works if you configured passwordless sudo. The web console does not interactively ask for a sudo password.

.Prerequisites

* IdM domain running and reachable in your company environment.
+
For details, see
ifdef::cockpit-title[]
xref:joining-a-rhel-8-system-to-an-idm-domain-using-the-web-console_configuring-single-sign-on-for-the-web-console-in-the-idm-domain[Joining a {ProductShortName} {ProductNumber} system to an IdM domain using the web console].
endif::[]
ifndef::cockpit-title[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/configuring_single_sign_on_for_the_rhel_{ProductNumber}_web_console_in_the_idm_domain_system-management-using-the-rhel-{ProductNumber}-web-console#joining-a-rhel-{ProductNumber}-system-to-an-idm-domain-using-the-web-console_configuring-single-sign-on-for-the-web-console-in-the-idm-domain[Joining a {ProductShortName} {ProductNumber} system to an IdM domain using the web console].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_systems_using_the_rhel_9_web_console/configuring_single_sign_on_for_the_rhel_8_web_console_in_the_idm_domain_system-management-using-the-rhel-9-web-console#joining-a-rhel-8-system-to-an-idm-domain-using-the-web-console_configuring-single-sign-on-for-the-web-console-in-the-idm-domain[Joining a {ProductShortName} {ProductNumber} system to an IdM domain using the web console].
endif::[]
endif::[]

* Enable the `cockpit.socket` service on remote systems to which you want to connect and manage them with the RHEL web console.
+
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].

* If the system does not use a Kerberos ticket managed by the SSSD client, try to request the ticket with the `kinit` utility manually.

.Procedure

Log in to the RHEL web console with the following address: `\https://dns_name:9090`.

At this point, you are successfully connected to the RHEL web console and you can start with configuration.

image:idm-cockpit-logging-done.png[A screenshot of the web console with a menu in a column along the left that has the following buttons: System - Logs - Storage - Networking - Accounts - Services - Applications - Diagnostic Reports - Kernel Dump - SELinux. The "System" option has been chosen and displays details for the system such as Hardware - Machine ID - Operating system - Secure Shell Keys - Hostname - and others. 3 graphs display usage of CPUs over time - use of Memory and Swap over time - and Disk I/O over time.]
