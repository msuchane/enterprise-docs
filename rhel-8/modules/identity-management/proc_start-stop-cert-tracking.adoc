:_mod-docs-content-type: PROCEDURE
[id="start-stop-cert-tracking_{context}"]
= Starting and stopping certificate tracking

[role="_abstract"]
Follow this procedure to use the `getcert stop-tracking` and `getcert start-tracking` commands to monitor certificates. The two commands are provided by the `certmonger` service. Enabling certificate tracking is especially useful if you have imported a certificate issued by the Identity Management (IdM) certificate authority (CA) onto the machine from a different IdM client. Enabling certificate tracking can also be the final step of the following provisioning scenario:

. On the IdM server, you create a certificate for a system that does not exist yet.
. You create the new system.
. You enroll the new system as an IdM client.
. You import the certificate and the key from the IdM server on to the IdM client.
. You start tracking the certificate using `certmonger` to ensure that it gets renewed when it is due to expire.


.Procedure

* To disable the monitoring of a certificate with the Request ID of 20190408143846:
+
[literal,subs="+quotes,attributes"]
....
# *getcert stop-tracking -i 20190408143846*
....

+
For more options, see the `getcert stop-tracking` man page.


* To enable the monitoring of a certificate stored in the `/tmp/some_cert.crt` file, whose private key is stored in the `/tmp/some_key.key` file:
+
[literal,subs="+quotes,attributes"]
....
# *getcert start-tracking -c IPA -f /tmp/some_cert.crt -k /tmp/some_key.key*
....

+
`Certmonger` cannot automatically identify the CA type that issued the certificate. For this reason, add the `-c` option with the `IPA` value to the `getcert start-tracking` command if the certificate was issued by the IdM CA. Omitting to add the `-c` option results in `certmonger` entering the NEED_CA state.

+
For more options, see the `getcert start-tracking` man page.

[NOTE]
====
The two commands do not manipulate the certificate. For example, `getcert stop-tracking` does not delete the certificate or remove it from the NSS database or from the filesystem but simply removes the certificate from the list of monitored certificates. Similarly, `getcert start-tracking` only adds a certificate to the list of monitored certificates.
====
