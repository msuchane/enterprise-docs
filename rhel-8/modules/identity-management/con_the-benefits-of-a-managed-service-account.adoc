// Module included in the following assemblies:
//
// assembly_accessing-ad-with-a-managed-service-account.adoc


[id="con_the-benefits-of-a-managed-service-account_{context}"]
= The benefits of a Managed Service Account

[role="_abstract"]
If you want to allow a RHEL host to access an Active Directory (AD) domain without joining it, you can use a Managed Service Account (MSA) to access that domain. An MSA is an account in AD that corresponds to a specific computer, which you can use to connect to AD resources as a specific user principal.

For example, if the AD domain `production.example.com` has a one-way trust relationship with the `lab.example.com` AD domain, the following conditions apply:

* The `lab` domain trusts users and hosts from the `production` domain.
* The `production` domain does *not* trust users and hosts from the `lab` domain.

This means that a host joined to the `lab` domain, such as `client.lab.example.com`, cannot access resources from the `production` domain through the trust.

If you want to create an exception for the `client.lab.example.com` host, you can use the `adcli` utility to create a MSA for the `client` host in the `production.example.com` domain. By authenticating with the Kerberos principal of the MSA, you can perform secure LDAP searches in the `production` domain from the `client` host.
