:_mod-docs-content-type: CONCEPT

[id="host-enrollment-privileges_{context}"]
= User privileges required for host enrollment

[role="_abstract"]
The host enrollment operation requires authentication to prevent an unprivileged user from adding unwanted machines to the IdM domain. The privileges required depend on several factors, for example:

* If a host entry is created separately from running `ipa-client-install`
* If a one-time password (OTP) is used for enrollment

.User privileges for optionally manually creating a host entry in IdM LDAP

The user privilege required for creating a host entry in IdM LDAP using the `ipa host-add` CLI command or the IdM Web UI is `Host Administrators`. The `Host Administrators` privilege can be obtained through the `IT Specialist` role.

.User privileges for joining the client to the IdM domain

Hosts are configured as IdM clients during the execution of the `ipa-client-install` command.
The level of credentials required for executing the `ipa-client-install` command depends on which of the following enrolling scenarios you find yourself in:

//After the initial configuration, IdM has tools to manage both Kerberos and DNS in response to changes in the domain services, changes to the IT environment, or changes on the hosts themselves which affect Kerberos, certificate, and DNS services.

//The host enrollment operation requires authentication to prevent an unprivileged user from adding unwanted machines to the IdM domain. As regards the configuration of the host itself, the level of credentials required for executing the `ipa-client-install` command depends on which of the following enrolling scenarios you find yourself in:

* The host entry in IdM LDAP does not exist. For this scenario, you need a full administrator’s credentials or the `Host Administrators` role. A full administrator is a member of the `admins` group. The `Host Administrators` role provides privileges to add hosts and enroll hosts. For details about this scenario, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/assembly_installing-an-idm-client_installing-identity-management#proc_installing-a-client-by-using-user-credentials-interactive-installation_assembly_installing-an-idm-client[Installing a client using user credentials: interactive installation].

* The host entry in IdM LDAP exists. For this scenario, you need a limited administrator’s credentials to execute `ipa-client-install` successfully. The limited administrator in this case has the `Enrollment Administrator` role, which provides the `Host Enrollment` privilege. For details, https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/assembly_installing-an-idm-client_installing-identity-management#proc_installing-a-client-by-using-user-credentials-interactive-installation_assembly_installing-an-idm-client[Installing a client using user credentials: interactive installation].

* The host entry in IdM LDAP exists, and an OTP has been generated for the host by a full or limited administrator. For this scenario, you can install an IdM client as an ordinary user if you run the `ipa-client-install` command with the `--password` option, supplying the correct OTP. For details, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/assembly_installing-an-idm-client_installing-identity-management#proc_installing-a-client-by-using-a-one-time-password-interactive-installation_assembly_installing-an-idm-client[Installing a client by using a one-time password: Interactive installation].

//Hosts are configured as IdM clients during the execution of the `ipa-client-install` command. Running the command configures both the Kerberos and SSSD services for the host, bringing the host within the IdM domain and allowing it to identify the IdM server it will connect with. If the host belongs to a DNS zone managed by IdM, `ipa-client-install` adds DNS records for the host too.
//Regarding DNS, the command does not modify the DNS server used by the host as specified in the `/etc/resolv.conf` file, and the host may use any DNS server. However, if the host is in a DNS zone managed by an IdM DNS server, then `ipa-client-install` adds the host to the DNS.


After enrollment, IdM hosts authenticate every new session to be able to access IdM resources. Machine authentication is required for the IdM server to trust the machine and to accept IdM connections from the client software installed on that machine. After authenticating the client, the IdM server can respond to its requests.
