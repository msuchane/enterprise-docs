:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_determining-which-node-a-resource-runs-on.adoc

[id='proc_setting-resource-stickiness-{context}']

= Configuring a resource to prefer its current node

[role="_abstract"]
Resources have a `resource-stickiness` value that you can set as a meta attribute when you create the resource, as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_configuring-cluster-resources-configuring-and-managing-high-availability-clusters#proc_configuring-resource-meta-options-configuring-cluster-resources[Configuring resource meta options]. The `resource-stickiness` value determines how much a resource wants to remain on the node where it is currently running. Pacemaker considers the `resource-stickiness` value in conjunction with other settings (for example, the score values of location constraints) to determine whether to move a resource to another node or to leave it in place.

With a `resource-stickiness` value of 0, a cluster may move resources as needed to balance resources across nodes. This may result in resources moving when unrelated resources start or stop. With a positive stickiness, resources have a preference to stay where they are, and move only if other circumstances outweigh the stickiness. This may result in newly-added nodes not getting any resources assigned to them without administrator intervention.

ifeval::[{ProductNumber} == 8]
By default, a resource is created with a `resource-stickiness` value of 0. Pacemaker's default behavior when `resource-stickiness` is set to 0 and there are no location constraints is to move resources so that they are evenly distributed among the cluster nodes. This may result in healthy resources moving more often than you desire. To prevent this behavior, you can set the default `resource-stickiness` value to 1. This default will apply to all resources in the cluster. This small value can be easily overridden by other constraints that you create, but it is enough to prevent Pacemaker from needlessly moving healthy resources around the cluster.

The following command sets the default `resource-stickiness` value to 1.

[subs="+quotes"]
....

# *pcs resource defaults update resource-stickiness=1*

....
endif::[]
ifeval::[{ProductNumber} == 9]
Newly-created clusters in RHEL 9 set the default value for `resource-stickiness` to 1. This small value can easily be overridden by other constraints that you create, but it is enough to prevent Pacemaker from needlessly moving healthy resources around the cluster. If you prefer cluster behavior that results from a `resource-stickiness` value of 0, you can change the `resource-stickiness` default value to 0 with the following command:

[subs="+quotes"]
....

# *pcs resource defaults update resource-stickiness=0*

....

If you upgrade an existing cluster to RHEL 9 and you have not explicitly set a default value for `resource-stickiness`, the `resource-stickiness` value remains 0 and the `pcs resource defaults` command will not show anything for stickiness.
endif::[]

With a positive `resource-stickiness` value, no resources will move to a newly-added node. If resource balancing is desired at that point, you can temporarily set the `resource-stickiness` value to 0.

Note that if a location constraint score is higher than the `resource-stickiness` value, the cluster may still move a healthy resource to the node where the location constraint points.

For further information about how Pacemaker determines where to place a resource, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_configuring-node-placement-strategy-configuring-and-managing-high-availability-clusters[Configuring a node placement strategy].
