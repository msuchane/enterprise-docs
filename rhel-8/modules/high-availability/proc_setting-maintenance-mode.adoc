:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_cluster-maintenance.adoc

[id='proc_setting-maintenance-mode-{context}']

= Putting a cluster in maintenance mode

[role="_abstract"]
When a cluster is in maintenance mode, the cluster does not start or stop any services until told otherwise. When maintenance mode is completed, the cluster does a sanity check of the current state of any services, and then stops or starts any that need it.

To put a cluster in maintenance mode, use the following command to set the `maintenance-mode` cluster property to `true`.

[subs="+quotes"]
....

# *pcs property set maintenance-mode=true*

....

To remove a cluster from maintenance mode, use the following command to set the `maintenance-mode` cluster property to `false`.

[subs="+quotes"]
....

# *pcs property set maintenance-mode=false*

....


You can remove a cluster property from the configuration with the following command.

[subs="+quotes"]
....

pcs property unset _property_

....

Alternately, you can remove a cluster property from a configuration by leaving the value field of the [command]`pcs property set` command blank. This restores that property to its default value. For example, if you have previously set the `symmetric-cluster` property to `false`, the following command removes the value you have set from the configuration and restores the value of `symmetric-cluster` to `true`, which is its default value.

[subs="+quotes"]
....

# *pcs property set symmetric-cluster=*

....
