:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-node-placement-strategy.adoc

[id='configuring-utilization-attributes-{context}']

= Utilization attributes and placement strategy

[role="_abstract"]
To configure the capacity that a node provides or a resource requires, you can use _utilization attributes_ for nodes and resources. You do this by setting a utilization variable for a resource and assigning a value to that variable to indicate what the resource requires, and then setting that same utilization variable for a node and assigning a value to that variable to indicate what that node provides.

You can name utilization attributes according to your preferences and define as many name and value pairs as your configuration needs. The values of utilization attributes must be integers.

== Configuring node and resource capacity

The following example configures a utilization attribute of CPU capacity for two nodes, setting this attribute as the variable `cpu`. It also configures a utilization attribute of RAM capacity, setting this attribute as the variable `memory`. In this example:

* Node 1 is defined as providing a CPU capacity of two and a RAM capacity of 2048

* Node 2 is defined as providing a CPU capacity of four and a RAM capacity of 2048

[subs="+quotes"]
....

# *pcs node utilization node1 cpu=2 memory=2048*
# *pcs node utilization node2 cpu=4 memory=2048*

....

The following example specifies the same utilization attributes that three different resources require. In this example:

* resource `dummy-small` requires a CPU capacity of 1 and a RAM capacity of 1024

* resource `dummy-medium` requires a CPU capacity of 2 and a RAM capacity of 2048

* resource `dummy-large` requires a CPU capacity of 1 and a RAM capacity of 3072

[subs="+quotes"]
....

# *pcs resource utilization dummy-small cpu=1 memory=1024*
# *pcs resource utilization dummy-medium cpu=2 memory=2048*
# *pcs resource utilization dummy-large cpu=3 memory=3072*

....

A node is considered eligible for a resource if it has sufficient free capacity to satisfy the resource’s requirements, as defined by the utilization attributes.

== Configuring placement strategy

After you have configured the capacities your nodes provide and the capacities your resources require, you need to set the `placement-strategy` cluster property, otherwise the capacity configurations have no effect.
// For information about setting cluster properties, see <<ch-clusteropts-HAAR>>.

Four values are available for the `placement-strategy` cluster property:

* `default` &mdash; Utilization values are not taken into account at all. Resources are allocated according to allocation scores. If scores are equal, resources are evenly distributed across nodes.

* `utilization` &mdash; Utilization values are taken into account only when deciding whether a node is considered eligible (that is, whether it has sufficient free capacity to satisfy the resource’s requirements). Load-balancing is still done based on the number of resources allocated to a node.

* `balanced` &mdash; Utilization values are taken into account when deciding whether a node is eligible to serve a resource and when load-balancing, so an attempt is made to spread the resources in a way that optimizes resource performance.

* `minimal` &mdash; Utilization values are taken into account only when deciding whether a node is eligible to serve a resource. For load-balancing, an attempt is made to concentrate the resources on as few nodes as possible, thereby enabling possible power savings on the remaining nodes.

The following example command sets the value of `placement-strategy` to `balanced`. After running this command, Pacemaker will ensure the load from your resources will be distributed evenly throughout the cluster, without the need for complicated sets of colocation constraints.

[subs="+quotes"]
....

# *pcs property set placement-strategy=balanced*

....

