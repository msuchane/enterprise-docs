:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_creating-mounting-gfs2.adoc

[id='proc_suspending-activity-on-a-gfs2-filesystem-{context}']

= Suspending activity on a GFS2 file system

[role="_abstract"]
You can suspend write activity to a file system by using the [command]`dmsetup suspend` command. Suspending write activity allows hardware-based device snapshots to be used to capture the file system in a consistent state. The [command]`dmsetup resume` command ends the suspension.

The format for the command to suspend activity on a GFS2 file system is as follows.

[subs="+quotes"]
....

dmsetup suspend _MountPoint_

....

This example suspends writes to file system `/mygfs2`.

[subs="+quotes"]
....

# *dmsetup suspend /mygfs2*

....

The format for the command to end suspension of activity on a GFS2 file system is as follows.

[subs="+quotes"]
....

dmsetup resume _MountPoint_

....

This example ends suspension of writes to file system `/mygfs2`.

[subs="+quotes"]
....

# *dmsetup resume /mygfs2*

....
