:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-pacemaker-alert-agents.adoc

[id='cluster-alert-configuration-{context}']

= Cluster alert configuration command examples

[role="_abstract"]
The following sequential examples show some basic alert configuration commands to show the format to use to create alerts, add recipients, and display the configured alerts.

Note that while you must install the alert agents themselves on each node in a cluster, you need to run the `pcs` commands only once.

The following commands create a simple alert, add two recipients to the alert, and display the configured values.

* Since no alert ID value is specified, the system creates an alert ID value of `alert`.

* The first recipient creation command specifies a recipient of `rec_value`. Since this command does not specify a recipient ID, the value of `alert-recipient` is used as the recipient ID.

* The second recipient creation command specifies a recipient of `rec_value2`. This command specifies a recipient ID of `my-recipient` for the recipient.

[subs="+quotes"]
....

# *pcs alert create path=/my/path*
# *pcs alert recipient add alert value=rec_value*
# *pcs alert recipient add alert value=rec_value2 id=my-recipient*
# *pcs alert config*
Alerts:
 Alert: alert (path=/my/path)
  Recipients:
   Recipient: alert-recipient (value=rec_value)
   Recipient: my-recipient (value=rec_value2)

....

This following commands add a second alert and a recipient for that alert. The alert ID for the second alert is `my-alert` and the recipient value is `my-other-recipient`. Since no recipient ID is specified, the system provides a recipient id of `my-alert-recipient`.

[subs="+quotes"]
....

# *pcs alert create id=my-alert path=/path/to/script description=alert_description options option1=value1 opt=val meta timeout=50s timestamp-format="%H%B%S"*
# *pcs alert recipient add my-alert value=my-other-recipient*
# *pcs alert*
Alerts:
 Alert: alert (path=/my/path)
  Recipients:
   Recipient: alert-recipient (value=rec_value)
   Recipient: my-recipient (value=rec_value2)
 Alert: my-alert (path=/path/to/script)
  Description: alert_description
  Options: opt=val option1=value1
  Meta options: timestamp-format=%H%B%S timeout=50s
  Recipients:
   Recipient: my-alert-recipient (value=my-other-recipient)

....

The following commands modify the alert values for the alert `my-alert` and for the recipient `my-alert-recipient`.

[subs="+quotes"]
....

# *pcs alert update my-alert options option1=newvalue1 meta timestamp-format="%H%M%S"*
# *pcs alert recipient update my-alert-recipient options option1=new meta timeout=60s*
# *pcs alert*
Alerts:
 Alert: alert (path=/my/path)
  Recipients:
   Recipient: alert-recipient (value=rec_value)
   Recipient: my-recipient (value=rec_value2)
 Alert: my-alert (path=/path/to/script)
  Description: alert_description
  Options: opt=val option1=newvalue1
  Meta options: timestamp-format=%H%M%S timeout=50s
  Recipients:
   Recipient: my-alert-recipient (value=my-other-recipient)
    Options: option1=new
    Meta options: timeout=60s

....

The following command removes the recipient `my-alert-recipient` from `alert`.

[literal,subs="+quotes,verbatim,macros,attributes"]
[subs="+quotes"]
....

# *pcs alert recipient remove my-recipient*
# *pcs alert*
Alerts:
 Alert: alert (path=/my/path)
  Recipients:
   Recipient: alert-recipient (value=rec_value)
 Alert: my-alert (path=/path/to/script)
  Description: alert_description
  Options: opt=val option1=newvalue1
  Meta options: timestamp-format="%M%B%S" timeout=50s
  Recipients:
   Recipient: my-alert-recipient (value=my-other-recipient)
    Options: option1=new
    Meta options: timeout=60s

....

The following command removes `myalert` from the configuration.

[subs="+quotes"]
....

# *pcs alert remove myalert*
# *pcs alert*
Alerts:
 Alert: alert (path=/my/path)
  Recipients:
   Recipient: alert-recipient (value=rec_value)

....
