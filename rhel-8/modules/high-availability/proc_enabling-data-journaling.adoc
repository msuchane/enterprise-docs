:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_gfs2-performance.adoc

[id='enabling-data-journaling-{context}']

= Enabling data journaling

[role="_abstract"]
Ordinarily, GFS2 writes only metadata to its journal. File contents are subsequently written to disk by the kernel's periodic sync that flushes file system buffers. An [command]`fsync()` call on a file causes the file's data to be written to disk immediately. The call returns when the disk reports that all data is safely written.

Data journaling can result in a reduced [command]`fsync()` time for very small files because the file data is written to the journal in addition to the metadata. This advantage rapidly reduces as the file size increases. Writing to medium and larger files will be much slower with data journaling turned on.

Applications that rely on [command]`fsync()` to sync file data may see improved performance by using data journaling. Data journaling can be enabled automatically for any GFS2 files created in a flagged directory (and all its subdirectories). Existing files with zero length can also have data journaling turned on or off.

Enabling data journaling on a directory sets the directory to "inherit jdata", which indicates that all files and directories subsequently created in that directory are journaled. You can enable and disable data journaling on a file with the [command]`chattr` command.

The following commands enable data journaling on the `/mnt/gfs2/gfs2_dir/newfile` file and then check whether the flag has been set properly.

[subs="+quotes"]
....

# *chattr +j /mnt/gfs2/gfs2_dir/newfile*
# *lsattr /mnt/gfs2/gfs2_dir*
---------j--- /mnt/gfs2/gfs2_dir/newfile

....

The following commands disable data journaling on the `/mnt/gfs2/gfs2_dir/newfile` file and then check whether the flag has been set properly.

[subs="+quotes"]
....

# *chattr -j /mnt/gfs2/gfs2_dir/newfile*
# *lsattr /mnt/gfs2/gfs2_dir*
------------- /mnt/gfs2/gfs2_dir/newfile

....

You can also use the [command]`chattr` command to set the `j` flag on a directory. When you set this flag for a directory, all files and directories subsequently created in that directory are journaled. The following set of commands sets the `j` flag on the `gfs2_dir` directory, then checks whether the flag has been set properly. After this, the commands create a new file called `newfile` in the `/mnt/gfs2/gfs2_dir` directory and then check whether the `j` flag has been set for the file. Since the `j` flag is set for the directory, then `newfile` should also have journaling enabled.

[subs="+quotes"]
....

# *chattr -j /mnt/gfs2/gfs2_dir*
# *lsattr /mnt/gfs2*
---------j--- /mnt/gfs2/gfs2_dir
# *touch /mnt/gfs2/gfs2_dir/newfile*
# *lsattr /mnt/gfs2/gfs2_dir*
---------j--- /mnt/gfs2/gfs2_dir/newfile

....

