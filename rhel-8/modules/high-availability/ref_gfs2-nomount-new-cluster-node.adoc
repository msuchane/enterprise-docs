:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_troubleshooting-gfs2.adoc

[id='ref_gfs2-nomount-new-cluster-node-{context}']

= GFS2 file system does not mount on newly added cluster node

[role="_abstract"]
If you add a new node to a cluster and find that you cannot mount your GFS2 file system on that node, you may have fewer journals on the GFS2 file system than nodes attempting to access the GFS2 file system. You must have one journal per GFS2 host you intend to mount the file system on (with the exception of GFS2 file systems mounted with the `spectator` mount option set, since these do not require a journal). You can add journals to a GFS2 file system with the [command]`gfs2_jadd` command, as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_gfs2_file_systems/assembly_creating-mounting-gfs2-configuring-gfs2-file-systems#proc_adding-gfs2-journal-creating-mounting-gfs2[Adding journals to a GFS2 file system].

