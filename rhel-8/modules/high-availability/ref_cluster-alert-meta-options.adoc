:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-pacemaker-alert-agents.adoc

[id='cluster-alert-meta-options-{context}']

= Alert meta options

[role="_abstract"]
As with resource agents, meta options can be configured for alert agents to affect how Pacemaker calls them. The following table describes the alert meta options. Meta options can be configured per alert agent as well as per recipient.

[[tb-alert-meta-HAAR]]
.Alert Meta Options

[options="header"]
|===
|Meta-Attribute|Default|Description
ifeval::[{ProductNumber} == 9]
|`enabled`|`true`|(RHEL 9.3 and later) If set to `false` for an alert, the alert will not be used. If set to `true` for an alert and `false` for a particular recipient of that alert, that recipient will not be used.
endif::[]
ifeval::[{ProductNumber} == 8]
|`enabled`|`true`|(RHEL 8.9 and later) If set to `false` for an alert, the alert will not be used. If set to `true` for an alert and `false` for a particular recipient of that alert, that recipient will not be used.
endif::[]
|`timestamp-format`|%H:%M:%S.%06N|Format the cluster will use when sending the event’s timestamp to the agent. This is a string as used with the `date`(1) command.
|`timeout`|30s|If the alert agent does not complete within this amount of time, it will be terminated.
|===

The following example configures an alert that calls the script `myscript.sh` and then adds two recipients to the alert. The first recipient has an ID of `my-alert-recipient1` and the second recipient has an ID of `my-alert-recipient2`. The script will get called twice for each event, with each call using a 15-second timeout. One call will be passed to the recipient `someuser@example.com` with a timestamp in the format %D %H:%M, while the other call will be passed to the recipient `otheruser@example.com` with a timestamp in the format %c.

[subs="+quotes"]
....

# *pcs alert create id=my-alert path=/path/to/myscript.sh meta timeout=15s*
# *pcs alert recipient add my-alert value=someuser@example.com id=my-alert-recipient1 meta timestamp-format="%D %H:%M"*
# *pcs alert recipient add my-alert value=otheruser@example.com id=my-alert-recipient2 meta timestamp-format="%c"*

....

