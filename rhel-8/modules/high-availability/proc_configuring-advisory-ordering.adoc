:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_determining-resource-order.adoc

[id='proc_configuring-advisory-ordering-{context}']

= Configuring advisory ordering

[role="_abstract"]
When the `kind=Optional` option is specified for an ordering constraint, the constraint is considered optional and only applies if both resources are executing the specified actions. Any change in state by the first resource you specify will have no effect on the second resource you specify.

The following command configures an advisory ordering constraint for the resources named `VirtualIP` and `dummy_resource`.

[subs="+quotes"]
....

# *pcs constraint order VirtualIP then dummy_resource kind=Optional* 

....

