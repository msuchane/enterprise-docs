:_mod-docs-content-type: PROCEDURE
:parent-context-of-configuring-resource-meta-options: {context}

// This module  is included in the following assemblies:
// assembly_configuring-cluster-resources.adoc


[id='proc_configuring-resource-meta-options-{context}']
= Configuring resource meta options
:context: configuring-resource-meta-options

[role="_abstract"]
In addition to the resource-specific parameters, you can configure additional resource options for any resource. These options are used by the cluster to decide how your resource should behave.

The following table describes the resource meta options.

[[tb-resource-options-HAAR]]
.Resource Meta Options

[cols="2,2,3" options="header"]
|===
|Field|Default|Description
|`priority`|`0`|If not all resources can be active, the cluster will stop lower priority resources in order to keep higher priority ones active.
|`target-role`|`Started`|Indicates what state the cluster should attempt to keep this resource in. Allowed values:

* `Stopped` - Force the resource to be stopped

ifeval::[{ProductNumber} == 8]
* `Started` - Allow the resource to be started (and in the case of promotable clones, promoted to master role if appropriate)

* `Master` - Allow the resource to be started and, if appropriate, promoted

* `Slave` - Allow the resource to be started, but only in slave mode if the resource is promotable

As of RHEL 8.5, The `pcs` command-line interface accepts `Promoted` and `Unpromoted` anywhere roles are specified in Pacemaker configuration. These role names are the functional equivalent of the `Master` and `Slave` Pacemaker roles.
endif::[]
ifeval::[{ProductNumber} == 9]
* `Started` - Allow the resource to be started (and in the case of promotable clones, promoted if appropriate)

* `Promoted` - Allow the resource to be started and, if appropriate, promoted

* `Unpromoted` - Allow the resource to be started, but only in unpromoted mode if the resource is promotable
endif::[]


|`is-managed`|`true`|Indicates whether the cluster is allowed to start and stop the resource. Allowed values: `true`, `false`
ifeval::[{ProductNumber} == 8]
|`resource-stickiness`|0|Value to indicate how much the resource prefers to stay where it is. For information about this attribute, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_determining-which-node-a-resource-runs-on-configuring-and-managing-high-availability-clusters#proc_setting-resource-stickiness-determining-which-node-a-resource-runs-on[Configuring a resource to prefer its current node].
endif::[]
ifeval::[{ProductNumber} == 9]
|`resource-stickiness`|1|Value to indicate how much the resource prefers to stay where it is.
endif::[]
|`requires`|Calculated|Indicates under what conditions the resource can be started.

Defaults to `fencing` except under the conditions noted below. Possible values:

* `nothing` - The cluster can always start the resource.

* `quorum` - The cluster can only start this resource if a majority of the configured nodes are active. This is the default value if `stonith-enabled` is `false` or the resource's `standard` is `stonith`.

* `fencing` - The cluster can only start this resource if a majority of the configured nodes are active _and_ any failed or unknown nodes have been fenced.

* `unfencing` - The cluster can only start this resource if a majority of the configured nodes are active _and_ any failed or unknown nodes have been fenced _and_ only on nodes that have been _unfenced_. This is the default value if the `provides=unfencing` `stonith` meta option has been set for a fencing device.
|`migration-threshold`|`INFINITY`|How many failures may occur for this resource on a node before this node is marked ineligible to host this resource. A value of 0 indicates that this feature is disabled (the node will never be marked ineligible); by contrast, the cluster treats `INFINITY` (the default) as a very large but finite number. This option has an effect only if the failed operation has `on-fail=restart` (the default), and additionally for failed start operations if the cluster property `start-failure-is-fatal` is `false`.
|`failure-timeout`|`0` (disabled)|Used in conjunction with the `migration-threshold` option, indicates how many seconds to wait before acting as if the failure had not occurred, and potentially allowing the resource back to the node on which it failed.
// LINK TO For information about configuring the `failure-timeout` option, see <<s1-failure_migration-HAAR>>.
|`multiple-active`|`stop_start`|Indicates what the cluster should do if it ever finds the resource active on more than one node. Allowed values:

* `block` - mark the resource as unmanaged

* `stop_only` - stop all active instances and leave them that way

* `stop_start` - stop all active instances and start the resource in one location only

ifeval::[{ProductNumber} == 8]
* `stop_unexpected` - (RHEL 8.7 and later) 
endif::[]
ifeval::[{ProductNumber} == 9]
* `stop_unexpected` -  (RHEL 9.1 and later)
endif::[]
stop only unexpected instances of the resource, without requiring a full restart.  It is the user's responsibility to verify that the service and its resource agent can function with extra active instances without requiring a full restart.
ifeval::[{ProductNumber} == 8]
|`critical`|`true`|(RHEL 8.4 and later) Sets the default value for the `influence` option for
endif::[]
ifeval::[{ProductNumber} == 9]
|`critical`|`true`|Sets the default value for the `influence` option for
endif::[]
all colocation constraints involving the resource as a dependent resource (_target_resource_), including implicit colocation constraints created when the resource is part of a resource group. The `influence` colocation constraint option determines whether the cluster will move both the primary and dependent resources to another node when the dependent resource reaches its migration threshold for failure, or whether the cluster will leave the dependent resource offline without causing a service switch. The `critical` resource meta option can have a value of `true` or `false`, with a default value of `true`.
ifeval::[{ProductNumber} == 8]
|`allow-unhealthy-nodes`|`false`|(RHEL 8.7 and later)
endif::[]
ifeval::[{ProductNumber} == 9]
|`allow-unhealthy-nodes`|`false`|(RHEL 9.1 and later)
endif::[]
When set to `true`, the resource is not forced off a node due to degraded node health. When health resources have this attribute set, the cluster can automatically detect if the node's health recovers and move resources back to it. A node's health is determined by a combination of the health attributes set by health resource agents based on local conditions, and the strategy-related options that determine how the cluster reacts to those conditions.
|===


== Changing the default value of a resource option

ifeval::[{ProductNumber} == 8]
As of Red Hat Enterprise Linux 8.3, you can change the default value
endif::[]
ifeval::[{ProductNumber} == 9]
You can change the default value
endif::[]
of a resource option for all resources with the `pcs resource defaults update` command. The following command resets the default value of `resource-stickiness` to 100.

[subs="+quotes"]
....

# *pcs resource defaults update resource-stickiness=100*

....

The original `pcs resource defaults _name_=_value_` command, which set defaults for all resources in previous releases, remains supported unless there is more than one set of defaults configured. However, `pcs resource defaults update` is now the preferred version of the command.

== Changing the default value of a resource option for sets of resources 

ifeval::[{ProductNumber} == 8]
As of Red Hat Enterprise Linux 8.3, you can create multiple sets of resource defaults with the `pcs resource defaults set create` command, which allows you to specify a rule that contains `resource` expressions. In RHEL 8.3, only `resource` expressions, including `and`, `or` and parentheses, are allowed in rules that you specify with this command. In RHEL 8.4 and later, only `resource` and `date` expressions, including `and`, `or` and parentheses, are allowed in rules that you specify with this command.
endif::[]
ifeval::[{ProductNumber} == 9]
You can create multiple sets of resource defaults with the `pcs resource defaults set create` command, which allows you to specify a rule that contains `resource` expressions. Only `resource` and `date` expressions, including `and`, `or` and parentheses, are allowed in rules that you specify with this command.
endif::[]

With the `pcs resource defaults set create` command, you can configure a default resource value for all resources of a particular type. If, for example, you are running databases which take a long time to stop, you can increase the `resource-stickiness` default value for all resources of the database type to prevent those resources from moving to other nodes more often than you desire.

The following command sets the default value of `resource-stickiness` to 100 for all resources of type `pqsql`. 

* The `id` option, which names the set of resource defaults, is not mandatory. If you do not set this option `pcs` will generate an ID automatically. Setting this value allows you to provide a more descriptive name.

* In this example, `::pgsql` means a resource of any class, any provider, of type `pgsql`.
** Specifying `ocf:heartbeat:pgsql` would indicate class `ocf`, provider `heartbeat`, type `pgsql`,
** Specifying `ocf:pacemaker:` would indicate all resources of class `ocf`, provider `pacemaker`, of any type.

[subs="+quotes"]
....

# *pcs resource defaults set create id=pgsql-stickiness meta resource-stickiness=100 rule resource ::pgsql*

....

To change the default values in an existing set, use the `pcs resource defaults set update` command.


== Displaying currently configured resource defaults

The `pcs resource defaults` command displays a list of currently configured default values for resource options, including any rules you specified.

The following example shows the output of this command after you have reset the default value of `resource-stickiness` to 100.

[subs="+quotes"]
....

# *pcs resource defaults*
Meta Attrs: rsc_defaults-meta_attributes
  resource-stickiness=100

....

The following example shows the output of this command after you have reset the default value of `resource-stickiness` to 100 for all resources of type `pqsql` and set the `id` option to `id=pgsql-stickiness`.

[subs="+quotes"]
....

# *pcs resource defaults*
Meta Attrs: pgsql-stickiness
  resource-stickiness=100
  Rule: boolean-op=and score=INFINITY
    Expression: resource ::pgsql

....

== Setting meta options on resource creation

Whether you have reset the default value of a resource meta option or not, you can set a resource option for a particular resource to a value other than the default when you create the resource. The following shows the format of the `pcs resource create` command you use when specifying a value for a resource meta option.

[subs="+quotes"]
....

pcs resource create _resource_id_ [_standard_:[_provider_:]]_type_ [_resource options_] [meta _meta_options_...]

....

For example, the following command creates a resource with a `resource-stickiness` value of 50.

[subs="+quotes"]
....

# *pcs resource create VirtualIP ocf:heartbeat:IPaddr2 ip=192.168.0.120 meta resource-stickiness=50*

....

You can also set the value of a resource meta option for an existing resource, group, or cloned resource with the following command.

[subs="+quotes"]
....

pcs resource meta _resource_id_ | _group_id_ | _clone_id_ _meta_options_

....

In the following example, there is an existing resource named `dummy_resource`. This command sets the `failure-timeout` meta option to 20 seconds, so that the resource can attempt to restart on the same node in 20 seconds.

[subs="+quotes"]
....

# *pcs resource meta dummy_resource failure-timeout=20s* 

....

After executing this command, you can display the values for the resource to verify that `failure-timeout=20s` is set.

[subs="+quotes"]
....

# *pcs resource config dummy_resource*
 Resource: dummy_resource (class=ocf provider=heartbeat type=Dummy)
  Meta Attrs: failure-timeout=20s
  ...

....



:context: {parent-context-of-configuring-resource-meta-options}
