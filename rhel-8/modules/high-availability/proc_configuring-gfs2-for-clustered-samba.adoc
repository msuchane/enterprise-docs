:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-active-active-samba-in-a-cluster.adoc

[id='proc_configuring-gfs2-for-clustered-samba_adoc-{context}']

= Configuring a GFS2 file system for a Samba service in a high availability cluster

Before configuring an active/active Samba service in a Pacemaker cluster, configure a GFS2 file system for the cluster.

.Prerequisites

* A two-node Red Hat High Availability cluster with fencing configured for each node
* Shared storage available for each cluster node
* A subscription to the AppStream channel and the Resilient Storage channel for each cluster node

For information about creating a Pacemaker cluster and configuring fencing for the cluster, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_creating-high-availability-cluster-configuring-and-managing-high-availability-clusters[Creating a Red Hat High-Availability cluster with Pacemaker].

.Procedure

. On both nodes in the cluster, perform the following initial setup steps.

.. Enable the repository for Resilient Storage that corresponds to your system architecture. For example, to enable the Resilient Storage repository for an x86_64 system, enter the following `subscription-manager` command:
+
[subs="+quotes"]
....

ifeval::[{ProductNumber} == 8]
# *subscription-manager repos --enable=rhel-8-for-x86_64-resilientstorage-rpms*
endif::[]
ifeval::[{ProductNumber} == 9]
# *subscription-manager repos --enable=rhel-9-for-x86_64-resilientstorage-rpms*
endif::[]

....
+
The Resilient Storage repository is a superset of the High Availability repository. If you enable the Resilient Storage repository, you do not need to also enable the High Availability repository.

.. Install the `lvm2-lockd`, `gfs2-utils`, and `dlm` packages.				
+
[subs="+quotes"]
....

# *yum install lvm2-lockd gfs2-utils dlm*

....

.. Set the `use_lvmlockd` configuration option in the `/etc/lvm/lvm.conf` file to `use_lvmlockd=1`. 	
+
[subs="+quotes"]
....

...

use_lvmlockd = 1

...

....

. On one node in the cluster, set the global Pacemaker parameter `no-quorum-policy` to `freeze`.
+
[NOTE]
====

By default, the value of `no-quorum-policy` is set to `stop`, indicating that once quorum is lost, all the resources on the remaining partition will immediately be stopped. Typically this default is the safest and most optimal option, but unlike most resources, GFS2 requires quorum to function. When quorum is lost both the applications using the GFS2 mounts and the GFS2 mount itself cannot be correctly stopped. Any attempts to stop these resources without quorum will fail which will ultimately result in the entire cluster being fenced every time quorum is lost.

To address this situation, set `no-quorum-policy` to `freeze` when GFS2 is in use. This means that when quorum is lost, the remaining partition will do nothing until quorum is regained.

====
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs property set no-quorum-policy=freeze*

....

. Set up a `dlm` resource. This is a required dependency for configuring a GFS2 file system in a cluster. This example creates the `dlm` resource as part of a resource group named `locking`. If you have not previously configured fencing for the cluster, this step fails and the `pcs status` command displays a resource failure message.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create dlm --group locking ocf:pacemaker:controld op monitor interval=30s on-fail=fence*

....

. Clone the `locking` resource group so that the resource group can be active on both nodes of the cluster.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource clone locking interleave=true*

....

. Set up an `lvmlockd` resource as part of the `locking` resource group.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create lvmlockd --group locking ocf:heartbeat:lvmlockd op monitor interval=30s on-fail=fence*

....

. Create a physical volume and a shared volume group on the shared device `/dev/vdb`. This example creates the shared volume group `csmb_vg`.
+
[subs="+quotes"]
....

[root@z1 ~]# *pvcreate /dev/vdb* 
[root@z1 ~]# *vgcreate -Ay --shared csmb_vg /dev/vdb*
Volume group "csmb_vg" successfully created
VG csmb_vg starting dlm lockspace
Starting locking.  Waiting until locks are ready

....

. On the second node in the cluster:


ifeval::[{ProductNumber} == 8]
. (RHEL 8.5 and later) If you have enabled the use of a devices file by setting `use_devicesfile = 1` in the `lvm.conf` file, add the shared device to the devices file on the second node in the cluster. By default, the use of a devices file is not enabled.
endif::[]
ifeval::[{ProductNumber} == 9]
. If the use of a devices file is enabled with the `use_devicesfile = 1` parameter in the `lvm.conf` file, add the shared device to the devices file on the second node in the cluster. This feature is enabled by default.
endif::[]
+
[subs="+quotes"]
....

[root@z2 ~]# *lvmdevices --adddev /dev/vdb*

....

.. Start the lock manager for the shared volume group.
+
[subs="+quotes"]
....

[root@z2 ~]# *vgchange --lockstart csmb_vg*
  VG csmb_vg starting dlm lockspace
  Starting locking.  Waiting until locks are ready...

....


. On one node in the cluster, create a logical volume and format the volume with a GFS2 file system that will be used exclusively by CTDB for internal locking. Only one such file system is required in a cluster even if your deployment exports multiple shares.
+
When specifying the lock table name with the `-t` option of the `mkfs.gfs2` command, ensure that the first component of the _clustername:filesystemname_ you specify matches the name of your cluster. In this example, the cluster name is `my_cluster`.
+
[subs="+quotes"]
....

[root@z1 ~]# *lvcreate -L1G -n ctdb_lv csmb_vg*
[root@z1 ~]# *mkfs.gfs2 -j3 -p lock_dlm -t my_cluster:ctdb /dev/csmb_vg/ctdb_lv*

....


. Create a logical volume for each GFS2 file system that will be shared over Samba and format the volume with the GFS2 file system. This example creates a single GFS2 file system and Samba share, but you can create multiple file systems and shares.
+
[subs="+quotes"]
....

[root@z1 ~]# *lvcreate -L50G -n csmb_lv1 csmb_vg*
[root@z1 ~]# *mkfs.gfs2 -j3 -p lock_dlm -t my_cluster:csmb1 /dev/csmb_vg/csmb_lv1*

....

. Set up `LVM_Activate` resources to ensure that the required shared volumes are activated. This example creates the `LVM_Activate` resources as part of a resource group `shared_vg`, and then clones that resource group so that it runs on all nodes in the cluster. 
+
Create the resources as disabled so they do not start automatically before you have configured the necessary order constraints.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create --disabled --group shared_vg ctdb_lv ocf:heartbeat:LVM-activate lvname=ctdb_lv vgname=csmb_vg activation_mode=shared vg_access_mode=lvmlockd*
[root@z1 ~]# *pcs resource create --disabled --group shared_vg csmb_lv1 ocf:heartbeat:LVM-activate lvname=csmb_lv1 vgname=csmb_vg activation_mode=shared vg_access_mode=lvmlockd*
[root@z1 ~]# *pcs resource clone shared_vg interleave=true*

....

. Configure an ordering constraint to start all members of the `locking` resource group before the members of the `shared_vg` resource group.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs constraint order start locking-clone then shared_vg-clone*
Adding locking-clone shared_vg-clone (kind: Mandatory) (Options: first-action=start then-action=start)

....

. Enable the `LVM-activate` resources.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource enable ctdb_lv csmb_lv1*

....

. On one node in the cluster, perform the following steps to create the `Filesystem` resources you require.

.. Create `Filesystem` resources as cloned resources, using the GFS2 file systems you previously configured on your LVM volumes. This configures Pacemaker to mount and manage file systems. 
+
[NOTE]
====

You should not add the file system to the `/etc/fstab` file because it will be managed as a Pacemaker cluster resource. You can specify mount options as part of the resource configuration with `options=pass:attributes[{blank}]_options_pass:attributes[{blank}]`. Run the [command]`pcs resource describe Filesystem` command to display the full configuration options.

====
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create ctdb_fs Filesystem device="/dev/csmb_vg/ctdb_lv" directory="/mnt/ctdb" fstype="gfs2" op monitor interval=10s on-fail=fence clone interleave=true*
[root@z1 ~]# *pcs resource create csmb_fs1 Filesystem device="/dev/csmb_vg/csmb_lv1" directory="/srv/samba/share1" fstype="gfs2" op monitor interval=10s on-fail=fence clone interleave=true*

....

.. Configure ordering constraints to ensure that Pacemaker mounts the file systems after the shared volume group `shared_vg` has started.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs constraint order start shared_vg-clone then ctdb_fs-clone*
Adding shared_vg-clone ctdb_fs-clone (kind: Mandatory) (Options: first-action=start then-action=start)
[root@z1 ~]# *pcs constraint order start shared_vg-clone then csmb_fs1-clone*
Adding shared_vg-clone csmb_fs1-clone (kind: Mandatory) (Options: first-action=start then-action=start)

....

