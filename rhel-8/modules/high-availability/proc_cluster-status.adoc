:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_pcs-operation.adoc

[id='proc_cluster-status-{context}']

= Displaying cluster status

[role="_abstract"]
There are a variety of commands you can use to display the status of a cluster and its components.

You can display the status of the cluster and the cluster resources with the following command.

[subs="+quotes"]
....

# *pcs status* 

....

You can display the status of a particular cluster component with the _commands_ parameter of the [command]`pcs status` command, specifying `resources`, `cluster`, `nodes`, or `pcsd`.


[subs="+quotes"]
....

pcs status _commands_

....

For example, the following command displays the status of the cluster resources.

[subs="+quotes"]
....

# *pcs status resources*

....

The following command displays the status of the cluster, but not the cluster resources.

[subs="+quotes"]
....

# *pcs cluster status*

....

