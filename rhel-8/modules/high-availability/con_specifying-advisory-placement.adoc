:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_colocating-cluster-resources.adoc

[id='con_specifying-advisory-placement-{context}']

= Specifying advisory placement of resources

[role="_abstract"]
Advisory placement of resources indicates the placement of resources is a preference, but is not mandatory. For constraints with scores greater than `-INFINITY` and less than `INFINITY`, the cluster will try to accommodate your wishes but may ignore them if the alternative is to stop some of the cluster resources.
