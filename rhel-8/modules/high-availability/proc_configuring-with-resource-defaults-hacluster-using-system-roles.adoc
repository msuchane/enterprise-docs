:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="configuring-ha-cluster-with-resource-defaults_{context}"]
= Configuring a high availability cluster with resource and resource operation defaults
ifeval::[{ProductNumber} == 8]
(RHEL 8.9 and later)
endif::[]
ifeval::[{ProductNumber} == 9]
(RHEL 9.3 and later)
endif::[]
The following procedure uses the `ha_cluster` system role to create a high availability cluster that defines resource and resource operation defaults.

[WARNING]
====
The `ha_cluster` system role replaces any existing cluster configuration on the specified nodes. Any settings not specified in the role will be lost.
====


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]
+
* The systems that you will use as your cluster members have active subscription coverage for RHEL and the RHEL High Availability Add-On.
* The inventory file specifies the cluster nodes as described in xref:ha-system-role-inventory_configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role[Specifying an inventory for the ha_cluster system role].


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Create a high availability cluster that defines resource and resource operation defaults
  hosts: node1 node2
  roles:
    - rhel-system-roles.ha_cluster
  vars:
    ha_cluster_cluster_name: my-new-cluster
    ha_cluster_hacluster_password: _<password>_
    ha_cluster_manage_firewall: true
    ha_cluster_manage_selinux: true
    # Set a different `resource-stickiness` value during
    # and outside work hours. This allows resources to
    # automatically move back to their most
    # preferred hosts, but at a time that
    # does not interfere with business activities.
    ha_cluster_resource_defaults:
      meta_attrs:
        - id: core-hours
          rule: date-spec hours=9-16 weekdays=1-5
          score: 2
          attrs:
            - name: resource-stickiness
              value: INFINITY
        - id: after-hours
          score: 1
          attrs:
            - name: resource-stickiness
              value: 0
    # Default the timeout on all 10-second-interval
    # monitor actions on IPaddr2 resources to 8 seconds.
    ha_cluster_resource_operation_defaults:
      meta_attrs:
        - rule: resource ::IPaddr2 and op monitor interval=10s
          score: INFINITY
          attrs:
            - name: timeout
              value: 8s
....
+
This example playbook file configures a cluster running the `firewalld` and `selinux` services. The cluster includes resource and resource operation defaults.
+
When creating your playbook file for production, vault encrypt the password, as described in link:++https://docs.ansible.com/ansible/latest/user_guide/vault.html++[Encrypting content with Ansible Vault].


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.ha_cluster/README.md` file
* `/usr/share/doc/rhel-system-roles/ha_cluster/` directory

