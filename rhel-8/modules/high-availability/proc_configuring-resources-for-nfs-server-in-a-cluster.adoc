:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-active-passive-nfs-server-in-a-cluster.adoc

[id='proc_configuring_resources_for_nfs_server_in_a_cluster-{context}']

= Configuring the resources and resource group for an NFS server in a cluster

[role="_abstract"]
Configure the cluster resources for an NFS server in a cluster with the following procedure.

[NOTE]
====

If you have not configured a fencing device for your cluster, by default the resources do not start.

If you find that the resources you configured are not running, you can run the [command]`pcs resource debug-start _resource_pass:attributes[{blank}]` command to test the resource configuration. This starts the service outside of the cluster’s control and knowledge. At the point the configured resources are running again, run [command]`pcs resource cleanup _resource_pass:attributes[{blank}]`  to make the cluster aware of the updates.

====

.Procedure

The following procedure configures the system resources. To ensure these resources all run on the same node, they are configured as part of the resource group `nfsgroup`. The resources will start in the order in which you add them to the group, and they will stop in the reverse order in which they are added to the group. Run this procedure from one node of the cluster only.

. Create the LVM-activate resource named `my_lvm`. Because the resource group `nfsgroup` does not yet exist, this command creates the resource group.
+
[WARNING]
====

Do not configure more than one `LVM-activate` resource that uses the same LVM volume group in an active/passive HA configuration, as this risks data corruption. Additionally, do not configure an `LVM-activate` resource as a clone resource in an active/passive HA configuration.

====
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create my_lvm ocf:heartbeat:LVM-activate vgname=my_vg vg_access_mode=system_id --group nfsgroup*

....

. Check the status of the cluster to verify that the resource is running.
+
[subs="+quotes"]
....

root@z1 ~]#  *pcs status*
Cluster name: my_cluster
Last updated: Thu Jan  8 11:13:17 2015
Last change: Thu Jan  8 11:13:08 2015
Stack: corosync
Current DC: z2.example.com (2) - partition with quorum
Version: 1.1.12-a14efad
2 Nodes configured
3 Resources configured

Online: [ z1.example.com z2.example.com ]

Full list of resources:
 myapc  (stonith:fence_apc_snmp):       Started z1.example.com
 Resource Group: nfsgroup
     my_lvm     (ocf::heartbeat:LVM-activate):   Started z1.example.com

PCSD Status:
  z1.example.com: Online
  z2.example.com: Online

Daemon Status:
  corosync: active/enabled
  pacemaker: active/enabled
  pcsd: active/enabled

....

. Configure a `Filesystem` resource for the cluster.
+
The following command configures an XFS `Filesystem` resource named `nfsshare` as part of the `nfsgroup` resource group. This file system uses the LVM volume group and XFS file system you created in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_configuring-active-passive-nfs-server-in-a-cluster-configuring-and-managing-high-availability-clusters#proc_configuring-lvm-volume-with-ext4-file-system-configuring-ha-nfs[Configuring an LVM volume with an XFS file system] and will be mounted on the `/nfsshare` directory you created in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_configuring-active-passive-nfs-server-in-a-cluster-configuring-and-managing-high-availability-clusters#proc_configuring-nfs-share-configuring-ha-nfs[Configuring an NFS share].
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create nfsshare Filesystem device=/dev/my_vg/my_lv directory=/nfsshare fstype=xfs --group nfsgroup*

....
+
You can specify mount options as part of the resource configuration for a `Filesystem` resource with the `options=pass:attributes[{blank}]_options_pass:attributes[{blank}]` parameter. Run the [command]`pcs resource describe Filesystem` command for full configuration options.

. Verify that the `my_lvm` and `nfsshare` resources are running.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs status*
...
Full list of resources:
 myapc  (stonith:fence_apc_snmp):       Started z1.example.com
 Resource Group: nfsgroup
     my_lvm     (ocf::heartbeat:LVM-activate):   Started z1.example.com
     nfsshare   (ocf::heartbeat:Filesystem):    Started z1.example.com
...

....
. Create the `nfsserver` resource named `nfs-daemon` as part of the resource group `nfsgroup`.
+
[NOTE]
====

The `nfsserver` resource allows you to specify an `nfs_shared_infodir` parameter, which is a directory that NFS servers use to store NFS-related stateful information.

It is recommended that this attribute be set to a subdirectory of one of the `Filesystem` resources you created in this collection of exports. This ensures that the NFS servers are storing their stateful information on a device that will become available to another node if this resource group needs to relocate. In this example;

* `/nfsshare` is the shared-storage directory managed by the `Filesystem` resource

* `/nfsshare/exports/export1` and `/nfsshare/exports/export2` are the export directories

* `/nfsshare/nfsinfo` is the shared-information directory for the `nfsserver` resource

====
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create nfs-daemon nfsserver nfs_shared_infodir=/nfsshare/nfsinfo nfs_no_notify=true --group nfsgroup*

[root@z1 ~]# *pcs status*
...

....

. Add the `exportfs` resources to export the `/nfsshare/exports` directory. These resources are part of the resource group `nfsgroup`. This builds a virtual directory for NFSv4 clients. NFSv3 clients can access these exports as well.
+
[NOTE]
====

The `fsid=0` option is required only if you want to create a virtual directory for NFSv4 clients. For more information, see link:++https://access.redhat.com/solutions/548083/++[How do I configure the fsid option in an NFS server's /etc/exports file?].
																	 //
====
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create  nfs-root exportfs clientspec=192.168.122.0/255.255.255.0 options=rw,sync,no_root_squash directory=/nfsshare/exports fsid=0 --group nfsgroup*

[root@z1 ~]# *pcs resource create  nfs-export1 exportfs clientspec=192.168.122.0/255.255.255.0 options=rw,sync,no_root_squash directory=/nfsshare/exports/export1 fsid=1 --group nfsgroup*

[root@z1 ~]# *pcs resource create  nfs-export2 exportfs clientspec=192.168.122.0/255.255.255.0 options=rw,sync,no_root_squash directory=/nfsshare/exports/export2 fsid=2 --group nfsgroup*

....

. Add the floating IP address resource that NFS clients will use to access the NFS share. This resource is part of the resource group `nfsgroup`. For this example deployment, we are using 192.168.122.200 as the floating IP address.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create nfs_ip IPaddr2 ip=192.168.122.200 cidr_netmask=24 --group nfsgroup*

....

. Add an `nfsnotify` resource for sending NFSv3 reboot notifications once the entire NFS deployment has initialized. This resource is part of the resource group `nfsgroup`.
+
[NOTE]
====

For the NFS notification to be processed correctly, the floating IP address must have a host name associated with it that is consistent on both the NFS servers and the NFS client.

====
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs resource create nfs-notify nfsnotify source_host=192.168.122.200 --group nfsgroup*

....

. After creating the resources and the resource constraints, you can check the status of the cluster. Note that all resources are running on the same node.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs status*
...
Full list of resources:
 myapc  (stonith:fence_apc_snmp):       Started z1.example.com
 Resource Group: nfsgroup
     my_lvm     (ocf::heartbeat:LVM-activate):   Started z1.example.com
     nfsshare   (ocf::heartbeat:Filesystem):    Started z1.example.com
     nfs-daemon (ocf::heartbeat:nfsserver):     Started z1.example.com
     nfs-root   (ocf::heartbeat:exportfs):      Started z1.example.com
     nfs-export1        (ocf::heartbeat:exportfs):      Started z1.example.com
     nfs-export2        (ocf::heartbeat:exportfs):      Started z1.example.com
     nfs_ip     (ocf::heartbeat:IPaddr2):       Started  z1.example.com
     nfs-notify (ocf::heartbeat:nfsnotify):     Started z1.example.com
...

....
