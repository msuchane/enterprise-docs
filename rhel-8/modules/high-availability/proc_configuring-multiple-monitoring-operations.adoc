:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_resource-monitoring-operations.adoc

[id='proc_configuring-multiple-monitoring-operations-{context}']

= Configuring multiple monitoring operations

[role="_abstract"]
You can configure a single resource with as many monitor operations as a resource agent supports. In this way you can do a superficial health check every minute and progressively more intense ones at higher intervals.

[NOTE]
====

When configuring multiple monitor operations, you must ensure that no two operations are performed at the same interval.

====

To configure additional monitoring operations for a resource that supports more in-depth checks at different levels, you add an `OCF_CHECK_LEVEL=pass:attributes[{blank}]_n_pass:attributes[{blank}]` option.

For example, if you configure the following `IPaddr2` resource, by default this creates a monitoring operation with an interval of 10 seconds and a timeout value of 20 seconds.

[subs="+quotes"]
....

# *pcs resource create VirtualIP ocf:heartbeat:IPaddr2 ip=192.168.0.99 cidr_netmask=24 nic=eth2* 

....

If the Virtual IP supports a different check with a depth of 10, the following command causes Pacemaker to perform the more advanced monitoring check every 60 seconds in addition to the normal Virtual IP check every 10 seconds. (As noted, you should not configure the additional monitoring operation with a 10-second interval as well.)

[subs="+quotes"]
....

# *pcs resource op add VirtualIP monitor interval=60s OCF_CHECK_LEVEL=10*

....
