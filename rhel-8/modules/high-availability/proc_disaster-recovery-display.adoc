:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-disaster-recovery.adoc

[id='proc_disaster-recovery-display-{context}']

= Displaying status of recovery clusters

[role="_abstract"]
To configure a primary and a disaster recovery cluster so that you can display the status of both clusters, perform the following procedure.
ifeval::[{ProductNumber} == 8]
(RHEL 8.2 and later)
endif::[]

[NOTE]
====

Setting up a disaster recovery cluster does not automatically configure resources or replicate data. Those items must be configured manually by the user.

====

In this example:

* The primary cluster will be named `PrimarySite` and will consist of the nodes `z1.example.com`. and `z2.example.com`.

* The disaster recovery site cluster will be named `DRsite` and will consist of the nodes `z3.example.com` and `z4.example.com`.

This example sets up a basic cluster with no resources or fencing configured.

.Procedure

. Authenticate all of the nodes that will be used for both clusters.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs host auth z1.example.com z2.example.com z3.example.com z4.example.com  -u hacluster -p password*
z1.example.com: Authorized
z2.example.com: Authorized
z3.example.com: Authorized
z4.example.com: Authorized

....

. Create the cluster that will be used as the primary cluster and start cluster services for the cluster.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs cluster setup PrimarySite z1.example.com z2.example.com  --start* 
{...}
Cluster has been successfully set up.
Starting cluster on hosts: 'z1.example.com', 'z2.example.com'...
....

. Create the cluster that will be used as the disaster recovery cluster and start cluster services for the cluster.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs cluster setup DRSite z3.example.com z4.example.com --start*
{...}
Cluster has been successfully set up.
Starting cluster on hosts: 'z3.example.com', 'z4.example.com'...

....

. From a node in the primary cluster, set up the second cluster as the recovery site. The recovery site is defined by a name of one of its nodes.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs dr set-recovery-site z3.example.com*
Sending 'disaster-recovery config' to 'z3.example.com', 'z4.example.com'
z3.example.com: successful distribution of the file 'disaster-recovery config'
z4.example.com: successful distribution of the file 'disaster-recovery config'
Sending 'disaster-recovery config' to 'z1.example.com', 'z2.example.com'
z1.example.com: successful distribution of the file 'disaster-recovery config'
z2.example.com: successful distribution of the file 'disaster-recovery config'

....

. Check the disaster recovery configuration.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs dr config*
Local site:
  Role: Primary
Remote site:
  Role: Recovery
  Nodes:
    z3.example.com
    z4.example.com

....

. Check the status of the primary cluster and the disaster recovery cluster from a node in the primary cluster.
+
[subs="+quotes"]
....

[root@z1 ~]# *pcs dr status*
--- Local cluster - Primary site ---
Cluster name: PrimarySite

WARNINGS:
No stonith devices and stonith-enabled is not false

Cluster Summary:
  * Stack: corosync
  * Current DC: z2.example.com (version 2.0.3-2.el8-2c9cea563e) - partition with quorum
  * Last updated: Mon Dec  9 04:10:31 2019
  * Last change:  Mon Dec  9 04:06:10 2019 by hacluster via crmd on z2.example.com
  * 2 nodes configured
  * 0 resource instances configured

Node List:
  * Online: [ z1.example.com z2.example.com ]

Full List of Resources:
  * No resources

Daemon Status:
  corosync: active/disabled
  pacemaker: active/disabled
  pcsd: active/enabled


--- Remote cluster - Recovery site ---
Cluster name: DRSite

WARNINGS:
No stonith devices and stonith-enabled is not false

Cluster Summary:
  * Stack: corosync
  * Current DC: z4.example.com (version 2.0.3-2.el8-2c9cea563e) - partition with quorum
  * Last updated: Mon Dec  9 04:10:34 2019
  * Last change:  Mon Dec  9 04:09:55 2019 by hacluster via crmd on z4.example.com
  * 2 nodes configured
  * 0 resource instances configured

Node List:
  * Online: [ z3.example.com z4.example.com ]

Full List of Resources:
  * No resources

Daemon Status:
  corosync: active/disabled
  pacemaker: active/disabled
  pcsd: active/enabled
....

For additional display options for a disaster recovery configuration, see the help screen for the `pcs dr` command.


