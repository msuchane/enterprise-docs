:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role.adoc

[id="configuring-ha-cluster-with-quorum-device_{context}"]
= Configuring a high availability cluster using a quorum device

ifeval::[{ProductNumber} == 8]
(RHEL 8.8 and later)
endif::[]
ifeval::[{ProductNumber} == 9]
(RHEL 9.2 and later)
endif::[]
To configure a high availability cluster with a separate quorum device by using the `ha_cluster` System Role, first set up the quorum device. After setting up the quorum device, you can use the device in any number of clusters.


include::proc_configuring-a-quorum-device.adoc[leveloffset=+1]

include::proc_configuring-aa-cluster-to-use-a-quorum-device.adoc[leveloffset=+1]

