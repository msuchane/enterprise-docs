:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-cluster-quorum.adoc

[id='proc_modifying-quorum-options-{context}']

= Modifying quorum options

[role="_abstract"]
You can modify general quorum options for your cluster with the [command]`pcs quorum update` command. You can modify the `quorum.two_node` and `quorum.expected_votes` options on a running system. For all other quorum options, executing this command requires that the cluster be stopped. For information on the quorum options, see the [command]`votequorum`(5) man page.

The format of the [command]`pcs quorum update` command is as follows.

[subs="+quotes"]
....

pcs quorum update [auto_tie_breaker=[0|1]] [last_man_standing=[0|1]] [last_man_standing_window=[_time-in-ms_] [wait_for_all=[0|1]]

....

The following series of commands modifies the `wait_for_all` quorum option and displays the updated status of the option. Note that the system does not allow you to execute this command while the cluster is running.

[subs="+quotes"]
....

[root@node1:~]# *pcs quorum update wait_for_all=1*
Checking corosync is not running on nodes...
Error: node1: corosync is running
Error: node2: corosync is running

[root@node1:~]# *pcs cluster stop --all*
node2: Stopping Cluster (pacemaker)...
node1: Stopping Cluster (pacemaker)...
node1: Stopping Cluster (corosync)...
node2: Stopping Cluster (corosync)...

[root@node1:~]# *pcs quorum update wait_for_all=1*
Checking corosync is not running on nodes...
node2: corosync is not running
node1: corosync is not running
Sending updated corosync.conf to nodes...
node1: Succeeded
node2: Succeeded

[root@node1:~]# *pcs quorum config*
Options:
  wait_for_all: 1

....

