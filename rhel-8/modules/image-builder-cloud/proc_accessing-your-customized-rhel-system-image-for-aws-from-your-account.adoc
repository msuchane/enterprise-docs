:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="accessing-your-customized-rhel-system-image-for-aws-from-your-account_{context}"]
* Title: = Accessing your customized RHEL system image for AWS from your account

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

[id="accessing-your-customized-rhel-system-image-for-aws-from-your-account_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Accessing your customized RHEL system image for AWS from your account
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
After the image is built, uploaded, and the cloud registration process status is marked as *Ready*, you can access the Amazon Web Services (AWS) image you created and shared with your *AWS EC2* account. 

.Prerequisites

* You have access to your link:https://us-east-2.console.aws.amazon.com/ec2/v2/home?region=us-east-2#Images:sort=name[AWS Management Console].

.Procedure

. Access your link:https://us-east-2.console.aws.amazon.com/ec2/v2/home?region=us-east-2#Images:sort=name[AWS account] and navigate to Service->EC2. 

. In the upper right menu, verify if you are under the correct region: `us-east-1`.

. In the left side menu, under *Images*, click *AMIs*.
+
The dashboard with the *Owned by me* images opens.

. From the dropdown menu, choose *Private images*.
+
You can see the image successfully shared with the *AWS* account you specified.
