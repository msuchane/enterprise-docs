:converting-to-rhel-in-insights:

// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

This document provides instructions on how to convert your operating system from CentOS Linux to Red Hat Enterprise Linux (RHEL) 7 using Red Hat Insights. 

include::common-content/making-open-source-more-inclusive.adoc[]

include::modules/common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

//include::modules/conversion/con_service-preview-title-warning.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_key-migration-terminology.adoc[leveloffset=+1]

include::modules/conversion/con_supported-conversion-paths.adoc[leveloffset=+1]

include::modules/conversion/con_conversion-methods.adoc[leveloffset=+1]

include::modules/conversion/con_planning-a-rhel-conversion.adoc[leveloffset=+1]

include::modules/conversion/proc_preparing-for-a-rhel-conversion-using-insights.adoc[leveloffset=+1]

include::modules/conversion/proc_reviewing-the-pre-conversion-analysis-report-using-insights.adoc[leveloffset=+1]

include::modules/conversion/proc_converting-to-a-rhel-system-using-insights.adoc[leveloffset=+1]

//include::modules/conversion/con_rollback.adoc[leveloffset=+1]

include::assemblies/assembly_troubleshooting-rhel-conversions.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/converting_from_an_rpm-based_linux_distribution_to_rhel/index[Converting from an RPM-based Linux distribution to RHEL]
* link:https://access.redhat.com/articles/2360841[How to perform an unsupported conversion from a RHEL-derived Linux distribution to RHEL]
* link:https://access.redhat.com/articles/rhel-limits[Red Hat Enterprise Linux technology capabilities and limits]
* link:https://access.redhat.com/articles/5941531[ Convert2RHEL FAQ (Frequently Asked Questions) ]
