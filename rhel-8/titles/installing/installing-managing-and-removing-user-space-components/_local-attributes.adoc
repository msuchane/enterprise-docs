// Show the table of contents
:toc:
// The name of the title
:ProjectName: Installing, managing, and removing user-space components
// The subtitle of the title
:Subtitle: Managing content in the BaseOS and AppStream repositories by using the YUM software management tool
// The abstract of the title
:Abstract: Find, install, and utilize content distributed through the BaseOS and AppStream repositories by using the YUM tool. Learn how to work with packages, modules, streams, and profiles.
// The name of the title for the purposes of {context}
:ProjectNameID: using-appstream

:doctype: book
