// Show the table of contents
:toc:
// The name of the title
:ProjectName: Developing IBM Cloud using RHEL 8 Hypervisor for Public Clouds
// The subtitle of the title
:Subtitle: Developing IBM Cloud features using the virtualization APIs in {ProductName}{nbsp}{ProductNumber} 8
// The abstract of the title
:Abstract: This document describes how to use the {ProductName}{nbsp}{ProductNumber} (RHEL 8) Hypervisor for Public Clouds as a developer of IBM Cloud. More details TBD?
// The name of the title for the purposes of {context}
:ProjectNameID: developing-ibm-cloud-using-rhel-8-AV

// To enable reusability
:virt-title:

// The following are not required
:ProjectVersion: 0.1
