// Show the table of contents
:toc:
// The name of the title
:ProjectName: Migrating from previous versions of .NET
// The subtitle of the title
:Subtitle: Migration from previous versions of .NET and porting from .NET Framework

// The abstract of the title
:Abstract: This guide describes how migrate from previous versions of .NET and how to port from .NET Framework.
// The name of the title for the purposes of {context}
:ProjectNameID: migrating-from-previous-version-of-dotnet

// The following are not required
:ProjectVersion:
