// Include shared, global .NET attributes
//////////////////////////////////////////////////////////////////////////////
// *IMPORTANT*:
// RHEL version MUST be defined in rhel-dotnet-attribute.adoc
// .NET version MUST be defined in common-content/global-dotnet-attributes.adoc
//////////////////////////////////////////////////////////////////////////////

include::rhel-dotnet-attribute.adoc[]

include::common-content/_global-dotnet-attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::modules/dotnet/con_introducing-dotnet.adoc[leveloffset=+1]

//remove
//include::assemblies/assembly_using-dotnet-on-rhel.adoc[leveloffset=+1]

include::modules/dotnet/proc_installing-dotnet.adoc[leveloffset=+1]

include::modules/dotnet/proc_creating-an-application-using-dotnet.adoc[leveloffset=+1]

include::assemblies/assembly_publishing-apps-using-dotnet.adoc[leveloffset=+1]

include::modules/dotnet/proc_running-apps-in-containers-using-dotnet.adoc[leveloffset=+1]

ifeval::[{dotnet-ver} == 2.1]
include::modules/dotnet/ref_appendix-revision-history.adoc[leveloffset=+1]
endif::[]

include::assemblies/assembly_using-dotnet-on-openshift-container-platform.adoc[leveloffset=+1]

include::assemblies/assembly_dotnet-migration.adoc[leveloffset=+1]
