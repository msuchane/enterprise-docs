:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="the-certificate-system-role_{context}"]
= The `certificate` system role

[role="_abstract"]

Using the `certificate` system role, you can manage issuing and renewing TLS and SSL certificates using Ansible Core.

The role uses `certmonger` as the certificate provider, and currently supports issuing and renewing self-signed certificates and using the IdM integrated certificate authority (CA).

You can use the following variables in your Ansible playbook with the `certificate` system role:

`certificate_wait`:: to specify if the task should wait for the certificate to be issued.
`certificate_requests`:: to represent each certificate to be issued and its parameters.

[role="_additional-resources"]
.Additional resources
* See the `/usr/share/ansible/roles/rhel-system-roles.certificate/README.md` file.
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[Preparing a control node and managed nodes to use RHEL system roles]
//link format correct? Or should I xref/something elses?
