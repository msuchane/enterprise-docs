:_mod-docs-content-type: PROCEDURE

[id="configuring-server-logging-with-relp_{context}"]
= Configuring server logging with RELP

[role="_abstract"]
You can use the `logging` system role to configure logging in RHEL systems as a server and can receive logs from the remote logging system with RELP by running an Ansible playbook.

This procedure configures RELP on all hosts in the `server` group in the Ansible inventory. The RELP configuration uses TLS to encrypt the message transmission for secure transfer of logs over the network.

.Prerequisites

* You have permissions to run playbooks on managed nodes on which you want to configure RELP.
* The managed nodes are listed in the inventory file on the control node.
* The `ansible` and `rhel-system-roles` packages are installed on the control node.

.Procedure

. Create a `_playbook.yml_` file with the following content:
+
----
---
- name: Deploying remote input and remote_files output
  hosts: server
  roles:
    - rhel-system-roles.logging
  vars:
    logging_inputs:
      - name: relp_server
        type: relp
        port: 20514
        tls: true
        ca_cert: _/etc/pki/tls/certs/ca.pem_
        cert: _/etc/pki/tls/certs/server-cert.pem_
        private_key: _/etc/pki/tls/private/server-key.pem_
        pki_authmode: name
        permitted_clients:
          - '_*example.client.com_'
    logging_outputs:
      - name: _remote_files_output_
        type: _remote_files_
    logging_flows:
      - name: _example_flow_
        inputs: _relp_server_
        outputs: _remote_files_output_
----
+
The playbooks uses following settings:

** `port`: Port number the remote logging system is listening.

** `tls`: Ensures secure transfer of logs over the network. If you do not want a secure wrapper you can set the `tls` variable to `false`. By default `tls` parameter is set to true while working with RELP and requires key/certificates and triplets {`ca_cert`, `cert`, `private_key`} and/or {`ca_cert_src`, `cert_src`, `private_key_src`}.

*** If {`ca_cert_src`, `cert_src`, `private_key_src`} triplet is set, the default locations `/etc/pki/tls/certs` and `/etc/pki/tls/private` are used as the destination on the managed node to transfer files from control node. In this case, the file names are identical to the original ones in the triplet

*** If {`ca_cert`, `cert`, `private_key`} triplet is set, files are expected to be on the default path before the logging configuration.

*** If both the triplets are set, files are transferred from local path from control node to specific path of the managed node.

** `ca_cert`: Represents the path to CA certificate. Default path is `/etc/pki/tls/certs/ca.pem` and the file name is set by the user.

** `cert`: Represents the path to cert. Default path is `/etc/pki/tls/certs/server-cert.pem` and the file name is set by the user.

** `private_key`: Represents the path to private key. Default path is `/etc/pki/tls/private/server-key.pem` and the file name is set by the user.

** `ca_cert_src`: Represents local CA cert file path which is copied to the target host. If ca_cert is specified, it is copied to the location.

** `cert_src`: Represents the local cert file path which is copied to the target host. If cert is specified, it is copied to the location.

** `private_key_src`: Represents the local key file path which is copied to the target host. If private_key is specified, it is copied to the location.

** `pki_authmode`: Accepts the authentication mode as `name` or `fingerprint`.

** `permitted_clients`: List of clients that will be allowed by the logging server to connect and send logs over TLS.

** `inputs`: List of logging input dictionary.

** `outputs`: List of logging output dictionary.

. Optional: Verify playbook syntax.
+
[subs="quotes,attributes"]
----
# *ansible-playbook --syntax-check _playbook.yml_*
----

. Run the playbook:
+
[subs="quotes,attributes"]
----
# *ansible-playbook -i _inventory_file_ _playbook.yml_*
----
