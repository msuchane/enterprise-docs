////

////
:_mod-docs-content-type: PROCEDURE

[id="unregistering-a-system-using-the-rhc-system-role_{context}"]
= Unregistering a system by using the RHC system role

You can unregister the system from Red Hat if you no longer need the subscription service. 

.Prerequisites
// Common prerequisites for all system role procedures:
ifdef::system-roles-ansible[]
* xref:assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes].
endif::[]
* You are logged in to the control node as a user who can run playbooks on the managed nodes.
* The account you use to connect to the managed nodes has `sudo` permissions on them.
* The managed nodes or groups of managed nodes on which you want to run this playbook are listed in the Ansible inventory file.

// The following are prerequisites that are specific for this task:
* The system is already registered. 


.Procedure

. To unregister, create a playbook file, for example, `~/unregister.yml` and add the following content to it:
+
[source,yaml,subs="+quotes"]
----
---
- name: Unregister the system
  hosts: managed-node-01.example.com
  vars:
    rhc_state: __absent__
  roles:
    - role: rhel-system-roles.rhc
----

. Run the playbook:
+
[subs="quotes",attributes options="nowrap",role=white-space-pre]
----
# ansible-playbook ~/unregister.yml
----

[role="_additional-resources"]
.Additional resources

* The `/usr/share/ansible/roles/rhel-system-roles.rhc/README.md` file
