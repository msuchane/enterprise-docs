
:_mod-docs-content-type: REFERENCE

[id="variables-for-the-ad-integration-rhel-system-role_{context}"]
= Variables for the `ad_integration` RHEL system role

[role="_abstract"]

The `ad_integration` RHEL system role uses the following parameters:

[%header,cols=2*]
|===
|Role Variable
|Description

| ad_integration_realm
|Active Directory realm, or domain name to join.

| ad_integration_password
|The password of the user used to authenticate with when joining the machine to the realm. Do not use plain text. Instead, use Ansible Vault to encrypt the value.

| ad_integration_manage_crypto_policies
|If `true`, the `ad_integration` role will use `fedora.linux_system_roles.crypto_policies` as needed.

Default: `false`

| ad_integration_allow_rc4_crypto
|If `true`, the `ad_integration` role will set the crypto policy to allow RC4 encryption. 

Providing this variable automatically sets `ad_integration_manage_crypto_policies` to `true`.

Default: `false`
| ad_integration_timesync_source
|Hostname or IP address of time source to synchronize the system clock with. 
Providing this variable automatically sets `ad_integration_manage_timesync` to `true`.
|===

[role="_additional-resources"]
.Additional resources

* The `/usr/share/ansible/roles/rhel-system-roles.ad_integration/README.md` file.
