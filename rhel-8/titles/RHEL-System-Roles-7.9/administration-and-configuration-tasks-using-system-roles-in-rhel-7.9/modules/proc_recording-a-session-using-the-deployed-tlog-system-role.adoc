:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="recording-a-session-using-the-deployed-tlog-system-role_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
=  Recording a session using the deployed `tlog` system role in the CLI

// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
After you have deployed the `tlog` system role in the system you have specified, you are able to record a user terminal session using the command-line interface (CLI).

.Prerequisites

* You have deployed the `tlog` system role in the target system.
* The SSSD configuration drop file was created in the `/etc/sssd/conf.d` directory. See xref:deploying-the-tlog-system-role_configuring-a-system-for-session-recording-using-the-tlog-rhel-system-roles[Deploying the Terminal Session Recording {RHELSystemRoles}].

.Procedure

. Create a user and assign a password for this user:
+
[subs="quotes,attributes"]
----
# *useradd _recorded-user_*
# *passwd _recorded-user_*
----

. Log in to the system as the user you just created:
+
[subs="quotes,attributes"]
----
# *ssh recorded-user@localhost*
----

. Type "yes" when the system prompts you to type yes or no to authenticate.

. Insert the _recorded-user's_ password.
+
The system displays a message about your session being recorded.
+
[subs="quotes,attributes"]
----
ATTENTION! Your session is being recorded!
----

. After you have finished recording the session, type:
+
[subs="quotes,attributes"]
----
# *exit*
----
+
The system logs out from the user and closes the connection with the localhost.

As a result, the user session is recorded, stored and you can play it using a journal.

.Verification steps

To view your recorded session in the journal, do the following steps:

. Run the command below:
+
[subs="quotes,attributes"]
----
# *journalctl -o verbose -r*
----

. Search for the `MESSAGE` field of the `tlog-rec` recorded journal entry.
+
[subs="quotes,attributes"]
----
# *journalctl -xel _EXE=/usr/bin/tlog-rec-session*
----
