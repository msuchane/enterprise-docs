:_module-type: REFERENCE

[id="ref_high-availability-variables_{context}"]
= High availability variables

[role="_abstract"]
You can configure high availability for Microsoft SQL Server with the variables from the table below.

.High availability configuration variables
[options="header"]
|====
|Variable|Description
|`mssql_ha_configure` a|The default value is `false`.

When it is set to `true`, performs the following actions:

* Configures firewall by opening a port from the `mssql_ha_listener_port` variable and enables the `high-availability` service in firewall.
* Configures SQL Server for high availability.
** Enables Always On Health events.
** Creates certificate on the primary replica and distributes it to other replicas.
** Configures endpoint and availability group.
** Configures the user from the `mssql_ha_login` variable for Pacemaker.
* Optional: Includes the system roles `ha_cluster` role to configure Pacemaker. You must set `mssql_ha_cluster_run_role` to `true` and provide all variables that the `ha_cluster` role requires for a Pacemaker cluster configuration.

|`mssql_ha_replica_type`|This variable specifies which type of replica you can configure on the host. You can set this variable to `primary`, `synchronous`, and `witness`. You must set it to `primary` only on one host.
|`mssql_ha_listener_port`|The default port is `5022`.

The role uses this TCP port to replicate data for an Always On availability group.
|`mssql_ha_cert_name`|You must define the name of the certificate to secure transactions between members of an Always On availability group.
|`mssql_ha_master_key_password`|You must set the password for the master key to use with the certificate.
|`mssql_ha_private_key_password`|You must set the password for the private key to use with the certificate.
|`mssql_ha_reset_cert`|The default value is `false`.

If it is set to `true`, resets the certificate which an Always On availability group uses.
|`mssql_ha_endpoint_name`|You must define the name of the endpoint to configure.
|`mssql_ha_ag_name`|You must define the name of the availability group to configure.
|`mssql_ha_db_names`|You can define a list of the databases to replicate, otherwise the role creates a cluster without replicating databases.
|`mssql_ha_login`|The SQL Server Pacemaker resource agent utilizes this user to perform database health checks and manage state transitions from replica to primary server.
|`mssql_ha_login_password`|The password for the `mssql_ha_login` user in SQL Server.
|`mssql_ha_cluster_run_role`|The default value is `false`.

This variable defines if this role runs the `ha_cluster` role.

Note that the `ha_cluster` role replaces the configuration of the HA cluster on specified nodes, any variables currently configured for the HA cluster are erased and overwritten.

To work around this limitation, the `microsoft.sql.server` role does not set any variables for the `ha_cluster` role to ensure that it does not overwrite any existing Pacemaker configuration.

If you want the `microsoft.sql.server` to run the `ha_cluster` role, set this variable to `true` and provide variables for the `ha_cluster` role with the `microsoft.sql.server` role call.
|====

Note, this role backs up the database to the `/var/opt/mssql/data/` directory.

For examples on how to use high availability variables for Microsoft SQL Server:

* If you install the role from Automation Hub, see the `~/.ansible/collections/ansible_collections/microsoft/sql/roles/server/README.md` file on your server.
* If you install the role from a package, open the `/usr/share/microsoft/sql-server/README.html` file in your browser.
