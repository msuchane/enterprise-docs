:_mod-docs-content-type: CONCEPT
:experimental:
// included in assembly_deploying-the-same-selinux-configuration-on-multiple-systems.adoc

[id="introduction-to-the-selinux-system-role_{context}"]
= Introduction to the `selinux` system role

[role="_abstract"]
{RHELSystemRoles}s is a collection of Ansible roles and modules that provide a consistent configuration interface to remotely manage multiple {ProductShortName} systems. The `selinux` system role enables the following actions:

* Cleaning local policy modifications related to SELinux booleans, file contexts, ports, and logins.
* Setting SELinux policy booleans, file contexts, ports, and logins.
* Restoring file contexts on specified files or directories.
* Managing SELinux modules.

The following table provides an overview of input variables available in the `selinux` system role.

.`selinux` system role variables
[options="header"]
|====
|Role variable|Description|CLI alternative
|selinux_policy|Chooses a policy protecting targeted processes or Multi Level Security protection.|`SELINUXTYPE` in `/etc/selinux/config`
|selinux_state|Switches SELinux modes.|`setenforce` and `SELINUX` in `/etc/selinux/config`.
|selinux_booleans|Enables and disables SELinux booleans.|`setsebool`
|selinux_fcontexts|Adds or removes a SELinux file context mapping.|`semanage fcontext`
|selinux_restore_dirs|Restores SELinux labels in the file-system tree.|`restorecon -R`
|selinux_ports|Sets SELinux labels on ports.|`semanage port`
|selinux_logins|Sets users to SELinux user mapping.|`semanage login`
|selinux_modules|Installs, enables, disables, or removes SELinux modules.|`semodule`
|====

The `/usr/share/doc/rhel-system-roles/selinux/example-selinux-playbook.yml` example playbook installed by the `rhel-system-roles` package demonstrates how to set the targeted policy in enforcing mode. The playbook also applies several local policy modifications and restores file contexts in the `/tmp/test_dir/` directory.

For a detailed reference on `selinux` role variables, install the `rhel-system-roles` package, and see the `README.md` or `README.html` files in the `/usr/share/doc/rhel-system-roles/selinux/` directory.

[role="_additional-resources"]
.Additional resources
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[Introduction to RHEL system roles]
