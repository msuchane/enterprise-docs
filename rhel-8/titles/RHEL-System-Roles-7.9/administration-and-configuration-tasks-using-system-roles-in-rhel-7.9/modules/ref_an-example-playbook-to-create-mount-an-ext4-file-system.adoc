:_mod-docs-content-type: REFERENCE

[id="an-example-playbook-to-create-mount-an-ext4-file-system_{context}"]
= Example Ansible playbook to create and mount an Ext4 file system

[role="_abstract"]
This section provides an example Ansible playbook. This playbook applies the `storage` role to create and mount an Ext4 file system.

.A playbook that creates Ext4 on /dev/sdb and mounts it at /mnt/data
====

[subs="+quotes"]
----
---
- hosts: all
  vars:
    storage_volumes:
      - name: [replaceable]__barefs__
        type: disk
        disks:
          - [replaceable]__sdb__
        fs_type: ext4
        fs_label: [replaceable]__label-name__
        mount_point: [replaceable]__/mnt/data__
  roles:
    - rhel-system-roles.storage
----

* The playbook creates the file system on the `/dev/sdb` disk.

* The playbook persistently mounts the file system at the `[replaceable]__/mnt/data__` directory.

* The label of the file system is `[replaceable]__label-name__`.

====

[role="_additional-resources"]
.Additional resources
* The [filename]`/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file.
