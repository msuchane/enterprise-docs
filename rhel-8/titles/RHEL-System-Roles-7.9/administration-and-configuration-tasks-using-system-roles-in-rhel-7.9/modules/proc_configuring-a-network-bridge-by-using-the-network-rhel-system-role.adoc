:_mod-docs-content-type: PROCEDURE
:experimental:

[id="configuring-a-network-bridge-by-using-the-network-rhel-system-role_{context}"]
= Configuring a network bridge by using the network {RHELSystemRoles}

[role="_abstract"]
You can use the `network` {RHELSystemRoles} to configure a Linux bridge. For example, use it to configure a network bridge that uses two Ethernet devices, and sets IPv4 and IPv6 addresses, default gateways, and DNS configuration.

[NOTE]
====
Set the IP configuration on the bridge and not on the ports of the Linux bridge.
====

Perform this procedure on the Ansible control node.

.Prerequisites
// Common prerequisites for all system role procedures:
ifdef::system-roles-ansible[]
* xref:assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes].
endif::[]
ifndef::system-roles-ansible[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes]
endif::[]
* You are logged in to the control node as a user who can run playbooks on the managed nodes.
* The account you use to connect to the managed nodes has `sudo` permissions on them.
* The managed nodes or groups of managed nodes on which you want to run this playbook are listed in the Ansible inventory file.

// The following are prerequisites that are specific for this task:
* Two or more physical or virtual network devices are installed on the server.


.Procedure

. Create a playbook file, for example `__~/bridge-ethernet.yml__`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Configure the network
  hosts: __managed-node-01.example.com__
  tasks:
  - name: Configure a network bridge that uses two Ethernet ports
    include_role:
      name: rhel-system-roles.network

    vars:
      network_connections:
        # Define the bridge profile
        - name: bridge0
          type: bridge
          interface_name: bridge0
          ip:
            address:
              - "192.0.2.1/24"
              - "2001:db8:1::1/64"
            gateway4: 192.0.2.254
            gateway6: 2001:db8:1::fffe
            dns:
              - 192.0.2.200
              - 2001:db8:1::ffbb
            dns_search:
              - example.com
          state: up

        # Add an Ethernet profile to the bridge
        - name: bridge0-port1
          interface_name: enp7s0
          type: ethernet
          controller: bridge0
          port_type: bridge
          state: up

        # Add a second Ethernet profile to the bridge
        - name: bridge0-port2
          interface_name: enp8s0
          type: ethernet
          controller: bridge0
          port_type: bridge
          state: up
....

. Run the playbook:
+
[literal,subs="+quotes"]
....
# **ansible-playbook __~/bridge-ethernet.yml__**
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file

