:_mod-docs-content-type: PROCEDURE
:experimental:


[id="configuring-vlan-tagging-by-using-the-network-rhel-system-role_{context}"]
= Configuring VLAN tagging by using the network {RHELSystemRoles}

[role="_abstract"]
You can use the `network` {RHELSystemRoles} to configure VLAN tagging. This example adds an Ethernet connection and a VLAN with ID [command]`10` on top of this Ethernet connection. As the child device, the VLAN connection contains the IP, default gateway, and DNS configurations.

Depending on your environment, adjust the play accordingly. For example:

* To use the VLAN as a port in other connections, such as a bond, omit the `ip` attribute, and set the IP configuration in the child configuration.
* To use team, bridge, or bond devices in the VLAN, adapt the `interface_name` and `type` attributes of the ports you use in the VLAN.

Perform this procedure on the Ansible control node.


.Prerequisites
// Common prerequisites for all system role procedures:
ifdef::system-roles-ansible[]
* xref:assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes].
endif::[]
ifndef::system-roles-ansible[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes]
endif::[]
* You are logged in to the control node as a user who can run playbooks on the managed nodes.
* The account you use to connect to the managed nodes has `sudo` permissions on them.
* The managed nodes or groups of managed nodes on which you want to run this playbook are listed in the Ansible inventory file.

// The following are prerequisites that are specific for this task:
// none


.Procedure

. Create a playbook file, for example `__~/vlan-ethernet.yml__`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Configure the network
  hosts: __managed-node-01.example.com__
  tasks:
  - name: Configure a VLAN that uses an Ethernet connection
    include_role:
      name: rhel-system-roles.network

    vars:
      network_connections:
        # Add an Ethernet profile for the underlying device of the VLAN
        - name: enp1s0
          type: ethernet
          interface_name: enp1s0
          autoconnect: yes
          state: up
          ip:
            dhcp4: no
            auto6: no

        # Define the VLAN profile
        - name: enp1s0.10
          type: vlan
          ip:
            address:
              - "192.0.2.1/24"
              - "2001:db8:1::1/64"
            gateway4: 192.0.2.254
            gateway6: 2001:db8:1::fffe
            dns:
              - 192.0.2.200
              - 2001:db8:1::ffbb
            dns_search:
              - example.com
          vlan_id: 10
          parent: enp1s0
          state: up
....
+
The `parent` attribute in the VLAN profile configures the VLAN to operate on top of the `enp1s0` device.

. Run the playbook:
+
[literal,subs="+quotes"]
....
# **ansible-playbook __~/vlan-ethernet.yml__**
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file

