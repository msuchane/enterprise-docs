// Module included in the following assemblies:
//
// assembly_getting-started-with-rhel-system-roles.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/con_intro-to-rhel-system-roles.adoc[leveloffset=+1]

:_mod-docs-content-type: CONCEPT
[id="intro-to-rhel-system-roles_{context}"]
= Introduction to {RHELSystemRoles}s

[role="_abstract"]
By using {RHELSystemRoles}s, you can remotely manage the system configurations of multiple RHEL systems across major versions of RHEL.
{RHELSystemRoles}s is a collection of Ansible roles and modules. To use it to configure systems, you must use the following components:

Control node:: A control node is the system from which you run Ansible commands and playbooks. Your control node can be an Ansible Automation Platform, Red Hat Satellite, or a RHEL 9, 8, or 7 host. For more information, see
ifdef::automating-system-administration-by-using-rhel-system-roles[]
xref:proc_preparing-a-control-node_assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[].
endif::[]
ifndef::automating-system-administration-by-using-rhel-system-roles[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles#proc_preparing-a-control-node_assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[Preparing a control node on RHEL {ProductNumber}].
endif::[]


Managed node:: Managed nodes are the servers and network devices that you manage with Ansible. Managed nodes are also sometimes called hosts. Ansible does not have to be installed on managed nodes. For more information, see
ifdef::automating-system-administration-by-using-rhel-system-roles[]
xref:proc_preparing-a-managed-node_assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[].
endif::[]
ifndef::automating-system-administration-by-using-rhel-system-roles[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles#proc_preparing-a-managed-node_assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[Preparing a managed node].
endif::[]


Ansible playbook:: In a playbook, you define the configuration you want to achieve on your managed nodes or a set of steps for the system on the managed node to perform. Playbooks are Ansible’s configuration, deployment, and orchestration language.

Inventory:: In an inventory file, you list the managed nodes and specify information such as IP address for each managed node. In an inventory, you can also organize managed nodes, creating and nesting groups for easier scaling. An inventory file is also sometimes called a hostfile.

On {RHEL} {ProductNumber}, you can use the following roles provided by the `rhel-system-roles` package, which is available in the `AppStream` repository:

[options="header"]
|===
|Role name|Role description|Chapter title
|`certificate`|Certificate Issuance and Renewal|Requesting certificates using RHEL system roles
|`cockpit`|Web console|Installing and configuring web console with the cockpit RHEL system role
|`crypto_policies`|System-wide cryptographic policies|Setting a custom cryptographic policy across systems
|`firewall`|Firewalld|Configuring firewalld using system roles
| `ha_cluster`|HA Cluster| Configuring a high-availability cluster using system roles
| `kdump`|Kernel Dumps| Configuring kdump using RHEL system roles
| `kernel_settings`|Kernel Settings| Using Ansible roles to permanently configure kernel parameters
| `logging`|Logging| Using the logging system role
| `metrics`|Metrics (PCP)| Monitoring performance using RHEL system roles
ifeval::[{ProductNumber} == 8]
| `microsoft.sql.server`|Microsoft SQL Server| Configuring Microsoft SQL Server using the microsoft.sql.server Ansible role
endif::[]
| `network`|Networking| Using the network RHEL system role to manage InfiniBand connections
| `nbde_client`|Network Bound Disk Encryption client| Using the nbde_client and nbde_server system roles
| `nbde_server`|Network Bound Disk Encryption server| Using the nbde_client and nbde_server system roles
| `postfix`|Postfix| Variables of the postfix role in system roles
| `selinux`|SELinux| Configuring SELinux using system roles
| `ssh`|SSH client| Configuring secure communication with the ssh system roles
| `sshd`|SSH server| Configuring secure communication with the ssh system roles
| `storage`|Storage| Managing local storage using RHEL system roles
| `tlog`|Terminal Session Recording| Configuring a system for session recording using the tlog RHEL system role
| `timesync`|Time Synchronization| Configuring time synchronization using RHEL system roles
| `vpn`|VPN| Configuring VPN connections with IPsec by using the vpn RHEL system role
|===

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/articles/3050101[Red Hat Enterprise Linux (RHEL) system roles]
* [filename]`/usr/share/doc/rhel-system-roles/` provided by the `rhel-system-roles` package
