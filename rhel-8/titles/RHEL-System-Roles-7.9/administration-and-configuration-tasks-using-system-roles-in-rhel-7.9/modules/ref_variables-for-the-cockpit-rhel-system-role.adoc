////
Base the file name and the ID on the module title. For example:
* file name: ref-my-reference-a.adoc
* ID: [id="ref-my-reference-a_{context}"]
* Title: = My reference A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix ref- or ref_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: REFERENCE

[id="ref_variables-for-the-cockpit-rhel-system-role_{context}"]
= Variables for the `cockpit` RHEL system role
////
In the title of a reference module, include nouns that are used in the body text. For example, "Keyboard shortcuts for ___" or "Command options for ___." This helps readers and search engines find the information quickly.
////

[role="_abstract"]
The parameters used for the `cockpit` RHEL system roles are:

[%header,cols=2*]
|===
|Role Variable
|Description

| cockpit_packages: (default: default)
|Sets one of the predefined package sets: default, minimal, or full.

* cockpit_packages: (default: default) - most common pages and on-demand install UI

* cockpit_packages: (default: minimal) - just the Overview, Terminal, Logs, Accounts, and Metrics pages; minimal dependencies

* cockpit_packages: (default: full) - all available pages

Optionally, specify your own selection of cockpit packages you want to install.


| cockpit_enabled: (default:true)
|Configures if the web console web server is enabled to start automatically at boot

| cockpit_started: (default:true)
| Configures if the web console should be started

| cockpit_config: (default: nothing)
|You can apply settings in the `/etc/cockpit/cockpit.conf` file.
NOTE: The previous settings file will be lost.

| cockpit_port: (default: 9090)
| The web console runs on port 9090 by default. You can change the port using this option.

| cockpit_manage_firewall: (default: false)
| Allows the `cockpit` role to control the `firewall` role to add ports. It cannot be used for removing ports. If you want to remove ports, you will need to use the firewall system role directly.

| cockpit_manage_selinux: (default: false)
| Allows the `cockpit` role to configure SELinux using the `selinux` role. The default SELinux policy does not allow Cockpit to listen on anything other than port 9090. If you change the port, set this option to `true` so that the `selinux` role can set the correct port permissions (`websm_port_t`).

| cockpit_certificates: (default: nothing)
| Allows the `cockpit` role to generate new certificates using the `certificate` role. The value of `cockpit_certificates` is passed on to the `certificate_requests` variable of the `certificate` role. This role is called internally by the `cockpit` role and it generates the private key and certificate.
|===


[role="_additional-resources"]
.Additional resources
* The `/usr/share/ansible/roles/rhel-system-roles.cockpit/README.md` file.
* The link:https://cockpit-project.org/guide/latest/cockpit.conf.5.html[Cockpit configuration file] man page.
