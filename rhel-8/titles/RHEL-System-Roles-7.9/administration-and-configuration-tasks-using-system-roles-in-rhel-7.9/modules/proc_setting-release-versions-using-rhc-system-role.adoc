////
This module is included in the following assembly: assembly_using-the-rhc-system-role-to-register-the-system.adoc
////
:_mod-docs-content-type: PROCEDURE

[id="setting-release-versions-using-rhc-system-role_{context}"]
= Setting release versions by using the rhc system role

You can limit the system to use only repositories for a particular minor RHEL version instead of the latest one. This way, you can lock your system to a specific minor RHEL version.

.Prerequisites

// Common prerequisites for all system role procedures:
ifdef::system-roles-ansible[]
* xref:assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes].
endif::[]
* You are logged in to the control node as a user who can run playbooks on the managed nodes.
* The account you use to connect to the managed nodes has `sudo` permissions on them.
* The managed nodes or groups of managed nodes on which you want to run this playbook are listed in the Ansible inventory file.

// The following are prerequisites that are specific for this task:
* You know the minor {ProductShortName} version to which you want to lock the system. Note that you can only lock the system to the {ProductShortName} minor version that the host currently runs or a later minor version. 
* You have registered the system. 

.Procedure

. Create a playbook file, for example `~/release.yml`:
+
[source,yaml,subs="+quotes"]
----
---
- name: Set Release
  hosts: __managed-node-01.example.com__
  vars:
    rhc_release: "__8.6__"
  roles:
    - role: rhel-system-roles.rhc
----

. Run the playbook:
+
[subs="quotes",attributes options="nowrap",role=white-space-pre]
----
# *ansible-playbook ~/release.yml*
----

[role="_additional-resources"]
.Additional resources

* The `/usr/share/ansible/roles/rhel-system-roles.rhc/README.md` file
