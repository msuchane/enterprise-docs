:_mod-docs-content-type: PROCEDURE

[id="applying-a-remote-logging-solution-using-the-logging-system-role_{context}"]
= Applying a remote logging solution using the `logging` system role

[role="_abstract"]
Follow these steps to prepare and apply a Red Hat Ansible Core playbook to configure a remote logging solution. In this playbook, one or more clients take logs from `systemd-journal` and forward them to a remote server. The server receives remote input from `remote_rsyslog` and `remote_files` and outputs the logs to local files in directories named by remote host names.

.Prerequisites

* Access and permissions to one or more _managed nodes_, which are systems you want to configure with the `logging` system role.

* Access and permissions to a _control node_, which is a system from which Red Hat Ansible Core configures other systems.
+
On the control node:

** The `ansible-core` and `rhel-system-roles` packages are installed.

** An inventory file which lists the managed nodes.

NOTE: You do not have to have the `rsyslog` package installed, because the system role installs `rsyslog` when deployed.

.Procedure

. Create a playbook that defines the required role:

.. Create a new YAML file and open it in a text editor, for example:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *vi _logging-playbook.yml_*
----

.. Insert the following content into the file:
+
----
---
- name: Deploying remote input and remote_files output
  hosts: server
  roles:
    - rhel-system-roles.logging
  vars:
    logging_inputs:
      - name: remote_udp_input
        type: remote
        udp_ports: [ 601 ]
      - name: remote_tcp_input
        type: remote
        tcp_ports: [ 601 ]
    logging_outputs:
      - name: remote_files_output
        type: remote_files
    logging_flows:
      - name: flow_0
        inputs: [remote_udp_input, remote_tcp_input]
        outputs: [remote_files_output]

- name: Deploying basics input and forwards output
  hosts: clients
  roles:
    - rhel-system-roles.logging
  vars:
    logging_inputs:
      - name: basic_input
        type: basics
    logging_outputs:
      - name: forward_output0
        type: forwards
        severity: info
        target: _host1.example.com_
        udp_port: 601
      - name: forward_output1
        type: forwards
        facility: mail
        target: _host1.example.com_
        tcp_port: 601
    logging_flows:
      - name: flows0
        inputs: [basic_input]
        outputs: [forward_output0, forward_output1]

[basic_input]
[forward_output0, forward_output1]
----
+
Where `_host1.example.com_` is the logging server.
+
NOTE: You can modify the parameters in the playbook to fit your needs.
+
WARNING: The logging solution works only with the ports defined in the SELinux policy of the server or client system and open in the firewall. The default SELinux policy includes ports 601, 514, 6514, 10514, and 20514. To use a different port,
ifdef::using_selinux[]
xref:introduction-to-the-selinux-system-role_deploying-the-same-selinux-configuration-on-multiple-systems[modify the SELinux policy on the client and server systems].
endif::[]
ifndef::using_selinux[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/configuring-selinux-for-applications-and-services-with-non-standard-configurations_using-selinux#customizing-the-selinux-policy-for-the-apache-http-server-in-a-non-standard-configuration_configuring-selinux-for-applications-and-services-with-non-standard-configurations[modify the SELinux policy on the client and server systems].
endif::[]

. Create an inventory file that lists your servers and clients:

.. Create a new file and open it in a text editor, for example:
+
[subs="quotes,attributes"]
----
# *vi _inventory.ini_*
----

.. Insert the following content into the inventory file:
+
[subs="quotes,attributes"]
----
[servers]
server ansible_host=_host1.example.com_
[clients]
client ansible_host=_host2.example.com_
----
+
Where:

** `_host1.example.com_` is the logging server.
** `_host2.example.com_` is the logging client.

. Run the playbook on your inventory.
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *ansible-playbook -i _/path/to/file/inventory.ini_ _/path/to/file/_logging-playbook.yml_*
----
+
Where:

** `_inventory.ini_` is the inventory file.
** `_logging-playbook.yml_` is the playbook you created.

.Verification

. On both the client and the server system, test the syntax of the `/etc/rsyslog.conf` file:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *rsyslogd -N 1*
rsyslogd: version 8.1911.0-6.el8, config validation run (level 1), master config /etc/rsyslog.conf
rsyslogd: End of config validation run. Bye.
----

. Verify that the client system sends messages to the server:
+

.. On the client system, send a test message:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *logger test*
----
.. On the server system, view the `/var/log/messages` log, for example:
+
[subs="quotes,attributes",options="nowrap",role=white-space-pre]
----
# *cat /var/log/messages*
Aug  5 13:48:31 _host2.example.com_ root[6778]: test
----
Where `_host2.example.com_` is the host name of the client system. Note that the log contains the user name of the user that entered the logger command, in this case `root`.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[Preparing a control node and managed nodes to use RHEL system roles]
* Documentation installed with the `rhel-system-roles` package in `/usr/share/ansible/roles/rhel-system-roles.logging/README.html`
* link:https://access.redhat.com/node/3050101[RHEL system roles] KB article
