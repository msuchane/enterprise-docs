
:_mod-docs-content-type: REFERENCE
[id="collections-structure_{context}"]

= Collections structure


[role="_abstract"]
Collections are a package format for Ansible content. The data structure is as below:

* docs/: local documentation for the collection, with examples, if the role provides the documentation

* galaxy.yml: source data for the MANIFEST.json that will be part of the Ansible Collection package

* playbooks/: playbooks are available here

** tasks/: this holds 'task list files' for include_tasks/import_tasks usage

* plugins/: all Ansible plugins and modules are available here, each in its subdirectory

** modules/: Ansible modules

** modules_utils/: common code for developing modules

** lookup/: search for a plugin

** filter/: Jinja2 filter plugin

** connection/: connection plugins required if not using the default

* roles/: directory for Ansible roles

* tests/: tests for the collection's content
