
ifdef::context[:parent-context-of-assembly_the-redfish-modules-in-rhel: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_the-redfish-modules-in-rhel"]
endif::[]
ifdef::context[]
[id="assembly_the-redfish-modules-in-rhel_{context}"]
endif::[]
= The Redfish modules in RHEL
////
If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.
////

:context: assembly_the-redfish-modules-in-rhel

The Redfish modules for remote management of devices are now part of the `redhat.rhel_mgmt` Ansible collection. With the Redfish modules, you can easily use management automation on bare-metal servers and platform hardware by getting information about the servers or control them through an Out-Of-Band (OOB) controller, using the standard HTTPS transport and JSON format. 

include::modules/con_the-redfish-modules.adoc[leveloffset=+1]

include::modules/ref_redfish-modules-parameters.adoc[leveloffset=+1]

include::modules/proc_using-the-redfish-info-module.adoc[leveloffset=+1]

include::modules/proc_using-the-redfish-command-module.adoc[leveloffset=+1]

include::modules/proc_using-the-redfish-config-module.adoc[leveloffset=+1]


////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-assembly_the-redfish-modules-in-rhel[:context: {parent-context-of-assembly_the-redfish-modules-in-rhel}]
ifndef::parent-context-of-assembly_the-redfish-modules-in-rhel[:!context:]

