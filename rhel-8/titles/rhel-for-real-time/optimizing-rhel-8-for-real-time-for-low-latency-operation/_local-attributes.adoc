// Show the table of contents
:toc:
// The name of the title
:ProjectName: Optimizing RHEL 8 for Real Time for low latency operation
// The subtitle of the title
:Subtitle: Optimizing the RHEL for Real Time kernel on Red Hat Enterprise Linux
// The abstract of the title
:Abstract: Tune your workstations on the RHEL for Real Time kernel to achieve consistently low latency and a predictable response time on latency-sensitive applications. Perform real-time kernel tuning by managing system resources, measuring latency between events, and recording latency for analysis on applications with strict determinism requirements. 
// The name of the title for the purposes of {context}
:ProjectNameID: optimizing-RHEL8-for-real-time-for-low-latency-operation

// The following are not required
:ProjectVersion: 0.1

:RT: Red Hat Enterprise Linux for Real Time
