// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:ha-cluster-title:

//This is how the offset used to be set -- here but not in the include statemenst
// :leveloffset: +1

include::common-content/making-open-source-more-inclusive.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_overview-of-high-availability.adoc[leveloffset=+1]

include::assemblies/assembly_getting-started-with-pacemaker.adoc[leveloffset=+1]

include::assemblies/assembly_pcs-operation.adoc[leveloffset=+1]

include::assemblies/assembly_creating-high-availability-cluster.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-active-passive-http-server-in-a-cluster.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-active-passive-nfs-server-in-a-cluster.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-gfs2-in-a-cluster.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-active-active-samba-in-a-cluster.adoc[leveloffset=+1]

include::assemblies/assembly_getting-started-with-the-pcsd-web-ui.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-fencing.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-cluster-resources.adoc[leveloffset=+1]

include::assemblies/assembly_determining-which-node-a-resource-runs-on.adoc[leveloffset=+1]

include::assemblies/assembly_determining-resource-order.adoc[leveloffset=+1]

include::assemblies/assembly_colocating-cluster-resources.adoc[leveloffset=+1]

include::modules/high-availability/proc_displaying-resource-constraints.adoc[leveloffset=+1]

include::assemblies/assembly_determining-resource-location-with-rules.adoc[leveloffset=+1]

include::assemblies/assembly_managing-cluster-resources.adoc[leveloffset=+1]

include::assemblies/assembly_creating-multinode-resources.adoc[leveloffset=+1]

include::assemblies/assembly_clusternode-management.adoc[leveloffset=+1]

include::assemblies/assembly_cluster-permissions.adoc[leveloffset=+1]

include::assemblies/assembly_resource-monitoring-operations.adoc[leveloffset=+1]

include::assemblies/assembly_controlling-cluster-behavior.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-resources-to-remain-stopped.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-node-placement-strategy.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-virtual-domain-as-a-resource.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-cluster-quorum.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-quorum-devices.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-pacemaker-alert-agents.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-multisite-cluster.adoc[leveloffset=+1]

include::assemblies/assembly_remote-node-management.adoc[leveloffset=+1]

include::assemblies/assembly_cluster-maintenance.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-disaster-recovery.adoc[leveloffset=+1]

include::modules/high-availability/ref_interpreting-resource-exit-codes.adoc[leveloffset=+1]

include::modules/high-availability/ref_ibmz.adoc[leveloffset=+1]

// include::assemblies/assembly_enabling-ports-for-high-availability.adoc[leveloffset=+1]

