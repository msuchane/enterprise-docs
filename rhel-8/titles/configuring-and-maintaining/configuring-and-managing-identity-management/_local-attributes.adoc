// Show the table of contents
:toc:
// The name of the title
:ProjectName: Configuring and managing Identity{nbsp}Management
//Getting started using {IPA}
// The subtitle of the title
:Subtitle: Logging in to IdM and managing services, users, hosts, groups, access control rules, and certificates.
// The abstract of the title
//:Abstract: The abstract is directly in docinfo.xml, because within an attribute, multiple paragraphs are not supported.
// The name of the title for the purposes of {context}
:ProjectNameID: configuring-and-managing-idm

// The following are not required
:ProjectVersion: 0.1
