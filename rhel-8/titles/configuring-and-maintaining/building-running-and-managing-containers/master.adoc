// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:


// define a unique token to check against, so as to let content verify if it's in this book or not
:container-id:


include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_starting-with-containers.adoc[leveloffset=+1]

include::assemblies/assembly_types-of-container-images.adoc[leveloffset=+1]

include::assemblies/assembly_working-with-container-registries.adoc[leveloffset=+1]

include::assemblies/assembly_working-with-container-images.adoc[leveloffset=+1]

include::assemblies/assembly_working-with-containers.adoc[leveloffset=+1]

include::assemblies/assembly_selecting-a-container-runtime.adoc[leveloffset=+1]

include::assemblies/assembly_adding-software-to-a-ubi-container.adoc[leveloffset=+1]

include::assemblies/assembly_signing-container-images.adoc[leveloffset=+1]

include::assemblies/assembly_managing-a-container-network.adoc[leveloffset=+1]

include::assemblies/assembly_working-with-pods.adoc[leveloffset=+1]

include::assemblies/assembly_communicating-among-containers.adoc[leveloffset=+1]

include::assemblies/assembly_setting-container-network-modes.adoc[leveloffset=+1]

include::assemblies/assembly_porting-containers-to-openshift-using-podman.adoc[leveloffset=+1]

include::assemblies/assembly_porting-containers-to-systemd-using-podman.adoc[leveloffset=+1]

include::assemblies/assembly_managing-containers-using-the-ansible-playbook.adoc[leveloffset=+1]

include::assemblies/assembly_managing-container-images-by-using-the-rhel-web-console.adoc[leveloffset=+1]

include::assemblies/assembly_managing-containers-by-using-the-rhel-web-console.adoc[leveloffset=+1]

include::assemblies/assembly_running-skopeo-buildah-and-podman-in-a-container.adoc[leveloffset=+1]

include::assemblies/assembly_building-container-images-with-buildah.adoc[leveloffset=+1]

include::assemblies/assembly_working-with-containers-using-buildah.adoc[leveloffset=+1]

include::assemblies/assembly_monitoring-containers.adoc[leveloffset=+1]

include::assemblies/assembly_creating-and-restoring-container-checkpoints.adoc[leveloffset=+1]

include::assemblies/assembly_using-toolbx-for-development-and-troubleshooting.adoc[leveloffset=+1]

include::assemblies/assembly_using-podman-in-hpc-environment.adoc[leveloffset=+1]

include::assemblies/assembly_running-special-container-images.adoc[leveloffset=+1]

include::assemblies/assembly_using-the-container-tools-api.adoc[leveloffset=+1]
