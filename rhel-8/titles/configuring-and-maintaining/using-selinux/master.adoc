:using-selinux:

// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}



// include::common-content/beta-title.adoc[]

include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_getting-started-with-selinux.adoc[leveloffset=+1]

include::assemblies/assembly_changing-selinux-states-and-modes.adoc[leveloffset=+1]

include::assemblies/assembly_managing-confined-and-unconfined-users.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-selinux-for-applications-and-services-with-non-standard-configurations.adoc[leveloffset=+1]

include::assemblies/assembly_troubleshooting-problems-related-to-selinux.adoc[leveloffset=+1]

include::assemblies/assembly_using-multi-level-security-mls.adoc[leveloffset=+1]

include::assemblies/assembly_using-multi-category-security-mcs-for-data-confidentiality.adoc[leveloffset=+1]

include::assemblies/assembly_writing-a-custom-selinux-policy.adoc[leveloffset=+1]

include::assemblies/assembly_creating-selinux-policies-for-containers.adoc[leveloffset=+1]

include::assemblies/assembly_deploying-the-same-selinux-configuration-on-multiple-systems.adoc[leveloffset=+1]

