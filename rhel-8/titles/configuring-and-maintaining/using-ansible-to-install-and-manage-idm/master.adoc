include::common-content/_attributes.adoc[]

include::_local-attributes.adoc[]

= {ProjectName}

:context: {ProjectNameID}
:u-a-in-idm:

// invisible text for test commit

include::common-content/making-open-source-more-inclusive-idm.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]


include::modules/identity-management/ref_ansible-terminology.adoc[leveloffset=+1]

include::assemblies/assembly_installing-an-Identity-Management-server-using-an-Ansible-playbook.adoc[leveloffset=+1]

include::assemblies/assembly_installing-an-Identity-Management-replica-using-an-Ansible-playbook.adoc[leveloffset=+1]

include::assemblies/assembly_installing-an-Identity-Management-client-using-an-Ansible-playbook.adoc[leveloffset=+1]


:context: {ProjectNameID}

include::assemblies/assembly_preparing-your-environment-for-managing-idm-using-ansible-playbooks.adoc[leveloffset=+1]


//modules/identity-management/proc_preparing-your-environment-for-managing-idm-using-ansible-playbooks.adoc[leveloffset=+1]


//include::modules/identity-management/proc_executing-ansible-freeipa-modules-remotely.adoc[leveloffset=+1]


include::assemblies/assembly_configuring-global-idm-settings-using-ansible-playbooks.adoc[leveloffset=+1]


include::assemblies/assembly_managing-user-accounts-using-Ansible-playbooks.adoc[leveloffset=+1]


include::assemblies/assembly_managing-user-groups-using-ansible-playbooks.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-to-automate-group-membership-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-playbooks-to-manage-self-service-rules-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_delegating-permissions-to-user-groups-to-manage-users-using-ansible-playbooks.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-playbooks-to-manage-role-based-access-control-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-playbooks-to-manage-rbac-privileges-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-playbooks-to-manage-rbac-permissions-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-to-manage-the-replication-topology-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_managing-idm-servers-by-using-ansible.adoc[leveloffset=+1]


include::assemblies/assembly_managing-hosts-using-Ansible-playbooks.adoc[leveloffset=+1]


include::assemblies/assembly_managing-host-groups-using-Ansible-playbooks.adoc[leveloffset=+1]


include::assemblies/assembly_defining-idm-password-policies.adoc[leveloffset=+1]


include::assemblies/assembly_granting-sudo-access-to-an-IdM-user-on-an-IdM-client.adoc[leveloffset=+1]


include::assemblies/assembly_ensuring-the-presence-of-host-based-access-control-rules-in-idm-using-Ansible-playbooks.adoc[leveloffset=+1]


include::assemblies/assembly_managing-idm-certificates-using-ansible.adoc[leveloffset=+1]


include::assemblies/assembly_vaults-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-to-manage-idm-user-vaults-storing-and-retrieving-secrets.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-to-manage-idm-service-vaults-storing-and-retrieving-secrets.adoc[leveloffset=+1]


include::assemblies/assembly_ensuring-the-presence-and-absence-of-services-in-idm-using-ansible.adoc[leveloffset=+1]


include::assemblies/assembly_managing-global-dns-configuration-in-idm-using-ansible-playbooks.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-playbooks-to-manage-idm-dns-zones.adoc[leveloffset=+1]


//include::assemblies/assembly_managing-dns-locations-in-idm.adoc[leveloffset=+1]

// uncommented the following line on May 12 after the Stage version of the Conf&Mngng guide would not build:
// [31m[1mUnknown ID or title "using-ansible-to-ensure-an-idm-location-is-present_using-ansible-to-manage-dns-locations-in-idm", used as an internal cross reference[0m
include::assemblies/assembly_using-ansible-to-manage-dns-locations-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_managing-dns-forwarding-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-to-manage-dns-records-in-idm.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-to-automount-nfs-shares-for-idm-users.adoc[leveloffset=+1]


include::assemblies/assembly_using-ansible-to-integrate-idm-with-nis-domains-and-netgroups.adoc[leveloffset=+1]


include::modules/identity-management/proc_using-ansible-to-configure-hbac-and-sudo-rules-in-idm.adoc[leveloffset=+1]

include::assemblies/assembly_using-ansible-to-delegate-authentication-for-idm-users-to-external-identity-providers.adoc[leveloffset=+1]

include::assemblies/assembly_integrating-rhel-systems-into-ad-directly-with-ansible-using-rhel-system-roles.adoc[leveloffset=+1]
