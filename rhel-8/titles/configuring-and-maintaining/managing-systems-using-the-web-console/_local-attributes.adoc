// Show the table of contents
:toc:

:ProjectName: Managing systems using the RHEL 8 web console

:Subtitle: Server management with a graphical web-based interface 

:Abstract: The RHEL web console is a web-based graphical interface, which is based on the upstream Cockpit project. By using it, you can perform system administration tasks, such as inspecting and controlling systemd services, managing storage, configuring networks, analyzing network issues, and inspecting logs.

// The name of the title for the purposes of {context}
:ProjectNameID: system-management-using-the-RHEL-8-web-console

// The following are not required
:ProjectVersion: 0.1

:cockpit-title:
