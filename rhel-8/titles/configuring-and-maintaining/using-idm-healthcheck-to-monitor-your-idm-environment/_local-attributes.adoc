// Show the table of contents
:toc:
// The name of the title
:ProjectName: Using IdM Healthcheck to monitor your IdM environment
// The subtitle of the title
:Subtitle: Performing status and health checks
// The abstract of the title
//:Abstract: The abstract is directly in docinfo.xml, because within an attribute, markups are not supported.
// The name of the title for the purposes of {context}
:ProjectNameID: using-idm-healthcheck-to-monitor-your-idm-environment

// The following are not required
:ProjectVersion: 0.1
