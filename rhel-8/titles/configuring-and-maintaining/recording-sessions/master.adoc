// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
//:internal:

include::common-content/making-open-source-more-inclusive.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_getting-started-with-session-recording.adoc[leveloffset=+1]

include::assemblies/assembly_deploying-session-recording.adoc[leveloffset=+1]

include::assemblies/assembly_playing-back-a-recorded-session.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-a-system-for-session-recording-using-the-tlog-rhel-system-roles.adoc[leveloffset=+1]

