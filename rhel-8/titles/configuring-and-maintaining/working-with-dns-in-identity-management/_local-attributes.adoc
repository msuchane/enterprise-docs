// Show the table of contents
:toc:
// The name of the title
:ProjectName: Working with DNS in Identity{nbsp}Management
//Getting started using {IPA}
// The subtitle of the title
:Subtitle: Managing the IdM-integrated DNS service
// The abstract of the title
:Abstract: DNS is an important component in a {RH} {IPA} (IdM) domain. For example, clients use DNS to locate services and identify servers in the same site. You can manage records, zones, locations, and forwarding in the DNS server that is integrated in IdM by using the command line, the IdM Web UI, and Ansible Playbooks.
// The name of the title for the purposes of {context}
:ProjectNameID: working-with-dns-in-identity-management

// The following are not required
:ProjectVersion: 0.1
