// Show the table of contents
:toc:
// The name of the title
:ProjectName: Preparing for disaster recovery with Identity{nbsp}Management
// The subtitle of the title
:Subtitle: Mitigating the effects of server and data loss scenarios in IdM environments
// The abstract of the title
:Abstract: Server and data loss scenarios, for example due to a hardware failure, are the highest risks in IT environments. In a {RH} {IPA} (IdM) topology, you can configure replication with other servers, use virtual machine (VM) snapshots, and IdM backups to mitigate the effects of these situations.
// The name of the title for the purposes of {context}
:ProjectNameID: preparing-for-disaster-recovery

// The following are not required
:ProjectVersion: 0.1
