:_mod-docs-content-type: ASSEMBLY
:parent-context-of-granting-sudo-access-to-an-IdM-user-on-an-IdM-client: {context}

[id="granting-sudo-access-to-an-IdM-user-on-an-IdM-client_{context}"]
= Granting sudo access to an IdM user on an IdM client

:context: granting-sudo-access-to-an-IdM-user-on-an-IdM-client

[role="_abstract"]
Learn more about granting `sudo` access to users in Identity Management.

include::modules/identity-management/con_sudo-access-on-an-IdM-client.adoc[leveloffset=+1]
//include::modules/identity-management/proc_making-central-IdM-sudo-rules-take-precedence-over-local-sudo-rules-on-an-IdM-client.adoc[leveloffset=+1]

include::modules/identity-management/proc_granting-sudo-access-to-an-idm-user-on-an-idm-client-using-the-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_granting-sudo-access-to-an-ad-user-on-an-idm-client-using-the-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-IdM-Web-UI-to-grant-sudo-access-to-an-IdM-user-on-an-IdM-client.adoc[leveloffset=+1]

include::modules/identity-management/proc_creating-a-sudo-rule-on-the-cli-that-runs-a-command-as-a-service-account-on-an-idm-client.adoc[leveloffset=+1]

include::modules/identity-management/proc_creating-a-sudo-rule-in-the-idm-webui-that-runs-a-command-as-a-service-account-on-an-idm-client.adoc[leveloffset=+1]

include::modules/identity-management/proc_enabling-gssapi-authentication-for-sudo-on-an-idm-client.adoc[leveloffset=+1]

include::modules/identity-management/proc_enabling-gssapi-authentication-and-enforcing-kerberos-authentication-indicators-for-sudo-on-an-idm-client.adoc[leveloffset=+1]

include::modules/identity-management/ref_sssd-options-controlling-gssapi-authentication-for-pam-services.adoc[leveloffset=+1]

include::modules/identity-management/proc_troubleshooting-gssapi-authentication-for-sudo.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-an-Ansible-playbook-to-ensure-sudo-access-for-an-IdM-user-on-an-IdM-client.adoc[leveloffset=+1]

:context: {parent-context-of-granting-sudo-access-to-an-IdM-user-on-an-IdM-client}
