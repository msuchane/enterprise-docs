:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly-troubleshooting-gfs2: {context}

[id='assembly_troubleshooting-gfs2-{context}']
= Diagnosing and correcting problems with GFS2 file systems
:context: troubleshooting-gfs2

[role="_abstract"]
The following procedures describe some common GFS2 issues and provide information on how to address them.


include::modules/high-availability/ref_gfs2-filesystem-unavailable.adoc[leveloffset=+1]

include::modules/high-availability/ref_gfs2-filesystem-hangs-one-node.adoc[leveloffset=+1]

include::modules/high-availability/ref_gfs2-filesystem-hangs-all-nodes.adoc[leveloffset=+1]

include::modules/high-availability/ref_gfs2-nomount-new-cluster-node.adoc[leveloffset=+1]

include::modules/high-availability/ref_gfs2-used-space-empty-filesystem.adoc[leveloffset=+1]

include::modules/high-availability/proc_gathering-gfs2-data.adoc[leveloffset=+1]


:context: {parent-context-of-assembly-troubleshooting-gfs2}


