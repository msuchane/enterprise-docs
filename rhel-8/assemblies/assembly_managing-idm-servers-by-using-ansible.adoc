:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-managing-idm-servers-by-using-ansible: {context}]

[id="managing-idm-servers-by-using-ansible_{context}"]
= Managing IdM servers by using Ansible
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: managing-idm-servers-by-using-ansible

[role="_abstract"]
You can use `Red Hat Ansible Engine` to manage the servers in your {IPA} (IdM) topology. You can use the `server` module in the `ansible-freeipa` package to check the presence or absence of a server in the IdM topology. You can also hide any replica or make a replica visible.

The section contains the following topics:

* xref:checking-that-an-idm-server-is-present-by-using-ansible_{context}[Checking that an IdM server is present by using Ansible]
* xref:ensuring-that-an-idm-server-is-absent-from-an-idm-topology-by-using-ansible_{context}[Ensuring that an IdM server is absent from an IdM topology by using Ansible]
* xref:ensuring-the-absence-of-an-idm-server-despite-hosting-a-last-idm-server-role_{context}[Ensuring the absence of an IdM server despite hosting a last IdM server role]
* xref:ensuring-that-an-idm-server-is-absent-but-not-necessarily-disconnected-from-other-idm-servers_{context}[Ensuring that an IdM server is absent but not necessarily disconnected from other IdM servers]
* xref:ensuring-that-an-existing-idm-server-is-hidden-using-an-ansible-playbook_{context}[Ensuring that an existing IdM server is hidden using an Ansible playbook]
* xref:ensuring-that-an-existing-idm-server-is-visible-using-an-ansible-playbook_{context}[Ensuring that an existing IdM server is visible using an Ansible playbook]
* xref:ensuring-that-an-existing-idm-server-has-an-idm-dns-location-assigned_{context}[Ensuring that an existing IdM server has an IdM DNS location assigned]
* xref:ensuring-that-an-existing-idm-server-has-no-idm-dns-location-assigned_{context}[Ensuring that an existing IdM server has no IdM DNS location assigned]


include::modules/identity-management/proc_checking-that-an-idm-server-is-present-by-using-ansible.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-that-an-idm-server-is-absent-from-an-idm-topology-by-using-ansible.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-the-absence-of-an-idm-server-despite-hosting-a-last-idm-server-role.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-that-an-idm-server-is-absent-but-not-necessarily-disconnected-from-other-idm-servers.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-that-an-existing-idm-server-is-hidden-using-an-ansible-playbook.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-that-an-existing-idm-server-is-visible-using-an-ansible-playbook.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-that-an-existing-idm-server-has-an-idm-dns-location-assigned.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-that-an-existing-idm-server-has-no-idm-dns-location-assigned.adoc[leveloffset=+1]


ifdef::parent-context-of-managing-idm-servers-by-using-ansible[:context: {parent-context-of-managing-idm-servers-by-using-ansible}]
ifndef::parent-context-of-managing-idm-servers-by-using-ansible[:!context:]
