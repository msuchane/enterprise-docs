ifdef::context[:parent-context-of-assembly_tracking-client-requests-using-the-log-analyzer-tool: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_tracking-client-requests-using-the-log-analyzer-tool"]
endif::[]
ifdef::context[]
[id="assembly_tracking-client-requests-using-the-log-analyzer-tool_{context}"]
endif::[]
= Tracking client requests using the log analyzer tool

:context: assembly_tracking-client-requests-using-the-log-analyzer-tool

The System Security Services Daemon (SSSD) includes a log parsing tool that can be used to track requests from start to finish across log files from multiple SSSD components.

include::modules/identity-management/con_how-the-log-analyzer-tool-works.adoc[leveloffset=+1]

include::modules/identity-management/proc_running-the-log-analyzer-tool.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_tracking-client-requests-using-the-log-analyzer-tool[:context: {parent-context-of-assembly_tracking-client-requests-using-the-log-analyzer-tool}]
ifndef::parent-context-of-assembly_tracking-client-requests-using-the-log-analyzer-tool[:!context:]
