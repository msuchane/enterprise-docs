:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-using-the-kdc-proxy-in-idm: {context}]
[id="using-the-kdc-proxy-in-idm_{context}"]
= Using the KDC Proxy in IdM
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: using-the-kdc-proxy-in-idm

[role="_abstract"]
Some administrators might choose to make the default Kerberos ports inaccessible in their deployment. To allow users, hosts, and services to obtain Kerberos credentials, you can use the `HTTPS` service as a proxy that communicates with Kerberos via the `HTTPS` port 443.

In {IPA} (IdM), the *Kerberos Key Distribution Center Proxy* (KKDCP) provides this functionality.

On an IdM server, KKDCP is enabled by default and available at `https://__server.idm.example.com__/KdcProxy`. On an IdM client, you must change its Kerberos configuration to access the KKDCP.

include::modules/identity-management/proc_configuring-an-idm-client-to-use-kkdcp.adoc[leveloffset=+1]

include::modules/identity-management/proc_verifying-that-kkdcp-is-enabled-on-an-idm-server.adoc[leveloffset=+1]

include::modules/identity-management/proc_disabling-kkdcp-on-an-idm-server.adoc[leveloffset=+1]

include::modules/identity-management/proc_re-enabling-kkdcp-on-an-idm-server.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-the-kkdcp-server-i.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-the-kkdcp-server-ii.adoc[leveloffset=+1]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-using-the-kdc-proxy-in-idm[:context: {parent-context-of-using-the-kdc-proxy-in-idm}]
ifndef::parent-context-of-using-the-kdc-proxy-in-idm[:!context:]
