:_mod-docs-content-type: ASSEMBLY
:parent-context-of-connecting-to-asmi: {context}

[id="connecting-to-asmi_{context}"]
= Connecting to Advanced System Management Interface

You can connect to Advanced System Management Interface (ASMI) using DHCP or a static IP address.


include::modules/installer/proc_connecting-to-asmi-with-dhcp.adoc[leveloffset=+1]

include::modules/installer/proc_connecting-to-asmi-with-static-ip-address.adoc[leveloffset=+1]


:context: {parent-context-of-connecting-to-asmi}
