
ifdef::context[:parent-context-of-assembly_support-for-boolean-dependencies: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_support-for-boolean-dependencies"]
endif::[]
ifdef::context[]
[id="assembly_support-for-boolean-dependencies_{context}"]
endif::[]
= Support for Boolean dependencies

:context: assembly_support-for-boolean-dependencies

Starting with version 4.13, RPM is able to process boolean expressions in the following dependencies:

* `Requires`
* `Recommends`
* `Suggests`
* `Supplements`
* `Enhances`
* `Conflicts`

The following sections describe xref:con_boolean-dependencies-syntax_assembly_support-for-boolean-dependencies[boolean dependencies syntax], provides a list of xref:ref_boolean-operators_assembly_support-for-boolean-dependencies[boolean operators], and explains xref:con_nesting_assembly_support-for-boolean-dependencies[boolean dependencies nesting] as well as xref:con_semantics_assembly_support-for-boolean-dependencies[boolean dependencies semantics].



include::modules/core-services/con_boolean-dependencies-syntax.adoc[leveloffset=+1]

include::modules/core-services/ref_boolean-operators.adoc[leveloffset=+1]

include::modules/core-services/con_nesting.adoc[leveloffset=+1]

include::modules/core-services/con_semantics.adoc[leveloffset=+1]

include::modules/core-services/con_understanding-the-output-of-the-if-operator.adoc[leveloffset=+1]




ifdef::parent-context-of-assembly_support-for-boolean-dependencies[:context: {parent-context-of-assembly_support-for-boolean-dependencies}]
ifndef::parent-context-of-assembly_support-for-boolean-dependencies[:!context:]
