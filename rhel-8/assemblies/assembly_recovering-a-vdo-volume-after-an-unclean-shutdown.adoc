:_mod-docs-content-type: ASSEMBLY
:parent-context-of-recovering-a-vdo-volume-after-an-unclean-shutdown: {context}

[id="recovering-a-vdo-volume-after-an-unclean-shutdown_{context}"]
= Recovering a VDO volume after an unclean shutdown

:context: recovering-a-vdo-volume-after-an-unclean-shutdown

ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

You can recover a VDO volume after an unclean shutdown to enable it to continue operating. The task is mostly automated. Additionally, you can clean up after a VDO volume was unsuccessfully created because of a failure in the process.

// .Prerequisites
// 
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

include::modules/filesystems-and-storage/con_vdo-write-modes.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_vdo-volume-recovery.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/ref_vdo-operating-modes.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_recovering-a-vdo-volume-online.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_forcing-an-offline-rebuild-of-a-vdo-volume-metadata.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_removing-an-unsuccessfully-created-vdo-volume.adoc[leveloffset=+1]


// [id='related-information-{context}']
// == Related information
// 
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

:context: {parent-context-of-recovering-a-vdo-volume-after-an-unclean-shutdown}
