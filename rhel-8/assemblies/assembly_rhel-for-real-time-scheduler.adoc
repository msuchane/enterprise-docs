

ifdef::context[:parent-context-of-assembly_rhel-for-real-time-scheduler: {context}]


:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_rhel-for-real-time-scheduler"]
endif::[]
ifdef::context[]
[id="assembly_rhel-for-real-time-scheduler_{context}"]
endif::[]
= RHEL for Real Time scheduler


:context: assembly_rhel-for-real-time-scheduler



[role="_abstract"]
RHEL for Real Time uses the command line utilities help you to configure and monitor process configurations.


include::modules/rt-kernel/con_chrt-utility-for-setting-the-scheduler.adoc[leveloffset=+1]

include::modules/rt-kernel/con_preemptive-scheduling.adoc[leveloffset=+1]

include::modules/rt-kernel/ref_library-functions-for-scheduler-priority.adoc[leveloffset=+1]






ifdef::parent-context-of-assembly_rhel-for-real-time-scheduler[:context: {parent-context-of-assembly_rhel-for-real-time-scheduler}]
ifndef::parent-context-of-assembly_rhel-for-real-time-scheduler[:!context:]
