ifdef::context[:parent-context-of-monitoring-stratis-file-systems: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="monitoring-stratis-file-systems"]
endif::[]
ifdef::context[]
[id="monitoring-stratis-file-systems_{context}"]
endif::[]
= Monitoring Stratis file systems

:context: monitoring-stratis-file-systems

As a Stratis user, you can view information about Stratis volumes on your system to monitor their state and free space.

ifeval::[{ProductNumber}==8]
:TechPreviewName: Stratis
include::common-content/snip_techpreview.adoc[]
:TechPreviewName!: Stratis
endif::[]

include::modules/filesystems-and-storage/con_stratis-sizes-reported-by-different-utilities.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_displaying-information-about-stratis-volumes.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* link:https://stratis-storage.github.io/[The _Stratis Storage_ website]

ifdef::parent-context-of-monitoring-stratis-file-systems[:context: {parent-context-of-monitoring-stratis-file-systems}]
ifndef::parent-context-of-monitoring-stratis-file-systems[:!context:]
