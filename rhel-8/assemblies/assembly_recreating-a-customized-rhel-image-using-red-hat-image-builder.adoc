ifdef::context[:parent-context-of-assembly_recreating-a-customized-rhel-image-using-red-hat-image-builder: {context}]


:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_recreating-a-customized-rhel-image-using-red-hat-image-builder"]
endif::[]
ifdef::context[]
[id="assembly_recreating-a-customized-rhel-image-using-red-hat-image-builder_{context}"]
endif::[]
= Creating a new image from an existing customized build by using Insights image builder

:context: assembly_recreating-a-customized-rhel-image-using-red-hat-image-builder

You can recreate an existing customized RHEL image by using Insights image builder. It recreates the exact image you want to recreate, but the new image is recreated with a different UUID. Additionally, the new image fetches the package updates and refreshes the content with those updates. You can customize this new image with additional customizations according to your requirements.

include::modules/image-builder-cloud/proc_creating-a-new-image-from-an-existing-build.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_recreating-a-customized-rhel-image-using-red-hat-image-builder[:context: {parent-context-of-assembly_recreating-a-customized-rhel-image-using-red-hat-image-builder}]
ifndef::parent-context-of-assembly_recreating-a-customized-rhel-image-using-red-hat-image-builder[:!context:]

