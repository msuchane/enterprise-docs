:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-changing-a-subscripton-service: {context}]

[id="changing-a-subscripton-service_{context}"]
[appendix]
= Changing a subscription service

:context: changing-a-subscripton-service

[role="_abstract"]
To manage the subscriptions, you can register a RHEL system with either Red Hat Subscription Management Server or Red Hat Satellite Server.
If required, you can change the subscription service at a later point.
To change the subscription service under which you are registered, unregister the system from the current service and then register it with a new service.

This section contains information about how to unregister your RHEL system from the Red Hat Subscription Management Server and Red Hat Satellite Server.

.Prerequisites

You have registered your system with any one of the following:

* Red Hat Subscription Management Server
* Red Hat Satellite Server version 6.11 

[NOTE]
====
To receive the system updates, register your system with either of the management server.
====

include::assembly_unregistering-from-subscription-management-server.adoc[leveloffset=+1]

include::modules/installer/proc_unregistering-from-satellite-server.adoc[leveloffset=+1]


ifdef::parent-context-of-changing-a-subscripton-service[:context: {parent-context-of-changing-a-subscripton-service}]
ifndef::parent-context-of-changing-a-subscripton-service[:!context:]
