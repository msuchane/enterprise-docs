:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly-analyzing-gfs2-with-pcp: {context}

[id='assembly_analyzing-gfs2-with-pcp-{context}']
= Monitoring and analyzing GFS2 file systems using Performance Co-Pilot (PCP)
:context: analyzing-gfs2-with-pcp

[role="_abstract"]
Performance Co-Pilot (PCP) can help with monitoring and analyzing GFS2 file systems. Monitoring of GFS2 file systems in PCP is provided by the GFS2 PMDA module in Red Hat Enterprise Linux which is available through the `pcp-pmda-gfs2` package.

The GFS2 PMDA provides a number of metrics given by the GFS2 statistics provided in the `debugfs` subsystem. When installed, the PMDA exposes values given in the `glocks`, `glstats`, and `sbstats` files. These report sets of statistics on
each mounted GFS2 filesystem. The PMDA also makes use of the GFS2 kernel tracepoints exposed by the Kernel Function Tracer (`ftrace`).

include::modules/high-availability/proc_installing-gfs2-pdma.adoc[leveloffset=+1]

include::modules/high-availability/proc_examining-number-of-glocks.adoc[leveloffset=+1]

include::modules/high-availability/ref_available-gfs2-PCP-metrics.adoc[leveloffset=+1]

include::modules/high-availability/proc_installing-minimal-PCP-setup.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_gfs2_file_systems/con_gfs2-tracepoints-configuring-gfs2-file-systems[GFS2 tracepoints and the glock debugfs interface].
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/monitoring-performance-with-performance-co-pilot_monitoring-and-managing-system-status-and-performance[Monitoring performance with Performance Co-Pilot]
* link:https://access.redhat.com/articles/1145953[Index of Performance Co-Pilot(PCP) articles, solutions, tutorials and white papers]

:context: {parent-context-of-assembly-analyzing-gfs2-with-pcp}
