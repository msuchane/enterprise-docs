:_mod-docs-content-type: ASSEMBLY
:parent-context-of-managing-redundant-arrays-of-independent-disks-in-the-web-console: {context}


[id="managing-redundant-arrays-of-independent-disks-in-the-web-console_{context}"]
= Managing RAID in the web console

:context: managing-redundant-arrays-of-independent-disks-in-the-web-console


[role="_abstract"]
Redundant Arrays of Independent Disks (RAID) represents a way how to arrange more disks into one storage for performance and redundancy goals.

RAID uses the following data distribution strategies:

* Mirroring -- data are copied to two different locations. If one disk fails, you have a copy and your data is not lost.
* Striping -- data are evenly distributed among disks.

Level of protection depends on the RAID level.

The {ProductShortName} web console supports the following RAID levels:

* RAID 0 (Stripe)
* RAID 1 (Mirror)
* RAID 4 (Dedicated parity)
* RAID 5 (Distributed parity)
* RAID 6 (Double Distributed Parity)
* RAID 10 (Stripe of Mirrors)

Before you can use disks in RAID, you must:

* Create a RAID.
* Format it with file system.
* Mount the RAID to the system.

.Prerequisites

* The {ProductShortName} {ProductNumber} web console is installed and accessible. For details, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].

* The `cockpit-storaged` package is installed on your system.

include::modules/cockpit/proc_creating-raid-in-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/proc_formatting-raid-in-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/proc_creating-a-partition-table-on-raid-using-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/proc_creating-partitions-on-raid-using-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/proc_creating-volume-group-on-top-of-raid-using-the-web-console.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* To learn more about soft corruption and how you can protect your data when configuring a RAID LV, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/configuring-raid-logical-volumes_configuring-and-managing-logical-volumes#creating-a-raid-lv-with-dm-integrity_configuring-raid-logical-volumes[Creating a RAID LV with DM integrity].




// The following line is necessary to allow assemblies be included i  n other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-managing-redundant-arrays-of-independent-disks-in-the-web-console}
