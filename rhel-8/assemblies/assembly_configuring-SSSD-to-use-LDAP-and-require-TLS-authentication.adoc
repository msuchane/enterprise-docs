:_mod-docs-content-type: ASSEMBLY
:parent-context-of-configuring-SSSD-to-use-LDAP-and-require-TLS-authentication: {context}

[id="configuring-SSSD-to-use-LDAP-and-require-TLS-authentication_{context}"]
= Configuring SSSD to use LDAP and require TLS authentication

:context: configuring-SSSD-to-use-LDAP-and-require-TLS-authentication

[role="_abstract"]
The System Security Services Daemon (SSSD) is a daemon that manages identity data retrieval and authentication on a {RHEL} host. A system administrator can configure the host to use a standalone LDAP server as the user account database. The administrator can also specify the requirement that the connection with the LDAP server must be encrypted with a TLS certificate.

[NOTE]
====
The SSSD configuration option to enforce TLS,  `ldap_id_use_start_tls`, defaults to `false`. When using `ldap://` without TLS for identity lookups, it can pose a risk for an attack vector, namely a man-in-the-middle (MITM) attack which could allow you to impersonate a user by altering, for example, the UID or GID of an object returned in an LDAP search.

Ensure that your setup operates in a trusted environment and decide if it is safe to use unencrypted communication for `id_provider = ldap`. Note  `id_provider = ad` and `id_provider = ipa` are not affected as they use encrypted connections protected by SASL and GSSAPI.

If it is not safe to use unencrypted communication, you should enforce TLS by setting the `ldap_id_use_start_tls` option to  `true` in the `/etc/sssd/sssd.conf` file.
====

//You can configure the SSSD on your host to use Lightweight Directory Access Protocol (LDAP)and require Transport Layer Security (TLS) authentication.

////
The System Security Services Daemon (SSSD) is a daemon that manages identity data retrieval and authentication on a RHEL 8 host. A system administrator can configure the host to use a standalone LDAP server as the user account database. The administrator can also specify the requirement that the connection with the LDAP server must be encrypted with a TLS certificate.

Please note that the authentication and authorization of the LDAP objects are not addressed in this chapter.

If you want to use a comprehensive identity management solution for

* retrieving the data from the central database,
* authentication,
* authorization,

consider deploying Red Hat Identity Management. For details, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8_test/html-single/planning_identity_management/index#IdM_terminology_overview-of-planning-idm-and-access-control[Planning Identity Management].
////

include::modules/identity-management/con_OpenLDAP-client-using-SSSD-to-retrieve-data-from-LDAP-in-an-encrypted-way.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-SSSD-to-use-LDAP-and-require-TLS-authentication.adoc[leveloffset=+1]

:context: {parent-context-of-configuring-SSSD-to-use-LDAP-and-require-TLS-authentication}
