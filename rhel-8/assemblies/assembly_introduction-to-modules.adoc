:_mod-docs-content-type: ASSEMBLY
:parent-context-of-introduction-to-modules: {context}

[id="introduction-to-modules_{context}"]
= Introduction to modules

:context: introduction-to-modules

Besides individual RPM packages, the AppStream repository contains modules. A module is a set of RPM packages that represent a component and are usually installed together. A typical module contains packages with an application, packages with the application-specific dependency libraries, packages with documentation for the application, and packages with helper utilities.

In the following sections, learn features for organization and handling of content within modules:

* xref:module-streams_{context}[Module streams] - organization of content by version.
* xref:module-profiles_{context}[Module profiles] - organization of content by purpose.


include::modules/appstream/con_module-streams.adoc[leveloffset=+1]

include::modules/appstream/con_module-profiles.adoc[leveloffset=+1]


:context: {parent-context-of-introduction-to-modules}
