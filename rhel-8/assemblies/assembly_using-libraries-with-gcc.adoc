:_mod-docs-content-type: ASSEMBLY
:parent-context-of-using-libraries-with-gcc: {context}
[id="using-libraries-with-gcc_{context}"]
= Using Libraries with GCC
:context: using-libraries-with-gcc

ifdef::user-stories[__As a developer, I want to use someone else's library in my program__]

Learn about using libraries in code.


include::modules/platform-tools/con_library-naming-conventions.adoc[leveloffset=+1]

include::modules/platform-tools/con_static-and-dynamic-linking.adoc[leveloffset=+1]

include::modules/platform-tools/con_using-a-library-with-gcc.adoc[leveloffset=+1]

include::modules/platform-tools/con_using-a-static-library-with-gcc.adoc[leveloffset=+1]

include::modules/platform-tools/con_using-a-dynamic-library-with-gcc.adoc[leveloffset=+1]

include::modules/platform-tools/con_using-both-static-and-dynamic-libraries-with-gcc.adoc[leveloffset=+1]


:context: {parent-context-of-using-libraries-with-gcc}

