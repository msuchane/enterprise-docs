:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_installing-and-using-python: {context}]

ifndef::context[]
[id="assembly_installing-and-using-python"]
endif::[]
ifdef::context[]
[id="assembly_installing-and-using-python_{context}"]
endif::[]
= Installing and using Python

:context: assembly_installing-and-using-python

[role="_abstract"]
In {RHEL} 8, Python 3 is distributed in versions 3.6, 3.8, and 3.9, provided by the [package]`python36`, [package]`python38`, and [package]`python39` modules, and the [package]`python3.11` and [package]`python3.12` package suites in the AppStream repository.

[WARNING]
====
Using the unversioned [command]`python` command to install or run Python does not work by default due to ambiguity. Always specify the version of Python, or configure the system default version by using the [command]`alternatives` command.
====


include::modules/core-services/proc_installing-python-3.adoc[leveloffset=+1]

include::modules/core-services/proc_installing-additional-python-3-packages.adoc[leveloffset=+1]

include::modules/core-services/proc_installing-additional-python-3-tools-for-developers.adoc[leveloffset=+1]

include::modules/core-services/proc_installing-python-2.adoc[leveloffset=+1]

include::modules/core-services/ref_migrating-from-python-2-to-python-3.adoc[leveloffset=+1]

include::modules/core-services/proc_using-python.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_installing-and-using-python[:context: {parent-context-of-assembly_installing-and-using-python}]
ifndef::parent-context-of-assembly_installing-and-using-python[:!context:]
