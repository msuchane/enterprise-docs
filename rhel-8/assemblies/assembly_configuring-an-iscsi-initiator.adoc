:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-configuring-an-iscsi-initiator: {context}]

ifndef::context[]
[id="configuring-an-iscsi-initiator"]
endif::[]
ifdef::context[]
[id="configuring-an-iscsi-initiator_{context}"]
endif::[]
= Configuring an iSCSI initiator

:context: configuring-an-iscsi-initiator

[role="_abstract"]
An iSCSI initiator forms a session to connect to the iSCSI target. By default, an iSCSI service is lazily started and the service starts after running the `iscsiadm` command. If root is not on an iSCSI device or there are no nodes marked with `node.startup = automatic` then the iSCSI service will not start until an `iscsiadm` command is executed that requires `iscsid` or the `iscsi` kernel modules to be started.

Execute the `systemctl start iscsid.service` command as root to force the `iscsid` daemon to run and iSCSI kernel modules to load.

include::modules/filesystems-and-storage/proc_creating-an-iscsi-initiator.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_setting-up-the-challenge-handshake-authentication-protocol-for-the-initiator.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_monitoring-an-iscsi-session-using-the-iscsiadm-utility.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_dm-multipath-overrides-of-the-device-timeout.adoc[leveloffset=+1]

ifdef::parent-context-of-configuring-an-iscsi-initiator[:context: {parent-context-of-configuring-an-iscsi-initiator}]
ifndef::parent-context-of-configuring-an-iscsi-initiator[:!context:]
