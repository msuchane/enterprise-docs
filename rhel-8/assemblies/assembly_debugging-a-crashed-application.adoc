:_mod-docs-content-type: ASSEMBLY
:parent-context-of-debugging-a-crashed-application: {context}
[id="debugging-a-crashed-application_{context}"]
= Debugging a Crashed Application
:context: debugging-a-crashed-application

// assembly under debugging part
// application which can not be tested live

ifdef::user-stories[__I want to find out what breaks in some application on some other computer__]

Sometimes, it is not possible to debug an application directly. In these situations, you can collect information about the application at the moment of its termination and analyze it afterwards.

////
use ABRT to catch, sosreport to know what are the exact versions ("sos")
sosreport = creates an archive with system files (config etc.)
directory sosreport/sos_commands/rpm = version data in plain text
gotcha: ABRT on RHEL sends to RH only! upstream ours msuchy
demonstrate on glibc (universal dependency)
////

include::modules/platform-tools/con_core-dumps.adoc[leveloffset=+1]

include::modules/platform-tools/proc_recording-application-crashes-with-core-dumps.adoc[leveloffset=+1]

include::modules/platform-tools/proc_inspecting-application-crash-states-with-core-dumps.adoc[leveloffset=+1]

include::modules/platform-tools/proc_creating-and-accessing-a-core-dump-with-coredumpctl.adoc[leveloffset=+1]

include::modules/platform-tools/proc_dumping-process-memory-with-gcore.adoc[leveloffset=+1]

include::modules/platform-tools/proc_dumping-protected-process-memory-with-gdb.adoc[leveloffset=+1]

////
== Reproducing an Application Crash

ifdef::user-stories[__I want to know how to reproduce a crash situation or increase chance of catching it__]

general advice about crash reproducibility

core dump + sos || reproducer

TODO what belongs here?
////

:context: {parent-context-of-debugging-a-crashed-application}
