:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Retains the context of the parent assembly if this assembly is nested within another assembly.
// For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
// See also the complementary step on the last line of this file.
ifdef::context[:parent-context-of-enabling-the-compose-key: {context}]

// Base the file name and the ID on the assembly title. For example:
// * file name: my-assembly-a.adoc
// * ID: [id="my-assembly-a"]
// * Title: = My assembly A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
// If the assembly is reused in other assemblies in a guide, include {context} in the ID: [id="a-collection-of-modules-{context}"].
[id="enabling-the-compose-key_{context}"]
= Enabling the Compose key
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: enabling-the-compose-key
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

You can enable the Compose key either for an individual user or for all users.


// The following include statements pull in the module files that comprise the assembly. Include any combination of concept, procedure, or reference modules required to cover the user story. You can also include other assemblies.

// include::modules/TEMPLATE_CONCEPT_explaining_a_concept.adoc[leveloffset=+1]

// [leveloffset=+1] ensures that when a module starts with a level-1 heading (= Heading), the heading will be interpreted as a level-2 heading (== Heading) in the assembly.
include::modules/desktop/proc_enabling-the-compose-key-for-an-individual-user-using-tweaks.adoc[leveloffset=+1]

include::modules/desktop/proc_enabling-the-compose-key-for-an-individual-user-using-gsettings.adoc[leveloffset=+1]

include::modules/desktop/proc_enabling-the-compose-key-for-all-users.adoc[leveloffset=+1]

// include::modules/TEMPLATE_PROCEDURE_doing_one_procedure.adoc[leveloffset=+1]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-enabling-the-compose-key[:context: {parent-context-of-enabling-the-compose-key}]
ifndef::parent-context-of-enabling-the-compose-key[:!context:]
ifdef::parent-context-of-inputting-uncommon-characters-using-the-compose-key[:context: {parent-context-of-inputting-uncommon-characters-using-the-compose-key}]
ifndef::parent-context-of-inputting-uncommon-characters-using-the-compose-key[:!context:]
