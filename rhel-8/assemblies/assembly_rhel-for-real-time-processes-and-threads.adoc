

ifdef::context[:parent-context-of-assembly_rhel-for-real-time-processes-and-threads: {context}]


:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_rhel-for-real-time-processes-and-threads"]
endif::[]
ifdef::context[]
[id="assembly_rhel-for-real-time-processes-and-threads_{context}"]
endif::[]
= RHEL for Real Time processes and threads


:context: assembly_rhel-for-real-time-processes-and-threads


[role="_abstract"]
The RHEL for Real Time key factors in operating systems are minimal interrupt latency and minimal thread switching latency. Although all programs use threads and processes, RHEL for Real Time handles them in a different way compared to the standard Red Hat Enterprise Linux.

In real-time, using parallelism helps achieve greater efficiency in task execution and latency. Parallelism is when multiple tasks or several sub-tasks run at the same time using the multi-core infrastructure of CPU.

include::modules/rt-kernel/con_processes.adoc[leveloffset=+1]

include::modules/rt-kernel/con_threads.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* `fork(2)` man page
* `exec(2)` man page



ifdef::parent-context-of-assembly_rhel-for-real-time-processes-and-threads[:context: {parent-context-of-assembly_rhel-for-real-time-processes-and-threads}]
ifndef::parent-context-of-assembly_rhel-for-real-time-processes-and-threads[:!context:]
