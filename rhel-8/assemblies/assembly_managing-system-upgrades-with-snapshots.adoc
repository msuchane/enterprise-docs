ifdef::context[:parent-context-of-managing-system-upgrades-with-snapshots: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="managing-system-upgrades-with-snapshots"]
endif::[]
ifdef::context[]
[id="managing-system-upgrades-with-snapshots_{context}"]
endif::[]
= Managing system upgrades with snapshots

:context: managing-system-upgrades-with-snapshots

Perform rollback capable upgrades of {RHEL} systems to return to an earlier version of the operating system. You can use the *Boom Boot Manager* and the Leapp operating system modernization framework.

Before performing the operating system upgrades, consider the following aspects:

* System upgrades with snapshots do not work on multiple file systems in your system tree, for example, a separate `/var` or `/usr` partition.

* System upgrades with snapshots do not work for the Red Hat Update Infrastructure (RHUI) systems. Instead of using the Boom utility, consider creating snapshots of your virtual machines (VMs).

include::modules/filesystems-and-storage/con_overview-of-the-boom-process.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_upgrading-to-another-version-using-boom-boot-manager.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_switching-between-red-hat-enterprise-linux-versions.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_deleting-the-logical-volume-snapshot.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_creating-a-rollback-boot-entry.adoc[leveloffset=+1]

ifdef::parent-context-of-managing-system-upgrades-with-snapshots[:context: {parent-context-of-managing-system-upgrades-with-snapshots}]
ifndef::parent-context-of-managing-system-upgrades-with-snapshots[:!context:]
