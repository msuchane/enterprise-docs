:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_using-ansible-automation-platform-and-ansible-execution-environments-to-automate-workload: {context}]

ifndef::context[]
[id="assembly_using-ansible-automation-platform-and-ansible-execution-environments-to-automate-workload"]
endif::[]
ifdef::context[]
[id="assembly_using-ansible-automation-platform-and-ansible-execution-environments-to-automate-workload_{context}"]
endif::[]
= Using Ansible automation platform and Ansible execution environments to automate workload

:context: assembly_using-ansible-automation-platform-and-ansible-execution-environments-to-automate-workload

[role="_abstract"]
Previously, customers used to develop their Ansible content such as playbooks, roles, or collections locally. They had to ensure that all the necessary dependencies were readily available directly on their computer for their Ansible content to work correctly. The dependencies could be:

* Python libraries
* Dependencies for Ansible Collections
* Dependencies for system packages

[#{context}-figu-ansible-collections]
.Ansible Collections
image::ansible-collections.png[]


Afterwards, customers had to install the content on their production environment. This could cause a problem with restrictions when putting such content on the lock-down production systems.

Next, verifying that everything was installed correctly could be another obstacle. Especially when some dependencies were omitted, or when additional content which required dependencies was added. Since at each such time the whole process had to be repeated.

The following sections introduce Ansible execution environment and explain how to use it to avoid the obstacles outlined above.

[IMPORTANT]
====
For customers to profit from content consumption of this chapter, it is necessary to have the Ansible Automation Platform subscription.
====

include::modules/core-kernel/con_ansible-execution-environment.adoc[leveloffset=+1]

include::modules/core-kernel/proc_preparing-a-system-for-custom-ansible-execution-environments.adoc[leveloffset=+1]

include::modules/core-kernel/proc_creating-custom-ansible-execution-environments.adoc[leveloffset=+1]

include::modules/core-kernel/proc_deploying-rhel-system-roles-collection-using-custom-ansible-execution-environment.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_using-ansible-automation-platform-and-ansible-execution-environments-to-automate-workload[:context: {parent-context-of-assembly_using-ansible-automation-platform-and-ansible-execution-environments-to-automate-workload}]
ifndef::parent-context-of-assembly_using-ansible-automation-platform-and-ansible-execution-environments-to-automate-workload[:!context:]
