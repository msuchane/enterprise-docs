:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-scripts-in-kickstart-file: {context}]

[id="scripts-in-kickstart-file_{context}"]
= Scripts in Kickstart file

:context: scripts-in-kickstart-file

[role="_abstract"]
A kickstart file can include the following scripts:

* `%pre`
* `%pre-install`
* `%post`

This section provides the following details about the scripts:

* Execution time
* Types of commands that can be included in the script
* Purpose of the script
* Script options

include::assembly_pre-script-in-kickstart-file.adoc[leveloffset=+1]

include::assembly_pre-install-script-in-kickstart-file.adoc[leveloffset=+1]

include::assembly_post-script-in-kickstart-file.adoc[leveloffset=+1]




ifdef::parent-context-of-scripts-in-kickstart-file[:context: {parent-context-of-scripts-in-kickstart-file}]
ifndef::parent-context-of-scripts-in-kickstart-file[:!context:]
