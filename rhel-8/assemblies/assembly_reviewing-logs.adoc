:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-reviewing-logs: {context}]

ifndef::context[]
[id="reviewing-logs"]
endif::[]
ifdef::context[]
[id="reviewing-logs_{context}"]
endif::[]
= Reviewing logs in the web console

:context: reviewing-logs

[role="_abstract"]
Learn how to access, review and filter logs in the RHEL 8 web console.

include::modules/cockpit/proc_reviewing-logs-in-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/proc_filtering-logs-in-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/con_text-search-options-for-filtering-logs-in-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/proc_using-a-text-search-box-to-filter-logs-in-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/ref_options-for-logs-filtering.adoc[leveloffset=+1]


ifdef::parent-context-of-reviewing-logs[:context: {parent-context-of-reviewing-logs}]
ifndef::parent-context-of-reviewing-logs[:!context:]
