:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-configuring-secure-communication-with-the-ssh-system-roles: {context}]


[id="configuring-secure-communication-with-the-ssh-system-roles_{context}"]
= Configuring secure communication with the `ssh` system roles

:context: configuring-secure-communication-with-the-ssh-system-roles

[role="_abstract"]
ifeval::[{ProductNumber} == 8]
As an administrator, you can use the `sshd` system role to configure SSH servers and the `ssh` system role to configure SSH clients consistently on any number of RHEL systems at the same time by using Red Hat Ansible Automation Platform.
endif::[]

ifeval::[{ProductNumber} == 9]
As an administrator, you can use the `sshd` system role to configure SSH servers and the `ssh` system role to configure SSH clients consistently on any number of RHEL systems at the same time using the Ansible Core package.
endif::[]

include::modules/security/ref_sshd-system-role-variables.adoc[leveloffset=+1]

include::modules/security/proc_configuring-openssh-servers-using-the-sshd-system-role.adoc[leveloffset=+1]

include::modules/security/ref_ssh-system-role-variables.adoc[leveloffset=+1]

include::modules/security/proc_configuring-openssh-clients-using-the-ssh-system-role.adoc[leveloffset=+1]

include::modules/security/proc_using-the-ssh-server-system-role-for-non-exclusive-configuration.adoc[leveloffset=+1]

ifdef::parent-context-of-configuring-secure-communication-with-the-ssh-system-roles[:context: {parent-context-of-configuring-secure-communication-with-the-ssh-system-roles}]
ifndef::parent-context-of-configuring-secure-communication-with-the-ssh-system-roles[:!context:]
