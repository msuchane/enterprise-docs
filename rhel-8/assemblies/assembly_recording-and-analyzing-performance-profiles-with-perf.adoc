:_mod-docs-content-type: ASSEMBLY
:parent-context-of-recording-and-analyzing-performance-profiles-with-perf: {context}


[id="recording-and-analyzing-performance-profiles-with-perf_{context}"]
= Recording and analyzing performance profiles with perf


:context: recording-and-analyzing-performance-profiles-with-perf

[role="_abstract"]
The `perf` tool allows you to record performance data and analyze it at a later time.

.Prerequisites

* You have the `perf` user space tool installed as described in xref:installing-perf_getting-started-with-perf[Installing perf].


include::modules/performance/con_the-purpose-of-perf-record.adoc[leveloffset=+1]

include::modules/performance/proc_recording-a-performance-profile-without-root-access.adoc[leveloffset=+1]

include::modules/performance/proc_recording-a-performance-profile-with-root-access.adoc[leveloffset=+1]

include::modules/performance/proc_recording-a-performance-profile-in-per-cpu-mode.adoc[leveloffset=+1]

include::modules/performance/proc_capturing-call-graph-data-with-perf-record.adoc[leveloffset=+1]

include::modules/performance/proc_analyzing-perf-data-with-perf-report.adoc[leveloffset=+1]

include::modules/performance/con_interpretation-of-perf-report-output.adoc[leveloffset=+1]

include::modules/performance/proc_generating-a-perf-data-file-that-is-readable-on-a-different-device.adoc[leveloffset=+1]

include::modules/performance/proc_analyzing-a-perf-data-file-that-was-created-on-a-different-device.adoc[leveloffset=+1]

include::modules/performance/con_why-perf-displays-some-function-names-as-raw-function-addresses.adoc[leveloffset=+1]

include::modules/platform-tools/proc_enabling-debug-and-source-repositories.adoc[leveloffset=+1]

include::modules/platform-tools/proc_getting-debuginfo-packages-for-an-application-or-library-using-gdb.adoc[leveloffset=+1]


:context: {parent-context-of-recording-and-analyzing-performance-profiles-with-perf}
