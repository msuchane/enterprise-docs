:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_using-the-logging-system-roles-with-relp: {context}]

ifndef::context[]
[id="assembly_using-the-logging-system-roles-with-relp"]
endif::[]
ifdef::context[]
[id="assembly_using-the-logging-system-roles-with-relp_{context}"]
endif::[]

= Using the `logging` RHEL system roles with RELP

:context: assembly_using-the-logging-system-roles-with-relp

[role="_abstract"]
Reliable Event Logging Protocol (RELP) is a networking protocol for data and message logging over the TCP network. It ensures reliable delivery of event messages and you can use it in environments that do not tolerate any message loss.

The RELP sender transfers log entries in form of commands and the receiver acknowledges them once they are processed. To ensure consistency, RELP stores the transaction number to each transferred command for any kind of message recovery.

You can consider a remote logging system in between the RELP Client and RELP Server. The RELP Client transfers the logs to the remote logging system and the RELP Server receives all the logs sent by the remote logging system.

Administrators can use the `logging` {RHELSystemRoles} to configure the logging system to reliably send and receive log entries.

include::modules/system-roles/proc_configuring-client-logging-with-relp.adoc[leveloffset=+1]

include::modules/system-roles/proc_configuring-server-logging-with-relp.adoc[leveloffset=+1]


ifdef::parent-context-of-using-the-logging-system-roles-with-relp[:context: {parent-context-of-using-the-logging-system-roles-with-relp}]
ifndef::parent-context-of-using-the-logging-system-roles-with-relp[:!context:]
