ifdef::context[:parent-context-of-assembly_application-timestamping: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_application-timestamping"]
endif::[]
ifdef::context[]
[id="assembly_application-timestamping_{context}"]
endif::[]
= Application timestamping

:context: assembly_application-timestamping

[role="_abstract"]
Applications that perform frequent timestamps are affected by the CPU cost of reading the clock. The high cost and amount of time used to read the clock can have a negative impact on an application’s performance.

You can reduce the cost of reading the clock by selecting a hardware clock that has a reading mechanism, faster than that of the default clock.

In RHEL for Real Time, a further performance gain can be acquired by using POSIX clocks with the [function]`clock_gettime()` function to produce clock readings with the lowest possible CPU cost.

These benefits are more evident on systems which use hardware clocks with high reading costs.

include::modules/rt-kernel/con_posix-clocks.adoc[leveloffset=+1]

include::modules/rt-kernel/con_the-_coarse-clock-variant-in-clock_gettime.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* `clock_gettime()` man page




ifdef::parent-context-of-assembly_application-timestamping[:context: {parent-context-of-assembly_application-timestamping}]
ifndef::parent-context-of-assembly_application-timestamping[:!context:]
