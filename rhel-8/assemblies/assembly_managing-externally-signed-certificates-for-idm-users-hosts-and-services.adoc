ifdef::context[:parent-context-of-managing-externally-signed-certificates-for-idm-users-hosts-and-services: {context}]
[id="managing-externally-signed-certificates-for-idm-users-hosts-and-services_{context}"]
= Managing externally signed certificates for IdM users, hosts, and services
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: managing-externally-signed-certificates-for-idm-users-hosts-and-services
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

This chapter describes how to use the {IPA} (IdM) command-line interface (CLI) and the IdM Web UI to add or remove user, host, or service certificates that were issued by an external certificate authority (CA).


include::modules/identity-management/proc_adding-a-certificate-issued-by-an-external-ca-to-an-idm-user-host-or-service-by-using-the-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_adding-a-certificate-issued-by-an-external-ca-to-an-idm-user-host-or-service-by-using-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_removing-a-certificate-issued-by-an-external-ca-from-an-idm-user-host-or-service-account-by-using-the-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_removing-a-certificate-issued-by-an-external-ca-from-an-idm-user-host-or-service-account-by-using-the-idm-web-ui.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/using_ansible_to_install_and_manage_identity_management/index#ensuring-the-presence-of-an-externally-signed-certificate-in-an-idm-service-entry-using-an-ansible-playbook_ensuring-the-presence-and-absence-of-services-in-idm-using-ansible[Ensuring the presence of an externally signed certificate in an IdM service entry using an Ansible playbook]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-managing-externally-signed-certificates-for-idm-users-hosts-and-services[:context: {parent-context-of-managing-externally-signed-certificates-for-idm-users-hosts-and-services}]
ifndef::parent-context-of-managing-externally-signed-certificates-for-idm-users-hosts-and-services[:!context:]
