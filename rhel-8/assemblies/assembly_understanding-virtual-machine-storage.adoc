:_mod-docs-content-type: ASSEMBLY
// This assembly can be included from other assemblies using the following
// include statement:
// include::assembly_understanding-virtual-machine-storage.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-understanding-virtual-machine-storage: {context}

[id="understanding-virtual-machine-storage_{context}"]
= Understanding virtual machine storage

:context: understanding-virtual-machine-storage

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
Managing storage for virtual machines
| User story |
As a virtualization administrator, I want to add, remove, and modify disks and other storage devices to virtual machines (both live and shut-off) in {ProductShortName} {ProductNumber}, so my VMs can flexibly use the storage resources of my hardware.
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

If you are new to virtual machine (VM) storage, or are unsure about how it works, the following sections provide a general overview about the various components of VM storage, how it works, management basics, and the supported solutions provided by Red Hat.

You can find information about:

* xref:understanding-storage-pools_understanding-virtual-machine-storage[Storage pools]
* xref:storage-volumes_understanding-virtual-machine-storage[Storage volumes]
* xref:con_storage-management-using-libvirt_understanding-virtual-machine-storage[Managing storage using libvirt]
* xref:con_overview-of-storage-management_understanding-virtual-machine-storage[Overview of VM storage]
* xref:ref_supported-and-unsupported-storage-pool-types_understanding-virtual-machine-storage[Supported and unsupported storage pool types]

//include::modules/virtualization/con_understanding-virtual-machine-storage.adoc[leveloffset=+1]

include::modules/virtualization/con_understanding-storage-pools.adoc[leveloffset=+1]

include::modules/virtualization/con_understanding-storage-volumes.adoc[leveloffset=+1]

include::modules/virtualization/con_storage-management-using-libvirt.adoc[leveloffset=+1]

include::modules/virtualization/con_overview-of-storage-management.adoc[leveloffset=+1]

include::modules/virtualization/ref_supported-and-unsupported-storage-pool-types.adoc[leveloffset=+1]


// [id='related-information-{context}']
// == Related information
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-understanding-virtual-machine-storage}
