:_mod-docs-content-type: ASSEMBLY

:parent-context-of-providing-dhcp-services: {context}

[id='providing-dhcp-services_{context}']
= Providing DHCP services

:context: providing-dhcp-services


ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

[role="_abstract"]
The dynamic host configuration protocol (DHCP) is a network protocol that automatically assigns IP information to clients. You can set up the `dhcpd` service to provide a DHCP server and DHCP relay in your network.

include::modules/network-infrastructure/con_the-differences-between-static-and-dynamic-ip-addressing.adoc[leveloffset=+1]

include::modules/network-infrastructure/con_the-dhcp-transaction-phases.adoc[leveloffset=+1]

include::modules/network-infrastructure/ref_the-differences-when-using-dhcpd-for-dhcpv4-and-dhcpv6.adoc[leveloffset=+1]

include::modules/network-infrastructure/con_the-lease-database-of-the-dhcpd-service.adoc[leveloffset=+1]

include::modules/network-infrastructure/ref_comparison-of-dhcpv6-to-radvd.adoc[leveloffset=+1]

include::modules/network-infrastructure/proc_configuring-the-radvd-service-for-ipv6-routers.adoc[leveloffset=+1]

include::modules/network-infrastructure/proc_setting-network-interfaces-for-the-dhcp-server.adoc[leveloffset=+1]

include::modules/network-infrastructure/proc_setting-up-the-dhcp-service-for-subnets-directly-connected-to-the-dhcp-server.adoc[leveloffset=+1]

include::modules/network-infrastructure/proc_setting-up-the-dhcp-service-for-subnets-that-are-not-directly-connected-to-the-dhcp-server.adoc[leveloffset=+1]

include::modules/network-infrastructure/proc_assigning-a-static-address-to-a-host-using-dhcp.adoc[leveloffset=+1]

include::modules/network-infrastructure/proc_using-a-group-declaration-to-apply-parameters-to-multiple-hosts-subnets-and-shared-networks-at-the-same-time.adoc[leveloffset=+1]

include::modules/network-infrastructure/proc_restoring-a-corrupt-lease-database.adoc[leveloffset=+1]

include::modules/network-infrastructure/proc_setting-up-a-dhcp-relay-agent.adoc[leveloffset=+1]

:context: {parent-context-of-providing-dhcp-services}
