////
Retains the context of the parent assembly if this assembly is nested within another assembly.
For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
See also the complementary step on the last line of this file.
////

ifdef::context[:parent-context-of-managing-containers-using-the-ansible-playbook: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="managing-containers-using-the-ansible-playbook"]
endif::[]
ifdef::context[]
[id="managing-containers-using-the-ansible-playbook_{context}"]
endif::[]
= Managing containers using the Ansible playbook

:context: managing-containers-using-the-ansible-playbook

With Podman 4.2, you can use the Podman RHEL system role to manage Podman configuration, containers, and systemd services which run Podman containers.

RHEL system roles provide a configuration interface to remotely manage multiple RHEL systems. You can use the interface to manage system configurations across multiple versions of RHEL, as well as adopting new major releases.
For more information, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/index[Automating system administration by using {RHELSystemRoles}s]. 

include::modules/containers/proc_creating-a-rootless-container-with-bind-mount.adoc[leveloffset=+1]

include::modules/containers/proc_creating-a-rootful-container-with-podman-volume.adoc[leveloffset=+1]

include::modules/containers/proc_creating-a-quadlet-application-with-secrets.adoc[leveloffset=+1]

//[role="_additional-resources"]
//== Additional resources (or Next steps)

////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-managing-containers-using-the-ansible-playbook[:context: {parent-context-of-managing-containers-using-the-ansible-playbook}]
ifndef::parent-context-of-managing-containers-using-the-ansible-playbook[:!context:]

