
ifdef::context[:parent-context-of-managing-public-ssh-keys-for-users: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="managing-public-ssh-keys-for-users"]
endif::[]
ifdef::context[]
[id="managing-public-ssh-keys-for-users_{context}"]
endif::[]
= Managing public SSH keys for users

:context: managing-public-ssh-keys-for-users


Identity Management allows you to upload a public SSH key to a user entry. The user who has access to the corresponding private SSH key can use SSH to log into an IdM machine without using Kerberos credentials. Note that users can still authenticate by providing their Kerberos credentials if they are logging in from a machine where their private SSH key file is not available.

include::modules/identity-management/proc_uploading-ssh-keys-using-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_uploading-ssh-keys-using-the-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_deleting-ssh-keys-for-a-user-using-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_deleting-ssh-keys-for-a-user-using-the-idm-cli.adoc[leveloffset=+1]


ifdef::parent-context-of-managing-public-ssh-keys-for-users[:context: {parent-context-of-managing-public-ssh-keys-for-users}]
ifndef::parent-context-of-managing-public-ssh-keys-for-users[:!context:]
