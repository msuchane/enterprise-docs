:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-snapshot-of-logical-volumes: {context}]

ifndef::context[]
[id="snapshot-of-logical-volumes"]
endif::[]
ifdef::context[]
[id="snapshot-of-logical-volumes_{context}"]
endif::[]
= Snapshot of logical volumes

:context: snapshot-of-logical-volumes

Using the LVM snapshot feature, you can create virtual images of a volume, for example, _/dev/sda_,  at a particular instant without causing a service interruption.

// include::modules/filesystems-and-storage/proc_controlling-auto-activation.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_overview-of-snapshot-volumes.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_creating-a-snapshot-of-the-original-volume.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_merging-snapshot-to-its-original-volume.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_creating-lvm-snapshots-using-the-snapshot-rhel-system-role.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_unmounting-lvm-snapshots-using-the-snapshot-rhel-system-role.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_extending-lvm-snapshots-using-the-snapshot-rhel-system-role.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_reverting-lvm-snapshots-using-the-snapshot-rhel-system-role.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_removing-lvm-snapshots-using-the-snapshot-rhel-system-role.adoc[leveloffset=+1]

ifdef::parent-context-of-snapshot-of-logical-volumes[:context: {parent-context-of-snapshot-of-logical-volumes}]
ifndef::parent-context-of-snapshot-of-logical-volumes[:!context:]
