ifdef::context[:parent-context-of-troubleshooting-lvm: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="troubleshooting-lvm"]
endif::[]
ifdef::context[]
[id="troubleshooting-lvm_{context}"]
endif::[]
= Troubleshooting LVM

:context: troubleshooting-lvm

You can use Logical Volume Manager (LVM) tools to troubleshoot a variety of issues in LVM volumes and groups.

include::modules/filesystems-and-storage/proc_gathering-diagnostic-data-on-lvm.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_displaying-information-about-failed-lvm-devices.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_removing-lost-lvm-physical-volumes-from-a-volume-group.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_finding-the-metadata-of-a-missing-lvm-physical-volume.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_restoring-metadata-on-an-lvm-physical-volume.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_rounding-errors-in-lvm-output.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_preventing-the-rounding-error-when-creating-an-lvm-volume.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_lvm-metadata-and-their-location-on-disk.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_extracting-vg-metadata-from-a-disk.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_saving-extracted-metadata-to-a-file.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_repairing-a-disk-with-damaged-lvm-headers-and-metadata-using-the-pvcreate-and-the-vgcfgrestore-commands.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_repairing-a-disk-with-damaged-lvm-headers-and-metadata-using-the-pvck-command.adoc[leveloffset=+1]

include::assembly_troubleshooting-lvm-raid.adoc[leveloffset=+1]

include::assembly_troubleshooting-duplicate-physical-volume-warnings-for-multipathed-lvm-devices.adoc[leveloffset=+1]

ifdef::parent-context-of-troubleshooting-lvm[:context: {parent-context-of-troubleshooting-lvm}]
ifndef::parent-context-of-troubleshooting-lvm[:!context:]
