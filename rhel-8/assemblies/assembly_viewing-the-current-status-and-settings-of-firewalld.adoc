:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
// assembly_using-and-configuring-firewalld.adoc

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_viewing-the-current-status-and-settings-of-firewalld.adoc[leveloffset=+1]

:parent-context-of-viewing-the-current-status-and-settings-of-firewalld: {context}

[id="viewing-the-current-status-and-settings-of-firewalld_{context}"]
= Viewing the current status and settings of `firewalld`

:context: viewing-the-current-status-and-settings-of-firewalld

[role="_abstract"]
To monitor the `firewalld` service, you can display the status, allowed services, and settings.

include::modules/packet-filtering/proc_viewing-the-current-status-of-firewalld.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_viewing-allowed-services-using-gui.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_viewing-firewalld-settings-using-cli.adoc[leveloffset=+1]

:context: {parent-context-of-viewing-the-current-status-and-settings-of-firewalld}
