:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="removing-storage-devices"]
endif::[]
ifdef::context[]
[id="removing-storage-devices_{context}"]
endif::[]
= Removing storage devices

:context: removing-storage-devices

You can safely remove a storage device from a running system, which helps prevent system memory overload and data loss.

.Prerequisites
* Before you remove a storage device, you must ensure that you have enough free system memory due to the increased system memory load during an I/O flush. Use the following commands to view the current memory load and free memory of the system:
+
[subs=+quotes]
....
# *vmstat 1 100*
# *free*
....

* Red Hat does not recommend removing a storage device on a system where:

** Free memory is less than 5% of the total memory in more than 10 samples per 100.
** Swapping is active (non-zero `si` and `so` columns in the `vmstat` command output).

include::modules/filesystems-and-storage/con_storage-device-safe-removal.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_removing-block-devices-and-associated-metadata.adoc[leveloffset=+1]

ifdef::parent-context-of-removing-storage-devices[:context: {parent-context-of-removing-storage-devices}]
ifndef::parent-context-of-removing-storage-devices[:!context:]
