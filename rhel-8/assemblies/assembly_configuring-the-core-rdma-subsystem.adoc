:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-configuring-the-core-rdma-subsystem: {context}]

[id="configuring-the-core-rdma-subsystem_{context}"]
= Configuring the core RDMA subsystem

:context: configuring-the-core-rdma-subsystem

The `rdma` service configuration manages the network protocols and communication standards such as InfiniBand, iWARP, and RoCE.

include::modules/infiniband-rdma/proc_renaming-ipoib-devices.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_increasing-the-amount-of-memory-that-users-are-allowed-to-pin-in-the-system.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_configuring-the-rdma-service.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_enabling-nfs-over-rdma-on-an-nfs-server.adoc[leveloffset=+1]

ifdef::parent-context-of-configuring-the-core-rdma-subsystem[:context: {parent-context-of-configuring-the-core-rdma-subsystem}]
ifndef::parent-context-of-configuring-the-core-rdma-subsystem[:!context:]
