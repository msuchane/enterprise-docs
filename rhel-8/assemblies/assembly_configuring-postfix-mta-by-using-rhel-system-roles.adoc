ifdef::context[:parent-context-of-configuring-postfix-mta-by-using-rhel-system-roles: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-postfix-mta-by-using-rhel-system-roles"]
endif::[]
ifdef::context[]
[id="configuring-postfix-mta-by-using-rhel-system-roles_{context}"]
endif::[]
= Configuring Postfix MTA by using the RHEL system role

:context: configuring-postfix-mta-by-using-rhel-system-roles

[role="_abstract"]
With the `postfix` {RHELSystemRoles}, you can consistently streamline automated configurations of the Postfix service, a Sendmail-compatible mail transfer agent (MTA) with modular design and a variety of configuration options.
The `rhel-system-roles` package contains this {RHELSystemRoles}, and also the reference documentation.

include::modules/system-roles/proc_using-the-postfix-system-role-to-automate-basic-postfix-mta-administration.adoc[leveloffset=+1]


ifdef::parent-context-of-configuring-postfix-mta-by-using-rhel-system-roles[:context: {parent-context-of-configuring-postfix-mta-by-using-rhel-system-roles}]
ifndef::parent-context-of-configuring-postfix-mta-by-using-rhel-system-roles[:!context:]
