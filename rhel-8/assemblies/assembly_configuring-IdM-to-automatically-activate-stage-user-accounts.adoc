:_mod-docs-content-type: ASSEMBLY
:parent-context-of-configuring-IdM-for-external-provisioning-of-users: {context}

[id="configuring-IdM-for-external-provisioning-of-users_{context}"]
= Configuring IdM for external provisioning of users

:context: configuring-IdM-for-external-provisioning-of-users

As a system administrator, you can configure {IPA} (IdM) to support the provisioning of users by an external solution for managing identities.

Rather than use the `ipa` utility, the administrator of the external provisioning system can access the IdM LDAP using the `ldapmodify` utility. The administrator can add individual stage users xref:adding-an-IdM-stage-user-directly-from-the-CLI-using-ldapmodify_{context}[from the CLI using ldapmodify] or xref:adding-an-IdM-stage-user-defined-in-an-LDIF-file_{context}[using an LDIF file].

The assumption is that you, as an IdM administrator, fully trust your external provisioning system to only add validated users. However, at the same time you do not want to assign the administrators of the external provisioning system the IdM role of `User Administrator` to enable them to add new active users directly.

You can xref:configuring-automatic-activation-of-IdM-stage-user-accounts_{context}[configure a script] to automatically move the staged users created by the external provisioning system to active users automatically.

This chapter contains these sections:

. xref:preparing-IdM-accounts-for-automatic-activation-of-stage-user-accounts_{context}[Preparing {IPA} (IdM)] to use an external provisioning system to add stage users to IdM.

. xref:configuring-automatic-activation-of-IdM-stage-user-accounts_{context}[Creating a script] to move the users added by the external provisioning system from stage to active users.

. Using an external provisioning system to add an IdM stage user. You can do that in two ways:

* xref:adding-an-IdM-stage-user-defined-in-an-LDIF-file_{context}[Add an IdM stage user using an LDIF file]
* xref:adding-an-IdM-stage-user-directly-from-the-CLI-using-ldapmodify_{context}[Add an IdM stage user directly from the CLI using ldapmodify]


include::modules/identity-management/proc_preparing-IdM-accounts-for-automatic-activation-of-stage-user-accounts.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-automatic-activation-of-IdM-stage-user-accounts.adoc[leveloffset=+1]

include::modules/identity-management/proc_adding-an-IdM-stage-user-defined-in-an-LDIF-file.adoc[leveloffset=+1]

include::modules/identity-management/proc_adding-an-IdM-stage-user-directly-from-the-CLI-using-ldapmodify.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
ifeval::[{ProductNumber} == 8]
* See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/using-ldapmodify-to-manage-idm-users-externally_configuring-and-managing-idm[Using ldapmodify to manage IdM users externally].
endif::[]
ifeval::[{ProductNumber} == 9]
* See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_idm_users_groups_hosts_and_access_control_rules/using-ldapmodify-to-manage-idm-users-externally_managing-users-groups-hosts[Using ldapmodify to manage IdM users externally].
endif::[]

:context: {parent-context-of-configuring-IdM-for-external-provisioning-of-users}
