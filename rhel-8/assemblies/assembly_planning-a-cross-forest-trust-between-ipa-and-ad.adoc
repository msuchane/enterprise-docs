:_mod-docs-content-type: ASSEMBLY
include::modules/identity-management/_attributes-identity-management-and-access-control.adoc[]
//:internal:

:parent-context-of-planning-a-cross-forest-trust-between-idm-and-ad: {context}

[id="planning-a-cross-forest-trust-between-idm-and-ad_{context}"]
= Planning a cross-forest trust between IdM and AD

[role="_abstract"]
{AD} (AD) and {IPA} (IdM) are two alternative environments managing a variety of core services, such as Kerberos, LDAP, DNS, and certificate services. A _cross-forest trust_ relationship transparently integrates these two diverse environments by enabling all core services to interact seamlessly. The following sections provide advice on how to plan and design a cross-forest trust deployment.

:context: planning-a-cross-forest-trust-between-idm-and-ad

// https://wiki.idm.lab.bos.redhat.com/export/idmwiki/File:2014-trust.odp
// https://www.freeipa.org/page/Active_Directory_trust_setup

include::modules/identity-management/con_cross-forest-and-external-trusts-between-idm-and-ad.adoc[leveloffset=+1]

include::modules/identity-management/con_trust-controllers-and-trust-agents.adoc[leveloffset=+1]

//include::modules/identity-management/con_one-way-trusts-and-two-way-trusts-fin.adoc[leveloffset=+1]
include::modules/identity-management/con_one-way-trusts-and-two-way-trusts-reduced.adoc[leveloffset=+1]

include::assembly_ensuring-support-for-common-encryption-types-in-ad-and-rhel.adoc[leveloffset=+1]

include::modules/identity-management/con_kerberos-fast-for-trusted-domains.adoc[leveloffset=+1]

include::modules/identity-management/con_posix-and-id-mapping-id-range-types-for-ad-users.adoc[leveloffset=+1]

include::modules/identity-management/ref_options-for-automatically-mapping-private-groups-for-ad-users-posix.adoc[leveloffset=+1]

include::modules/identity-management/ref_options-for-automatically-mapping-private-groups-for-ad-users-id-mapping.adoc[leveloffset=+1]

include::modules/identity-management/proc_enabling-automatic-private-group-mapping-for-a-posix-id-range-on-the-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_enabling-automatic-private-group-mapping-for-a-posix-id-range-in-the-idm-webui.adoc[leveloffset=+1]

include::modules/identity-management/con_non-posix-external-groups-and-sid-mapping.adoc[leveloffset=+1]

include::modules/identity-management/con_guidelines-for-setting-up-dns-for-an-idm-ad-trust.adoc[leveloffset=+1]

include::modules/identity-management/con_guidelines-for-configuring-netbios-names.adoc[leveloffset=+1]

include::modules/identity-management/con_supported-versions-of-windows-server.adoc[leveloffset=+1]

include::modules/identity-management/con_ad-server-discovery-and-affinity.adoc[leveloffset=+1]

include::modules/identity-management/ref_operations-performed-during-indirect-integration-of-idm-to-ad.adoc[leveloffset=+1]
:context: {parent-context-of-planning-a-cross-forest-trust-between-idm-and-ad}
