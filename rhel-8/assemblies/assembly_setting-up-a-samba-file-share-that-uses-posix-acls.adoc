:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// assembly_setting-up-a-samba-file-share-that-uses-posix-acls.adoc

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_setting-up-a-samba-file-share-that-uses-posix-acls.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-assembly_setting-up-a-samba-file-share-that-uses-posix-acls: {context}

// The file name and the ID are based on the assembly title.
// For example:
// * file name: assembly_my-assembly-a.adoc
// * ID: [id='assembly_my-assembly-a_{context}']
// * Title: = My assembly A
//
// The ID is used as an anchor for linking to the module.
// Avoid changing it after the module has been published
// to ensure existing links are not broken.
//
// In order for  the assembly to be reusable in other assemblies in a guide,
// include {context} in the ID: [id='a-collection-of-modules_{context}'].
//
// If the assembly covers a task, start the title with a verb in the gerund
// form, such as Creating or Configuring.
[id='assembly_setting-up-a-samba-file-share-that-uses-posix-acls_{context}']
= Setting up a Samba file share that uses POSIX ACLs

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: assembly_setting-up-a-samba-file-share-that-uses-posix-acls

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

As a Linux service, Samba supports shares with POSIX ACLs. They enable you to manage permissions locally on the Samba server using utilities, such as `chmod`. If the share is stored on a file system that supports extended attributes, you can define ACLs with multiple users and groups.


[NOTE]
====
ifeval::[{ProductNumber} == 8]
ifdef::differentserver-title[]
If you need to use fine-granular Windows ACLs instead, see xref:assembly_setting-up-a-share-that-uses-windows-acls_assembly_using-samba-as-a-server[Setting up a share that uses Windows ACLs].
endif::[]
ifndef::differentserver-title[]
If you need to use fine-granular Windows ACLs instead, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/assembly_using-samba-as-a-server_deploying-different-types-of-servers#assembly_setting-up-a-share-that-uses-windows-acls_assembly_using-samba-as-a-server[Setting up a share that uses Windows ACLs]
endif::[]
endif::[]
ifeval::[{ProductNumber} == 9]
ifdef::network-file-services[]
If you need to use fine-granular Windows ACLs instead, see xref:assembly_setting-up-a-share-that-uses-windows-acls_assembly_using-samba-as-a-server[Setting up a share that uses Windows ACLs].
endif::[]
ifndef::network-file-services[]
If you need to use fine-granular Windows ACLs instead, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring-and-using-network-file-services/assembly_using-samba-as-a-server_configuring-and-using-network-file-services#assembly_setting-up-a-share-that-uses-windows-acls_assembly_using-samba-as-a-server[Setting up a share that uses Windows ACLs]
endif::[]
endif::[]
====

Parts of this section were adopted from the link:https://wiki.samba.org/index.php/Setting_up_a_Share_Using_POSIX_ACLs[Setting up a Share Using POSIX ACLs] documentation published in the Samba Wiki. License: link:https://creativecommons.org/licenses/by/4.0/[CC BY 4.0]. Authors and contributors: See the link:https://wiki.samba.org/index.php?title=Setting_up_a_Share_Using_POSIX_ACLs&action=history[history] tab on the Wiki page.

// The following include statements pull in the module files that comprise
// the assembly. Include any combination of concept, procedure, or reference
// modules required to cover the user story. You can also include other
// assemblies.

include::modules/core-services/proc_adding-a-share-that-uses-posix-acls.adoc[leveloffset=+1]

include::modules/core-services/proc_setting-standard-linux-acls-on-a-samba-share-that-uses-posix-acls.adoc[leveloffset=+1]

include::modules/core-services/proc_setting-extended-acls-on-a-samba-share-that-uses-posix-acls.adoc[leveloffset=+1]

// [leveloffset=+1] ensures that when a module starts with a level-1 heading
// (= Heading), the heading will be interpreted as a level-2 heading
// (== Heading) in the assembly.

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-assembly_setting-up-a-samba-file-share-that-uses-posix-acls}
