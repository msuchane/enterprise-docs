:_mod-docs-content-type: ASSEMBLY
:parent-context-of-managing-host-groups-using-the-idm-web-ui: {context}

[id="managing-host-groups-using-the-idm-web-ui_{context}"]
= Managing host groups using the IdM Web UI

:context: managing-host-groups-using-the-idm-web-ui

[role="_abstract"]
Learn more about how to manage host groups and their members in the Web interface (Web UI) by using the following operations:

* Viewing host groups and their members
* Creating host groups
* Deleting host groups
* Adding host group members
* Removing host group members
* Adding host group member managers
* Removing host group member managers

include::modules/identity-management/con_host-groups-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/proc_viewing-host-groups-in-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_creating-host-groups-in-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_deleting-host-groups-in-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_adding-host-group-members-in-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_removing-host-group-members-in-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_adding-idm-host-group-member-managers-using-the-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_removing-idm-host-group-member-managers-using-the-web-ui.adoc[leveloffset=+1]

:context: {parent-context-of-managing-host-groups-using-the-idm-web-ui}
