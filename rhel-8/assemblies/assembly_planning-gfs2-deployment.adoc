:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly-planning-gfs2-deployment: {context}

[id='assembly_planning-gfs2-deployment-{context}']
= Planning a GFS2 file system deployment
:context: planning-gfs2-deployment

[role="_abstract"]
The Red Hat Global File System 2 (GFS2) file system is a 64-bit symmetric cluster file system which provides a shared name space and manages coherency between multiple nodes sharing a common block device. A GFS2 file system is intended to provide a feature set which is as close as possible to a local file system, while at the same time enforcing full cluster coherency between nodes. To achieve this, the nodes employ a cluster-wide locking scheme for file system resources. This locking scheme uses communication protocols such as TCP/IP to exchange locking information.

In a few cases, the Linux file system API does not allow the clustered nature of GFS2 to be totally transparent; for example, programs using POSIX locks in GFS2 should avoid using the `GETLK` function since, in a clustered environment, the process ID may be for a different node in the cluster. In most cases however, the functionality of a GFS2 file system is identical to that of a local file system.

The Red Hat Enterprise Linux (RHEL) Resilient Storage Add-On provides GFS2, and it depends on the RHEL High Availability Add-On to provide the cluster management required by GFS2.

The `gfs2.ko` kernel module implements the GFS2 file system and is loaded on GFS2 cluster nodes.

To get the best performance from GFS2, it is important to take into account the performance considerations which stem from the underlying design. Just like a local file system, GFS2 relies on the page cache in order to improve performance by local caching of frequently used data. In order to maintain coherency across the nodes in the cluster, cache control is provided by the _glock_ state machine.
// LINK TO For more information about glocks
// and their performance implications, see
// <<s1-ov-lockbounce>>.

[IMPORTANT]
====

Make sure that your deployment of the Red Hat High Availability Add-On meets your needs and can be supported. Consult with an authorized Red Hat representative to verify your configuration prior to deployment.

====

ifeval::[{ProductNumber} == 9]
include::modules/high-availability/con_gfs2-filesystem-format.adoc[leveloffset=+1]
endif::[]


include::modules/high-availability/con_basic-gfs2-parameters.adoc[leveloffset=+1]

include::modules/high-availability/con_gfs2-support-limits.adoc[leveloffset=+1]

include::modules/high-availability/con_gfs2-formatting-considerations.adoc[leveloffset=+1]

include::modules/high-availability/con_gfs2-cluster-considerations.adoc[leveloffset=+1]

include::modules/high-availability/con_gfs2-hardware-considerations.adoc[leveloffset=+1]


:context: {parent-context-of-assembly-planning-gfs2-deployment}
