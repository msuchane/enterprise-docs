:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_creating-and-restoring-container-checkpoints: {context}]

ifndef::context[]
[id="assembly_creating-and-restoring-container-checkpoints"]
endif::[]
ifdef::context[]
[id="assembly_creating-and-restoring-container-checkpoints_{context}"]
endif::[]
= Creating and restoring container checkpoints

:context: assembly_creating-and-restoring-container-checkpoints

[role="_abstract"]
Checkpoint/Restore In Userspace (CRIU) is a software that enables you to set a checkpoint on a running container or an individual application and store its state to disk. You can use data saved to restore the container after a reboot at the same point in time it was checkpointed.

WARNING: The kernel does not support pre-copy checkpointing on AArch64.

//== Prerequisites


include::modules/containers/proc_creating-and-restoring-a-container-checkpoint-locally.adoc[leveloffset=+1]

include::modules/containers/proc_reducing-startup-time-using-container-restore.adoc[leveloffset=+1]

include::modules/containers/proc_migrating-containers-among-systems.adoc[leveloffset=+1]

//[role="_additional-resources"]
//== Additional resources (or Next steps)

ifdef::parent-context-of-assembly_creating-and-restoring-container-checkpoints[:context: {parent-context-of-assembly_creating-and-restoring-container-checkpoints}]
ifndef::parent-context-of-assembly_creating-and-restoring-container-checkpoints[:!context:]
