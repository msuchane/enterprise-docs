

ifdef::context[:parent-context-of-assembly_scheduling-policies-for-rhel-for-real-time: {context}]




:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_scheduling-policies-for-rhel-for-real-time"]
endif::[]
ifdef::context[]
[id="assembly_scheduling-policies-for-rhel-for-real-time_{context}"]
endif::[]
= Scheduling policies for RHEL for Real Time


:context: assembly_scheduling-policies-for-rhel-for-real-time



[role="_abstract"]
In real-time, the scheduler is the kernel component that determines the runnable  thread to run. Each thread has an associated scheduling policy and a static scheduling priority, known as `sched_priority`. The scheduling is preemptive and therefore the currently running thread stops when a thread with a higher static priority gets ready to run. The running thread then returns to the `waitlist` for its static priority.

All Linux threads have one of the following scheduling policies:

* `SCHED_OTHER` or `SCHED_NORMAL`: is the default policy.
* `SCHED_BATCH`: is similar to `SCHED_OTHER`, but with incremental orientation.
* `SCHED_IDLE`: is the policy with lower priority than `SCHED_OTHER`.
* `SCHED_FIFO`: is the first in and first out real-time policy.
* `SCHED_RR`: is the round-robin real-time policy.
* `SCHED_DEADLINE`: is a scheduler policy to prioritize tasks according to the job deadline. The job with the earliest absolute deadline runs first.




include::modules/rt-kernel/con_scheduler-policies.adoc[leveloffset=+1]

include::modules/rt-kernel/ref_parameters-for-sched_deadline-policy.adoc[leveloffset=+1]






ifdef::parent-context-of-assembly_scheduling-policies-for-rhel-for-real-time[:context: {parent-context-of-assembly_scheduling-policies-for-rhel-for-real-time}]
ifndef::parent-context-of-assembly_scheduling-policies-for-rhel-for-real-time[:!context:]
