:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-multipath-devices: {context}]

ifndef::context[]
[id="multipath-devices"]
endif::[]
ifdef::context[]
[id="multipath-devices_{context}"]
endif::[]
= Multipath devices

:context: multipath-devices

[role="_abstract"]
DM Multipath provides a way of organizing the I/O paths logically, by creating a single multipath device on top of the underlying devices. Without DM Multipath, system treats each path from a server node to a storage controller as a separate device, even when the I/O path connects the same server node to the same storage controller.

//include
include::modules/filesystems-and-storage/con_multipath-device-identifiers.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_multipath-devices-in-logical-volumes.adoc[leveloffset=+1]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-multipath-devices[:context: {parent-context-of-multipath-devices}]
ifndef::parent-context-of-multipath-devices[:!context:]
