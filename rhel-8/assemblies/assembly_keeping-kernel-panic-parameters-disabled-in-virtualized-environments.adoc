:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

:parent-context-of-keeping-kernel-panic-parameters-disabled-in-virtualized-environments: {context}

[id='keeping-kernel-panic-parameters-disabled-in-virtualized-environments_{context}']
= Keeping kernel panic parameters disabled in virtualized environments

:context: keeping-kernel-panic-parameters-disabled-in-virtualized-environments

[role="_abstract"]
When configuring a Virtual Machine in {ProductShortName} {ProductNumber},
you should not enable the `softlockup_panic` and `nmi_watchdog` kernel parameters,
because the Virtual Machine might suffer from a spurious soft lockup. And that should not require a kernel panic.

Find the reasons behind this advice in the following sections.

include::modules/core-kernel/con_what-is-a-soft-lockup.adoc[leveloffset=+1]

include::modules/core-kernel/ref_parameters-controlling-kernel-panic.adoc[leveloffset=+1]

include::modules/core-kernel/con_spurious-soft-lockups-in-virtualized-environments.adoc[leveloffset=+1]

:context: {parent-context-of-keeping-kernel-panic-parameters-disabled-in-virtualized-environments}
