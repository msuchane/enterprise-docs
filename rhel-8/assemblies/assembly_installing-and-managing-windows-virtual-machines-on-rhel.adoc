:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <master.adoc> -> context: configuring-and-managing-virtualization

// This assembly can be included from other assemblies using the following
// include statement:
// include::assembly_installing-and-managing-windows-virtual-machines-on-rhel.adoc[leveloffset=+1]

:parent-context-of-installing-and-managing-windows-virtual-machines-on-rhel: {context}

[id='installing-and-managing-windows-virtual-machines-on-rhel_{context}']
= Installing and managing Windows virtual machines

:context: installing-and-managing-windows-virtual-machines-on-rhel

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
master.adoc
| User story |
As an administrator, I want to install, optimize, and configure Windows VMs.
| Jira |
https://projects.engineering.redhat.com/browse/RHELPLAN-15026
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

To use Microsoft Windows as the guest operating system in your virtual machines (VMs) on a {ProductShortName}{nbsp}{ProductNumber} host, Red Hat recommends taking extra steps to ensure these VMs run correctly.

For this purpose, the following sections provide information about installing and optimizing Windows VMs on the host, as well as installing and configuring drivers in these VMs.

//This chapter needs some serious rewrite/restructures

include::modules/virtualization/proc_installing-windows-virtual-machines-on-rhel.adoc[leveloffset=+1]

include::assembly_optimizing-windows-virtual-machines-on-rhel.adoc[leveloffset=+1]

include::modules/virtualization/proc_enabling-standard-harware-security-on-windows-virtual-machines.adoc[leveloffset=+1]

include::modules/virtualization/proc_enabling-enhanced-hardware-security-on-windows-virtual-machines.adoc[leveloffset=+1]


[id='next-steps-{context}']
== Next steps
ifeval::[{ProductNumber} != 8]
* To use utilities for accessing, editing, and creating virtual machine disks or other disk images for a Windows VM, install the [package]`libguestfs-tools` and [package]`libguestfs-winsupport` packages on the host machine:
+
[subs="quotes,attributes"]
----
$ *sudo {PackageManagerCommand} install libguestfs-tools libguestfs-winsupport*
----
endif::[]
ifeval::[{ProductNumber} == 9]
* To use utilities for accessing, editing, and creating virtual machine disks or other disk images for a Windows VM, install the [package]`guestfs-tools` and [package]`guestfs-winsupport` packages on the host machine:
+
[subs="quotes,attributes"]
----
$ *sudo {PackageManagerCommand} install guestfs-tools guestfs-winsupport*
----
endif::[]


* To share files between your {ProductShortName} {ProductNumber} host and its Windows VMs, you can use
ifeval::[{ProductNumber} == 9]
xref:sharing-files-between-the-host-and-its-virtual-machines-using-virtiofs_sharing-files-between-the-host-and-its-virtual-machines-using-virtio-fs[virtiofs] or
endif::[]
xref:sharing-files-between-the-host-and-its-virtual-machines-using-NFS_sharing-files-between-the-host-and-its-virtual-machines[NFS].

:context: {parent-context-of-installing-and-managing-windows-virtual-machines-on-rhel}
