:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_monitoring-containers: {context}]


ifndef::context[]
[id="assembly_monitoring-containers"]
endif::[]
ifdef::context[]
[id="assembly_monitoring-containers_{context}"]
endif::[]
= Monitoring containers

:context: assembly_monitoring-containers


[role="_abstract"]
Use Podman commands to manage a Podman environment. With that, you can determine the health of the container, by displaying system and pod information, and monitoring Podman events.

//== Prerequisites

include::modules/containers/con_using-a-health-check-on-a-container.adoc[leveloffset=+1]

include::modules/containers/proc_performing-a-health-check-using-the-command-line.adoc[leveloffset=+1]

include::modules/containers/proc_performing-a-health-check-using-a-containerfile.adoc[leveloffset=+1]

include::modules/containers/proc_displaying-podman-system-information.adoc[leveloffset=+1]

include::modules/containers/con_podman-event-types.adoc[leveloffset=+1]

include::modules/containers/proc_monitoring-podman-events.adoc[leveloffset=+1]

include::modules/containers/proc_using-podman-events-for-auditing.adoc[leveloffset=+1]

//[role="_additional-resources"]
//== Additional resources (or Next steps)


ifdef::parent-context-of-assembly_monitoring-containers[:context: {parent-context-of-assembly_monitoring-containers}]
ifndef::parent-context-of-assembly_monitoring-containers[:!context:]
