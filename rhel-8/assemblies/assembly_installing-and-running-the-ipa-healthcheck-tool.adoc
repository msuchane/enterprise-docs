:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following titles:
// titles/installing/installing-identity-management-and-acess-control

ifdef::context[:parent-context-of-installing-and-running-the-ipa-healthcheck-tool: {context}]

[id="installing-and-running-the-ipa-healthcheck-tool_{context}"]
= Installing and running the IdM Healthcheck tool
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: installing-and-running-the-ipa-healthcheck-tool

[role="_abstract"]
Learn more about the IdM Healthcheck tool and how to install and run it.

// Only display the following if this is the RHEL 8 Installation guide
ifeval::[{ProductNumber} == 8]
[NOTE]
====
//* The Healthcheck tool can only be installed on IdM servers, not clients.
* The Healthcheck tool is only available on RHEL 8.1 or later.
====
endif::[]

// Add modules here:

include::modules/identity-management/con_healthcheck-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-ipa-healthcheck.adoc[leveloffset=+1]

include::modules/identity-management/proc_running-idm-healthcheck.adoc[leveloffset=+1]

// If this is the smaller "Using IdM Healthcheck to monitor
// your IdM environment" title, include the following 2 modules:
ifeval::["{parent-context-of-installing-and-running-the-ipa-healthcheck-tool}" == "using-idm-healthcheck-to-monitor-your-idm-environment"]
include::modules/identity-management/con_log-rotation.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-log-rotation-using-the-idm-healthcheck.adoc[leveloffset=+1]

include::modules/identity-management/con_changing-idm-healthcheck-configuration.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-healthcheck-to-change-the-output-logs-format.adoc[leveloffset=+1]
endif::[]

ifeval::["{ProductNumberLink}" == "8"]

[role="_additional-resources"]
== Additional resources
* See the following sections of the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/index[Configuring and managing Identity Management] guide for examples of using IdM Healthcheck.
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/checking-services-using-idm-healthcheck_configuring-and-managing-idm[Checking services]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/verifying-your-idm-and-ad-trust-configuration-using-idm-healthcheck_configuring-and-managing-idm[Verifying your IdM and AD trust configuration]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/verifying-certificates-using-idm-healthcheck_configuring-and-managing-idm[Verifying certificates]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/verifying-system-certificates-using-idm-healthcheck_configuring-and-managing-idm[Verifying system certificates]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/checking-disk-space-using-idm-healthcheck_configuring-and-managing-idm[Checking disk space]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/verifying-permissions-of-idm-configuration-files-using-healthcheck_configuring-and-managing-idm[Verifying permissions of IdM configuration files]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/checking-idm-replication-using-healthcheck_configuring-and-managing-idm[Checking replication]
* You can also see those chapters organized into a single guide: link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/using_idm_healthcheck_to_monitor_your_idm_environment/index[Using IdM Healthcheck to monitor your IdM environment]

endif::[]

ifeval::["{ProductNumberLink}" == "9"]

[role="_additional-resources"]
== Additional resources
* See the following sections of the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_idm_healthcheck_to_monitor_your_idm_environment[Using IdM Healthcheck to monitor your IdM environment] guide for examples of using IdM Healthcheck.
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_idm_healthcheck_to_monitor_your_idm_environment/checking-services-using-idm-healthcheck_using-idm-healthcheck-to-monitor-your-idm-environment[Checking services]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_idm_healthcheck_to_monitor_your_idm_environment/verifying-your-idm-and-ad-trust-configuration-using-idm-healthcheck_using-idm-healthcheck-to-monitor-your-idm-environment[Verifying your IdM and AD trust configuration]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_idm_healthcheck_to_monitor_your_idm_environment/verifying-certificates-using-idm-healthcheck_using-idm-healthcheck-to-monitor-your-idm-environment[Verifying certificates]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_idm_healthcheck_to_monitor_your_idm_environment/verifying-system-certificates-using-idm-healthcheck_using-idm-healthcheck-to-monitor-your-idm-environment[Verifying system certificates]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_idm_healthcheck_to_monitor_your_idm_environment/checking-disk-space-using-idm-healthcheck_using-idm-healthcheck-to-monitor-your-idm-environment[Checking disk space]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_idm_healthcheck_to_monitor_your_idm_environment/verifying-permissions-of-idm-configuration-files-using-healthcheck_using-idm-healthcheck-to-monitor-your-idm-environment[Verifying permissions of IdM configuration files]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_idm_healthcheck_to_monitor_your_idm_environment/checking-idm-replication-using-healthcheck_using-idm-healthcheck-to-monitor-your-idm-environment[Checking replication]

endif::[]



// Restore the context to what it was before this assembly.
ifdef::parent-context-of-installing-and-running-the-ipa-healthcheck-tool[:context: {parent-context-of-installing-and-running-the-ipa-healthcheck-tool}]
ifndef::parent-context-of-installing-and-running-the-ipa-healthcheck-tool[:!context:]
