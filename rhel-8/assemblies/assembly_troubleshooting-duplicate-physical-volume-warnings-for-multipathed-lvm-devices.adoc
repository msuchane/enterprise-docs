:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-troubleshooting-duplicate-physical-volume-warnings-for-multipathed-lvm-devices: {context}]


[id="troubleshooting-duplicate-physical-volume-warnings-for-multipathed-lvm-devices_{context}"]
= Troubleshooting duplicate physical volume warnings for multipathed LVM devices

:context: troubleshooting-duplicate-physical-volume-warnings-for-multipathed-lvm-devices

When using LVM with multipathed storage, LVM commands that list a volume group or logical volume might display messages such as the following:

----
Found duplicate PV GDjTZf7Y03GJHjteqOwrye2dcSCjdaUi: using /dev/dm-5 not /dev/sdd
Found duplicate PV GDjTZf7Y03GJHjteqOwrye2dcSCjdaUi: using /dev/emcpowerb not /dev/sde
Found duplicate PV GDjTZf7Y03GJHjteqOwrye2dcSCjdaUi: using /dev/sddlmab not /dev/sdf
----

You can troubleshoot these warnings to understand why LVM displays them, or to hide the warnings.

// .Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

include::modules/filesystems-and-storage/con_root-cause-of-duplicate-pv-warnings.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_cases-of-duplicate-pv-warnings.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/ref_example-lvm-device-filters-that-prevent-duplicate-pv-warnings.adoc[leveloffset=+1]

// include::modules/filesystems-and-storage/proc_applying-an-lvm-device-filter-configuration.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
ifdef::lvm-guide[]
* xref:limiting-lvm-device-visibility-and-usage_configuring-and-managing-logical-volumes[Limiting LVM device visibility and usage]
endif::[]
ifndef::lvm-guide[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/limiting-lvm-device-visibility-and-usage_configuring-and-managing-logical-volumes[Limiting LVM device visibility and usage]
endif::[]


* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/limiting-lvm-device-visibility-and-usage_configuring-and-managing-logical-volumes#the-lvm-device-filter_limiting-lvm-device-visibility-and-usage[The LVM device filter]

ifdef::parent-context-of-troubleshooting-duplicate-physical-volume-warnings-for-multipathed-lvm-devices[:context: {parent-context-of-troubleshooting-duplicate-physical-volume-warnings-for-multipathed-lvm-devices}]
ifndef::parent-context-of-troubleshooting-duplicate-physical-volume-warnings-for-multipathed-lvm-devices[:!context:]
