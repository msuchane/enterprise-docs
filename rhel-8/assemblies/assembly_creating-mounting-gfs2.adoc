:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}

[id='assembly_creating-mounting-gfs2-{context}']
= Administering GFS2 file systems
:context: creating-mounting-gfs2

[role="_abstract"]
There are a variety of commands and options that you use to create, mount, grow, and manage GFS2 file systems.

include::modules/high-availability/proc_creating-gfs2.adoc[leveloffset=+1]

include::modules/high-availability/proc_mounting-gfs2-filesystem.adoc[leveloffset=+1]

include::modules/high-availability/proc_backing-up-a-gfs2-filesystem.adoc[leveloffset=+1]

include::modules/high-availability/proc_suspending-activity-on-a-gfs2-filesystem.adoc[leveloffset=+1]

include::modules/high-availability/proc_growing-gfs2-filesystem.adoc[leveloffset=+1]

include::modules/high-availability/proc_adding-gfs2-journals.adoc[leveloffset=+1]


:context: {parent-context}


