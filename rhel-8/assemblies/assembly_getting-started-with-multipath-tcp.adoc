:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following titles:
//
// Configuring and managing networking

// Retains the context of the parent assembly if this assembly is nested within another assembly.
// For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
// See also the complementary step on the last line of this file.
ifdef::context[:parent-context-of-getting-started-with-multipath-tcp: {context}]

// Base the file name and the ID on the assembly title. For example:
// * file name: my-assembly-a.adoc
// * ID: [id="my-assembly-a"]
// * Title: = My assembly A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
// If the assembly is reused in other assemblies in a guide, include {context} in the ID: [id="a-collection-of-modules-{context}"].
[id="getting-started-with-multipath-tcp_{context}"]
= Getting started with Multipath TCP
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: getting-started-with-multipath-tcp
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

[role="_abstract"]
Transmission Control Protocol (TCP) ensures reliable delivery of the data through the internet and automatically adjusts its bandwidth in response to network load. Multipath TCP (MPTCP) is an extension to the original TCP protocol (single-path). MPTCP enables a transport connection to operate across multiple paths simultaneously, and brings network connection redundancy to user endpoint devices.


include::modules/networking/con_understanding-mptcp.adoc[leveloffset=+1]

include::modules/networking/proc_preparing-rhel-to-enable-mptcp-support.adoc[leveloffset=+1]

include::modules/networking/proc_using-iproute2-to-temporarily-configure-and-enable-multiple-paths-for-mptcp-applications.adoc[leveloffset=+1]

include::modules/networking/proc_permanently-configuring-multiple-paths-for-mptcp-applications.adoc[leveloffset=+1]

include::modules/networking/proc_monitoring-mptcp-sub-flows.adoc[leveloffset=+1]

include::modules/networking/proc_disabling-multipath-tcp-in-the-kernel.adoc[leveloffset=+1]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-getting-started-with-multipath-tcp[:context: {parent-context-of-getting-started-with-multipath-tcp}]
ifndef::parent-context-of-getting-started-with-multipath-tcp[:!context:]
