:_mod-docs-content-type: ASSEMBLY
:parent-context-of-getting-started-with-tuned: {context}

[id='getting-started-with-tuned_{context}']
= Getting started with TuneD

:context: getting-started-with-tuned

[role="_abstract"]
As a system administrator, you can use the *TuneD* application to optimize the performance profile of your system for a variety of use cases.

// .Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.


include::modules/performance/con_the-purpose-of-tuned.adoc[leveloffset=+1]

include::modules/performance/con_tuned-profiles.adoc[leveloffset=+1]

include::modules/performance/con_the-default-tuned-profile.adoc[leveloffset=+1]

include::modules/performance/con_merged-tuned-profiles.adoc[leveloffset=+1]

include::modules/performance/con_the-location-of-tuned-profiles.adoc[leveloffset=+1]

include::modules/performance/ref_tuned-profiles-distributed-with-rhel.adoc[leveloffset=+1]

include::modules/performance/con_tuned-cpu-partitioning-profile.adoc[leveloffset=+1]

include::modules/performance/proc_using-the-tuned-cpu-partitioning-profile-for-low-latency-tuning.adoc[leveloffset=+1]

include::modules/performance/proc_customizing-the-cpu-partitioning-tuned-profile.adoc[leveloffset=+1]

include::modules/performance/ref_real-time-tuned-profiles-distributed-with-rhel.adoc[leveloffset=+1]

include::modules/performance/con_static-and-dynamic-tuning-in-tuned.adoc[leveloffset=+1]

include::modules/performance/con_tuned-no-daemon-mode.adoc[leveloffset=+1]

include::modules/performance/proc_installing-and-enabling-tuned.adoc[leveloffset=+1]

include::modules/performance/proc_listing-available-tuned-profiles.adoc[leveloffset=+1]

include::modules/performance/proc_setting-a-tuned-profile.adoc[leveloffset=+1]

include::assembly_using-the-tuned-d-bus-interface.adoc[leveloffset=+1]

include::modules/performance/proc_disabling-tuned.adoc[leveloffset=+1]

ifdef::upstream[]
[id='related-information-{context}']
[role="_additional-resources"]
== Additional resources
* The `tuned(8)` man page
* The `tuned-adm(8)` man page
* link:https://tuned-project.org/[The *TuneD* project website]
endif::[]

:context: {parent-context-of-getting-started-with-tuned}
