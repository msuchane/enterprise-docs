:_mod-docs-content-type: ASSEMBLY
include::modules/identity-management/_attributes-identity-management-and-access-control.adoc[]

:parent-context-of-logging-in-to-ipa-from-the-command-line: {context}

[id="logging-in-to-ipa-from-the-command-line_{context}"]
= Logging in to Identity Management from the command line
:context: logging-in-to-ipa-from-the-command-line

[role="_abstract"]
{IPA} (IdM) uses the Kerberos protocol to support single sign-on. Single sign-on means that the user enters the correct user name and password only once, and then accesses IdM services without the system prompting for the credentials again.

[IMPORTANT]
====
In IdM, the {SSSD} (SSSD) automatically obtains a ticket-granting ticket (TGT) for a user after the user successfully logs in to the desktop environment on an IdM client machine with the corresponding Kerberos principal name. This means that after logging in, the user is not required to use the *kinit* utility to access IdM resources.
====

//You are already logged into IdM when you log onto your local machine. Kerberos utilities `kinit`, `klist`, `kdestroy`, and `kvno` are provided by the `krb5-workstation` package. This package is required by the `ipa-client` package and is available on all RHEL machines enrolled into IdM.

//If a system where you are planning to use Kerberos authentication has not been enrolled into IdM through `ipa-client-install`, make sure the `krb5-workstation` package is installed.

If you have cleared your Kerberos credential cache or your Kerberos TGT has expired, you need to request a Kerberos ticket manually to access IdM resources. The following sections present basic user operations when using Kerberos in IdM.

include::modules/identity-management/proc_using-kinit-to-log-in-to-ipa.adoc[leveloffset=+1]

//include::modules/identity-management/proc_storing-multiple-kerberos-tickets.adoc[leveloffset=+1]

//include::modules/identity-management/proc_obtaining-kerberos-tickets-automatically.adoc[leveloffset=+1]

include::modules/identity-management/proc_destroying-the-users-active-kerberos-ticket-idm.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-an-external-system-for-kerberos-authentication.adoc[leveloffset=+1]


//[id='additional-resources-{context}']
[role="_additional-resources"]
== Additional resources
* The `krb5.conf(5)` man page.
* The `kinit(1)` man page.
* The `klist(1)` man page.
* The `kdestroy(1)` man page.

:context: {parent-context-of-logging-in-to-ipa-from-the-command-line}
