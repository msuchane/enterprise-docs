:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_overview-of-network-time-security-in-chrony: {context}]

ifndef::context[]
[id="assembly_overview-of-network-time-security-in-chrony"]
endif::[]
ifdef::context[]
[id="assembly_overview-of-network-time-security-in-chrony_{context}"]
endif::[]
= Overview of Network Time Security (NTS) in chrony

:context: assembly_overview-of-network-time-security-in-chrony

[role="_abstract"]
Network Time Security (NTS) is an authentication mechanism for Network Time Protocol (NTP), designed to scale substantial clients. It verifies that the packets received from the server machines are unaltered while moving to the client machine.
Network Time Security (NTS) includes a Key Establishment (NTS-KE) protocol that automatically creates the encryption keys used between the server and its clients.

[WARNING]
====
NTS is not compatible with the FIPS and OSPP profile. When you enable the FIPS and OSPP profile, `chronyd` that is configured with NTS can abort with a fatal message. You can disable the OSPP profile and FIPS mode for `chronyd` service by adding the `GNUTLS_FORCE_FIPS_MODE=0` to the `/etc/sysconfig/chronyd` file.
====

include::modules/core-services/proc_enabling-nts-in-the-client-configuration-file.adoc[leveloffset=+1]

include::modules/core-services/proc_enabling-nts-on-the-server.adoc[leveloffset=+1]

ifdef::parent-context-of-assembly_overview-of-network-time-security-in-chrony[:context: {parent-context-of-assembly_overview-of-network-time-security-in-chrony}]
ifndef::parent-context-of-assembly_overview-of-network-time-security-in-chrony[:!context:]
