:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-shutting-down-suspending-and-hibernating-the-system: {context}]


[id="shutting-down-suspending-and-hibernating-the-system_{context}"]
= Shutting down, suspending, and hibernating the system

:context: shutting-down-suspending-and-hibernating-the-system

[role="_abstract"]
As a system administrator, you can use different power management options to manage power consumption, perform a proper shutdown to ensure that all data is saved, or restart the system to apply changes and updates.

include::modules/core-services/con_system-shutdown.adoc[leveloffset=+1]

include::modules/core-services/proc_scheduling-a-system-shutdown.adoc[leveloffset=+1]

include::modules/core-services/proc_shutting-down-the-system-using-systemctl-command.adoc[leveloffset=+1]

include::modules/core-services/proc_restarting-the-system.adoc[leveloffset=+1]

include::modules/core-services/proc_optimizing-power-consumption-by-suspending-and-hibernating-the-system.adoc[leveloffset=+1]

include::modules/core-services/ref_overview-of-the-power-management-commands-with-systemctl.adoc[leveloffset=+1]

include::assembly_changing-the-power-button-behavior.adoc[leveloffset=+1]

ifdef::parent-context-of-shutting-down-suspending-and-hibernating-the-system[:context: {parent-context-of-shutting-down-suspending-and-hibernating-the-system}]
ifndef::parent-context-of-shutting-down-suspending-and-hibernating-the-system[:!context:]

