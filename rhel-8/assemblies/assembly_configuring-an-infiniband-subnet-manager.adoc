:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-configuring-an-infiniband-subnet-manager: {context}]

[id="configuring-an-infiniband-subnet-manager_{context}"]
= Configuring an InfiniBand subnet manager

:context: configuring-an-infiniband-subnet-manager

[role="_abstract"]
All InfiniBand networks must have a subnet manager running for the network to function. This is true even if two machines are connected directly with no switch involved.

It is possible to have more than one subnet manager. In that case, one acts as a master and another subnet manager acts as a slave that will take over in case the master subnet manager fails.

Most InfiniBand switches contain an embedded subnet manager. However, if you need a more up-to-date subnet manager or if you require more control, use the `OpenSM` subnet manager provided by {RHEL}. 

ifeval::[{ProductNumber} >= 9]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_infiniband_and_rdma_networks/configuring-an-infiniband-subnet-manager_configuring-infiniband-and-rdma-networks#installing-the-opensm-subnet-manager_configuring-an-infiniband-subnet-manager[Installing the OpenSM subnet manager]
endif::[]

ifeval::[{ProductNumber} == 8]
include::modules/infiniband-rdma/proc_installing-the-opensm-subnet-manager.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_configuring-opensm-using-the-simple-method.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_configuring-opensm-by-editing-the-opensm-conf-file.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_configuring-multiple-opensm-instances.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_creating-a-partition-configuration.adoc[leveloffset=+1]
endif::[]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-configuring-an-infiniband-subnet-manager[:context: {parent-context-of-configuring-an-infiniband-subnet-manager}]
ifndef::parent-context-of-configuring-an-infiniband-subnet-manager[:!context:]
