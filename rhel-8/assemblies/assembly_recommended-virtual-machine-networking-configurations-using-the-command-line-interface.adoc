:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// configuring-virtual-machine-network-connections

ifdef::context[:parent-context-of-recommended-virtual-machine-networking-configurations-using-the-command-line-interface: {context}]

[id="recommended-virtual-machine-networking-configurations-using-the-command-line-interface_{context}"]
= Recommended virtual machine networking configurations

:context: recommended-virtual-machine-networking-configurations-using-the-command-line-interface

In many scenarios, the default VM networking configuration is sufficient. However, if adjusting the configuration is required, you can use the command-line interface (CLI) or the {ProductShortName} {ProductNumber} web console to do so. The following sections describe selected VM network setups for such situations.


include::modules/virtualization/proc_configuring-externally-visible-virtual-machines-using-the-command-line-interface.adoc[leveloffset=+1]

include::modules/virtualization/proc_configuring-externally-visible-virtual-machines-using-the-web-console.adoc[leveloffset=+1]



ifdef::parent-context-of-recommended-virtual-machine-networking-configurations-using-the-command-line-interface[:context: {parent-context-of-recommended-virtual-machine-networking-configurations-using-the-command-line-interface}]
ifndef::parent-context-of-recommended-virtual-machine-networking-configurations-using-the-command-line-interface[:!context:]
