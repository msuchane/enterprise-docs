:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-getting-started-with-selinux: {context}]


ifndef::context[]
[id="getting-started-with-selinux"]
endif::[]
ifdef::context[]
[id="getting-started-with-selinux_{context}"]
endif::[]
= Getting started with SELinux

:context: getting-started-with-selinux


[role="_abstract"]
Security Enhanced Linux (SELinux) provides an additional layer of system security. SELinux fundamentally answers the question: _May <subject> do <action> to <object>?_, for example: _May a web server access files in users' home directories?_

include::modules/security/con_introduction-to-selinux.adoc[leveloffset=+1]

include::modules/security/con_benefits-of-selinux.adoc[leveloffset=+1]

include::modules/security/con_selinux-examples.adoc[leveloffset=+1]

include::modules/security/con_selinux-architecture.adoc[leveloffset=+1]

include::modules/security/con_selinux-states-and-modes.adoc[leveloffset=+1]


ifdef::parent-context-of-getting-started-with-selinux[:context: {parent-context-of-getting-started-with-selinux}]
ifndef::parent-context-of-getting-started-with-selinux[:!context:]
