ifdef::context[:parent-context-of-managing-public-ssh-keys-for-hosts: {context}]


:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="managing-public-ssh-keys-for-hosts"]
endif::[]
ifdef::context[]
[id="managing-public-ssh-keys-for-hosts_{context}"]
endif::[]
= Managing public SSH keys for hosts

:context: managing-public-ssh-keys-for-hosts


OpenSSH uses public keys to authenticate hosts. One machine attempts to access another machine and presents its key pair. The first time the host authenticates, the administrator on the target machine has to approve the request manually. The machine then stores the host's public key in a `known_hosts` file. Any time that the remote machine attempts to access the target machine again, the target machine checks its `known_hosts` file and then grants access automatically to approved hosts.


include::modules/identity-management/proc_uploading-ssh-keys-for-a-host-using-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_uploading-ssh-keys-for-a-host-using-the-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_deleting-ssh-keys-for-a-host-using-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_deleting-ssh-keys-for-a-host-using-the-idm-cli.adoc[leveloffset=+1]





ifdef::parent-context-of-managing-public-ssh-keys-for-hosts[:context: {parent-context-of-managing-public-ssh-keys-for-hosts}]
ifndef::parent-context-of-managing-public-ssh-keys-for-hosts[:!context:]
