:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-updating-software-packages: {context}]

[id="updating-software-packages_{context}"]
= Updating software packages

:context: updating-software-packages

[subs="+attributes"]
[application]*{PackageManagerCommand}* allows you to check if your system has any pending updates. You can list packages that need updating and choose to update a single package, multiple packages, or all packages at once. If any of the packages you choose to update have dependencies, they are updated as well.

[subs="+attributes"]
The following section describes how to use *{PackageManagerCommand}* to:

* Check for updates.
* Update a single package.
* Update a package group.
* Update all packages and their dependencies.
* Apply security updates.
* Automate software updates.
// * Specify global expressions in {PackageManagerCommand} input.
// * Specify a package name in {PackageManagerCommand} input.

include::modules/core-services/proc_checking-for-updates-yum.adoc[leveloffset=+1]

include::modules/core-services/proc_updating-a-single-package-with-yum.adoc[leveloffset=+1]

include::modules/core-services/proc_updating-a-package-group-with-yum.adoc[leveloffset=+1]

include::modules/core-services/proc_updating-all-packages-and-dependencies-with-yum.adoc[leveloffset=+1]

include::modules/core-services/proc_updating-security-related-packages-with-yum.adoc[leveloffset=+1]

include::assembly_automatically-refreshing-package-database.adoc[leveloffset=+1]

// include::modules/core-services/con_specifying-global-expressions-in-yum-input.adoc[leveloffset=+1]

// include::modules/core-services/con_specifying-a-package-name-in-yum-input.adoc[leveloffset=+1]


ifdef::parent-context-of-updating-software-packages[:context: {parent-context-of-updating-software-packages}]
ifndef::parent-context-of-updating-software-packages[:!context:]
