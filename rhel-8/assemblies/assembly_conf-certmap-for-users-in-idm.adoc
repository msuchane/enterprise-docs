:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}

[id="conf-certmap-for-users-in-idm_{context}"]
= Configuring certificate mapping for users stored in IdM

:context: conf-certmap-for-users-in-idm

[role="_abstract"]
To enable certificate mapping in IdM if the user for whom certificate authentication is being configured is stored in IdM, a system administrator must complete the following tasks:

* Set up a certificate mapping rule so that IdM users with certificates that match the conditions specified in the mapping rule and in their certificate mapping data entries can authenticate to IdM.

* Enter certificate mapping data to an IdM user entry so that the user can authenticate using multiple certificates provided that they all contain the values specified in the certificate mapping data entry.

.Prerequisites

* The user has an account in IdM.
* The administrator has either the whole certificate or the certificate mapping data to add to the user entry.

include::modules/identity-management/proc_add-maprule-webui.adoc[leveloffset=+1]

include::modules/identity-management/proc_add-maprule-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_add-certmapdata-to-user-webui.adoc[leveloffset=+1]

include::modules/identity-management/proc_add-certmapdata-to-user-cli.adoc[leveloffset=+1]



:context: {parent-context}
