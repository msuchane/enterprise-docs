:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-factors-affecting-i-o-and-file-system-performance: {context}]

ifndef::context[]
[id="factors-affecting-i-o-and-file-system-performance"]
endif::[]
ifdef::context[]
[id="factors-affecting-i-o-and-file-system-performance_{context}"]
endif::[]
= Factors affecting I/O and file system performance

:context: factors-affecting-i-o-and-file-system-performance

[role="_abstract"]
The appropriate settings for storage and file system performance are highly dependent on the storage purpose.

I/O and file system performance can be affected by any of the following factors:

* Data write or read patterns
* Sequential or random
* Buffered or Direct IO
* Data alignment with underlying geometry
* Block size
* File system size
* Journal size and location
* Recording access times
* Ensuring data reliability
* Pre-fetching data
* Pre-allocating disk space
* File fragmentation
* Resource contention

include::modules/performance/con_tools-for-monitoring-and-diagnosing-i-o-and-file-system-issues.adoc[leveloffset=+1]

include::modules/performance/con_available-tuning-options-for-formatting-a-file-system.adoc[leveloffset=+1]

include::modules/performance/con_available-tuning-options-for-mounting-a-file-system.adoc[leveloffset=+1]

include::modules/performance/con_types-of-discarding-unused-blocks.adoc[leveloffset=+1]

include::modules/performance/con_solid-state-disks-tuning-considerations.adoc[leveloffset=+1]

include::modules/performance/con_generic-block-device-tuning-parameters.adoc[leveloffset=+1]


ifdef::parent-context-of-factors-affecting-i-o-and-file-system-performance[:context: {parent-context-of-factors-affecting-i-o-and-file-system-performance}]
ifndef::parent-context-of-factors-affecting-i-o-and-file-system-performance[:!context:]
