:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-lvm-vdo-requirements: {context}]

ifndef::context[]
[id="lvm-vdo-requirements"]
endif::[]
ifdef::context[]
[id="lvm-vdo-requirements_{context}"]
endif::[]
= LVM-VDO requirements

:context: lvm-vdo-requirements

[role="_abstract"]
VDO on LVM has certain requirements on its placement and your system resources.

include::modules/filesystems-and-storage/con_vdo-memory-requirements.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_vdo-storage-space-requirements.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/ref_examples-of-vdo-requirements-by-physical-size.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_placement-of-lvm-vdo-in-the-storage-stack.adoc[leveloffset=+1]

ifdef::parent-context-of-lvm-vdo-requirements[:context: {parent-context-of-lvm-vdo-requirements}]
ifndef::parent-context-of-lvm-vdo-requirements[:!context:]
