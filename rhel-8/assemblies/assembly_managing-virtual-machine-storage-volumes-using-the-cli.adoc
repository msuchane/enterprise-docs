// This assembly is included in the following assemblies:
//
// assembly_managing-storage-for-virtual-machines

ifdef::context[:parent-context-of-assembly_managing-virtual-machine-storage-volumes-using-the-cli: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_managing-virtual-machine-storage-volumes-using-the-cli"]
endif::[]
ifdef::context[]
[id="assembly_managing-virtual-machine-storage-volumes-using-the-cli_{context}"]
endif::[]
= Managing virtual machine storage volumes by using the CLI

:context: assembly_managing-virtual-machine-storage-volumes-using-the-cli

You can use the CLI to manage the following aspects of your storage volumes to assign storage to your virtual machines (VMs):

* xref:viewing-storage-volume-information-using-the-cli_{context}[View storage volume information]
* xref:creating-and-assigning-storage-volumes-using-the-cli_{context}[Create storage volumes]
* xref:deleting-storage-volumes-using-the-cli_{context}[Delete storage volumes]

include::modules/virtualization/proc_viewing-storage-volume-information-using-the-cli.adoc[leveloffset=+1]

include::assembly_creating-and-assigning-storage-volumes-using-the-cli.adoc[leveloffset=+1]

include::modules/virtualization/proc_deleting-storage-volumes-using-the-cli.adoc[leveloffset=+1]


////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-assembly_managing-virtual-machine-storage-volumes-using-the-cli[:context: {parent-context-of-assembly_managing-virtual-machine-storage-volumes-using-the-cli}]
ifndef::parent-context-of-assembly_managing-virtual-machine-storage-volumes-using-the-cli[:!context:]
