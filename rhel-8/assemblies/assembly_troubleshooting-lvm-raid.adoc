:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-troubleshooting-lvm-raid: {context}]


ifndef::context[]
[id="troubleshooting-lvm-raid"]
endif::[]
ifdef::context[]
[id="troubleshooting-lvm-raid_{context}"]
endif::[]
= Troubleshooting LVM RAID

:context: troubleshooting-lvm-raid

You can troubleshoot various issues in LVM RAID devices to correct data errors, recover devices, or replace failed devices.

// .Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.


include::modules/filesystems-and-storage/proc_checking-data-coherency-in-a-raid-logical-volume.adoc[leveloffset=+1]

//Recovering a failed device section is not required because refresh will not work to handle any raid device failures. There are not likely to be any official procedures that use refresh although it can be a useful tool in some specialized ad hoc situations.
//include::modules/filesystems-and-storage/proc_recovering-a-failed-raid-device-in-a-logical-volume.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_replacing-a-failed-raid-device-in-a-logical-volume.adoc[leveloffset=+1]


// == Additional resources (or Next steps)
//
// * A bulleted list of links to other material closely related to the contents of the assembly, including xref links to other assemblies in your collection.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

ifdef::parent-context-of-troubleshooting-lvm-raid[:context: {parent-context-of-troubleshooting-lvm-raid}]
ifndef::parent-context-of-troubleshooting-lvm-raid[:!context:]

