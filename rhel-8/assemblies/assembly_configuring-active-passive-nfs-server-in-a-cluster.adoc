:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}

[id='assembly_configuring-active-passive-nfs-server-in-a-cluster-{context}']

= Configuring an active/passive NFS server in a Red Hat High Availability cluster
:context: configuring-ha-nfs

[role="_abstract"]
The Red Hat High Availability Add-On provides support for running a highly available active/passive NFS server on a Red Hat Enterprise Linux High Availability Add-On cluster using shared storage. In the following example, you are configuring a two-node cluster in which clients access the NFS file system through a floating IP address. The NFS server runs on one of the two nodes in the cluster. If the node on which the NFS server is running becomes inoperative, the NFS server starts up again on the second node of the cluster with minimal service interruption.

This use case requires that your system include the following components:

* A two-node Red Hat High Availability cluster with power fencing configured for each node. We recommend but do not require a private network. This procedure uses the cluster example provided in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_creating-high-availability-cluster-configuring-and-managing-high-availability-clusters[Creating a Red Hat High-Availability cluster with Pacemaker].

* A public virtual IP address, required for the NFS server.

* Shared storage for the nodes in the cluster, using iSCSI, Fibre Channel, or other shared network block device.

Configuring a highly available active/passive NFS server on an existing two-node Red Hat Enterprise Linux High Availability cluster requires that you perform the following steps:

. Configure a file system on an LVM logical volume on the shared storage for the nodes in the cluster.

. Configure an NFS share on the shared storage on the LVM logical volume.

. Create the cluster resources.

. Test the NFS server you have configured.

include::modules/high-availability/proc_configuring-lvm-volume-with-ext4-file-system.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 8]
include::modules/high-availability/proc_ensuring-cluster-volume-not-multiply-activated.adoc[leveloffset=+1]
endif::[]


include::modules/high-availability/proc_configuring-nfs-share.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-resources-for-nfs-server-in-a-cluster.adoc[leveloffset=+1]

include::modules/high-availability/proc_testing-nfs-resource-configuration.adoc[leveloffset=+1]

:context: {parent-context}


