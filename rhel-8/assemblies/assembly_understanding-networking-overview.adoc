:_mod-docs-content-type: ASSEMBLY
//This is included in the following assemblies:
//
// configuring-virtual-machine-network-connections

:parent-context-of-understanding-networking-overview: {context}


[id="understanding-virtual-networking-overview_{context}"]
= Understanding virtual networking

:context: understanding-virtual-networking-overview

ifdef::internal[]
[cols="1,4"]
|===
| Included in |
configuring-and-managing-virtualization
| User story |
As a user, I want to understand basic virtualization networking concepts.
| Jira |
JIRA LINK
| BZ |
https://bugzilla.redhat.com/show_bug.cgi?id=1590909
| SMEs |
Jamie Bainbridge
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

The connection of virtual machines (VMs) to other devices and locations on a network has to be facilitated by the host hardware. The following sections explain the mechanisms of VM network connections and describe the default VM network setting.

include::modules/virtualization/con_how-virtual-networks-work.adoc[leveloffset=+1]

include::modules/virtualization/con_virtual-networking-default-configuration.adoc[leveloffset=+1]

:context: {parent-context-of-understanding-networking-overview}
