:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}

[id='assembly_configuring-gfs2-in-a-cluster-{context}']

= GFS2 file systems in a cluster
:context: configuring-gfs2-cluster

[role="_abstract"]
Use the following administrative procedures to configure GFS2 file systems in a Red Hat high availability cluster.

include::modules/high-availability/proc_configuring-gfs2-in-a-cluster.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-encrypted-gfs2.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 8]
include::modules/high-availability/proc_migrate-gfs2-rhel7-rhel8.adoc[leveloffset=+1]
endif::[]

:context: {parent-context}





