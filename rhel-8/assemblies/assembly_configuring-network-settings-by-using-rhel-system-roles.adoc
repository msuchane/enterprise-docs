ifdef::context[:parent-context-of-assembly_configuring-network-settings-by-using-rhel-system-roles: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_configuring-network-settings-by-using-rhel-system-roles"]
endif::[]
ifdef::context[]
[id="assembly_configuring-network-settings-by-using-rhel-system-roles_{context}"]
endif::[]
= Configuring network settings by using the {RHELSystemRoles}

:context: assembly_configuring-network-settings-by-using-rhel-system-roles

Administrators can automate network-related configuration and management tasks by using the `network` {RHELSystemRoles}.


include::modules/networking/proc_configuring-an-ethernet-connection-with-a-static-ip-address-by-using-the-network-rhel-system-role-with-an-interface-name.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-an-ethernet-connection-with-a-static-ip-address-by-using-the-network-rhel-system-role-with-a-device-path.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-an-ethernet-connection-with-a-dynamic-ip-address-by-using-the-network-rhel-system-role-with-an-interface-name.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-an-ethernet-connection-with-a-dynamic-ip-address-by-using-the-network-rhel-system-role-with-a-device-path.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-vlan-tagging-by-using-the-rhel-network-system-role.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-a-network-bridge-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-a-network-bond-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_configuring-an-ipoib-connection-by-using-the-network-system-role.adoc[leveloffset=+1]

include::modules/networking/proc_routing-traffic-from-a-specific-subnet-to-a-different-default-gateway-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-a-static-ethernet-connection-with-802-1x-network-authentication-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 9]
include::modules/networking/proc_configuring-a-wifi-connection-with-802-1x-network-authentication-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]
endif::[]

include::modules/networking/proc_setting-the-default-gateway-on-an-existing-connection-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-a-static-route-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-an-ethtool-offload-feature-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-an-ethtool-coalesce-settings-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

include::modules/networking/proc_increasing-the-ring-buffer-size-to-reduce-a-high-packet-drop-rate-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

include::modules/networking/ref_network-states-for-the-network-rhel-system-role.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_configuring-network-settings-by-using-rhel-system-roles[:context: {parent-context-of-assembly_configuring-network-settings-by-using-rhel-system-roles}]
ifndef::parent-context-of-assembly_configuring-network-settings-by-using-rhel-system-roles[:!context:]
