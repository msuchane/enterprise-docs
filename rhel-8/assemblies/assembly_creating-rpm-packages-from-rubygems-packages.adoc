
ifdef::context[:parent-context-of-assembly_creating-rpm-packages-from-rubygems-packages: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_creating-rpm-packages-from-rubygems-packages"]
endif::[]
ifdef::context[]
[id="assembly_creating-rpm-packages-from-rubygems-packages_{context}"]
endif::[]
= Creating RPM packages from RubyGems packages

:context: assembly_creating-rpm-packages-from-rubygems-packages


[role="_abstract"]
To create a source RPM for a RubyGems package, the following files are needed:

// * RubyGems SPEC file (a file or an archive with the `.gem` extension)
* A gem file
* An RPM `spec` file

The following sections describe how to create RPM packages from packages created by RubyGems.

// For information about RubyGems SPEC file conventions, see xref:rubygems-spec-file-conventions[].

// Moved here from con_what-ruby-gem-packages-are.adoc based on SME review
// Moved to the intro section
// [discrete]
// == Naming conventions
//
// Names of packages created by RubyGems must follow this pattern:
//
// [literal,subs="+quotes,verbatim,normal,normal"]
// ....
// rubygem-%{gem_name}
// ....

// Moved here from con_what-ruby-gem-packages-are.adoc based on SME review
// Moved to the intro section
//[discrete]
//== Implementing a shebang line
//
//To implement a shebang line, packages created by RubyGems use the following string:
//
//[literal,subs="+quotes,verbatim,normal,normal"]
//....
//#!/usr/bin/ruby
//....



include::modules/core-services/con_rubygems-spec-file-conventions.adoc[leveloffset=+1]

include::modules/core-services/ref_rubygems-macros.adoc[leveloffset=+1]

include::modules/core-services/ref_rubygems-spec-file-example.adoc[leveloffset=+1]

include::assembly_converting-rubygems-packages-to-rpm-spec-files-with-gem2rpm.adoc[leveloffset=+1]




ifdef::parent-context-of-assembly_creating-rpm-packages-from-rubygems-packages[:context: {parent-context-of-assembly_creating-rpm-packages-from-rubygems-packages}]
ifndef::parent-context-of-assembly_creating-rpm-packages-from-rubygems-packages[:!context:]
