:_newdoc-version: 2.15.0
:_template-generated: 2023-11-20

ifdef::context[:parent-context-of-integrating-rhel-systems-into-ad-directly-with-ansible-using-rhel-system-roles: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="integrating-rhel-systems-into-ad-directly-with-ansible-using-rhel-system-roles"]
endif::[]
ifdef::context[]
[id="integrating-rhel-systems-into-ad-directly-with-ansible-using-rhel-system-roles_{context}"]
endif::[]
= Integrating RHEL systems into AD directly by using the RHEL system role

:context: integrating-rhel-systems-into-ad-directly-with-ansible-using-rhel-system-roles

[role="_abstract"]
With the `ad_integration` system role, you can automate a direct integration of a RHEL system with Active Directory (AD) by using Red Hat Ansible Automation Platform.

ifdef::u-a-in-idm[]
IMPORTANT: The `ad_integration` system role is not included in the `ansible-freeipa` package. It is part of the `rhel-system-roles` package. You can install `rhel-system-roles` on systems with a `Red Hat Enterprise Linux Server` subscription attached.
endif::[]


include::modules/system-roles/con_the-ad-integration-system-role.adoc[leveloffset=+1]


// This module is temporarily commented out for RHEL 9 because of a known issue on the Microsoft side. See https://issues.redhat.com/browse/RHELDOCS-17720
ifeval::[{ProductNumber} == 8]
include::modules/system-roles/proc_connecting-a-rhel-system-directly-to-ad-using-the-ad-integration-system-role.adoc[leveloffset=+1]
endif::[]


ifdef::parent-context-of-integrating-rhel-systems-into-ad-directly-with-ansible-using-rhel-system-roles[:context: {parent-context-of-integrating-rhel-systems-into-ad-directly-with-ansible-using-rhel-system-roles}]
ifndef::parent-context-of-integrating-rhel-systems-into-ad-directly-with-ansible-using-rhel-system-roles[:!context:]

