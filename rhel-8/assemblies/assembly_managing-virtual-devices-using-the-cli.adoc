// This assembly is included in the following assemblies:
//
// assembly_managing-virtual-devices.adoc

ifdef::context[:parent-context-of-assembly_managing-virtual-devices-using-the-cli: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_managing-virtual-devices-using-the-cli"]
endif::[]
ifdef::context[]
[id="assembly_managing-virtual-devices-using-the-cli_{context}"]
endif::[]
= Managing devices attached to virtual machines by using the CLI

:context: assembly_managing-virtual-devices-using-the-cli

[role="_abstract"]
To modify the functionality of your virtual machine (VM), you can manage the devices attached to your VM by using the command-line interface (CLI).

You can use the CLI to:

* xref:attaching-devices-to-virtual-machines_assembly_managing-virtual-devices-using-the-cli[Attach devices]
* xref:modifying-devices-attached-to-virtual-machines_assembly_managing-virtual-devices-using-the-cli[Modify devices]
* xref:removing-devices-from-virtual-machines_assembly_managing-virtual-devices-using-the-cli[Remove devices]

include::modules/virtualization/proc_attaching-devices-to-virtual-machines.adoc[leveloffset=+1]

include::modules/virtualization/proc_modifying-devices-attached-to-virtual-machines.adoc[leveloffset=+1]

include::modules/virtualization/proc_removing-devices-from-virtual-machines.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_managing-virtual-devices-using-the-cli[:context: {parent-context-of-assembly_managing-virtual-devices-using-the-cli}]
ifndef::parent-context-of-assembly_managing-virtual-devices-using-the-cli[:!context:]
