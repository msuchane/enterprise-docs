ifdef::context[:parent-context-of-assembly_example-protecting-a-lan-and-dmz-using-an-nftables-script: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_example-protecting-a-lan-and-dmz-using-an-nftables-script"]
endif::[]
ifdef::context[]
[id="assembly_example-protecting-a-lan-and-dmz-using-an-nftables-script_{context}"]
endif::[]
= Example: Protecting a LAN and DMZ using an nftables script

:context: assembly_example-protecting-a-lan-and-dmz-using-an-nftables-script
Use the `nftables` framework on a RHEL router to write and install a firewall script that protects the network clients in an internal LAN and a web server in a DMZ from unauthorized access from the internet and from other networks.

[IMPORTANT]
====
This example is only for demonstration purposes and describes a scenario with specific requirements.

Firewall scripts highly depend on the network infrastructure and security requirements. Use this example to learn the concepts of `nftables` firewalls when you write scripts for your own environment.
====

include::modules/packet-filtering/example-nftables-lan-dmz/con_network-conditions.adoc[leveloffset=+1]

include::modules/packet-filtering/example-nftables-lan-dmz/con_security-requirements-to-the-firewall-script.adoc[leveloffset=+1]

include::modules/packet-filtering/example-nftables-lan-dmz/proc_configuring-logging-of-dropped-packets-to-a-file.adoc[leveloffset=+1]

include::modules/packet-filtering/example-nftables-lan-dmz/proc_writing-and-activating-the-nftables-script.adoc[leveloffset=+1]



ifdef::parent-context-of-assembly_example-protecting-a-lan-and-dmz-using-an-nftables-script[:context: {parent-context-of-assembly_example-protecting-a-lan-and-dmz-using-an-nftables-script}]
ifndef::parent-context-of-assembly_example-protecting-a-lan-and-dmz-using-an-nftables-script[:!context:]

