ifdef::context[:parent-context-of-limiting-lvm-device-visibility-and-usage: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="limiting-lvm-device-visibility-and-usage"]
endif::[]
ifdef::context[]
[id="limiting-lvm-device-visibility-and-usage_{context}"]
endif::[]
= Limiting LVM device visibility and usage

:context: limiting-lvm-device-visibility-and-usage

You can limit the devices that are visible and usable to Logical Volume Manager (LVM) by controlling the devices that LVM can scan.

ifeval::[{ProductNumber} == 9]
Use LVM commands to control LVM device scanning. LVM commands interact with a file called the `system.devices` file, which lists the visible and usable devices. This feature is enabled by default in {RHEL} {ProductNumber}.

If you disable the devices file feature, the LVM device filter is enabled automatically.
endif::[]

To adjust the configuration of LVM device scanning, edit the LVM device filter settings in the `/etc/lvm/lvm.conf` file. The filters in the `lvm.conf` file consist of a series of simple regular expressions. The system applies these expressions to each device name in the `/dev` directory to decide whether to accept or reject each detected block device.

ifeval::[{ProductNumber} == 9]
include::assembly_the-lvm-devices-file.adoc[leveloffset=+1]
endif::[]

include::modules/filesystems-and-storage/con_persistent-identifiers-for-lvm-filtering.adoc[leveloffset=+1]

include::assembly_the-lvm-device-filter.adoc[leveloffset=+1]

ifdef::parent-context-of-limiting-lvm-device-visibility-and-usage[:context: {parent-context-of-limiting-lvm-device-visibility-and-usage}]
ifndef::parent-context-of-limiting-lvm-device-visibility-and-usage[:!context:]
