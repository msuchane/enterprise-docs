:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-using-ansible-playbooks-to-manage-idm-dns-zones: {context}]
[id="using-ansible-playbooks-to-manage-idm-dns-zones_{context}"]
= Using Ansible playbooks to manage IdM DNS zones

:context: using-ansible-playbooks-to-manage-idm-dns-zones
:ansible-dns-zones:

[role="_abstract"]
As {IPA} (IdM) administrator, you can manage how IdM DNS zones work using the `dnszone` module available in the `ansible-freeipa` package. 

* xref:supported-dns-zone-types_{context}[What DNS zone types are supported in IdM]
* xref:configuration-attributes-of-primary-idm-dns-zones_{context}[What DNS attributes you can configure in IdM]
* xref:using-ansible-to-create-a-primary-zone-in-idm-dns_{context}[How to use an Ansible playbook to create a primary zone in IdM DNS]
* xref:using-an-ansible-playbook-to-ensure-the-presence-of-a-primary-dns-zone-in-idm-with-multiple-variables_{context}[How to use an Ansible playbook to ensure the presence of a primary IdM DNS zone with multiple variables]
* xref:using-an-ansible-playbook-to-ensure-the-presence-of-a-zone-for-reverse-dns-lookup-when-an-ip-address-is-given_{context}[How to use an Ansible playbook to ensure the presence of a zone for reverse DNS lookup when an IP address is given]


.Prerequisites

* DNS service is installed on the IdM server. For more information about how to use {RH} Ansible Engine to install an IdM server with integrated DNS, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/installing_identity_management/index#installing-an-Identity-Management-server-using-an-Ansible-playbook_installing-identity-management[Installing an Identity Management server using an Ansible playbook].


include::modules/identity-management/con_supported-dns-zone-types.adoc[leveloffset=+1]

include::modules/identity-management/ref_configuration-attributes-of-primary-idm-dns-zones.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-create-a-primary-zone-in-idm-dns.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-an-ansible-playbook-to-ensure-the-presence-of-a-primary-dns-zone-in-idm-with-multiple-variables.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-an-ansible-playbook-to-ensure-the-presence-of-a-zone-for-reverse-dns-lookup-when-an-ip-address-is-given.adoc[leveloffset=+1]

:!ansible-dns-zones:

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-using-ansible-playbooks-to-manage-idm-dns-zones[:context: {parent-context-of-using-ansible-playbooks-to-manage-idm-dns-zones}]
ifndef::parent-context-of-using-ansible-playbooks-to-manage-idm-dns-zones[:!context:]
