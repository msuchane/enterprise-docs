:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>
ifdef::context[:parent-context-of-assembly_creating-a-bootable-installation-medium: {context}]
ifndef::context[]
[id="assembly_creating-a-bootable-installation-medium"]
endif::[]
ifdef::context[]
[id="assembly_creating-a-bootable-installation-medium_{context}"]
endif::[]
= Creating a bootable installation medium for RHEL
:context: assembly_creating-a-bootable-installation-medium

[role="_abstract"]
ifdef::installation-title[]
This section contains information about using the ISO image file that you downloaded in xref:downloading-a-specific-beta-iso-image_downloading-beta-installation-images[Downloading the installation ISO image]
to create a bootable physical installation medium, such as a USB, DVD, or CD.
endif::[]
ifndef::installation-title[]

This section contains information about using the ISO image file that you have downloaded to create a bootable physical installation medium, such as a USB, DVD, or CD.
For more information about downloading the ISO images, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/performing_a_standard_rhel_{ProductNumber}_installation/index#downloading-beta-installation-images_assembly_preparing-for-your-installation[Downloading the installation ISO image]
endif::[]

ifeval::[{ProductNumber} == 8]
[NOTE]
====
By default, the [option]`inst.stage2=` boot option is used on the installation medium and is set to a specific label, for example, [option]`inst.stage2=hd:LABEL=RHEL8\x86_64`. If you modify the default label of the file system containing the runtime image, or if you use a customized procedure to boot the installation system, verify that the label is set to the correct value.
====
endif::[]


include::modules/installer/con_choose-an-installation-boot-method.adoc[leveloffset=+1]

include::modules/installer/con_making-an-installation-cd-or-dvd.adoc[leveloffset=+1]

include::modules/installer/proc_creating-bootable-usb-linux.adoc[leveloffset=+1]

include::modules/installer/proc_creating-a-bootable-usb-windows.adoc[leveloffset=+1]

include::modules/installer/proc_creating-a-bootable-usb-mac.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_creating-a-bootable-installation-medium[:context: {parent-context-of-assembly_creating-a-bootable-installation-medium}]
ifndef::parent-context-of-assembly_creating-a-bootable-installation-medium[:!context:]
