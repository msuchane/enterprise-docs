ifdef::context[:parent-context-of-using-free-space-from-an-active-partition: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="using-free-space-from-an-active-partition"]
endif::[]
ifdef::context[]
[id="using-free-space-from-an-active-partition_{context}"]
endif::[]
= Using free space from an active partition

:context: using-free-space-from-an-active-partition

This process can be difficult to manage because an active partition, that is already in use, contains the required free space. In most cases, hard disks of computers with preinstalled software contain one larger partition holding the operating system and data.

[WARNING]
====
If you want to use an operating system (OS) on an active partition, you must reinstall the OS. Be aware that some computers, which include pre-installed software, do not include installation media to reinstall the original OS. Check whether this applies to your OS before you destroy an original partition and the OS installation.
====

To optimise the use of available free space, you can use the methods of destructive or non-destructive repartitioning.

include::modules/filesystems-and-storage/con_destructive-repartitioning.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_non-destructive-repartitioning.adoc[leveloffset=+1]

ifdef::parent-context-of-using-free-space-from-an-active-partition[:context: {parent-context-of-using-free-space-from-an-active-partition}]
ifndef::parent-context-of-using-free-space-from-an-active-partition[:!context:]
