#!/bin/bash

# tag-content.sh - tag all modules and assemblies with an appropriate tag
# Copyright (C) 2021 Levi V <levi@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.


# navigate to the root of the reppo
git_root=$(git rev-parse --show-toplevel)
cd $git_root

# replace old :_module-type: tag with new :_content-type: tag
all_adoc_files=$(find . -type f -name '*.adoc')
old_tag_files=$(grep -rl ":_module-type:" $all_adoc_files)
for i in $old_tag_files; do sed -i 's/:_module-type:/:_content-type:/g' $i; done

# tag all assemblies
all_assemblies=$(find . -type f -name 'assembly*.adoc')
no_tag_assemblies=$(grep -LH "^:_content-type: ASSEMBLY" $all_assemblies)
for i in $no_tag_assemblies; do sed -i '1 i\:_content-type: ASSEMBLY' $i; done

# tag all concept modules
all_concept_modules=$(find . -type f -name 'con*.adoc')
no_tag_concept_modules=$(grep -LH "^:_content-type: CONCEPT" $all_concept_modules)
for i in $no_tag_concept_modules; do sed -i '1 i\:_content-type: CONCEPT' $i; done

# tag all procedure modules
all_procedure_modules=$(find . -type f -name 'proc*.adoc')
no_tag_procedure_modules=$(grep -LH "^:_content-type: PROCEDURE" $all_procedure_modules)
for i in $no_tag_procedure_modules; do sed -i '1 i\:_content-type: PROCEDURE' $i; done

# tag all reference modules
all_reference_modules=$(find . -type f -name 'ref*.adoc')
no_tag_reference_modules=$(grep -LH "^:_content-type: REFERENCE" $all_reference_modules)
for i in $no_tag_reference_modules; do sed -i '1 i\:_content-type: REFERENCE' $i; done

# report files with multiple tags
multi_match=$(grep -o -c "^:_content-type:" $all_adoc_files | awk -F: '{if ($2 > 1){print $1}}' 2>/dev/null)

if [[ ! -z "$multi_match" ]]; then
    echo "Tag appears multiple times in the following files:"
    for i in $multi_match; do echo -e "\t" $i; done
fi

cd - > /dev/null
