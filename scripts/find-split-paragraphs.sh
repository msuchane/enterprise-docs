#!/bin/sh

# Copyright 2024 Marek Suchánek <msuchane@redhat.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage:
#
# This script looks for paragraphs split over multiple consecutive lines
# in AsciiDoc files.
# It produces CSV rows on the terminal output. You can redirect and save them
# to a file:
#
# rhel-8-docs]$ scripts/find-split-paragraphs.sh > split-paragraphs.csv

# Load common constants:
. "${0%/*}/lib/constants.sh"

# Check that the presciidoc dependency is installed:
if ! command -v presciidoc >/dev/null; then
    echo "Error: Install the presciidoc utility: https://github.com/msuchane/presciidoc"
    exit 1
fi

# Check that the ripgrep dependency is installed:
if ! command -v rg >/dev/null; then
    echo "Error: Install the ripgrep utility: https://github.com/BurntSushi/ripgrep#installation"
    exit 1
fi

# Find all files.
files=$(find "$REPO_TOPLEVEL" -not -path '*/\.*' -type f -regex "$FCC_PATTERN")

for file in $files; do
    # Search each file.
    # The regular expression finds where a paragraph ends in a word character
    # (\w) and the next line also starts with another word character (\w).
    # It ignores split paragraphs where the first line ends in a dot or colon.
    result=$(presciidoc --no-comments --no-blocks $file \
        | rg --multiline --line-number '^\w.*\w$\n\w+[\s\.]')
    
    # See how many issues this file has.
    issues_length=${#result}

    # If there's at least one issue, print the findings.
    if [ $issues_length -gt 0 ]; then
	    # Convert the file path from absolute to relative to the current working directory.
	    relative_path=$(realpath --relative-to=. $file)
	    # Print a CSV row with the findings.
        echo \"$relative_path\",\"$result\"
    fi
done
