#!/bin/bash

# list-missing-context.sh - list assemblies that do not define context
# Copyright (C) 2021 Jaromir Hradilek <jhradilek@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.


# Load common constants:
. "${0%/*}/lib/constants.sh"

# Locate all assemblies that do not define the context attribute:
find "$REPO_TOPLEVEL" -not -path '*/\.*' -type f -regex "$ASSEMBLY_PATTERN" | xargs -I %% -P 0 bash -c "grep -qe '^:context: *[^ \]\+' '%%' || echo '%%'"
